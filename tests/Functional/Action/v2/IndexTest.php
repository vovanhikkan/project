<?php

declare(strict_types=1);

namespace Test\Functional\Action\v2;

use Test\Functional\Action\Json;
use Test\Functional\Action\WebTestCase;

/**
 * IndexTest.
 */
class IndexTest extends WebTestCase
{
    public function testSuccess(): void
    {
        $response = $this->app()->handle(self::json('GET', '/v2'));

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('application/json', $response->getHeaderLine('Content-Type'));
        self::assertJson($body = (string)$response->getBody());

        $data = Json::decode($body);

        self::assertEquals(
            ['version' => 2],
            $data
        );
    }
}
