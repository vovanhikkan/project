<?php

declare(strict_types=1);

namespace Test\Functional\Action\v2\Auth;

use Test\Functional\Action\Json;
use Test\Functional\Action\WebTestCase;

/**
 * LoginTest.
 */
class LoginTest extends WebTestCase
{
    public function testEmpty(): void
    {
        $response = $this->app()->handle(self::json('POST', '/v2/auth/login'));

        self::assertEquals(400, $response->getStatusCode());
        self::assertJson($body = (string)$response->getBody());

        $data = Json::decode($body);

        self::assertEquals([
            'errors' => [
                [
                    'property' => 'email',
                    'message' => 'Заполните e-mail',
                ],
                [
                    'property' => 'password',
                    'message' => 'Заполните пароль',
                ]
            ],
        ], $data);
    }
}
