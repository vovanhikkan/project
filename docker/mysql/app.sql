-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: mysql
-- Время создания: Янв 18 2021 г., 09:13
-- Версия сервера: 8.0.22
-- Версия PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `app`
--
CREATE DATABASE IF NOT EXISTS `rosa` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `rosa`;

-- --------------------------------------------------------

--
-- Структура таблицы `actions`
--

CREATE TABLE `actions` (
  `id` int NOT NULL,
  `group_id` int DEFAULT NULL,
  `name` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `viewFrom` date DEFAULT NULL,
  `shownFrom` date DEFAULT NULL,
  `shownTo` date DEFAULT NULL,
  `discountAbsoluteValue` float DEFAULT NULL,
  `discountPercentageValue` float DEFAULT NULL,
  `cashbackPointsAbsoluteValue` float DEFAULT NULL,
  `cashbackPointsPercentageValue` float DEFAULT NULL,
  `minRoomDaysCount` int DEFAULT NULL,
  `maхRoomDaysCount` int DEFAULT NULL,
  `bookingFrom` int DEFAULT NULL,
  `bookingTo` int DEFAULT NULL,
  `minProductQuantity` int DEFAULT NULL,
  `maxProductQuantity` int DEFAULT NULL,
  `activationDateFrom` date DEFAULT NULL,
  `activationDateTo` date DEFAULT NULL,
  `hasLevels` tinyint(1) DEFAULT NULL,
  `hasPromocodes` tinyint(1) DEFAULT NULL,
  `promocodeLifetime` int DEFAULT NULL,
  `promocodePrefix` varchar(20) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `totalCount` int DEFAULT NULL,
  `activatedCount` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `actions_levels`
--

CREATE TABLE `actions_levels` (
  `id` int NOT NULL,
  `action_id` int DEFAULT NULL,
  `level_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `actions_products`
--

CREATE TABLE `actions_products` (
  `id` int NOT NULL,
  `action_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `actions_rate_plans`
--

CREATE TABLE `actions_rate_plans` (
  `id` int NOT NULL,
  `action_id` int DEFAULT NULL,
  `rate_plan_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `action_groups`
--

CREATE TABLE `action_groups` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `baskets`
--

CREATE TABLE `baskets` (
  `id` bigint NOT NULL,
  `order_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `base_price` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `promocode` varchar(255) DEFAULT NULL,
  `promocode_discount` float DEFAULT NULL,
  `given_points` int DEFAULT NULL,
  `given_points_discount` float DEFAULT NULL,
  `cost_to_pay` float DEFAULT NULL,
  `received_points` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `basket_items`
--

CREATE TABLE `basket_items` (
  `id` bigint NOT NULL,
  `basket_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `price_id` int DEFAULT NULL,
  `base_price` float DEFAULT NULL,
  `disqount` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `settings` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE `cart` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `status` smallint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cart_item`
--

CREATE TABLE `cart_item` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `cart_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `quantity` smallint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` bigint NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `web_icon` int DEFAULT NULL,
  `mobile_icon` int DEFAULT NULL,
  `mobile_background` int DEFAULT NULL,
  `web_gradient_color_1` varchar(9) DEFAULT NULL,
  `web_gradient_color_2` varchar(9) DEFAULT NULL,
  `has_sections` tinyint(1) DEFAULT NULL,
  `has_places` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories_places`
--

CREATE TABLE `categories_places` (
  `id` bigint NOT NULL,
  `category_id` bigint DEFAULT NULL,
  `place_id` bigint DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories_sections`
--

CREATE TABLE `categories_sections` (
  `id` bigint NOT NULL,
  `category_id` bigint DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('App\\Data\\Migrations\\Version20201028231709', '2021-01-15 14:42:27', 343),
('App\\Data\\Migrations\\Version20201028231802', '2021-01-15 14:42:28', 29),
('App\\Data\\Migrations\\Version20201028231803', '2021-01-15 14:42:28', 114),
('App\\Data\\Migrations\\Version20201028231808', '2021-01-15 14:42:28', 128),
('App\\Data\\Migrations\\Version20201107150322', '2021-01-15 14:42:28', 527),
('App\\Data\\Migrations\\Version20201109204018', '2021-01-15 14:42:29', 141),
('App\\Data\\Migrations\\Version20201111142406', '2021-01-15 14:42:29', 31),
('App\\Data\\Migrations\\Version20201111152749', '2021-01-15 14:42:29', 84),
('App\\Data\\Migrations\\Version20201111163343', '2021-01-15 14:42:29', 978),
('App\\Data\\Migrations\\Version20201113131721', '2021-01-15 14:42:30', 526),
('App\\Data\\Migrations\\Version20201115090801', '2021-01-15 14:42:30', 936),
('App\\Data\\Migrations\\Version20201117045538', '2021-01-15 14:42:31', 282),
('App\\Data\\Migrations\\Version20201122152642', '2021-01-15 14:42:32', 159),
('App\\Data\\Migrations\\Version20201128104455', '2021-01-15 14:42:32', 160),
('App\\Data\\Migrations\\Version20201208035741', '2021-01-15 14:42:32', 87),
('App\\Data\\Migrations\\Version20201208215847', '2021-01-15 14:42:32', 338),
('App\\Data\\Migrations\\Version20201209062146', '2021-01-15 14:42:32', 43),
('App\\Data\\Migrations\\Version20201211060856', '2021-01-15 14:42:33', 127),
('App\\Data\\Migrations\\Version20201211122523', '2021-01-15 14:42:33', 120),
('App\\Data\\Migrations\\Version20201211134420', '2021-01-15 14:42:33', 186),
('App\\Data\\Migrations\\Version20201211162708', '2021-01-15 14:42:33', 519),
('App\\Data\\Migrations\\Version20201211173858', '2021-01-15 14:42:34', 638),
('App\\Data\\Migrations\\Version20201211180511', '2021-01-15 14:42:34', 394),
('App\\Data\\Migrations\\Version20201211183420', '2021-01-15 14:42:35', 216),
('App\\Data\\Migrations\\Version20201211185712', '2021-01-15 14:42:35', 216),
('App\\Data\\Migrations\\Version20201214085522', '2021-01-15 14:42:35', 408),
('App\\Data\\Migrations\\Version20201217095557', '2021-01-15 14:42:36', 140),
('App\\Data\\Migrations\\Version20201217123245', '2021-01-15 14:42:36', 154),
('App\\Data\\Migrations\\Version20201218121225', '2021-01-15 14:42:36', 455),
('App\\Data\\Migrations\\Version20201222014858', '2021-01-15 14:42:36', 227),
('App\\Data\\Migrations\\Version20201222030620', '2021-01-15 14:42:37', 112),
('App\\Data\\Migrations\\Version20201222140214', '2021-01-15 14:42:37', 655),
('App\\Data\\Migrations\\Version20201223171736', '2021-01-15 14:42:37', 27),
('App\\Data\\Migrations\\Version20201223173044', '2021-01-15 14:42:37', 181),
('App\\Data\\Migrations\\Version20201223184524', '2021-01-15 14:42:38', 103),
('App\\Data\\Migrations\\Version20201223193210', '2021-01-15 14:42:38', 110),
('App\\Data\\Migrations\\Version20201223195419', '2021-01-15 14:42:38', 115),
('App\\Data\\Migrations\\Version20201223204523', '2021-01-15 14:42:38', 83),
('App\\Data\\Migrations\\Version20201224165442', '2021-01-15 14:42:38', 146),
('App\\Data\\Migrations\\Version20201225095752', '2021-01-15 14:42:38', 218),
('App\\Data\\Migrations\\Version20201225174006', '2021-01-15 14:42:39', 301),
('App\\Data\\Migrations\\Version20201225205428', '2021-01-15 14:42:39', 527),
('App\\Data\\Migrations\\Version20201225210118', '2021-01-15 14:42:39', 240),
('App\\Data\\Migrations\\Version20201226154254', '2021-01-15 14:42:40', 218),
('App\\Data\\Migrations\\Version20201228060256', '2021-01-15 14:42:40', 730),
('App\\Data\\Migrations\\Version20201228094941', '2021-01-15 14:42:41', 307),
('App\\Data\\Migrations\\Version20201228101503', '2021-01-15 14:42:41', 57),
('App\\Data\\Migrations\\Version20201228103832', '2021-01-15 14:42:41', 164),
('App\\Data\\Migrations\\Version20201228105309', '2021-01-15 14:42:41', 168),
('App\\Data\\Migrations\\Version20201228122239', '2021-01-15 14:42:41', 30),
('App\\Data\\Migrations\\Version20201228164901', '2021-01-15 14:42:41', 306),
('App\\Data\\Migrations\\Version20201229110726', '2021-01-15 14:42:42', 191),
('App\\Data\\Migrations\\Version20201229172736', '2021-01-15 14:42:42', 126),
('App\\Data\\Migrations\\Version20201229192810', '2021-01-15 14:42:42', 250),
('App\\Data\\Migrations\\Version20201229202110', '2021-01-15 14:42:42', 168),
('App\\Data\\Migrations\\Version20201230124715', '2021-01-15 14:42:43', 314),
('App\\Data\\Migrations\\Version20201230173341', '2021-01-15 14:42:43', 83),
('App\\Data\\Migrations\\Version20210106134212', '2021-01-15 14:42:43', 491),
('App\\Data\\Migrations\\Version20210106135631', '2021-01-15 14:42:44', 677),
('App\\Data\\Migrations\\Version20210106142711', '2021-01-15 14:42:44', 330);

-- --------------------------------------------------------

--
-- Структура таблицы `extra_place_types`
--

CREATE TABLE `extra_place_types` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `facilities`
--

CREATE TABLE `facilities` (
  `id` int NOT NULL,
  `facility_group_id` int NOT NULL,
  `name` json NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `web_icon` int DEFAULT NULL,
  `sort` int DEFAULT '1',
  `is_shown_in_room_list` tinyint(1) DEFAULT NULL,
  `mobile_icon` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `facility_groups`
--

CREATE TABLE `facility_groups` (
  `id` int NOT NULL,
  `name` json NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int DEFAULT '1',
  `web_icon` int DEFAULT NULL,
  `mobile_icon` int DEFAULT NULL,
  `isShownInHotel` tinyint(1) DEFAULT NULL,
  `isShownInRoom` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `food_types`
--

CREATE TABLE `food_types` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotels`
--

CREATE TABLE `hotels` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `starCount` int DEFAULT NULL,
  `location_id` bigint DEFAULT NULL,
  `address` json DEFAULT NULL,
  `logo_svg` int DEFAULT NULL,
  `logo` int DEFAULT NULL,
  `links` json DEFAULT NULL,
  `lat` decimal(9,6) DEFAULT NULL,
  `lon` decimal(9,6) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `subDescription` json DEFAULT NULL,
  `params` json DEFAULT NULL,
  `checkInTime` varchar(100) DEFAULT NULL,
  `checkOutTime` varchar(100) DEFAULT NULL,
  `typeId` int DEFAULT NULL,
  `webBackground` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotels_facilities`
--

CREATE TABLE `hotels_facilities` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `facility_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotels_photos`
--

CREATE TABLE `hotels_photos` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `photo` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotels_videos`
--

CREATE TABLE `hotels_videos` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `youtube_code` varchar(50) DEFAULT NULL,
  `background` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_age_range`
--

CREATE TABLE `hotel_age_range` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `min_age` int DEFAULT NULL,
  `max_age` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_services`
--

CREATE TABLE `hotel_services` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `priceType` int DEFAULT NULL,
  `photo` int DEFAULT NULL,
  `name` json DEFAULT NULL,
  `hasPriceValuesByDate` tinyint(1) DEFAULT NULL,
  `hasQuotaValuesByDate` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_service_price_values`
--

CREATE TABLE `hotel_service_price_values` (
  `id` int NOT NULL,
  `hotelServiceId` int DEFAULT NULL,
  `value` int DEFAULT NULL,
  `priceDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_service_quota_values`
--

CREATE TABLE `hotel_service_quota_values` (
  `id` int NOT NULL,
  `hotelServiceId` int DEFAULT NULL,
  `totalQuantity` int DEFAULT NULL,
  `occupiedQuantity` int DEFAULT NULL,
  `quotaDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_types`
--

CREATE TABLE `hotel_types` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `invoice`
--

CREATE TABLE `invoice` (
  `id` int NOT NULL,
  `order_id` int DEFAULT NULL,
  `pay_system_id` int DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `bank_responce` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `levels`
--

CREATE TABLE `levels` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `locations`
--

CREATE TABLE `locations` (
  `id` bigint NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `altitude` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `amount` bigint UNSIGNED NOT NULL,
  `status` smallint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` bigint NOT NULL,
  `status_id` int DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `comment` text,
  `manager_comment` text,
  `pps_settings` json DEFAULT NULL,
  `travelline_settings` json DEFAULT NULL,
  `user_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `order_item`
--

CREATE TABLE `order_item` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `order_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `price` bigint UNSIGNED NOT NULL,
  `quantity` smallint UNSIGNED NOT NULL,
  `amount` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `packets`
--

CREATE TABLE `packets` (
  `id` int NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `title` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `dayCount` int DEFAULT NULL,
  `minPrice` int DEFAULT NULL,
  `profit` int DEFAULT NULL,
  `foodTypeId` int DEFAULT NULL,
  `isWithAccommodation` tinyint(1) DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `webBackground` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `packets_rate_plans`
--

CREATE TABLE `packets_rate_plans` (
  `id` int NOT NULL,
  `packetId` int DEFAULT NULL,
  `ratePlanId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `packet_items`
--

CREATE TABLE `packet_items` (
  `id` int NOT NULL,
  `title` json DEFAULT NULL,
  `subTitle` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `photoId` int DEFAULT NULL,
  `placeId` bigint DEFAULT NULL,
  `packetId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

CREATE TABLE `payment` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `order_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `provider` smallint UNSIGNED NOT NULL,
  `external_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pay_systems`
--

CREATE TABLE `pay_systems` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `storage_id` int DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `hint` varchar(255) DEFAULT NULL,
  `parent` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `periods`
--

CREATE TABLE `periods` (
  `id` int NOT NULL,
  `pps_settings` json NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `places`
--

CREATE TABLE `places` (
  `id` bigint NOT NULL,
  `title` json DEFAULT NULL,
  `summary` json DEFAULT NULL,
  `subtitle` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `background` int DEFAULT NULL,
  `working_hours` json DEFAULT NULL,
  `age_restriction` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `code` varchar(30) DEFAULT NULL,
  `sort` int DEFAULT '1',
  `location_id` bigint DEFAULT NULL,
  `is_free` tinyint(1) DEFAULT NULL,
  `is_cableway_required` tinyint(1) DEFAULT NULL,
  `rating` json DEFAULT NULL,
  `button_text` json DEFAULT NULL,
  `width` int DEFAULT NULL,
  `min_price` int DEFAULT NULL,
  `label` json DEFAULT NULL,
  `address` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `places_blocks`
--

CREATE TABLE `places_blocks` (
  `id` bigint NOT NULL,
  `place_id` bigint DEFAULT NULL,
  `title` json DEFAULT NULL,
  `body` json DEFAULT NULL,
  `params` json DEFAULT NULL,
  `photo` int DEFAULT NULL,
  `video_youtube_code` varchar(50) DEFAULT NULL,
  `video_background` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `places_geo_points`
--

CREATE TABLE `places_geo_points` (
  `id` bigint NOT NULL,
  `lat` decimal(9,6) DEFAULT NULL,
  `lon` decimal(9,6) DEFAULT NULL,
  `description` json DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `place_id` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `places_photos`
--

CREATE TABLE `places_photos` (
  `id` bigint NOT NULL,
  `place_id` bigint DEFAULT NULL,
  `photo` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `places_sections`
--

CREATE TABLE `places_sections` (
  `id` bigint NOT NULL,
  `place_id` bigint DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `places_videos`
--

CREATE TABLE `places_videos` (
  `id` bigint NOT NULL,
  `place_id` bigint DEFAULT NULL,
  `youtube_code` varchar(50) DEFAULT NULL,
  `background` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pps_groups`
--

CREATE TABLE `pps_groups` (
  `id` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  `module_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pps_periods`
--

CREATE TABLE `pps_periods` (
  `id` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pps_prices`
--

CREATE TABLE `pps_prices` (
  `id` int NOT NULL,
  `period_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pps_products`
--

CREATE TABLE `pps_products` (
  `id` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  `group_id` bigint NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `prices`
--

CREATE TABLE `prices` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  `value` float NOT NULL,
  `base_value` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `section_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int NOT NULL,
  `section_id` int NOT NULL,
  `settings` json DEFAULT NULL,
  `pps_settings` json DEFAULT NULL,
  `name` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_price`
--

CREATE TABLE `product_price` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `price` bigint UNSIGNED NOT NULL,
  `date_start` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `date_end` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `promocodes`
--

CREATE TABLE `promocodes` (
  `id` int NOT NULL,
  `action_id` int DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `totalCount` int DEFAULT NULL,
  `activatedCount` int DEFAULT NULL,
  `expiredAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `quotas`
--

CREATE TABLE `quotas` (
  `id` int NOT NULL,
  `group_id` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `hasValuesByDate` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `quotas_products`
--

CREATE TABLE `quotas_products` (
  `id` int NOT NULL,
  `quota_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `quota_groups`
--

CREATE TABLE `quota_groups` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `quota_values`
--

CREATE TABLE `quota_values` (
  `id` int NOT NULL,
  `quota_id` int DEFAULT NULL,
  `totalQuantity` int DEFAULT NULL,
  `occupiedQuantity` int DEFAULT NULL,
  `quotaDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rate_plans`
--

CREATE TABLE `rate_plans` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `name` json DEFAULT NULL,
  `groupId` int DEFAULT NULL,
  `foodTypeId` int DEFAULT NULL,
  `taxTypeId` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `activeFrom` date DEFAULT NULL,
  `activeTo` date DEFAULT NULL,
  `shownFrom` date DEFAULT NULL,
  `shownTo` date DEFAULT NULL,
  `isShownWithPackets` tinyint(1) DEFAULT NULL,
  `isShownWithoutPackets` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rate_plans_rooms`
--

CREATE TABLE `rate_plans_rooms` (
  `id` int NOT NULL,
  `ratePlanId` int DEFAULT NULL,
  `roomId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rate_plan_groups`
--

CREATE TABLE `rate_plan_groups` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `refresh_token`
--

CREATE TABLE `refresh_token` (
  `id` int NOT NULL,
  `uid` int DEFAULT NULL,
  `refreshToken` varchar(68) NOT NULL,
  `ua` varchar(255) NOT NULL,
  `ip` binary(16) NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint NOT NULL,
  `user_id` int NOT NULL,
  `place_id` bigint DEFAULT NULL,
  `body` text,
  `rating` int DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE `rooms` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `name` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `area` int DEFAULT NULL,
  `mainPlaceCount` int DEFAULT NULL,
  `extraPlaceCount` int DEFAULT NULL,
  `zeroPlaceCount` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rooms_facilities`
--

CREATE TABLE `rooms_facilities` (
  `id` int NOT NULL,
  `room_id` int DEFAULT NULL,
  `facility_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rooms_photos`
--

CREATE TABLE `rooms_photos` (
  `id` int NOT NULL,
  `room_id` int DEFAULT NULL,
  `photo` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rooms_videos`
--

CREATE TABLE `rooms_videos` (
  `id` int NOT NULL,
  `room_id` int DEFAULT NULL,
  `youtube_code` varchar(50) DEFAULT NULL,
  `background` int DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section`
--

CREATE TABLE `section` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tariff_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `inner_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inner_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sections`
--

CREATE TABLE `sections` (
  `id` int NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `tariff_description` text,
  `picture` int DEFAULT NULL,
  `settings` json NOT NULL,
  `inner_title` varchar(255) DEFAULT NULL,
  `inner_description` text,
  `web_icon` int DEFAULT NULL,
  `web_background` int DEFAULT NULL,
  `mobile_icon` int DEFAULT NULL,
  `mobile_background` int DEFAULT NULL,
  `recommendation_sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

CREATE TABLE `sessions` (
  `id` int NOT NULL,
  `session` varchar(128) DEFAULT NULL,
  `expires` int DEFAULT NULL,
  `data` blob,
  `created` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `storage`
--

CREATE TABLE `storage` (
  `id` int NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `subdir` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tax_types`
--

CREATE TABLE `tax_types` (
  `id` int NOT NULL,
  `value` int DEFAULT NULL,
  `name` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `role` smallint UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_hash` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
  `sex` smallint UNSIGNED DEFAULT NULL,
  `status` smallint UNSIGNED NOT NULL,
  `reset_password_token_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_token_expired_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `firstName` varchar(128) DEFAULT NULL,
  `middleName` varchar(128) DEFAULT NULL,
  `lastName` varchar(128) DEFAULT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `balance` float DEFAULT NULL,
  `resetHash` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `mailing` tinyint(1) DEFAULT '0',
  `service_letters` tinyint(1) DEFAULT '0',
  `sex` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users_properties`
--

CREATE TABLE `users_properties` (
  `id` int NOT NULL,
  `uid` int DEFAULT NULL,
  `upd_subscr_queue` tinyint(1) DEFAULT NULL,
  `touragent` tinyint(1) DEFAULT NULL,
  `touragent_contract` int DEFAULT NULL,
  `was_confirmed` tinyint(1) DEFAULT NULL,
  `im_search` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_refresh_token`
--

CREATE TABLE `user_refresh_token` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `token_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_expired_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Индексы таблицы `actions_levels`
--
ALTER TABLE `actions_levels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `level_id` (`level_id`);

--
-- Индексы таблицы `actions_products`
--
ALTER TABLE `actions_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `actions_rate_plans`
--
ALTER TABLE `actions_rate_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `rate_plan_id` (`rate_plan_id`);

--
-- Индексы таблицы `action_groups`
--
ALTER TABLE `action_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `baskets`
--
ALTER TABLE `baskets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `basket_items`
--
ALTER TABLE `basket_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BA388B7A76ED395` (`user_id`);

--
-- Индексы таблицы `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F0FE25271AD5CDBF` (`cart_id`),
  ADD KEY `IDX_F0FE25274584665A` (`product_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `web_icon` (`web_icon`),
  ADD KEY `mobile_icon` (`mobile_icon`),
  ADD KEY `mobile_background` (`mobile_background`);

--
-- Индексы таблицы `categories_places`
--
ALTER TABLE `categories_places`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `place_id` (`place_id`);

--
-- Индексы таблицы `categories_sections`
--
ALTER TABLE `categories_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `section_id` (`section_id`);

--
-- Индексы таблицы `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `extra_place_types`
--
ALTER TABLE `extra_place_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_ADE885D5A85760C8` (`facility_group_id`),
  ADD KEY `IDX_ADE885D5659429DB` (`web_icon`),
  ADD KEY `FK_ADE885D587A63139` (`mobile_icon`);

--
-- Индексы таблицы `facility_groups`
--
ALTER TABLE `facility_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_5D592E0AB74DB4B9` (`web_icon`),
  ADD KEY `FK_5D592E0A87A63139` (`mobile_icon`);

--
-- Индексы таблицы `food_types`
--
ALTER TABLE `food_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `logo_svg` (`logo_svg`),
  ADD KEY `logo` (`logo`),
  ADD KEY `typeId` (`typeId`),
  ADD KEY `webBackground` (`webBackground`);

--
-- Индексы таблицы `hotels_facilities`
--
ALTER TABLE `hotels_facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `facility_id` (`facility_id`);

--
-- Индексы таблицы `hotels_photos`
--
ALTER TABLE `hotels_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `photo` (`photo`);

--
-- Индексы таблицы `hotels_videos`
--
ALTER TABLE `hotels_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `background` (`background`);

--
-- Индексы таблицы `hotel_age_range`
--
ALTER TABLE `hotel_age_range`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`);

--
-- Индексы таблицы `hotel_services`
--
ALTER TABLE `hotel_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `photo` (`photo`);

--
-- Индексы таблицы `hotel_service_price_values`
--
ALTER TABLE `hotel_service_price_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotelServiceId` (`hotelServiceId`);

--
-- Индексы таблицы `hotel_service_quota_values`
--
ALTER TABLE `hotel_service_quota_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotelServiceId` (`hotelServiceId`);

--
-- Индексы таблицы `hotel_types`
--
ALTER TABLE `hotel_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pay_system_id` (`pay_system_id`);

--
-- Индексы таблицы `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F5299398A76ED395` (`user_id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_52EA1F098D9F6D38` (`order_id`),
  ADD KEY `IDX_52EA1F094584665A` (`product_id`);

--
-- Индексы таблицы `packets`
--
ALTER TABLE `packets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foodTypeId` (`foodTypeId`),
  ADD KEY `webBackground` (`webBackground`);

--
-- Индексы таблицы `packets_rate_plans`
--
ALTER TABLE `packets_rate_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `packetId` (`packetId`),
  ADD KEY `ratePlanId` (`ratePlanId`);

--
-- Индексы таблицы `packet_items`
--
ALTER TABLE `packet_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photoId` (`photoId`),
  ADD KEY `placeId` (`placeId`),
  ADD KEY `packetId` (`packetId`);

--
-- Индексы таблицы `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6D28840D8D9F6D38` (`order_id`);

--
-- Индексы таблицы `pay_systems`
--
ALTER TABLE `pay_systems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `storage_id` (`storage_id`);

--
-- Индексы таблицы `periods`
--
ALTER TABLE `periods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `background` (`background`);

--
-- Индексы таблицы `places_blocks`
--
ALTER TABLE `places_blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place_id` (`place_id`),
  ADD KEY `photo` (`photo`),
  ADD KEY `video_background` (`video_background`);

--
-- Индексы таблицы `places_geo_points`
--
ALTER TABLE `places_geo_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place_id` (`place_id`);

--
-- Индексы таблицы `places_photos`
--
ALTER TABLE `places_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place_id` (`place_id`),
  ADD KEY `photo` (`photo`);

--
-- Индексы таблицы `places_sections`
--
ALTER TABLE `places_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place_id` (`place_id`),
  ADD KEY `section_id` (`section_id`);

--
-- Индексы таблицы `places_videos`
--
ALTER TABLE `places_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `background` (`background`),
  ADD KEY `place_id` (`place_id`);

--
-- Индексы таблицы `pps_groups`
--
ALTER TABLE `pps_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pps_periods`
--
ALTER TABLE `pps_periods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pps_prices`
--
ALTER TABLE `pps_prices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pps_products`
--
ALTER TABLE `pps_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D34A04ADD823E37A` (`section_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6B9459854584665A` (`product_id`);

--
-- Индексы таблицы `promocodes`
--
ALTER TABLE `promocodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_id` (`action_id`);

--
-- Индексы таблицы `quotas`
--
ALTER TABLE `quotas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Индексы таблицы `quotas_products`
--
ALTER TABLE `quotas_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quota_id` (`quota_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `quota_groups`
--
ALTER TABLE `quota_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `quota_values`
--
ALTER TABLE `quota_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quota_id` (`quota_id`);

--
-- Индексы таблицы `rate_plans`
--
ALTER TABLE `rate_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `groupId` (`groupId`),
  ADD KEY `foodTypeId` (`foodTypeId`),
  ADD KEY `taxTypeId` (`taxTypeId`);

--
-- Индексы таблицы `rate_plans_rooms`
--
ALTER TABLE `rate_plans_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratePlanId` (`ratePlanId`),
  ADD KEY `roomId` (`roomId`);

--
-- Индексы таблицы `rate_plan_groups`
--
ALTER TABLE `rate_plan_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `refresh_token`
--
ALTER TABLE `refresh_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `place_id` (`place_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`);

--
-- Индексы таблицы `rooms_facilities`
--
ALTER TABLE `rooms_facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `facility_id` (`facility_id`);

--
-- Индексы таблицы `rooms_photos`
--
ALTER TABLE `rooms_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `photo` (`photo`);

--
-- Индексы таблицы `rooms_videos`
--
ALTER TABLE `rooms_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `background` (`background`);

--
-- Индексы таблицы `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `web_icon` (`web_icon`),
  ADD KEY `web_background` (`web_background`),
  ADD KEY `mobile_icon` (`mobile_icon`),
  ADD KEY `mobile_background` (`mobile_background`);

--
-- Индексы таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `storage`
--
ALTER TABLE `storage`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tax_types`
--
ALTER TABLE `tax_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Индексы таблицы `users_properties`
--
ALTER TABLE `users_properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Индексы таблицы `user_refresh_token`
--
ALTER TABLE `user_refresh_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_29C18CC5A76ED395` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `actions_levels`
--
ALTER TABLE `actions_levels`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `actions_products`
--
ALTER TABLE `actions_products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `actions_rate_plans`
--
ALTER TABLE `actions_rate_plans`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `action_groups`
--
ALTER TABLE `action_groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `baskets`
--
ALTER TABLE `baskets`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `basket_items`
--
ALTER TABLE `basket_items`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `categories_places`
--
ALTER TABLE `categories_places`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `categories_sections`
--
ALTER TABLE `categories_sections`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `extra_place_types`
--
ALTER TABLE `extra_place_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `facility_groups`
--
ALTER TABLE `facility_groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `food_types`
--
ALTER TABLE `food_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotels_facilities`
--
ALTER TABLE `hotels_facilities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotels_photos`
--
ALTER TABLE `hotels_photos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotels_videos`
--
ALTER TABLE `hotels_videos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotel_age_range`
--
ALTER TABLE `hotel_age_range`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotel_services`
--
ALTER TABLE `hotel_services`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotel_service_price_values`
--
ALTER TABLE `hotel_service_price_values`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotel_service_quota_values`
--
ALTER TABLE `hotel_service_quota_values`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotel_types`
--
ALTER TABLE `hotel_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `packets`
--
ALTER TABLE `packets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `packets_rate_plans`
--
ALTER TABLE `packets_rate_plans`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `packet_items`
--
ALTER TABLE `packet_items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pay_systems`
--
ALTER TABLE `pay_systems`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `periods`
--
ALTER TABLE `periods`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `places`
--
ALTER TABLE `places`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `places_blocks`
--
ALTER TABLE `places_blocks`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `places_geo_points`
--
ALTER TABLE `places_geo_points`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `places_photos`
--
ALTER TABLE `places_photos`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `places_sections`
--
ALTER TABLE `places_sections`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `places_videos`
--
ALTER TABLE `places_videos`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pps_prices`
--
ALTER TABLE `pps_prices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `promocodes`
--
ALTER TABLE `promocodes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `quotas`
--
ALTER TABLE `quotas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `quotas_products`
--
ALTER TABLE `quotas_products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `quota_groups`
--
ALTER TABLE `quota_groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `quota_values`
--
ALTER TABLE `quota_values`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rate_plans`
--
ALTER TABLE `rate_plans`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rate_plans_rooms`
--
ALTER TABLE `rate_plans_rooms`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rate_plan_groups`
--
ALTER TABLE `rate_plan_groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `refresh_token`
--
ALTER TABLE `refresh_token`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rooms_facilities`
--
ALTER TABLE `rooms_facilities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rooms_photos`
--
ALTER TABLE `rooms_photos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rooms_videos`
--
ALTER TABLE `rooms_videos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `storage`
--
ALTER TABLE `storage`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tax_types`
--
ALTER TABLE `tax_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users_properties`
--
ALTER TABLE `users_properties`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `action_groups` (`id`);

--
-- Ограничения внешнего ключа таблицы `actions_levels`
--
ALTER TABLE `actions_levels`
  ADD CONSTRAINT `actions_levels_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`),
  ADD CONSTRAINT `actions_levels_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`);

--
-- Ограничения внешнего ключа таблицы `actions_products`
--
ALTER TABLE `actions_products`
  ADD CONSTRAINT `actions_products_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`),
  ADD CONSTRAINT `actions_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Ограничения внешнего ключа таблицы `actions_rate_plans`
--
ALTER TABLE `actions_rate_plans`
  ADD CONSTRAINT `actions_rate_plans_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`),
  ADD CONSTRAINT `actions_rate_plans_ibfk_2` FOREIGN KEY (`rate_plan_id`) REFERENCES `rate_plans` (`id`);

--
-- Ограничения внешнего ключа таблицы `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `FK_BA388B7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `cart_item`
--
ALTER TABLE `cart_item`
  ADD CONSTRAINT `FK_F0FE25271AD5CDBF` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  ADD CONSTRAINT `FK_F0FE25274584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`web_icon`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`mobile_icon`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `categories_ibfk_3` FOREIGN KEY (`mobile_background`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `categories_places`
--
ALTER TABLE `categories_places`
  ADD CONSTRAINT `categories_places_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `categories_places_ibfk_2` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`);

--
-- Ограничения внешнего ключа таблицы `categories_sections`
--
ALTER TABLE `categories_sections`
  ADD CONSTRAINT `categories_sections_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `categories_sections_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`);

--
-- Ограничения внешнего ключа таблицы `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `FK_ADE885D5659429DB` FOREIGN KEY (`web_icon`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `FK_ADE885D587A63139` FOREIGN KEY (`mobile_icon`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `FK_ADE885D5A85760C8` FOREIGN KEY (`facility_group_id`) REFERENCES `facility_groups` (`id`);

--
-- Ограничения внешнего ключа таблицы `facility_groups`
--
ALTER TABLE `facility_groups`
  ADD CONSTRAINT `FK_5D592E0A87A63139` FOREIGN KEY (`mobile_icon`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `FK_5D592E0AB74DB4B9` FOREIGN KEY (`web_icon`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotels`
--
ALTER TABLE `hotels`
  ADD CONSTRAINT `hotels_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `hotels_ibfk_2` FOREIGN KEY (`logo_svg`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `hotels_ibfk_3` FOREIGN KEY (`logo`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `hotels_ibfk_4` FOREIGN KEY (`typeId`) REFERENCES `hotel_types` (`id`),
  ADD CONSTRAINT `hotels_ibfk_5` FOREIGN KEY (`webBackground`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotels_facilities`
--
ALTER TABLE `hotels_facilities`
  ADD CONSTRAINT `hotels_facilities_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  ADD CONSTRAINT `hotels_facilities_ibfk_2` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotels_photos`
--
ALTER TABLE `hotels_photos`
  ADD CONSTRAINT `hotels_photos_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  ADD CONSTRAINT `hotels_photos_ibfk_2` FOREIGN KEY (`photo`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotels_videos`
--
ALTER TABLE `hotels_videos`
  ADD CONSTRAINT `hotels_videos_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  ADD CONSTRAINT `hotels_videos_ibfk_2` FOREIGN KEY (`background`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotel_age_range`
--
ALTER TABLE `hotel_age_range`
  ADD CONSTRAINT `hotel_age_range_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotel_services`
--
ALTER TABLE `hotel_services`
  ADD CONSTRAINT `hotel_services_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  ADD CONSTRAINT `hotel_services_ibfk_2` FOREIGN KEY (`photo`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotel_service_price_values`
--
ALTER TABLE `hotel_service_price_values`
  ADD CONSTRAINT `hotel_service_price_values_ibfk_1` FOREIGN KEY (`hotelServiceId`) REFERENCES `hotel_services` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotel_service_quota_values`
--
ALTER TABLE `hotel_service_quota_values`
  ADD CONSTRAINT `hotel_service_quota_values_ibfk_1` FOREIGN KEY (`hotelServiceId`) REFERENCES `hotel_services` (`id`);

--
-- Ограничения внешнего ключа таблицы `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`pay_system_id`) REFERENCES `pay_systems` (`id`);

--
-- Ограничения внешнего ключа таблицы `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_F5299398A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `FK_52EA1F094584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_52EA1F098D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);

--
-- Ограничения внешнего ключа таблицы `packets`
--
ALTER TABLE `packets`
  ADD CONSTRAINT `packets_ibfk_1` FOREIGN KEY (`foodTypeId`) REFERENCES `food_types` (`id`),
  ADD CONSTRAINT `packets_ibfk_2` FOREIGN KEY (`webBackground`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `packets_rate_plans`
--
ALTER TABLE `packets_rate_plans`
  ADD CONSTRAINT `packets_rate_plans_ibfk_1` FOREIGN KEY (`packetId`) REFERENCES `packets` (`id`),
  ADD CONSTRAINT `packets_rate_plans_ibfk_2` FOREIGN KEY (`ratePlanId`) REFERENCES `rate_plans` (`id`);

--
-- Ограничения внешнего ключа таблицы `packet_items`
--
ALTER TABLE `packet_items`
  ADD CONSTRAINT `packet_items_ibfk_1` FOREIGN KEY (`photoId`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `packet_items_ibfk_2` FOREIGN KEY (`placeId`) REFERENCES `places` (`id`),
  ADD CONSTRAINT `packet_items_ibfk_3` FOREIGN KEY (`packetId`) REFERENCES `packets` (`id`);

--
-- Ограничения внешнего ключа таблицы `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `FK_6D28840D8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);

--
-- Ограничения внешнего ключа таблицы `pay_systems`
--
ALTER TABLE `pay_systems`
  ADD CONSTRAINT `pay_systems_ibfk_1` FOREIGN KEY (`storage_id`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `places`
--
ALTER TABLE `places`
  ADD CONSTRAINT `places_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `places_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  ADD CONSTRAINT `places_ibfk_3` FOREIGN KEY (`background`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `places_blocks`
--
ALTER TABLE `places_blocks`
  ADD CONSTRAINT `places_blocks_ibfk_1` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`),
  ADD CONSTRAINT `places_blocks_ibfk_2` FOREIGN KEY (`photo`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `places_blocks_ibfk_3` FOREIGN KEY (`video_background`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `places_geo_points`
--
ALTER TABLE `places_geo_points`
  ADD CONSTRAINT `places_geo_points_ibfk_1` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`);

--
-- Ограничения внешнего ключа таблицы `places_photos`
--
ALTER TABLE `places_photos`
  ADD CONSTRAINT `places_photos_ibfk_1` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`),
  ADD CONSTRAINT `places_photos_ibfk_2` FOREIGN KEY (`photo`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `places_sections`
--
ALTER TABLE `places_sections`
  ADD CONSTRAINT `places_sections_ibfk_1` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`),
  ADD CONSTRAINT `places_sections_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`);

--
-- Ограничения внешнего ключа таблицы `places_videos`
--
ALTER TABLE `places_videos`
  ADD CONSTRAINT `places_videos_ibfk_1` FOREIGN KEY (`background`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `places_videos_ibfk_2` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`);

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04ADD823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`);

--
-- Ограничения внешнего ключа таблицы `product_price`
--
ALTER TABLE `product_price`
  ADD CONSTRAINT `FK_6B9459854584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Ограничения внешнего ключа таблицы `promocodes`
--
ALTER TABLE `promocodes`
  ADD CONSTRAINT `promocodes_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`);

--
-- Ограничения внешнего ключа таблицы `quotas`
--
ALTER TABLE `quotas`
  ADD CONSTRAINT `quotas_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `quota_groups` (`id`);

--
-- Ограничения внешнего ключа таблицы `quotas_products`
--
ALTER TABLE `quotas_products`
  ADD CONSTRAINT `quotas_products_ibfk_1` FOREIGN KEY (`quota_id`) REFERENCES `quotas` (`id`),
  ADD CONSTRAINT `quotas_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Ограничения внешнего ключа таблицы `quota_values`
--
ALTER TABLE `quota_values`
  ADD CONSTRAINT `quota_values_ibfk_1` FOREIGN KEY (`quota_id`) REFERENCES `quotas` (`id`);

--
-- Ограничения внешнего ключа таблицы `rate_plans`
--
ALTER TABLE `rate_plans`
  ADD CONSTRAINT `rate_plans_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  ADD CONSTRAINT `rate_plans_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `rate_plan_groups` (`id`),
  ADD CONSTRAINT `rate_plans_ibfk_3` FOREIGN KEY (`foodTypeId`) REFERENCES `food_types` (`id`),
  ADD CONSTRAINT `rate_plans_ibfk_4` FOREIGN KEY (`taxTypeId`) REFERENCES `tax_types` (`id`);

--
-- Ограничения внешнего ключа таблицы `rate_plans_rooms`
--
ALTER TABLE `rate_plans_rooms`
  ADD CONSTRAINT `rate_plans_rooms_ibfk_1` FOREIGN KEY (`ratePlanId`) REFERENCES `rate_plans` (`id`),
  ADD CONSTRAINT `rate_plans_rooms_ibfk_2` FOREIGN KEY (`roomId`) REFERENCES `rooms` (`id`);

--
-- Ограничения внешнего ключа таблицы `refresh_token`
--
ALTER TABLE `refresh_token`
  ADD CONSTRAINT `refresh_token_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`);

--
-- Ограничения внешнего ключа таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`);

--
-- Ограничения внешнего ключа таблицы `rooms_facilities`
--
ALTER TABLE `rooms_facilities`
  ADD CONSTRAINT `rooms_facilities_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `rooms_facilities_ibfk_2` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`);

--
-- Ограничения внешнего ключа таблицы `rooms_photos`
--
ALTER TABLE `rooms_photos`
  ADD CONSTRAINT `rooms_photos_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `rooms_photos_ibfk_2` FOREIGN KEY (`photo`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `rooms_videos`
--
ALTER TABLE `rooms_videos`
  ADD CONSTRAINT `rooms_videos_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `rooms_videos_ibfk_2` FOREIGN KEY (`background`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`web_icon`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `sections_ibfk_2` FOREIGN KEY (`web_background`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `sections_ibfk_3` FOREIGN KEY (`mobile_icon`) REFERENCES `storage` (`id`),
  ADD CONSTRAINT `sections_ibfk_4` FOREIGN KEY (`mobile_background`) REFERENCES `storage` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `users_properties`
--
ALTER TABLE `users_properties`
  ADD CONSTRAINT `users_properties_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_refresh_token`
--
ALTER TABLE `user_refresh_token`
  ADD CONSTRAINT `FK_29C18CC5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
