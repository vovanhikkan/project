FROM php:7.4-fpm-alpine

RUN apk add icu-dev
RUN docker-php-ext-install mysqli pdo_mysql intl

RUN set -ex && apk --no-cache add libxml2-dev

RUN docker-php-ext-install soap
RUN docker-php-ext-install xml
RUN docker-php-ext-install xmlrpc

RUN apk upgrade --update-cache --available && \
    apk add openssl && \
    apk add --no-cache gmp gmp-dev && \
    docker-php-ext-install gmp && \
    rm -rf /var/cache/apk/*

RUN apk --update --no-cache add \
    libgcc \
    libstdc++ \
    musl \
    qt5-qtbase \
    qt5-qtbase-x11 \
    qt5-qtsvg \
    qt5-qtwebkit \
    ttf-freefont \
    ttf-dejavu \
    ttf-droid \
    ttf-liberation \
    ttf-ubuntu-font-family \
    fontconfig

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.8/main' >> /etc/apk/repositories && \
    apk add --no-cache libcrypto1.0 libssl1.0

COPY wkhtmltopdf/wkhtmltopdf /bin/wkhtmltopdf

RUN set -ex \
    && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool \
    && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
    && pecl install imagick-3.4.3 \
    && docker-php-ext-enable imagick \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && apk add --no-cache --virtual .imagick-runtime-deps imagemagick \
    && apk del .phpize-deps

WORKDIR /app
