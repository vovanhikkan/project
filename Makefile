####################
# LOCAL
####################

uid := $(shell id -u)
gid := $(shell id -g)

up: docker-up
down: docker-down
restart: docker-down docker-up

init: docker-down-clear docker-build docker-up

docker-up:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml up -d

docker-down:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml down --remove-orphans

docker-down-clear:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml down -v --remove-orphans

docker-pull:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml pull

docker-build:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml build

docker-ps:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml ps

docker-top:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml top

app-init: app-composer-install

app-composer-install:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml exec php-fpm composer install

#
# CLI
#
cli:
	docker-compose -f ./docker-compose-local.yml exec php-fpm /bin/sh

cli-add-migration:
	docker-compose -f ./docker-compose-local.yml exec php-fpm php bin/console.php migrations:generate

cli-init: cli-composer-install

cli-composer-install:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml exec php-cli composer install

cli-migrate:
	export uid=$(uid) gid=$(gid); \
	docker-compose -f ./docker-compose-local.yml exec php-cli php bin/console.php --no-interaction doctrine:migrations:migrate;