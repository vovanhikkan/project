<?php

declare(strict_types=1);

use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\Migration\ConfigurationArray;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\Tools\Console\Command as DoctrineMigrationCommands;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application;
use Doctrine\ORM\Tools\Console\Command as DoctrineORMCommands;
use Doctrine\ORM\Tools\Console\Command\ClearCache as ClearCache;
use App\UI\Console;

define('APP_CONSOLE', true);

/** @var ContainerInterface $container */
$container = require_once __DIR__. '/../config/bootstrap.php';

$cli = new Application('Application Console');

/** @var EntityManagerInterface $entityManager */
$entityManager = $container->get(EntityManagerInterface::class);

$connection = $entityManager->getConnection();

$config = new ConfigurationArray([
    'migrations_paths' => [
        'App\Data\Migrations' => __DIR__ . '/../src/Data/Migrations'
    ]
]);

$dependencyFactory = DependencyFactory::fromEntityManager(
    $config,
    new ExistingEntityManager($entityManager)
);

$cli->addCommands(array(
    new DoctrineORMCommands\ValidateSchemaCommand(),
    new DoctrineORMCommands\SchemaTool\UpdateCommand(),
    new DoctrineORMCommands\ConvertMappingCommand(),
    new DoctrineORMCommands\GenerateRepositoriesCommand(),
    new DoctrineORMCommands\GenerateEntitiesCommand(),
    new ClearCache\MetadataCommand(),
    new ClearCache\ResultCommand(),
    new ClearCache\CollectionRegionCommand(),
    new ClearCache\QueryCommand(),
    new ClearCache\QueryRegionCommand(),
    new ClearCache\EntityRegionCommand(),
    new DoctrineMigrationCommands\DiffCommand($dependencyFactory),
    new DoctrineMigrationCommands\ExecuteCommand($dependencyFactory),
    new DoctrineMigrationCommands\GenerateCommand($dependencyFactory),
    new DoctrineMigrationCommands\LatestCommand($dependencyFactory),
    new DoctrineMigrationCommands\ListCommand($dependencyFactory),
    new DoctrineMigrationCommands\MigrateCommand($dependencyFactory),
    new DoctrineMigrationCommands\RollupCommand($dependencyFactory),
    new DoctrineMigrationCommands\StatusCommand($dependencyFactory),
    new DoctrineMigrationCommands\VersionCommand($dependencyFactory),
));

$commands = [
    Console\CardTypeSyncCommand::class,
    Console\MigrateCommand::class,
    Console\PpsImportCommand::class
];

if (getenv('APP_ENV') === 'dev') {
    $commands = array_merge($commands, [
        Console\CrudGenerator\CrudGenerateCommand::class,
        Console\CrudGenerator\AbstractActionGenerateCommand::class,
        Console\CrudGenerator\ModelGenerateCommand::class,
        Console\CrudGenerator\CreateActionGenerateCommand::class,
        Console\CrudGenerator\IndexActionGenerateCommand::class,
        Console\CrudGenerator\UpdateActionGenerateCommand::class,
        Console\CrudGenerator\DeleteActionGenerateCommand::class,
        Console\CrudGenerator\ViewActionGenerateCommand::class,
        Console\CrudGenerator\RepositoryGenerateCommand::class,
        Console\CrudGenerator\CreateCommandGenerateCommand::class,
        Console\CrudGenerator\CreateHandlerGenerateCommand::class,
        Console\CrudGenerator\CreateServiceGenerateCommand::class,
        Console\CrudGenerator\UpdateCommandGenerateCommand::class,
        Console\CrudGenerator\UpdateHandlerGenerateCommand::class,
        Console\CrudGenerator\UpdateServiceGenerateCommand::class,
        Console\CrudGenerator\DeleteCommandGenerateCommand::class,
        Console\CrudGenerator\DeleteHandlerGenerateCommand::class,
        Console\CrudGenerator\DeleteServiceGenerateCommand::class,
    ]);
}

foreach ($commands as $command) {
    $cli->add($container->get($command));
}

$exitCode = $cli->run();

exit($exitCode);
