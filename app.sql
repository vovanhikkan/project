-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Apr 15, 2021 at 07:23 AM
-- Server version: 8.0.22
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app`
--
CREATE DATABASE IF NOT EXISTS `app` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `app`;

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE `action` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `description` json NOT NULL,
  `shown_from` date NOT NULL,
  `shown_to` date NOT NULL,
  `discount_absolute_value` int DEFAULT NULL,
  `discount_percentage_value` int DEFAULT NULL,
  `cashback_point_absolute_value` int DEFAULT NULL,
  `cashback_point_percentage_value` int DEFAULT NULL,
  `min_day_count` int DEFAULT NULL,
  `max_day_count` int DEFAULT NULL,
  `booking_from` date DEFAULT NULL,
  `booking_to` date DEFAULT NULL,
  `min_product_quantity` int DEFAULT NULL,
  `max_product_quantity` int DEFAULT NULL,
  `activation_date_from` date DEFAULT NULL,
  `activation_date_to` date DEFAULT NULL,
  `has_levels` smallint UNSIGNED NOT NULL,
  `has_promocodes` smallint UNSIGNED NOT NULL,
  `promocode_lifetime` bigint DEFAULT NULL,
  `promocode_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` smallint UNSIGNED NOT NULL,
  `total_count` int DEFAULT NULL,
  `activated_count` int DEFAULT NULL,
  `sort` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `actions_rate_plans`
--

CREATE TABLE `actions_rate_plans` (
  `id` int NOT NULL,
  `action_id` int DEFAULT NULL,
  `rate_plan_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_group`
--

CREATE TABLE `action_group` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `sort` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_level`
--

CREATE TABLE `action_level` (
  `action_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_product`
--

CREATE TABLE `action_product` (
  `action_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bed_type`
--

CREATE TABLE `bed_type` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `web_icon` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_icon` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bed_type`
--

INSERT INTO `bed_type` (`id`, `name`, `web_icon`, `mobile_icon`) VALUES
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"односпальная кровать\"}', 'd298bd24-a164-481b-aaef-cfc2c277939c', 'a69e08e9-9463-412f-9b9c-7bcf1ee5ed60'),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"двуспальная кровать\"}', 'a69e08e9-9463-412f-9b9c-7bcf1ee5ed60', 'a69e08e9-9463-412f-9b9c-7bcf1ee5ed60'),
('6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"тип кровати 3\"}', 'ab89a4a8-5c96-4589-9161-735f6c0f687e', 'ab89a4a8-5c96-4589-9161-735f6c0f687e'),
('8658b5b9-1a6f-49ce-ad37-12278ed1095c', '{\"en\": \"\", \"ru\": \"Best \"}', NULL, NULL),
('a31e05c9-af8a-486f-ad4d-3ca31ff52aa7', '{\"en\": \"\", \"ru\": \"33\"}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `is_active` tinyint UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `is_active`) VALUES
('18924058-1e6e-4bbc-a785-e5505515f74c', '{\"en\": \"\", \"ru\": \"AZIMUT\"}', 1),
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"Ski Inn\"}', 1),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"AYS\"}', 1),
('6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"Golden Tulip\"}', 1),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"Radisson\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `budget`
--

CREATE TABLE `budget` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `budget_from` int UNSIGNED NOT NULL,
  `budget_to` int UNSIGNED NOT NULL,
  `sort` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `budget`
--

INSERT INTO `budget` (`id`, `name`, `budget_from`, `budget_to`, `sort`) VALUES
('2452d813-c3fc-4c95-b89d-a1dd2dacc342', '{\"ru\": \"бюджет5\"}', 500, 5500, 1),
('35199f84-5cae-47c3-9806-4a3baa2b103c', '{\"ru\": \"бюджет\"}', 1000, 5000, 2),
('5be68cfe-4bd6-4070-b183-e6f8600fdf4a', '{\"ru\": \"бюджет2\"}', 7000, 13000, 3),
('8352d813-c3fc-4c95-b89d-a1dd2dacc132', '{\"ru\": \"бюджет1\"}', 5500, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `bundle`
--

CREATE TABLE `bundle` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `description` json DEFAULT NULL,
  `day_count` int UNSIGNED DEFAULT NULL,
  `min_price` int UNSIGNED DEFAULT NULL,
  `profit` int UNSIGNED DEFAULT NULL,
  `food_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_with_accommodation` tinyint UNSIGNED NOT NULL,
  `sort` int UNSIGNED DEFAULT NULL,
  `web_background` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint UNSIGNED DEFAULT NULL,
  `recommendation_sort` int UNSIGNED DEFAULT NULL,
  `width_type` smallint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundle`
--

INSERT INTO `bundle` (`id`, `code`, `name`, `description`, `day_count`, `min_price`, `profit`, `food_type_id`, `is_with_accommodation`, `sort`, `web_background`, `is_active`, `recommendation_sort`, `width_type`) VALUES
('0c4d5554-36e7-4a4e-a1e3-19eb41a715b3', 'katayus-na-bw2021-bez-pitaniya', '{\"en\": \"\", \"ru\": \"Катаемся на BoogelWoogel 2021, проживание без завтрака\"}', '{\"en\": \"\", \"ru\": \"Тур с проживанием и ски-пассом. Высокогорный карнавал BoogelWoogel возвращается! Бронируйте на 2, 3 или 4 ночи\"}', 5, 3200, 800, 'b15b4686-aa1b-4606-97ca-6f241554bd82', 1, 4, '10f2cdc1-afaa-4f93-a188-b4b9b2dfaff8', 1, 4, 100),
('1a707729-7a6e-48d0-a4ce-5841f8a7fb29', 'Skiing-at-spring', '{\"en\": \"Skiing at spring\", \"ru\": \"Катайся весной, тур с завтраком\"}', '{\"en\": \"Yes, in April there is still a rolling season on Rosa Khutor, and the prices for vacations are several times lower.\\nIn this variant of the tour - with breakfast - you can choose one of 17 hotels.\", \"ru\": \"Да, в апреле на Роза Хутор ещё катальный сезон, и цены на отдых в разы ниже.\\nВ данном варианте тура – с завтраком – вы можете выбрать один из 17 отелей.\"}', 4, 1000, 500, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 7, '5e79ea11-644b-45d2-9075-cbe04ab1efad', 1, NULL, 300),
('1d3219eb-5df3-4110-94e7-00d745570172', 'superBundle23', '{\"en\": \"superBundle\", \"ru\": \"superBundle123123213123123\"}', '{\"en\": \"superBundle\", \"ru\": \"superBundle\"}', 32123123, 123, 31231, NULL, 1, 67, NULL, 1, 5, NULL),
('23be2c1d-3a56-45c9-9540-57763103f912', 'code_test13', '{\"en\": \"\", \"ru\": \"пакет очень клас323\"}', '{\"en\": \"\", \"ru\": \"описание п3123\"}', 5, 18345, 284, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 15, 'ab89a4a8-5c96-4589-9161-735f6c0f687e', NULL, NULL, 100),
('3392181e-8977-442e-89b4-8b472dd4b696', 'boogel-woogel-2021-zavtrak', '{\"en\": \"\", \"ru\": \"Тур на BoogelWoogel 2021, проживание с завтраком\"}', '{\"en\": \"\", \"ru\": \"Высокогорный карнавал BoogelWoogel возвращается! Успейте забронировать на 2, 3 или 4 ночи! Ски-пасс карнавальный включен в стоимость.\"}', 4, 1500, 600, '6582b0da-b8d0-4618-a763-864358706ce1', 1, 2, 'c09ceec6-fca6-45e4-9250-e62cc3d3c09e', 1, 2, 100),
('33be2c1d-3a56-45c9-9540-57763103f912', 'code_test 4', '{\"en\": \"\", \"ru\": \"пакет очень классный\"}', '{\"en\": \"\", \"ru\": \"описание пакет 4\"}', 5, 18345, 284, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 17, 'ab89a4a8-5c96-4589-9161-735f6c0f687e', NULL, NULL, 100),
('33be2c7d-3a56-45c9-9540-57763103f912', 'code_test 2', '{\"ru\": \"пакет 1\"}', '{\"en\": \"\", \"ru\": \"Высокогорный карнавал BoogelWoogel возвращается! Успейте забронировать на 2, 3 или 4 ночи! Ски-пасс карнавальный включен в стоимость.\"}', 3, 1000, 100, '33be2c7d-3a56-45c9-9540-57763103f912', 1, 125, 'd298bd24-a164-481b-aaef-cfc2c277939c', 1, 3, 200),
('5e03062e-f354-46a8-99e4-15fa7b913b05', 'code_test 1', '{\"ru\": \"пакет 2\"}', '{\"en\": \"\", \"ru\": \"Высокогорный карнавал BoogelWoogel возвращается! Успейте забронировать на 2, 3 или 4 ночи! Ски-пасс карнавальный включен в стоимость.\"}', 5, 2000, 150, '6582b0da-b8d0-4618-a763-864358706ce1', 1, 30, 'd298bd24-a164-481b-aaef-cfc2c277939c', 1, 2, NULL),
('695e4b3d-a1b6-4613-8d1e-0c5c27a06277', 'katayus-na-bw2021-zavtrak1', '{\"en\": \"\", \"ru\": \"Катаемся на BoogelWoogel 2021, проживание с завтраком\"}', '{\"en\": \"\", \"ru\": \"Тур с проживанием и ски-пассом. Высокогорный карнавал BoogelWoogel возвращается! Бронируйте на 2, 3 или 4 ночи\"}', 6, 2500, 1000, '18924058-1e6e-4bbc-a785-e5505515f74c', 0, 3, 'ad5c612d-ada9-438c-a29d-3936251fe85b', 1, 3, 100),
('6bcca44d-e0d7-4cb4-8ffd-523c9979b17a', '4234fdsgerg23423', '{\"en\": \"2423423\", \"ru\": \"fdsf\"}', '{\"en\": \"34234\", \"ru\": \"4234234234\"}', 2342, 12312, 3123, '18924058-1e6e-4bbc-a785-e5505515f74c', 1, 20, NULL, 1, NULL, NULL),
('70562620-54e3-4994-abb4-52deab9722f3', 'sk_spr1', '{\"en\": \"Skiing spring breakfast\", \"ru\": \"Катайся весной, тур с завтраком и ужином\"}', '{\"en\": \"Yes, in April there is still a rolling season on Rosa Khutor, and the prices for vacations are several times lower.\\nIn this variant of the tour - with breakfast and dinner - you can choose one of 4 hotels\", \"ru\": \"Да, в апреле на Роза Хутор ещё катальный сезон, и цены на отдых в разы ниже.\\nВ данном варианте тура – с завтраком и ужином – вы можете выбрать один из 4 отелей\"}', 5, 2000, 500, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 9, NULL, 1, NULL, NULL),
('83068a33-115e-4077-a774-b1c3c44f471a', 'sk_spring_fin', '{\"en\": \"Skiing spring breakfast\", \"ru\": \"Катайся весной, тур с завтраком и ужином\"}', '{\"en\": \"Yes, in April there is still a rolling season on Rosa Khutor, and the prices for vacations are several times lower.\\nIn this variant of the tour - with breakfast and dinner - you can choose one of 4 hotels\", \"ru\": \"Да, в апреле на Роза Хутор ещё катальный сезон, и цены на отдых в разы ниже.\\nВ данном варианте тура – с завтраком и ужином – вы можете выбрать один из 4 отелей\"}', 5, 2000, 500, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 10, NULL, 1, NULL, NULL),
('892c522d-3fc5-4339-8d83-358c2c79a7de', 'sk_spring_fin_1', '{\"en\": \"Skiing spring breakfast\", \"ru\": \"Катайся весной, тур с завтраком и ужином\"}', '{\"en\": \"Yes, in April there is still a rolling season on Rosa Khutor, and the prices for vacations are several times lower.\\nIn this variant of the tour - with breakfast and dinner - you can choose one of 4 hotels\", \"ru\": \"Да, в апреле на Роза Хутор ещё катальный сезон, и цены на отдых в разы ниже.\\nВ данном варианте тура – с завтраком и ужином – вы можете выбрать один из 4 отелей\"}', 5, 2000, 500, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 8, '0a726ba5-6c40-4e53-9fa9-cc6381d274cb', 1, 4, NULL),
('9e31cae0-d76e-4d94-9359-1a5f22712ed6', 'fffffffff4345', '{\"en\": \"fffffffff4345\", \"ru\": \"fffffffff4345\"}', '{\"en\": \"fffffffff4345\", \"ru\": \"fffffffff4345\"}', 4345, 4345, 4345, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 19, NULL, 1, NULL, NULL),
('a2a12769-6092-416e-aabd-4300bd53ef61', 'sk_spring_21', '{\"en\": \"Skiing spring breakfast\", \"ru\": \"Катайся весной, тур с завтраком и ужином\"}', '{\"en\": \"Yes, in April there is still a rolling season on Rosa Khutor, and the prices for vacations are several times lower.\\nIn this variant of the tour - with breakfast and dinner - you can choose one of 4 hotels\", \"ru\": \"Да, в апреле на Роза Хутор ещё катальный сезон, и цены на отдых в разы ниже.\\nВ данном варианте тура – с завтраком и ужином – вы можете выбрать один из 4 отелей\"}', 5, 2000, 500, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 11, NULL, 1, 6, NULL),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'code_test 3', '{\"ru\": \"пакет 3\"}', '{\"ru\": \"описание пакет 3\"}', 9, 5999, 187, 'b15b4686-aa1b-4606-97ca-6f241554bd82', 1, 10, 'a69e08e9-9463-412f-9b9c-7bcf1ee5ed60', NULL, NULL, 100),
('c1e56949-7536-4f8c-ad13-896e31b38f2f', 'boogel-woogel-2021-bez-pitaniya4', '{\"en\": \"\", \"ru\": \"Тур на BoogelWoogel 2021, проживание без завтрака\"}', '{\"en\": \"\", \"ru\": \"Высокогорный карнавал BoogelWoogel возвращается! Успейте забронировать на 2, 3 или 4 ночи! Ски-пасс карнавальный включен в стоимость.\"}', 3, 2000, 700, '33be2c1d-3a56-45c9-9540-57763103f912', 0, 1, 'd80a6ead-02f5-44a3-b250-25494542c77f', 1, 1, 400),
('caf9cfd6-98c5-4568-bcdc-58967352f4a0', 'dsfgrftgertert', '{\"en\": \"44444444444\", \"ru\": \"4444\"}', '{\"en\": \"\", \"ru\": \"fgdgdf\"}', 123123, 123123, 12323, '18924058-1e6e-4bbc-a785-e5505515f74c', 1, 13, NULL, 1, NULL, NULL),
('cb8483c8-3f79-4ae6-9585-458612a9af50', '123', '{\"en\": \"bundler\", \"ru\": \"bundler\"}', '{\"en\": \"bundler\", \"ru\": \"bundler\"}', 123, 3, 123, NULL, 1, 39, NULL, 1, NULL, NULL),
('cb8483c8-3f79-4ae6-9585-458612a9af51', 'dsfdsf', '{\"en\": \"\", \"ru\": \"bundler\"}', '{\"en\": \"\", \"ru\": \"bundler\"}', 123, 3, 123, '33be2c1d-3a56-45c9-9540-57763103f912', 1, 37, '8cb5fb96-4875-4396-840d-e488905733d9', 1, NULL, NULL),
('d2bd8cd5-6f02-4287-9e2f-e4f02592013a', 'rosa-run-2021-zavtrak', '{\"en\": \"\", \"ru\": \"Rosa Run 2021, тур с завтраком\"}', '{\"en\": \"\", \"ru\": \"Тур с проживанием и ски-пассом. Высокогорный карнавал BoogelWoogel возвращается! Бронируйте на 2, 3 или 4 ночи\"}', 3, 2200, 500, '33be2c7d-3a56-45c9-9540-57763103f912', 1, 5, '3d255bcd-4a64-4839-abc6-0c0be33aed1b', 1, 5, 100),
('d6ee1fcc-4c7b-4c1c-9f5f-ee22c40ea564', 'BoogelWoogel', '{\"en\": \"Tour on BoogelWoogel 2021, accommodation without breakfast\", \"ru\": \"Тур на BoogelWoogel 2021, проживание без завтрака\"}', '{\"en\": \"Yes, in April there is still a rolling season on Rosa Khutor, and the prices for vacations are several times lower.\\nIn this variant of the tour - with breakfast - you can choose one of 17 hotels.\", \"ru\": \"Да, в апреле на Роза Хутор ещё катальный сезон, и цены на отдых в разы ниже.\\nВ данном варианте тура – с завтраком – вы можете выбрать один из 17 отелей.\"}', 6, 4000, 700, '6582b0da-b8d0-4618-a763-864358706ce1', 1, 35, 'aa6c2f5c-eba0-4e75-8a1b-70b8aabfafd8', 1, 1, 400),
('f40d73a2-9f06-4108-9ad0-74a68ccf154f', 'ewrweqc1', '{\"en\": \"ewrtw\", \"ru\": \"уеяыаы\"}', '{\"en\": \"sdfsdf\", \"ru\": \"gfsdf\"}', 34, 324, 4324, '18924058-1e6e-4bbc-a785-e5505515f74c', 1, 18, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bundle_bundle_item`
--

CREATE TABLE `bundle_bundle_item` (
  `bundle_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bundle_item_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundle_bundle_item`
--

INSERT INTO `bundle_bundle_item` (`bundle_id`, `bundle_item_id`, `sort`) VALUES
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', 1),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', 1),
('5e03062e-f354-46a8-99e4-15fa7b913b05', '5e03062e-f354-46a8-99e4-15fa7b913b05', 1),
('6bcca44d-e0d7-4cb4-8ffd-523c9979b17a', '16a0b380-440a-47a9-a5f5-a85117ca022c', NULL),
('6bcca44d-e0d7-4cb4-8ffd-523c9979b17a', '2ef23eac-f3f5-494a-b0ef-a88203e25f7e', NULL),
('6bcca44d-e0d7-4cb4-8ffd-523c9979b17a', '3291405e-4870-40f3-b448-37461075cf43', NULL),
('9e31cae0-d76e-4d94-9359-1a5f22712ed6', '16a0b380-440a-47a9-a5f5-a85117ca022c', NULL),
('9e31cae0-d76e-4d94-9359-1a5f22712ed6', '2ef23eac-f3f5-494a-b0ef-a88203e25f7e', NULL),
('9e31cae0-d76e-4d94-9359-1a5f22712ed6', '3291405e-4870-40f3-b448-37461075cf43', NULL),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '5e03062e-f354-46a8-99e4-15fa7b913b05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bundle_item`
--

CREATE TABLE `bundle_item` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `sub_name` json NOT NULL,
  `description` json DEFAULT NULL,
  `sort` int UNSIGNED DEFAULT NULL,
  `is_active` tinyint UNSIGNED DEFAULT NULL,
  `photo_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width_type` smallint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundle_item`
--

INSERT INTO `bundle_item` (`id`, `name`, `sub_name`, `description`, `sort`, `is_active`, `photo_id`, `place_id`, `width_type`) VALUES
('16a0b380-440a-47a9-a5f5-a85117ca022c', '{\"en\": \"rwer\", \"ru\": \"wer\"}', '{\"en\": \"rewrwe\", \"ru\": \"werew\"}', '{\"en\": \"rwerewr\", \"ru\": \"werewrwe\"}', NULL, NULL, NULL, 'adf02960-c923-4b5a-bb8a-04feb6509255', NULL),
('2ef23eac-f3f5-494a-b0ef-a88203e25f7e', '{\"en\": \"\", \"ru\": \"w234213123\"}', '{\"en\": \"\", \"ru\": \"123123123\"}', '{\"en\": \"\", \"ru\": \"123123123\"}', NULL, NULL, NULL, NULL, NULL),
('3291405e-4870-40f3-b448-37461075cf43', '{\"en\": \"\", \"ru\": \"w234213123\"}', '{\"en\": \"\", \"ru\": \"123123123\"}', '{\"en\": \"\", \"ru\": \"123123123\"}', NULL, NULL, NULL, NULL, NULL),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"bundle item 1\"}', '{\"ru\": \"bundle item 1_sub name\"}', '{\"ru\": \"bundle item 1_описание\"}', 1, 1, 'd298bd24-a164-481b-aaef-cfc2c277939c', '926e960c-0d5b-4796-a58a-0c5d6968ed0f', NULL),
('46d04372-7fc4-46f3-a724-755c320e3ea1', '{\"en\": \"\", \"ru\": \"123\"}', '{\"en\": \"\", \"ru\": \"123\"}', '{\"en\": \"\", \"ru\": \"3123\"}', NULL, NULL, NULL, 'adf02960-c923-4b5a-bb8a-04feb6509255', NULL),
('5e03062e-f354-46a8-99e4-15fa7b913b05', '{\"ru\": \"bundle item 2\"}', '{\"ru\": \"bundle item 2_sub name\"}', '{\"ru\": \"bundle item 2_описание\"}', 1, 1, 'd298bd24-a164-481b-aaef-cfc2c277939c', '926e960c-0d5b-4796-a58a-0c5d6968ed0f', NULL),
('6bc2f5b0-f78e-4631-a27f-1cbd5ad04a31', '{\"en\": \"\", \"ru\": \"3333sdfsd\"}', '{\"en\": \"\", \"ru\": \"12312fsdfsdf\"}', '{\"en\": \"\", \"ru\": \"2134242\"}', NULL, NULL, NULL, 'adf02960-c923-4b5a-bb8a-04feb6509255', NULL),
('9b460de3-d295-4b4b-aad6-5c787f85b22d', '{\"en\": \"test\", \"ru\": \"test\"}', '{\"en\": \"test\", \"ru\": \"test\"}', '{\"en\": \"test\", \"ru\": \"test\"}', NULL, NULL, NULL, '926e960c-0d5b-4796-a58a-0c5d6968ed0f', NULL),
('ac309897-c4ec-4974-9cb3-e0be2173002e', '{\"en\": \"webBackground\", \"ru\": \"webBackground\"}', '{\"en\": \"webBackground\", \"ru\": \"webBackground\"}', '{\"en\": \"webBackground\", \"ru\": \"webBackground\"}', NULL, NULL, NULL, 'adf02960-c923-4b5a-bb8a-04feb6509255', NULL),
('e36b6cf2-49cf-4c2c-baf8-253aab06304c', '{\"en\": \"333333333333333\", \"ru\": \"3333\"}', '{\"en\": \"3333333333333333\", \"ru\": \"333333333\"}', '{\"en\": \"3333333333333333\", \"ru\": \"333333333\"}', NULL, NULL, NULL, 'adf02960-c923-4b5a-bb8a-04feb6509255', NULL),
('e52968fb-dffc-4eba-af6e-ce89cbdecefd', '{\"en\": \"Resort Beach Rate\", \"ru\": \"Тариф на пляж курорта\"}', '{\"en\": \"Everyday\", \"ru\": \"Eжедневно\"}', '{\"en\": \"Resort hotels provide a daily free shuttle to the resort\'s Black Sea beach. Comfortable buses will bring guests to the coast every day\", \"ru\": \"Гостям отелей курорта предоставляется ежедневный бесплатный трансфер на пляж курорта на Черном море. Комфортабельные автобусы будут ежедневно доставлять гостей на побережье\"}', NULL, NULL, NULL, 'bec0804d-bbd9-4fc2-bfad-af11c14b5e84', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bundle_rate_plan`
--

CREATE TABLE `bundle_rate_plan` (
  `bundle_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate_plan_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundle_rate_plan`
--

INSERT INTO `bundle_rate_plan` (`bundle_id`, `rate_plan_id`) VALUES
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912'),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912'),
('5e03062e-f354-46a8-99e4-15fa7b913b05', '6582b0da-b8d0-4618-a763-864358706ce1'),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82');

-- --------------------------------------------------------

--
-- Table structure for table `bundle_term`
--

CREATE TABLE `bundle_term` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bundle_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` json NOT NULL,
  `sort` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundle_term`
--

INSERT INTO `bundle_term` (`id`, `bundle_id`, `description`, `sort`) VALUES
('6cba68ff-f8a2-41e6-afff-6f42ea7c0aa6', '3392181e-8977-442e-89b4-8b472dd4b696', '{\"en\": \"\", \"ru\": \"УСЛОВИЯ ПОЛУЧЕНИЯ КЕШБЭКА 1)      Зарегистрируйте карту «Мир» в Программе лояльности платежной системы «Мир».  Если у вас нет карты «Мир», вы можете оформить её в одном из банков-партнёров. Обращайте внимание на срок оформления. Карты выпускаются бесплатно, условия обслуживания уточняйте в банке. Для получения нужен паспорт.      2)      Оплатите тур зарегистрированной в Программе лояльности картой «Мир» в период с 18 марта по 15 июня 2021 года на этом сайте.     Кешбэк будет начислен только за поездки с датой возвращения до 30 июня 2021 года. Вы получите кешбэк в размере 20% от стоимости каждого тура, но не более 20 000 ₽ за одну транзакцию по карте. Число транзакций по одной карте не ограничено.  3)      Кешбэк начисляется на карту «Мир», с которой производилась оплата, в течение 5 рабочих дней с момента оплаты тура.\"}', 2),
('fc110835-1e63-4304-b90c-34e69d326960', 'c1e56949-7536-4f8c-ad13-896e31b38f2f', '{\"en\": \"\", \"ru\": \"Даты заезда с 05.04.2021 по 18.04.2021 Наполнение пакетного тура зависит от продолжительности отдыха – 4, 6 или 8 дней. Выбор продолжительности отдыха происходит ниже – на первом этапе бронирования. Выбор даты заезда – на втором этапе бронирования. Дата отъезда устанавливается автоматически. Выбор отеля – на третьем этапе бронирования.  В СТОИМОСТЬ ВКЛЮЧЕНО  4 дня / 3 ночи Проживание в отеле с завтраком и ужином. Ски-пасс на 2 любых дня из 4 – доступ со снаряжением на все открытые для дневного катания подъемники и трассы. Родельбан 3 круга: катание от 1 до 3 кругов в любые 3 дня отдыха.  Входной билет в интерактивный Музей Археологии. Входной билет в этнопарк «Моя Россия». Прокат велосипеда 1 час. Скидка 20% на прокат Роза Хутор.* Скидка 10% на услуги инструкторов Горнолыжной Школы Роза Хутор.*  6 дней / 5 ночей Проживание в отеле с завтраком и ужином. Ски-пасс на 4 любых дня из 6 – доступ со снаряжением на все открытые для дневного катания подъемники и трассы. Родельбан 3 круга: катание от 1 до 3 кругов в любые 3 дня отдыха.  Входной билет в интерактивный Музей Археологии. Входной билет в этнопарк «Моя Россия». Прокат велосипеда 1 час. Скидка 20% на прокат Роза Хутор.* Скидка 10% на услуги инструкторов Горнолыжной Школы Роза Хутор.*  8 дней / 7 ночей Проживание в отеле с завтраком и ужином. Ски-пасс на 5 любых дней из 8 – доступ со снаряжением на все открытые для дневного катания подъемники и трассы. Родельбан 3 круга: катание от 1 до 3 кругов в любые 3 дня отдыха.  Входной билет в интерактивный Музей Археологии. Входной билет в этнопарк «Моя Россия». Прокат велосипеда 1 час. Скидка 20% на прокат Роза Хутор.* Скидка 10% на услуги инструкторов Горнолыжной Школы Роза Хутор\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `card_type`
--

CREATE TABLE `card_type` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `type` smallint UNSIGNED NOT NULL,
  `price` bigint UNSIGNED DEFAULT NULL,
  `pps_product_id` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `card_type`
--

INSERT INTO `card_type` (`id`, `type`, `price`, `pps_product_id`, `created_at`, `updated_at`) VALUES
('2f18c50b-f89d-4016-9ead-f7cb97ffe0f1', 200, 35600, 23554125, '2021-03-03 15:07:10', '2021-03-03 15:08:00'),
('35b0bb3a-bef2-469c-9911-81ad080dc58b', 300, 0, 25297694, '2021-03-03 15:07:10', '2021-03-03 15:08:00'),
('e0765eed-0680-467b-a905-430f023f9e0b', 100, 0, 23554129, '2021-03-03 15:07:10', '2021-03-03 15:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `status` smallint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
('0157f8e1-ac1d-4d84-8294-2e8ae466dc49', '6e4549c6-b9db-4602-85b6-a0bf3ddffb56', 200, '2021-03-05 14:38:49', '2021-03-05 14:38:49'),
('02a3cc10-e28c-4015-99bd-4e1bd63a48b5', '87f59790-51cc-447d-b6fe-273ff067edd9', 200, '2021-03-15 11:37:26', '2021-03-15 11:37:37'),
('0380fe8d-19f5-4f79-a5e8-1cd9404bdbf5', '89639e92-638d-4135-bbfe-ac88ba455ccd', 200, '2021-03-05 15:22:44', '2021-03-05 15:22:44'),
('0467e91a-da48-4c40-b566-26a204f5ef11', '8956722f-8c97-416f-9b00-a2b374122fcf', 200, '2021-03-05 13:25:46', '2021-03-16 07:20:47'),
('0678e00e-0d71-4753-80bb-773cf956e92f', 'c589f048-a104-47f5-a679-febfaa57cb33', 200, '2021-03-05 10:53:19', '2021-03-05 11:35:41'),
('08f98fa6-0884-4c78-956a-7b3cddafba74', '2bfed563-132d-48e1-b113-f549b43c4322', 100, '2021-03-03 05:31:37', NULL),
('0b54ad94-3917-4dbf-89fd-6f929c1766e5', '979b0d5f-8806-4378-8313-1bc31d3fc0b1', 200, '2021-02-24 12:01:59', '2021-02-24 12:01:59'),
('0cbba92c-83df-4de3-980a-7c62edbb6950', 'd46072ca-9c17-48bd-a82f-289f4c4550e8', 200, '2021-03-17 14:52:28', '2021-03-17 15:52:23'),
('0cdfe298-8d3e-4a93-b2c8-876e46379cf6', '6cd1b662-3f8a-4cd8-a2c6-aa30618e7cd0', 100, '2021-03-19 15:10:39', NULL),
('0d68beb8-6341-4952-adc7-38351fa91e2d', '53b70f6d-ddc6-456d-91ca-96d80052660f', 200, '2021-03-10 08:59:18', '2021-03-10 08:59:18'),
('0dee7234-860c-4cb2-a9da-b1a2d46c0ea4', '65da37f4-79f6-41e6-841b-2cec513b9bbb', 100, '2021-03-18 07:36:49', NULL),
('0f5cf68b-2145-49d7-b0e7-5482042a63b7', '2f5121e9-77d0-41d3-9e9e-1494b979469b', 200, '2021-03-09 06:50:41', '2021-03-09 06:50:41'),
('1078d7ba-347f-44b2-8173-36eb9a3dccd3', '674e9ad0-4bab-41f5-9840-7f04fa0d38f8', 200, '2021-02-19 10:48:25', '2021-02-19 10:48:25'),
('11583b4c-ff8a-4bbc-baf5-63b24ed9cade', '27b9eeed-aee2-4f1f-ad13-59abdc97c819', 100, '2021-03-19 04:55:27', NULL),
('12f783c3-e3a7-497f-b756-52a72ccf59f0', 'efd67efa-2fcf-4287-9561-b2963ff5a2bd', 200, '2021-03-05 06:38:47', '2021-03-05 06:38:47'),
('15274821-8581-4057-8d96-118059f26cd2', '83eb5fd2-8b5f-4bc1-ab18-d365072b2ff9', 200, '2021-02-24 10:39:31', '2021-02-24 10:39:32'),
('18ffb854-85eb-4032-95c2-8a7b97e47d65', '8a3f54c5-113c-41da-8ffb-710462bb0233', 200, '2021-02-11 11:32:06', '2021-02-11 11:32:06'),
('1aa2e2ef-b3cd-47a9-8d1a-72cfbc47aed4', '495224d5-5806-4525-925d-5a73f5c681d8', 100, '2021-03-10 10:43:14', NULL),
('1cf1c1a3-9568-46d1-8c29-1c2d0595d895', '8956722f-8c97-416f-9b00-a2b374122fcf', 200, '2021-03-18 11:49:17', '2021-03-18 11:49:21'),
('1ffa2bc5-8258-455c-bae8-8874209bbe5a', '247bed1f-68ab-4a7c-976c-2c06335e5898', 100, '2021-03-05 06:33:28', NULL),
('2215741a-55b3-4250-8843-986e4b1d9645', 'b06d3137-d3ce-4737-aaa2-3309a7d5d3a7', 100, '2021-03-05 08:20:08', NULL),
('236bf34f-ea20-4b54-a47a-abf091f678de', '63f2d790-3165-4516-9461-b7681a7be833', 200, '2021-03-09 14:32:44', '2021-03-17 18:57:29'),
('25cab061-5df1-4175-b427-99e62bdde996', 'c55be4c4-47dc-421b-b06e-752e613f7fa1', 200, '2021-03-05 08:22:31', '2021-03-05 08:22:31'),
('2632f176-246a-4ede-b4e6-9e7fe923210e', '8956722f-8c97-416f-9b00-a2b374122fcf', 200, '2021-03-19 10:16:11', '2021-03-19 11:11:03'),
('266a6383-b1d6-43aa-950e-03ad946a138c', '8956722f-8c97-416f-9b00-a2b374122fcf', 200, '2021-03-18 11:51:44', '2021-03-18 11:52:04'),
('266f707f-3a97-4c67-aaf6-be75bef52644', '2a23cd11-66d1-4c8f-9c81-60992e8fbe14', 200, '2021-03-05 15:17:07', '2021-03-05 15:17:07'),
('281b1179-b2a6-491a-b251-54649a7e716f', 'b71a8ac9-025f-4935-8798-bb421cde80e8', 200, '2021-03-18 06:21:19', '2021-03-18 06:21:19'),
('2c4d4a77-4461-4f01-9172-eaf51fb4f5dc', 'c589f048-a104-47f5-a679-febfaa57cb33', 200, '2021-03-05 11:39:39', '2021-03-05 11:39:53'),
('2c614a47-f108-46f7-9150-12ee3028caf0', '8956722f-8c97-416f-9b00-a2b374122fcf', 100, '2021-03-19 11:12:37', NULL),
('2c9a38f7-e231-4700-98d8-a3b76ee0cd54', '843e18a3-e0cb-4ae3-a584-19b7f20d7c59', 100, '2021-03-15 11:49:31', NULL),
('2dc8903b-7580-4f6c-adc1-349c910f9012', 'f96c900d-8215-4f63-a2fb-427a08ffad34', 200, '2021-02-24 11:59:11', '2021-02-24 11:59:11'),
('30d1d1e4-46e4-44e3-981b-1341a9c9850c', '86e089e9-4e8e-4652-b896-a42f1dc2590e', 100, '2021-03-18 11:21:57', NULL),
('32054726-9f36-4867-934f-dda63611d02d', '6aefb3ef-90e0-4d97-8f31-fe0dcdb6d048', 200, '2021-02-07 14:41:39', '2021-02-07 14:41:39'),
('3299b76b-7911-461b-8567-9ac4b1230871', 'd1fd2aa5-452c-4ae6-b9a5-018c52cfd128', 200, '2021-03-23 11:53:42', '2021-03-23 11:53:42'),
('346f838c-bb0c-49b3-afa6-10362da58102', 'cd8f1694-77e4-4842-87f0-7bf6fbe05e11', 100, '2021-03-10 08:03:38', NULL),
('3801e3ad-1aaf-441e-b70a-82021ea283cc', '903d29fd-9166-4dc3-81bd-4f798cdb582c', 100, '2021-02-19 16:27:33', NULL),
('38b9d95e-6c33-460b-9e8e-c36f40f37e42', 'c589f048-a104-47f5-a679-febfaa57cb33', 200, '2021-03-05 10:35:43', '2021-03-05 10:35:43'),
('3dedc94d-4289-47c3-8167-1c6ac841fa73', 'b6bfc2ea-7035-4c4b-a5b5-1831cd7be999', 100, '2021-03-04 10:14:51', NULL),
('44f85307-9c14-4701-9ed4-972fb483dbf1', 'fdf04558-40f5-4090-9242-0e8277feae81', 100, '2021-03-05 09:32:39', NULL),
('45d6796b-84b1-42c8-b57a-6edfb595bfe9', 'befb80c4-87e3-4609-8eef-744c74dd0f29', 200, '2021-03-17 12:25:22', '2021-03-17 12:25:22'),
('4794c548-e841-4615-8e1e-cb0886fc55cc', '8e5aae66-b12b-4bac-8fe3-04407ba1857d', 200, '2021-02-08 14:31:18', '2021-02-08 14:31:18'),
('48989843-a161-4703-a6c3-ec05c950e0b9', '168041e5-b5d8-446d-aac0-a4507c5e30f1', 200, '2021-02-24 11:53:46', '2021-02-24 11:53:46'),
('48a9da31-2774-44e1-9596-3e36c1c74afd', '89639e92-638d-4135-bbfe-ac88ba455ccd', 100, '2021-03-05 15:24:30', NULL),
('496eacac-aa53-4571-af7c-8c20d33c9775', 'b4cef9cd-b750-4eb1-b672-7ac8f17986c0', 100, '2021-03-15 05:43:28', NULL),
('4be976e8-fc4d-4209-8404-9182fb851c8a', '2bd07951-6b22-4021-bacb-16027acdf085', 200, '2021-02-11 12:53:55', '2021-02-11 12:53:55'),
('4c7a9558-7fc8-41da-a2d1-8822590003f3', '87f59790-51cc-447d-b6fe-273ff067edd9', 200, '2021-03-06 06:52:08', '2021-03-06 06:52:20'),
('4d1d3435-fc02-4757-8e21-80c737079f87', 'cc5dbe0d-7e23-41a7-ab60-ef955b3993fb', 200, '2021-03-02 10:19:11', '2021-03-02 10:19:11'),
('4d4a4fad-ce82-40b6-8a83-1f50f80bf75b', 'efd67efa-2fcf-4287-9561-b2963ff5a2bd', 100, '2021-03-05 06:41:58', NULL),
('4ee13ed6-1993-4c01-851e-b9bd334f9d32', '2a23cd11-66d1-4c8f-9c81-60992e8fbe14', 100, '2021-03-05 15:19:20', NULL),
('4fead0ed-5e2a-4e02-ab78-8490b5b92701', 'e362e4d0-58d8-4f6b-bdb0-a323b6f8ce00', 200, '2021-03-02 09:10:28', '2021-03-02 09:10:28'),
('5065b027-f88b-45bb-9f83-3b09dfec7a2b', '27b9eeed-aee2-4f1f-ad13-59abdc97c819', 200, '2021-03-05 08:27:27', '2021-03-05 08:27:27'),
('50c4b62a-6a35-4036-98cd-c42aa4f731f3', 'cd8f1694-77e4-4842-87f0-7bf6fbe05e11', 100, '2021-03-10 08:03:38', NULL),
('516c89bc-6a88-4d54-8ecf-b72d84df6b0c', 'b06d3137-d3ce-4737-aaa2-3309a7d5d3a7', 200, '2021-03-05 08:14:36', '2021-03-05 08:14:36'),
('5206afcb-4178-48f6-9610-b0f8f8d32b57', '684b0537-5623-460e-8919-3c2fc7b5dcbd', 200, '2021-03-02 14:27:17', '2021-03-02 14:27:17'),
('5392291d-0ec4-4403-8c1d-bcb8a3b81356', '46666f3e-dbdd-416d-9235-c76c0843552b', 200, '2021-03-05 14:48:16', '2021-03-05 14:51:52'),
('54f74e66-d82d-47cc-a8a5-4049d0a40b53', '58b6bce3-746b-42e0-b81a-ce4e4199ffa4', 100, '2021-03-15 05:55:24', NULL),
('56c080bf-b923-4788-b57e-9e7a8247fe4a', 'dffc3af2-7f2f-42c9-8c3d-a61c74b8368f', 200, '2021-02-24 11:37:55', '2021-02-24 11:37:55'),
('5a1c18ce-3524-426b-a138-18daa760e208', 'de7a4145-b7ce-43b4-9210-e85bd45ad6cc', 200, '2021-03-05 13:37:06', '2021-03-05 13:37:06'),
('5aa6e79f-79d4-4074-b01f-9573712347cd', 'cd8f1694-77e4-4842-87f0-7bf6fbe05e11', 200, '2021-03-05 11:42:41', '2021-03-05 11:42:41'),
('5cd7946c-2cd4-44ea-a61c-49b99a1df275', 'd46072ca-9c17-48bd-a82f-289f4c4550e8', 200, '2021-03-17 13:01:49', '2021-03-17 13:01:49'),
('5dfe119b-e52f-4909-9848-bc21af669422', '63f2d790-3165-4516-9461-b7681a7be833', 100, '2021-03-09 14:32:44', NULL),
('60a05248-c6a5-419f-b142-d41bfeb13d6a', 'ebab9bb0-aeba-4943-81a4-c3162f3394f0', 200, '2021-03-12 14:38:45', '2021-03-12 14:38:45'),
('619b67d7-0db6-417c-98e1-69f57282faad', '5c5d696b-a194-4a14-a745-4c868518f9cf', 200, '2021-02-24 12:00:57', '2021-02-24 12:00:58'),
('645d5479-88c9-4264-aa2c-8dd461b37d3c', '87f59790-51cc-447d-b6fe-273ff067edd9', 200, '2021-03-11 05:37:39', '2021-03-11 14:24:44'),
('67c65018-b0a7-4830-a2e5-e2296939bbbb', 'c55be4c4-47dc-421b-b06e-752e613f7fa1', 100, '2021-03-05 08:24:38', NULL),
('6896e8a0-55b6-4ae0-8cdd-555398653ebd', '6a03367d-d6ba-4b14-bdd9-2d1a4b6ddd13', 100, '2021-03-03 12:26:55', NULL),
('68e5bdaf-bdf0-4464-8261-7782edf144b1', '290ac6c2-bb39-4740-acca-dbb0d84bd427', 200, '2021-02-24 11:32:09', '2021-02-24 11:32:09'),
('6966f404-40b4-48e8-92a1-912044e08c08', '6a810a00-9f98-4f9b-9429-3e5339420b5a', 100, '2021-03-22 12:30:39', NULL),
('710d1c69-19f7-4f52-b907-b11e75382fc5', '642837b7-6846-4d0c-a7ce-b0bc909d6c58', 200, '2021-03-05 13:46:09', '2021-03-05 13:46:09'),
('757a3246-2080-4bf3-97da-f44c2c0a35e9', '5bc305f9-45e7-4f32-8b6e-c89b0bb51859', 100, '2021-03-02 09:32:48', NULL),
('764f677a-9717-49fe-a824-0e82c5900e0f', '33a1b771-a9e0-466c-bb48-24d3efaf7d1e', 100, '2021-03-04 07:33:06', NULL),
('7796f229-a335-4899-b798-6d04b6a561f1', '12a66a06-a4a5-49a4-8d41-a24bd9e8b0c2', 200, '2021-03-02 13:48:06', '2021-03-02 13:48:06'),
('7818071e-f388-467f-8276-df1e22fd126d', '395fb206-2304-4af1-bab9-2f780136812e', 200, '2021-03-05 10:44:43', '2021-03-05 10:44:43'),
('781ba4c1-cc74-4c0c-b5a5-04720d9b95f6', '8956722f-8c97-416f-9b00-a2b374122fcf', 200, '2021-03-16 13:33:31', '2021-03-18 11:35:05'),
('78c9a006-96f5-4f37-b0a9-b58a0cf31d15', '9bd56fbe-6b3c-484b-84fe-2edd0f304272', 100, '2021-03-09 06:42:10', NULL),
('7927edeb-c148-45c2-981a-9a99c98c5efd', 'befb80c4-87e3-4609-8eef-744c74dd0f29', 100, '2021-03-17 12:35:26', NULL),
('7ae1f1e8-b8e5-4c5d-ad25-50e5b5d195ee', 'c799a607-9374-4912-9314-b4977c13e170', 200, '2021-03-05 15:33:20', '2021-03-05 15:33:20'),
('7f07943a-2571-434a-909d-9ac4cde665ab', '6b250f66-687c-4343-8930-95ddadef3df4', 100, '2021-03-05 12:54:14', NULL),
('814296cb-6785-4805-bd1a-118897f7ef6c', '0b8532fa-daef-467d-bd6c-08447607f55a', 200, '2021-02-20 14:14:57', '2021-02-20 14:14:58'),
('862d175c-0d77-4ecb-9e22-1d3117171ddc', '65c7c0c7-226f-4631-a89e-f471e1da4c2f', 200, '2021-03-16 15:31:38', '2021-03-16 15:31:39'),
('886ddeaa-c4da-43c0-adcf-bc73d3631579', 'de7a4145-b7ce-43b4-9210-e85bd45ad6cc', 100, '2021-03-05 13:42:06', NULL),
('88aba333-0b1c-428a-b449-7481bbaf1b12', '6d5e86d5-afbb-4826-a0ef-3a892ea27ad1', 200, '2021-03-02 14:23:09', '2021-03-02 14:23:09'),
('89b9f830-c5df-431a-bdf7-7cb46f29acb8', 'b2eccdc0-cc39-4605-97b3-399372965273', 200, '2021-02-11 10:59:26', '2021-02-11 10:59:26'),
('8c2958fd-15b7-4827-9753-00e8527ec627', 'c6bca30b-2939-462c-9993-40e71c9a8e13', 200, '2021-03-18 06:08:02', '2021-03-18 06:08:02'),
('8d47c920-bef6-483f-b624-d7267217ca9f', '87f59790-51cc-447d-b6fe-273ff067edd9', 200, '2021-03-15 11:33:43', '2021-03-15 11:34:53'),
('8d6b5203-0dcf-4a56-a659-89ff523f962d', 'c4fd6fdd-bf59-416c-b54d-3f20b05ef6fa', 200, '2021-03-11 14:27:04', '2021-03-11 14:27:04'),
('8e5fcfa6-7c3e-41d6-b1f0-2ec671807b75', '33826dfc-3fce-457f-b9da-6d4aa8fff655', 200, '2021-03-18 06:06:53', '2021-03-18 06:06:53'),
('8eecc6e2-8df0-4f8c-b0b1-a8ac5ecf78b4', 'a6f61ce6-f88d-464a-8c5f-dc3bb605d189', 200, '2021-03-05 13:37:29', '2021-03-05 13:37:29'),
('8f16767d-367a-475c-a70a-3aff59a4353e', '734000dc-8067-46c2-b845-32130aa74cc6', 200, '2021-03-09 06:47:45', '2021-03-09 06:47:45'),
('91be7dba-0f2b-4042-b5c1-2a08644e407c', '3821b2cc-c9a7-4790-9b37-d6842a2d3024', 100, '2021-02-26 13:40:47', NULL),
('92b08fac-32d8-48fd-92dc-9d063d98865a', 'd9540b4c-c20a-447b-b661-23e9aa703875', 200, '2021-03-05 06:54:24', '2021-03-05 06:54:24'),
('93738bc1-b24d-4e76-9cd2-8d3cdbb32ace', 'fcfcb4a3-9b19-4d0f-a2c8-83fafe3c583c', 200, '2021-03-18 06:08:59', '2021-03-18 06:08:59'),
('9407ca11-be4b-44e8-9410-131a84b307ab', 'e1236b7f-c332-4a2b-84dc-44070d4dfa5c', 200, '2021-03-18 05:59:17', '2021-03-18 05:59:17'),
('98358954-b446-4c77-8087-10000b65a16c', '27b9eeed-aee2-4f1f-ad13-59abdc97c819', 100, '2021-03-19 04:55:27', NULL),
('985b8037-3cea-491f-8142-bf3203a056f6', 'b9fd77b2-27ea-41fd-9727-ab30822f1258', 200, '2021-03-05 06:43:58', '2021-03-05 06:43:58'),
('9d19ca9b-abf9-4366-8e05-46fa077d78d3', 'd46072ca-9c17-48bd-a82f-289f4c4550e8', 100, '2021-03-17 18:05:37', NULL),
('9d7c682e-162c-4fd3-a89b-a2bffdbeb263', 'b5d40428-ef2c-474a-b043-a84f773a104f', 200, '2021-03-06 14:26:09', '2021-03-06 14:26:09'),
('a06d00c3-d47d-43da-af80-ff0f48c75c37', '61bef744-5c97-4fb8-a856-7bc178ae8fe3', 200, '2021-02-19 13:26:26', '2021-02-19 13:26:26'),
('a16e387d-763f-45d2-aa17-03351a4d90c7', '71d3b61a-1a63-45d3-bb2d-cf86dcc007e5', 200, '2021-03-06 14:25:52', '2021-03-06 14:25:52'),
('a55a0763-f994-464b-a54e-4a8a86edd068', '16992a4c-cd47-4731-b483-ea08cc6d6ea8', 100, '2021-03-17 09:00:46', NULL),
('a5d57082-d549-415f-8f7d-7ff8067a110c', 'd8002bff-daff-4b5f-9217-40662dd688eb', 200, '2021-03-16 15:31:13', '2021-03-16 15:31:13'),
('a65a1b54-07db-40e3-af21-e81346bd1f0d', 'c799a607-9374-4912-9314-b4977c13e170', 100, '2021-03-05 15:35:20', NULL),
('aaa4d380-de2e-4424-8c15-b1dc8b586260', 'bf841828-ce79-4560-b5c0-c3e82bf213fd', 200, '2021-02-19 10:43:52', '2021-02-19 10:43:52'),
('acb59baf-a1be-4f00-af1e-5e5d34866723', '88ff3a09-832e-4693-8b33-78d46288495c', 200, '2021-03-02 14:46:28', '2021-03-02 14:46:28'),
('acbb3118-10ea-4af0-8a70-d919e423d2ec', 'cd8f1694-77e4-4842-87f0-7bf6fbe05e11', 100, '2021-03-10 08:03:38', NULL),
('b05a9493-d389-47b2-adcb-85d3865d9d96', 'edc9b6e6-fc78-4290-9823-d90f144ad1ae', 100, '2021-03-17 12:16:52', NULL),
('b4da2e8b-9cf0-4d8d-b599-d44322238b52', 'e80f9a5a-a674-42a7-a4e8-1c9988b7f8dc', 200, '2021-02-08 14:32:34', '2021-02-08 14:32:34'),
('b6dabb25-3431-4e7a-9f10-d91f80ff8676', '87f59790-51cc-447d-b6fe-273ff067edd9', 200, '2021-03-06 06:20:13', '2021-03-06 06:20:13'),
('b7358a3f-0e84-4e8b-a5cf-95a20c3aa9f5', '6a199751-6db8-4fc0-a2ec-160c601cc50c', 200, '2021-03-05 15:06:26', '2021-03-05 15:06:26'),
('ba07c729-01ef-4a7e-a70b-8e2a38d1c723', '588af053-240f-461b-a2e1-74b3a1e56a1b', 100, '2021-03-15 06:55:31', NULL),
('bb154f80-624c-473e-9e03-f301b4029cfc', 'b8cb3e0d-59e3-49f2-9067-9f6fe1f9ca9d', 200, '2021-02-24 11:48:29', '2021-02-24 11:48:29'),
('bb4fed66-ddc7-4831-bad3-6cf7b126bb60', 'd350c3dd-7295-45ac-8de6-8df9c4cc62de', 200, '2021-03-15 04:59:25', '2021-03-15 04:59:25'),
('be67f4c1-f163-490f-9462-7a192cc0e7c2', '87f59790-51cc-447d-b6fe-273ff067edd9', 100, '2021-03-15 12:40:16', NULL),
('c0ddfd40-a818-42c8-a85c-74c7b38dfb6d', '8956722f-8c97-416f-9b00-a2b374122fcf', 200, '2021-03-05 09:27:53', '2021-03-05 13:22:43'),
('c38bd8a1-68f1-4ba3-9364-3c2de50ab74e', '2f44aaf3-cf55-47a1-bd26-0e97e4704390', 200, '2021-02-24 09:57:17', '2021-02-24 09:57:18'),
('c5935b9b-332c-4ccb-8bbe-90ef95fcf655', '1890ddef-d30b-481d-a6f0-bbf5a29b170f', 100, '2021-03-01 08:50:31', NULL),
('c63a2d41-251b-4f77-b98e-b68c519d7ea0', '3cf2c7e5-64c4-4289-b36d-10b1277a61ea', 100, '2021-03-13 13:03:23', NULL),
('c67d6b66-2b1b-4460-a6c6-50c83bf3ce41', 'b5d40428-ef2c-474a-b043-a84f773a104f', 200, '2021-03-06 14:48:52', '2021-03-06 14:49:01'),
('cb07f9ba-a50f-4110-8d3e-d30a8eca52ed', '95de8a66-d187-4a26-908a-de44fd1b9e46', 200, '2021-02-11 12:59:25', '2021-02-11 12:59:25'),
('cba4b2b0-4784-4b74-8dff-c6ecf1a132d6', '7f6d0f8e-82e2-4c18-8e78-d74fad31959b', 100, '2021-03-12 09:53:17', NULL),
('cba82a63-22eb-4d6f-aeae-75edf1068463', '32a00c16-2ce9-4470-a033-7b00334119dd', 100, '2021-02-26 12:42:45', NULL),
('cd0352dc-22d5-448f-aa35-01228b399975', '7f6d0f8e-82e2-4c18-8e78-d74fad31959b', 200, '2021-03-11 14:33:03', '2021-03-11 14:33:03'),
('d01b0267-a276-451b-8997-eb95dc5561b5', '994064dd-1d99-4e15-97fa-77fb080b46f9', 200, '2021-03-24 06:55:24', '2021-03-24 06:55:24'),
('d0630f3a-959f-4a83-9c18-ee946cf60495', 'edc9b6e6-fc78-4290-9823-d90f144ad1ae', 200, '2021-03-17 11:08:24', '2021-03-17 11:08:24'),
('d655d786-30db-46be-8e67-3951f041ef8c', 'b9fd77b2-27ea-41fd-9727-ab30822f1258', 100, '2021-03-05 06:46:24', NULL),
('d9042a11-2ec9-4ddb-b1b5-cbab439e06b2', '324d18f1-8035-4ecd-85c2-3a88fe13063f', 200, '2021-03-06 14:38:18', '2021-03-06 14:38:18'),
('da65a3c9-e56d-4321-b54e-9ca5b36780d2', 'b74b0e14-40db-4189-8009-8e49eb0f6c8e', 200, '2021-03-18 06:18:45', '2021-03-18 06:18:45'),
('dda49a3b-0fe6-4afa-9e26-92efeaa101f5', '93c77cba-e896-4aac-9530-71d07d454351', 200, '2021-02-24 11:31:04', '2021-02-24 11:31:04'),
('e18e40b0-4f66-455f-8e0b-27667be9caec', '994064dd-1d99-4e15-97fa-77fb080b46f9', 100, '2021-03-24 06:59:09', NULL),
('e2546eb7-682a-4889-a080-3e3980919d7e', '6b250f66-687c-4343-8930-95ddadef3df4', 200, '2021-03-05 12:05:29', '2021-03-05 12:05:29'),
('e3b52bdc-1e92-4714-a7c9-193a89578581', 'd54250af-941c-496d-97d6-c8ec867d47e9', 100, '2021-03-18 12:07:30', NULL),
('e79760f7-42b8-4207-90a7-4df9a574ecb9', '305abe94-9792-449f-bd4e-5ad61ecc5cd6', 100, '2021-03-04 12:33:17', NULL),
('e86e1b9d-752f-4c19-a391-58d3a0987744', '0ebba7a5-f04d-4ebb-a155-6fe14622376b', 100, '2021-03-11 11:59:09', NULL),
('e8ddd62c-d9d5-4d1a-8e78-fb02e8e0fe2e', '247bed1f-68ab-4a7c-976c-2c06335e5898', 200, '2021-03-05 06:32:01', '2021-03-05 06:32:01'),
('e9e205fd-0536-47fe-aea4-00308d2c3c28', '642837b7-6846-4d0c-a7ce-b0bc909d6c58', 100, '2021-03-05 13:50:11', NULL),
('eb1a75b4-8274-48b6-b668-b79c5ae2cd0f', 'c8ab93ff-8d1a-4ed3-95f8-e42ee8117eea', 100, '2021-03-03 11:32:52', NULL),
('ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', '820403c7-ab5e-41d7-945e-638349d6024e', 100, '2021-03-09 05:54:21', NULL),
('ece21266-29e0-45b1-871b-c5523fa80735', '48d382e0-5dd5-4fb0-badf-5e7339988d6a', 100, '2021-03-03 11:17:16', NULL),
('f1c32aa9-1a3e-4e74-8b88-e3319fc17da8', '250eaf6c-7a5d-4bd9-923d-47be78439196', 200, '2021-03-02 14:35:29', '2021-03-02 14:35:29'),
('f39d1750-52b0-407e-a41b-5d6cdde4c07c', '5d23565d-0454-4af4-a41d-34f4ce58f29c', 100, '2021-03-11 05:47:00', NULL),
('f654d0e2-7150-473f-adb4-592cb2fdd7d8', 'fdf04558-40f5-4090-9242-0e8277feae81', 200, '2021-03-05 09:21:43', '2021-03-05 09:21:43'),
('f6baa61a-1e18-437b-bf0b-81c60ea941c7', 'e1236b7f-c332-4a2b-84dc-44070d4dfa5c', 100, '2021-03-18 05:59:50', NULL),
('f6d2f872-044f-4e82-9ac5-d541f15c013d', '70fef0aa-3a94-4f2b-badd-4301ff663fb2', 100, '2021-03-10 05:39:16', NULL),
('f860d45e-49fb-41c5-a364-df24b51344d7', 'ad816613-d31e-430e-97fa-dc75eb3d3892', 200, '2021-03-17 08:41:25', '2021-03-17 08:41:25'),
('fc35d5d4-b047-4dca-8741-488502203e8b', '6b250f66-687c-4343-8930-95ddadef3df4', 200, '2021-03-05 12:09:30', '2021-03-05 12:52:40'),
('fd3ed008-8d65-474b-ba50-8449bfe35881', 'a6f61ce6-f88d-464a-8c5f-dc3bb605d189', 100, '2021-03-05 13:38:05', NULL),
('fd789617-f6e6-4fcd-b5b1-7b5a4d5a87af', '843e18a3-e0cb-4ae3-a584-19b7f20d7c59', 200, '2021-03-10 05:45:12', '2021-03-10 05:45:12'),
('fe415fc1-5ed7-4fbd-9932-52c037335d88', '6b250f66-687c-4343-8930-95ddadef3df4', 200, '2021-03-05 12:06:29', '2021-03-05 12:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `cart_item`
--

CREATE TABLE `cart_item` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `cart_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `product_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `quantity` smallint UNSIGNED NOT NULL,
  `activation_date` date DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
  `card_num` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_new_card` tinyint UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_item`
--

INSERT INTO `cart_item` (`id`, `cart_id`, `product_id`, `quantity`, `activation_date`, `last_name`, `first_name`, `middle_name`, `birth_date`, `card_num`, `is_new_card`, `created_at`, `updated_at`) VALUES
('000aa11c-fe31-4076-bb00-e5440d719e0d', '32054726-9f36-4867-934f-dda63611d02d', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-07 14:41:39', NULL),
('00925fb4-3ad8-41ab-9167-8602dbc69e16', '12f783c3-e3a7-497f-b756-52a72ccf59f0', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:38:47', NULL),
('02bc1ac1-b202-4aac-95f9-4658bfe8fb6d', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:20:01', NULL),
('02cf2c2b-5570-4323-9a2d-35664c74eb95', '757a3246-2080-4bf3-97da-f44c2c0a35e9', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 3, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 09:32:48', '2021-03-02 10:44:41'),
('03154ede-87da-4117-8e10-70641ea2d8fe', '0cbba92c-83df-4de3-980a-7c62edbb6950', '17ec8006-84b6-41e7-aedf-349b83efcca1', 3, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 14:52:37', NULL),
('0375df24-b45a-47d6-a16d-b3d21cef0e6c', '3dedc94d-4289-47c3-8167-1c6ac841fa73', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 1, '2021-03-04', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 12:03:23', NULL),
('03b61c1a-0361-4e5e-9531-ebacf65e91fd', '5cd7946c-2cd4-44ea-a61c-49b99a1df275', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 13:01:49', NULL),
('043f8897-81e5-4d2d-a7b5-16563a337102', '8d6b5203-0dcf-4a56-a659-89ff523f962d', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-11', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-11 14:27:04', NULL),
('0677d630-93b4-4057-8df7-9eba2ecb3869', '0b54ad94-3917-4dbf-89fd-6f929c1766e5', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 12:01:59', NULL),
('06bbc011-6ab2-4c9a-8db6-cd119fa33e07', 'e18e40b0-4f66-455f-8e0b-27667be9caec', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-24', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-24 06:59:09', NULL),
('08a96d8a-8978-420b-8a71-565a0185e176', '7818071e-f388-467f-8276-df1e22fd126d', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 1, '2021-03-14', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:44:43', NULL),
('0a437229-2052-4f55-8eb7-054c77b12b91', '44f85307-9c14-4701-9ed4-972fb483dbf1', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:34:33', NULL),
('0a52dd74-063c-4401-bf47-ad593723392a', '4ee13ed6-1993-4c01-851e-b9bd334f9d32', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:20:14', NULL),
('0b41f1cd-230b-4a1f-8f5c-9487db95b0c6', '5392291d-0ec4-4403-8c1d-bcb8a3b81356', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 14:50:51', NULL),
('0c9ce3f6-827d-4874-9d31-2d9a0821d6dd', '2215741a-55b3-4250-8843-986e4b1d9645', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 08:20:08', NULL),
('0caa27c9-fdc9-487f-b7b7-fa29e9572d21', 'e2546eb7-682a-4889-a080-3e3980919d7e', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:05:29', NULL),
('0cd3d309-2d98-469b-a95a-8f8d5eedd1f5', '7818071e-f388-467f-8276-df1e22fd126d', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:44:43', NULL),
('0db2b749-7251-4ecb-b33b-12da90a5a9a5', 'b7358a3f-0e84-4e8b-a5cf-95a20c3aa9f5', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:06:26', NULL),
('0dda706c-9fb4-4589-bebc-65917b72fee8', 'fe415fc1-5ed7-4fbd-9932-52c037335d88', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 3, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:06:29', NULL),
('0e289569-ab2c-46d9-afc3-dcd337ece776', '44f85307-9c14-4701-9ed4-972fb483dbf1', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:32:45', NULL),
('0f8bd1b5-9e05-4008-97cc-6056258bb218', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:25:09', NULL),
('12fa32c4-648a-4fa9-929d-83f0e582e55d', 'a55a0763-f994-464b-a54e-4a8a86edd068', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 10:30:46', NULL),
('13a0dae7-a54d-4fdc-92a9-dbfbdaac4acd', '0cdfe298-8d3e-4a93-b2c8-876e46379cf6', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-19 15:10:39', '2021-03-19 15:12:24'),
('1419b212-c409-413d-abb5-49d9aea99259', '60a05248-c6a5-419f-b142-d41bfeb13d6a', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 2, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-12 14:38:45', '2021-03-12 14:38:45'),
('14f2d683-7f83-4a2d-aaf1-12451907c541', '9d19ca9b-abf9-4366-8e05-46fa077d78d3', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-17 18:05:37', NULL),
('152ad8b1-a94c-43c6-aa56-84237047a6db', '985b8037-3cea-491f-8142-bf3203a056f6', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:43:58', NULL),
('1602f9f9-b088-462f-8326-99e79e556e02', '0157f8e1-ac1d-4d84-8294-2e8ae466dc49', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 14:38:49', NULL),
('168aba40-bab0-409f-afa0-de44e89620e0', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('1830730e-8fb3-4c18-bca5-e85c17f823d7', 'b4da2e8b-9cf0-4d8d-b599-d44322238b52', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 14:32:34', NULL),
('1965f954-89d2-4564-b309-4012629d8f33', '30d1d1e4-46e4-44e3-981b-1341a9c9850c', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:22:01', NULL),
('1be5c366-fba8-4652-aded-8fe1b456cffd', '757a3246-2080-4bf3-97da-f44c2c0a35e9', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 2, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 09:35:09', '2021-03-02 09:35:16'),
('1c0544cf-368d-4338-b3cf-c39c1b6e6ce3', '7796f229-a335-4899-b798-6d04b6a561f1', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 13:48:06', NULL),
('1dba1bf3-2891-4d65-a1e1-d34e37240c4f', '48989843-a161-4703-a6c3-ec05c950e0b9', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:53:46', NULL),
('1e3d7f39-f55a-43a4-b750-457a7ae6f36b', 'cd0352dc-22d5-448f-aa35-01228b399975', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-11', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-11 14:33:03', NULL),
('1ee301f3-5043-4570-a1a2-8f7494ff790e', '5206afcb-4178-48f6-9610-b0f8f8d32b57', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:27:17', NULL),
('1ff5643f-894f-45d1-84ca-c0fef5a89c17', '764f677a-9717-49fe-a824-0e82c5900e0f', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-04', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 07:34:09', NULL),
('20b4ac8a-662d-4636-ae51-a4588c9a2128', '5206afcb-4178-48f6-9610-b0f8f8d32b57', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:27:17', NULL),
('2100ab2b-b5e6-4247-8df3-7d23de4fba31', 'ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 1, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 05:54:22', NULL),
('2392d395-da55-491b-ab44-07d301726106', '0cdfe298-8d3e-4a93-b2c8-876e46379cf6', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-19', NULL, NULL, NULL, NULL, '1234567890qwerty', 1, '2021-03-19 15:10:39', '2021-03-19 15:12:24'),
('241eb157-e3b4-4ed4-aec3-18cc189968cb', '4ee13ed6-1993-4c01-851e-b9bd334f9d32', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:19:20', NULL),
('24dbf928-83fb-4e87-8517-5fcbb38cfdbe', '710d1c69-19f7-4f52-b907-b11e75382fc5', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 13:46:09', NULL),
('24fb7d30-c654-4461-bb12-4056f861b7ed', 'b6dabb25-3431-4e7a-9f10-d91f80ff8676', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-06 06:20:13', NULL),
('2590dad6-d40c-494f-86f1-560bfdc5bed4', '6966f404-40b4-48e8-92a1-912044e08c08', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-03-22 12:31:07', NULL),
('25c75ebb-7f4d-44ca-8b71-cf861fedee73', '2c614a47-f108-46f7-9150-12ee3028caf0', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-03-30 05:53:51', NULL),
('267c4dba-b48a-4e3c-8367-c789f8b45b2a', '4d1d3435-fc02-4757-8e21-80c737079f87', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 10:19:11', NULL),
('279d48c2-de47-44cc-a705-16d279d157a9', '44f85307-9c14-4701-9ed4-972fb483dbf1', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:32:39', NULL),
('28fd261d-6009-4222-b69a-1d885b991cf6', '54f74e66-d82d-47cc-a8a5-4049d0a40b53', 'fc285538-b95f-4b04-982e-9728852681ec', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 05:55:24', NULL),
('29016b15-402c-4c5e-82ae-fb5cbe8e1036', 'e8ddd62c-d9d5-4d1a-8e78-fb02e8e0fe2e', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:32:01', NULL),
('299e06f7-b286-4fe0-8cfc-1ad1726cc94d', 'f6d2f872-044f-4e82-9ac5-d541f15c013d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 05:47:48', NULL),
('2b1b3e91-5f68-4a47-a449-3990b91f2d5b', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:25:08', NULL),
('2b83d9df-7132-4b36-8e65-3b2bd2f81b36', '67c65018-b0a7-4830-a2e5-e2296939bbbb', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 08:24:38', NULL),
('2c932f62-f1be-4ef1-9fcf-8aed91c21bf5', 'e86e1b9d-752f-4c19-a391-58d3a0987744', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-11 11:59:10', NULL),
('2cc262d8-709a-43f7-a4b5-1b79d34ce0cd', 'f6d2f872-044f-4e82-9ac5-d541f15c013d', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 05:39:17', NULL),
('2f5ba59e-6a88-40d2-95fd-543c5f226962', '0cbba92c-83df-4de3-980a-7c62edbb6950', '41491d6c-4bdc-430a-99ec-019fc1abf102', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 14:52:33', '2021-03-17 15:52:22'),
('2f6860ec-46e4-4684-b7d9-4ad7ef0ec5be', 'f654d0e2-7150-473f-adb4-592cb2fdd7d8', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:21:43', NULL),
('322a1d2f-9c34-43a5-9801-791cb1124864', 'a5d57082-d549-415f-8f7d-7ff8067a110c', '35f028ec-512b-4ff9-b077-8179c6eef588', 1, '2021-03-16', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 15:31:13', NULL),
('32b621ed-63e5-48d8-a3ac-177dff3859a0', 'b05a9493-d389-47b2-adcb-85d3865d9d96', '35f028ec-512b-4ff9-b077-8179c6eef588', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:17:44', '2021-03-17 12:47:46'),
('32f190ee-bdae-4a5b-a34b-f55ea827b518', '08f98fa6-0884-4c78-956a-7b3cddafba74', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 05:31:37', NULL),
('32f9c9a7-38eb-4403-9f9b-df07dd86d038', 'e86e1b9d-752f-4c19-a391-58d3a0987744', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-11 11:59:09', NULL),
('33b60af2-bfc8-4f9c-af38-a9b3ed85d9ff', 'a55a0763-f994-464b-a54e-4a8a86edd068', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:56:20', NULL),
('35b0abd5-f3b1-4490-b27d-bf5757fa2de8', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('363051d3-19be-4d69-a1c5-79bee00eb052', 'c0ddfd40-a818-42c8-a85c-74c7b38dfb6d', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:27:57', '2021-03-05 13:22:25'),
('3641785c-6b7f-4858-9f9e-49c202e36681', '781ba4c1-cc74-4c0c-b5a5-04720d9b95f6', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 2, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-18 11:35:00', NULL),
('36445d8e-9542-4040-b05b-ba56e1f22e20', '08f98fa6-0884-4c78-956a-7b3cddafba74', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 3, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 05:40:23', '2021-03-03 05:40:28'),
('37615e7b-7da4-4e78-bb1f-69ccb6bac969', '9d7c682e-162c-4fd3-a89b-a2bffdbeb263', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-06 14:26:09', NULL),
('3879412c-66c9-42ea-9b15-f4b09b4ec2de', '236bf34f-ea20-4b54-a47a-abf091f678de', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'орпо', 'парп', NULL, '2000-01-01', NULL, NULL, '2021-03-09 14:32:44', '2021-03-17 18:57:27'),
('38cd4492-99c7-46a4-abff-c67231f19af7', 'f860d45e-49fb-41c5-a364-df24b51344d7', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, 'Литвинково', 'Рере', NULL, '2000-02-02', NULL, NULL, '2021-03-17 08:41:25', NULL),
('38d02038-9b88-4d8f-8baa-c569c62a35a5', '8f16767d-367a-475c-a70a-3aff59a4353e', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 1, '2021-03-09', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 06:47:45', NULL),
('3967eacb-1ef3-48ca-8b13-386ee61ded8d', '2c9a38f7-e231-4700-98d8-a3b76ee0cd54', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 1, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 11:49:46', NULL),
('3a0db777-7f02-45ed-903b-a888e45689f7', '7927edeb-c148-45c2-981a-9a99c98c5efd', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('3ac550de-7145-4fd2-bc44-fca4fb80a3e9', 'a55a0763-f994-464b-a54e-4a8a86edd068', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 10:31:34', NULL),
('3b2db8ed-e9d3-49ea-9161-8407ea9ea4f0', 'c0ddfd40-a818-42c8-a85c-74c7b38dfb6d', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:52:18', NULL),
('3e109510-3425-47cd-a2ec-2bfb5bcf7b48', '08f98fa6-0884-4c78-956a-7b3cddafba74', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 05:36:10', NULL),
('3e4191ac-e230-450b-bccb-568a4f874909', 'f6d2f872-044f-4e82-9ac5-d541f15c013d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 05:39:16', NULL),
('3fb5ae2d-e703-4198-90a2-c891c76a23a4', '7927edeb-c148-45c2-981a-9a99c98c5efd', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('40812f70-2347-4899-9d00-26a2b392d2f9', 'ba07c729-01ef-4a7e-a70b-8e2a38d1c723', '0e2760d7-87ae-4477-841c-168ad557850c', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 06:56:00', '2021-03-15 11:24:49'),
('4174bd27-b2ce-4b73-9a25-736a606b3dd4', '1078d7ba-347f-44b2-8173-36eb9a3dccd3', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-19 10:48:25', NULL),
('420ebdce-1eab-4587-8296-201214d20886', '4d4a4fad-ce82-40b6-8a83-1f50f80bf75b', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:42:37', NULL),
('425ec352-104e-4756-8639-63152d20cf56', 'ba07c729-01ef-4a7e-a70b-8e2a38d1c723', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 06:56:14', '2021-03-15 11:24:50'),
('4285b7bb-90d5-4b03-8046-373713acc785', '2c4d4a77-4461-4f01-9172-eaf51fb4f5dc', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 11:39:39', NULL),
('42d9f70d-267f-4413-8c8e-eabb1875ea7a', '764f677a-9717-49fe-a824-0e82c5900e0f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-04', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 07:34:09', NULL),
('431c1b11-8886-4dcb-b1e8-0f7436065703', '1ffa2bc5-8258-455c-bae8-8874209bbe5a', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:33:28', NULL),
('43501b9f-f301-49d4-8f62-9a9b8ab13386', '7927edeb-c148-45c2-981a-9a99c98c5efd', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:38', NULL),
('4477546f-08a6-4e8d-a9df-b6b0c4cd7c9e', '4fead0ed-5e2a-4e02-ab78-8490b5b92701', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 09:10:28', NULL),
('45462d7f-2e47-4158-9722-4406ba13f49e', 'da65a3c9-e56d-4321-b54e-9ca5b36780d2', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'asdad', 'asdsa', NULL, '1111-11-11', NULL, 1, '2021-03-18 06:18:45', NULL),
('486ef7b4-0056-4419-a463-a053681ce7ed', '5aa6e79f-79d4-4074-b01f-9573712347cd', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 11:42:41', NULL),
('499f8ffb-d214-45ad-b3a5-33826fb911cc', 'f6d2f872-044f-4e82-9ac5-d541f15c013d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 05:39:44', NULL),
('49dd58f3-cf82-4db5-86d5-59a08fc3467f', '4fead0ed-5e2a-4e02-ab78-8490b5b92701', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 5, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 09:10:28', NULL),
('4af6c4f8-dd2a-4161-a815-77288005e773', 'e79760f7-42b8-4207-90a7-4df9a574ecb9', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-04', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 12:33:17', NULL),
('4bceff71-4636-4366-a5c6-f65d9a20d542', 'fc35d5d4-b047-4dca-8741-488502203e8b', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:09:42', NULL),
('4cba2e67-a999-4de8-962f-77746917005a', '8eecc6e2-8df0-4f8c-b0b1-a8ac5ecf78b4', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 13:37:29', NULL),
('4cc72c93-d882-48d5-a65a-385d7895356c', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:25:11', NULL),
('4d6ec293-4bad-4691-b3e6-c8549a8f592e', '4fead0ed-5e2a-4e02-ab78-8490b5b92701', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 09:10:28', NULL),
('4d85c334-5dcf-4e86-8e8c-b78d144b750b', '44f85307-9c14-4701-9ed4-972fb483dbf1', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:33:49', NULL),
('50b79443-d0ef-4219-8f64-22537c1db96c', '89b9f830-c5df-431a-bdf7-7cb46f29acb8', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 10:59:26', NULL),
('517657d4-b608-4c88-b817-d37ddabc2c21', '88aba333-0b1c-428a-b449-7481bbaf1b12', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:23:09', NULL),
('53100a4b-6a23-4d0e-9001-000f3dcb0a01', '30d1d1e4-46e4-44e3-981b-1341a9c9850c', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:22:01', NULL),
('53e6c0b7-00ba-4972-bf4c-c19ffeefabb0', 'f860d45e-49fb-41c5-a364-df24b51344d7', '35f028ec-512b-4ff9-b077-8179c6eef588', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 08:41:25', NULL),
('53f72f8f-b7c1-42b9-b6c0-848577d235ac', 'd01b0267-a276-451b-8997-eb95dc5561b5', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'test878', 'test', 'test', '1996-11-21', NULL, 1, '2021-03-24 06:55:24', NULL),
('543cdcc0-dd42-4ff1-9b9e-7abc705dcdb9', 'a55a0763-f994-464b-a54e-4a8a86edd068', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:01:07', NULL),
('56e2b81f-78fc-46c3-a0ee-1b7c5ee4c22b', 'a16e387d-763f-45d2-aa17-03351a4d90c7', '002c4d13-da17-45ff-8479-e4761fb738dd', 2, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-06 14:25:52', NULL),
('58a08e0f-1302-45c2-8463-991767121699', '2632f176-246a-4ede-b4e6-9e7fe923210e', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-19 10:27:27', '2021-03-19 11:11:02'),
('58f1995e-802d-48cd-9d64-2059c43659ec', '91be7dba-0f2b-4042-b5c1-2a08644e407c', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 9, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-26 13:40:47', '2021-02-26 16:16:52'),
('59930afe-6c33-4515-b3e2-5c9d3a903b76', '6966f404-40b4-48e8-92a1-912044e08c08', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-03-22 12:30:39', NULL),
('5b5bf2bc-818c-45bb-98dd-0e90d4d77c04', '91be7dba-0f2b-4042-b5c1-2a08644e407c', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-26 15:02:09', NULL),
('5bea42e0-3a89-40a3-b16c-e2a1f4581d8f', '7f07943a-2571-434a-909d-9ac4cde665ab', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:54:17', '2021-03-05 12:58:37'),
('5ef135e0-d513-46fb-a961-875a59d030ea', '11583b4c-ff8a-4bbc-baf5-63b24ed9cade', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-19 04:55:27', NULL),
('603236e2-c7e1-42d5-8ce6-fac195bfe5a8', '236bf34f-ea20-4b54-a47a-abf091f678de', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'лддод', 'биб', NULL, '2000-02-02', NULL, NULL, '2021-03-09 14:32:56', '2021-03-17 18:57:28'),
('618d041f-5548-4a40-9c65-06dedd28cdd2', 'c0ddfd40-a818-42c8-a85c-74c7b38dfb6d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:53:41', NULL),
('637cbe50-3c54-4d67-a942-f57374914d0d', 'c63a2d41-251b-4f77-b98e-b68c519d7ea0', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-13 19:54:16', NULL),
('64c06558-2e83-451d-bc20-1dac643a97d0', '266a6383-b1d6-43aa-950e-03ad946a138c', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:51:44', '2021-03-18 11:52:01'),
('65b8ca1b-0c29-4183-8d00-9d514e5424dc', 'a55a0763-f994-464b-a54e-4a8a86edd068', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:00:56', NULL),
('65db2f71-d849-4743-a63c-a18c0daf27b9', '5cd7946c-2cd4-44ea-a61c-49b99a1df275', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 13:01:49', NULL),
('67ff8b85-680f-4194-9029-3def7b687204', '45d6796b-84b1-42c8-b57a-6edfb595bfe9', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:25:22', NULL),
('67fffcc9-e764-4e80-b6a2-0d5f97b02f4d', '496eacac-aa53-4571-af7c-8c20d33c9775', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 05:43:28', NULL),
('68bcd5b6-09ac-4920-83c4-b17c82545990', '710d1c69-19f7-4f52-b907-b11e75382fc5', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 13:46:09', NULL),
('6a2abdc9-4b0a-4aee-9200-26eea6735600', '08f98fa6-0884-4c78-956a-7b3cddafba74', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 05:36:51', NULL),
('6a70f778-71f5-409d-b502-c8b83503d126', '346f838c-bb0c-49b3-afa6-10362da58102', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-12 13:06:58', NULL),
('6bb1c72f-9a2c-4b1d-97f6-074d597d24f1', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('6cb55c94-89b5-4221-b0c6-6ab0d0e87907', 'd655d786-30db-46be-8e67-3951f041ef8c', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:46:24', NULL),
('6d95e0cb-c623-4538-839c-9cf8651fcb67', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:25:10', NULL),
('6e216b6c-1d51-4468-bdda-e9f4e2cd1929', 'cb07f9ba-a50f-4110-8d3e-d30a8eca52ed', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 12:59:25', NULL),
('6e9050e3-ecbb-4378-be69-bf819b2ff57e', '93738bc1-b24d-4e76-9cd2-8d3cdbb32ace', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 06:08:59', NULL),
('6ef77e99-cb58-4d15-907c-515d1c479845', '0678e00e-0d71-4753-80bb-773cf956e92f', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:53:19', NULL),
('6f5e693d-c1bf-433c-ac1f-38293c7d602f', '2c614a47-f108-46f7-9150-12ee3028caf0', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-03-30 05:53:51', NULL),
('70e73d38-6fa1-4b5d-b786-ed6b7a2ae074', 'cba4b2b0-4784-4b74-8dff-c6ecf1a132d6', '002c4d13-da17-45ff-8479-e4761fb738dd', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-12 09:53:18', '2021-03-12 10:17:44'),
('71b20730-a376-4b40-9717-8c7c4d1d076c', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('72a85eff-d79d-430e-9774-9226eced7526', '3299b76b-7911-461b-8567-9ac4b1230871', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, 'Литвинково', 'Рере', NULL, '2000-02-02', NULL, 1, '2021-03-23 11:53:42', NULL),
('745769ad-570c-43d2-8e33-6295f27d145e', '5065b027-f88b-45bb-9f83-3b09dfec7a2b', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 08:27:27', NULL),
('74663256-c403-4d5d-92d5-93f10d53870b', '30d1d1e4-46e4-44e3-981b-1341a9c9850c', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:22:01', NULL),
('75cc57de-8378-40e4-a31b-642ba86fd5aa', 'f6d2f872-044f-4e82-9ac5-d541f15c013d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 05:44:55', NULL),
('763a3520-d582-49dd-88f5-c8b697513d62', '11583b4c-ff8a-4bbc-baf5-63b24ed9cade', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-19 04:55:27', NULL),
('76e7c02f-4be4-4d7c-9291-b76c85ccdf34', '5cd7946c-2cd4-44ea-a61c-49b99a1df275', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 13:01:49', NULL),
('77518ec0-1261-4715-a23e-6f68264371c1', '4c7a9558-7fc8-41da-a2d1-8822590003f3', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-06 06:52:08', NULL),
('7815c665-61d1-4ab6-aa6f-615ffcdac560', '7927edeb-c148-45c2-981a-9a99c98c5efd', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:40', NULL),
('78542377-3fbf-43a4-b12d-38f56d7fbe30', '2632f176-246a-4ede-b4e6-9e7fe923210e', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-19 10:16:12', NULL),
('7af2da95-d302-4a23-b8ff-00dff31eb25b', '56c080bf-b923-4788-b57e-9e7a8247fe4a', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:37:55', NULL),
('7cf24864-9589-443d-ae14-ec1c6e1efc8d', 'bb4fed66-ddc7-4831-bad3-6cf7b126bb60', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 04:59:25', NULL),
('7d367d03-93b4-417a-b2be-a039978db543', '18ffb854-85eb-4032-95c2-8a7b97e47d65', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 11:32:06', NULL),
('7d49f3dc-eaa0-46c5-8991-b83bdf72fb7c', '1aa2e2ef-b3cd-47a9-8d1a-72cfbc47aed4', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 10:43:14', NULL),
('7dfe7930-b044-4333-a2bf-e90f668b50f8', '4d1d3435-fc02-4757-8e21-80c737079f87', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 5, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 10:19:11', NULL),
('7f55b7a1-d4e7-4598-8dc2-d5f69764371f', 'ece21266-29e0-45b1-871b-c5523fa80735', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 11:17:16', NULL),
('7f8d7810-4154-48fb-97de-c8865b60ff03', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('81dd001c-5c5a-4774-8d6e-821942c682d3', '44f85307-9c14-4701-9ed4-972fb483dbf1', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:34:21', NULL),
('8286be41-2bc5-4d90-b4a4-816fa194295a', '3dedc94d-4289-47c3-8167-1c6ac841fa73', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 12:03:23', NULL),
('82d1ed52-0be9-4c3e-974a-cbea13dbe766', '5a1c18ce-3524-426b-a138-18daa760e208', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 13:37:06', NULL),
('834d19dd-361c-40ba-aa2a-02e214ee308d', '38b9d95e-6c33-460b-9e8e-c36f40f37e42', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:35:43', NULL),
('840c728a-65d5-4e6e-8d4a-51c2e0e243a9', '7927edeb-c148-45c2-981a-9a99c98c5efd', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:40', NULL),
('85da5d69-da7d-4c67-ac7d-00ae111b6155', 'd0630f3a-959f-4a83-9c18-ee946cf60495', '35f028ec-512b-4ff9-b077-8179c6eef588', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 11:08:24', NULL),
('88006173-70c6-4e67-92d6-d5944d53734e', '0157f8e1-ac1d-4d84-8294-2e8ae466dc49', 'fc285538-b95f-4b04-982e-9728852681ec', 2, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 14:38:49', NULL),
('88501ded-2db0-4ab0-9b7c-d51fad590194', '7796f229-a335-4899-b798-6d04b6a561f1', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 5, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 13:48:06', NULL),
('893efa39-0581-42b2-8bd9-770707efd353', 'f6d2f872-044f-4e82-9ac5-d541f15c013d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 05:40:30', NULL),
('89c3dedf-638a-42a7-8d3e-bc00775ad6ca', '4d4a4fad-ce82-40b6-8a83-1f50f80bf75b', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:41:58', NULL),
('8a89e80d-e268-4084-a327-4ee1b3497a7b', '764f677a-9717-49fe-a824-0e82c5900e0f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-04', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 07:33:06', NULL),
('8ab5e981-119f-4fcb-93f4-9c29c728f7e0', '44f85307-9c14-4701-9ed4-972fb483dbf1', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:33:49', NULL),
('8bd7d2b8-991f-46e4-8061-e11336e2e1b5', 'a55a0763-f994-464b-a54e-4a8a86edd068', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:00:46', NULL),
('8bd8c513-78e8-4148-aa45-aeb32f2b894a', '8d47c920-bef6-483f-b624-d7267217ca9f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 11:33:43', NULL),
('8c0a51fc-a923-4694-9f3d-b6a15bfa3016', '3299b76b-7911-461b-8567-9ac4b1230871', '35f028ec-512b-4ff9-b077-8179c6eef588', 1, '2021-03-16', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-23 11:53:42', NULL),
('8c0b5bd8-6454-4a90-99d2-321fbece44f1', '2c614a47-f108-46f7-9150-12ee3028caf0', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, NULL, 'dfdf', 'dfdfd', NULL, '1111-11-11', NULL, 1, '2021-03-22 05:45:45', '2021-03-23 05:16:17'),
('8c6ea5d7-88bb-4eab-a099-6e936179cb1d', 'c0ddfd40-a818-42c8-a85c-74c7b38dfb6d', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:51:06', '2021-03-05 12:53:13'),
('8d052e05-2b5b-424c-8805-120c4cb087f2', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:25:11', NULL),
('8fb543d3-837f-4ea7-903b-065de226aba1', '0cdfe298-8d3e-4a93-b2c8-876e46379cf6', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, 'Пушистенький', 'Котейка', NULL, '2010-02-02', NULL, 1, '2021-03-19 15:10:39', '2021-03-19 15:12:25'),
('90f23595-611c-499b-9449-f6965d64384f', 'aaa4d380-de2e-4424-8c15-b1dc8b586260', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-19 10:43:52', NULL),
('91b731a8-dbd1-43ac-b00a-19df5a7becea', 'ba07c729-01ef-4a7e-a70b-8e2a38d1c723', '41491d6c-4bdc-430a-99ec-019fc1abf102', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 06:55:31', '2021-03-15 11:24:51'),
('92299c03-fc0d-4ebc-a000-887e53fbaeaa', 'b7358a3f-0e84-4e8b-a5cf-95a20c3aa9f5', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:06:26', NULL),
('92385fa5-f82c-4151-a30f-43629ad834e6', '4794c548-e841-4615-8e1e-cb0886fc55cc', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 14:31:18', NULL),
('92f310f0-a0f7-4a39-8e4c-d5435bb1c7b4', '7927edeb-c148-45c2-981a-9a99c98c5efd', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('94d78ebd-d7d2-498f-90ce-abaea5496e1c', '7796f229-a335-4899-b798-6d04b6a561f1', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 13:48:06', NULL),
('95e8bb43-291c-406d-8d63-95d50c68e82d', '88aba333-0b1c-428a-b449-7481bbaf1b12', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:23:09', NULL),
('95f60449-ab70-44be-a0af-632634c388ec', 'c5935b9b-332c-4ccb-8bbe-90ef95fcf655', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 4, '2021-03-01', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-01 08:50:31', '2021-03-01 08:53:17'),
('961d3340-9900-4f1c-84b5-ee791ff5455e', '2c9a38f7-e231-4700-98d8-a3b76ee0cd54', '41491d6c-4bdc-430a-99ec-019fc1abf102', 1, '2021-03-17', NULL, NULL, NULL, NULL, '1234567890qwerty', NULL, '2021-03-15 11:49:37', '2021-03-16 14:15:35'),
('96cc8a6f-e5b3-46b6-8783-26f2bb11240b', 'cba4b2b0-4784-4b74-8dff-c6ecf1a132d6', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-12 10:20:15', '2021-03-12 10:50:36'),
('973e0819-a541-4c50-b210-58712ffaa7f9', '346f838c-bb0c-49b3-afa6-10362da58102', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-12 13:06:58', NULL),
('9876ca88-e172-4f08-9f4b-0afe8ad28961', 'a55a0763-f994-464b-a54e-4a8a86edd068', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 10:31:34', NULL),
('98e2bc15-d954-49d1-8c80-bf3e7ce7ee17', 'c38bd8a1-68f1-4ba3-9364-3c2de50ab74e', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 09:57:17', NULL),
('9a1ffb53-f7fd-4cb6-bec2-ff137fa6c7c6', 'd0630f3a-959f-4a83-9c18-ee946cf60495', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, 'Ноис', 'Илли', NULL, '2000-02-02', NULL, NULL, '2021-03-17 11:08:24', NULL),
('9aefaddc-8716-4931-a7e1-e3039f6fe04a', '8e5fcfa6-7c3e-41d6-b1f0-2ec671807b75', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 06:06:53', NULL),
('9b721b5b-93ed-4094-b028-c714ba63ad5f', '4d4a4fad-ce82-40b6-8a83-1f50f80bf75b', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:41:58', NULL),
('9ba0af2c-fae0-463e-b534-0bf6547fa71d', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('9c2fa72b-bd11-4fbf-9930-d2155e430317', '266a6383-b1d6-43aa-950e-03ad946a138c', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:51:44', '2021-03-18 11:52:02'),
('9db89359-0f06-4683-8b69-2c71f2037e79', '88aba333-0b1c-428a-b449-7481bbaf1b12', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:23:09', NULL),
('9e5e5d6c-4f9a-45a3-940f-dce9c6ae7e76', '68e5bdaf-bdf0-4464-8261-7782edf144b1', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:32:09', NULL),
('9ee71df3-e95a-4d3b-a1fc-2faa74731351', '30d1d1e4-46e4-44e3-981b-1341a9c9850c', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:22:01', NULL),
('9f8e5e8a-1784-475b-834b-4136faec1ccb', 'ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 05:54:23', NULL),
('a06dadd5-2e5b-480d-b18f-0c07db9a66ac', 'c63a2d41-251b-4f77-b98e-b68c519d7ea0', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-13 19:47:25', '2021-03-13 19:52:12'),
('a0e6cd10-f454-482a-b778-7e94128cfdc6', 'fe415fc1-5ed7-4fbd-9932-52c037335d88', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 3, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:06:33', NULL),
('a31de76c-03b5-45b7-993c-adbc930b57df', 'ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 05:54:23', NULL),
('a3a28953-26cc-4957-aa06-1272b6891926', 'b05a9493-d389-47b2-adcb-85d3865d9d96', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 1, NULL, 'рпоро', 'прпо', NULL, '2000-01-01', NULL, NULL, '2021-03-17 12:16:53', '2021-03-17 12:47:46'),
('a47d868d-744b-45d2-96b8-3bfccc623465', '91be7dba-0f2b-4042-b5c1-2a08644e407c', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-26 15:11:58', NULL),
('a4d4cbc0-a7bf-4622-a554-4b558de05121', 'fc35d5d4-b047-4dca-8741-488502203e8b', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:17:11', NULL),
('a4dc8b0b-dd03-455a-a945-899ede3ddc1e', 'd9042a11-2ec9-4ddb-b1b5-cbab439e06b2', '0e2760d7-87ae-4477-841c-168ad557850c', 1, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-06 14:38:18', NULL),
('a526b056-83da-4d60-8711-d47e320b2615', 'e18e40b0-4f66-455f-8e0b-27667be9caec', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-24', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-24 07:00:00', NULL),
('a705fdc3-0f68-4aff-9c82-11337d0b2e39', '7818071e-f388-467f-8276-df1e22fd126d', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:44:43', NULL),
('a71a800a-74a9-434c-bc04-c5888930a192', '1cf1c1a3-9568-46d1-8c29-1c2d0595d895', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-18 11:49:17', NULL),
('a7a4bb5d-1f48-46bb-b29c-7912361f96e8', '91be7dba-0f2b-4042-b5c1-2a08644e407c', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-26 15:43:32', NULL),
('a7d5044d-d126-4af6-ae06-eb919b57aeb1', '266f707f-3a97-4c67-aaf6-be75bef52644', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:17:07', NULL),
('a8834b81-60df-4880-84d4-e59a5bb6a835', 'b05a9493-d389-47b2-adcb-85d3865d9d96', '35f028ec-512b-4ff9-b077-8179c6eef588', 1, '2021-03-17', NULL, NULL, NULL, NULL, '1234567890qwerty', NULL, '2021-03-17 12:17:02', '2021-03-17 12:47:47'),
('a9241c01-671f-4b41-80d8-984b761134af', 'f6baa61a-1e18-437b-bf0b-81c60ea941c7', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 05:59:50', NULL),
('a94f1606-8a28-4f27-a5e4-04492d899536', 'f39d1750-52b0-407e-a41b-5d6cdde4c07c', '002c4d13-da17-45ff-8479-e4761fb738dd', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-11 05:47:00', '2021-03-11 06:34:02'),
('a952b993-01c2-4a67-b253-a3725f8140a3', '15274821-8581-4057-8d96-118059f26cd2', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 10:39:32', NULL),
('a98c03a0-52da-4fd4-8777-d347f7cb5713', '6896e8a0-55b6-4ae0-8cdd-555398653ebd', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 12:26:55', NULL),
('aa5063fc-9c6b-4498-9cd9-84a8331a3a02', '8c2958fd-15b7-4827-9753-00e8527ec627', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-18 06:08:02', NULL),
('ab41514d-9f48-4d73-b94e-b52a22d220bb', 'bb154f80-624c-473e-9e03-f301b4029cfc', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:48:29', NULL),
('ab810172-cb41-4cbe-ad2a-365a45eb4e60', '4d1d3435-fc02-4757-8e21-80c737079f87', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 10:19:11', NULL),
('abe9d380-ed07-4ce5-81e8-0f97e042f04f', '0d68beb8-6341-4952-adc7-38351fa91e2d', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 2, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:59:18', NULL),
('add6cd8f-35a2-4669-affd-da02692248bb', 'a65a1b54-07db-40e3-af21-e81346bd1f0d', '002c4d13-da17-45ff-8479-e4761fb738dd', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:43:35', '2021-03-05 15:48:39'),
('ae8ee239-28b8-43fb-a80e-fae81585406f', 'b05a9493-d389-47b2-adcb-85d3865d9d96', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 3, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:17:21', '2021-03-17 12:17:33'),
('aecd80df-6d77-4f89-92db-b9ea19ef4197', 'a55a0763-f994-464b-a54e-4a8a86edd068', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:56:20', NULL),
('b04a5337-1d03-435a-becf-52745a5f0502', '645d5479-88c9-4264-aa2c-8dd461b37d3c', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-11 14:18:12', NULL),
('b09d550a-a900-443b-9ef8-c8b2bd7ef5c8', '266a6383-b1d6-43aa-950e-03ad946a138c', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:51:44', '2021-03-18 11:52:02'),
('b0b8dae5-495b-4baf-a907-9077b5a6f9f6', '44f85307-9c14-4701-9ed4-972fb483dbf1', 'fc285538-b95f-4b04-982e-9728852681ec', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:32:49', NULL),
('b12d24e8-35aa-4554-bee4-45c2d4c09e2e', 'fc35d5d4-b047-4dca-8741-488502203e8b', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:09:30', NULL),
('b1876232-cb44-4d4d-9444-3ba812f7829c', '7796f229-a335-4899-b798-6d04b6a561f1', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 13:48:06', NULL),
('b1f58d8d-5cad-46b0-9d2a-1f6fade51b03', '0f5cf68b-2145-49d7-b0e7-5482042a63b7', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 06:50:41', NULL),
('b2d11c3b-363e-4275-9d15-29013f052e26', '7927edeb-c148-45c2-981a-9a99c98c5efd', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('b3173132-cb6c-402f-925e-252474bc3296', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('b3b83d42-df55-49d6-83c9-5ddbcd25389d', 'e2546eb7-682a-4889-a080-3e3980919d7e', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:05:29', NULL),
('b5265b36-b480-44a2-983d-88d7cc2c3534', 'acb59baf-a1be-4f00-af1e-5e5d34866723', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:46:28', NULL),
('b5479d46-01bc-4d1c-a158-073439e0126d', '619b67d7-0db6-417c-98e1-69f57282faad', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 12:00:58', NULL),
('b5dadd71-4da9-41cf-9cd9-0a3820d50ca0', '44f85307-9c14-4701-9ed4-972fb483dbf1', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:34:21', NULL),
('b67b5eb3-f649-4f03-b4c2-517addf53c7b', '88aba333-0b1c-428a-b449-7481bbaf1b12', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 5, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:23:09', NULL),
('b7554613-7876-4eb3-be25-047f6e4aa09f', '281b1179-b2a6-491a-b251-54649a7e716f', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'sdfsdf', 'sfsdf', NULL, '1232-12-12', NULL, 1, '2021-03-18 06:21:19', NULL),
('b8153dd5-25af-4f85-a52d-6a227df64002', '4be976e8-fc4d-4209-8404-9182fb851c8a', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 12:53:55', NULL),
('b98b59c3-7577-421e-9201-2c435a7799fc', 'ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 05:54:21', NULL),
('bbd9bdd8-da9c-4a2e-8572-e28462e20b82', 'ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', '41491d6c-4bdc-430a-99ec-019fc1abf102', 1, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 05:54:22', NULL),
('bc7b9091-2a6d-40c9-afe0-19fbe7095f87', '44f85307-9c14-4701-9ed4-972fb483dbf1', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 09:32:39', NULL),
('bc89db76-39e0-4a8c-8d29-725e2600908d', 'd0630f3a-959f-4a83-9c18-ee946cf60495', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 11:08:24', NULL),
('bd7d9735-8c43-43e1-9001-458f7e208c1c', 'ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 05:54:22', NULL),
('bee76b3d-91a7-41fe-8e3c-e8f18a960c94', '0cdfe298-8d3e-4a93-b2c8-876e46379cf6', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-19 15:10:39', NULL),
('bf792d99-9f34-4900-871d-b8c4136313fd', '48a9da31-2774-44e1-9596-3e36c1c74afd', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 3, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:24:30', '2021-03-05 15:24:46'),
('c24cf01c-02fb-4396-b1e6-d13462b20bd8', '7927edeb-c148-45c2-981a-9a99c98c5efd', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:38', NULL),
('c452fcd0-227c-45c6-8f65-b11e23e92f45', '5dfe119b-e52f-4909-9848-bc21af669422', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 14:32:44', NULL),
('c476a42d-4efe-45c7-85de-fb111e67cb58', '2dc8903b-7580-4f6c-adc1-349c910f9012', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:59:11', NULL),
('c4f25589-04f1-41b4-bd02-d1011d9c3ff3', '92b08fac-32d8-48fd-92dc-9d063d98865a', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 06:54:24', NULL),
('c72a9138-3f63-4d07-9a34-d2e35aaf6e8f', '862d175c-0d77-4ecb-9e22-1d3117171ddc', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 1, '2021-03-16', 'Литвинкова', NULL, NULL, NULL, NULL, NULL, '2021-03-16 15:31:39', NULL),
('c9b80238-e9c1-416d-a284-93f0028626e8', '50c4b62a-6a35-4036-98cd-c42aa4f731f3', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:03:38', NULL),
('ca060322-ec9a-49af-899d-e6720994298b', '0cbba92c-83df-4de3-980a-7c62edbb6950', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, NULL, 'рапр', 'пвап', NULL, '2000-01-01', NULL, NULL, '2021-03-17 14:52:28', '2021-03-17 15:52:23'),
('ca4e83f7-f754-4008-a5d9-b9e8e881db5f', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:25:09', NULL),
('cb4987c0-4bad-4acd-b165-6e9c917ef2e8', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('cbf58353-2e4c-4a5c-943e-8b62913b6efc', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('cd36ac9d-08db-4997-bbd0-53a94e11169e', '2c9a38f7-e231-4700-98d8-a3b76ee0cd54', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, 'zxcvbn', 'asdf', NULL, '1999-01-01', NULL, NULL, '2021-03-15 11:49:31', '2021-03-16 14:15:36'),
('cd69fe8e-a113-4e5f-8ec0-5924403b7785', 'eb1a75b4-8274-48b6-b668-b79c5ae2cd0f', '17ec8006-84b6-41e7-aedf-349b83efcca1', 7, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 05:50:24', '2021-03-05 05:52:21'),
('cd7e7bbb-ca56-405d-ac8e-60677c953627', 'be67f4c1-f163-490f-9462-7a192cc0e7c2', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'Лалетин', 'Алексей', NULL, '1989-02-22', NULL, NULL, '2021-03-15 13:02:35', '2021-03-16 12:00:55'),
('ce0b5510-4c95-4aeb-adb5-589df3757fde', 'a55a0763-f994-464b-a54e-4a8a86edd068', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:56:20', NULL),
('ce30be66-f33f-4f5b-8080-46db46d79163', '2c614a47-f108-46f7-9150-12ee3028caf0', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'dfdfd', 'dfdfdf', NULL, '1111-11-11', NULL, 1, '2021-03-23 04:58:50', '2021-03-23 05:16:18');
INSERT INTO `cart_item` (`id`, `cart_id`, `product_id`, `quantity`, `activation_date`, `last_name`, `first_name`, `middle_name`, `birth_date`, `card_num`, `is_new_card`, `created_at`, `updated_at`) VALUES
('cef66242-964e-499c-b4ed-7e912ae8f205', 'f860d45e-49fb-41c5-a364-df24b51344d7', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 1, '2021-03-17', 'Литвинкова', NULL, NULL, NULL, NULL, NULL, '2021-03-17 08:41:25', NULL),
('d0284c32-9baa-4a24-b7aa-ae47e3a8ff56', '0157f8e1-ac1d-4d84-8294-2e8ae466dc49', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 1, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 14:38:49', NULL),
('d064d169-9bf1-4abe-ad5a-26d79bcf2e2e', '236bf34f-ea20-4b54-a47a-abf091f678de', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-17 18:46:06', '2021-03-17 18:57:28'),
('d0744ecf-37fb-4250-a96d-8981100fa614', '2c614a47-f108-46f7-9150-12ee3028caf0', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-23', NULL, NULL, NULL, NULL, '1111111111111111', 1, '2021-03-23 04:58:47', '2021-03-23 05:16:19'),
('d27a6a35-a325-4141-a4c3-1f44a9c441f3', '2c9a38f7-e231-4700-98d8-a3b76ee0cd54', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 11:49:50', NULL),
('d43714b7-3d3e-43f0-94d9-cc32806b6a64', '346f838c-bb0c-49b3-afa6-10362da58102', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 1, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:25:08', NULL),
('d5b3b996-ef68-46a7-9ec6-77e6b704732d', '814296cb-6785-4805-bd1a-118897f7ef6c', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 14:14:58', NULL),
('d6704f87-5675-4c08-b68a-3058adb45fa8', '516c89bc-6a88-4d54-8ecf-b72d84df6b0c', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 08:14:36', NULL),
('d7098b12-d41a-49d4-9442-705b06673a55', '7f07943a-2571-434a-909d-9ac4cde665ab', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:54:14', '2021-03-05 12:58:37'),
('d75b665b-856e-4b08-913f-2a6dd20987d6', 'e79760f7-42b8-4207-90a7-4df9a574ecb9', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 12:33:18', NULL),
('d7f14ef6-4566-4939-bdca-da2e896a7f2d', 'c0ddfd40-a818-42c8-a85c-74c7b38dfb6d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:43:44', NULL),
('d8c6cb07-d848-4d9b-80c7-322495049109', '30d1d1e4-46e4-44e3-981b-1341a9c9850c', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 3, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-18 11:21:57', NULL),
('d932a9b2-3a88-4ffa-a313-c5c21b8b211c', '3299b76b-7911-461b-8567-9ac4b1230871', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 1, '2021-03-16', 'Литвинкова', NULL, NULL, NULL, NULL, 0, '2021-03-23 11:53:42', NULL),
('d948fe23-76dd-4525-a586-cb0ecea5f2f3', '25cab061-5df1-4175-b427-99e62bdde996', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 08:22:31', NULL),
('da341401-285f-4792-9a8f-4642ad70c470', 'cba82a63-22eb-4d6f-aeae-75edf1068463', '002c4d13-da17-45ff-8479-e4761fb738dd', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-26 12:42:45', NULL),
('da424230-8092-42f3-a446-2cc0749750fb', '764f677a-9717-49fe-a824-0e82c5900e0f', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-04', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 07:33:06', NULL),
('dace6762-ebc6-4e94-841c-f7b3c47cd9ae', '496eacac-aa53-4571-af7c-8c20d33c9775', '0e2760d7-87ae-4477-841c-168ad557850c', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 05:45:17', NULL),
('db00dd82-25de-4242-82b7-4eb5d479a8b0', '7f07943a-2571-434a-909d-9ac4cde665ab', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:54:14', '2021-03-05 12:58:38'),
('dcedb9a7-1ec6-4988-9f6e-bb33ca930cfa', '0dee7234-860c-4cb2-a9da-b1a2d46c0ea4', '17ec8006-84b6-41e7-aedf-349b83efcca1', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-03-18 07:36:49', NULL),
('dd23837a-fda7-4c3e-a2b9-ccab84271162', '38b9d95e-6c33-460b-9e8e-c36f40f37e42', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:35:43', NULL),
('dd5e9bd9-3fea-4c7d-8cd6-0387637383a9', '98358954-b446-4c77-8087-10000b65a16c', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 1, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 0, '2021-03-19 04:55:27', NULL),
('df9fe12c-a8b4-4f71-8eaf-c5259cc04f9d', 'acbb3118-10ea-4af0-8a70-d919e423d2ec', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 08:03:38', NULL),
('e1f67d0c-8e68-4bb2-aeb2-3b939532ec57', '02a3cc10-e28c-4015-99bd-4e1bd63a48b5', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 11:37:26', NULL),
('e35c65bd-c810-4a9c-b36b-66057a0fb4bb', '4d1d3435-fc02-4757-8e21-80c737079f87', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 10:19:11', NULL),
('e3bc2a89-ecc9-4180-bf51-5f84010d2ed3', '5392291d-0ec4-4403-8c1d-bcb8a3b81356', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 14:48:16', NULL),
('e56818ee-4aa2-489e-ab19-e7ca0c0e07dc', 'dda49a3b-0fe6-4afa-9e26-92efeaa101f5', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:31:04', NULL),
('e7231c98-5401-482d-aa23-90e8b43c034e', 'e3b52bdc-1e92-4714-a7c9-193a89578581', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 12:07:30', NULL),
('e72f6270-920a-488f-a27c-c4f30246d861', '08f98fa6-0884-4c78-956a-7b3cddafba74', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 05:36:53', NULL),
('e82abffd-e99b-4997-a6d9-c3d9b2ff342e', '08f98fa6-0884-4c78-956a-7b3cddafba74', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 1, '2021-03-03', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03 05:38:54', NULL),
('e9a71883-5404-4d20-8e23-7e1a22e7aa37', 'ba07c729-01ef-4a7e-a70b-8e2a38d1c723', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 06:55:38', '2021-03-15 11:24:51'),
('ebc4f7ef-2d4c-4e13-9a06-e0221ac5be20', '5a1c18ce-3524-426b-a138-18daa760e208', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 13:37:06', NULL),
('ec0391b5-0536-4212-a2fc-72bb6e404d37', '496eacac-aa53-4571-af7c-8c20d33c9775', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 05:45:37', NULL),
('ec6fdd50-5d8f-4039-8cd3-1c0d8d224c8b', '0467e91a-da48-4c40-b566-26a204f5ef11', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-03-16', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 07:15:15', NULL),
('ed7c565c-7f99-4e92-95d1-cbaccfd67bc4', 'ebd90ff1-f8d0-40c9-8d42-d3661f1ead04', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-02-26', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-09 05:54:21', NULL),
('eff78f20-9b82-44dd-9f1c-4612ac7062bb', 'c0ddfd40-a818-42c8-a85c-74c7b38dfb6d', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 12:53:42', NULL),
('f21ebbce-52cb-403c-9811-548df6765f51', '7ae1f1e8-b8e5-4c5d-ad25-50e5b5d195ee', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:33:20', NULL),
('f33447a6-7b44-4f13-be99-41519a01d7a1', '54f74e66-d82d-47cc-a8a5-4049d0a40b53', 'e41cab29-37f3-4e05-916b-64ae00d6e0f5', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 05:55:24', NULL),
('f341533f-bca7-4ab2-ad98-37056193b8e2', 'f1c32aa9-1a3e-4e74-8b88-e3319fc17da8', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 14:35:29', NULL),
('f38299fe-c95d-486b-b309-c9641124bf26', '3dedc94d-4289-47c3-8167-1c6ac841fa73', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-04', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-04 12:00:26', NULL),
('f4311201-cbad-4749-8a11-4de0095fd0d9', '8e5fcfa6-7c3e-41d6-b1f0-2ec671807b75', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 06:06:53', NULL),
('f5175581-257a-4d58-a16d-e4167b533e9e', 'fd789617-f6e6-4fcd-b5b1-7b5a4d5a87af', '002c4d13-da17-45ff-8479-e4761fb738dd', 2, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-10 05:45:12', NULL),
('f587dd45-2eff-4b49-b085-d4f6f48e9396', 'a06d00c3-d47d-43da-af80-ff0f48c75c37', '906c9040-3d9f-4517-b16b-5df8115f16bc', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-19 13:26:26', NULL),
('f5c10935-5819-444e-bea9-c2e90492726d', '4fead0ed-5e2a-4e02-ab78-8490b5b92701', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-02', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-02 09:10:28', NULL),
('f633ff7a-ce45-4f3a-832c-26b0844bd88d', 'f39d1750-52b0-407e-a41b-5d6cdde4c07c', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-11 05:47:00', '2021-03-11 06:34:02'),
('f69f6121-6cba-4bba-b678-bd3612ce2521', '266a6383-b1d6-43aa-950e-03ad946a138c', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 1, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 1, '2021-03-18 11:51:44', '2021-03-18 11:52:03'),
('f6e0dcf6-4963-4df5-8fb4-a2702922f0d2', 'c67d6b66-2b1b-4460-a6c6-50c83bf3ce41', '002c4d13-da17-45ff-8479-e4761fb738dd', 1, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-06 14:48:53', NULL),
('f766eb23-1a1a-451d-b115-f4b1caf584a2', '496eacac-aa53-4571-af7c-8c20d33c9775', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-15 05:43:34', NULL),
('f92ef33c-8302-4d47-8f2d-a2be343e2ab8', '886ddeaa-c4da-43c0-adcf-bc73d3631579', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 2, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 13:42:06', NULL),
('fb4a825e-2a98-4d7b-9639-db5af3d16c2c', '7927edeb-c148-45c2-981a-9a99c98c5efd', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('fc0498bd-b299-4e3e-a15c-e5c3ff1440f7', '0380fe8d-19f5-4f79-a5e8-1cd9404bdbf5', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 15:22:44', NULL),
('fcb4a124-981a-463d-8582-b58a2f25d00d', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:35:26', NULL),
('fe3c4d6a-4e18-40c2-be4b-a7a95ce66df1', '9407ca11-be4b-44e8-9410-131a84b307ab', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, 'ываываы', 'ыаываы', NULL, '1121-12-21', NULL, 1, '2021-03-18 05:59:17', NULL),
('ff541a87-dc8d-4105-86ec-fe19b2639e8b', '7927edeb-c148-45c2-981a-9a99c98c5efd', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL),
('ffc452c7-4d64-4fe5-b6a7-f9cad9d05ca2', '44f85307-9c14-4701-9ed4-972fb483dbf1', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 10:34:33', NULL),
('ffc94f00-b959-415d-ad7d-d0d9d88a9358', '45d6796b-84b1-42c8-b57a-6edfb595bfe9', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 2, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:25:22', NULL),
('ffee8301-9f59-4230-aa5f-07bd0fd72e3a', '7927edeb-c148-45c2-981a-9a99c98c5efd', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 12:36:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_promocode`
--

CREATE TABLE `cart_promocode` (
  `cart_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promocode_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `name` json NOT NULL,
  `description` json NOT NULL,
  `web_gradient_color1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_gradient_color2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_icon_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `web_icon_svg` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `mobile_icon_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `mobile_background_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `has_sections` smallint UNSIGNED NOT NULL DEFAULT '0',
  `has_places` smallint UNSIGNED NOT NULL DEFAULT '0',
  `is_active` smallint UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `web_gradient_color1`, `web_gradient_color2`, `web_icon_id`, `web_icon_svg`, `mobile_icon_id`, `mobile_background_id`, `created_at`, `updated_at`, `has_sections`, `has_places`, `is_active`) VALUES
('010cba3b-658b-4a1e-9b06-87cb19b32653', '{\"ru\": \"Сувениры\"}', '{\"ru\": \"test\"}', 'E9EB00', '3F94DF', '97508267-2a56-4731-8d0a-6a29c6e92ac1', NULL, '97508267-2a56-4731-8d0a-6a29c6e92ac1', NULL, '2021-02-11 15:00:34', '2021-02-20 09:06:44', 1, 1, 1),
('01161fbd-5cdc-4434-a248-dc77a6865dac', '{\"ru\": \"Одежда и аксессуары\"}', '{\"ru\": \"test\"}', '3F94DF\r\n', '541F8C\r\n', 'cc5a5327-0971-408b-bad9-527d09e327bf', NULL, 'cc5a5327-0971-408b-bad9-527d09e327bf', NULL, '2021-02-20 06:49:52', NULL, 0, 0, 1),
('1659445b-4931-4192-8a1d-d9147ecd6f61', '{\"ru\": \"Пляжный отдых\"}', '{\"description\": \"some2\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:49:05', NULL, 0, 0, 1),
('1858d77d-42d7-4525-859c-18fa42c464d4', '{\"ru\": \"Все развлечения\"}', '{\"ru\": \"test\"}', 'FFFFFF', 'FFFFFF', '1ad411fa-ce62-4868-80c9-70e7a11e6b08', NULL, '1ad411fa-ce62-4868-80c9-70e7a11e6b08', NULL, '2021-02-12 07:02:59', '2021-02-19 10:37:21', 0, 0, 1),
('1ccc82be-353a-4508-b3b3-8dac36e92f99', '{\"ru\": \"Спортивное снаряжение и экипировка\"}', '{\"ru\": \"test\"}', 'E9EB00', '3F94DF', 'df5de035-6906-4c96-8cf4-a5baabdaaf28', NULL, 'df5de035-6906-4c96-8cf4-a5baabdaaf28', NULL, '2021-02-11 14:42:47', '2021-02-16 18:02:01', 0, 0, 1),
('1ec8a709-7906-485b-aaa6-e354f166abb8', '{\"ru\": \"Кафе на склонах\"}', '{\"ru\": \"test\"}', 'E9EB00\r\n', 'C20067\r\n', '58926f9e-befb-46e5-921a-02ccfd34f646', NULL, '58926f9e-befb-46e5-921a-02ccfd34f646', NULL, '2021-02-12 07:02:30', NULL, 0, 0, 1),
('1f4f2d43-280c-42a1-99ea-5dd4710e59cc', '{\"ru\": \"Бары, караоке\"}', '{\"ru\": \"test\"}', 'E9EB00', '3F94DF', 'c371afb4-2747-4a3e-af1d-bd877da8cc94', NULL, 'c371afb4-2747-4a3e-af1d-bd877da8cc94', NULL, '2021-02-11 14:52:22', '2021-02-11 16:04:29', 0, 0, 1),
('21498eb6-a32f-423b-aa9d-8c0b46a76062', '{\"ru\": \"Рестораны в отелях\"}', '{\"ru\": \"test\"}', 'E9EB00\r\n\r\n', 'C20067\r\n\r\n', '66b63612-c3a3-4dce-9932-727059ca2c07', NULL, '66b63612-c3a3-4dce-9932-727059ca2c07', NULL, '2021-02-20 06:51:19', NULL, 0, 0, 1),
('22100f0b-c9cc-4b75-8ebf-34c02f015ad8', '{\"ru\": \"Авторская кухня\"}', '{\"ru\": \"test\"}', 'E9EB00', 'C20067', '458b51b7-8d23-44b1-8163-552bd2c485ac', NULL, '458b51b7-8d23-44b1-8163-552bd2c485ac', NULL, '2021-02-20 06:48:50', NULL, 0, 0, 1),
('256a0d96-fb04-48de-98e1-0833619466f1', '{\"ru\": \"Пицца и бургеры\"}', '{\"ru\": \"test\"}', 'E9EB00', 'C20067', '176ef1da-a857-4c3f-8d1a-c8f323742cf9', NULL, '176ef1da-a857-4c3f-8d1a-c8f323742cf9', NULL, '2021-02-16 19:01:32', NULL, 0, 0, 1),
('31d36958-eb40-4919-b896-903a60e55f7c', '{\"ru\": \"Продукты, напитки, косметика\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 06:52:46', NULL, 0, 0, 1),
('38b00cbd-de1b-45c8-8fdb-f310cbd240cd', '{\"ru\": \"Семейные кафе\"}', '{\"ru\": \"test\"}', 'E9EB00', 'C20067', 'a29d549d-c47d-4bfa-88df-c6e6fdbbb4a3', NULL, 'a29d549d-c47d-4bfa-88df-c6e6fdbbb4a3', NULL, '2021-02-12 06:55:59', NULL, 0, 0, 1),
('39b234a1-9c34-4594-a015-2d48cd33344a', '{\"ru\": \"Пешие маршруты\"}', '{\"ru\": \"test\"}', '3F94DF\r\n', 'C20067\r\n', 'eb2f118d-4b0f-4d6b-8d8c-4a67a477a3aa', NULL, 'eb2f118d-4b0f-4d6b-8d8c-4a67a477a3aa', NULL, '2021-02-12 06:53:43', NULL, 0, 0, 1),
('39e3c13b-cfaf-4491-84fe-a9f06684ab7f', '{\"ru\": \"Маршруты с гидом\"}', '{\"ru\": \"test\"}', '3F94DF\r\n', 'C20067\r\n\r\n', 'd7e07948-d263-4421-97ba-119b7f3382aa', NULL, 'd7e07948-d263-4421-97ba-119b7f3382aa', NULL, '2021-02-16 19:02:44', '2021-02-17 20:50:56', 0, 0, 1),
('4018cca0-2ae9-4a15-be43-ba09fdd4ec02', '{\"ru\": \"Есть детская комната\"}', '{\"ru\": \"test\"}', 'E9EB00\r\n', '3F94DF\r\n', 'c8db13ec-8805-46d9-8074-3b0f5e4b1819', NULL, 'c8db13ec-8805-46d9-8074-3b0f5e4b1819', NULL, '2021-02-12 06:59:32', NULL, 0, 0, 1),
('42465631-e7d6-4a95-97d6-d4b02db15872', '{\"ru\": \"В любую погоду\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 14:49:05', '2021-02-18 14:24:38', 0, 0, 1),
('44461664-1d51-41a1-a4e3-3345b1cf2d34', '{\"ru\": \"Развлечения для детей\"}', '{\"ru\": \"test\"}', 'E9EB00\r\n', 'C20067\r\n', 'c23646f7-0f16-44b4-971a-48ac29295150', NULL, 'c23646f7-0f16-44b4-971a-48ac29295150', NULL, '2021-02-20 06:29:38', NULL, 0, 0, 1),
('485b323c-c66f-476b-b0e9-ce9a65da4fb1', '{\"ru\": \"Детские комнаты\"}', '{\"ru\": \"test\"}', 'E9EB00\r\n\r\n', 'C20067\r\n\r\n', 'c69841e6-efdf-45cd-abc5-e0397a513fb8', NULL, 'c69841e6-efdf-45cd-abc5-e0397a513fb8', NULL, '2021-02-11 14:52:29', '2021-02-11 15:31:06', 0, 0, 1),
('491a0d49-15de-488e-aa7c-af48fca84879', '{\"ru\": \"Салоны красоты\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:58:40', NULL, 0, 0, 1),
('534f8691-9390-4537-9b81-eb7ed157db44', '{\"ru\": \"SPA и оздоровление\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 15:30:52', '2021-02-11 16:46:22', 0, 0, 1),
('574f71f5-0245-4085-b7a6-892875eeb5d2', '{\"ru\": \"Интересно детям\"}', '{\"ru\": \"test\"}', 'E9EB00\r\n', 'C20067\r\n\r\n\r\n\r\n\r\n\r\n\r\n', '8b83c8e1-a21d-4b1b-a5b7-767562ff8f16', NULL, '8b83c8e1-a21d-4b1b-a5b7-767562ff8f16', NULL, '2021-02-20 06:52:10', NULL, 0, 0, 1),
('5a345863-43ed-4c4f-b67e-212874704b95', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:59:09', NULL, 0, 0, 1),
('6972e904-2f7e-4b06-9516-b15e7ae0ace3', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-16 19:03:47', '2021-02-19 10:37:28', 0, 0, 1),
('74b19008-415f-4c7d-9eba-037529693fad', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 14:26:18', '2021-02-11 15:31:12', 0, 0, 1),
('755d3112-c700-4e03-b5f7-c8bf753aa3c7', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:59:22', NULL, 0, 0, 1),
('75ca31db-ad7d-4b84-b4a7-2766fc59fad8', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 14:48:31', NULL, 0, 0, 1),
('7ce15885-7d20-4fcc-90cd-3f47eeb644d9', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 15:30:39', NULL, 0, 0, 1),
('80403cb6-a628-4a70-8d24-5a91a75c5226', '{\"ru\": \"Скипассы\"}', '{\"ru\": \"test\"}', '458eda', 'bd066c', NULL, '<svg width=\"48\" height=\"48\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path d=\"M37.5579 11.6953C39.5563 11.6953 41.2472 10.0803 41.2472 8.00391C41.2472 6.00439 39.5563 4.3125 37.5579 4.3125C35.4827 4.3125 33.8686 6.00439 33.8686 8.00391C33.8686 10.0803 35.4827 11.6953 37.5579 11.6953ZM43.1687 39.1501C42.4001 38.3811 41.2472 38.3811 40.5554 39.1501C39.6331 40.073 38.1728 40.3037 37.0967 39.7654L26.6437 34.3821L30.4099 28.6912C31.409 27.23 31.1785 25.3074 29.9487 24.0769L26.8743 20.9238L18.6502 16.8479C18.4196 18.386 18.8808 19.9241 19.9568 21.0007L25.7214 26.8455L22.1858 32.075L7.0443 24.2307C6.12198 23.7693 4.96907 24.1538 4.50791 24.9998C4.04675 25.9226 4.43105 27.0762 5.35337 27.5376L35.4058 43.0723C36.3281 43.5337 37.3273 43.6875 38.3265 43.6875C40.0943 43.6875 41.8621 42.9954 43.1687 41.688C43.8604 40.9958 43.8604 39.8423 43.1687 39.1501ZM13.5774 11.3877L12.6551 13.1565C13.7312 13.6948 15.0378 13.4641 15.9601 12.772L19.5725 14.5408L28.3346 18.9243L30.871 17.9246L31.3322 19.3857C31.6396 20.3086 32.3314 21.0776 33.1768 21.5391L37.6347 23.7693C38.8645 24.3845 40.3249 23.9231 40.9397 22.6926C41.5546 21.4622 41.0935 20.001 39.8637 19.3857L35.867 17.3862L34.5603 13.4641C33.9454 11.5415 31.409 9.08057 28.0272 10.3879L21.8015 12.9258L16.9593 10.5417C17.0362 9.38818 16.4213 8.31152 15.4221 7.77319L14.4998 9.46509L11.8865 8.92676C11.7328 8.92676 11.5791 8.92676 11.5022 9.08057C11.3485 9.23438 11.3485 9.46509 11.5022 9.6189L13.5774 11.3877Z\" fill=\"#00187F\"/>\n</svg>\n', 'bcc5cb7a-202c-4cbb-817c-bfa89c1b4a50', NULL, '2021-02-18 08:22:54', NULL, 0, 0, 1),
('814e9d2c-6c70-4ea8-9bc2-3dd99b00caaf', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 07:00:56', NULL, 0, 0, 1),
('837197eb-2866-4e96-b989-a445f490bf68', '{\"ru\": \"some1\"}', '{\"description\": \"some2\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:49:25', NULL, 0, 0, 1),
('85304a73-e36c-454b-bb83-7a32dee615d1', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 07:02:16', NULL, 0, 0, 1),
('8a308fdc-6564-47bd-a3c7-e9dd7e9f71f9', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 22:43:10', NULL, 0, 0, 1),
('91e6d61e-bfde-4092-b99b-9ca38ad80e1a', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 06:47:13', NULL, 0, 0, 1),
('944bb20b-77c0-4000-afa6-fcc9c7126c51', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 06:48:30', NULL, 0, 0, 1),
('95d66eef-db2c-46c6-ba9b-83aa51137b1e', '{\"ru\": \"12\"}', '{\"description\": \"123\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:43:56', NULL, 0, 0, 1),
('a214c0a3-3611-4bcc-9905-9efcc7498b1e', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 14:49:56', '2021-02-11 15:30:31', 0, 0, 1),
('ae0e3924-098a-41b9-ba5c-17b39181ea49', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:59:12', NULL, 0, 0, 1),
('af715a1d-c0e7-444f-99d3-53f92b5b2ba7', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 06:47:06', NULL, 0, 0, 1),
('b1ea3e74-762b-4186-af43-2c17edf1cabb', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 07:05:31', NULL, 0, 0, 1),
('b2cece51-9014-443c-a606-1ce1b677630c', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 07:15:56', NULL, 0, 0, 1),
('b9df1733-d71a-4cec-a13e-faa3b2636463', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:56:03', NULL, 0, 0, 1),
('bbf4b3a0-28f9-4244-8b48-145abf04b700', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 14:20:45', '2021-02-11 15:10:23', 0, 0, 1),
('be4ad4d8-e121-4e9b-a12c-9557e31153d0', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 14:50:21', '2021-02-11 15:31:02', 0, 0, 1),
('c0a1ba91-5488-41bd-8511-b8248c05449a', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-16 19:03:16', NULL, 0, 0, 1),
('c252bbb5-e2a6-4069-9a81-cd7691d79313', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:54:20', NULL, 0, 0, 1),
('c2903bec-5c24-4fb9-991e-dce7fb708e1b', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:55:54', NULL, 0, 0, 1),
('c7cf6200-7238-499d-a209-f77416ccf232', '{\"ru\": \"test1\"}', '{\"description\": \"test1\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:47:43', NULL, 0, 0, 1),
('cabe69aa-a1f6-40a7-9ea7-f69ffb71de8b', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 06:48:45', NULL, 0, 0, 1),
('cd9998a2-9fdc-47e7-bf9c-494dd2344083', '{\"inputName\": \"3123\"}', '{\"inputDescription\": \"312\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:40:40', NULL, 0, 0, 1),
('d1cb4443-13fc-41b0-a8aa-7ef34335ef1d', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:57:33', NULL, 0, 0, 1),
('dfe32206-edfa-4e5b-8128-0b2ef1bc7fed', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 07:02:22', NULL, 0, 0, 1),
('e0d09022-aede-4157-ae4d-63a684446f11', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 14:49:56', NULL, 0, 0, 1),
('e6143e4e-bddd-47ab-b579-bfcc3fdd80c3', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-11 22:43:13', NULL, 0, 0, 1),
('ebaaa530-68b5-46cf-afb6-6a251322457a', '{\"ru\": \"Прогулочные билеты\"}', '{\"ru\": \"test\"}', '541f8c', '5480ce', NULL, '<svg width=\"48\" height=\"48\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path d=\"M27.4901 21.1564C27.7617 21.606 28.2487 21.8807 28.774 21.8807H36C36.8284 21.8807 37.5 21.2092 37.5 20.3807V19.9128C37.5 19.0844 36.8284 18.4128 36 18.4128H31.2402C30.7178 18.4128 30.233 18.1411 29.9605 17.6954L26.4762 11.9972C25.8877 11.0339 24.8285 10.3789 23.6123 10.3789C23.2592 10.3789 22.9454 10.4367 22.6315 10.533L13.0608 13.4641C12.4304 13.6572 12 14.2391 12 14.8984V22.3073C12 23.1358 12.6716 23.8073 13.5 23.8073H14.0308C14.8592 23.8073 15.5308 23.1358 15.5308 22.3073V17.8451C15.5308 17.1863 15.9606 16.6047 16.5903 16.4112L19.6696 15.4651L12.4874 43.123C12.2407 44.073 12.9578 45 13.9393 45H14.4768C15.1092 45 15.6737 44.6034 15.888 44.0085L21.1604 29.3752L25.4234 34.964C25.6228 35.2254 25.7308 35.545 25.7308 35.8738V43.5C25.7308 44.3284 26.4023 45 27.2308 45H27.7615C28.59 45 29.2615 44.3284 29.2615 43.5V33.0409C29.2615 32.7849 29.196 32.5331 29.0712 32.3096L24.3773 23.9037L25.8092 18.3743L27.4901 21.1564ZM27.6923 9.93578C29.6538 9.93578 31.2231 8.3945 31.2231 6.46789C31.2231 4.54128 29.6538 3 27.6923 3C25.7308 3 24.1615 4.54128 24.1615 6.46789C24.1615 8.3945 25.7308 9.93578 27.6923 9.93578Z\" fill=\"#00187F\"/>\n</svg>\n', '4b549b96-6265-4a21-aa3d-c8f1f3dfdba6', NULL, '2021-02-18 08:22:54', NULL, 0, 0, 1),
('f3d7829d-0ffc-4d5f-aa16-0bf9888d18c9', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-12 06:53:49', NULL, 0, 0, 1),
('f59ef843-c520-45d4-95c6-64f42533e40d', '{\"ru\": \"Для детей\"}', '{\"ru\": \"test\"}', 'c30864', 'e8e204', NULL, '<svg width=\"48\" height=\"48\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M23.998 36.9995C31.1777 36.9995 36.998 31.1792 36.998 23.9995C36.998 16.8198 31.1777 10.9995 23.998 10.9995C16.8183 10.9995 10.998 16.8198 10.998 23.9995C10.998 31.1792 16.8183 36.9995 23.998 36.9995ZM23.998 39.9995C32.8346 39.9995 39.998 32.8361 39.998 23.9995C39.998 15.163 32.8346 7.99951 23.998 7.99951C15.1615 7.99951 7.99805 15.163 7.99805 23.9995C7.99805 32.8361 15.1615 39.9995 23.998 39.9995Z\" fill=\"#00187F\"/>\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M22.5 1.5C22.5 0.671573 23.1716 0 24 0C27.0376 0 29.5 2.46243 29.5 5.5C29.5 8.53757 27.0376 11 24 11C23.1716 11 22.5 10.3284 22.5 9.5C22.5 8.67157 23.1716 8 24 8C25.3807 8 26.5 6.88071 26.5 5.5C26.5 4.11929 25.3807 3 24 3C23.1716 3 22.5 2.32843 22.5 1.5Z\" fill=\"#00187F\"/>\n<path d=\"M22 21.0005C22 22.1051 21.1046 23.0005 20 23.0005C18.8954 23.0005 18 22.1051 18 21.0005C18 19.8959 18.8954 19.0005 20 19.0005C21.1046 19.0005 22 19.8959 22 21.0005Z\" fill=\"#00187F\"/>\n<path d=\"M29.998 21.0005C29.998 22.1051 29.1026 23.0005 27.998 23.0005C26.8935 23.0005 25.998 22.1051 25.998 21.0005C25.998 19.8959 26.8935 19.0005 27.998 19.0005C29.1026 19.0005 29.998 19.8959 29.998 21.0005Z\" fill=\"#00187F\"/>\n<path d=\"M10.998 23.9995C10.998 25.6564 9.6549 27 7.99805 27C6.34119 27 4.99805 25.6569 4.99805 24C4.99805 22.3431 6.34119 21 7.99805 21C9.6549 21 10.998 22.3427 10.998 23.9995Z\" fill=\"#00187F\"/>\n<path d=\"M43.002 24C43.002 25.6569 41.6588 27 40.002 27C38.3451 27 37.002 25.6569 37.002 24C37.002 22.3431 38.3451 21 40.002 21C41.6588 21 43.002 22.3431 43.002 24Z\" fill=\"#00187F\"/>\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M19.7541 25.4996C20.3726 27.2495 22.0414 28.4995 23.9984 28.4995C25.9554 28.4995 27.6243 27.2495 28.2428 25.4996L31.0713 26.4994C30.0424 29.4103 27.2663 31.4995 23.9984 31.4995C20.7305 31.4995 17.9545 29.4103 16.9256 26.4994L19.7541 25.4996Z\" fill=\"#00187F\"/>\n</svg>\n', 'b8ad16db-b4f5-464a-b9a5-1ba78339132f', NULL, '2021-02-18 08:22:54', NULL, 0, 0, 1),
('f6430994-b6d6-46fe-bfe0-1e4e75af4e55', '{\"ru\": \"test\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-20 06:51:40', NULL, 0, 0, 1),
('f83644c8-9475-4067-8a6f-ba2e3ed9d1d3', '{\"ru\": \"Самое популярное\"}', '{\"ru\": \"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-18 08:22:54', '2021-02-18 13:59:29', 0, 0, 1),
('fcf8ae8d-a6e8-45de-9879-12c4a4965135', '{\"ru\": \"super\"}', '{\"description\": \"super2\"}', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:25:06', NULL, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_place`
--

CREATE TABLE `category_place` (
  `category_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `place_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `sort` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_place`
--

INSERT INTO `category_place` (`category_id`, `place_id`, `sort`) VALUES
('010cba3b-658b-4a1e-9b06-87cb19b32653', '8ce04491-a8db-44a9-a536-5c87778be5f9', 1),
('010cba3b-658b-4a1e-9b06-87cb19b32653', '8f88cdf2-d596-46ff-8595-efb8b012e788', 3),
('010cba3b-658b-4a1e-9b06-87cb19b32653', 'c96cbdcf-8f61-4899-b2a0-af780204588b', 2),
('01161fbd-5cdc-4434-a248-dc77a6865dac', 'd1b83c6c-089b-4605-856e-0fd0a01b0ae3', 5),
('01161fbd-5cdc-4434-a248-dc77a6865dac', 'd914c149-e1d4-49a6-a6d2-dcdb3ec4dba7', 6),
('01161fbd-5cdc-4434-a248-dc77a6865dac', 'f08424cf-d0d5-4717-82cd-ff774583c092', 4),
('1659445b-4931-4192-8a1d-d9147ecd6f61', '38a49948-7243-43b7-907c-3964a2b359b5', 9),
('1659445b-4931-4192-8a1d-d9147ecd6f61', 'a4786183-4442-45e5-b59c-57036d1589ca', 8),
('1659445b-4931-4192-8a1d-d9147ecd6f61', 'da187285-abea-4dce-ab24-d6d1fc5f6586', 7),
('1ccc82be-353a-4508-b3b3-8dac36e92f99', '644e272b-48b9-4f43-9bf2-86beb467b100', 11),
('1ccc82be-353a-4508-b3b3-8dac36e92f99', 'ee46bf8e-3aa0-40ed-9356-eb4064db5ff1', 10);

-- --------------------------------------------------------

--
-- Table structure for table `category_section`
--

CREATE TABLE `category_section` (
  `category_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `section_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `sort` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_section`
--

INSERT INTO `category_section` (`category_id`, `section_id`, `sort`) VALUES
('80403cb6-a628-4a70-8d24-5a91a75c5226', '71fda3ad-bb21-49bc-81fd-df2ca9512fc1', 0),
('80403cb6-a628-4a70-8d24-5a91a75c5226', '96d76c1f-128b-44b9-b0a0-3558b61b2ddd', 0),
('80403cb6-a628-4a70-8d24-5a91a75c5226', 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 0),
('ebaaa530-68b5-46cf-afb6-6a251322457a', '33be5c7d-3a56-45c9-9540-57763103f912', 0),
('ebaaa530-68b5-46cf-afb6-6a251322457a', 'ad9c6903-7ec8-4da1-b1e0-69a96b39e7fb', 0),
('f83644c8-9475-4067-8a6f-ba2e3ed9d1d3', '3b560e37-944f-4517-aaad-97daa5c447df', 0),
('f83644c8-9475-4067-8a6f-ba2e3ed9d1d3', '8d26384a-5098-4f8a-b3d8-d9a8832e4747', 0),
('f83644c8-9475-4067-8a6f-ba2e3ed9d1d3', 'a4741b9e-e677-4fb1-8187-84ff8b52e23d', 0),
('f83644c8-9475-4067-8a6f-ba2e3ed9d1d3', 'ae74e939-8259-48e8-9992-9ad030d347a2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('App\\Data\\Migrations\\Version20201028231709', '2021-01-15 14:42:27', 343),
('App\\Data\\Migrations\\Version20201028231802', '2021-01-15 14:42:28', 29),
('App\\Data\\Migrations\\Version20201028231803', '2021-01-15 14:42:28', 114),
('App\\Data\\Migrations\\Version20201028231808', '2021-01-15 14:42:28', 128),
('App\\Data\\Migrations\\Version20201107150322', '2021-01-15 14:42:28', 527),
('App\\Data\\Migrations\\Version20201109204018', '2021-01-15 14:42:29', 141),
('App\\Data\\Migrations\\Version20201111142406', '2021-01-15 14:42:29', 31),
('App\\Data\\Migrations\\Version20201111152749', '2021-01-15 14:42:29', 84),
('App\\Data\\Migrations\\Version20201111163343', '2021-01-15 14:42:29', 978),
('App\\Data\\Migrations\\Version20201113131721', '2021-01-15 14:42:30', 526),
('App\\Data\\Migrations\\Version20201115090801', '2021-01-15 14:42:30', 936),
('App\\Data\\Migrations\\Version20201117045538', '2021-01-15 14:42:31', 282),
('App\\Data\\Migrations\\Version20201122152642', '2021-01-15 14:42:32', 159),
('App\\Data\\Migrations\\Version20201128104455', '2021-01-15 14:42:32', 160),
('App\\Data\\Migrations\\Version20201208035741', '2021-01-15 14:42:32', 87),
('App\\Data\\Migrations\\Version20201208215847', '2021-01-15 14:42:32', 338),
('App\\Data\\Migrations\\Version20201209062146', '2021-01-15 14:42:32', 43),
('App\\Data\\Migrations\\Version20201211060856', '2021-01-15 14:42:33', 127),
('App\\Data\\Migrations\\Version20201211122523', '2021-01-15 14:42:33', 120),
('App\\Data\\Migrations\\Version20201211134420', '2021-01-15 14:42:33', 186),
('App\\Data\\Migrations\\Version20201211162708', '2021-01-15 14:42:33', 519),
('App\\Data\\Migrations\\Version20201211173858', '2021-01-15 14:42:34', 638),
('App\\Data\\Migrations\\Version20201211180511', '2021-01-15 14:42:34', 394),
('App\\Data\\Migrations\\Version20201211183420', '2021-01-15 14:42:35', 216),
('App\\Data\\Migrations\\Version20201211185712', '2021-01-15 14:42:35', 216),
('App\\Data\\Migrations\\Version20201214085522', '2021-01-15 14:42:35', 408),
('App\\Data\\Migrations\\Version20201217095557', '2021-01-15 14:42:36', 140),
('App\\Data\\Migrations\\Version20201217123245', '2021-01-15 14:42:36', 154),
('App\\Data\\Migrations\\Version20201218121225', '2021-01-15 14:42:36', 455),
('App\\Data\\Migrations\\Version20201222014858', '2021-01-15 14:42:36', 227),
('App\\Data\\Migrations\\Version20201222030620', '2021-01-15 14:42:37', 112),
('App\\Data\\Migrations\\Version20201222140214', '2021-01-15 14:42:37', 655),
('App\\Data\\Migrations\\Version20201223171736', '2021-01-15 14:42:37', 27),
('App\\Data\\Migrations\\Version20201223173044', '2021-01-15 14:42:37', 181),
('App\\Data\\Migrations\\Version20201223184524', '2021-01-15 14:42:38', 103),
('App\\Data\\Migrations\\Version20201223193210', '2021-01-15 14:42:38', 110),
('App\\Data\\Migrations\\Version20201223195419', '2021-01-15 14:42:38', 115),
('App\\Data\\Migrations\\Version20201223204523', '2021-01-15 14:42:38', 83),
('App\\Data\\Migrations\\Version20201224165442', '2021-01-15 14:42:38', 146),
('App\\Data\\Migrations\\Version20201225095752', '2021-01-15 14:42:38', 218),
('App\\Data\\Migrations\\Version20201225174006', '2021-01-15 14:42:39', 301),
('App\\Data\\Migrations\\Version20201225205428', '2021-01-15 14:42:39', 527),
('App\\Data\\Migrations\\Version20201225210118', '2021-01-15 14:42:39', 240),
('App\\Data\\Migrations\\Version20201226154254', '2021-01-15 14:42:40', 218),
('App\\Data\\Migrations\\Version20201228060256', '2021-01-15 14:42:40', 730),
('App\\Data\\Migrations\\Version20201228094941', '2021-01-15 14:42:41', 307),
('App\\Data\\Migrations\\Version20201228101503', '2021-01-15 14:42:41', 57),
('App\\Data\\Migrations\\Version20201228103832', '2021-01-15 14:42:41', 164),
('App\\Data\\Migrations\\Version20201228105309', '2021-01-15 14:42:41', 168),
('App\\Data\\Migrations\\Version20201228122239', '2021-01-15 14:42:41', 30),
('App\\Data\\Migrations\\Version20201228164901', '2021-01-15 14:42:41', 306),
('App\\Data\\Migrations\\Version20201229110726', '2021-01-15 14:42:42', 191),
('App\\Data\\Migrations\\Version20201229172736', '2021-01-15 14:42:42', 126),
('App\\Data\\Migrations\\Version20201229192810', '2021-01-15 14:42:42', 250),
('App\\Data\\Migrations\\Version20201229202110', '2021-01-15 14:42:42', 168),
('App\\Data\\Migrations\\Version20201230124715', '2021-01-15 14:42:43', 314),
('App\\Data\\Migrations\\Version20201230173341', '2021-01-15 14:42:43', 83),
('App\\Data\\Migrations\\Version20210106134212', '2021-01-15 14:42:43', 491),
('App\\Data\\Migrations\\Version20210106135631', '2021-01-15 14:42:44', 677),
('App\\Data\\Migrations\\Version20210106142711', '2021-01-15 14:42:44', 330),
('App\\Data\\Migrations\\Version20210120025725', '2021-01-25 10:21:39', 355),
('App\\Data\\Migrations\\Version20210120032456', '2021-01-25 10:21:39', 1218),
('App\\Data\\Migrations\\Version20210121080436', '2021-03-22 08:25:07', 2010),
('App\\Data\\Migrations\\Version20210123101145', '2021-01-25 10:21:41', 401),
('App\\Data\\Migrations\\Version20210203105244', '2021-02-04 06:03:33', 394),
('App\\Data\\Migrations\\Version20210203110927', '2021-02-04 06:03:34', 271),
('App\\Data\\Migrations\\Version20210209112947', '2021-02-09 11:39:38', 242),
('App\\Data\\Migrations\\Version20210209160617', '2021-02-10 08:57:04', 1678),
('App\\Data\\Migrations\\Version20210210145948', '2021-02-10 15:42:19', 1346),
('App\\Data\\Migrations\\Version20210212025954', '2021-02-12 07:55:11', 439),
('App\\Data\\Migrations\\Version20210212043723', '2021-02-12 07:55:11', 215),
('App\\Data\\Migrations\\Version20210212110439', '2021-02-12 12:14:07', 3468),
('App\\Data\\Migrations\\Version20210216055358', '2021-02-16 06:01:05', 534),
('App\\Data\\Migrations\\Version20210218073044', '2021-02-18 08:22:31', 1588),
('App\\Data\\Migrations\\Version20210220104640', '2021-03-11 13:14:57', 7470),
('App\\Data\\Migrations\\Version20210220125341', '2021-02-20 13:01:02', 1179),
('App\\Data\\Migrations\\Version20210225074122', '2021-02-20 13:01:02', 62),
('App\\Data\\Migrations\\Version20210225110016', '2021-03-12 08:22:59', 1150),
('App\\Data\\Migrations\\Version20210226032446', '2021-02-20 13:01:02', 67),
('App\\Data\\Migrations\\Version20210226050314', '2021-02-20 13:01:02', 86),
('App\\Data\\Migrations\\Version20210302123452', '2021-03-03 07:15:16', 243),
('App\\Data\\Migrations\\Version20210303051858', '2021-03-06 05:35:13', 455),
('App\\Data\\Migrations\\Version20210303142821', '2021-03-03 15:07:10', 972),
('App\\Data\\Migrations\\Version20210304140259', '2021-03-05 09:05:58', 613),
('App\\Data\\Migrations\\Version20210305104309', '2021-03-05 10:59:25', 210),
('App\\Data\\Migrations\\Version20210310125510', '2021-03-12 09:42:31', 883),
('App\\Data\\Migrations\\Version20210310204955', '2021-03-11 05:15:50', 1049),
('App\\Data\\Migrations\\Version20210310205847', '2021-03-11 05:15:51', 100),
('App\\Data\\Migrations\\Version20210311040119', '2021-03-11 05:15:51', 126),
('App\\Data\\Migrations\\Version20210311043024', '2021-03-11 05:15:51', 296),
('App\\Data\\Migrations\\Version20210311044904', '2021-03-11 05:15:51', 185),
('App\\Data\\Migrations\\Version20210315135503', '2021-03-16 06:22:24', 593),
('App\\Data\\Migrations\\Version20210316100902', '2021-03-22 08:30:49', 3115),
('App\\Data\\Migrations\\Version20210317144426', '2021-03-17 16:08:35', 1295),
('App\\Data\\Migrations\\Version20210318074348', '2021-03-18 10:27:57', 1711),
('App\\Data\\Migrations\\Version20210324120305', '2021-03-24 15:54:48', 865),
('App\\Data\\Migrations\\Version20210324164137', '2021-03-25 12:30:56', 11378),
('App\\Data\\Migrations\\Version20210324204057', '2021-03-25 12:39:19', 4746),
('App\\Data\\Migrations\\Version20210401145805', '2021-04-01 15:39:50', 919),
('App\\Data\\Migrations\\Version20210401154754', '2021-04-01 16:09:53', 454),
('App\\Data\\Migrations\\Version20210405125835', '2021-04-05 17:10:36', 990),
('App\\Data\\Migrations\\Version20210405162737', '2021-04-05 17:10:37', 806),
('App\\Data\\Migrations\\Version20210405165106', '2021-04-05 17:10:38', 606),
('App\\Data\\Migrations\\Version20210406092223', '2021-04-07 08:16:30', 532),
('App\\Data\\Migrations\\Version20210409093237', '2021-04-09 09:35:05', 90),
('App\\Data\\Migrations\\Version20210409104454', '2021-04-13 10:29:19', 968),
('App\\Data\\Migrations\\Version20210412082020', '2021-04-12 10:10:37', 363),
('App\\Data\\Migrations\\Version20210413085628', '2021-04-13 10:26:46', 252);

-- --------------------------------------------------------

--
-- Table structure for table `extra_place_types`
--

CREATE TABLE `extra_place_types` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `is_active` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `facility_group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_shown_in_room_list` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `mobile_icon` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_icon` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `name`, `is_active`, `facility_group_id`, `is_shown_in_room_list`, `mobile_icon`, `web_icon`, `sort`) VALUES
('0339442b-93e1-44ea-bd2b-855bbe9930e1', '{\"en\": \"\", \"ru\": \"Места для курения\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('04227a97-f237-4307-9f4c-ba60549add18', '{\"en\": \"\", \"ru\": \"Вино/шампанское\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('0ac79fdb-3d6c-499d-9702-7d99cffcf6c1', '{\"en\": \"\", \"ru\": \"Огнетушители\"}', 1, '50363ec6-a523-40d3-af0f-1b58cf2163d8', 1, NULL, NULL, NULL),
('0b833a4d-5aee-4c10-a534-fafc931e0c55', '{\"en\": \"\", \"ru\": \"Турецкая баня\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('0df02fc7-f0b8-473f-a330-3513c15b6d85', '{\"en\": \"\", \"ru\": \"Фитнес\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('0f9f57a6-2289-4568-a07f-f849c24298cf', '{\"en\": \"\", \"ru\": \"английский\"}', 1, '70803b30-d470-4dc6-ab0f-838700250b32', 1, NULL, NULL, NULL),
('10f6a657-9059-4761-9bb2-b60453d8405d', '{\"en\": \"\", \"ru\": \"VIP-удобства в номерах\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('13db84fe-87c4-46ee-ad59-d6f4822e8871', '{\"en\": \"\", \"ru\": \"Купальня на открытом воздухе\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('179e16d4-5194-4e9e-9209-d47655677e4e', '{\"en\": \"\", \"ru\": \"Корзина для домашних животных\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('1871e3f6-6617-4426-88e6-dc79a1f04e32', '{\"en\": \"\", \"ru\": \"Люкс для новобрачных\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('18c891c4-a827-4125-a96c-565321707d8d', '{\"en\": \"\", \"ru\": \"Настольные игры и/или пазлы\"}', 1, '48815594-8e6b-4a6e-b82f-44a77cdec15f', 1, NULL, NULL, NULL),
('18d4d12a-dc22-4f16-bccb-12c7ce3a1019', '{\"en\": \"\", \"ru\": \"Педикюр\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('1a82e930-64f2-4d8a-bd35-8bc447ee0ea3', '{\"en\": \"\", \"ru\": \"Прокат автомобилей\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('20d4fd41-306e-4931-90ce-d5505446612b', '{\"en\": \"\", \"ru\": \"Запирающиеся шкафчики\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('23118403-aa94-4e3f-b879-ec81929f0e35', '{\"en\": \"\", \"ru\": \"Шоколад или печенье\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('235bc4c6-8d63-4b50-86ed-ff87958a0687', '{\"en\": \"\", \"ru\": \"Уход за телом\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('23e8666f-5fb0-40b4-97bc-e592ddde67b1', '{\"en\": \"\", \"ru\": \"Экскурсия или презентация о местной культуре\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('25ee8325-4f17-4dc3-9e7d-23e7cd702d94', '{\"en\": \"\", \"ru\": \"Детская коляска\"}', 1, '48815594-8e6b-4a6e-b82f-44a77cdec15f', 1, NULL, NULL, NULL),
('286a2640-0afd-49b3-a225-0c49db8ad6cd', '{\"en\": \"\", \"ru\": \"Номера для некурящих\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('28f35a55-9d8b-4325-a8d7-2db75d069848', '{\"en\": \"\", \"ru\": \"Ежедневная уборка\"}', 1, '00cf4a62-0d60-4b99-8b7e-b3d696243f79', 1, NULL, NULL, NULL),
('2a8767e4-ace7-43bf-9073-052868f0f705', '{\"en\": \"\", \"ru\": \"Газеты\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('2d58756a-e183-4cda-a7ac-7d0e7017fa33', '{\"en\": \"\", \"ru\": \"Пилинг для тела\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('2e50f11c-6afd-4d03-931b-07312b0168e9', '{\"en\": \"\", \"ru\": \"Завтрак в номер\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('2edc89bd-0d87-49cf-8772-801f02b88d38', '{\"en\": \"\", \"ru\": \"Велосипедные экскурсии\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('311d7a45-ecac-4a3b-be4c-f402fe6153d2', '{\"en\": \"\", \"ru\": \"Видеонаблюдение в местах общего пользования\"}', 1, '50363ec6-a523-40d3-af0f-1b58cf2163d8', 1, NULL, NULL, NULL),
('3207c923-1f89-43a4-9a69-4afb90e953d0', '{\"en\": \"\", \"ru\": \"Ванночка для ног\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('33be1c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"test\"}', 1, '33be2c7d-3a56-45c9-9540-57763103f912', 0, NULL, NULL, NULL),
('37148e22-806c-4be8-a995-560a5794110b', '{\"en\": \"\", \"ru\": \"Лифт\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('372b24f4-9124-41f5-a0d2-9dd233edd596', '{\"en\": \"\", \"ru\": \"Детское меню\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('38623096-bed7-4d61-b5fc-3b0f81c9583e', '{\"en\": \"\", \"ru\": \"Бар\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('38a302da-0e1b-4678-a36c-c3622b9b13e4', '{\"en\": \"\", \"ru\": \"Детская игровая площадка\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('3ad744f9-4bd6-486a-94ad-d3eedd392ad0', '{\"en\": \"\", \"ru\": \"Сауна\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('3d00c16c-a77a-4a6f-b65f-f79ae2628819', '{\"en\": \"\", \"ru\": \"Ускоренная регистрация заезда/отъезда\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('3d7c030b-8ce1-4aa4-ac91-70eff93855da', '{\"en\": \"\", \"ru\": \"Хранение багажа\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('3dd16aec-8183-4248-a278-a68a0f3661dd', '{\"en\": \"\", \"ru\": \"Cтанция зарядки электромобилей\"}', 1, 'dac2dd2d-c25e-45f1-b9a0-18c6a90152bf', 1, NULL, NULL, NULL),
('3e99d5e1-9fd1-42b2-8c60-eab6ab68f4a7', '{\"en\": \"\", \"ru\": \"Пакеты услуг спа/велнесс\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('3f47fbe4-595e-46bd-82b4-028eeea8363c', '{\"en\": \"\", \"ru\": \"Размещение домашних животных допускается по предварительному запросу. Данная услуга может быть платной.\"}', 1, 'a669ea19-2736-4c3f-9af8-40d75abf977a', 1, NULL, NULL, NULL),
('414e478d-2069-4d86-b807-7ed10d4f603b', '{\"en\": \"\", \"ru\": \"Няня / услуги по уходу за детьми\"}', 1, '48815594-8e6b-4a6e-b82f-44a77cdec15f', 1, NULL, NULL, NULL),
('423e5480-1565-47ac-9be5-17561e186b49', '{\"en\": \"\", \"ru\": \"Маникюр\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('428e0b94-e15e-40d3-aa7e-a27cd52646cb', '{\"en\": \"\", \"ru\": \"Парная\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('43db32be-2bd2-48d4-9073-34d724cd9bed', '{\"en\": \"\", \"ru\": \"Сейф\"}', 1, '50363ec6-a523-40d3-af0f-1b58cf2163d8', 1, NULL, NULL, NULL),
('459b3911-ab5c-44e5-bd9e-136ccb1172de', '{\"en\": \"\", \"ru\": \"Место для пикника\"}', 1, 'ad636fe1-1e29-43ae-981f-27f0607099dd', 1, NULL, NULL, NULL),
('4b6e5e2a-902e-4549-b7ce-53ec6ac34d62', '{\"en\": \"\", \"ru\": \"Общий лаундж/гостиная с телевизором\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('50e60545-c72f-4804-94cd-443332ad56c7', '{\"en\": \"\", \"ru\": \"Трансфер\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('51839161-5c18-419c-a76d-718e660de977', '{\"en\": \"\", \"ru\": \"Игровая комната\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('55a21266-d62b-46e8-879a-212012165687', '{\"en\": \"\", \"ru\": \"Круглосуточная стойка регистрации\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('56a975ce-5fdb-4fa7-b584-7b70d1812994', '{\"en\": \"\", \"ru\": \"Услуги по продаже билетов\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('56b851cc-640c-47f1-8049-8ce1bad5ab82', '{\"en\": \"\", \"ru\": \"Охранная сигнализация\"}', 1, '50363ec6-a523-40d3-af0f-1b58cf2163d8', 1, NULL, NULL, NULL),
('589350dc-0546-41d1-8116-8d9423ecd529', '{\"en\": \"\", \"ru\": \"Прачечная\"}', 1, '00cf4a62-0d60-4b99-8b7e-b3d696243f79', 1, NULL, NULL, NULL),
('58b44fbc-bb74-4696-b6cc-887c276ad5af', '{\"en\": \"\", \"ru\": \"Терраса\"}', 1, 'ad636fe1-1e29-43ae-981f-27f0607099dd', 1, NULL, NULL, NULL),
('594c73c5-5fbe-4563-b973-cb6049177a1a', '{\"en\": \"\", \"ru\": \"русский\"}', 1, '70803b30-d470-4dc6-ab0f-838700250b32', 1, NULL, NULL, NULL),
('5f0a1ea2-2c09-44d1-b361-cbec108bdbe3', '{\"en\": \"\", \"ru\": \"Место для хранения лыж\"}', 1, '7bb8be8d-b120-49cf-b141-7f4b2739a598', 1, NULL, NULL, NULL),
('6000a72c-d77c-4f48-965e-83304e77e95a', '{\"en\": \"\", \"ru\": \"Фрукты\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('62f67163-300e-4c30-bef8-7232defd8010', '{\"en\": \"\", \"ru\": \"Детские телеканалы\"}', 1, '48815594-8e6b-4a6e-b82f-44a77cdec15f', 1, NULL, NULL, NULL),
('642c7ab1-e3ed-4d1f-9757-a0a034f562d3', '{\"en\": \"\", \"ru\": \"Бизнес-центр\"}', 1, '9c9da0db-f8b9-49b6-8f52-0fb595df6347', 1, NULL, NULL, NULL),
('65f982a8-7f66-41df-bc97-fe3785ec9311', '{\"en\": \"\", \"ru\": \"Экскурсионное бюро\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('6840f561-4062-45b5-8024-6bd6c4dab344', '{\"en\": \"\", \"ru\": \"Кондиционер\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('70e6176a-d514-43de-9d06-f19207fc7d35', '{\"en\": \"\", \"ru\": \"Спа и оздоровительный центр\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('7110a928-bfa7-4200-89d7-c5fa31c4a22c', '{\"en\": \"\", \"ru\": \"Садовая мебель\"}', 1, 'ad636fe1-1e29-43ae-981f-27f0607099dd', 1, NULL, NULL, NULL),
('7644e66a-fd9b-4949-b3a6-7e2ff4e1b306', '{\"en\": \"\", \"ru\": \"Отопление\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('7c58a437-cb34-4a6c-b785-1e24d05c1925', '{\"en\": \"\", \"ru\": \"Вечерняя программа\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('7e3ef0c9-f863-4795-a469-64c2a83971ef', '{\"en\": \"\", \"ru\": \"Буфет, подходящий для детей\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('7f35279c-2f8d-4c0f-bc45-830431a6328b', '{\"en\": \"\", \"ru\": \"Уход за лицом\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('7fbdcbe8-49df-4f84-892d-e18cfbdf0e14', '{\"en\": \"\", \"ru\": \"Трансфер от/до аэропорта (оплачивается отдельно)\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('82dd023f-8c71-464a-b238-3bf20ca19853', '{\"en\": \"\", \"ru\": \"Трансфер в аэропорт\"}', 1, '90b8a0ae-0495-425d-ae70-86c89dc772e6', 1, NULL, NULL, NULL),
('84bcc958-4d8c-41a6-9c00-de82e1a47aba', '{\"en\": \"\", \"ru\": \"Упакованные ланчи\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('84d55735-63b0-4ac9-8880-66e849563f92', '{\"en\": \"\", \"ru\": \"Видеонаблюдение снаружи здания\"}', 1, '50363ec6-a523-40d3-af0f-1b58cf2163d8', 1, NULL, NULL, NULL),
('885d6507-416a-4ca5-91d9-75ea591ccf17', '{\"en\": \"\", \"ru\": \"Парковочные места для людей с ограниченными физическими возможностями\"}', 1, 'dac2dd2d-c25e-45f1-b9a0-18c6a90152bf', 1, NULL, NULL, NULL),
('8bf9b640-6902-4328-9533-28a4cd4ffd9b', '{\"en\": \"\", \"ru\": \"Трансфер из аэропорта\"}', 1, '90b8a0ae-0495-425d-ae70-86c89dc772e6', 1, NULL, NULL, NULL),
('8c924222-5c49-42d8-b4f3-3d3c8b29874c', '{\"en\": \"\", \"ru\": \"Факс/ксерокопирование\"}', 1, '9c9da0db-f8b9-49b6-8f52-0fb595df6347', 1, NULL, NULL, NULL),
('8f15aefc-3c0b-4b8b-863d-7bcc8daa9f6e', '{\"en\": \"\", \"ru\": \"Оборудование для занятия водными видами спорта\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('923e709e-10a4-4706-ac26-20041cd58e27', '{\"en\": \"\", \"ru\": \"Специальные диетические меню (по запросу)\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('93505fbd-7d84-4a8a-8d64-2fc54fcec6e7', '{\"en\": \"\", \"ru\": \"Спа-процедуры\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('94541d7a-b194-4efd-b97e-fb05c735dc63', '{\"en\": \"\", \"ru\": \"Гипоаллергенный номер\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('94a7708d-d312-497a-940e-dd59f988e64d', '{\"en\": \"\", \"ru\": \"Фототерапия\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('95b7eff9-f0d8-47aa-b515-ea7de806f729', '{\"en\": \"\", \"ru\": \"Детский клуб\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('969b1a17-7ebb-499a-b1c9-30e4f7a00140', '{\"en\": \"\", \"ru\": \"Анимационный персонал\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('9c1869bc-19ee-4e6b-b34b-f4bb6caec095', '{\"en\": \"\", \"ru\": \"Бутылка воды\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('9d2dd91a-ade8-4dd6-97bd-79bf2703f520', '{\"en\": \"\", \"ru\": \"Ночной клуб/диджей\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('9d82ce4a-f3fb-4c65-87b0-4670756b44d2', '{\"en\": \"\", \"ru\": \"Трансфер (оплачивается отдельно)\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('a2dea00c-41d9-4cbe-b463-e2c9082f2797', '{\"en\": \"\", \"ru\": \"Доставка продуктов питания\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('a5d482ab-1fd3-4102-b7b2-0541eb29fd66', '{\"en\": \"\", \"ru\": \"Датчики дыма\"}', 1, '50363ec6-a523-40d3-af0f-1b58cf2163d8', 1, NULL, NULL, NULL),
('a5f2b355-f8db-4cd3-93f4-17fab095d0a4', '{\"en\": \"\", \"ru\": \"Зонты от солнца\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('a6a7707b-0e15-45e5-83e8-a6654d26012f', '{\"en\": \"\", \"ru\": \"Рыбная ловля За территорией\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('ab982093-9fdf-424b-96e4-87dc27bf4004', '{\"en\": \"\", \"ru\": \"Обертывание\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('ace39ed7-38d1-4258-b01e-9e4a12b15c1d', '{\"en\": \"\", \"ru\": \"Услуги консьержа\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('ada441f1-fa5d-4893-a128-0b0cea3592fb', '{\"en\": \"\", \"ru\": \"Миски для домашних животных\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('afba889d-f078-4d65-b7d7-cda628854321', '{\"en\": \"\", \"ru\": \"Круглосуточная охрана\"}', 1, '50363ec6-a523-40d3-af0f-1b58cf2163d8', 1, NULL, NULL, NULL),
('b075ce27-022e-4baf-a339-e0a445be3548', '{\"en\": \"\", \"ru\": \"Прокат лыжного снаряжения\"}', 1, '7bb8be8d-b120-49cf-b141-7f4b2739a598', 1, NULL, NULL, NULL),
('b485c234-45a8-48a7-9bf0-fc98aa818e22', '{\"en\": \"\", \"ru\": \"немецкий\"}', 1, '70803b30-d470-4dc6-ab0f-838700250b32', 1, NULL, NULL, NULL),
('b6277ae9-dc7d-4017-aa9c-6bfa942e9e2b', '{\"en\": \"\", \"ru\": \"Услуги по глажению одежды\"}', 1, '00cf4a62-0d60-4b99-8b7e-b3d696243f79', 1, NULL, NULL, NULL),
('b7957085-509f-4ba8-9a79-d9220c36c8c1', '{\"en\": \"\", \"ru\": \"Временные художественные экспозиции\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('b8f43dad-df42-40c0-8477-5a04de134b02', '{\"en\": \"\", \"ru\": \"Маршруты для пеших прогулок\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('bab3c9f7-282d-4228-beb8-3ec220ad1bdb', '{\"en\": \"\", \"ru\": \"Фитнес-центр\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('c0bd6cc3-9207-4670-a876-ad8a8bd1010b', '{\"en\": \"\", \"ru\": \"Удобства для гостей с ограниченными физическими возможностями\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('c7b4db7c-2ffa-4ba4-b46b-b51e64d9b33a', '{\"en\": \"\", \"ru\": \"Парикмахерская/салон красоты\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('c9cc80b4-1137-433b-a7ce-bf303c10db19', '{\"en\": \"\", \"ru\": \"Курение на всей территории запрещено\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL),
('cf2e5302-bc10-43bc-aff7-298b2ed55ad8', '{\"en\": \"\", \"ru\": \"Прямая трансляция спортивных мероприятий\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('d2b8866b-c02a-4aff-88ee-922c3538dbdb', '{\"en\": \"\", \"ru\": \"Снэк-бар\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('d5e4eda3-797a-4a08-af50-327c7ea0dc08', '{\"en\": \"\", \"ru\": \"Массаж\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('d842fc32-e357-488b-a447-2de66ee0c33a', '{\"en\": \"\", \"ru\": \"Продажа горнолыжного абонемента\"}', 1, '7bb8be8d-b120-49cf-b141-7f4b2739a598', 1, NULL, NULL, NULL),
('d8832c42-0ab5-47c9-aee1-68136c55ed6d', '{\"en\": \"\", \"ru\": \"Гидромассажная ванна/джакузи\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('dd48e0b3-cea1-4e7e-bb95-761ab4c14fc0', '{\"en\": \"\", \"ru\": \"Конференц-зал/банкетный зал\"}', 1, '9c9da0db-f8b9-49b6-8f52-0fb595df6347', 1, NULL, NULL, NULL),
('df3ccb50-42ea-4b09-a200-a6b88847bb91', '{\"en\": \"\", \"ru\": \"Пешеходные экскурсии\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('e25bbffe-1862-4bcb-aaf9-6dfc1022a2d6', '{\"en\": \"\", \"ru\": \"Аэробика\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('e2bc37cd-7836-428d-ac41-f02656447c95', '{\"en\": \"\", \"ru\": \"Косметические услуги\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 0, NULL, NULL, NULL),
('e2e87596-7465-4717-9d5b-7f0fe998f2fa', '{\"en\": \"\", \"ru\": \"Игровая зона в помещении\"}', 1, '48815594-8e6b-4a6e-b82f-44a77cdec15f', 1, NULL, NULL, NULL),
('e5b8c2ce-d623-4324-a5b3-b2455f58572e', '{\"en\": \"\", \"ru\": \"Химчистка\"}', 1, '00cf4a62-0d60-4b99-8b7e-b3d696243f79', 1, NULL, NULL, NULL),
('e82cb3c3-e9ba-4ce3-91c7-9f09f73dc8dc', '{\"en\": \"\", \"ru\": \"Индивидуальная регистрация заезда/отъезда\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('e86a7420-004c-4bcd-8383-6727b27a1d4b', '{\"en\": \"\", \"ru\": \"Ресторан\"}', 1, '49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', 1, NULL, NULL, NULL),
('eb31f0ec-8863-4537-8a47-a0dfce54bd76', '{\"en\": \"\", \"ru\": \"Верховая езда\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('ec253ccb-83aa-4e82-8645-7121592e27c3', '{\"en\": \"\", \"ru\": \"Раздевалка со шкафчиками в фитнес-центре / спа-салоне\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('ee9a9e98-d16f-4923-868f-8a028974062b', '{\"en\": \"\", \"ru\": \"Крытая парковка\"}', 1, 'dac2dd2d-c25e-45f1-b9a0-18c6a90152bf', 1, NULL, NULL, NULL),
('f2f445d6-1cf1-4d67-bf57-6ec4a93d2d7d', '{\"en\": \"\", \"ru\": \"Спа-лаундж/зона для релаксации\"}', 1, 'ab00540a-1890-435a-b985-ad1f44ae5509', 1, NULL, NULL, NULL),
('f532c91c-c1a3-4d45-81d1-bcb75c83a947', '{\"en\": \"\", \"ru\": \"Охраняемая парковка\"}', 1, 'dac2dd2d-c25e-45f1-b9a0-18c6a90152bf', 1, NULL, NULL, NULL),
('f6154f61-1137-4883-a744-82d3240bd015', '{\"en\": \"\", \"ru\": \"Детские книги, музыка или фильмы\"}', 1, '48815594-8e6b-4a6e-b82f-44a77cdec15f', 1, NULL, NULL, NULL),
('f6855db9-16a4-4562-a485-2aeeeae460fe', '{\"en\": \"\", \"ru\": \"Банкомат на территории отеля\"}', 1, '1099297d-2eda-4fb3-864b-811530c352fe', 1, NULL, NULL, NULL),
('f6e28482-7564-48bb-82be-1762d9e80a3a', '{\"en\": \"\", \"ru\": \"Катание на лыжах За территорией\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('f74cba07-4ad4-4182-bca8-7abc8721f76a', '{\"en\": \"\", \"ru\": \"Wi-Fi предоставляется на территории всего отеля бесплатно.\"}', 1, '9d8b92a4-4c22-4ca3-8e14-82a78717ad85', 1, NULL, NULL, NULL),
('f75f1a50-bf34-451d-b70d-87fc770a7aaf', '{\"en\": \"\", \"ru\": \"Билеты на общественный транспорт\"}', 1, '90b8a0ae-0495-425d-ae70-86c89dc772e6', 1, NULL, NULL, NULL),
('faffb74f-4157-4401-8bc4-8017039bb00c', '{\"en\": \"\", \"ru\": \"Велоспорт\"}', 1, 'fbb97046-909c-465d-aa63-4ab3c069cd69', 1, NULL, NULL, NULL),
('fb2c0e46-3c95-46ba-b6df-9ca087d1f6fb', '{\"en\": \"\", \"ru\": \"Чистка обуви\"}', 1, '00cf4a62-0d60-4b99-8b7e-b3d696243f79', 1, NULL, NULL, NULL),
('fc9abc71-145e-4c06-a5d4-68207d7e3f02', '{\"en\": \"\", \"ru\": \"Доставка еды и напитков в номер\"}', 1, '3aca4996-28d3-4d1e-9c91-41c82b185af5', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facility_group`
--

CREATE TABLE `facility_group` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `is_active` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `is_shown_hotel` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `is_shown_room` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `mobile_icon` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_icon` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facility_group`
--

INSERT INTO `facility_group` (`id`, `name`, `is_active`, `is_shown_hotel`, `is_shown_room`, `mobile_icon`, `web_icon`, `sort`) VALUES
('00cf4a62-0d60-4b99-8b7e-b3d696243f79', '{\"en\": \"\", \"ru\": \"Услуги уборки\"}', 1, 1, 1, NULL, NULL, NULL),
('1099297d-2eda-4fb3-864b-811530c352fe', '{\"en\": \"\", \"ru\": \"Стойка регистрации\"}', 1, 1, 1, NULL, NULL, NULL),
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"test\"}', 1, 0, 0, NULL, NULL, NULL),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"test\"}', 1, 0, 0, NULL, NULL, NULL),
('3aca4996-28d3-4d1e-9c91-41c82b185af5', '{\"en\": \"\", \"ru\": \"Общие\"}', 1, 1, 1, NULL, NULL, NULL),
('48815594-8e6b-4a6e-b82f-44a77cdec15f', '{\"en\": \"\", \"ru\": \"Развлечения и семейные услуги\"}', 1, 1, 1, NULL, NULL, NULL),
('49ac0bb8-25dc-4a00-b1ed-7cd6c7418314', '{\"en\": \"\", \"ru\": \"Питание и напитки\"}', 1, 1, 1, NULL, NULL, NULL),
('50363ec6-a523-40d3-af0f-1b58cf2163d8', '{\"en\": \"\", \"ru\": \"Безопасность\"}', 1, 1, 1, NULL, NULL, NULL),
('70803b30-d470-4dc6-ab0f-838700250b32', '{\"en\": \"\", \"ru\": \"Персонал говорит на этих языках\"}', 1, 1, 1, NULL, NULL, NULL),
('7bb8be8d-b120-49cf-b141-7f4b2739a598', '{\"en\": \"\", \"ru\": \"Лыжи\"}', 1, 1, 1, NULL, NULL, NULL),
('90b8a0ae-0495-425d-ae70-86c89dc772e6', '{\"en\": \"\", \"ru\": \"Транспорт\"}', 1, 1, 1, NULL, NULL, NULL),
('9c9da0db-f8b9-49b6-8f52-0fb595df6347', '{\"en\": \"\", \"ru\": \"Услуги бизнес-центра\"}', 1, 1, 1, NULL, NULL, NULL),
('9d8b92a4-4c22-4ca3-8e14-82a78717ad85', '{\"en\": \"\", \"ru\": \"Интернет\"}', 1, 1, 1, NULL, NULL, NULL),
('a669ea19-2736-4c3f-9af8-40d75abf977a', '{\"en\": \"\", \"ru\": \"Тур на BoogelWoogel 2021, проживание без завтрака\"}', 1, 1, 1, NULL, NULL, NULL),
('ab00540a-1890-435a-b985-ad1f44ae5509', '{\"en\": \"\", \"ru\": \"Оздоровительные услуги\"}', 1, 1, 1, NULL, NULL, NULL),
('ad636fe1-1e29-43ae-981f-27f0607099dd', '{\"en\": \"\", \"ru\": \"На свежем воздухе\"}', 1, 1, 1, NULL, NULL, NULL),
('dac2dd2d-c25e-45f1-b9a0-18c6a90152bf', '{\"en\": \"\", \"ru\": \"Парковка\"}', 1, 1, 1, NULL, NULL, NULL),
('fbb97046-909c-465d-aa63-4ab3c069cd69', '{\"en\": \"\", \"ru\": \"Спорт и отдых\"}', 1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `type` smallint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint UNSIGNED NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `type`, `name`, `mime_type`, `size`, `file_name`, `file_path`, `created_at`, `updated_at`) VALUES
('013901a1-45cc-418a-81ae-038b003f4837', 100, 'blue.svg', 'image/svg+xml', 1753, '7cc0329e-8d1c-4380-a8a6-a856e20f88de.svg', '2021-04-13/7cc0329e-8d1c-4380-a8a6-a856e20f88de.svg', '2021-04-13 11:31:57', NULL),
('0a726ba5-6c40-4e53-9fa9-cc6381d274cb', 100, 'IMG04874.jpg', 'image/jpeg', 86760, '1194fa5c-e2c9-44f9-b4db-37b0bdb0a8eb.jpg', '2021-04-11/1194fa5c-e2c9-44f9-b4db-37b0bdb0a8eb.jpg', '2021-04-11 22:07:01', NULL),
('0c8f2278-6cf9-47f7-9561-3f77701e472a', 100, 'e432cff2-f278-47cb-97ba-775f54f47b49', 'image/jpeg', 385410, 'be0b22b0-e78b-4a30-8264-2eef172213ec.jpg', '2021-02-10/be0b22b0-e78b-4a30-8264-2eef172213ec.jpg', '2021-02-10 15:44:40', NULL),
('10f2cdc1-afaa-4f93-a188-b4b9b2dfaff8', 100, '698_low_k-umrikhin_UMR_9155.jpg', 'image/jpeg', 101240, 'b6050e97-9dde-4318-b1d8-a3b274a04e74.jpg', '2021-04-11/b6050e97-9dde-4318-b1d8-a3b274a04e74.jpg', '2021-04-11 22:05:52', NULL),
('175b567c-4296-4772-a167-d16664e498fa', 100, 'blue.svg', 'image/svg+xml', 897, '52be21d5-45ca-4a80-a146-a0ccb3797402.svg', '2021-04-13/52be21d5-45ca-4a80-a146-a0ccb3797402.svg', '2021-04-13 11:43:26', NULL),
('176ef1da-a857-4c3f-8d1a-c8f323742cf9', 100, 'white.svg', 'image/svg+xml', 1325, '2f3320b2-d1c7-43b0-ba3e-bfbcdb279c89.svg', '2021-04-13/2f3320b2-d1c7-43b0-ba3e-bfbcdb279c89.svg', '2021-04-13 11:46:02', NULL),
('1864a8b7-3da5-41dd-a26c-b482f0510b2f', 100, 'photo_2021-02-16_20-44-26.jpg', 'image/jpeg', 37728, 'a76a9b30-03e4-42f4-b26d-6c9649fba2e9.jpg', '2021-04-09/a76a9b30-03e4-42f4-b26d-6c9649fba2e9.jpg', '2021-04-09 19:28:06', NULL),
('1ad411fa-ce62-4868-80c9-70e7a11e6b08', 100, 'blue.svg', 'image/svg+xml', 1325, '40503137-ba24-4a18-be5e-dc1f35e156ea.svg', '2021-04-13/40503137-ba24-4a18-be5e-dc1f35e156ea.svg', '2021-04-13 11:12:28', NULL),
('209918db-5b3f-4681-85b4-25f3c36d3b12', 100, 'GT.jpg', 'image/jpeg', 695808, '61d608ec-2d25-4ba6-a490-293393732a63.jpg', '2021-04-11/61d608ec-2d25-4ba6-a490-293393732a63.jpg', '2021-04-11 22:19:39', NULL),
('220083d9-764e-43c7-958f-d0b332beccc3', 100, 'blue.svg', 'image/svg+xml', 1509, 'a3d95b8a-8ad2-4b6d-8f62-d6310aa987f5.svg', '2021-04-13/a3d95b8a-8ad2-4b6d-8f62-d6310aa987f5.svg', '2021-04-13 11:38:04', NULL),
('22a1bb74-65bd-4376-9ebd-7db8b8f147c0', 100, 'Rosa Springs (8).jpg', 'image/jpeg', 352440, '43fcd498-73b4-4fbd-b1c0-987a09c4224b.jpg', '2021-04-11/43fcd498-73b4-4fbd-b1c0-987a09c4224b.jpg', '2021-04-11 22:22:09', NULL),
('2723120e-7b85-477e-821c-0096bd6a6965', 100, 'radisson.svg', 'image/svg+xml', 3640, '63427b07-c79c-4732-b415-0a3e745abc58.svg', '2021-04-09/63427b07-c79c-4732-b415-0a3e745abc58.svg', '2021-04-09 14:33:51', NULL),
('27a32af6-9c79-4a50-af30-6d9d4a915cad', 100, 'ays.jpg', 'image/jpeg', 211421, '9eb8f244-c024-488a-a933-bcd9f42387eb.jpg', '2021-04-11/9eb8f244-c024-488a-a933-bcd9f42387eb.jpg', '2021-04-11 22:44:37', NULL),
('28b7f043-fd77-4d29-92b5-815b44d2207b', 100, 'aysdes.jpg', 'image/jpeg', 241163, '90e3a8a5-34c1-4e3e-a024-0ecc60126986.jpg', '2021-04-11/90e3a8a5-34c1-4e3e-a024-0ecc60126986.jpg', '2021-04-11 22:43:22', NULL),
('2fd855f6-b0dc-4eaf-899b-207da1f54de5', 100, 'blue.svg', 'image/svg+xml', 2002, '47aa6c91-12dd-4555-8dd1-dbda706a609b.svg', '2021-04-13/47aa6c91-12dd-4555-8dd1-dbda706a609b.svg', '2021-04-13 11:38:46', NULL),
('325cc07f-1f3d-41e2-9b87-b57e0c3615a3', 100, 'mercure.webp', 'image/webp', 2540, 'eb6f60fd-0805-4f5b-a2dd-9a7235b5a28f.webp', '2021-04-09/eb6f60fd-0805-4f5b-a2dd-9a7235b5a28f.webp', '2021-04-09 15:02:13', NULL),
('33be5c7d-3a56-45c9-9540-57763103f912', 100, 'c712ff4ecc45ed79', 'image/jpeg', 22, 'c712ff4ecc45ed79.jpg', 'section/c712ff4ecc45ed79.jpg', '2021-02-26 15:36:58', NULL),
('3614d48b-d648-48eb-a3da-cb79b891294c', 100, 'nevsk.jpg', 'image/jpeg', 109761, '49315321-8390-4b16-9c32-f3bd8e1973e1.jpg', '2021-04-11/49315321-8390-4b16-9c32-f3bd8e1973e1.jpg', '2021-04-11 22:29:24', NULL),
('3b560e37-944f-4517-aaad-97daa5c447df', 100, 'a685b86b17596221', 'image/jpeg', 22, 'a685b86b17596221.jpg', 'section/a685b86b17596221.jpg', '2021-02-26 15:33:04', NULL),
('3d255bcd-4a64-4839-abc6-0c0be33aed1b', 100, 'DSCF8415.jpg', 'image/jpeg', 102693, '4d579290-ad56-45e1-acf0-0722a7380a4c.jpg', '2021-04-11/4d579290-ad56-45e1-acf0-0722a7380a4c.jpg', '2021-04-11 22:06:08', NULL),
('3fee316a-220b-47ac-9943-dd778c6682ae', 100, 'FV7A1262-Pano-Edit (1).jpg', 'image/jpeg', 374469, 'fdfbddba-97ef-48e1-90f9-855f7509fd6c.jpg', '2021-04-11/fdfbddba-97ef-48e1-90f9-855f7509fd6c.jpg', '2021-04-11 22:39:47', NULL),
('40b2080c-e692-49ac-9862-a5b6390718cc', 100, 'park inn.webp', 'image/webp', 3550, '11854251-fe45-41ae-bd88-a26a7bf4ec06.webp', '2021-04-09/11854251-fe45-41ae-bd88-a26a7bf4ec06.webp', '2021-04-09 14:59:58', NULL),
('44c6962b-9a17-4277-9fdc-51b12e38e613', 100, 'Ski Inn SPA (top).jpg', 'image/jpeg', 405415, 'c7602da0-00ec-4081-a4c7-37f3a915b395.jpg', '2021-04-11/c7602da0-00ec-4081-a4c7-37f3a915b395.jpg', '2021-04-11 22:24:24', NULL),
('458b51b7-8d23-44b1-8163-552bd2c485ac', 100, 'white.svg', 'image/svg+xml', 1325, '878c8235-7f8b-42cb-8dbd-12c3d594b5d9.svg', '2021-04-13/878c8235-7f8b-42cb-8dbd-12c3d594b5d9.svg', '2021-04-13 11:45:27', NULL),
('4b549b96-6265-4a21-aa3d-c8f1f3dfdba6', 100, 'da89f0bd-d3c7-44fe-bde0-f252d3b4f51a', 'image/png', 988, '1fce90f6-8b86-4ab8-bc95-bf92c1a47fb4.png', '2021-02-18/1fce90f6-8b86-4ab8-bc95-bf92c1a47fb4.png', '2021-02-18 08:22:54', NULL),
('4c211792-35f1-4573-ada5-d04ba43d7d32', 100, 'blue.svg', 'image/svg+xml', 2926, '06f7ce58-c923-4441-95e7-dfa9951b3132.svg', '2021-04-13/06f7ce58-c923-4441-95e7-dfa9951b3132.svg', '2021-04-13 11:40:28', NULL),
('4efa61f7-58a6-45e7-ac94-b491edae923d', 100, 'blue.svg', 'image/svg+xml', 1509, 'cc199dcd-485a-4c2a-a300-a83e42ea98ce.svg', '2021-04-13/cc199dcd-485a-4c2a-a300-a83e42ea98ce.svg', '2021-04-13 11:26:55', NULL),
('5074e78f-4241-4db4-b900-9416091f7371', 100, 'RVRV.jpg', 'image/jpeg', 781684, '8cca2808-837c-457c-8528-0398413ec622.jpg', '2021-04-11/8cca2808-837c-457c-8528-0398413ec622.jpg', '2021-04-11 22:38:53', NULL),
('563cec23-cb49-4666-8ac1-354f21f1c9d3', 100, 'boog.jpg', 'image/jpeg', 212731, '28240f2a-869a-4a9f-b576-d774a074464f.jpg', '2021-04-11/28240f2a-869a-4a9f-b576-d774a074464f.jpg', '2021-04-11 22:45:45', NULL),
('566bc5d6-5056-4d5d-b6c7-7760372e776c', 100, 'blue.svg', 'image/svg+xml', 1301, '530fc0fe-ffbb-451d-81dc-748b220d8c38.svg', '2021-04-13/530fc0fe-ffbb-451d-81dc-748b220d8c38.svg', '2021-04-13 11:42:32', NULL),
('58926f9e-befb-46e5-921a-02ccfd34f646', 100, 'white.svg', 'image/svg+xml', 1325, '0ca424b0-643b-4682-9810-af8dfac220ad.svg', '2021-04-13/0ca424b0-643b-4682-9810-af8dfac220ad.svg', '2021-04-13 11:44:19', NULL),
('5939ac16-d351-47ec-8c52-24d6c84ae581', 100, 'blue.svg', 'image/svg+xml', 1335, 'f51159e8-5be2-4d8a-95cd-2cdc84d191ec.svg', '2021-04-13/f51159e8-5be2-4d8a-95cd-2cdc84d191ec.svg', '2021-04-13 11:44:07', NULL),
('5977f602-3c2e-431e-9b70-5047d27a819b', 100, 'lermont.jpg', 'image/jpeg', 201609, '6081250d-4478-48c0-8f7e-14dc8565d8d7.jpg', '2021-04-11/6081250d-4478-48c0-8f7e-14dc8565d8d7.jpg', '2021-04-11 22:28:09', NULL),
('5a42d46f-5483-4b25-b04a-0e746e5d0562', 100, 'Park.jpg', 'image/jpeg', 775064, '48cc66ff-0395-4de4-8fcd-0f36ff16dd77.jpg', '2021-04-11/48cc66ff-0395-4de4-8fcd-0f36ff16dd77.jpg', '2021-04-11 22:21:04', NULL),
('5e79ea11-644b-45d2-9075-cbe04ab1efad', 100, 'Rosa_241220_by@asyart-142.jpg', 'image/jpeg', 306933, '10f0c8b2-c057-4bd1-9784-d8741485e85d.jpg', '2021-04-11/10f0c8b2-c057-4bd1-9784-d8741485e85d.jpg', '2021-04-11 22:06:23', NULL),
('62b163c4-9f24-4df3-9e5f-f593b81b5706', 100, 'white.svg', 'image/svg+xml', 895, '051fe69c-d8a9-459b-b416-952e9c723e35.svg', '2021-04-13/051fe69c-d8a9-459b-b416-952e9c723e35.svg', '2021-04-13 11:43:38', NULL),
('6643de64-edaa-4039-8fff-6376de382b05', 100, 'photo_2021-02-16_20-44-26.jpg', 'image/jpeg', 37728, 'd4323d9a-53dc-4e88-8070-30083cff4ec3.jpg', '2021-04-09/d4323d9a-53dc-4e88-8070-30083cff4ec3.jpg', '2021-04-09 14:39:38', NULL),
('66976728-a3ba-44d9-ba9f-b62f6687d782', 100, 'ЛКО.jpg', 'image/jpeg', 14632, '543bd3d3-07a9-435c-ba81-a84130a15d43.jpg', '2021-04-09/543bd3d3-07a9-435c-ba81-a84130a15d43.jpg', '2021-04-09 19:57:54', NULL),
('66b63612-c3a3-4dce-9932-727059ca2c07', 100, 'white.svg', 'image/svg+xml', 1325, 'e9d2e902-3abf-4d66-956c-864a3f65ab6a.svg', '2021-04-13/e9d2e902-3abf-4d66-956c-864a3f65ab6a.svg', '2021-04-13 11:45:00', NULL),
('6c7b246b-b9d0-404b-9e71-d1b526c79ad3', 100, '7635f79373ce0e12.jpg', 'image/jpeg', 106294, '2ba4604d-9554-4b00-809f-32df9e64fc1b.jpg', '2021-04-05/2ba4604d-9554-4b00-809f-32df9e64fc1b.jpg', '2021-04-05 18:25:22', NULL),
('6eefedbc-35c9-42a3-823b-f68239bb56ee', 100, 'Green Flow Winter (6).jpg', 'image/jpeg', 187016, '94b7e9a8-34b8-4aaf-95b3-12d10ada1cf8.jpg', '2021-04-11/94b7e9a8-34b8-4aaf-95b3-12d10ada1cf8.jpg', '2021-04-11 22:25:07', NULL),
('71fda3ad-bb21-49bc-81fd-df2ca9512fc1', 100, '31b20d01cee16894', 'image/jpeg', 22, '31b20d01cee16894.jpg', 'section/31b20d01cee16894.jpg', '2021-02-26 15:39:10', NULL),
('751651bb-240c-4a75-a413-926a5583a42a', 100, 'b36fec70-6499-41e4-be53-083a60231c73', 'image/jpeg', 386724, '3498389c-1ae5-49b6-866d-4f9510127e1c.jpg', '2021-02-10/3498389c-1ae5-49b6-866d-4f9510127e1c.jpg', '2021-02-10 15:44:39', NULL),
('77e77aa3-e4c9-4755-8a99-0c4115d3b3f7', 100, 'kizhi.jpg', 'image/jpeg', 131050, '3b7a531f-4911-464c-b73a-8393f5c8df5d.jpg', '2021-04-11/3b7a531f-4911-464c-b73a-8393f5c8df5d.jpg', '2021-04-11 22:28:47', NULL),
('7af3a9e8-cfe2-48a2-ba68-b305d6c73850', 100, 'bf5dc847-523a-417a-8afc-b47e81a6ab80', 'image/jpeg', 493766, 'd7f83f82-fbd5-4863-b232-cc889167fea1.jpg', '2021-02-10/d7f83f82-fbd5-4863-b232-cc889167fea1.jpg', '2021-02-10 15:44:38', NULL),
('7dfcf866-06ce-4af6-9a89-40a6106baf66', 100, '7635f79373ce0e12.jpg', 'image/jpeg', 106294, 'e49bb0a2-bac7-4ed4-a3e1-dd2eeec3e660.jpg', '2021-04-05/e49bb0a2-bac7-4ed4-a3e1-dd2eeec3e660.jpg', '2021-04-05 18:25:14', NULL),
('846d0fa4-a3cd-4665-967b-57b18319b2d1', 100, 'AzFr11.jpg', 'image/jpeg', 659962, 'd68a32c0-77c1-49e7-ab55-74771f35f543.jpg', '2021-04-11/d68a32c0-77c1-49e7-ab55-74771f35f543.jpg', '2021-04-11 22:32:47', NULL),
('8a396270-2c47-4cbe-829d-90beaaa0203c', 100, 'blue.svg', 'image/svg+xml', 920, 'c83e0578-117b-41a0-8b29-29268743d3df.svg', '2021-04-13/c83e0578-117b-41a0-8b29-29268743d3df.svg', '2021-04-13 11:42:01', NULL),
('8b83c8e1-a21d-4b1b-a5b7-767562ff8f16', 100, 'white.svg', 'image/svg+xml', 1988, '12afe599-b1fc-4119-976b-90bc5d77aa51.svg', '2021-04-13/12afe599-b1fc-4119-976b-90bc5d77aa51.svg', '2021-04-13 11:39:19', NULL),
('8cb5fb96-4875-4396-840d-e488905733d9', 100, '7635f79373ce0e12.jpg', 'image/jpeg', 106294, '44e11050-2cec-47bb-860c-57653e41c2c6.jpg', '2021-04-05/44e11050-2cec-47bb-860c-57653e41c2c6.jpg', '2021-04-05 18:25:30', NULL),
('8d26384a-5098-4f8a-b3d8-d9a8832e4747', 100, 'c38ab0da5457bcb9', 'image/jpeg', 22, 'c38ab0da5457bcb9.jpg', 'section/c38ab0da5457bcb9.jpg', '2021-02-26 15:34:14', NULL),
('90ab960e-89bb-4a4b-a8fd-f346b9d520c1', 100, 'SkiInn.jpg', 'image/jpeg', 461173, 'dccd6d22-b724-47b4-aca1-8c6d6e08e62f.jpg', '2021-04-11/dccd6d22-b724-47b4-aca1-8c6d6e08e62f.jpg', '2021-04-11 22:35:18', NULL),
('96d76c1f-128b-44b9-b0a0-3558b61b2ddd', 100, 'b152214f9ad53523', 'image/jpeg', 22, 'b152214f9ad53523.jpg', 'section/b152214f9ad53523.jpg', '2021-02-26 15:40:15', NULL),
('97508267-2a56-4731-8d0a-6a29c6e92ac1', 100, 'white.svg', 'image/svg+xml', 1295, '30442797-e51c-4973-a7ea-9b13fb195594.svg', '2021-04-13/30442797-e51c-4973-a7ea-9b13fb195594.svg', '2021-04-13 11:43:09', NULL),
('9a09b0d1-3a2d-4105-accd-ecaa522b8f2c', 100, 'Valset Premium Winter (4).jpg', 'image/jpeg', 539174, '955a27fd-9907-4a21-bb9f-3a258b5ba037.jpg', '2021-04-11/955a27fd-9907-4a21-bb9f-3a258b5ba037.jpg', '2021-04-11 22:33:49', NULL),
('a29d549d-c47d-4bfa-88df-c6e6fdbbb4a3', 100, 'white.svg', 'image/svg+xml', 1325, '954dbb00-5b07-4a56-b171-c5cbec700424.svg', '2021-04-13/954dbb00-5b07-4a56-b171-c5cbec700424.svg', '2021-04-13 11:46:38', NULL),
('a4741b9e-e677-4fb1-8187-84ff8b52e23d', 100, 'bcb299f3e8fc1e60', 'image/jpeg', 232, 'bcb299f3e8fc1e60.jpg', 'section/bcb299f3e8fc1e60.jpg', '2021-02-26 15:36:09', NULL),
('a69e08e9-9463-412f-9b9c-7bcf1ee5ed60', 100, '0df50059-9979-4acb-961e-434843c65a70', 'image/png', 1114, '1ee34198-53e6-4fef-8620-d65e0c2cc4c8.png', '2021-02-18/1ee34198-53e6-4fef-8620-d65e0c2cc4c8.png', '2021-02-18 08:22:54', NULL),
('aa6c2f5c-eba0-4e75-8a1b-70b8aabfafd8', 100, 'ЛКО.jpg', 'image/jpeg', 14632, '0ba82f9f-98bf-4219-b7d8-e96e38f2012d.jpg', '2021-04-09/0ba82f9f-98bf-4219-b7d8-e96e38f2012d.jpg', '2021-04-09 09:54:48', NULL),
('ab89a4a8-5c96-4589-9161-735f6c0f687e', 100, '7e7ed8dd-86cf-4ac4-b9f5-1452247aaf30', 'image/jpeg', 193144, '3d5225d0-c0a1-4cad-b0a8-a2cd3dbfec70.jpg', '2021-02-10/3d5225d0-c0a1-4cad-b0a8-a2cd3dbfec70.jpg', '2021-02-10 15:44:39', NULL),
('ad048437-1534-44dc-a74a-ac16132cf79c', 100, 'c712ff4ecc45ed79.jpg', 'image/jpeg', 210044, 'e989fe62-6542-4ac2-809e-78ffbc3a10d5.jpg', '2021-03-31/e989fe62-6542-4ac2-809e-78ffbc3a10d5.jpg', '2021-03-31 13:12:27', NULL),
('ad5c612d-ada9-438c-a29d-3936251fe85b', 100, '5B5A6835.jpg', 'image/jpeg', 259727, '0fb7f5bb-7578-4bd2-9891-f0b6e2d7f7fa.jpg', '2021-04-11/0fb7f5bb-7578-4bd2-9891-f0b6e2d7f7fa.jpg', '2021-04-11 22:05:41', NULL),
('ae74e939-8259-48e8-9992-9ad030d347a2', 100, '9cbf5c46ee42d4b5', 'image/jpeg', 22, '9cbf5c46ee42d4b5.jpg', 'section/9cbf5c46ee42d4b5.jpg', '2021-02-26 15:34:14', NULL),
('b00b96a7-3676-4dd6-8e92-318034f9125f', 100, 'b9b7639c-5b0f-4ed5-9398-4a7bf8fe74a3', 'image/jpeg', 697397, 'f5e1983d-9aba-45b7-b00e-8d05a014ffd6.jpg', '2021-02-10/f5e1983d-9aba-45b7-b00e-8d05a014ffd6.jpg', '2021-02-10 15:44:38', NULL),
('b1680aab-18b7-444a-b4d0-e830771e0883', 100, 'aaa85fa1-1a9e-4b00-b472-7f9e1c7ee6d5', 'image/jpeg', 398674, 'f724e20e-fabb-403d-8392-57a40bedf22c.jpg', '2021-02-10/f724e20e-fabb-403d-8392-57a40bedf22c.jpg', '2021-02-10 15:44:39', NULL),
('b212cc4e-03b2-47df-9c3c-95bda13eee0f', 100, '62d3a8fd55371f5f', 'image/jpeg', 22, '62d3a8fd55371f5f.jpg', 'section/62d3a8fd55371f5f.jpg', '2021-02-26 15:38:12', NULL),
('b42047b3-01a8-49be-a501-bf9de33d5437', 100, 'c712ff4ecc45ed79.jpg', 'image/jpeg', 210044, '224d511a-f9a9-4352-9134-1c58478d872f.jpg', '2021-03-31/224d511a-f9a9-4352-9134-1c58478d872f.jpg', '2021-03-31 13:12:19', NULL),
('b4a2d12c-a5c1-4f8c-93c0-6b868af1d979', 100, 'MER2.jpg', 'image/jpeg', 674279, '40760a83-6d90-4efd-a4e0-fa7e7ff0f4c0.jpg', '2021-04-11/40760a83-6d90-4efd-a4e0-fa7e7ff0f4c0.jpg', '2021-04-11 22:22:54', NULL),
('b8ad16db-b4f5-464a-b9a5-1ba78339132f', 100, '4ffac46e-d6cd-45e5-b5ec-03093e1aaba6', 'image/png', 1417, '80ccf042-f552-4964-9226-c713d8c0cbcc.png', '2021-02-18/80ccf042-f552-4964-9226-c713d8c0cbcc.png', '2021-02-18 08:22:54', NULL),
('b9a51db0-7fa1-4f53-9100-f0154ded9b59', 100, 'blue.svg', 'image/svg+xml', 2002, 'e2e5b52c-7667-450b-b276-1119a33ddf1c.svg', '2021-04-13/e2e5b52c-7667-450b-b276-1119a33ddf1c.svg', '2021-04-13 11:34:24', NULL),
('bcc5cb7a-202c-4cbb-817c-bfa89c1b4a50', 100, '35b02700-ec76-40b4-ab4c-bc7a8b79b0ad', 'image/png', 887, '7cddac0e-0db3-460d-bbe0-db5110bae931.png', '2021-02-18/7cddac0e-0db3-460d-bbe0-db5110bae931.png', '2021-02-18 08:22:54', NULL),
('c09ceec6-fca6-45e4-9250-e62cc3d3c09e', 100, '_MG_7521.jpg', 'image/jpeg', 67889, '9b2552b0-9fc8-4bd8-b367-e5dbbb5d161f.jpg', '2021-04-11/9b2552b0-9fc8-4bd8-b367-e5dbbb5d161f.jpg', '2021-04-11 22:05:27', NULL),
('c23646f7-0f16-44b4-971a-48ac29295150', 100, 'white.svg', 'image/svg+xml', 1988, '5ada12d2-73f8-4eea-946d-6f1a208add1e.svg', '2021-04-13/5ada12d2-73f8-4eea-946d-6f1a208add1e.svg', '2021-04-13 11:36:22', NULL),
('c371afb4-2747-4a3e-af1d-bd877da8cc94', 100, 'white.svg', 'image/svg+xml', 2914, '6f990dfe-0ef9-48c8-b651-b452c33cac83.svg', '2021-04-13/6f990dfe-0ef9-48c8-b651-b452c33cac83.svg', '2021-04-13 11:41:09', NULL),
('c388d324-2959-48b3-ac10-a4f50b475f46', 100, 'ef14cab1-788b-4511-b59c-a030d7bebca1', 'image/jpeg', 362000, '7fabddbd-ad5d-43cb-858c-4a7b0c82337d.jpg', '2021-02-10/7fabddbd-ad5d-43cb-858c-4a7b0c82337d.jpg', '2021-02-10 15:44:38', NULL),
('c4b3e02e-46ea-438a-a6f5-720446ed862d', 100, 'blue.svg', 'image/svg+xml', 1040, '8d02a83d-28af-4ca3-bfdb-eaa7a34c0f26.svg', '2021-04-13/8d02a83d-28af-4ca3-bfdb-eaa7a34c0f26.svg', '2021-04-13 11:47:30', NULL),
('c69841e6-efdf-45cd-abc5-e0397a513fb8', 100, 'white.svg', 'image/svg+xml', 1988, '3b18ae2f-0878-4ed6-a1b6-4981520bb11e.svg', '2021-04-13/3b18ae2f-0878-4ed6-a1b6-4981520bb11e.svg', '2021-04-13 11:38:52', NULL),
('c8db13ec-8805-46d9-8074-3b0f5e4b1819', 100, 'white.svg', 'image/svg+xml', 1988, 'd49a7719-959f-49fe-ab91-4039194e563a.svg', '2021-04-13/d49a7719-959f-49fe-ab91-4039194e563a.svg', '2021-04-13 11:37:10', NULL),
('c9d0acde-73c6-406c-ae23-5cbb04f63024', 100, 'mercure.webp', 'image/webp', 2540, '0e3575ac-e037-4bc8-b401-cc38a2c07d75.webp', '2021-04-09/0e3575ac-e037-4bc8-b401-cc38a2c07d75.webp', '2021-04-09 14:34:06', NULL),
('cc5a5327-0971-408b-bad9-527d09e327bf', 100, 'white.svg', 'image/svg+xml', 1743, 'fcaacd69-df03-4193-a781-eedaf71e7dd0.svg', '2021-04-13/fcaacd69-df03-4193-a781-eedaf71e7dd0.svg', '2021-04-13 11:32:19', NULL),
('cd359a84-84fa-414c-807f-d72d828847cc', 100, 'Rad3.jpg', 'image/jpeg', 685961, 'efa37fd8-7c4c-4c0e-ab55-28a878b80014.jpg', '2021-04-11/efa37fd8-7c4c-4c0e-ab55-28a878b80014.jpg', '2021-04-11 22:17:10', NULL),
('cfb9b388-44d0-474c-b8a9-136f361a854b', 100, 'Riders Lodge 2.jpg', 'image/jpeg', 471337, 'a08116a9-68ff-476a-91fd-7b694af9ccb0.jpg', '2021-04-11/a08116a9-68ff-476a-91fd-7b694af9ccb0.jpg', '2021-04-11 22:36:18', NULL),
('d298bd24-a164-481b-aaef-cfc2c277939c', 100, '3ff2b6b8-02e4-41f3-b865-7bb27fe3f1c1', 'image/jpeg', 591181, '0f4f5f5b-3514-43ef-af68-7ab312009ab3.jpg', '2021-02-10/0f4f5f5b-3514-43ef-af68-7ab312009ab3.jpg', '2021-02-10 15:44:37', NULL),
('d3c1c9be-b301-479f-b75c-3ceb18bfc556', 100, 'blue.svg', 'image/svg+xml', 1244, 'aa5f0ffd-6c01-49b3-9f51-5510bce9916b.svg', '2021-04-13/aa5f0ffd-6c01-49b3-9f51-5510bce9916b.svg', '2021-04-13 11:28:38', NULL),
('d7e07948-d263-4421-97ba-119b7f3382aa', 100, 'white.svg', 'image/svg+xml', 1242, 'ebc34f08-45ac-4b8f-9980-f31162dd3645.svg', '2021-04-13/ebc34f08-45ac-4b8f-9980-f31162dd3645.svg', '2021-04-13 11:38:15', NULL),
('d80a6ead-02f5-44a3-b250-25494542c77f', 100, '_MG_6485.jpeg', 'image/jpeg', 61457, '7a059ae1-d7ea-4191-8d1b-d90e95721dca.jpeg', '2021-04-11/7a059ae1-d7ea-4191-8d1b-d90e95721dca.jpeg', '2021-04-11 22:04:46', NULL),
('df5de035-6906-4c96-8cf4-a5baabdaaf28', 100, 'white.svg', 'image/svg+xml', 1507, '0be8d1b0-0158-4a81-906a-6eba5b158381.svg', '2021-04-13/0be8d1b0-0158-4a81-906a-6eba5b158381.svg', '2021-04-13 11:27:42', NULL),
('eb2f118d-4b0f-4d6b-8d8c-4a67a477a3aa', 100, 'white.svg', 'image/svg+xml', 1242, 'd8685858-21ec-4329-8f16-7c972a3c9297.svg', '2021-04-13/d8685858-21ec-4329-8f16-7c972a3c9297.svg', '2021-04-13 11:29:01', NULL),
('ef679123-5b15-4cff-b8d7-c60fad273b3b', 100, 'shale.jpg', 'image/jpeg', 662661, '5e0286ca-eda6-4969-98b9-9eabc20a9aa0.jpg', '2021-04-11/5e0286ca-eda6-4969-98b9-9eabc20a9aa0.jpg', '2021-04-11 22:34:38', NULL),
('f1a7271d-a7f5-4845-8914-17aa4399e71a', 100, 'blue.svg', 'image/svg+xml', 956, '52e4371e-8908-4297-a7c4-fd1a01128a6e.svg', '2021-04-13/52e4371e-8908-4297-a7c4-fd1a01128a6e.svg', '2021-04-13 11:47:10', NULL),
('f1f9cb20-eb6e-44bc-bd21-2fe7b94801df', 100, 'blue.svg', 'image/svg+xml', 1217, '72ac8baf-f5b9-4b48-980e-6a223424d1b9.svg', '2021-04-13/72ac8baf-f5b9-4b48-980e-6a223424d1b9.svg', '2021-04-13 11:33:09', NULL),
('f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 100, '7635f79373ce0e12', 'image/jpeg', 22, '7635f79373ce0e12.jpg', 'section/7635f79373ce0e12.jpg', '2021-02-26 15:40:01', NULL),
('f75079c6-d214-497d-be6f-3c96fe6d6ece', 100, '4af262a8-d4bb-4063-8908-93b4bbb29552', 'image/jpeg', 396328, '50db1fa9-b632-4047-a9ef-c49b5b941d00.jpg', '2021-02-10/50db1fa9-b632-4047-a9ef-c49b5b941d00.jpg', '2021-02-10 15:44:39', NULL),
('f7adabed-6af0-460c-941d-73a73633a684', 100, 'Tulip Inn (5).jpg', 'image/jpeg', 518551, 'b83a24a3-fff9-4875-adc9-dbe752003614.jpg', '2021-04-11/b83a24a3-fff9-4875-adc9-dbe752003614.jpg', '2021-04-11 22:30:02', NULL),
('f9d95733-e640-4c37-b226-ac82160bf611', 100, '28.jpg', 'image/jpeg', 614628, '06dd2b94-f1d0-4502-8619-fb0870d0f1e1.jpg', '2021-04-11/06dd2b94-f1d0-4502-8619-fb0870d0f1e1.jpg', '2021-04-11 22:38:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `food_type`
--

CREATE TABLE `food_type` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `sort` int DEFAULT NULL,
  `is_active` tinyint UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food_type`
--

INSERT INTO `food_type` (`id`, `name`, `sort`, `is_active`) VALUES
('18924058-1e6e-4bbc-a785-e5505515f74c', '{\"en\": \"\", \"ru\": \"Полупансион\"}', NULL, 1),
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"Завтрак\"}', NULL, 1),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"Без питания\"}', NULL, 1),
('6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"Завтрак\"}', NULL, 1),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"Полупансион\"}', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `star_count` int DEFAULT NULL,
  `location_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` json DEFAULT NULL,
  `logo_svg` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `links` json DEFAULT NULL,
  `lat` decimal(9,6) DEFAULT NULL,
  `lon` decimal(9,6) DEFAULT NULL,
  `type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotel_brand_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `food_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_in_time` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `check_out_time` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `cancel_prepayment` json DEFAULT NULL,
  `extra_beds` json DEFAULT NULL,
  `pets` json DEFAULT NULL,
  `extra_info` json DEFAULT NULL,
  `payments` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `description`, `star_count`, `location_id`, `address`, `logo_svg`, `logo`, `links`, `lat`, `lon`, `type_id`, `hotel_brand_id`, `food_type_id`, `check_in_time`, `check_out_time`, `cancel_prepayment`, `extra_beds`, `pets`, `extra_info`, `payments`) VALUES
('0689aff9-eb6d-48c6-9b14-b1f4423523cf', '{\"ru\": \"Rosa Village\"}', '{\"ru\": \"Rosa Village – уютный коттеджный комплекс на высоте 1 100 м, расположенный на территории Горной Олимпийской деревни круглогодичного курорта «Роза Хутор».\"}', 1, '266f29e8-61c0-4b83-9ffd-321a46237034', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Сулимовка, 27.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.417758', '39.946194', '33be2c7d-3a56-45c9-9540-57763103f912', '18924058-1e6e-4bbc-a785-e5505515f74c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('12bfb004-5dc5-43bb-b0e4-860ec7b2cce4', '{\"ru\": \"Отель «28»\"}', '{\"ru\": \"Отель «28» – это комфортный отдых по доступной цене. Отель расположен на высоте 1 100 м в окружении гор, на территории Горной Олимпийской деревни на круглогодичном горном курорте «Роза Хутор».\"}', 2, '4538fd97-6bef-458a-8cdb-81e5383aee70', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Сулимовка, 7.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.632117', '40.316102', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('18924058-1e6e-4bbc-a785-e5505515f74c', '{\"ru\": \"Отель Radisson Rosa Khutor 5\"}', '{\"ru\": \"Единственный пятизвездочный отель на курорте Роза Хутор. Отель Radisson Rosa Khutor 5* предлагает первоклассный сервис, комфортное проживание и широкий спектр дополнительных услуг.\"}', 5, '8432c645-a344-4b45-917e-83ac51b4eeb9', '{\"ru\": \"г. Сочи, п. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, Набережная Панорама, 4.\"}', '2723120e-7b85-477e-821c-0096bd6a6965', NULL, NULL, '43.672792', '40.297525', '18924058-1e6e-4bbc-a785-e5505515f74c', '18924058-1e6e-4bbc-a785-e5505515f74c', '18924058-1e6e-4bbc-a785-e5505515f74c', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1b2cb1af-423b-48d9-a079-b8c643d73809', '{\"ru\": \"Бутик-отель Кижи 4*\"}', '{\"ru\": \"Переступая порог добротного сруба, словно попадаешь в палаты Ивана Грозного. Тяжелая мебель из массива, роспись русской вязью под потолками, авторские светильники из латуни и золотые рыбки на ковре под ногами. Бонус – вид на горную реку Мзымта из окон горницы. И это все Бутик-отель Кижи.\"}', 4, 'e43ac171-1728-4ece-b9d0-ce86d98e2a72', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, наб. Лаванда, дом 9.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.671834', '40.295854', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2b35e14e-e9e6-470d-baef-0e2b71993ecd', '{\"ru\": \"Бутик-отель Невский 4*\"}', '{\"ru\": \"4-х звездочный Бутик-отель на территории Интерактивного парк-музея Моя Россия предлагает своим гостям самый разнообразный номерной фонд от доступных стандартов до шикарного двухкомнатного люкса. Интерьеры номеров, созданные по дизайн-проекту Валентина Юдашкина, оборудованы всем необходимым для комфортного отдыха круглый год.\"}', 4, '4538fd97-6bef-458a-8cdb-81e5383aee70', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, наб. Лаванда, дом 9.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.671834', '40.295854', '18924058-1e6e-4bbc-a785-e5505515f74c', '18924058-1e6e-4bbc-a785-e5505515f74c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('31fa0a0f-fbcc-444c-90de-283431a8c980', '{\"ru\": \"Ski Inn SPA Hotel 4*\"}', '{\"ru\": \"Ski Inn SPA Hotel 4* – это главный корпус отеля Rosa Ski Inn, прошедший полную реновацию. Это все то же любимое гостями место с теплой семейной атмосферой, в шаге от подъемника и склонов Роза Хутор, но в сочетании с абсолютно новыми интерьерами и современной мебелью, спроектированной специально для поклонников и профессионалов горнолыжного отдыха.\"}', 4, '266f29e8-61c0-4b83-9ffd-321a46237034', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Пихтовая аллея, дом 1.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.660963', '40.317675', '6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"Отель Golden Tulip Rosa Khutor 4*\"}', '{\"ru\": \"Откройте для себя совершенство отдыха в горах с отелем международной сети Golden Tulip, который расположен в самом центре курорта «Роза Хутор», на солнечном берегу реки Мзымта.\"}', 4, '266f29e8-61c0-4b83-9ffd-321a46237034', '{\"ru\": \"г. Сочи, п. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, Набережная Панорама, 3.\"}', '2723120e-7b85-477e-821c-0096bd6a6965', NULL, NULL, '43.672231', '40.294992', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"AYS Let It Snow Hotel\"}', '{\"ru\": \"AYS Hotel — это экстремально-позитивный молодёжный отель для тех, кто хочет сэкономить на проживании, но не на впечатлениях.\"}', 1, '65e75bd9-a3ab-45eb-bfcd-12a17c6f8bb6', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Сулимовка, 11.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.664171', '40.315141', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('64090b04-8065-4eec-8e89-71af65f8f44b', '{\"ru\": \"Бутик-отель Лермонтов 4*\"}', '{\"ru\": \"4-х звездочный бутик-отель на территории интерактивного парка-музея Моя Россия. Снаружи это кавказская крепость на берегу горной реки Мзымты, а внутри – современный комфортабельный отель. Здание построено в традиционном для Северного Кавказа стиле с использованием натурального камня и дерева, а общие пространства и номера стилизованы под интерьер конца XIX, начала XX века.\"}', 4, 'e43ac171-1728-4ece-b9d0-ce86d98e2a72', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, наб. Лаванда, дом 9.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.671834', '40.295854', '6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"Отель Park Inn by Radisson Rosa Khutor 4\"}', '{\"ru\": \"Park Inn by Radisson 4* – семейный, стильный и яркий отель, расположенный на курорте «Роза Хутор» на набережной реки Мзымта, в шаговой доступности от канатных дорог «Олимпия» и «Стрела».\"}', 4, '4b24d53f-6de8-49e3-8f31-eefe2b4ab941', '{\"ru\": \"г. Сочи, п. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, Набережная Лаванда, 5.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.671560', '40.294264', '6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('7aae156e-2f1f-4412-b8de-c7d983c932df', '{\"ru\": \"Отель RIDERS LODGE\"}', '{\"ru\": \"RIDERS LODGE – современный отель с дружелюбной атмосферой, концептуальным дизайном и с бесконечной чередой разнообразных активностей. Это первый в России отель для райдеров и любителей активного отдыха, где можно отлично отдохнуть в компании друзей или в кругу семьи, а также со своими домашними питомцами.\"}', 2, 'af634681-1c69-43a2-ba03-ede8fc496717', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Медовея, 6.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.660715', '40.320325', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('820d148a-0944-44d7-addc-fba98d6c9de7', '{\"ru\": \"Отель Rosa Ski Inn\"}', '{\"ru\": \"Отель находится на курорте «Роза Хутор» на высоте 1100 метров в Горной Олимпийской деревне – одном из знаковых объектов Олимпийских Зимних Игр 2014 года в Сочи.\"}', 2, '663e802b-64da-4820-bdad-47b0483702fd', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Пихтовая аллея, дом 1.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.660963', '40.317675', '6582b0da-b8d0-4618-a763-864358706ce1', '33be2c1d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('99edd926-5e39-42bc-afc3-a1a360e1ce3f', '{\"ru\": \"Отель BoogelWoogel Bar&Hotel\"}', '{\"ru\": \"BoogelWoogel Bar&Hotel – небольшой и шумный отель с баром, который можно полностью снять большой компанией. Вместимость отеля 20 человек.\"}', 2, '4b24d53f-6de8-49e3-8f31-eefe2b4ab941', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Сулимовка, дом 24.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.662665', '40.316974', '18924058-1e6e-4bbc-a785-e5505515f74c', '33be2c1d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('9a456b69-19bc-41c4-8b48-b0a9b3ec4c94', '{\"ru\": \"Апартаменты Valset Apartments by AZIMUT\"}', '{\"ru\": \"Многокомнатные апартаменты Valset отлично подходят для семей с детьми и больших компаний. Каждый номер оборудован кухней, со всем необходимым как для краткосрочного, так и для долгосрочного размещения. От отдалённых корпусов (4,5) курсирует бесплатный шаттл до подъёмников.\"}', 3, '8432c645-a344-4b45-917e-83ac51b4eeb9', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, корпуса апартаментов рассредоточены вдоль набережной реки Мзымта, расстояние до канатной дороги Олимпия от 300 до 1000 м.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.632117', '40.316102', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('9e889411-b12f-45c0-93ef-05ecd58e900d', '{\"ru\": \"Green Flow Hotel 4*\"}', '{\"ru\": \"Новый well-being отель Green Flow 4* со SPA-комплексом располагается на высоте 1100 метров над уровнем моря, на территории Горной Олимпийской деревни, в окружении потрясающей красоты горных вершин. Отель Green Flow входит в международную ассоциацию Healing Hotels of the World.\"}', 4, '4b24d53f-6de8-49e3-8f31-eefe2b4ab941', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Сулимовка, 9.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.664661', '40.314737', '33be2c1d-3a56-45c9-9540-57763103f912', '18924058-1e6e-4bbc-a785-e5505515f74c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a7d927f1-d25c-47fd-8483-72ba8a42053a', '{\"ru\": \"AYS Design Hotel\"}', '{\"ru\": \"AYS DESIGN – красочный отель в Горной Олимпийской деревне. Главная особенность – дизайнерские номера с граффити, которые украшают стены и создают позитивную атмосферу для отдыха.\"}', 2, 'af634681-1c69-43a2-ba03-ede8fc496717', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Сулимовка, дом 5.\"}', '2723120e-7b85-477e-821c-0096bd6a6965', NULL, NULL, '43.663695', '40.314432', '33be2c1d-3a56-45c9-9540-57763103f912', '18924058-1e6e-4bbc-a785-e5505515f74c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a907810e-1223-48ff-a22d-3878f28b6905', '{\"ru\": \"ПриютПанды\"}', '{\"ru\": \"Отель «ПриютПанды» расположен в Горной Олимпийской деревне. На территории отеля к услугам гостей кафе, бесплатный Wi-Fi, кухня, комната для хранения и сушки лыж, конференц-рум, оригинальное пространство для отдыха.\"}', 1, 'af634681-1c69-43a2-ba03-ede8fc496717', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Сулимовка, 8.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.663376', '40.315411', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('aeb75597-3e5e-47d6-9742-d5109eb3db55', '{\"ru\": \"Комплекс Rosa Chalet\"}', '{\"ru\": \"Уникальный комплекс Роза Шале расположен на высоте 1100 м, относится к отелю типа ski-in/ski-out. Это один из самых высокогорных комплексов класса Premium.\"}', 4, 'af634681-1c69-43a2-ba03-ede8fc496717', '{\"ru\": \"г. Сочи, п. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, Пихтовая Аллея, 7.\"}', '2723120e-7b85-477e-821c-0096bd6a6965', NULL, NULL, '43.658002', '40.317163', 'b15b4686-aa1b-4606-97ca-6f241554bd82', '6582b0da-b8d0-4618-a763-864358706ce1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"Отель Mercure Rosa Khutor 4\"}', '{\"ru\": \"Теплая семейная атмосфера отеля, уютные номера и велнес-центр с бассейном со стеклянной крышей круглый год привлекают любителей гор. Здесь можно провести волшебный weekend вдвоем или полноценный отпуск в веселой компании друзей.\"}', 4, '663e802b-64da-4820-bdad-47b0483702fd', '{\"ru\": \"г. Сочи, п. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, Набережная Лаванда, 4.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.671951', '40.293034', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('f09bf7ed-2a7d-4da5-8703-05a52f8b1ceb', '{\"ru\": \"Medical Spa отель Rosa Springs 4*\"}', '{\"ru\": \"Отель Rosa Springs 4* – самый высокогорный санаторий в России и первый бальнеологический отель в горах Сочи. Это идеальное место для восстановления физических и душевных сил.\"}', 4, '266f29e8-61c0-4b83-9ffd-321a46237034', '{\"ru\": \"г. Сочи, п. Эсто-Cадок, курорт «Роза Хутор», Горная Олимпийская деревня 1100 м, ул. Медовея, 4.\"}', '2723120e-7b85-477e-821c-0096bd6a6965', NULL, NULL, '43.660748', '40.318878', '6582b0da-b8d0-4618-a763-864358706ce1', '33be2c1d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('f6636ffb-d162-4d6b-83da-b8dc586e7ece', '{\"ru\": \"Отель Tulip Inn Rosa Khutor 3*\"}', '{\"ru\": \"Tulip Inn 3* прекрасно подойдет как для индивидуальных путешественников, так и для семей с детьми, компаний друзей. Конференц-залы гостиницы оснащены всем необходимым для проведения семинаров и тренингов.\"}', 3, 'af634681-1c69-43a2-ba03-ede8fc496717', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, наб. Панорама, 2.\"}', '40b2080c-e692-49ac-9862-a5b6390718cc', NULL, NULL, '43.672505', '40.293941', '18924058-1e6e-4bbc-a785-e5505515f74c', '33be2c1d-3a56-45c9-9540-57763103f912', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('fbe3a871-69fe-432e-9a76-d5119bd84c7d', '{\"ru\": \"Отель AZIMUT Freestyle 3*\"}', '{\"ru\": \"AZIMUT Freestyle 3* располагает финской сауной, бесплатной к посещению, где можно отдохнуть после активного дня в горах. Также в зимний сезон от отеля курсирует бесплатный шаnтл до подъемников.\"}', 3, '4b24d53f-6de8-49e3-8f31-eefe2b4ab941', '{\"ru\": \"г. Сочи, пос. Эсто-Cадок, курорт «Роза Хутор», Роза Долина 560 м, наб. Полянка, 4.\"}', '325cc07f-1f3d-41e2-9b87-b57e0c3615a3', NULL, NULL, '43.674748', '40.288875', '18924058-1e6e-4bbc-a785-e5505515f74c', '6582b0da-b8d0-4618-a763-864358706ce1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_age_range`
--

CREATE TABLE `hotel_age_range` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age_from` tinyint UNSIGNED NOT NULL,
  `age_to` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_age_range`
--

INSERT INTO `hotel_age_range` (`id`, `hotel_id`, `age_from`, `age_to`) VALUES
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', 18, 90),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', 7, 30);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_bed_type`
--

CREATE TABLE `hotel_bed_type` (
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bed_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_extra_place_type`
--

CREATE TABLE `hotel_extra_place_type` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_extra_place_type`
--

INSERT INTO `hotel_extra_place_type` (`id`, `hotel_id`, `name`) VALUES
('18924058-1e6e-4bbc-a785-e5505515f74c', '18924058-1e6e-4bbc-a785-e5505515f74c', '{\"ru\": \"тохта\"}'),
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"раскладушка\"}'),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"детская кроватка\"}'),
('6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"раскладушка большая\"}'),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"диван раскладной\"}');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_extra_place_type_age_range`
--

CREATE TABLE `hotel_extra_place_type_age_range` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_extra_place_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_age_range_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_extra_place_type_age_range`
--

INSERT INTO `hotel_extra_place_type_age_range` (`id`, `hotel_extra_place_type_id`, `hotel_age_range_id`) VALUES
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912'),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_facility`
--

CREATE TABLE `hotel_facility` (
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facility_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_photo`
--

CREATE TABLE `hotel_photo` (
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_photo`
--

INSERT INTO `hotel_photo` (`hotel_id`, `photo_id`) VALUES
('0689aff9-eb6d-48c6-9b14-b1f4423523cf', '5074e78f-4241-4db4-b900-9416091f7371'),
('12bfb004-5dc5-43bb-b0e4-860ec7b2cce4', 'f9d95733-e640-4c37-b226-ac82160bf611'),
('18924058-1e6e-4bbc-a785-e5505515f74c', 'cd359a84-84fa-414c-807f-d72d828847cc'),
('1b2cb1af-423b-48d9-a079-b8c643d73809', '77e77aa3-e4c9-4755-8a99-0c4115d3b3f7'),
('2b35e14e-e9e6-470d-baef-0e2b71993ecd', '3614d48b-d648-48eb-a3da-cb79b891294c'),
('33be2c1d-3a56-45c9-9540-57763103f912', '209918db-5b3f-4681-85b4-25f3c36d3b12'),
('33be2c7d-3a56-45c9-9540-57763103f912', '27a32af6-9c79-4a50-af30-6d9d4a915cad'),
('64090b04-8065-4eec-8e89-71af65f8f44b', '5977f602-3c2e-431e-9b70-5047d27a819b'),
('6582b0da-b8d0-4618-a763-864358706ce1', '5a42d46f-5483-4b25-b04a-0e746e5d0562'),
('7aae156e-2f1f-4412-b8de-c7d983c932df', 'cfb9b388-44d0-474c-b8a9-136f361a854b'),
('820d148a-0944-44d7-addc-fba98d6c9de7', '44c6962b-9a17-4277-9fdc-51b12e38e613'),
('820d148a-0944-44d7-addc-fba98d6c9de7', '90ab960e-89bb-4a4b-a8fd-f346b9d520c1'),
('99edd926-5e39-42bc-afc3-a1a360e1ce3f', '563cec23-cb49-4666-8ac1-354f21f1c9d3'),
('9a456b69-19bc-41c4-8b48-b0a9b3ec4c94', '9a09b0d1-3a2d-4105-accd-ecaa522b8f2c'),
('9e889411-b12f-45c0-93ef-05ecd58e900d', '6eefedbc-35c9-42a3-823b-f68239bb56ee'),
('a7d927f1-d25c-47fd-8483-72ba8a42053a', '28b7f043-fd77-4d29-92b5-815b44d2207b'),
('a907810e-1223-48ff-a22d-3878f28b6905', 'ad048437-1534-44dc-a74a-ac16132cf79c'),
('aeb75597-3e5e-47d6-9742-d5109eb3db55', 'ef679123-5b15-4cff-b8d7-c60fad273b3b'),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'b4a2d12c-a5c1-4f8c-93c0-6b868af1d979'),
('f09bf7ed-2a7d-4da5-8703-05a52f8b1ceb', '22a1bb74-65bd-4376-9ebd-7db8b8f147c0'),
('f6636ffb-d162-4d6b-83da-b8dc586e7ece', 'f7adabed-6af0-460c-941d-73a73633a684'),
('fbe3a871-69fe-432e-9a76-d5119bd84c7d', '846d0fa4-a3cd-4665-967b-57b18319b2d1');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_place`
--

CREATE TABLE `hotel_place` (
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_place`
--

INSERT INTO `hotel_place` (`hotel_id`, `place_id`) VALUES
('1b2cb1af-423b-48d9-a079-b8c643d73809', '0af25a14-ff71-47b5-88c4-32ee31e50f5c'),
('1b2cb1af-423b-48d9-a079-b8c643d73809', '1b99dd5a-3202-4013-ad7a-5a2d65f238f8'),
('1b2cb1af-423b-48d9-a079-b8c643d73809', '1ba8995d-202f-4590-b610-5178c5d3ddc4'),
('1b2cb1af-423b-48d9-a079-b8c643d73809', '2b9a3c67-aa30-43e7-a81d-16b5845b8e9d'),
('1b2cb1af-423b-48d9-a079-b8c643d73809', 'de66bcf5-4e3a-4d8e-b312-6374b901328e'),
('1b2cb1af-423b-48d9-a079-b8c643d73809', 'ebb12497-33ba-4a18-8ac2-9530bd81583b'),
('2b35e14e-e9e6-470d-baef-0e2b71993ecd', '2adfb632-a5b6-4fdc-9e27-1f2a14894f23'),
('2b35e14e-e9e6-470d-baef-0e2b71993ecd', 'c2246051-7a14-4fc5-a109-8948a64a286f'),
('2b35e14e-e9e6-470d-baef-0e2b71993ecd', 'e0eb30f5-e68c-4a20-b410-bf484d40b649'),
('2b35e14e-e9e6-470d-baef-0e2b71993ecd', 'f08424cf-d0d5-4717-82cd-ff774583c092');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_services`
--

CREATE TABLE `hotel_services` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `priceType` int DEFAULT NULL,
  `photo` int DEFAULT NULL,
  `name` json DEFAULT NULL,
  `hasPriceValuesByDate` tinyint(1) DEFAULT NULL,
  `hasQuotaValuesByDate` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_service_quota_values`
--

CREATE TABLE `hotel_service_quota_values` (
  `id` int NOT NULL,
  `hotelServiceId` int DEFAULT NULL,
  `totalQuantity` int DEFAULT NULL,
  `occupiedQuantity` int DEFAULT NULL,
  `quotaDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_tag`
--

CREATE TABLE `hotel_tag` (
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_tag`
--

INSERT INTO `hotel_tag` (`hotel_id`, `tag_id`) VALUES
('0689aff9-eb6d-48c6-9b14-b1f4423523cf', '06d8be64-2109-437d-9fc1-2d8bcf9dc424'),
('12bfb004-5dc5-43bb-b0e4-860ec7b2cce4', 'd0a0caf9-712b-4792-bbff-65d4ad7aa60f'),
('18924058-1e6e-4bbc-a785-e5505515f74c', 'd0a0caf9-712b-4792-bbff-65d4ad7aa60f'),
('1b2cb1af-423b-48d9-a079-b8c643d73809', '06d8be64-2109-437d-9fc1-2d8bcf9dc424'),
('2b35e14e-e9e6-470d-baef-0e2b71993ecd', '06d8be64-2109-437d-9fc1-2d8bcf9dc424'),
('31fa0a0f-fbcc-444c-90de-283431a8c980', 'd0a0caf9-712b-4792-bbff-65d4ad7aa60f'),
('64090b04-8065-4eec-8e89-71af65f8f44b', 'c54dbea9-9d9d-4247-a106-d60fc9aa7541'),
('6582b0da-b8d0-4618-a763-864358706ce1', '672bf553-10e6-4e70-9ac6-da56248847f7'),
('99edd926-5e39-42bc-afc3-a1a360e1ce3f', '672bf553-10e6-4e70-9ac6-da56248847f7'),
('9e889411-b12f-45c0-93ef-05ecd58e900d', '672bf553-10e6-4e70-9ac6-da56248847f7'),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '672bf553-10e6-4e70-9ac6-da56248847f7'),
('f09bf7ed-2a7d-4da5-8703-05a52f8b1ceb', 'd0a0caf9-712b-4792-bbff-65d4ad7aa60f'),
('fbe3a871-69fe-432e-9a76-d5119bd84c7d', 'c54dbea9-9d9d-4247-a106-d60fc9aa7541');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_type`
--

CREATE TABLE `hotel_type` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json DEFAULT NULL,
  `is_active` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_type`
--

INSERT INTO `hotel_type` (`id`, `name`, `is_active`, `sort`) VALUES
('18924058-1e6e-4bbc-a785-e5505515f74c', '{\"ru\": \"гостевой дом\"}', 1, NULL),
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"апартаменты\"}', 1, NULL),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"шале\"}', 1, NULL),
('6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"хостел\"}', 1, NULL),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"отель\"}', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_video`
--

CREATE TABLE `hotel_video` (
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` smallint UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `is_active`) VALUES
('891a02b4-bc87-4a01-a4b2-15eb92de4488', '', '', 1),
('8aa32692-8500-4882-bcf4-dd9ee809c0ee', '', '', 1),
('d19c1b65-3176-48ae-8de7-ef2db3633b74', '', '', 1),
('e342ff16-be20-4eeb-b121-7621cc9d87ed', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` smallint UNSIGNED NOT NULL DEFAULT '1',
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `altitude` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `is_active`, `name`, `sort`, `altitude`) VALUES
('266f29e8-61c0-4b83-9ffd-321a46237034', 1, '{\"en\": \"Mountain olympic village\", \"ru\": \"Горная Олимпийская деревня\"}', 3, 1100),
('4538fd97-6bef-458a-8cdb-81e5383aee70', 1, '{\"en\": \"\", \"ru\": \"333333333333\"}', 9, 0),
('4b24d53f-6de8-49e3-8f31-eefe2b4ab941', 1, '{\"en\": \"Rosa Peak\", \"ru\": \"Роза Пик\"}', 2, 2320),
('65e75bd9-a3ab-45eb-bfcd-12a17c6f8bb6', 1, '{\"en\": \"Lower station of CD Edelweiss\", \"ru\": \"Нижняя станция КД Эдельвейс\"}', 7, 400),
('663e802b-64da-4820-bdad-47b0483702fd', 1, '{\"en\": \"Black Sea beach\", \"ru\": \"Пляж на Черном море\"}', 6, 0),
('8432c645-a344-4b45-917e-83ac51b4eeb9', 1, '{\"en\": \"Rosa Valley\", \"ru\": \"Роза Долина\"}', 1, 560),
('af634681-1c69-43a2-ba03-ede8fc496717', 1, '{\"en\": \"Rose Stadium\", \"ru\": \"Роза Стадион\"}', 4, 940),
('e43ac171-1728-4ece-b9d0-ce86d98e2a72', 1, '{\"en\": \"Alcove\", \"ru\": \"Беседка\"}', 5, 1350);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `amount` bigint UNSIGNED NOT NULL,
  `cash_list_id` int UNSIGNED NOT NULL,
  `barcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint UNSIGNED NOT NULL,
  `enum_number` int UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `amount`, `cash_list_id`, `barcode`, `status`, `enum_number`, `created_at`, `updated_at`) VALUES
('01b15d32-064c-4d1c-a0d7-50309a302ba3', '6e4549c6-b9db-4602-85b6-a0bf3ddffb56', 422, 0, NULL, 100, NULL, '2021-03-05 14:38:49', NULL),
('0728fb50-f232-4980-b38d-515a1cf8e0a8', '07dc0bcb-89d6-4b3b-b4f4-ba5ae3f094c6', 2, 0, NULL, 100, NULL, '2021-02-25 11:59:33', NULL),
('09fd78c4-f751-412d-8282-4b6e49b4ff54', 'ad816613-d31e-430e-97fa-dc75eb3d3892', 2272100, 32081550, 'WOA632283CB09605A76D3D51DB', 100, 11, '2021-03-17 08:41:25', '2021-03-17 08:41:54'),
('0b003e43-4474-476f-914f-098af5e6eeb1', '356e41a4-0dad-457f-8b4c-c69ae608345b', 457, 0, NULL, 100, NULL, '2021-02-25 08:48:37', NULL),
('129aeafa-dd13-484d-a98a-c4a533d5bdbd', '5c5d696b-a194-4a14-a745-4c868518f9cf', 122, 0, NULL, 100, NULL, '2021-02-24 12:00:58', NULL),
('14fe2e94-1351-4d8a-b81f-d33f988c3fc8', 'c589f048-a104-47f5-a679-febfaa57cb33', 488, 0, NULL, 100, NULL, '2021-03-05 10:35:43', NULL),
('1b3880f1-21b8-4dc9-929e-699fd87c20d7', '324d18f1-8035-4ecd-85c2-3a88fe13063f', 3232, 32057099, NULL, 100, NULL, '2021-03-06 14:38:18', '2021-03-06 14:38:20'),
('22b444a2-e712-4e7c-bfd9-22626c256b2f', '6a199751-6db8-4fc0-a2ec-160c601cc50c', 543, 0, NULL, 100, NULL, '2021-03-05 15:06:26', NULL),
('231c026c-0ea7-4495-b6bf-ba93e657878e', '63f2d790-3165-4516-9461-b7681a7be833', 7750600, 32081639, 'WO7A9DAA7F01FFC82E2701B76D', 200, 16, '2021-03-17 18:57:29', '2021-03-17 18:58:49'),
('24b3cf47-01e4-4119-bd36-1ad076e933c3', 'b5d40428-ef2c-474a-b043-a84f773a104f', 122, 32057097, NULL, 200, NULL, '2021-03-06 14:26:09', '2021-03-06 14:27:16'),
('2857914d-47dc-4ace-9434-2c98a0cf04b8', 'b5d40428-ef2c-474a-b043-a84f773a104f', 122, 32057102, NULL, 100, NULL, '2021-03-06 14:49:01', '2021-03-06 14:49:02'),
('2ba47f73-e2ae-4ba6-b9df-d380f5c54821', '6d5e86d5-afbb-4826-a0ef-3a892ea27ad1', 493, 0, NULL, 100, NULL, '2021-03-02 14:23:09', NULL),
('2cee7532-c4d1-477f-901e-256ede02561d', '0b8532fa-daef-467d-bd6c-08447607f55a', 0, 0, NULL, 100, NULL, '2021-02-20 14:14:58', NULL),
('34ca7a37-9223-4902-9d73-c03129209ffa', '61bef744-5c97-4fb8-a856-7bc178ae8fe3', 200, 0, NULL, 100, NULL, '2021-02-19 13:26:26', NULL),
('3b32ea50-84ef-4b44-a9ac-0b76a85e4651', 'f96c900d-8215-4f63-a2fb-427a08ffad34', 122, 0, NULL, 100, NULL, '2021-02-24 11:59:11', NULL),
('4381816c-9743-4241-97c5-47ae5709650c', '979b0d5f-8806-4378-8313-1bc31d3fc0b1', 122, 0, NULL, 100, NULL, '2021-02-24 12:01:59', NULL),
('4a4c5cc6-646a-4f36-8de1-35c470023039', 'd46072ca-9c17-48bd-a82f-289f4c4550e8', 3627700, 32081630, 'WO4A22EF5D2AB04140F994B094', 200, 15, '2021-03-17 15:52:23', '2021-03-17 15:53:10'),
('4acfbadc-cbc3-4272-8edf-0a9123a90124', '8956722f-8c97-416f-9b00-a2b374122fcf', 35100, 32081727, 'WO89B75A5A42620889B645A796', 200, 24, '2021-03-18 11:49:21', '2021-03-18 11:49:53'),
('4be90a44-7644-4607-b70d-3c5712891f54', 'c589f048-a104-47f5-a679-febfaa57cb33', 1, 0, NULL, 100, NULL, '2021-03-05 11:39:53', NULL),
('50d95736-8090-42dd-9425-839669408652', '8956722f-8c97-416f-9b00-a2b374122fcf', 70200, 32081724, 'WOD031293C0424F6F6B09E2605', 100, 23, '2021-03-18 11:35:05', '2021-03-18 11:35:07'),
('510249fe-489c-4e75-916b-86a6b2c1b10c', '247bed1f-68ab-4a7c-976c-2c06335e5898', 100, 0, NULL, 100, NULL, '2021-03-05 06:32:01', NULL),
('51aa6608-c802-4153-9d34-63d854275a7e', '8e5aae66-b12b-4bac-8fe3-04407ba1857d', 200, 0, NULL, 100, NULL, '2021-02-08 14:31:18', NULL),
('52321a81-e473-45ab-96d7-341f63042529', '642837b7-6846-4d0c-a7ce-b0bc909d6c58', 103, 0, NULL, 100, NULL, '2021-03-05 13:46:09', NULL),
('527efc7c-d1f0-49ac-b843-182709e341a1', 'd350c3dd-7295-45ac-8de6-8df9c4cc62de', 1, 32080930, 'WOFB2C29C307E102E86CB395F4', 100, 2, '2021-03-15 04:59:25', '2021-03-15 04:59:28'),
('557f4750-aa44-43a8-b675-ab5190f378ea', '6b250f66-687c-4343-8930-95ddadef3df4', 303, 0, NULL, 100, NULL, '2021-03-05 12:06:53', NULL),
('57528959-12ca-46d4-849c-cecb85347fcc', '2f5121e9-77d0-41d3-9e9e-1494b979469b', 108, 32057141, NULL, 200, NULL, '2021-03-09 06:50:41', '2021-03-09 06:52:07'),
('5c3f3a44-6b2d-481a-9679-e915eecba961', '86e089e9-4e8e-4652-b896-a42f1dc2590e', 106, 32080938, 'WO954FE91B1E8056B78050E912', 200, 3, '2021-03-15 05:43:20', '2021-03-15 05:44:53'),
('60cc3577-ab3a-41b0-a661-d4e3212473a7', '734000dc-8067-46c2-b845-32130aa74cc6', 108, 32057139, NULL, 100, NULL, '2021-03-09 06:47:45', '2021-03-09 06:47:47'),
('654716e0-4abb-4d84-a65a-76a346abe8e2', '7f6d0f8e-82e2-4c18-8e78-d74fad31959b', 3, 32079809, 'WOF0DB5388B17FCA5DA269E032', 100, NULL, '2021-03-11 14:33:03', '2021-03-11 14:33:04'),
('65b5da6d-94ed-44b9-aae8-839f33238574', '71d3b61a-1a63-45d3-bb2d-cf86dcc007e5', 244, 32057094, NULL, 100, NULL, '2021-03-06 14:25:52', '2021-03-06 14:25:56'),
('68519880-017e-40c6-8efe-20daa85ee08c', '2bd07951-6b22-4021-bacb-16027acdf085', 200, 0, NULL, 100, NULL, '2021-02-11 12:53:55', NULL),
('686bfb31-6d15-4294-a32c-ca0d22f87848', 'ebab9bb0-aeba-4943-81a4-c3162f3394f0', 2, 32080461, 'WOE137105E10A296960F364F4A', 100, 1, '2021-03-12 14:38:45', '2021-03-12 14:38:47'),
('6b8db812-4bad-4280-846a-4d9bcd727bb2', '4b2eb24a-9ead-48ab-84de-50ce7d0e6d11', 3, 0, NULL, 100, NULL, '2021-02-25 11:19:14', NULL),
('6d434242-c428-499a-9eda-32eb816a2a0d', '87f59790-51cc-447d-b6fe-273ff067edd9', 122, 32081225, 'WO00DDC2466F777AAD084EB055', 100, 6, '2021-03-15 11:37:37', '2021-03-15 11:37:39'),
('71d42e8c-bf8b-4e90-b22b-998c6904c02c', '6b250f66-687c-4343-8930-95ddadef3df4', 4541, 0, NULL, 100, NULL, '2021-03-05 12:52:40', NULL),
('74c44f61-8c7a-4f1a-b1c0-d67aea9b7257', 'fcfcb4a3-9b19-4d0f-a2c8-83fafe3c583c', 105600, 32081664, 'WOC34D95451068ABF6FE526C96', 100, 20, '2021-03-18 06:08:59', '2021-03-18 06:09:02'),
('76927ede-8945-4307-8fb7-2b2a32239182', 'de7a4145-b7ce-43b4-9210-e85bd45ad6cc', 206, 0, NULL, 100, NULL, '2021-03-05 13:37:06', NULL),
('76ec53eb-0f2f-4fe1-a3e4-e0b6d879e99a', '86e089e9-4e8e-4652-b896-a42f1dc2590e', 170600, 32081292, 'WO73CED348AB3184BEBF06A8BE', 100, 7, '2021-03-15 13:11:40', '2021-03-15 13:11:41'),
('786249e6-59ac-4136-8e24-991d059d2052', 'a6f61ce6-f88d-464a-8c5f-dc3bb605d189', 244, 0, NULL, 100, NULL, '2021-03-05 13:37:29', NULL),
('7bee03dd-f1a4-4f6c-bc83-b85e032173f0', '250eaf6c-7a5d-4bd9-923d-47be78439196', 244, 0, NULL, 100, NULL, '2021-03-02 14:35:29', NULL),
('7c13a4d2-a3ec-4b2f-a330-182e78d4590e', '6aefb3ef-90e0-4d97-8f31-fe0dcdb6d048', 0, 0, NULL, 100, NULL, '2021-02-07 14:41:39', NULL),
('7cdd8b6f-5c90-43fb-bd83-6d4d72fb98f3', 'b71a8ac9-025f-4935-8798-bb421cde80e8', 3790000, 32081672, 'WO30DE9282C563453375830C6E', 100, 22, '2021-03-18 06:21:19', '2021-03-18 06:21:43'),
('7dcd035a-23e8-4cde-987d-15b6cb6e79e6', '89639e92-638d-4135-bbfe-ac88ba455ccd', 244, 0, NULL, 100, NULL, '2021-03-05 15:22:44', NULL),
('7e173dc1-6836-493f-bf57-880ba2362b56', '9cc99095-fecc-4630-a3bc-210004571d8b', 2, 0, NULL, 100, NULL, '2021-02-25 09:57:53', NULL),
('80ca16a1-27d4-4fa4-9150-8b6f49f076bd', '843e18a3-e0cb-4ae3-a584-19b7f20d7c59', 244, 32079596, NULL, 200, NULL, '2021-03-10 05:45:12', '2021-03-10 05:45:55'),
('81c38f92-73ed-4df1-a729-f4f831eee1da', '87f59790-51cc-447d-b6fe-273ff067edd9', 1, 32057091, NULL, 200, NULL, '2021-03-06 06:52:20', '2021-03-06 06:52:54'),
('83a9a5bf-f0fd-4345-8c79-0afcc2104336', 'b06d3137-d3ce-4737-aaa2-3309a7d5d3a7', 277, 0, NULL, 100, NULL, '2021-03-05 08:14:36', NULL),
('85584c49-3de8-4d32-9d82-97d4c73f29fc', '87f59790-51cc-447d-b6fe-273ff067edd9', 35844, 32081222, 'WO17BA863F66255A62CAADDEBA', 100, 5, '2021-03-15 11:34:53', '2021-03-15 11:34:54'),
('869c957f-b1ad-46bd-999d-da943aebae3e', 'efd67efa-2fcf-4287-9561-b2963ff5a2bd', 1, 0, NULL, 100, NULL, '2021-03-05 06:38:47', NULL),
('8851092a-cdeb-4e1e-9e87-58b4329fc395', '290ac6c2-bb39-4740-acca-dbb0d84bd427', 122, 0, NULL, 100, NULL, '2021-02-24 11:32:09', NULL),
('8a58290c-51bf-47d2-b856-3c9b4c781dfe', '395fb206-2304-4af1-bab9-2f780136812e', 698, 0, NULL, 100, NULL, '2021-03-05 10:44:43', NULL),
('8ade2123-24a7-4575-b4b6-a925cb922f5c', 'c6bca30b-2939-462c-9993-40e71c9a8e13', 35100, 32081661, 'WO5E4CF21425E4541F788FFF80', 100, 19, '2021-03-18 06:08:02', '2021-03-18 06:08:04'),
('8f5cb396-ed5e-42af-8d99-c72dda571d66', '65c7c0c7-226f-4631-a89e-f471e1da4c2f', 145000, 32081530, 'WOC914A9AF931ACA684089862D', 100, 10, '2021-03-16 15:31:39', '2021-03-16 15:31:41'),
('95cca975-6614-49e0-a546-62f71e2b1c3d', 'd8002bff-daff-4b5f-9217-40662dd688eb', 35799, 32081527, 'WO7B33BAE308025BFA2229B5E3', 100, 9, '2021-03-16 15:31:13', '2021-03-16 15:31:17'),
('966b1431-1115-474a-b6f7-1005f945f11b', '87f59790-51cc-447d-b6fe-273ff067edd9', 1, 32057088, NULL, 100, NULL, '2021-03-06 06:20:13', '2021-03-06 06:20:15'),
('9782a868-63d4-4e9c-8660-d926a8872b12', 'b8cb3e0d-59e3-49f2-9067-9f6fe1f9ca9d', 122, 0, NULL, 100, NULL, '2021-02-24 11:48:29', NULL),
('98fe2307-3872-4513-8333-904bb4ef45ca', '53b70f6d-ddc6-456d-91ca-96d80052660f', 2, 32079646, NULL, 100, NULL, '2021-03-10 08:59:18', '2021-03-10 08:59:21'),
('9ae70653-a3ca-4cc4-8afd-d09b0cb156fe', 'b2eccdc0-cc39-4605-97b3-399372965273', 200, 0, NULL, 100, NULL, '2021-02-11 10:59:26', NULL),
('9cafc570-e787-4c3c-b49f-a5301ccdde37', 'c589f048-a104-47f5-a679-febfaa57cb33', 200, 0, NULL, 100, NULL, '2021-03-05 11:35:41', NULL),
('9d4daf1d-e73f-4417-9958-2a751fc08651', '88ff3a09-832e-4693-8b33-78d46288495c', 1, 0, NULL, 100, NULL, '2021-03-02 14:46:28', NULL),
('a09bf250-9add-4275-8dd8-46adcee6f115', 'edc9b6e6-fc78-4290-9823-d90f144ad1ae', 2222100, 32081563, 'WOC76EF9C312AA8DA77A3BE0D4', 200, 12, '2021-03-17 11:08:24', '2021-03-17 11:11:27'),
('a0ff7244-ab40-4d7c-b605-5d0bfb682cc7', '8956722f-8c97-416f-9b00-a2b374122fcf', 4059600, 32082206, 'WO22622CAA17B869FB2E25D592', 100, 26, '2021-03-19 11:11:03', '2021-03-19 11:11:32'),
('a27dbd7d-5ce9-455e-9cc2-44282790ede2', '994064dd-1d99-4e15-97fa-77fb080b46f9', 3790000, 32082945, 'WOCA19806544D58A6CA3750FE4', 100, 28, '2021-03-24 06:55:24', '2021-03-24 06:55:53'),
('a49c0195-25e9-430f-860b-65214fdc02ee', '8a3f54c5-113c-41da-8ffb-710462bb0233', 200, 0, NULL, 100, NULL, '2021-02-11 11:32:06', NULL),
('a6ac203f-5333-4e1c-9668-3747ddf3f585', 'd46072ca-9c17-48bd-a82f-289f4c4550e8', 436200, 32081597, 'WO82BA129CF85E96BCF9BD50B0', 200, 14, '2021-03-17 13:01:49', '2021-03-17 13:02:15'),
('a79e7553-c9a0-4fd4-abe1-5054731c31be', '684b0537-5623-460e-8919-3c2fc7b5dcbd', 488, 0, NULL, 100, NULL, '2021-03-02 14:27:17', NULL),
('a827e774-17b9-43d7-a919-84d6277889c8', 'e1236b7f-c332-4a2b-84dc-44070d4dfa5c', 3790000, 32081651, 'WOF0C8E101418DA05B66881725', 100, 17, '2021-03-18 05:59:17', '2021-03-18 05:59:45'),
('a9e7ac0b-53bb-4651-810c-90ad41bd59f9', '87f59790-51cc-447d-b6fe-273ff067edd9', 3, 32079803, 'WO50745AB72C2612813AB58880', 100, NULL, '2021-03-11 14:24:44', '2021-03-11 14:24:46'),
('b175cbc7-ad33-4368-a329-119e2557043f', '674e9ad0-4bab-41f5-9840-7f04fa0d38f8', 200, 0, NULL, 100, NULL, '2021-02-19 10:48:25', NULL),
('b282761e-e173-4b01-8d13-7fe203b1ad3c', '46666f3e-dbdd-416d-9235-c76c0843552b', 209, 0, NULL, 100, NULL, '2021-03-05 14:51:52', NULL),
('b2caacc4-9a6b-4c81-b8db-1e53083a8004', '93c77cba-e896-4aac-9530-71d07d454351', 122, 0, NULL, 100, NULL, '2021-02-24 11:31:04', NULL),
('b552e2d7-bb97-4908-8365-03b8119ba4d9', 'b74b0e14-40db-4189-8009-8e49eb0f6c8e', 3790000, 32081667, 'WO61952758DE792A286F74D131', 100, 21, '2021-03-18 06:18:45', '2021-03-18 06:19:09'),
('b7a3b2b8-7a89-4f27-a0d6-1e1994e10a76', 'dffc3af2-7f2f-42c9-8c3d-a61c74b8368f', 122, 0, NULL, 100, NULL, '2021-02-24 11:37:55', NULL),
('b7b47fb1-5ce4-49e4-b2a8-0a3985c2b416', 'd9540b4c-c20a-447b-b661-23e9aa703875', 2, 0, NULL, 100, NULL, '2021-03-05 06:54:24', NULL),
('b80675d5-2cbf-4c3f-b08d-358e3a15b517', 'befb80c4-87e3-4609-8eef-744c74dd0f29', 104400, 32081572, 'WO091721BF7D86A930B6D8CB1C', 100, 13, '2021-03-17 12:25:22', '2021-03-17 12:25:26'),
('b9542334-bbfa-44cc-8e1b-fe9d24909185', '8956722f-8c97-416f-9b00-a2b374122fcf', 1, 32081399, 'WOFEF0D7FA97C466BB8FDCEF40', 100, 8, '2021-03-16 07:20:47', '2021-03-16 07:20:50'),
('ba5e7e09-c205-451b-b8b6-796a25212fac', 'e384fb37-b818-450c-96b9-a29e65ce0e73', 457, 0, NULL, 100, NULL, '2021-02-25 08:47:51', NULL),
('ba90a7ff-9650-4b0c-9f26-836a40cffd3b', '33826dfc-3fce-457f-b9da-6d4aa8fff655', 276200, 32081656, 'WOF1CA1229AD992A23353F691C', 100, 18, '2021-03-18 06:06:53', '2021-03-18 06:06:56'),
('bbbb9f6c-55c8-42ef-b731-c46076cf0b03', '740c2c54-310f-4979-b855-2524a14ef225', 3, 0, NULL, 100, NULL, '2021-02-25 11:01:52', NULL),
('bd7c74e6-86a7-4cb7-959e-b05c12f2c01f', 'c799a607-9374-4912-9314-b4977c13e170', 244, 0, NULL, 100, NULL, '2021-03-05 15:33:20', NULL),
('be2d81e1-30ec-43a6-b7c3-e58544597e30', 'acb2f492-f370-44d3-86e1-4365f3899d97', 3, 0, NULL, 100, NULL, '2021-02-25 11:35:40', NULL),
('c28c18a5-5afd-4ce1-aa2b-01d18debeb2a', '2a23cd11-66d1-4c8f-9c81-60992e8fbe14', 100, 0, NULL, 100, NULL, '2021-03-05 15:17:07', NULL),
('c2f533c8-1311-4e66-a9c3-d5a3bab60f8a', 'e633abd5-048d-4400-b387-03e48cc75b52', 457, 0, NULL, 100, NULL, '2021-02-25 08:36:33', NULL),
('c332e93a-1101-4e49-bb6b-53741d9a2958', 'c4fd6fdd-bf59-416c-b54d-3f20b05ef6fa', 3, 32079806, 'WOB5F039DE8A7552CD4651C97D', 100, NULL, '2021-03-11 14:27:04', '2021-03-11 14:27:06'),
('c66a702a-1150-46ee-8e15-40d246ad5481', '2f44aaf3-cf55-47a1-bd26-0e97e4704390', 122, 0, NULL, 100, NULL, '2021-02-24 09:57:17', NULL),
('c71cc515-8b2c-43e2-9eaf-4a0fa3a3b226', '27b9eeed-aee2-4f1f-ad13-59abdc97c819', 3, 0, NULL, 100, NULL, '2021-03-05 08:27:27', NULL),
('cc0890a1-ccb4-4ee9-b8f1-f4df7595918f', 'fdf04558-40f5-4090-9242-0e8277feae81', 2, 0, NULL, 100, NULL, '2021-03-05 09:21:43', NULL),
('cf121bff-52a6-4049-ae5e-03859ccbce09', 'cc5dbe0d-7e23-41a7-ab60-ef955b3993fb', 493, 0, NULL, 100, NULL, '2021-03-02 10:19:11', NULL),
('d3152913-9646-4d9d-962c-b69ae5501f5e', 'cd8f1694-77e4-4842-87f0-7bf6fbe05e11', 244, 0, NULL, 100, NULL, '2021-03-05 11:42:41', NULL),
('d4de4678-679c-457b-9845-352a89f22320', 'c55be4c4-47dc-421b-b06e-752e613f7fa1', 3, 0, NULL, 100, NULL, '2021-03-05 08:22:31', NULL),
('d63c8ab7-1ff5-4ba3-bd01-96fb5828bb82', 'd1fd2aa5-452c-4ae6-b9a5-018c52cfd128', 2272100, 32082852, 'WO6EC5076FF7F8A9CFB82ACB2C', 100, 27, '2021-03-23 11:53:42', '2021-03-23 11:54:09'),
('d792bf5e-fba4-4d4d-82d6-02511eca8422', '8956722f-8c97-416f-9b00-a2b374122fcf', 839, 0, NULL, 100, NULL, '2021-03-05 13:22:43', NULL),
('dd862281-8b8f-48cb-9354-ba0217f0a9e7', '559faf20-83e1-447a-8609-62d6603ae7da', 3, 0, NULL, 100, NULL, '2021-02-25 11:14:43', NULL),
('def3e6e9-34b7-4bd8-943b-4618711f3e0e', '95de8a66-d187-4a26-908a-de44fd1b9e46', 200, 0, NULL, 100, NULL, '2021-02-11 12:59:25', NULL),
('e05bbd4e-4de8-46f5-9b05-90af8b89b76f', 'b102f87b-43ed-4129-b62b-534c97dd1e31', 457, 0, NULL, 100, NULL, '2021-02-25 08:46:59', NULL),
('e10e9cfc-847a-45a8-86ef-3a6b1b62276f', '83eb5fd2-8b5f-4bc1-ab18-d365072b2ff9', 122, 0, NULL, 100, NULL, '2021-02-24 10:39:32', NULL),
('e493e506-9328-4447-924f-b7b526d4ce99', '8956722f-8c97-416f-9b00-a2b374122fcf', 552400, 32081730, 'WO8A5E51E53B099F2EA023EF86', 200, 25, '2021-03-18 11:52:04', '2021-03-18 11:52:43'),
('e8529161-8ef1-4903-810f-47136df10e3a', '168041e5-b5d8-446d-aac0-a4507c5e30f1', 122, 0, NULL, 100, NULL, '2021-02-24 11:53:46', NULL),
('ec63a361-a026-4de3-9e1c-dfa85682757f', 'e362e4d0-58d8-4f6b-bdb0-a323b6f8ce00', 493, 0, NULL, 100, NULL, '2021-03-02 09:10:28', NULL),
('efdd541d-476e-4e08-9610-db4b83fa4f87', '6b250f66-687c-4343-8930-95ddadef3df4', 204, 0, NULL, 100, NULL, '2021-03-05 12:05:29', NULL),
('f05f5413-3bf8-42ce-9ee8-e1246158dc9a', '86e089e9-4e8e-4652-b896-a42f1dc2590e', 17100, 32080954, 'WOB983A69C51CD2EBF2251466C', 100, 4, '2021-03-15 06:52:31', '2021-03-15 06:52:33'),
('f178bae5-4eba-45ee-8d1d-ad3cb74764e6', '12a66a06-a4a5-49a4-8d41-a24bd9e8b0c2', 493, 0, NULL, 100, NULL, '2021-03-02 13:48:06', NULL),
('f4c4338a-1d21-428e-9f3a-b187900001f0', 'b9fd77b2-27ea-41fd-9727-ab30822f1258', 2, 0, NULL, 100, NULL, '2021-03-05 06:43:58', NULL),
('f4dc357a-e576-403e-b020-e9ddcbdcefd3', 'e80f9a5a-a674-42a7-a4e8-1c9988b7f8dc', 200, 0, NULL, 100, NULL, '2021-02-08 14:32:34', NULL),
('f7ad6daf-b10b-4bc3-b280-c37d583b5af4', 'bf841828-ce79-4560-b5c0-c3e82bf213fd', 200, 0, NULL, 100, NULL, '2021-02-19 10:43:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `order_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `price` bigint UNSIGNED NOT NULL,
  `quantity` smallint UNSIGNED NOT NULL,
  `amount` bigint UNSIGNED NOT NULL,
  `activation_date` date DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
  `card_num` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_price` bigint UNSIGNED DEFAULT NULL,
  `client_id` int UNSIGNED DEFAULT NULL,
  `cash_item_id` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `order_id`, `product_id`, `price`, `quantity`, `amount`, `activation_date`, `last_name`, `first_name`, `middle_name`, `birth_date`, `card_num`, `card_price`, `client_id`, `cash_item_id`) VALUES
('01094129-f7f3-44ee-81eb-2b370b0bb99f', '231c026c-0ea7-4495-b6bf-ba93e657878e', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, 1, 3790000, NULL, 'лддод', 'биб', NULL, '2000-02-02', NULL, 0, 32081645, 32081646),
('01aa2de2-2f70-4a31-8df2-8df0ab26359c', 'b80675d5-2cbf-4c3f-b08d-358e3a15b517', '17ec8006-84b6-41e7-aedf-349b83efcca1', 17100, 2, 34200, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081575),
('01db2ae5-a34e-416a-8d35-85d092788608', 'efdd541d-476e-4e08-9610-db4b83fa4f87', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 2, 200, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('0200efbf-e594-4674-a4e1-7f41ad14f77a', 'a79e7553-c9a0-4fd4-abe1-5054731c31be', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('03f1a1ca-007f-439c-a395-4cea26d1a77d', '4be90a44-7644-4607-b70d-3c5712891f54', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('05335eef-9cae-4628-8a45-49fcf34bb9c4', 'ec63a361-a026-4de3-9e1c-dfa85682757f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('05f26110-033d-4084-8799-5eb6299d260f', 'c2f533c8-1311-4e66-a9c3-d5a3bab60f8a', '0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 213, 1, 213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('0a41a18b-00a4-4092-af01-a76899c1dfbe', '09fd78c4-f751-412d-8282-4b6e49b4ff54', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1590000, 1, 1590000, NULL, 'Литвинково', 'Рере', NULL, '2000-02-02', NULL, NULL, 32081526, 32081551),
('0c38b3a7-b7b4-4a30-83d2-a8898da53123', '2ba47f73-e2ae-4ba6-b9df-d380f5c54821', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('0f41bfe7-9998-4a76-ae19-fd969f6c1f49', '654716e0-4abb-4d84-a65a-76a346abe8e2', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 1, 3, '2021-03-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32079810),
('141c3ee9-d0f6-4af1-ad29-d46187fe37e9', '2ba47f73-e2ae-4ba6-b9df-d380f5c54821', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('149164d9-502d-4c7a-9713-9eb49f581589', '74c44f61-8c7a-4f1a-b1c0-d67aea9b7257', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 70000, 1, 70000, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081665),
('15434f98-a158-4fe6-9e2e-d0306bfe9b50', 'e05bbd4e-4de8-46f5-9b05-90af8b89b76f', '0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 213, 1, 213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1568aa4c-edd7-462d-822d-de60ee51f0a3', '34ca7a37-9223-4902-9d73-c03129209ffa', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1606c14f-ae92-4d15-8083-51bc41e50237', 'e8529161-8ef1-4903-810f-47136df10e3a', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1699d17e-e7c5-4e5f-a5ff-73888272eda8', '52321a81-e473-45ab-96d7-341f63042529', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 100, 1, 100, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('177e1189-f564-416a-bdb8-bf0bbdf3d6ab', 'f4c4338a-1d21-428e-9f3a-b187900001f0', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1a393047-ef36-4a5f-a3f2-5cf20ad12a8e', '80ca16a1-27d4-4fa4-9150-8b6f49f076bd', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 2, 244, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1ab76fbe-371a-49c2-9847-7134157b8cfd', 'c28c18a5-5afd-4ce1-aa2b-01d18debeb2a', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 1, 100, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1b066797-7d76-4287-b64e-60bab0026256', '51aa6608-c802-4153-9d34-63d854275a7e', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1c152f48-b76a-44f4-bbc1-4b828a806a2e', '2857914d-47dc-4ace-9434-2c98a0cf04b8', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1db0fb98-59be-4351-9f98-431d0ac6b656', 'e493e506-9328-4447-924f-b7b526d4ce99', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081731),
('2234e5af-cc4e-4e82-8160-56b3621d6055', 'a827e774-17b9-43d7-a919-84d6277889c8', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, 1, 3790000, NULL, 'ываываы', 'ыаываы', NULL, '1121-12-21', NULL, 0, 32081653, 32081654),
('25a62bfc-02f2-434a-a4a8-fee8cd17fa5e', 'b552e2d7-bb97-4908-8365-03b8119ba4d9', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, 1, 3790000, NULL, 'asdad', 'asdsa', NULL, '1111-11-11', NULL, 0, 32081669, 32081670),
('25de4513-4bf0-4837-b3e2-a4e6185b8221', 'a0ff7244-ab40-4d7c-b605-5d0bfb682cc7', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32082211),
('27750863-4a6b-4c25-9ee9-5c0f3c241f10', '527efc7c-d1f0-49ac-b843-182709e341a1', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32080931),
('2a68d477-8b43-40af-ad8f-9959a5cdd4fc', 'f178bae5-4eba-45ee-8d1d-ad3cb74764e6', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2ad1bcdd-f542-4cd1-bc88-1e9cf61b10b6', 'a0ff7244-ab40-4d7c-b605-5d0bfb682cc7', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, 1, 3790000, NULL, 'dfdfd', 'fdfdfd', 'dfdf', '1111-11-11', NULL, 0, 32082208, 32082209),
('2af1f8a7-e9bc-423a-bcb7-311b207ddf73', 'dd862281-8b8f-48cb-9354-ba0217f0a9e7', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2c25f367-8367-47b4-8d43-e32dcaae99a6', '4a4c5cc6-646a-4f36-8de1-35c470023039', '17ec8006-84b6-41e7-aedf-349b83efcca1', 17100, 3, 51300, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081631),
('2da53166-9aad-4592-964e-8e50daf68af9', 'd4de4678-679c-457b-9845-352a89f22320', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2ea80263-20a6-484d-b072-654a7243aab1', '7bee03dd-f1a4-4f6c-bc83-b85e032173f0', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('33cb6a61-15c4-42a6-8a75-2df93056a475', 'c2f533c8-1311-4e66-a9c3-d5a3bab60f8a', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('3542a06e-c6b8-4418-bcd0-98f8ca1aa28d', '8a58290c-51bf-47d2-b856-3c9b4c781dfe', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 299, 1, 299, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('35488ac9-3d81-4fcd-b097-f5922af0c1dd', 'c71cc515-8b2c-43e2-9eaf-4a0fa3a3b226', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('382b7665-6d63-4abb-8b5b-e13d5df59f6a', '231c026c-0ea7-4495-b6bf-ba93e657878e', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-17', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081648),
('39fb9329-06d3-4539-bf57-f9345ffa05e0', 'efdd541d-476e-4e08-9610-db4b83fa4f87', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, 2, 4, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('3be1fdbc-9132-42c2-ad58-336287285621', 'a09bf250-9add-4275-8dd8-46adcee6f115', '35f028ec-512b-4ff9-b077-8179c6eef588', 501500, 1, 501500, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081568),
('3bfe65c6-bf87-4664-92b8-c3e9642b08d7', 'ec63a361-a026-4de3-9e1c-dfa85682757f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('3d793925-d10b-455f-855b-e586830616d2', 'a27dbd7d-5ce9-455e-9cc2-44282790ede2', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, 1, 3790000, NULL, 'test878', 'test', 'test', '1996-11-21', NULL, 0, 32082947, 32082948),
('4306fd60-a81d-4009-83bb-5770c84d8256', '01b15d32-064c-4d1c-a0d7-50309a302ba3', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('477ba4ea-0793-44ff-b983-d7bde2635d6c', '0728fb50-f232-4980-b38d-515a1cf8e0a8', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('485ea206-e446-44f5-9508-bf1f74e3fd5a', 'a09bf250-9add-4275-8dd8-46adcee6f115', '002c4d13-da17-45ff-8479-e4761fb738dd', 95000, 1, 95000, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081570),
('4c153798-90df-43f0-b5af-712359a3e179', '510249fe-489c-4e75-916b-86a6b2c1b10c', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 1, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('50cf037f-55d9-4252-8878-4c16b1095339', '557f4750-aa44-43a8-b675-ab5190f378ea', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 3, 3, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('51df9307-cf14-480a-a5dc-384c2e273935', '22b444a2-e712-4e7c-bfd9-22626c256b2f', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 299, 1, 299, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('52103d6f-036f-4a17-8798-429c611f987a', 'e493e506-9328-4447-924f-b7b526d4ce99', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081735),
('54b759a1-b88c-408c-b462-2b86c58fa9ee', '4a4c5cc6-646a-4f36-8de1-35c470023039', '41491d6c-4bdc-430a-99ec-019fc1abf102', 250800, 1, 250800, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081633),
('55855d6b-732e-4fb8-925b-830ae3e9c9bc', '14fe2e94-1351-4d8a-b81f-d33f988c3fc8', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('578f0f73-89e4-4895-b69d-5eaa97b498f1', '60cc3577-ab3a-41b0-a661-d4e3212473a7', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 108, 1, 108, '2021-03-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('57d5d57f-12b8-44b1-b581-965c9305b85b', '8a58290c-51bf-47d2-b856-3c9b4c781dfe', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 299, 1, 299, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('5a59f0b9-c6f4-47fa-9943-76c59a246d98', 'b7a3b2b8-7a89-4f27-a0d6-1e1994e10a76', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('5a7c48ad-0c6b-4d46-bec8-d9b000ff0152', '22b444a2-e712-4e7c-bfd9-22626c256b2f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('5b54b48e-5e70-4664-b4cf-dc1534ab4517', '0b003e43-4474-476f-914f-098af5e6eeb1', '0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 213, 1, 213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('5d3a1d1b-7a2b-496f-be87-44fe3b4400d2', 'cf121bff-52a6-4049-ae5e-03859ccbce09', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('5d8772cc-33e2-47db-bdb6-4ac3973cc5b8', '76927ede-8945-4307-8fb7-2b2a32239182', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 100, 2, 200, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('61e47bf5-da93-4226-954c-87329142c8ac', '50d95736-8090-42dd-9425-839669408652', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 35100, 2, 70200, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 0, NULL, 32081725),
('65497878-a675-45b8-b2f2-5b8b0301365b', 'ec63a361-a026-4de3-9e1c-dfa85682757f', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('65ff2da7-c09c-4cdb-85ee-55da2c3a2749', 'a6ac203f-5333-4e1c-9668-3747ddf3f585', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081598),
('66313ea7-d448-404b-9855-088b627402cf', '9782a868-63d4-4e9c-8660-d926a8872b12', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('6f3523bb-84c5-4ac9-9cd2-9b24a02df012', '65b5da6d-94ed-44b9-aae8-839f33238574', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 2, 244, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('6f6fc3bb-bb11-4edf-8969-0abbdb8ca481', '9cafc570-e787-4c3c-b49f-a5301ccdde37', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 2, 200, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('6ff17b3b-0b15-4253-85c5-49cffdbe42c0', '76ec53eb-0f2f-4fe1-a3e4-e0b6d879e99a', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081293),
('708ac8ff-6668-47a5-a504-94a4186670ec', 'b175cbc7-ad33-4368-a329-119e2557043f', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('71c5bd05-ca03-4887-97b1-87276deb32ef', '9ae70653-a3ca-4cc4-8afd-d09b0cb156fe', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('74cd45a2-50a4-4460-b2ed-099eb5cfc141', 'a79e7553-c9a0-4fd4-abe1-5054731c31be', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('74ffac2d-e252-4d82-b343-f5e0ff77dda8', 'e05bbd4e-4de8-46f5-9b05-90af8b89b76f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('763520d6-2221-4151-b17d-b061a2e250d6', 'c332e93a-1101-4e49-bb6b-53741d9a2958', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 1, 3, '2021-03-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32079807),
('78e30e1d-d151-48cf-abd1-1a934ead2c30', '6d434242-c428-499a-9eda-32eb816a2a0d', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081226),
('7d312e14-711f-475b-ba8e-227dfecc3172', '76927ede-8945-4307-8fb7-2b2a32239182', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 2, 6, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('7f33ab08-0976-4f2c-8208-87280d300646', 'ba90a7ff-9650-4b0c-9f26-836a40cffd3b', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 70000, 1, 70000, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081659),
('8057dddb-347b-4e5f-97e6-38126891767b', '8851092a-cdeb-4e1e-9e87-58b4329fc395', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('8149d8b8-19cf-4eee-8339-dd8247729a05', '9d4daf1d-e73f-4417-9958-2a751fc08651', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('83433035-ae16-4958-8179-912a83974f5f', '71d42e8c-bf8b-4e90-b22b-998c6904c02c', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 2132, 1, 2132, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('84239465-f066-4dbc-8b5f-e9cf381b65ba', 'b80675d5-2cbf-4c3f-b08d-358e3a15b517', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 35100, 2, 70200, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081573),
('8442de71-7a20-40b5-95ec-4d7ce8d5f197', '83a9a5bf-f0fd-4345-8c79-0afcc2104336', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 277, 1, 277, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('88638c99-29ff-4a9c-84bc-e736046b57cb', '4a4c5cc6-646a-4f36-8de1-35c470023039', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 3290000, 1, 3290000, NULL, 'рапр', 'пвап', NULL, '2000-01-01', NULL, NULL, 32081636, 32081637),
('895c0b3d-3eda-4605-87f3-bb9fe55ebd3c', 'd792bf5e-fba4-4d4d-82d6-02511eca8422', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('8d2bde23-a85d-4cb0-833b-9823de3e26a4', '557f4750-aa44-43a8-b675-ab5190f378ea', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 3, 300, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('8e209fb3-b2da-4bc4-8335-124c69bad77b', '52321a81-e473-45ab-96d7-341f63042529', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 1, 3, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('8e76d1a7-1e20-4cf5-b5c9-700c4e3b5aed', 'b7b47fb1-5ce4-49e4-b2a8-0a3985c2b416', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('90ac204d-8968-469f-bd86-4afc2ee74197', '3b32ea50-84ef-4b44-a9ac-0b76a85e4651', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('91e8b08a-d593-48b0-a846-7097e0ed1bda', 'e10e9cfc-847a-45a8-86ef-3a6b1b62276f', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('98f3866a-f444-4179-849a-00ddbadf6c54', 'd63c8ab7-1ff5-4ba3-bd01-96fb5828bb82', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 145000, 1, 145000, '2021-03-16', 'Литвинкова', NULL, NULL, NULL, NULL, 0, NULL, 32082857),
('9a1af933-8244-4727-99f6-ac81312e5dc3', 'd792bf5e-fba4-4d4d-82d6-02511eca8422', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 2, 6, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('9a1bb2ea-925c-4f14-8de4-cb24ee9a610b', '7cdd8b6f-5c90-43fb-bd83-6d4d72fb98f3', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, 1, 3790000, NULL, 'sdfsdf', 'sfsdf', NULL, '1232-12-12', NULL, 0, 32081674, 32081675),
('9b61231d-291a-4d25-856a-a21407634610', '231c026c-0ea7-4495-b6bf-ba93e657878e', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, 1, 3790000, NULL, 'орпо', 'парп', NULL, '2000-01-01', NULL, 0, 32081641, 32081642),
('9e067d4d-a90a-4f3a-8e96-45017d854c4d', '01b15d32-064c-4d1c-a0d7-50309a302ba3', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 1, 100, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('9ecbb010-2e22-4d74-b434-576782cb265d', '09fd78c4-f751-412d-8282-4b6e49b4ff54', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 145000, 1, 145000, '2021-03-17', 'Литвинкова', NULL, NULL, NULL, NULL, NULL, NULL, 32081555),
('9f50c57c-eba3-44c2-abd0-b3e7456b2724', '2ba47f73-e2ae-4ba6-b9df-d380f5c54821', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('9fe048af-f70f-44a7-83a3-92bfc5313ae0', 'c66a702a-1150-46ee-8e15-40d246ad5481', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a121f3e0-884a-4eee-a811-be7bb23034b6', 'd792bf5e-fba4-4d4d-82d6-02511eca8422', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a16f2a33-5704-42bf-81fd-713cc92d00c7', '6b8db812-4bad-4280-846a-4d9bcd727bb2', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a4b823eb-11ae-499d-96ea-fea913925ea9', 'ba5e7e09-c205-451b-b8b6-796a25212fac', '0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 213, 1, 213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a5854402-f5d2-43c7-bc74-763f6c3f88a5', 'f178bae5-4eba-45ee-8d1d-ad3cb74764e6', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a74ee649-0c43-47e2-b120-490b91b16856', 'cc0890a1-ccb4-4ee9-b8f1-f4df7595918f', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a8ec1527-489b-440f-a77e-f93dbff4a2d9', 'd63c8ab7-1ff5-4ba3-bd01-96fb5828bb82', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1590000, 1, 1590000, NULL, 'Литвинково', 'Рере', NULL, '2000-02-02', NULL, 0, 32081526, 32082853),
('aa69bf23-c946-45cf-a6a3-2353dc9f194e', 'd63c8ab7-1ff5-4ba3-bd01-96fb5828bb82', '35f028ec-512b-4ff9-b077-8179c6eef588', 501500, 1, 501500, '2021-03-16', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32082855),
('aaedb8df-0d62-4a64-8ac8-29b8de260d25', '71d42e8c-bf8b-4e90-b22b-998c6904c02c', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 2132, 1, 2132, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('aea54c45-48c6-4dbb-a658-f293011c34a5', 'e493e506-9328-4447-924f-b7b526d4ce99', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 70000, 1, 70000, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081733),
('b2359105-743a-43d4-8dd1-70febf2619c6', '786249e6-59ac-4136-8e24-991d059d2052', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b44b8c80-b222-487e-bf87-6ed98965396e', 'cf121bff-52a6-4049-ae5e-03859ccbce09', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b67a513a-b3c0-4eff-b836-8d89c86c7ae3', 'f4dc357a-e576-403e-b020-e9ddcbdcefd3', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b6b23209-02e8-4372-865f-3a807443c3c5', '14fe2e94-1351-4d8a-b81f-d33f988c3fc8', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b73992e9-366a-44f8-bccc-d8da67922ef2', 'd792bf5e-fba4-4d4d-82d6-02511eca8422', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b73a551b-b4e9-4560-8993-15def9315919', '7c13a4d2-a3ec-4b2f-a330-182e78d4590e', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b9ca11c1-53db-4112-b8ff-6f2a6ea90d35', 'a0ff7244-ab40-4d7c-b605-5d0bfb682cc7', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 99000, 1, 99000, '2021-03-19', NULL, NULL, NULL, NULL, NULL, 0, NULL, 32082213),
('baf5e778-e059-43c2-accd-91eb3f408e72', '71d42e8c-bf8b-4e90-b22b-998c6904c02c', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 277, 1, 277, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('bb5c8e41-f1b2-4152-8f17-58098c54e645', 'be2d81e1-30ec-43a6-b7c3-e58544597e30', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('bc9d7862-e258-47ad-8c45-fa34c0279108', '24b3cf47-01e4-4119-bd36-1ad076e933c3', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('bcb1ab5e-8e27-41e7-96ad-ea03801c30e8', 'def3e6e9-34b7-4bd8-943b-4618711f3e0e', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('bdb383ad-cd6a-4207-94d5-96ae56e14ab2', 'a6ac203f-5333-4e1c-9668-3747ddf3f585', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081600),
('be756fe6-6c63-4be4-a0b9-1df6d8305a79', '57528959-12ca-46d4-849c-cecb85347fcc', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 108, 1, 108, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c08a7386-27c2-40a7-a1a5-9cb3a1d10d0b', 'ec63a361-a026-4de3-9e1c-dfa85682757f', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c37b4e4c-ddbe-46aa-9717-bf614c609d8c', 'bd7c74e6-86a7-4cb7-959e-b05c12f2c01f', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c6b2de97-6e00-4e4b-9a6c-e571de0cc4c7', 'b9542334-bbfa-44cc-8e1b-fe9d24909185', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, '2021-03-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081400),
('c6e39cb2-f335-4a71-a67c-0bd470c845b0', '2cee7532-c4d1-477f-901e-256ede02561d', '906c9040-3d9f-4517-b16b-5df8115f16bc', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c753ad4f-e166-416d-840d-e75351766472', '68519880-017e-40c6-8efe-20daa85ee08c', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c82dc8bd-9acc-4fe1-b2b7-712edef23daa', 'a49c0195-25e9-430f-860b-65214fdc02ee', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c895ce5a-bec9-4d6b-9a75-42e3e48429a1', '1b3880f1-21b8-4dc9-929e-699fd87c20d7', '0e2760d7-87ae-4477-841c-168ad557850c', 3232, 1, 3232, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c93551bf-2571-40e8-9d87-1f3cd4ddcd26', '4381816c-9743-4241-97c5-47ae5709650c', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('caeb45f6-e980-492a-b231-887497019fcf', '8a58290c-51bf-47d2-b856-3c9b4c781dfe', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 1, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('ce27ddb2-042c-4ac1-bea5-b5e319d41d73', 'b2caacc4-9a6b-4c81-b8db-1e53083a8004', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('cf3bb3e9-7cbe-412b-a124-325b9bdb51be', 'b282761e-e173-4b01-8d13-7fe203b1ad3c', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 3, 9, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('d16835cb-753d-4419-aded-907e67f2b769', '7dcd035a-23e8-4cde-987d-15b6cb6e79e6', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('d1e479e5-f4dc-4694-9771-b258ba52fb09', '129aeafa-dd13-484d-a98a-c4a533d5bdbd', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('d43e0cd8-d8f7-483c-ae19-2361a063477b', 'f178bae5-4eba-45ee-8d1d-ad3cb74764e6', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('d48cbd54-8566-4fc4-8f9e-27c7a3d73aaf', '09fd78c4-f751-412d-8282-4b6e49b4ff54', '35f028ec-512b-4ff9-b077-8179c6eef588', 501500, 1, 501500, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081553),
('d55eaaa1-a3df-478e-bfa0-1a16ab07fc88', '2ba47f73-e2ae-4ba6-b9df-d380f5c54821', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('d5635e58-4cd8-4dc5-8194-de00b777c558', 'd3152913-9646-4d9d-962c-b69ae5501f5e', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('d885c755-9a82-4495-815e-57efa3a415e2', 'f178bae5-4eba-45ee-8d1d-ad3cb74764e6', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('dcbd1c74-eace-4905-b097-cc351c2c33fb', '869c957f-b1ad-46bd-999d-da943aebae3e', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('dd97896d-83bc-496f-b823-8839b14b668c', 'd792bf5e-fba4-4d4d-82d6-02511eca8422', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('deb3b32f-f08c-4e3a-9e72-6c558683609e', '686bfb31-6d15-4294-a32c-ca0d22f87848', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 2, 2, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32080462),
('deb92592-57a6-4861-b16c-4e2e5c9ddb31', 'cf121bff-52a6-4049-ae5e-03859ccbce09', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('dfaa947f-5d10-4b59-b976-62a329ed2e3b', '95cca975-6614-49e0-a546-62f71e2b1c3d', '35f028ec-512b-4ff9-b077-8179c6eef588', 199, 1, 199, '2021-03-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081528),
('dfd15027-39db-4ba4-9955-df6c8b62f35c', '98fe2307-3872-4513-8333-904bb4ef45ca', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 2, 2, '2021-03-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('e043d5e2-805b-46e3-8b56-571acbe2c7c6', 'b282761e-e173-4b01-8d13-7fe203b1ad3c', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 2, 200, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('e2123f97-02c0-43e9-9297-1a3bf3544747', '85584c49-3de8-4d32-9d82-97d4c73f29fc', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081223),
('e2248314-94d7-4ee4-a4a1-9540201a2a06', '8ade2123-24a7-4575-b4b6-a925cb922f5c', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 35100, 1, 35100, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 0, NULL, 32081662),
('e380e68c-17bf-4dc3-9c5a-2ff8372051a8', '7e173dc1-6836-493f-bf57-880ba2362b56', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('e4cea036-1402-44ce-a01e-2fd500e6f6a8', 'bbbb9f6c-55c8-42ef-b731-c46076cf0b03', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('e84927ab-2c7d-4466-b4b2-098c4f9a448a', 'a09bf250-9add-4275-8dd8-46adcee6f115', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1590000, 1, 1590000, NULL, 'Ноис', 'Илли', NULL, '2000-02-02', NULL, NULL, 32081565, 32081566),
('e93789ae-25ce-4d0f-8c58-ab2c81151e79', '81c38f92-73ed-4df1-a729-f4f831eee1da', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('e97c9f8e-982b-4a23-9f4a-32f35540fba0', 'e493e506-9328-4447-924f-b7b526d4ce99', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 70000, 1, 70000, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081737),
('e99b762a-b07e-4f96-b5e9-fb8971de8f06', 'cf121bff-52a6-4049-ae5e-03859ccbce09', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('ec050f8f-ff53-4b59-98c5-90f3f164e73b', 'f7ad6daf-b10b-4bc3-b280-c37d583b5af4', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('ed0f1571-9b2f-4eee-a942-c4b159468f59', '8f5cb396-ed5e-42af-8d99-c72dda571d66', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 145000, 1, 145000, '2021-03-16', 'Литвинкова', NULL, NULL, NULL, NULL, NULL, NULL, 32081531),
('f05cc363-0d2f-4e32-95d5-f9e42dfc8fda', '4acfbadc-cbc3-4272-8edf-0a9123a90124', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 35100, 1, 35100, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 0, NULL, 32081728),
('f15e562c-12fb-433e-8aa8-10a13cf3f1f8', '0b003e43-4474-476f-914f-098af5e6eeb1', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('f289833c-8768-44bd-a074-16f5f5ac6ed2', 'ba5e7e09-c205-451b-b8b6-796a25212fac', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('f5469e34-15f2-46b7-b0ae-c1386e4620d8', 'ba90a7ff-9650-4b0c-9f26-836a40cffd3b', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, 1, 135000, '2021-03-18', NULL, NULL, NULL, NULL, NULL, 35600, NULL, 32081657),
('f55adeae-0492-4db1-933b-1007f13ae196', '966b1431-1115-474a-b6f7-1005f945f11b', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, 1, 1, '2021-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('f5fc21ad-f5fc-4de3-b385-e1c5d4f05f47', 'a6ac203f-5333-4e1c-9668-3747ddf3f585', '002c4d13-da17-45ff-8479-e4761fb738dd', 95000, 1, 95000, '2021-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32081602),
('f62a65d7-834b-47e4-8743-8cdb6565cc70', 'f05f5413-3bf8-42ce-9ee8-e1246158dc9a', '17ec8006-84b6-41e7-aedf-349b83efcca1', 17100, 1, 17100, '2021-03-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32080955),
('fbf957d0-b6ba-41dd-b8e6-dcb5229523ba', 'd792bf5e-fba4-4d4d-82d6-02511eca8422', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 100, 1, 100, '2021-03-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('fc576c87-908a-4db4-84fe-d76e957ff3c0', 'a9e7ac0b-53bb-4651-810c-90ad41bd59f9', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32079804),
('ff5d7792-b1a0-4f5b-88b1-68360c598f19', '01b15d32-064c-4d1c-a0d7-50309a302ba3', 'fc285538-b95f-4b04-982e-9728852681ec', 100, 2, 200, '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `order_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `pay_system_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `success_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `external_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `order_id`, `pay_system_id`, `success_url`, `external_id`, `redirect_url`, `status`, `created_at`) VALUES
('01d28fe8-b977-4c3d-9ecc-7ee1b579c5d4', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '023116f9-f901-7836-aa5b-cdca01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=023116f9-f901-7836-aa5b-cdca01ed6cd3', 100, '2021-02-19 14:51:19'),
('069aaa3e-a753-47c7-83da-5984f4628112', '4acfbadc-cbc3-4272-8edf-0a9123a90124', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=4acfbadc-cbc3-4272-8edf-0a9123a90124', '02b512e2-4214-7f71-9dc0-11a801f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02b512e2-4214-7f71-9dc0-11a801f9058f', 200, '2021-03-18 11:49:26'),
('14ad3e44-ebc2-4adb-828f-5c5b36042858', '57528959-12ca-46d4-849c-cecb85347fcc', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=57528959-12ca-46d4-849c-cecb85347fcc', '0287db3a-438d-70b5-aa38-bf6301f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0287db3a-438d-70b5-aa38-bf6301f9058f', 200, '2021-03-09 06:51:33'),
('1b95bf73-4153-4666-831d-72fc7129658a', '65b5da6d-94ed-44b9-aae8-839f33238574', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=65b5da6d-94ed-44b9-aae8-839f33238574', '027aac78-b63b-71d4-b6c2-ea6701f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=027aac78-b63b-71d4-b6c2-ea6701f9058f', 100, '2021-03-06 14:26:22'),
('231f57eb-d846-42f3-a86a-48a074bd852f', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 13:31:42'),
('3175a13f-a5bd-458d-8e47-8d794b587f1e', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 13:44:44'),
('337be55f-2556-4b7a-be15-456802ab8ec9', '80ca16a1-27d4-4fa4-9150-8b6f49f076bd', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=80ca16a1-27d4-4fa4-9150-8b6f49f076bd', '028c8aeb-9632-7b7c-b3f4-6b9e01f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=028c8aeb-9632-7b7c-b3f4-6b9e01f9058f', 200, '2021-03-10 05:45:35'),
('338202b3-4d3f-4817-b99b-5cfc13ca2f1d', '7bee03dd-f1a4-4f6c-bc83-b85e032173f0', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=7bee03dd-f1a4-4f6c-bc83-b85e032173f0', '02670fac-cad6-7fff-8601-599301ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02670fac-cad6-7fff-8601-599301ed6cd3', 100, '2021-03-02 14:35:55'),
('349afb56-bd5e-49da-b6c6-b5beb1f5b16d', '50d95736-8090-42dd-9425-839669408652', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=50d95736-8090-42dd-9425-839669408652', '02b50671-82ad-7fcd-b996-3efd01f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02b50671-82ad-7fcd-b996-3efd01f9058f', 100, '2021-03-18 11:35:11'),
('3707a960-1416-4f19-90cb-db826ed1f4dd', '7dcd035a-23e8-4cde-987d-15b6cb6e79e6', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=7dcd035a-23e8-4cde-987d-15b6cb6e79e6', '0275f4a8-9a04-762a-9c26-82aa01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275f4a8-9a04-762a-9c26-82aa01ed6cd3', 100, '2021-03-05 15:23:01'),
('38629a4e-174c-43c2-81a8-750e8c72bc9d', '966b1431-1115-474a-b6f7-1005f945f11b', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=966b1431-1115-474a-b6f7-1005f945f11b', '02790421-2a2c-747e-a8d7-046801f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02790421-2a2c-747e-a8d7-046801f9058f', 100, '2021-03-06 06:20:21'),
('3963092e-b2ed-4b59-9ef5-fbda503fbf68', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '02310528-0f36-77f2-b945-21f401ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02310528-0f36-77f2-b945-21f401ed6cd3', 100, '2021-02-19 14:30:54'),
('39754f4a-1de0-4909-b21f-caa9d4296061', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', 'https://ya.ru', '0235a9dd-1f5c-73cc-abc4-fc7501ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0235a9dd-1f5c-73cc-abc4-fc7501ed6cd3', 100, '2021-02-20 13:12:21'),
('49a307b5-9306-42ef-93a6-4ee76a6cfc36', 'c71cc515-8b2c-43e2-9eaf-4a0fa3a3b226', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=c71cc515-8b2c-43e2-9eaf-4a0fa3a3b226', '027489e3-dfa1-796d-a987-c37b01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=027489e3-dfa1-796d-a987-c37b01ed6cd3', 100, '2021-03-05 08:27:32'),
('5161cf7a-0a94-416a-803e-37ccde532867', 'b7b47fb1-5ce4-49e4-b2a8-0a3985c2b416', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=b7b47fb1-5ce4-49e4-b2a8-0a3985c2b416', '027438d8-9e49-7d3a-adf2-0ff601ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=027438d8-9e49-7d3a-adf2-0ff601ed6cd3', 100, '2021-03-05 06:54:43'),
('55712c13-cce7-4c5b-b1ae-ddfb33d75d77', '01b15d32-064c-4d1c-a0d7-50309a302ba3', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=01b15d32-064c-4d1c-a0d7-50309a302ba3', '0275ce31-3dce-7bfd-88a2-ce2401ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275ce31-3dce-7bfd-88a2-ce2401ed6cd3', 100, '2021-03-05 14:38:58'),
('57eb6af7-80f3-4ebe-8b6e-2c848dd9eb32', '85584c49-3de8-4d32-9d82-97d4c73f29fc', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=85584c49-3de8-4d32-9d82-97d4c73f29fc', '02a64a64-4ee7-7c16-a6ea-558901f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02a64a64-4ee7-7c16-a6ea-558901f9058f', 100, '2021-03-15 11:34:58'),
('594c32aa-79b5-4a07-954f-84b0e62e2845', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '023118c6-999f-7cc0-8683-1d0001ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=023118c6-999f-7cc0-8683-1d0001ed6cd3', 100, '2021-02-19 14:53:23'),
('5fa2dd20-df00-4581-a06f-5ce6e5ad50cb', '9cafc570-e787-4c3c-b49f-a5301ccdde37', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=9cafc570-e787-4c3c-b49f-a5301ccdde37', '02752ec2-e5dc-7fb3-b5e2-c97801ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02752ec2-e5dc-7fb3-b5e2-c97801ed6cd3', 100, '2021-03-05 11:36:22'),
('600e4a1d-9961-4d9d-b7ab-6895797a49ab', 'be2d81e1-30ec-43a6-b7c3-e58544597e30', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=be2d81e1-30ec-43a6-b7c3-e58544597e30', '024de3e7-a066-73db-b596-830801ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=024de3e7-a066-73db-b596-830801ed6cd3', 100, '2021-02-25 11:35:42'),
('63304688-068a-4388-8926-0b4948f58d2c', 'c28c18a5-5afd-4ce1-aa2b-01d18debeb2a', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=c28c18a5-5afd-4ce1-aa2b-01d18debeb2a', '0275ef8f-a0e9-7406-a140-1a1101ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275ef8f-a0e9-7406-a140-1a1101ed6cd3', 100, '2021-03-05 15:17:11'),
('63327f2c-0d94-42bf-985e-f25c51b48555', '0728fb50-f232-4980-b38d-515a1cf8e0a8', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=0728fb50-f232-4980-b38d-515a1cf8e0a8', '024df8c1-c4a1-73e0-a0c0-778a01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=024df8c1-c4a1-73e0-a0c0-778a01ed6cd3', 100, '2021-02-25 11:59:35'),
('648a87df-d030-4fd6-a656-5627f9bdd6e7', 'efdd541d-476e-4e08-9610-db4b83fa4f87', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=efdd541d-476e-4e08-9610-db4b83fa4f87', '0275485a-8e86-711b-863f-e2b701ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275485a-8e86-711b-863f-e2b701ed6cd3', 100, '2021-03-05 12:05:40'),
('7321d9df-f63b-40ed-81f6-f9a6de18fa71', '76927ede-8945-4307-8fb7-2b2a32239182', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=76927ede-8945-4307-8fb7-2b2a32239182', '0275984a-5b4a-7fff-b405-463901ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275984a-5b4a-7fff-b405-463901ed6cd3', 100, '2021-03-05 13:37:14'),
('746159d6-46e2-4b48-b531-190c184527ce', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 13:42:58'),
('76316d99-2757-4002-908a-67b38a002543', '4be90a44-7644-4607-b70d-3c5712891f54', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=4be90a44-7644-4607-b70d-3c5712891f54', '027531dd-c7e2-7762-8411-d1d101ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=027531dd-c7e2-7762-8411-d1d101ed6cd3', 100, '2021-03-05 11:39:55'),
('79784a79-1856-4612-a3fd-28be8d6f2e5a', '24b3cf47-01e4-4119-bd36-1ad076e933c3', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=24b3cf47-01e4-4119-bd36-1ad076e933c3', '027aac5f-0135-78c9-ac6c-85b401f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=027aac5f-0135-78c9-ac6c-85b401f9058f', 200, '2021-03-06 14:26:15'),
('7e95c9d0-89af-40ec-971b-ddf5446f0742', '557f4750-aa44-43a8-b675-ab5190f378ea', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=557f4750-aa44-43a8-b675-ab5190f378ea', '02754978-98db-710f-8673-d08e01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02754978-98db-710f-8673-d08e01ed6cd3', 100, '2021-03-05 12:06:58'),
('7f877722-50c7-497b-86d4-1e0404e879f1', '14fe2e94-1351-4d8a-b81f-d33f988c3fc8', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=14fe2e94-1351-4d8a-b81f-d33f988c3fc8', '0274f9e5-6913-7bf6-bfb1-c9cd01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0274f9e5-6913-7bf6-bfb1-c9cd01ed6cd3', 100, '2021-03-05 10:35:49'),
('821a28c4-f814-4f24-962d-1f2afb2f33a2', 'b282761e-e173-4b01-8d13-7fe203b1ad3c', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=b282761e-e173-4b01-8d13-7fe203b1ad3c', '0275d98c-ff8e-7afd-8bef-6dd101ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275d98c-ff8e-7afd-8bef-6dd101ed6cd3', 100, '2021-03-05 14:51:59'),
('8489e630-7209-4dce-8468-059520191f9e', '231c026c-0ea7-4495-b6bf-ba93e657878e', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=231c026c-0ea7-4495-b6bf-ba93e657878e', '02b1a01c-5013-79b9-bf10-9d2f01f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02b1a01c-5013-79b9-bf10-9d2f01f9058f', 200, '2021-03-17 18:58:22'),
('8bcc6cfc-0bcd-422d-98b4-02c47661c4f1', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 13:42:43'),
('8efa5b9c-49ed-4bb1-ab33-073c5e2d1fb8', 'a6ac203f-5333-4e1c-9668-3747ddf3f585', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=a6ac203f-5333-4e1c-9668-3747ddf3f585', '02b068f6-f698-778d-a9a0-6a0101f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02b068f6-f698-778d-a9a0-6a0101f9058f', 200, '2021-03-17 13:02:00'),
('a3682b46-4ce5-4d93-834f-238733be9844', '4a4c5cc6-646a-4f36-8de1-35c470023039', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=4a4c5cc6-646a-4f36-8de1-35c470023039', '02b0fe2f-6724-725f-87f0-752d01f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02b0fe2f-6724-725f-87f0-752d01f9058f', 200, '2021-03-17 15:52:55'),
('a3e0b5e2-c7f4-4c3f-b506-e4abb6749bec', '2857914d-47dc-4ace-9434-2c98a0cf04b8', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=2857914d-47dc-4ace-9434-2c98a0cf04b8', '027ac06c-b9ac-7b62-bdf5-c8b901f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=027ac06c-b9ac-7b62-bdf5-c8b901f9058f', 100, '2021-03-06 14:49:13'),
('a66a9458-0c36-4542-9d3a-e33db04791fb', '5c3f3a44-6b2d-481a-9679-e915eecba961', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=5c3f3a44-6b2d-481a-9679-e915eecba961', '02a51784-ed5c-7187-9562-843701f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02a51784-ed5c-7187-9562-843701f9058f', 200, '2021-03-15 05:43:30'),
('aae7ca41-5de7-441c-9870-8cac6bf7e951', '4381816c-9743-4241-97c5-47ae5709650c', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=12345', '0249119e-eb43-703d-b654-521501ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0249119e-eb43-703d-b654-521501ed6cd3', 100, '2021-02-24 12:02:03'),
('ae8815d2-2919-4bca-9141-82b5aa60fcec', 'e8529161-8ef1-4903-810f-47136df10e3a', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket', '02490aaf-d123-7dbe-8fda-3f9701ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02490aaf-d123-7dbe-8fda-3f9701ed6cd3', 100, '2021-02-24 11:54:07'),
('afc32391-777d-4f27-93a5-625cd6ca02e9', 'bbbb9f6c-55c8-42ef-b731-c46076cf0b03', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket', '024dc69e-b048-7fb4-aaf7-2a4a01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=024dc69e-b048-7fb4-aaf7-2a4a01ed6cd3', 100, '2021-02-25 11:02:10'),
('be60dc8f-d188-46af-8da3-ade74137852b', '81c38f92-73ed-4df1-a729-f4f831eee1da', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=81c38f92-73ed-4df1-a729-f4f831eee1da', '02792023-4c4c-79f4-a565-360601f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02792023-4c4c-79f4-a565-360601f9058f', 200, '2021-03-06 06:52:26'),
('c071047a-3308-42e8-8a59-5cab14695198', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '02311745-d673-75d3-bb81-086601ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02311745-d673-75d3-bb81-086601ed6cd3', 100, '2021-02-19 14:51:39'),
('c251a6cf-8e75-4f3c-9317-c35b5ac6341b', 'bd7c74e6-86a7-4cb7-959e-b05c12f2c01f', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=bd7c74e6-86a7-4cb7-959e-b05c12f2c01f', '0275fdbc-1674-79e7-820a-e96b01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275fdbc-1674-79e7-820a-e96b01ed6cd3', 100, '2021-03-05 15:33:25'),
('c447b2a6-6ca7-4469-8987-b0079368d340', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 14:28:57'),
('c7549a7a-e014-4465-98a0-8be9444bafab', '22b444a2-e712-4e7c-bfd9-22626c256b2f', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=22b444a2-e712-4e7c-bfd9-22626c256b2f', '0275e6bf-91af-76da-949a-e0d401ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275e6bf-91af-76da-949a-e0d401ed6cd3', 100, '2021-03-05 15:07:05'),
('c8ea328c-c8fe-466d-8a23-bc63ab81219b', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 14:26:28'),
('cf14d8cc-670a-430f-81b0-5c4baa28f2a0', '6b8db812-4bad-4280-846a-4d9bcd727bb2', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=6b8db812-4bad-4280-846a-4d9bcd727bb2', '024dd5a5-1b15-7f9c-9911-d05601ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=024dd5a5-1b15-7f9c-9911-d05601ed6cd3', 100, '2021-02-25 11:19:23'),
('d262f0c5-4e7b-451e-a8c8-50f8624ecdf7', '6d434242-c428-499a-9eda-32eb816a2a0d', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=6d434242-c428-499a-9eda-32eb816a2a0d', '02a64cc5-fa20-7545-a6d3-cc0701f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02a64cc5-fa20-7545-a6d3-cc0701f9058f', 100, '2021-03-15 11:37:42'),
('d9485343-933f-4327-bc06-1b11c71d94ef', '52321a81-e473-45ab-96d7-341f63042529', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/basket?order=52321a81-e473-45ab-96d7-341f63042529', '0275a042-1c45-7d10-8853-ca8a01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0275a042-1c45-7d10-8853-ca8a01ed6cd3', 100, '2021-03-05 13:46:21'),
('da1db2fc-3abd-404a-8a0a-a932f03461a3', '2cee7532-c4d1-477f-901e-256ede02561d', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000', '', '', 100, '2021-02-20 14:16:40'),
('dc11116c-e5e7-41e7-85b5-a1be3d6473f5', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 14:16:14'),
('e4dc0437-1072-4a7f-9e74-64de5e821539', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '023116e6-926e-7b0f-8d44-0c6e01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=023116e6-926e-7b0f-8d44-0c6e01ed6cd3', 100, '2021-02-19 14:51:14'),
('e6515645-b6c2-4a3c-892f-9527b0a2a2a6', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 14:21:43'),
('e9191f8e-4419-407b-8cb6-9d3fbea70b85', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 14:24:06'),
('ebf029f2-4995-4005-8f5c-ead1725512a2', '9d4daf1d-e73f-4417-9958-2a751fc08651', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=9d4daf1d-e73f-4417-9958-2a751fc08651', '02671939-5441-77a0-b910-2d0e01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02671939-5441-77a0-b910-2d0e01ed6cd3', 100, '2021-03-02 14:46:51'),
('efce2519-a27b-4abe-b3f3-c00e075d14c4', 'cc0890a1-ccb4-4ee9-b8f1-f4df7595918f', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=cc0890a1-ccb4-4ee9-b8f1-f4df7595918f', '0274b94a-5021-7aaf-a0e7-589101ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=0274b94a-5021-7aaf-a0e7-589101ed6cd3', 100, '2021-03-05 09:21:48'),
('f0fecc78-2000-4384-b3af-87ebe7c1461e', 'e493e506-9328-4447-924f-b7b526d4ce99', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=e493e506-9328-4447-924f-b7b526d4ce99', '02b5154d-1647-7403-a6b4-f70c01f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02b5154d-1647-7403-a6b4-f70c01f9058f', 200, '2021-03-18 11:52:12'),
('f4a52e37-a0ef-43c2-9f06-3ff123cc4e78', 'a09bf250-9add-4275-8dd8-46adcee6f115', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=a09bf250-9add-4275-8dd8-46adcee6f115', '02b00824-bb08-737f-9d4f-db0001f9058f', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=02b00824-bb08-737f-9d4f-db0001f9058f', 200, '2021-03-17 11:11:07'),
('f785293f-2715-46ab-8117-455d13a9b71f', '34ca7a37-9223-4902-9d73-c03129209ffa', '30bbe632-837e-4d43-a18c-13297784400e', '', '', '', 100, '2021-02-19 14:22:28'),
('f9fa3e2f-e02c-40b9-99a7-eeb561b3e0b3', 'f4c4338a-1d21-428e-9f3a-b187900001f0', '30bbe632-837e-4d43-a18c-13297784400e', 'http://localhost:3000/basket?order=f4c4338a-1d21-428e-9f3a-b187900001f0', '027430fb-25c3-73f5-ad08-3c8e01ed6cd3', 'https://web.rbsuat.com/ab/merchants/typical/payment_ru.html?mdOrder=027430fb-25c3-73f5-ad08-3c8e01ed6cd3', 100, '2021-03-05 06:45:42'),
('fdcbf1f0-9d1d-48d5-bbf2-cceead479098', '2cee7532-c4d1-477f-901e-256ede02561d', '30bbe632-837e-4d43-a18c-13297784400e', 'https://dev.rosakhutor.zbrsk.ru/', '', '', 100, '2021-02-20 14:17:36');

-- --------------------------------------------------------

--
-- Table structure for table `pay_system`
--

CREATE TABLE `pay_system` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `name` json NOT NULL,
  `provider` smallint UNSIGNED NOT NULL,
  `description` json DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pay_system`
--

INSERT INTO `pay_system` (`id`, `name`, `provider`, `description`, `code`, `image_id`, `created_at`, `updated_at`) VALUES
('30bbe632-837e-4d43-a18c-13297784400e', '{\"ru\": \"AlfaBank\"}', 100, NULL, NULL, NULL, '2021-02-19 15:08:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `periods`
--

CREATE TABLE `periods` (
  `id` int NOT NULL,
  `pps_settings` json NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `title` json NOT NULL,
  `description` json NOT NULL,
  `sub_title` json DEFAULT NULL,
  `sub_description` json DEFAULT NULL,
  `working_hours` json NOT NULL,
  `age_restriction` json DEFAULT NULL,
  `label` json DEFAULT NULL,
  `address` json DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_price` bigint DEFAULT NULL,
  `is_free` smallint UNSIGNED NOT NULL,
  `is_cable_way_required` smallint UNSIGNED NOT NULL,
  `button_text` json NOT NULL,
  `width` int DEFAULT NULL,
  `background_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `is_active` smallint UNSIGNED NOT NULL,
  `sort` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`id`, `title`, `description`, `sub_title`, `sub_description`, `working_hours`, `age_restriction`, `label`, `address`, `phone`, `code`, `min_price`, `is_free`, `is_cable_way_required`, `button_text`, `width`, `background_id`, `is_active`, `sort`, `created_at`, `updated_at`) VALUES
('014779eb-17b8-402f-96d2-3368f8f7fb27', '{\"en\": \"\", \"ru\": \"Супермаркет Перекресток\"}', '{\"en\": \"\", \"ru\": \"Перекресток – федеральная розничная торговая сеть, одна из первых сетей городских супермаркетов.\"}', '{\"en\": \"\", \"ru\": \"Супермаркет Перекресток\"}', '{\"en\": \"\", \"ru\": \"Перекресток – федеральная розничная торговая сеть, одна из первых сетей городских супермаркетов. Концепция обновленных супермаркетов позволяет создать все условия для комфортного совершения покупок за счет эффективной организации торгового зала, его зонирования и оформления.  Здесь вы можете приобрести продукты и товары первой необходимости.\"}', '{\"en\": \"\", \"ru\": \"09:00 — 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, Набережная Лаванда, 2\"}', '8-800-200-95-55', 'supermarket-perekrestok', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 27, '2021-04-12 22:11:12', NULL),
('0211d5e3-e291-4928-a75a-df8591f3f035', '{\"en\": \"\", \"ru\": \"Ресторан Москва\"}', '{\"en\": \"\", \"ru\": \"Ресторан с широкой и гостеприимной душой столицы времён Имперской России.\"}', '{\"en\": \"\", \"ru\": \"Ресторан Москва\"}', '{\"en\": \"\", \"ru\": \"Ресторан с широкой и гостеприимной душой столицы времён Имперской России.  «Москва» – хлебосольный семейный ресторан с широкой и гостеприимной душой!\"}', '{\"en\": \"\", \"ru\": \"11:00 – 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, Набережная Лаванда, 9, парк-музей Моя Россия\"}', '8 (928) 854-12-13', 'restoran-moskva', 1100, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 23, '2021-04-12 21:49:08', NULL),
('086a0d7e-24be-4567-ba9e-60d6d9eef721', '{\"en\": \"\", \"ru\": \"Хоспер-бар Mesto BBQ Bar\"}', '{\"en\": \"\", \"ru\": \"В хоспере здесь готовят все: от овощей и бифштексов для бургеров, до классического эталонного стейка.\"}', '{\"en\": \"\", \"ru\": \"Хоспер-бар Mesto BBQ Bar\"}', '{\"en\": \"\", \"ru\": \"В хоспере здесь готовят все: от овощей и бифштексов для бургеров, до классического эталонного стейка. Это меню для активных людей с аппетитом к жизни и еде. Горячее сердце заведения – хоспер, который сочетает преимущества дровяной печи и гриля, сохраняя не только натуральный вкус и сочность продукта, но и полезные свойства.    Открытая кухня – увлекательное зрелище, где каждый сможет не только проследить «рождение» выбранного блюда, но и взять на вооружения приемы шефов.  Тематическая атмосфера и несколько зон для гостей позволит каждому выбрать местечко по душе.   Главная тема меню - блюда из мраморной говядины от лучших производителей страны: классические и альтернативные стейки, давно любимые и оригинальные бургеры – все то, что утолит аппетит после активного дня в горах. Для приверженцев вегетарианства – овощные блюда на углях.   Есть нечто особенное в блюдах на огне. Это не только аппетитный аромат дымка, но и своеобразная отсылка к истокам кулинарии. Ведь приготовление на огне - древнейший метод готовки. Живой огонь не искажает натурального вкуса продукта и сохраняет его полезные свойства.  Будем В MESTE!\"}', '{\"en\": \"\", \"ru\": \"12:00 — 0:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, Набережная Лаванда д.2\"}', '+7 988 500 20 58', 'mesto-bbq-bar', 1000, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 30, '2021-04-12 22:24:19', NULL),
('0875b597-5b36-485c-97af-66bcdc8f1446', '{\"en\": \"\", \"ru\": \"Pyramid Health Club & Spa\"}', '{\"en\": \"\", \"ru\": \"Настоящий оазис гармонии и покоя в отеле Radisson Hotel Rosa Khutor.\"}', '{\"en\": \"\", \"ru\": \"Pyramid Health Club & Spa\"}', '{\"en\": \"\", \"ru\": \"Настоящий оазис гармонии и покоя расположен в самом центре горнолыжного курорта в отеле Radisson Hotel Rosa Khutor. Pyramid Health Club & Spa предлагает исключительно натуральную линию профессионального ухода бренда Anne Semonin, открывающую роскошную коллекцию массажей и уходов за лицом и телом. Уход от Anne Semonin – один из самых эффективных на сегодня среди тех, в которых использованы натуральные ингредиенты и растительные экстракты.  Наши специалисты подберут и осуществят для вас индивидуальный ритуал, учитывая потребности вашей кожи. А тем временем, ваши дети смогут весело провести время в нашем Детском Клубе.  Health Club & Spa Pyramid собрал у себя только лучших специалистов — настоящих мастеров своего дела, прошедших специальное обучение. Комфортные условия проведения процедур, профессионализм и особое внимание позволяют предложить гостям высочайший уровень сервиса и европейское качество услуг. Из всех процедурных, релаксационных комнат открывается прекрасный вид на горы и горную реку Мзымту.\"}', '{\"en\": \"\", \"ru\": \"9:00 – 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, ул. Набережная Панорама, 4, отель Radisson Rosa Khutor\"}', '+7 (862) 243-13-77', 'health-club-spa-pyramid', 4000, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 53, '2021-04-13 16:58:38', NULL),
('0af25a14-ff71-47b5-88c4-32ee31e50f5c', '{\"en\": \"\", \"ru\": \"СПА-банный комплекс ESPAS\"}', '{\"en\": \"\", \"ru\": \"Наполненное чистой энергией и силой гор, это пространство создано для вашего гармоничного отдыха и оздоровления организма.\"}', '{\"en\": \"\", \"ru\": \"СПА-банный комплекс ESPAS\"}', '{\"en\": \"\", \"ru\": \"Территория СПА-банного комплекса ESPAS объединяет банные традиции народов мира. Наполненное чистой энергией и силой гор, это пространство создано для вашего гармоничного отдыха и оздоровления организма.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 00:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, Набережная Лаванда, д. 9\"}', '8-800-222-27-09', 'spa-bannyy-kompleks-espas', 3000, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 54, '2021-04-13 17:02:32', NULL),
('110c9c35-6980-41c4-87a1-8904e87630c6', '{\"en\": \"\", \"ru\": \"Путь к водопадам\"}', '{\"en\": \"\", \"ru\": \"Маршрут вдоль спуска по Южному склону с альпийских лугов в вековой пихтовый лес. Доступен при подъеме на канатных дорогах. 7,5 км.\"}', '{\"en\": \"\", \"ru\": \"Путь к водопадам\"}', '{\"en\": \"\", \"ru\": \"Маршрут проложен в альпийской и лесной среднегорных зонах и позволяет познакомиться с природным ландшафтным и биологическим разнообразием Западного Кавказа.  Вдоль пологого спуска по Южному склону хребта Аибга из залитых солнцем альпийских лугов в тенистый вековой пихтовый лес путников сопровождают грандиозные виды на окружающие долину реки Псоу горы.   Маршрут станет испытанием на выносливость и приведет в тонус мышцы ног.  Перед выходом на маршрут проверьте свою обувь - она должна хорошо фиксировать ногу и обладать крепкой подошвой, иначе прогулка по горам вместо удовольствия принесет муки, а риск травмы будет высок.   В конце маршрута перед возвращением на Роза Пик на кресельной канатной дороге Эдельвейс путешественники смогут восстановить силы в кафе Лес и насладиться прохладой в Парке водопадов Менделиха.\"}', '{\"en\": \"\", \"ru\": \"Закрыто до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Пик, хребет Аибга\"}', '8-800-5000-555', 'pereval-krasivyy', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 35, '2021-04-12 23:21:06', NULL),
('14317abe-c4a0-4cf7-aee5-d741563424e4', '{\"en\": \"\", \"ru\": \"Макдональдс\"}', '{\"en\": \"\", \"ru\": \"Макдональдс — одна из крупнейших сетей быстрого питания в мире.\"}', '{\"en\": \"\", \"ru\": \"Макдональдс\"}', '{\"en\": \"\", \"ru\": \"Макдональдс — одна из крупнейших сетей быстрого питания в мире. В самой известной сети ресторанов быстрого питания использует международную систему контроля качества. Попробуйте и убедитесь в этом сами!\"}', '{\"en\": \"\", \"ru\": \"7:00 — 24:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, площадь Мзымта, 1\"}', '+7 (862) 243-98-95', 'mcdonalds', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 24, '2021-04-12 22:01:42', NULL),
('14a8fba4-4649-4a58-87a7-fd7ec480139a', '{\"en\": \"\", \"ru\": \"Ресторан Ешь хорошо\"}', '{\"en\": \"\", \"ru\": \"Ресторан Ешь хорошо расположен на первом этаже отеля AZIMUT Freestyle Rosa Khutor.\"}', '{\"en\": \"\", \"ru\": \"Ресторан Ешь хорошо\"}', '{\"en\": \"\", \"ru\": \"«Ешь Хорошо» – это не просто ресторан, это кулинарное пространство, где вкусно едят, встречаются за обедом и обсуждают дела, сюда забегают позавтракать и приходят целыми семьями на ужин.\"}', '{\"en\": \"\", \"ru\": \"07:00 – 00:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, наб. Полянка, 4, отель AZIMUT Freestyle 3*\"}', '+7 (938) 441-33-31', 'esh_horosho', 500, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 19, '2021-04-12 18:34:14', NULL),
('15e218e2-0bb2-4c9a-b0e7-e52a37661b7f', '{\"en\": \"\", \"ru\": \"Гранд-кафе Alen Rosso\"}', '{\"en\": \"\", \"ru\": \"Гостей Alen Rosso встречает большой бассейн с живыми моллюсками, морскими ежами и другими вкусными обитателями моря.\"}', '{\"en\": \"\", \"ru\": \"Гранд-кафе Alen Rosso\"}', '{\"en\": \"\", \"ru\": \"Гостей Alen Rosso встречает большой бассейн с живыми моллюсками, морскими ежами и другими вкусными обитателями моря. Есть то, от чего можно остановиться в восхищении, и лишь указать на свой выбор – возможно, это будет омар, или краб, или форель. Особенно Alen Rosso славится деликатесными курильскими устрицами и мидиями Грея, выводя на первый план сочетание изысканного вкуса и исключительной пользы.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 00:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, набережная Лаванда, 5, отель PARK INN by Radisson\"}', '+7 (938) 442-61-42', 'grand-kafe-alen-rosso', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 41, '2021-04-13 15:49:56', NULL),
('19ee3add-466f-4dc5-a543-e85cc69dca20', '{\"en\": \"\", \"ru\": \"Активная студия Electrolux\"}', '{\"en\": \"\", \"ru\": \"Уникальное пространство для взаимодействия с технологиями Electrolux для домашней кухни и ухода за одеждой.\"}', '{\"en\": \"\", \"ru\": \"Активная студия Electrolux\"}', '{\"en\": \"\", \"ru\": \"Уникальное пространство для взаимодействия с технологиями Electrolux для домашней кухни и ухода за одеждой.  В студии, вдохновленной эстетикой скандинавского дизайна, располагаются две зоны: кулинарная – с мастер-классами и интерактивная прачечная. В 2018 году Electrolux открыл первую высокогорную pop-up прачечную, а с появлением кулинарной локации, проект приобрел еще больший масштаб. Так, после катания гости курорта могут увлекательно и продуктивно провести тут и семейный досуг.    В кулинарной зоне, оборудованной техникой коллекции Electrolux Intuit, гости могут поучаствовать в кулинарных мастер-классах для взрослых и детей. Главной особенностью коллекции Electrolux Intuit является ее функциональность, эргономичность и интуитивно понятный интерфейс с сенсорным управлением, который позволит поднять ежедневное приготовление еды на абсолютно новый уровень.\"}', '{\"en\": \"\", \"ru\": \"10:00 – 21:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, ул. Каменка, 2\"}', '8 (800) 444-44-48', 'aktivnaya-studiya-electrolux', 1200, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 44, '2021-04-13 16:03:50', NULL),
('1b99dd5a-3202-4013-ad7a-5a2d65f238f8', '{\"en\": \"\", \"ru\": \"Хаски Хутор\"}', '{\"en\": \"\", \"ru\": \"Здесь можно понаблюдать за жизнью дружелюбных хаски, прокатиться на нартах в собачьей упряжке, сделать фото и даже отправиться вместе с ними на обзорную экскурсию.\"}', '{\"en\": \"\", \"ru\": \"Хаски Хутор\"}', '{\"en\": \"\", \"ru\": \"Ежедневно вас ждут взрослые и юные представители северных ездовых собак, таинственные рассказы народов севера, легенды шамана, история происхождения и стандарты пород. Взрослые – 700 ₽ Дети – 500 ₽ Дети до 5 лет в сопровождении взрослых – бесплатно   В услугу входит:  Знакомство с представителями северных ездовых пород: сибирские хаски, аляскинские маламуты; Рассказ о стандартах пород, истории происхождение северных ездовых;  Фотографирование на свой гаджет; Трансфер из Горной Олимпийской деревни.\"}', '{\"en\": \"\", \"ru\": \"10:00 – 17:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Озеро Верхнее 1050 м\"}', '+7 (962) 887-81-21, +7 (938) 466-69-66', 'khaski-khutor', 500, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 58, '2021-04-13 17:15:10', NULL),
('1ba8995d-202f-4590-b610-5178c5d3ddc4', '{\"en\": \"\", \"ru\": \"Алкомаркет О, Вино!\"}', '{\"en\": \"\", \"ru\": \"Широкий выбор вин, алкогольных и безалкогольных напитков различных регионов мира.\"}', '{\"en\": \"\", \"ru\": \"Алкомаркет О, Вино!\"}', '{\"en\": \"\", \"ru\": \"Широкий выбор вин, алкогольных и безалкогольных напитков различных регионов мира. Лимитированные серии вин Кубани и Крыма.  Поможем подобрать подарок друзьям и родственникам.\"}', '{\"en\": \"\", \"ru\": \"11:00 — 22:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня, ул. Медовея, 8\"}', NULL, 'alkomarket-o-vino', 0, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 28, '2021-04-12 22:16:27', NULL),
('2282a6a3-a7cd-4cd6-a90c-163b2228b7aa', '{\"en\": \"\", \"ru\": \"Магазин Сладкий Рай\"}', '{\"en\": \"\", \"ru\": \"Магазин необычных сладостей со всего мира.\"}', '{\"en\": \"\", \"ru\": \"Магазин Сладкий Рай\"}', '{\"en\": \"\", \"ru\": \"Магазин необычных сладостей со всего мира. Мы порадуем вас восточными сладостями — рахат-лукум, нуга, пахлава, сухофрукты в орехах, халва всегда в большом ассортименте. Для маленьких покупателей мармелад, шоколад, леденцы, игрушки, и не просто, а с конфетами. Всегда большой выбор чая, кофе, варенья, меда.  У нас в магазине самая вкусная чурчхела и пастила, приготовленные по специальному рецепту. Наши сладости могут быть отличным подарком для ваших друзей.\"}', '{\"en\": \"\", \"ru\": \"10:30 — 22:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня, ул. Медовея 9/1\"}', '+7 (918) 107-01-01; +7 (918) 918-55-00', 'magazin-sladkiy-ray', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 29, '2021-04-12 22:19:40', NULL),
('22d4e555-4fff-49ce-920c-22d56d432d76', '{\"en\": \"\", \"ru\": \"Музей Археологии\"}', '{\"en\": \"\", \"ru\": \"Место, где история оживает\"}', '{\"en\": \"\", \"ru\": \"Музей Археологии\"}', '{\"en\": \"\", \"ru\": \"Новый интерактивный Музей Археологии на курорте Роза Хутор приглашает в удивительный мир истории. Как в машине времени вы совершите увлекательное путешествие в прошлое, оживающее с помощью самых современных технологий.\"}', '{\"en\": \"\", \"ru\": \"9:30 – 20:30\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, площадь Роза, 1\"}', '+7 928 854 9891', 'muzey-arheologii', 171, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 43, '2021-04-13 16:00:31', NULL),
('2860cee9-1d38-4bc6-a30d-d45e7ac3494b', '{\"en\": \"\", \"ru\": \"Ресторан TheAmsterdam\"}', '{\"en\": \"\", \"ru\": \"Семейный ресторан европейской кухни предлагает отправиться в гастрономическое путешествие по Европе!\"}', '{\"en\": \"\", \"ru\": \"Ресторан TheAmsterdam\"}', '{\"en\": \"\", \"ru\": \"Семейный ресторан европейской кухни предлагает отправиться в гастрономическое путешествие по Европе!  Ресторан TheAmsterdam отеля Tulip Inn Rosa Khutor предлагает гостям не только отведать блюда европейской и голландской кухни, но и насладиться обществом друзей и близких в уютной атмосфере.     Из арочных окон открывается вид на набережную реки Мзымта и хребет Аибга. Для гостей ресторана открыта терраса с прекрасным видом на реку и горные вершины, а расположенная рядом детская площадка станет центром притяжения для наших маленьких гостей.\"}', '{\"en\": \"\", \"ru\": \"7:00 – 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, набережная Панорама, 2, отель Tulip Inn Rosa Khutor 3*, 1-й этаж\"}', '+7 (862) 243-00-00', 'restoran-the-amsterdam', 700, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 20, '2021-04-12 18:38:10', NULL),
('28bc6481-673a-4261-99f8-f20ec6a13827', '{\"en\": \"\", \"ru\": \"SunShine Park\"}', '{\"en\": \"\", \"ru\": \"Открытый бассейн с подогревом, баня и гастробар на панорамной смотровой площадке\"}', '{\"en\": \"\", \"ru\": \"SunShine Park\"}', '{\"en\": \"\", \"ru\": \"Открытый бассейн с подогревом, баня и гастробар на панорамной смотровой площадке Новый SunShine Park – это парк развлечений в Горной Олимпийской деревне. Здесь можно отдохнуть душой и телом, проводить насыщенный день на закате или взбодриться утром перед горными развлечениями.  В парке есть круглогодичный подогреваемый бассейн с зоной лежаков под открытым небом, вместительная баня с панорамными окнами и видом на горы, гастробар, иглу-бар.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 22:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня 1100 м, площадь Виктория, 2\"}', '8 800 600 66 20', 'sunshine-park', 1000, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 42, '2021-04-13 15:55:11', NULL),
('2adfb632-a5b6-4fdc-9e27-1f2a14894f23', '{\"ru\": \"test place 3\"}', '{\"ru\": \"test description 3\"}', NULL, NULL, '{}', '{\"ru\": 1}', NULL, NULL, NULL, NULL, NULL, 1, 0, '{}', NULL, NULL, 1, 3, '2021-02-07 13:11:49', NULL),
('2b9a3c67-aa30-43e7-a81d-16b5845b8e9d', '{\"en\": \"\", \"ru\": \"Сплав на надувных каяках\"}', '{\"en\": \"\", \"ru\": \"В летнюю жару и солнечное межсезонье приглашаем вас сплавиться на надувных каяках по горной реке Мзымта.\"}', '{\"en\": \"\", \"ru\": \"Сплав на надувных каяках\"}', '{\"en\": \"\", \"ru\": \"В летнюю жару и солнечное межсезонье приглашаем вас сплавиться на надувных каяках по горной реке Мзымта.  Сплав проходит на одноместных и двухместных надувных каяках по реке Мзымта. Каждую группу сопровождает один или два инструктора, которые обеспечивают безопасность и помогают в сложных ситуациях. Каждый гость (от 6 лет) может принять участие в сплаве, специальная подготовка не требуются, достаточно прослушать вводный инструктаж! Отличительной особенностью сплава на курорте в сравнении с традиционным рафтингом является то, что гости самостоятельно управляют каяком, и это дарит незабываемые эмоции от единения со стихией!\"}', '{\"en\": \"\", \"ru\": \"Закрыто до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина (560 м), Rosa Beach\"}', '+7 (862) 277-70-93', 'splavy-na-naduvnykh-kayakakh', 2000, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 47, '2021-04-13 16:15:56', NULL),
('38a49948-7243-43b7-907c-3964a2b359b5', '{\"en\": \"\", \"ru\": \"Зона отдыха Rosa Beach\"}', '{\"en\": \"\", \"ru\": \"Настоящий пляжный оазис среди горных вершин.\"}', '{\"en\": \"\", \"ru\": \"Зона отдыха Rosa Beach\"}', '{\"en\": \"\", \"ru\": \"Rosa Beach – настоящий пляжный оазис среди горных вершин: озеро с горной водой, шезлонги, зонтики, детский бассейн. Бонус – прекрасный горный загар, который, как известно, держится намного дольше, чем морской!  Вход на пляж для всех гостей курорта бесплатный.\"}', '{\"en\": \"\", \"ru\": \"Закрыта до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, за отелем Radisson Rosa Khutor\"}', '8 800 5000 555', 'zona-otdykha-rosa-beach', 0, 1, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 9, '2021-04-12 17:03:36', NULL),
('3faab8d1-b684-46d3-a374-977afe420f64', '{\"en\": \"\", \"ru\": \"Modus Cafe\"}', '{\"en\": \"\", \"ru\": \"Уникальная домашняя атмосфера в интерьере, напоминающем высокогорное шале, позволит гостям расслабиться после катания и почувствовать себя как дома.\"}', '{\"en\": \"\", \"ru\": \"Modus Cafe\"}', '{\"en\": \"\", \"ru\": \"Пиццерия Модус – реплика одноименного московского проекта. Уникальная домашняя атмосфера в интерьере, напоминающем высокогорное шале, позволит гостям расслабиться после катания и почувствовать себя как дома. Настоящая ароматная пицца на тонком тесте и другие блюда итальянской кухни уже не первый год высоко оцениваются гостями курорта от самого итальянского посла до взыскательной московской публики. Секрет успеха – только отборные и натуральные ингредиенты при полном отсутствии химических усилителей и заменителей вкуса, а также любовь наших поваров к своему делу.\"}', '{\"en\": \"\", \"ru\": \"12:00 — 00:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, ул. Набережная Панорама, 3, отель Golden Tulip 3*\"}', '+7 (862) 243-98-80', 'picceriya-modus', 1500, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 25, '2021-04-12 22:04:49', NULL),
('40b460e0-048b-4dce-b5c4-003f1e6d1c43', '{\"en\": \"\", \"ru\": \"Гастрокомплекс Мясной Дом\"}', '{\"en\": \"\", \"ru\": \"Мультиформатный, многоэтажный, гастрономический комплекс, в создании которого активное участие принимал Аркадий Новиков.\"}', '{\"en\": \"\", \"ru\": \"Гастрокомплекс Мясной Дом\"}', '{\"en\": \"\", \"ru\": \"Мультиформатный, многоэтажный, гастрономический комплекс, в создании которого активное участие принимал Аркадий Новиков.  Дизайн двухэтажного помещения выполнен в стиле шале, для создания столов и стульев использовался 200-летний дуб. Интерьер разбавлен охотничьими трофеями, украшающими стены комплекса.   Много дерева, теплый свет, открытый огонь - все эти элементы располагают к теплым посиделкам и застольям в кругу друзей, коллег, соратников.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 22:00, 12:00 — 00:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, Набережная Лаванда, 6\"}', '+7 (938) 491-12-55', 'burgernaya', 1500, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 40, '2021-04-13 15:43:16', NULL),
('415519f4-4299-40ab-8c11-e3f44e7471af', '{\"en\": \"\", \"ru\": \"Йети Кафе\"}', '{\"en\": \"\", \"ru\": \"Уютный дом в горах с панорамным видом, разнообразное меню пиццы, настоящий плов на костре и десерты для маленьких и больших снежных людей.\"}', '{\"en\": \"\", \"ru\": \"Йети Кафе\"}', '{\"en\": \"\", \"ru\": \"Уютный дом в горах с панорамным видом, разнообразное меню пиццы, настоящий плов на костре и десерты для маленьких и больших снежных людей.  Легендарное кафе, которое уже полюбилось большому количеству больших и маленьких гостей.   Это не просто комфортный дом в горах с панорамным видом, а тематическое кафе. Здесь каждая деталь напоминает о существовании самого таинственного и загадочного в мире существа — снежного человека. В честь него было названо это уютное место.   Кафе удачно совместило в себе по-домашнему вкусную и здоровую пищу по доступной цене, а также быстрое и доброжелательное обслуживание.\"}', '{\"en\": \"\", \"ru\": \"9:30 – 16:30\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Трасса Б-52, верхняя станция канатной дороги Беседа\"}', '+7 (928) 448-88-07', 'yeti-kafe', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 13, '2021-04-12 18:07:24', NULL),
('43451430-0597-4601-9945-1220577b0340', '{\"en\": \"Swing above the clouds\", \"ru\": \"Качели над облаками\"}', '{\"en\": \"Experience the thrill of the highest swing in Russia\", \"ru\": \"Испытайте острые ощущения на самых высокогорных качелях в России\"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, 'swing', 60000, 0, 1, '[]', 4, 'ab89a4a8-5c96-4589-9161-735f6c0f687e', 1, 3, '2021-02-10 15:44:39', NULL),
('43c98936-ac92-4fb8-99e8-d9965ae31708', '{\"en\": \"\", \"ru\": \"Центр здоровья Rosa Springs\"}', '{\"en\": \"\", \"ru\": \"Совместите отдых в горах с пользой для своего здоровья!\"}', '{\"en\": \"\", \"ru\": \"Центр здоровья Rosa Springs\"}', '{\"en\": \"\", \"ru\": \"Центр Здоровья Rosa Springs создан в 2016 году и является самым современным в горном кластере Сочи. Услуги медицинского центра могут стать полезным дополнением к программе отдыха в горах. Вы можете совместить пребывание на Роза Хутор с курсом бальнеологических процедур или массажа, пройти комплексную диагностику check up или полную лечебно-оздоровительную программу по назначению врача.\"}', '{\"en\": \"\", \"ru\": \"8:00 — 20:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня, ул. Медовея, д. 4, отель Rosa Springs 4*\"}', '+7 (938) 400-54-63', 'tsentr-zdorovya-rosa-springs', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 56, '2021-04-13 17:08:37', NULL),
('477aa6ab-ed63-4a4f-a7f4-4ad8a7affbfd', '{\"en\": \"\", \"ru\": \"Юрьев Хутор\"}', '{\"en\": \"\", \"ru\": \"Здесь даже летом есть снег, сюда приходят дикие кавказские серны, а слышно здесь только ветер.​ 7,7 км.\"}', '{\"en\": \"\", \"ru\": \"Юрьев Хутор\"}', '{\"en\": \"\", \"ru\": \"Конечный пункт похода по маршруту Юрьев Хутор – одноименный высокогорный цирк, уединенное урочище в окружении скальных отрогов горы Каменный Столб.    Пеший маршрут берет начало на высоте 1350 метров над уровнем моря и пролегает вдоль северного склона хребта Аибга.   Следуя на юго-восток по траверсу с плавным набором высоты, туристы насладятся грандиозными видами на вершины Главного Кавказского хребта, возвышающиеся над облаками на противоположной стороне долины реки Мзымта.    Поднявшись на высоту 1930 метров путешественники вступят в заповедные пределы цирка Юрьев Хутор, где до конца лета сочатся талой водой снежники, тишину нарушают лишь ветер или сорвавшийся с кручи камень. Здесь более частыми, чем люди, гостями являются кавказские серны.   Чтобы перемещение по маршруту было комфортным, правильно подберите снаряжение: понадобятся ветровка или дождевик на случай дождя, удобная одежда, обувь на крепкой подошве, питьевая вода и средства защиты от солнца.\"}', '{\"en\": \"\", \"ru\": \"Закрыто до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Беседка (1350 м), пересадочный узел между подъемниками Заповедный Лес, Кавказский Экспресс и Волчья Скала.\"}', '+7 (938) 888-02-14', 'yurev-khutor', 1000, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 38, '2021-04-13 15:29:02', NULL),
('5572c6e6-ef32-43ef-b4f8-4f0d93ac5f4b', '{\"en\": \"\", \"ru\": \"Салон красоты ROSA ET MARI\"}', '{\"en\": \"\", \"ru\": \"Парикмахерский зал, маникюр и педикюр, косметология, а также зона Spa и релакса (все виды массажа и ухода для тела).\"}', '{\"en\": \"\", \"ru\": \"Салон красоты ROSA ET MARI\"}', '{\"en\": \"\", \"ru\": \"Парикмахерский зал, маникюр и педикюр, косметология, а также зона Spa и релакса (все виды массажа и ухода для тела).  ROSA ET MARI — ваш собственный будуар, царство неги и покоя, где вы раскроете свою индивидуальность через красоту!\"}', '{\"en\": \"\", \"ru\": \"10:00 — 22:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, Набережная Лаванда, 5 PARK INN by Radisson\"}', '+7 928 242 42 52', 'byuti-salon-rosa-et-mari', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 52, '2021-04-13 16:54:32', NULL),
('59d83bb0-2dba-4e5f-8095-3360fac60db7', '{\"en\": \"Ascent to the observation deck 2320 m\", \"ru\": \"Подъем на смотровую площадку 2320 м\"}', '{\"en\": \"Take the cable cars above the clouds and enjoy the panorama of the winter Caucasus Mountains from the observation deck at an altitude of 2320 meters!\", \"ru\": \"Поднимитесь на канатных дорогах выше облаков и насладитесь панорамой зимних Кавказских гор со смотровой площадки на высоте 2320 метров!\"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, 'observation-deck-2320', 30000, 0, 1, '{\"en\": \"Choose ticket\", \"ru\": \"Выбрать билет\"}', 2, 'b00b96a7-3676-4dd6-8e92-318034f9125f', 1, 3, '2021-02-10 15:44:38', NULL),
('5d0f070a-a69d-4a53-8947-3e04af20dd9e', '{\"en\": \"\", \"ru\": \"Детский клуб\"}', '{\"en\": \"\", \"ru\": \"Детский клуб на Роза Хутор – территория детства, где царит атмосфера творчества, радости и знаний.\"}', '{\"en\": \"\", \"ru\": \"Детский клуб\"}', '{\"en\": \"\", \"ru\": \"Детский клуб на Роза Хутор – территория детства, где царит атмосфера творчества, радости и знаний.  Программа работы детского клуба разнообразна и понравится без исключения каждому ребенку. Развлечений хватит на целый день: комната сказок, развивающая среда, веселая игровая, творческие мастерские, развивающие занятия и мини-шоу. Мы ценим самое важное в детях – их личность, богатый красками и интересами внутренний мир.  Приводите к нам малыша на час, два или на целый день. Для удобства гостей создана система абонементов длительного пребывания.  В детский клуб принимаются дети в возрасте от 1 до 14 лет. Дети до 3 лет принимаются только в сопровождении взрослого (родители или предоставляется персональная няня).\"}', '{\"en\": \"\", \"ru\": \"Временно закрыт\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня, ул. Медовея, 10\"}', '+7 (928) 234-65-15', 'detskiy-klub', 550, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 50, '2021-04-13 16:42:33', NULL),
('63220762-afa3-42c3-99d7-c91e625d7898', '{\"en\": \"\", \"ru\": \"Бар «Мамонт»\"}', '{\"en\": \"\", \"ru\": \"Мамонт - место с гастрономической идеей, актуальным авторским баром и яркими событиями.\"}', '{\"en\": \"\", \"ru\": \"Бар «Мамонт»\"}', '{\"en\": \"\", \"ru\": \"Мамонт - место с гастрономической идеей, актуальным авторским баром и яркими событиями.  Расположившись в самом центре олимпийской деревни на высоте 1100м над уровнем моря, Мамонт – прямо с порога погружает гостей в атмосферу домашнего лофта: мягкие диваны, большое количество деталей интерьера, тёплый ламповый свет - к такому уюту сложно остаться равнодушным.  Гастрономическое предложение завязано на оригинальных подачах с сохранением традиций домашней кухни. Важную роль играют события, проходящие на нашей сцене.\"}', '{\"en\": \"\", \"ru\": \"с 12:00 до 0:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная олимпийская деревня, ул. Медовея, 6, отель Riders Lodge\"}', '+7 (938) 455-88-77', 'bar-mamont', 1000, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 17, '2021-04-12 18:23:56', NULL),
('644e272b-48b9-4f43-9bf2-86beb467b100', '{\"en\": \"\", \"ru\": \"X-STORE\"}', '{\"en\": \"\", \"ru\": \"Одежда в сегменте fashion premium sport.\"}', '{\"en\": \"\", \"ru\": \"X-STORE\"}', '{\"en\": \"\", \"ru\": \"Одежда в сегменте fashion premium sport. Мультибрендовый магазин премиум класса, объединивший в себе одежду и обувь итальянских, немецких, швейцарских, канадских и аргентинских брендов.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 22:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, набережная Панорама, д. 3\"}', '8 (928) 458-35-00', 'x-store', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 11, '2021-04-12 17:57:21', NULL),
('664d2ec9-5f8c-4509-bb7b-0dacd54a0c42', '{\"ru\": \"test place\"}', '{\"ru\": \"test description\"}', NULL, NULL, '{}', '{\"ru\": 1}', NULL, NULL, NULL, NULL, NULL, 1, 1, '{}', NULL, NULL, 1, 1, '2021-02-07 13:11:49', NULL),
('67e8dff5-2a09-4e31-bafc-f6831e666686', '{\"en\": \"\", \"ru\": \"Кофейня Surf Coffee™ 560 м\"}', '{\"en\": \"\", \"ru\": \"Неповторимая атмосфера уюта посредством высочайшего качества продукта и дружелюбной заботы о госте.\"}', '{\"en\": \"\", \"ru\": \"Кофейня Surf Coffee™ 560 м\"}', '{\"en\": \"\", \"ru\": \"Неповторимая атмосфера уюта посредством высочайшего качества продукта и дружелюбной заботы о госте.  Только лучшие зеленые зерна c различных уголков мира смогут стать Surf Coffee Espresso blend. Процесс по-настоящему завораживает и становится магическим, когда представляешь эволюцию маленького зеленого зерна, собранного в тропическом климате другого континента. Surf Coffee™ доставляет все те природные качества зерна и заботу фермеров до вашей чашки кофе, не нарушив ни один из процессов приготовления.  Основу меню составляют напитки, приготовленные на основе эспрессо. Возможны вариации: двойной эспрессо (доппио), американо, ристретто.  Умея делать абсолютно правильную молочную пенку — сладкую, глянцевую — бариста готовит капучино и латте. Добавляя различные сиропы, посыпки, украшая напитки взбитыми сливками, бариста создает уникальные фирменные напитки Surf special: двойной гавайский капучино, имбирно-пряничный латте, мятный мокко.\"}', '{\"en\": \"\", \"ru\": \"7:30 — 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, ул. Каменка, 1\"}', '+7 (965) 469-02-42', 'kofeynya-surf-coffee-560-m', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 32, '2021-04-12 22:30:23', NULL),
('7448f9cb-7a06-4f05-acd0-ca4b4b00cad5', '{\"en\": \"\", \"ru\": \"Ресторан «Вельвет»\"}', '{\"en\": \"\", \"ru\": \"Ресторан в уникальном комплексе класса Premium Роза Шале на высоте 1170 м над уровнем моря.\"}', '{\"en\": \"\", \"ru\": \"Ресторан «Вельвет»\"}', '{\"en\": \"\", \"ru\": \"Новое место рядом с Роза Шале для ценителей природы, комфорта и великолепной кухни.  Ресторан расположен на территории комплекса Роза Шале. Вельвет — уютное заведение в стиле альпийского шале с традиционными деревянными балками и стенами.   В основе гастрономической философии шеф-повара лежат рецепты самых популярных кухонь мира: средиземноморской, европейской, азиатской и фьюжн.\"}', '{\"en\": \"\", \"ru\": \"11:00 — 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня (1100 м), ул. Пихтовая аллея, д.7\"}', '+7 938 888-95-91', 'restoran-velvet', 1500, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 18, '2021-04-12 18:29:01', NULL),
('751db2d0-0bc6-472e-ba7e-29556c3e9e80', '{\"en\": \"Rodelbahn\", \"ru\": \"Родельбан\"}', '{\"en\": \"The favorite summer entertainment of the guests of the Alpine resorts is now on Rosa Khutor! But our rodelbahn is special - it offers panoramic views, it is year-round and super modern!\", \"ru\": \"Любимое летнее развлечение гостей альпийских курортов теперь есть и на Роза Хутор! Но наш родельбан особенный – с него открываются панорамные виды, он круглогодичный и супер современный!\"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, 'rodelbahn', 40000, 0, 0, '{\"en\": \"Choose ticket\", \"ru\": \"Выбрать билет\"}', 2, 'c388d324-2959-48b3-ac10-a4f50b475f46', 1, 3, '2021-02-10 15:44:38', NULL),
('757093e9-afd7-4fac-ad69-a6217872823e', '{\"en\": \"\", \"ru\": \"Тропа здоровья\"}', '{\"en\": \"\", \"ru\": \"Маршрут проходит в лесной зоне долины реки Мзымта, где деревья-великаны, мхи и лианы. 4,4 км\"}', '{\"en\": \"\", \"ru\": \"Тропа здоровья\"}', '{\"en\": \"\", \"ru\": \"Тропа представляет из себя три кольца разного уровня сложности и протяженности с набором высоты от 580 до 732 метров над уровнем моря.    Маршрут насчитывает 919 ступеней, пересекает 13 горных ручьев, 2 водопада.   Малое кольцо Тропы здоровья позволяет совершить прогулку под сенью колхидского леса с легкой физической нагрузкой. Путешественников ждут увитые лианами вековые дубы, клены, ясени, каштаны и тополи, вечнозеленые кустарники, папоротники нескольких видов, поблескивающая сквозь заросли горная река и полчаса покоя в окружении древнего горного леса.   Среднее кольцо Тропы здоровья ведет путешественников в глубину колхидского леса к горным ручьям, живописным скальным обнажениям и древним каменным обвалам. Преодоление маршрута потребует умеренных физических усилий, потратив которые туристы проведут час-другой в тихом потаенном уголке долины реки Мзымта и проверят свою готовность к выходу на более сложные и продолжительные горные маршруты.    Большое кольцо Тропы здоровья позволит проверить свои силы, а в качестве награды предоставит возможность познакомиться со всеми проявлениями низкогорного ландшафта – лесистыми склонами разной крутизны, скальными обнажениями, каменными завалами, глубокими распадками и гремящими в них ручьями. Туристов ждет разнообразие заселяющих колхидский лес древесных, кустарниковых и травянистых растений, а особо осторожных и внимательных – наблюдение за птицами и следы диких зверей.\"}', '{\"en\": \"\", \"ru\": \"с 10:00 до 16:45\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, за отелем Radisson Rosa Khutor\"}', '+7 (938) 888-02-14', 'tropa-zdorovya', 300, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 33, '2021-04-12 23:13:01', NULL),
('7a42dd66-a30e-4f4d-b2c2-84031471dda1', '{\"en\": \"\", \"ru\": \"Кафе Чё? Харчо!\"}', '{\"en\": \"\", \"ru\": \"Щедрые угощения, которые приятно разделить с друзьями, и десерты, которые не захочется делить ни с кем.\"}', '{\"en\": \"\", \"ru\": \"Кафе Чё? Харчо!\"}', '{\"en\": \"\", \"ru\": \"Щедрые угощения, которые приятно разделить с друзьями, и десерты, которые не захочется делить ни с кем. В меню Чё? Харчо! каждый найдет свои любимые блюда. Домашний форшмак, борщ и румяные куриные котлетки. Традиционные для Кавказа хинкали, полные вкуснейшего бульона, сочные люля и шашлык.\"}', '{\"en\": \"\", \"ru\": \"11:00 — 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, площадь Мзымты, 1, здание Мзымта Двор, второй этаж\"}', '+7 (862) 243-97-18', 'kafe-chjoharcho', 1500, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 39, '2021-04-13 15:39:40', NULL),
('7f7b627d-114a-4c20-974d-ae58345ca338', '{\"ru\": \"test place 2\"}', '{\"ru\": \"test description 2\"}', NULL, NULL, '{}', '{\"ru\": 1}', NULL, NULL, NULL, NULL, NULL, 0, 0, '{}', NULL, NULL, 1, 1, '2021-02-07 13:11:49', NULL),
('8ce04491-a8db-44a9-a536-5c87778be5f9', '{\"en\": \"\", \"ru\": \"Сувенирный магазин Роза Хутор\"}', '{\"en\": \"\", \"ru\": \"Фирменные аксессуары и подарки порадуют гостей курорта, которые хотят увезти с собой приятные воспоминания об отдыхе на курорте Роза Хутор.\"}', '{\"en\": \"\", \"ru\": \"Сувенирный магазин Роза Хутор\"}', '{\"en\": \"\", \"ru\": \"Фирменные аксессуары и подарки порадуют гостей курорта, которые хотят увезти с собой приятные воспоминания об отдыхе на курорте Роза Хутор.  Здесь вы найдете функциональные подарки, эксклюзивную линейку натуральной косметики, коллекцию одежды собственного производства и множество других эксклюзивных сувениров, при создании которых авторы вдохновлялись вершинами наших гор.    Магниты, кружки, брелоки, футболки, сумки, браслеты с логотипами Роза Хутор точно захочется потрогать и примерить или приобрести себе на память! Эксклюзивная гамма натуральной косметики станет лучшим подарком, который вы привезете из поездки.  Порадуйте себя и близких людей ценными подарками, привезенными с отдыха на курорте Роза Хутор!\"}', '{\"en\": \"\", \"ru\": \"Роза Приют 9:30 – 18:30, Роза Пик 9:00 – 16:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня 1100 м, ул. Олимпийская, 37/1, в здании Роза Приют, Роза Пик 2320 м, в здании ресторана «Высота 2320»\"}', '8 (800) 500-05-55', 'magazin-suvenirov', 100, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 1, '2021-04-12 15:51:03', NULL),
('8f88cdf2-d596-46ff-8595-efb8b012e788', '{\"en\": \"\", \"ru\": \"Горная мыловарня Краснополянская Косметика\"}', '{\"en\": \"\", \"ru\": \"Подарки, созданные с любовью и заботой о вашей красоте и здоровье на основе уникальных компонентов местной природы\"}', '{\"en\": \"\", \"ru\": \"Горная мыловарня Краснополянская Косметика\"}', '{\"en\": \"\", \"ru\": \"Подарки, созданные с любовью и заботой о вашей красоте и здоровье на основе уникальных компонентов местной природы. Вот уже более десяти лет команда Краснополянского в условиях экологически чистого высокогорья изготавливает полностью натуральные продукты для ухода за волосами, кожей лица и тела.  Сегодня линейка продукции состоит из органического мыла, шампуней и бальзамов для волос, кремов для лица, бальзамов для губ, а также дезодорантов. В продаже имеется состоящее полностью из природных компонентов натуральное средство для мытья посуды.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 19:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная олимпийская деревня, ул. Медовея, д. 9\"}', '+7 (965) 473-74-64', 'gornaya-mylovarnya-krasnopolyanskaya-kosmetika', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 3, '2021-04-12 16:38:17', NULL),
('926e960c-0d5b-4796-a58a-0c5d6968ed0f', '{\"en\": \"Interactive park-museum \\\"My Russia\\\"\", \"ru\": \"Интерактивный парк-музей «Моя Россия»\"}', '{\"en\": \"A unique architectural ensemble built taking into account the centuries-old traditions of housing construction in various regions of Russia.\", \"ru\": \"Уникальный архитектурный ансамбль, возведенный с учетом многовековых традиций домостроения различных регионов России. \"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, 'museum', 40000, 0, 0, '{\"en\": \"Choose ticket\", \"ru\": \"Выбрать билет\"}', 2, '7af3a9e8-cfe2-48a2-ba68-b305d6c73850', 1, 3, '2021-02-10 15:44:38', NULL),
('92e65dd4-2769-4f4f-87b7-df5cab58211c', '{\"en\": \"\", \"ru\": \"Ресторан SIMACH\"}', '{\"en\": \"\", \"ru\": \"Simach – это авторский проект с lounge-зоной и танцполом на первом и рестораном на втором этаже. Посидеть за баром можно на обоих уровнях.\"}', '{\"en\": \"\", \"ru\": \"Ресторан SIMACH\"}', '{\"en\": \"\", \"ru\": \"Simach на Роза Хутор – совместный проект Дениса Симачева и Антона Пинского. За кухню здесь отвечают сразу три шефа: Павел Гроцкий (шеф-повар Сыроварни на Красном Октябре), Михаил Кучма (шеф-повар Сыроварни в Санкт-Петербурге) и Стас Иванов (ставил меню в Avocado Queen в Бодруме). Меню состоит из классических японских позиций, традиционной итальянской гастрономии и авторских блюд.\"}', '{\"en\": \"\", \"ru\": \"9:00 – 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, пл. Роза, 1\"}', '+7 (862) 445-56-55', 'simach', 2500, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 16, '2021-04-12 18:15:52', NULL),
('93932b07-29a8-415e-b559-919942771980', '{\"en\": \"Helicopter rides\", \"ru\": \"Вертолетные прогулки\"}', '{\"en\": \"Get a bird\'s eye view of the amazing mountain landscape.\", \"ru\": \"Взгляните на удивительную горную природу с высоты птичьего полёта.\"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, NULL, 1500000, 0, 1, '[]', 4, '751651bb-240c-4a75-a413-926a5583a42a', 1, 3, '2021-02-10 15:44:39', NULL),
('94289317-68dc-4ad7-aa09-d77509090711', '{\"en\": \"\", \"ru\": \"SPA и Beauty-salon Rosa Springs\"}', '{\"en\": \"\", \"ru\": \"SPA-салон с классическими и эксклюзивными услугами.\"}', '{\"en\": \"\", \"ru\": \"SPA и Beauty-salon Rosa Springs\"}', '{\"en\": \"\", \"ru\": \"Мини-посольство красоты в Горной Олимпийской деревне. SPA-салон с классическими и эксклюзивными услугами. Новейшие аппаратные методики, профессиональная косметика, заботливые мастера. Процедуры моментального омоложения, уход за лицом и телом, коррекция фигуры.  Beauty-cалон в отеле Rosa Springs расположен на третьем этаже недалеко от зоны reception. Здесь предлагают лучшую профессиональную косметику и оборудование, созданные по медицинским стандартам. В списке услуг — косметические и спа-процедуры, профессиональные уходы за лицом и телом, аппаратные методики, комплексные спа-программы и локальные процедуры, услуги визажиста, стилиста и ногтевой сервис. В Beauty-салоне работают профессиональные консультанты, специалисты по стилю и имиджу. Некоторые процедуры представлены в Beauty-cалоне Rosa Springs эксклюзивно для Красной Поляны и Краснодарского края.  Настоящая красота — это не только внешний вид. Поэтому мы не украшаем — мы помогаем меняться! Красота – это искусство, которое рождается из заботы и любви, и мы владеем этим искусством в совершенстве.\"}', '{\"en\": \"\", \"ru\": \"8:00 — 20:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня, ул.Медовея, 4, отель Rosa Springs 4*\"}', '+7 (938) 400-54-66', 'beauty-salon-rosa-springs', 0, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 51, '2021-04-13 16:51:40', NULL),
('a43db35a-26ac-4230-8c1e-12f2326efa72', '{\"en\": \"\", \"ru\": \"Ресторан Red Fox\"}', '{\"en\": \"\", \"ru\": \"Фешенебельный ресторан, который занимает 15-е место в рейтинге лучших ресторанов мира The World\'s 50 Best Restaurants.\"}', '{\"en\": \"\", \"ru\": \"Ресторан Red Fox\"}', '{\"en\": \"\", \"ru\": \"Red Fox (Роза Хутор) – фешенебельный ресторан с авторской кухней Владимира Мухина (шеф-повар ресторана White Rabbit, который занимает 15-е место в рейтинге лучших ресторанов мира The World\'s 50 Best Restaurants). К услугам гостей уютный зал с камином, аквариум с живыми морепродуктами и богатая коллекция вин. Есть также отдельный зал-караоке.\"}', '{\"en\": \"\", \"ru\": \"12:00 — 00:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, ул. Набережная Лаванда, 3, отдельно стоящее здание рядом с отелем Mercure Rosa Khutor 4*\"}', '+7 (862) 274-44-44', 'restoran-red-fox', 3500, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 15, '2021-04-12 18:21:04', NULL),
('a4786183-4442-45e5-b59c-57036d1589ca', '{\"en\": \"\", \"ru\": \"Bogner Boutique\"}', '{\"en\": \"\", \"ru\": \"Bogner — бренд №1 в индустрии одежды и аксессуаров для горнолыжного спорта и отдыха.\"}', '{\"en\": \"\", \"ru\": \"Bogner Boutique\"}', '{\"en\": \"\", \"ru\": \"Bogner — бренд №1 в индустрии одежды и аксессуаров для горнолыжного спорта и отдыха. Мировой бренд из Баварии! Bogner — немецкий бренд, за плечами которого 80 лет работы, оригинально сочетает в своих коллекциях спортивный стиль и высокую моду. Во всем мире Bogner является лидером рынка лыжной моды высшего качества. Сегодня, как и десятилетия назад, ему отдают предпочтение богатые, знаменитые и активные люди!\"}', '{\"en\": \"\", \"ru\": \"10:00 — 22:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, Набережная Панорама, 3\"}', '+7 (928) 458-35-00', 'bogner-boutique', 0, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 8, '2021-04-12 17:41:59', NULL),
('a5c8b390-3e28-4879-95d6-31e03658caa8', '{\"en\": \"\", \"ru\": \"Детский клуб Ли-Ли Монте\"}', '{\"en\": \"\", \"ru\": \"Территория детства, где царит атмосфера творчества и знаний, радости и гармонии.\"}', '{\"en\": \"\", \"ru\": \"Детский клуб Ли-Ли Монте\"}', '{\"en\": \"\", \"ru\": \"Территория детства, где царит атмосфера творчества и знаний, радости и гармонии. Детский клуб Ли-Ли Монте расположен в здании отеля Tulip Inn 3*.\"}', '{\"en\": \"\", \"ru\": \"9:00 – 19:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, наб. Панорама, 2, отель Tulip Inn, 1 этаж\"}', '+7 (928) 449-89-00', 'detskiy-klub-li-li-monte', 300, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 48, '2021-04-13 16:29:09', NULL),
('adf02960-c923-4b5a-bb8a-04feb6509255', '{\"en\": \"Toboggan - Alpine Sled\", \"ru\": \"Тобогган – Альпийские сани\"}', '{\"en\": \"The toboggan is the oldest type of sleigh, which you can ride in the evening along the modern Rosa Khutor track.\", \"ru\": \"Тобогган — это древнейший вид саней, на которых вечером можно прокатиться по современной трассе «Роза Хутор».\"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, 'toboggan', 40000, 0, 1, '[]', 2, '0c8f2278-6cf9-47f7-9561-3f77701e472a', 1, 3, '2021-02-10 15:44:40', NULL),
('b102c571-aae8-449b-a74a-09d583776dcb', '{\"en\": \"\", \"ru\": \"Детская Горная Академия\"}', '{\"en\": \"\", \"ru\": \"Мир увлекательных занятий для ваших детей в красивейшем уголке горной природы.\"}', '{\"en\": \"\", \"ru\": \"Детская Горная Академия\"}', '{\"en\": \"\", \"ru\": \"Детская Горная Академия — это увлекательные интерактивные занятия для детей про мир гор, обучение секретам горных туристов, увлекательные рассказы о растениях и животных, познавательные лаборатории по изучению природы и даже вечерние слеты.\"}', '{\"en\": \"\", \"ru\": \"Закрыто до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Центр Активный отдых — площадь Мзымта, под Романовым мостом\"}', '+7 938 888 02 14', 'detskaya-gornaya-akademiya', 550, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 36, '2021-04-13 15:17:30', NULL),
('be2b9c7e-342a-424c-9324-ff7e701d45fd', '{\"en\": \"\", \"ru\": \"Ресторан Высота 2320\"}', '{\"en\": \"\", \"ru\": \"Ресторан Высота 2320 — вкус на высоте во всех смыслах.\"}', '{\"en\": \"\", \"ru\": \"Ресторан Высота 2320\"}', '{\"en\": \"\", \"ru\": \"Ресторан на пике курорта Роза Хутор. Из панорамных окон открывается величественный вид на Главный Кавказский хребет, гору Аибга и, конечно же, на Черное море. Сидя на деревянной веранде ресторана, вдыхая чистый горный воздух, вы, несомненно, прочувствуете настоящий вкус жизни. «Вкус на высоте во всех смыслах!» Ресторан Высота 2320 располагается на вершине Роза Хутор. И уже практически невозможно представить себе этот курорт без нашего заведения.   Из панорамных окон открывается величественный вид на Главный Кавказский хребет, гору Аибга и, конечно же, на Черное море.\"}', '{\"en\": \"\", \"ru\": \"9:30 — 17:15\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Высота Роза Пик (2320 м), верхняя станция подъемника Кавказский экспресс\"}', '+7 (862) 241-92-40', 'vysota-2320', 1800, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 21, '2021-04-12 21:34:35', NULL);
INSERT INTO `place` (`id`, `title`, `description`, `sub_title`, `sub_description`, `working_hours`, `age_restriction`, `label`, `address`, `phone`, `code`, `min_price`, `is_free`, `is_cable_way_required`, `button_text`, `width`, `background_id`, `is_active`, `sort`, `created_at`, `updated_at`) VALUES
('beb40e3e-a71c-4204-b453-da4487213e2c', '{\"en\": \"\", \"ru\": \"​Кафе Лес\"}', '{\"en\": \"\", \"ru\": \"Кафе «Лес» расположено в одном из живописных мест курорта «Роза Хутор» – горном пихтовом лесу, в окружении вековых деревьев и парка водопадов Менделиха.\"}', '{\"en\": \"\", \"ru\": \"​Кафе Лес\"}', '{\"en\": \"\", \"ru\": \"Кафе Лес расположено в одном из живописных мест курорта Роза Хутор – горном пихтовом лесу, в окружении вековых деревьев и парка водопадов Менделиха. Название кафе говорит само за себя – тихое зеленое место посреди величественных гор, вдали от городского кластера, курортной зоны и суматохи, наполненное атмосферой спокойствия и уединения с природой. Интерьер ресторана выполнен из местных пород дерева с добавлением дизайнерских деталей, что придает заведению домашний и при этом современный вид, поэтому отдыхать здесь особенно легко и приятно.\"}', '{\"en\": \"\", \"ru\": \"10:00 – 16:30\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Хутор, Южный склон, нижняя станция канатной дороги Эдельвейс\"}', '+7 (928) 233-42-83', 'cafe-les', 650, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 14, '2021-04-12 18:10:58', NULL),
('bec0804d-bbd9-4fc2-bfad-af11c14b5e84', '{\"en\": \"Sunset at the Peak\", \"ru\": \"Закат на Пике\"}', '{\"en\": \"Watch the sunset from 2,320 meters\", \"ru\": \"Полюбоваться закатом с высоты 2320 метров.\"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, 'sunset', NULL, 1, 1, '[]', 3, 'f75079c6-d214-497d-be6f-3c96fe6d6ece', 1, 3, '2021-02-10 15:44:39', NULL),
('beebbd41-6f7c-4bea-af34-915b077fcc88', '{\"en\": \"\", \"ru\": \"Буфет и Бар Берлога\"}', '{\"en\": \"\", \"ru\": \"Буфет, известный как заведение со вкусной домашней кухней, баром, с богатым выбором вок-лапши и мангалом.\"}', '{\"en\": \"\", \"ru\": \"Буфет и Бар Берлога\"}', '{\"en\": \"\", \"ru\": \"Буфет, известный как заведение со вкусной домашней кухней, баром, с богатым выбором вок-лапши и мангалом.  Здесь вы всегда сможете полноценно пообедать, завести новые знакомства и приятно провести время с видом на горы.  А удобное местоположение и демократические цены сделают отдых ещё приятнее.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 19:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня 1100 м, здание Роза Приют, 1-й этаж\"}', '+7 (928) 233-30-22', 'restoran-berloga', 850, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 12, '2021-04-12 18:01:58', NULL),
('c16b03f7-fd95-4a9c-b77a-7be1f5da4892', '{\"en\": \"\", \"ru\": \"Корпорация шалостей\"}', '{\"en\": \"\", \"ru\": \"Здесь можно делать то, что запрещено дома и в школе, но полезно для развития!\"}', '{\"en\": \"\", \"ru\": \"Корпорация шалостей\"}', '{\"en\": \"\", \"ru\": \"Здесь можно делать то, что запрещено дома и в школе, но полезно для развития!  Да здравствует Шалость – путь, который находит мудрый ребёнок, чтобы чувствовать себя свободным и развиваться, при этом не идти на конфликт со взрослыми.\"}', '{\"en\": \"\", \"ru\": \"Временно закрыто\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, наб. Лаванда, 2\"}', '+7 (938) 464-38-08', 'korporatsiya-shalostey', 450, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 45, '2021-04-13 16:08:29', NULL),
('c2246051-7a14-4fc5-a109-8948a64a286f', '{\"en\": \"Park of waterfalls \\\"Mendelikha\\\"\", \"ru\": \"Парк водопадов «Менделиха»\"}', '{\"en\": \"The Mendelikha Waterfalls Park is located on the southern slope of the Aibga ridge at an altitude of 1470 m.\\\\n\\\\nIt got its name from the river, in the bed of which the picturesque cascades are located.\\\\n\\\\nAnd the river was named after the Mendele family, who lived on its bank since the second half of the 19th century.\", \"ru\": \"Парк водопадов «Менделиха» находится на южном склоне хребта Аибга на высоте 1470 м.\\\\n\\\\nОн получил свое имя от реки, в русле которой и расположены живописные каскады.\\\\n\\\\nА река была названа в честь семьи Менделей, которые проживали на ее берегу со второй половины XIX века.\"}', NULL, NULL, '[]', '{\"ru\": 1}', '{\"type\": \"discount\", \"color\": \"49cb43\", \"content\": {\"en\": \"Up to 15% discount\", \"ru\": \"Скидка до 15%\"}}', NULL, NULL, 'mendelikha', 30000, 0, 1, '{\"en\": \"Choose ticket\", \"ru\": \"Выбрать билет\"}', 2, 'd298bd24-a164-481b-aaef-cfc2c277939c', 1, 3, '2021-02-10 15:44:38', NULL),
('c2da5654-ed15-4888-a269-d5d94e6e89eb', '{\"en\": \"Dawn on the track\", \"ru\": \"Рассвет на трассе\"}', '{\"en\": \"Discover the corduroy slopes first!\", \"ru\": \"Откройте первым вельвет склонов!\"}', NULL, NULL, '[]', '{\"ru\": 1}', NULL, NULL, NULL, 'dawn', 1000000, 0, 1, '[]', 3, 'b1680aab-18b7-444a-b4d0-e830771e0883', 1, 3, '2021-02-10 15:44:39', NULL),
('c325bd88-9ef5-4616-9029-24f52b5e1f7a', '{\"en\": \"\", \"ru\": \"Харгинский лес\"}', '{\"en\": \"\", \"ru\": \"Таинственный реликтовый лес, заросли папоротника, необъятные буки, зрелищный водопад. 14 км.\"}', '{\"en\": \"\", \"ru\": \"Харгинский лес\"}', '{\"en\": \"\", \"ru\": \"Путешествие в заповедный Харгинский лес, раскинувшийся на восточной окраине курорта Роза Хутор, начинается от Горной Олимпийской деревни.\"}', '{\"en\": \"\", \"ru\": \"Закрыто до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня\"}', '+7 (938) 888-02-14', 'kharginskiy-les', 1000, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 37, '2021-04-13 15:24:35', NULL),
('c8382d8b-d433-4b31-a2e2-332cd8b92f99', '{\"en\": \"\", \"ru\": \"Детский клуб Riders\"}', '{\"en\": \"\", \"ru\": \"Свободное пребывание и программы для детей от 3 до 15 лет.\"}', '{\"en\": \"\", \"ru\": \"Детский клуб Riders\"}', '{\"en\": \"\", \"ru\": \"Свободное пребывание и программы для детей от 3 до 15 лет  Творческие мастерские Исследовательские лаборатории Музыкальное погружение Всевозможные игры Кинотеатр Неоновая дискотека Детские дни рождения и праздники Полезный гаджет — мы научим детей творческим программам, и планшеты станут творческой площадкой для создания мультфильмов, кино и музыки Вечерние 3-часовые уникальные программы, включающие в себя творчество, образование и уличные игры: время спасателя, время экстрима, время стрит-арта, время музыки, время снежных сражений, день олимпиады, день путешествия и много другое.\"}', '{\"en\": \"\", \"ru\": \"Временно закрыто\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня 1100 м, здание Роза Приют, ул. Олимпийская, 37, цокольный этаж\"}', '+7 (938) 492-21-80', 'detskiy-klub-riders', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 49, '2021-04-13 16:32:22', NULL),
('c96cbdcf-8f61-4899-b2a0-af780204588b', '{\"en\": \"\", \"ru\": \"Галерея Сувениров Я ♥ Роза Хутор\"}', '{\"en\": \"\", \"ru\": \"Это не просто магазин или лавка, это сказочное и волшебное пространство, которое откроет вам всю красоту и прелесть сувениров.\"}', '{\"en\": \"\", \"ru\": \"Галерея Сувениров Я ♥ Роза Хутор\"}', '{\"en\": \"\", \"ru\": \"Это не просто магазин или лавка, это сказочное и волшебное пространство, которое откроет вам всю красоту и прелесть сувениров. Галерея сувениров Я ♥ Роза Хутор находится в самом сердце горного курорта, на площади Мзымта.  Наши сувениры уникальны и разнообразны, изготавливаются по индивидуальным заказам для нашей галереи, что само собой подразумевает – подобного больше не найти: колокольчики, фигурки из глины и камня, посуда, текстиль, канцелярские товары, предметы декора, авторские сувениры, магниты и многое другое.  В галерее вы всегда найдете подарок для самых близких вам людей – друзей, руководителей и партнеров. От нас вы не сможете уйти с пустыми руками и без памятной фотографии об этом чудесном месте. Наши сувениры оставят приятные воспоминания и улыбку о проведенном времени на Роза Хутор.\"}', '{\"en\": \"\", \"ru\": \"10:00 – 21:00 (лето), 10:00 – 22:00 (зима)\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, ул. Олимпийская, 35, Романов мост (подмостовое помещение)\"}', '+ 7 (938) 872-77-37', 'ya-lyublyu-roza-hutor', 0, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 2, '2021-04-12 16:25:34', NULL),
('cac78905-0b74-46ae-9c77-f3a83c0e377f', '{\"en\": \"\", \"ru\": \"Бар-ресторан Wok Bangkok\"}', '{\"en\": \"\", \"ru\": \"Первый ресторан паназиатской кухни на курорте Роза Хутор.\"}', '{\"en\": \"\", \"ru\": \"Бар-ресторан Wok Bangkok\"}', '{\"en\": \"\", \"ru\": \"Первый ресторан паназиатской кухни на курорте Роза Хутор!  Новый бар-ресторан на курорте, уголок азиатской кухни с любимыми блюдами из разных районов Азии и модным интерьером в лучшем стиле баров Бангкока и Сингапура!  Wok Bangkok предлагает посмотреть на зимний отдых шире. Добавить в него нотку экзотического колорита и отправиться в путешествие по Азии, не покидая заснеженного курорта. Ресторан создан на основе большого опыта проживания в странах Юго-восточной Азии, где аутентичная еда подаётся не только в уличных фургончиках, но и в респектабельных барах и ресторанах.\"}', '{\"en\": \"\", \"ru\": \"12:00 – 00:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, Набережная Лаванда, 2 (здание катка), 2-й этаж\"}', '+ 7 (938) 461-67-13', 'wokbangkok', 1500, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 22, '2021-04-12 21:39:59', NULL),
('d1b83c6c-089b-4605-856e-0fd0a01b0ae3', '{\"en\": \"\", \"ru\": \"Магазин One More Wave\"}', '{\"en\": \"\", \"ru\": \"One More Wave — качественная одежда для комфорта в жизни и спорте.\"}', '{\"en\": \"\", \"ru\": \"Магазин One More Wave\"}', '{\"en\": \"\", \"ru\": \"One More Wave — качественная одежда для комфорта в жизни и спорте. Фирменная одежда, которая подойдет как для отпуска, так и выходных дома, и на даче, и в городе. В продаже: женские и мужские комбинезоны, дизайнерские худи и кофты, майки и многое другое.  Вас приятно удивят цены, ведь одежда производится в России.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 21:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня, ул. Медовея, 8\"}', '+7 (918) 914-38-08', 'magazin-one-more-wave', 0, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 5, '2021-04-12 16:50:53', NULL),
('d25ef566-2c98-4b8d-a6ba-06e37f8c03a1', '{\"en\": \"\", \"ru\": \"От снежников до водопадов\"}', '{\"en\": \"\", \"ru\": \"Спуск по естественной тропе через балаган греческого пастуха, горный источник и  реликтовый лес к ручью. 8,2 км.\"}', '{\"en\": \"\", \"ru\": \"От снежников до водопадов\"}', '{\"en\": \"\", \"ru\": \"На маршруте От снежников до водопадов туристы совершают спуск по естественной тропе через балаган греческого пастуха, мимо горного источника, и далее через реликтовый лес к ручью. На этом маршруте путник почувствует себя вдали от суеты городов, услышит только пение птиц и шелест листвы. Это однодневное и легко доступное погружение в мир дикой природы.  Трекинг на 5-6 часов пешего пути стартует на отметке 2320 м, проходит по альпийским лугам, пересекает верхнюю границу горного леса. Далее вдоль спуска хвойные леса сменяются полосой букового и березового криволесья, где также можно найти рябину, черемуху, чернику. В зоне маршрута расположена красивейшая долина рододендронов, пик цветения которых приходится на июль.  Маршрут завершается в парке водопадов Менделиха, где гости курорта смогут продолжить трекинг по кольцевым маршрутам парка или подняться обратно в цивилизацию на кресельной канатной дороге Эдельвейс.\"}', '{\"en\": \"\", \"ru\": \"Закрыто до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Пик, 2320 м\"}', '+7 (938) 888-02-14', 'ot-snezhnikov-do-vodopadov', 1050, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 34, '2021-04-12 23:17:15', NULL),
('d914c149-e1d4-49a6-a6d2-dcdb3ec4dba7', '{\"en\": \"\", \"ru\": \"Магазин ODLO\"}', '{\"en\": \"\", \"ru\": \"Магазин ODLO предлагает широкий выбор товаров для отдыха и спорта.\"}', '{\"en\": \"\", \"ru\": \"Магазин ODLO\"}', '{\"en\": \"\", \"ru\": \"Магазин ODLO предлагает широкий выбор товаров для отдыха и спорта. ODLO – дизайнерская и в то же время функциональная одежда для активного отдыха и занятий спортом. Идеальное сочетание инновационных технологий, яркого стильного дизайна и комфортного кроя находит применение во всех линиях одежды ODLO.   В ассортименте:  функциональное белье лыжная экипировка детская одежда одежда для бега одежда для велоспорта аксессуары  Широкий ассортимент товаров  и высокое качество продукции ODLO удовлетворят требования самых искушенных спортсменов и не оставит равнодушными любителей активного образа жизни.  Почувствуй страсть к спорту вместе с ODLO!\"}', '{\"en\": \"\", \"ru\": \"9:00 — 20:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, наб. Панорама, 2\"}', '+7 (938) 471-53-94', 'magazin-odlo', 6000, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 6, '2021-04-12 16:58:14', NULL),
('da187285-abea-4dce-ab24-d6d1fc5f6586', '{\"en\": \"\", \"ru\": \"Пляж «Роза Хутор» на Черном море\"}', '{\"en\": \"\", \"ru\": \"Пляж на морском побережье с комфортными раздевалками, удобными лежаками, бесплатными полотенцами и массой развлечений для активного морского отдыха.\"}', '{\"en\": \"\", \"ru\": \"Пляж «Роза Хутор» на Черном море\"}', '{\"en\": \"\", \"ru\": \"Летом Роза Хутор приглашает своих гостей на пляж на морском побережье с комфортными раздевалками, удобными лежаками и массой развлечений для активного морского отдыха!  Для туристов, проживающих в отелях курорта, в летний период организован трансфер на пляж «Роза Хутор», расположенный на берегу Имеретинской бухты. К услугам гостей Роза Хутор — лежаки, зонты, полотенца и удобные раздевалки. Для самых маленьких работает детская анимация и детская площадка. А для любителей прыжков в воду сделан специальный понтон. Здесь можно провести время весело: попрактиковаться в сап-серфинге или заняться дайвингом. Роза Хутор гарантирует, что ваш пляжный день подарит вам новые впечатления и будет расписан по минутам. \"}', '{\"en\": \"\", \"ru\": \"Пляж закрыт до следующего летнего сезона\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Адлер, Имеретинская низменность, напротив стадиона Фишт\"}', '8-800-5000-555', 'sea-beach', 0, 1, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 7, '2021-04-12 17:11:31', NULL),
('de66bcf5-4e3a-4d8e-b312-6374b901328e', '{\"en\": \"\", \"ru\": \"Терраса отеля Radisson\"}', '{\"en\": \"\", \"ru\": \"Открытая панорамная терраса с подогреваемым джакузи и баром.\"}', '{\"en\": \"\", \"ru\": \"Терраса отеля Radisson\"}', '{\"en\": \"\", \"ru\": \"Панорамная терраса Mercedes Sky Lounge на крыше одного из лучших горнолыжных отелей России Radisson Rosa Khutor.     Попадая сюда, гостям открываются невероятные виды Кавказских гор, самой высокой точки горного хребта Аибги и главная площадь курорта.    Терраса разделена две зоны: СПА с двумя тёплыми джакузи и уютный открытый бар, где подаются авторские закуски, разнообразные сезонные предложения, авторские коктейли.\"}', '{\"en\": \"\", \"ru\": \"9:00 – 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, ул. Набережная Панорама, 4, отель Radisson Rosa Khutor\"}', '+7 (862) 2431 377', 'mercedes-sky-lounge', 4000, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 55, '2021-04-13 17:05:46', NULL),
('e0eb30f5-e68c-4a20-b410-bf484d40b649', '{\"en\": \"\", \"ru\": \"Родельбан\"}', '{\"en\": \"\", \"ru\": \"Сани, скользящие по рельсам со скоростью до 40 км/час по маршруту с 3 кольцами.\"}', '{\"en\": \"\", \"ru\": \"Родельбан\"}', '{\"en\": \"\", \"ru\": \"Любимое летнее развлечение гостей альпийских курортов теперь есть и на Роза Хутор! Но наш родельбан особенный – с него открываются панорамные виды, он круглогодичный и супер современный! Сани оборудованы ручным тормозом, контролем скорости и сближения, автоматическим подъемом на точку старта. Рельсовый трек имеет 3 кольца-виража.     Знаете, почему невозможно проехать только 1 круг? Сначала все осторожничают, пользуются тормозом и испытывают трек. Но во второй раз уже можно прокатиться с ветерком на полной скорости и от души повизжать на виражах! А если вы катаетесь с ребенком, поверьте нашему опыту – он обязательно захочет еще!  Сани, скользящие по рельсам со скоростью до 40 км/час по маршруту с 3 кольцами-виражами.\"}', '{\"en\": \"\", \"ru\": \"10:00 – 20:30\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня 1100 м, за рестораном Берлога (около подъемника Ювента)\"}', '8 800 5000 555', 'rodelban', 790, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 57, '2021-04-13 17:12:20', NULL),
('e388e631-3071-478b-b094-13e757de1505', '{\"en\": \"\", \"ru\": \"Ресторан Luciano\"}', '{\"en\": \"\", \"ru\": \"Ресторан Luciano – это нотка Италии, Сицилии и немного Неаполя в центре Кавказских гор.\"}', '{\"en\": \"\", \"ru\": \"Ресторан Luciano\"}', '{\"en\": \"\", \"ru\": \"Ресторан Luciano – это нотка Италии, Сицилии и немного Неаполя в центре Кавказских гор. В меню ресторана каждый найдет что-то по вкусу: истинно итальянские закуски, свежайшие салаты и предмет особой гордости поваров заведения – пицца, приготовленная по традиционным рецептам неаполитанских пиццейоло.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, Площадь Ратуши, 2, первый этаж\"}', '+7 (862) 243-97-20', 'restoran-luciano', 1500, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 26, '2021-04-12 22:08:00', NULL),
('ebb12497-33ba-4a18-8ac2-9530bd81583b', '{\"en\": \"\", \"ru\": \"Магазин ПОигрушки\"}', '{\"en\": \"\", \"ru\": \"Магазин с широким ассортиментом детских игрушек, книг и настольных игр!\"}', '{\"en\": \"\", \"ru\": \"Магазин ПОигрушки\"}', '{\"en\": \"\", \"ru\": \"Магазин с широким ассортиментом детских игрушек, книг и настольных игр! Самый большой и яркий магазин для детей на курорте Роза Хутор. Более 8000 лицензионных игрушек, товаров для творчества и книг для детей от 0 до 14 лет. Выделенная зона для творчества и проведения мастер классов. В магазине можно также купить настольные игры для подростков и взрослых. \"}', '{\"en\": \"\", \"ru\": \"10:00 — 22:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина 560 м, ул. Каменка, 1, напротив детской площадки\"}', '+7 (938) 442-22-90', 'magazin-dlya-detey-poigrushki', 750, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 46, '2021-04-13 16:12:03', NULL),
('ee46bf8e-3aa0-40ed-9356-eb4064db5ff1', '{\"en\": \"\", \"ru\": \"Магазин «АльпИндустрия»\"}', '{\"en\": \"\", \"ru\": \"Экипируем в горы!\"}', '{\"en\": \"\", \"ru\": \"Магазин «АльпИндустрия»\"}', '{\"en\": \"\", \"ru\": \"Экипируем в горы! На протяжении уже многих лет АльпИндустрия остается крупнейшим поставщиком outdoor-снаряжения в регионе. Клиенты ценят нас за добродушный прием, профессиональные консультации и за высокое качество одежды, обуви и снаряжения от ведущих мировых брендов. Мы болеем горами и помогаем Вам отправиться к вершине вашей мечты. Наша задача — сделать этот путь максимально безопасным и комфортным для вас.\"}', '{\"en\": \"\", \"ru\": \"9:00 — 20:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, ул. Каменка, д. 2\"}', '+7 (862) 243-97-12', 'alpindustriya', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 10, '2021-04-12 17:53:14', NULL),
('ee933e8a-41d2-4b15-8576-9710d1a72027', '{\"en\": \"\", \"ru\": \"Хаски Хутор на 1100 м\"}', '{\"en\": \"\", \"ru\": \"Самые активные и дружелюбные представители северных ездовых пород – сибирские хаски и аляскинские маламуты.\"}', '{\"en\": \"\", \"ru\": \"Хаски Хутор на 1100 м\"}', '{\"en\": \"\", \"ru\": \"Ежедневно вас ждут самые активные и дружелюбные представители северных ездовых пород – сибирские хаски и аляскинские маламуты. Четвероногие друзья с радостью идут на контакт с человеком.   Стоимость посещения (20 минут): Взрослые – 500 ₽ Дети – 300 ₽  Дети до 5 лет в сопровождении взрослых – бесплатно   Скидки:  50% на посещение Хаски Хутор в день рождения (при предъявлении подтверждающего документа). 50% скидка на посещение пенсионерам, многодетным семьям, инвалидам I и II группы и их сопровождающим, ветеранам военных действий. Договорная цена на проведение дня рождения, корпоратива, тематического праздника с участием аниматора, инструктора.\"}', '{\"en\": \"\", \"ru\": \"10:00 – 17:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Горная Олимпийская деревня 1100 м, терраса за рестораном Берлога\"}', '+7 (962) 887-81-21, +7 (938) 466-69-66', 'khaski-khutor-na-1100-m', 300, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 59, '2021-04-13 17:17:55', NULL),
('f08424cf-d0d5-4717-82cd-ff774583c092', '{\"en\": \"\", \"ru\": \"Магазин Quiksilver\"}', '{\"en\": \"\", \"ru\": \"Уже более 50 лет Quiksilver является лидирующим производителем одежды и обуви в категориях кэжуал, лайфстайл и экшн-спорт.\"}', '{\"en\": \"\", \"ru\": \"Магазин Quiksilver\"}', '{\"en\": \"\", \"ru\": \"Уже более 50 лет Quiksilver является лидирующим производителем одежды и обуви в категориях кэжуал, лайфстайл и экшн-спорт. Quiksilver продается и имеет поклонников более чем в 90 странах мира. В России бренд открыл уже более 40 магазинов и не собирается останавливаться на достигнутом. Магазины Quiksilver всегда отличаются уникальным и ярким дизайном. Покупателей приятно удивит разнообразие ассортимента мужской коллекции Quiksilver и женской одежды Roxy для города, океана и катания в горах. Культовый скейт-бренд DC Shoes представлен в магазине в городской, скейтовой и сноуборд-линейке.  Летом в магазине вы найдете все необходимое для отдыха на море: купальники, бордшорты, сланцы, сумки и рюкзаки. Зимой сноубордисты и лыжники будут обрадованы широким выбором высокотехнологичных курток, штанов, ботинок и аксессуаров для катания. Также в магазине всегда можно найти стильную одежду для города, соответствующую последним модным трендам.\"}', '{\"en\": \"\", \"ru\": \"10:00 — 19:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, набережная Панорама, 2. Горная Олимпийская деревня, ул. Пасека, 3\"}', '+7 (862) 244-01-13', 'quiksilver', 0, 0, 1, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 4, '2021-04-12 16:44:43', NULL),
('f2aec553-b857-4199-b346-91767921488e', '{\"en\": \"\", \"ru\": \"Гастропаб Трикони\"}', '{\"en\": \"\", \"ru\": \"Гастропаб от Трикони Family с абсолютно новым авторским меню по самым приятным ценам.\"}', '{\"en\": \"\", \"ru\": \"Гастропаб Трикони\"}', '{\"en\": \"\", \"ru\": \"Гастропаб от Трикони Family с абсолютно новым авторским меню по самым приятным ценам. Уютная летняя терраса с лучшими видами, восхитительные закаты. Импортное и местное пиво, фирменные лимонады и морсы.\"}', '{\"en\": \"\", \"ru\": \"8:00 – 23:00\"}', '{\"ru\": 1}', NULL, '{\"en\": \"\", \"ru\": \"Роза Долина, Набережная Полянка, 1\"}', '+7 (918) 000-12-73', 'trikoni', 800, 0, 0, '{\"en\": \"\", \"ru\": \"Купить\"}', NULL, NULL, 1, 31, '2021-04-12 22:27:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `place_block`
--

CREATE TABLE `place_block` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `place_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `title` json NOT NULL,
  `body` json NOT NULL,
  `photo_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `video_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `place_geo_point`
--

CREATE TABLE `place_geo_point` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `place_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `latitude` decimal(10,8) UNSIGNED NOT NULL,
  `longitude` decimal(11,8) UNSIGNED NOT NULL,
  `description` json NOT NULL,
  `sort` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `place_photo`
--

CREATE TABLE `place_photo` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `place_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `file_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `sort` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `place_section`
--

CREATE TABLE `place_section` (
  `place_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `section_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `place_video`
--

CREATE TABLE `place_video` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `place_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `video_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `sort` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pps_groups`
--

CREATE TABLE `pps_groups` (
  `id` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  `module_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pps_periods`
--

CREATE TABLE `pps_periods` (
  `id` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pps_prices`
--

CREATE TABLE `pps_prices` (
  `id` int NOT NULL,
  `period_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pps_products`
--

CREATE TABLE `pps_products` (
  `id` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  `group_id` bigint NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `type` smallint UNSIGNED NOT NULL,
  `section_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `type`, `section_id`, `name`, `created_at`, `updated_at`, `is_active`) VALUES
('002c4d13-da17-45ff-8479-e4761fb738dd', 0, 'a4741b9e-e677-4fb1-8187-84ff8b52e23d', 'Веревочный парк ВЗР безлимитный на 1 день (И)', '2021-02-04 13:39:33', '2021-03-15 11:39:22', 1),
('0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 0, 'a4741b9e-e677-4fb1-8187-84ff8b52e23d', 'Веревочный парк ДЕТ безлимитный на 1 день (И)', '2021-02-04 13:39:50', '2021-03-15 11:39:23', 1),
('0e2760d7-87ae-4477-841c-168ad557850c', 0, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', ' ', '2021-02-04 12:40:48', NULL, 1),
('14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 'Ски-пасс 2 дня ДЕТ (ИРБ) (RHE)', '2021-02-04 12:43:33', '2021-03-15 11:39:36', 1),
('153b2fff-4a9f-4289-8434-3b1ed72c8611', 300, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', 'Родельбан 3 круга ВЗР (И)', '2021-02-04 12:41:09', '2021-03-15 11:40:41', 1),
('16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 0, 'b212cc4e-03b2-47df-9c3c-95bda13eee0f', ' ', '2021-02-04 13:40:16', NULL, 1),
('17ec8006-84b6-41e7-aedf-349b83efcca1', 400, '3b560e37-944f-4517-aaad-97daa5c447df', 'Билет в Музей Археологии ДЕТ (И)', '2021-02-04 12:38:00', '2021-03-15 11:39:24', 1),
('189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 300, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', 'Родельбан 1 круг ВЗР (И)', '2021-02-04 12:38:46', '2021-03-15 11:40:40', 1),
('24cb3944-236e-49b6-bb46-f3bfe4dc3676', 100, '96d76c1f-128b-44b9-b0a0-3558b61b2ddd', 'Ски-пасс Годовой ВЗР (RHE)', '2021-02-04 14:51:49', '2021-03-15 11:39:39', 1),
('3068952b-c667-4c94-9bc1-8db61dc4d44b', 300, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', 'Родельбан 3 круга ВЗР+ДЕТ (ИП)', '2021-02-04 12:41:24', '2021-03-15 11:40:41', 1),
('309be581-4dcf-4b33-8d0e-d7b51dd3844c', 100, '71fda3ad-bb21-49bc-81fd-df2ca9512fc1', 'Ски-пасс Сезонный ДЕТ (RHE)', '2021-02-04 14:50:58', '2021-03-15 11:40:30', 1),
('347d6a24-758b-4cc3-8900-6ce9d3e1e070', 100, '71fda3ad-bb21-49bc-81fd-df2ca9512fc1', 'Ски-пасс Сезонный ВЗР (RHE)', '2021-02-04 13:40:56', '2021-03-15 11:40:20', 1),
('35f028ec-512b-4ff9-b077-8179c6eef588', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 'Ски-пасс 2 дня ВЗР (ИРБ) (RHE)', '2021-02-04 12:43:45', '2021-03-15 11:39:33', 1),
('41491d6c-4bdc-430a-99ec-019fc1abf102', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 'Ски-пасс 1 день ВЗР (ИРБ) (RHE)', '2021-02-04 12:43:18', '2021-03-15 11:39:26', 1),
('4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:44:23', NULL, 1),
('573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 'Ски-пасс 1 день Вечерний ДЕТ (ИРБ) (RHE)', '2021-02-04 12:41:43', '2021-03-15 11:39:25', 1),
('634aee0e-8a9a-4c55-8efc-95ff06aa948d', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 'Ски-пасс 1 день Вечерний ВЗР (ИРБ) (RHE)', '2021-02-04 12:42:47', '2021-03-15 11:39:25', 1),
('79c4d943-0a47-44ae-b26f-e5e51b24e95f', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 13:38:59', NULL, 1),
('7d9eff11-c45f-493c-8685-0750e5a4eec3', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:44:54', NULL, 1),
('906c9040-3d9f-4517-b16b-5df8115f16bc', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:45:59', NULL, 1),
('93beeaf1-9caa-4029-9ae2-055488d67f74', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:45:47', NULL, 1),
('98efb9e0-b996-4e44-8c01-887ce9e09da0', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 13:38:38', NULL, 1),
('99f047a1-0331-49e9-aa4e-5a39063b7f2f', 300, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', 'Родельбан 2 круга ВЗР+ДЕТ (ИП)', '2021-02-04 12:40:23', '2021-03-15 11:40:40', 1),
('9b11d8a4-a2ae-4346-b6f9-83b55353475e', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:46:42', NULL, 1),
('a4104c10-2746-4cca-9270-0745045acfc9', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:44:08', NULL, 1),
('ab4edbb1-3dc7-40c4-9f82-faed43e34705', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:45:25', NULL, 1),
('b41076b2-0f9d-4ad0-9790-b040f28ea536', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 13:36:43', NULL, 1),
('b7ddc11f-31da-44e5-bdb9-23db298dac04', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:46:56', NULL, 1),
('bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 100, '96d76c1f-128b-44b9-b0a0-3558b61b2ddd', 'Ски-пасс Годовой ДЕТ (RHE)', '2021-02-04 14:52:01', '2021-03-15 11:40:00', 1),
('d958ce8c-9214-44f6-aa4d-59a8cede1d88', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:45:08', NULL, 1),
('e41cab29-37f3-4e05-916b-64ae00d6e0f5', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:45:35', NULL, 1),
('e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 400, '3b560e37-944f-4517-aaad-97daa5c447df', 'Билет в Музей Археологии ВЗР (И)', '2021-02-04 12:37:15', '2021-03-15 11:39:23', 1),
('f840b292-6cac-4b7f-b373-23165726bbde', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', ' ', '2021-02-04 12:44:37', NULL, 1),
('fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 0, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 'Ски-пасс 1 день ДЕТ (ИРБ) (RHE)', '2021-02-04 12:43:03', '2021-03-15 11:39:29', 1),
('faa07c3a-04ef-458e-b4d7-9cfde450718f', 300, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', 'Родельбан 1 круг ВЗР+ДЕТ (ИП)', '2021-02-04 12:39:54', '2021-03-15 11:40:40', 1),
('fc285538-b95f-4b04-982e-9728852681ec', 0, 'b212cc4e-03b2-47df-9c3c-95bda13eee0f', ' ', '2021-02-04 13:40:32', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `price` bigint UNSIGNED NOT NULL,
  `date_start` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `date_end` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_active` tinyint UNSIGNED NOT NULL,
  `pps_period_id` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_price`
--

INSERT INTO `product_price` (`id`, `product_id`, `price`, `date_start`, `date_end`, `is_active`, `pps_period_id`, `created_at`, `updated_at`) VALUES
('0101fceb-e05b-4168-968f-d5e0f60cbafe', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 1, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('081c640e-a4ce-4273-b637-fd671e19aa80', 'd958ce8c-9214-44f6-aa4d-59a8cede1d88', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('0ba26998-ee87-4dc9-8144-97e83e3bc16f', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 5200000, '2020-12-05 00:00:00', '2021-01-31 00:00:00', 1, 31920840, '2021-03-11 12:23:14', '2021-03-15 11:40:30'),
('0c81ebb7-4535-4ec9-a463-c4121df58fc1', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('0fe4baf1-8fa3-4193-96a0-b1d28b5235c4', '41491d6c-4bdc-430a-99ec-019fc1abf102', 250800, '2021-01-11 00:00:00', '2021-01-29 00:00:00', 1, 28859651, '2021-03-15 11:39:28', NULL),
('13791622-2c0b-422a-a7e0-9f65291a40de', '0e2760d7-87ae-4477-841c-168ad557850c', 3232, '2021-02-07 04:30:35', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:30:35', NULL),
('19eb9637-3432-4a9a-ba04-f73d5afbcda2', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 153000, '2021-04-05 00:00:00', '2021-04-30 00:00:00', 1, 28859654, '2021-03-15 11:39:39', NULL),
('22eb86ab-9e0e-4d32-8069-2d0148ba78b0', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 255000, '2021-01-11 00:00:00', '2021-01-29 00:00:00', 1, 28859651, '2021-03-15 11:39:38', NULL),
('2905fa4e-0381-48de-965e-bc7407fef207', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 1490000, '2021-02-01 00:00:00', '2021-02-28 00:00:00', 1, 28869524, '2021-03-11 12:23:18', '2021-03-15 11:40:34'),
('30eced32-ee58-4d5f-8efe-d4aa61508ea9', 'a4104c10-2746-4cca-9270-0745045acfc9', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('3435630b-4a2c-4aa9-aa39-27d81e5bad97', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 35100, '2020-12-01 00:00:00', '2021-04-30 00:00:00', 1, 31205029, '2021-03-11 12:22:23', '2021-03-15 11:39:24'),
('35996842-e198-4cc6-90c6-f4c88a4df593', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 255000, '2020-12-12 00:00:00', '2020-12-25 00:00:00', 1, 28859649, '2021-03-15 11:39:37', NULL),
('3af66c0c-8788-4b85-b23a-02c85a27b938', '41491d6c-4bdc-430a-99ec-019fc1abf102', 267800, '2021-01-30 00:00:00', '2021-02-28 00:00:00', 1, 28859652, '2021-03-15 11:39:28', NULL),
('3bf6f1cc-3d3d-48b1-9664-cc00cd627520', '41491d6c-4bdc-430a-99ec-019fc1abf102', 148800, '2021-04-05 00:00:00', '2021-04-30 00:00:00', 1, 28859654, '2021-03-15 11:39:29', NULL),
('3d2991e4-6ead-4979-9314-d60670f76284', '93beeaf1-9caa-4029-9ae2-055488d67f74', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('3eb6287c-5b71-4014-8811-9abc019dbf0e', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 127500, '2021-03-01 00:00:00', '2021-04-04 00:00:00', 1, 28859653, '2021-03-15 11:39:32', NULL),
('418bcb38-0140-4200-8a0e-37a8817db35e', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('49a4cda4-9c06-47ca-8797-8893be76b0c6', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 100, '2021-02-07 04:26:47', '2021-03-24 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('4f3c9944-bfc9-4f5c-9201-5550b157e57e', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1050000, '2021-04-01 00:00:00', '2021-04-30 00:00:00', 1, 28869526, '2021-03-11 12:22:59', '2021-03-15 11:40:16'),
('50f31558-b056-4f9c-bf39-115fae777573', 'e41cab29-37f3-4e05-916b-64ae00d6e0f5', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('53e7c7ba-0d96-499b-9ea4-b2d77aa48909', 'b7ddc11f-31da-44e5-bdb9-23db298dac04', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('588ea2eb-dfe2-4ef9-aeb1-d018f11da301', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1790000, '2021-02-01 00:00:00', '2021-02-28 00:00:00', 1, 28869524, '2021-03-11 12:22:51', '2021-03-15 11:40:09'),
('59a38db0-de5c-4899-bc25-9ec6982f7465', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:33:08', NULL),
('5ce9305f-9e44-484c-90ed-30b0ae24e853', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 3, '2021-02-08 04:28:18', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:28:18', NULL),
('5dcefc98-66d1-4c89-89d5-b484084ac0c0', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 76500, '2021-04-05 00:00:00', '2021-04-30 00:00:00', 1, 28859654, '2021-03-15 11:39:33', NULL),
('5fa6ed1a-1875-4142-91ea-b88af2e3a4ce', '17ec8006-84b6-41e7-aedf-349b83efcca1', 17100, '2020-07-01 00:00:00', '2020-11-30 00:00:00', 1, 28519020, '2021-03-11 12:22:23', '2021-03-15 11:39:25'),
('60a37256-f01e-46a5-ade5-f0338f13c65f', 'ab4edbb1-3dc7-40c4-9f82-faed43e34705', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('61894dac-2e5c-4732-925f-d218af9be467', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 4500000, '2020-11-01 00:00:00', '2020-12-04 00:00:00', 1, 28869523, '2021-03-11 12:23:08', '2021-03-15 11:40:22'),
('66a71e6d-1a96-4c40-a473-81fe28769c18', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 289000, '2020-12-26 00:00:00', '2021-01-10 00:00:00', 1, 28859650, '2021-03-15 11:39:37', NULL),
('69ab4563-4990-47e6-a652-1ff2b5cff45f', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 100, '2021-02-08 04:32:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:32:47', NULL),
('6afdd410-dc91-4544-b585-88b5458b19a1', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 6200000, '2020-12-05 00:00:00', '2021-01-31 00:00:00', 1, 31920840, '2021-03-11 12:22:44', '2021-03-15 11:40:00'),
('6b1b5897-2f0b-4a38-ae84-cd67c84b713c', '35f028ec-512b-4ff9-b077-8179c6eef588', 297500, '2021-04-05 00:00:00', '2021-04-30 00:00:00', 1, 28859654, '2021-03-15 11:39:36', NULL),
('6c0bfaf7-50a7-4062-aa61-610e327dce1e', 'fc285538-b95f-4b04-982e-9728852681ec', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('6c2f8785-e96d-4c84-8a1c-3b53b6501152', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 127500, '2020-12-12 00:00:00', '2020-12-25 00:00:00', 1, 28859649, '2021-03-15 11:39:30', NULL),
('6e31c2f0-23b7-4715-8f5f-13edfdd14c6b', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 165000, '2020-12-01 00:00:00', '2021-04-30 00:00:00', 1, 31122724, '2021-03-11 12:23:25', '2021-03-15 11:40:41'),
('77541fa4-f121-4b47-ba18-7f0ee09a3535', '41491d6c-4bdc-430a-99ec-019fc1abf102', 400, '2021-02-08 04:28:18', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('7c331da5-0a33-4192-b2e6-891f210c9647', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 127500, '2021-01-11 00:00:00', '2021-01-29 00:00:00', 1, 28859651, '2021-03-15 11:39:31', NULL),
('7dc2c15e-396d-4658-adc6-b5db6ab04fac', 'b41076b2-0f9d-4ad0-9790-b040f28ea536', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('7df877e2-34b8-4d73-bbba-859b38cac2d6', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 277, '2021-02-08 04:28:18', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('7edf0deb-c68d-41d9-b6a2-c067dcd5f7e5', '35f028ec-512b-4ff9-b077-8179c6eef588', 199, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:27:41', NULL),
('8240a9ea-eebf-45d8-bf64-ff87fb0ab91e', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 70000, '2020-12-25 00:00:00', '2021-03-31 00:00:00', 1, 21692693, '2021-03-15 11:39:26', NULL),
('85ab0876-4e31-4681-abb2-8a0c7c7f8f93', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 1290000, '2021-03-01 00:00:00', '2021-03-31 00:00:00', 1, 28869525, '2021-03-11 12:23:20', '2021-03-15 11:40:36'),
('86e61246-0d76-44cd-8f1f-af8c9cd94efc', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 2500000, '2020-12-05 00:00:00', '2021-01-31 00:00:00', 1, 31920840, '2021-03-11 12:23:06', '2021-03-15 11:40:20'),
('8bc2279a-0f4f-4ec8-8091-9266515c6f21', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 267800, '2021-01-30 00:00:00', '2021-02-28 00:00:00', 1, 28859652, '2021-03-15 11:39:38', NULL),
('8be305e1-abd7-4d02-acda-4fb34b56de50', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 2560000, '2021-04-01 00:00:00', '2021-04-30 00:00:00', 1, 28869526, '2021-03-11 12:22:40', '2021-03-15 11:39:56'),
('8e6e8c5f-96f0-4a7c-886f-b6ed4d589afd', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 2000000, '2020-12-05 00:00:00', '2021-01-31 00:00:00', 1, 31920840, '2021-03-11 12:23:23', '2021-03-15 11:40:40'),
('91aa7994-9464-4d17-8bb4-6e451cf4a6fd', 'f840b292-6cac-4b7f-b373-23165726bbde', 200, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('98561279-22cd-4d5b-8e45-334cc80f8d8b', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 299, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('99e646c8-231d-45c1-a5f8-81c138c7b5e9', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 3290000, '2021-03-01 00:00:00', '2021-03-31 00:00:00', 1, 28869525, '2021-03-11 12:23:11', '2021-03-15 11:40:26'),
('9ad3d4af-df67-4cbc-8081-c7c84f328664', '7d9eff11-c45f-493c-8685-0750e5a4eec3', 1222, '2021-02-08 04:28:18', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:31:55', NULL),
('9bc9f6fa-ef54-4c70-9b17-1e9fd828f363', '41491d6c-4bdc-430a-99ec-019fc1abf102', 250800, '2020-12-12 00:00:00', '2020-12-25 00:00:00', 1, 28859649, '2021-03-15 11:39:26', NULL),
('9e891f60-5128-408a-9b47-74695535508a', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 2132, '2021-02-08 04:28:18', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:31:31', NULL),
('a215417a-ebac-4595-9836-d337d8f6ce3a', '98efb9e0-b996-4e44-8c01-887ce9e09da0', 200, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:27:41', NULL),
('a244130c-65fc-484e-bae1-f9ec0193d783', '17ec8006-84b6-41e7-aedf-349b83efcca1', 17100, '2020-12-01 00:00:00', '2021-04-30 00:00:00', 1, 31205029, '2021-03-11 12:22:24', '2021-03-15 11:39:25'),
('a38ac9b2-ac53-4ad4-816e-237812802609', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 144500, '2021-01-30 00:00:00', '2021-02-28 00:00:00', 1, 28859652, '2021-03-15 11:39:31', NULL),
('a43babc0-657b-4154-9c48-05c7d5a83ea8', '0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 213, '2021-02-08 04:29:50', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:29:50', NULL),
('a7bf5200-602b-4291-8a2f-e17879ee0a39', '35f028ec-512b-4ff9-b077-8179c6eef588', 535500, '2021-01-30 00:00:00', '2021-02-28 00:00:00', 1, 28859652, '2021-03-15 11:39:35', NULL),
('ad4897e6-9dcb-45ca-a662-063579a80a9e', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 99000, '2020-12-01 00:00:00', '2021-04-30 00:00:00', 1, 31122724, '2021-03-11 12:23:24', '2021-03-15 11:40:40'),
('af6109b3-7efe-4ee8-8212-2ede4479b219', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 2250000, '2021-04-01 00:00:00', '2021-04-30 00:00:00', 1, 28869526, '2021-03-11 12:23:13', '2021-03-15 11:40:28'),
('af655f0a-02f1-44fb-90ae-294e0f765613', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 255000, '2021-03-01 00:00:00', '2021-04-04 00:00:00', 1, 28859653, '2021-03-15 11:39:39', NULL),
('b213bd1f-f469-4bbe-8cb8-8829f08e88a0', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 108, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('ba2a1b03-0020-4529-9c57-8c354055b3db', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 4490000, '2021-02-01 00:00:00', '2021-02-28 00:00:00', 1, 28869524, '2021-03-11 12:22:32', '2021-03-15 11:39:47'),
('ba6367f5-ca4f-45d9-b9e4-91dd6b5bf581', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 135000, '2020-12-25 00:00:00', '2021-03-31 00:00:00', 1, 21692693, '2021-03-15 11:39:25', NULL),
('bc0a5859-9325-448d-99b2-f09db611b2a9', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 145000, '2020-12-01 00:00:00', '2021-04-30 00:00:00', 1, 31122724, '2021-03-11 12:23:25', '2021-03-15 11:40:41'),
('bdad4401-2d78-468d-a190-88677f5832de', '35f028ec-512b-4ff9-b077-8179c6eef588', 578000, '2020-12-26 00:00:00', '2021-01-10 00:00:00', 1, 28859650, '2021-03-15 11:39:34', NULL),
('bec0de16-a60f-4af8-b91f-24972989480e', '41491d6c-4bdc-430a-99ec-019fc1abf102', 293300, '2020-12-26 00:00:00', '2021-01-10 00:00:00', 1, 28859650, '2021-03-15 11:39:27', NULL),
('bf0d6187-1444-4c39-9aa9-169548302f7b', '35f028ec-512b-4ff9-b077-8179c6eef588', 501500, '2021-01-11 00:00:00', '2021-01-29 00:00:00', 1, 28859651, '2021-03-15 11:39:34', NULL),
('c244961a-1a39-4bdf-98d8-31677996035a', '17ec8006-84b6-41e7-aedf-349b83efcca1', 2, '2021-02-08 04:27:41', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:27:41', NULL),
('c73a68e8-2c3a-4d07-a9f9-d69dc2897fb8', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 35100, '2020-07-01 00:00:00', '2020-11-30 00:00:00', 1, 28519020, '2021-03-11 12:22:22', '2021-03-15 11:39:24'),
('c96b423c-c2ee-40ee-96e2-679463e3e046', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 200, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('cb8d84b4-1c00-4cb1-91ca-472f69ae4429', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 5300000, '2020-11-01 00:00:00', '2020-12-04 00:00:00', 1, 28869523, '2021-03-11 12:22:28', '2021-03-15 11:39:43'),
('ccd12b12-2b9b-46df-955b-e7e7608cb268', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 850000, '2021-04-01 00:00:00', '2021-04-30 00:00:00', 1, 28869526, '2021-03-11 12:23:22', '2021-03-15 11:40:37'),
('cda7611e-2100-48d9-a5bc-6034fbe217de', '35f028ec-512b-4ff9-b077-8179c6eef588', 501500, '2021-03-01 00:00:00', '2021-04-04 00:00:00', 1, 28859653, '2021-03-15 11:39:35', NULL),
('cfd8fde8-fb49-4e1e-b24f-1edbaf26354e', '002c4d13-da17-45ff-8479-e4761fb738dd', 122, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('d1d44cc8-7259-4c32-854d-7cd7b48f5cb9', 'd958ce8c-9214-44f6-aa4d-59a8cede1d88', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('d45c252a-697b-435b-bbde-8f1a67f3ce03', '9b11d8a4-a2ae-4346-b6f9-83b55353475e', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:32:22', NULL),
('d4aef626-b0da-4fea-a5ee-f557cf46ebf5', '41491d6c-4bdc-430a-99ec-019fc1abf102', 250800, '2021-03-01 00:00:00', '2021-04-04 00:00:00', 1, 28859653, '2021-03-15 11:39:29', NULL),
('d531351c-a3cb-46f9-a70c-70022108e483', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 1700000, '2020-11-01 00:00:00', '2020-12-04 00:00:00', 1, 28869523, '2021-03-11 12:23:16', '2021-03-15 11:40:32'),
('d832daa4-c87f-40ba-bc53-4d373edd5a1f', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 144500, '2020-12-26 00:00:00', '2021-01-10 00:00:00', 1, 28859650, '2021-03-15 11:39:30', NULL),
('d8f5a605-3682-416a-9843-3562dc8f4117', '3068952b-c667-4c94-9bc1-8db61dc4d44b', 165000, '2020-12-01 00:00:00', '2021-04-30 00:00:00', 1, 31122724, '2021-03-11 12:23:26', '2021-03-15 11:40:42'),
('d992d548-8eec-41ca-b645-21e1a8f5a050', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('dac45f8b-8444-4df8-852c-751fcf32a72c', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 3890000, '2021-02-01 00:00:00', '2021-02-28 00:00:00', 1, 28869524, '2021-03-11 12:23:09', '2021-03-15 11:40:24'),
('dc9bea79-612c-485e-8119-b48f6018ab63', '002c4d13-da17-45ff-8479-e4761fb738dd', 95000, '2020-12-12 00:00:00', '2021-04-30 00:00:00', 1, 29751993, '2021-03-11 12:22:21', '2021-03-15 11:39:23'),
('e368b723-e1d0-48f3-abfd-18d0ad4aaa91', '3068952b-c667-4c94-9bc1-8db61dc4d44b', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('e711182d-cff2-4512-bfe0-58319045a620', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 99000, '2020-12-01 00:00:00', '2021-04-30 00:00:00', 1, 31122724, '2021-03-11 12:23:24', '2021-03-15 11:40:40'),
('e7e3c514-d164-4ff7-bb78-c131d8648e3a', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 1590000, '2021-03-01 00:00:00', '2021-03-31 00:00:00', 1, 28869525, '2021-03-11 12:22:55', '2021-03-15 11:40:12'),
('e876424b-5845-4fb6-9d70-f85302cbc28a', '0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 65000, '2020-12-12 00:00:00', '2021-04-30 00:00:00', 1, 29751993, '2021-03-11 12:22:22', '2021-03-15 11:39:23'),
('e935ca61-7dc7-49e6-b3e6-188f3b1b121e', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 2100000, '2020-11-01 00:00:00', '2020-12-04 00:00:00', 1, 28869523, '2021-03-11 12:22:48', '2021-03-15 11:40:04'),
('ecafa4ac-4bf8-475f-bfda-0269d09457e2', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 3790000, '2021-03-01 00:00:00', '2021-03-31 00:00:00', 1, 28869525, '2021-03-11 12:22:36', '2021-03-15 11:39:51'),
('fb37cf9b-5d97-419b-a8c1-62abc495b222', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 100, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL),
('fc3f0951-0231-4853-848a-3956f4af0c96', '35f028ec-512b-4ff9-b077-8179c6eef588', 501500, '2020-12-12 00:00:00', '2020-12-25 00:00:00', 1, 28859649, '2021-03-15 11:39:33', NULL),
('ffdfcd1a-391f-4571-9f9a-86aa356600f8', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 244, '2021-02-07 04:26:47', '2021-03-17 02:19:19', 0, 0, '2021-02-07 04:26:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_settings`
--

CREATE TABLE `product_settings` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `card_type` smallint UNSIGNED NOT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_of_days` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rounds` smallint UNSIGNED NOT NULL,
  `pps_product_id` int UNSIGNED NOT NULL,
  `new_card_pps_product_id` int UNSIGNED NOT NULL,
  `old_card_pps_product_id` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_settings`
--

INSERT INTO `product_settings` (`id`, `product_id`, `card_type`, `age`, `amount_of_days`, `rounds`, `pps_product_id`, `new_card_pps_product_id`, `old_card_pps_product_id`, `created_at`, `updated_at`) VALUES
('06b8e3d7-fa73-4999-b908-e067833ce675', '906c9040-3d9f-4517-b16b-5df8115f16bc', 200, 'child', '7', 0, 0, 23901410, 11999288, '2021-02-04 13:45:09', NULL),
('088763cc-a57f-43a0-b806-10a81b53116f', 'faa07c3a-04ef-458e-b4d7-9cfde450718f', 100, 'adult+child', ' ', 1, 21973516, 0, 0, '2021-02-04 12:58:26', NULL),
('314eb1a5-9873-4d40-991a-08b021207949', '93beeaf1-9caa-4029-9ae2-055488d67f74', 200, 'adult', '6', 0, 0, 23901352, 9254608, '2021-02-04 13:43:57', NULL),
('3df6a4c3-c11e-4f64-a4f3-b9c0388fb164', '002c4d13-da17-45ff-8479-e4761fb738dd', 100, 'adult', '0', 0, 21585592, 0, 0, '2021-02-04 14:52:38', NULL),
('3f25af75-9cfd-41c6-9741-1c68e19abf46', 'a4104c10-2746-4cca-9270-0745045acfc9', 200, 'child', '3', 0, 0, 23901205, 9254598, '2021-02-04 13:12:21', NULL),
('44e29765-3e44-46d7-a386-3af034e11970', '573f1a0b-4ab9-4d91-b619-7f4463a44a2a', 200, 'child', '1-evening', 0, 0, 26726915, 13015162, '2021-02-04 13:06:12', NULL),
('486f0e11-a63a-45cc-b3a5-472b97330ca4', '79c4d943-0a47-44ae-b26f-e5e51b24e95f', 200, 'adult', '10', 0, 0, 23901463, 9254584, '2021-02-04 13:49:39', NULL),
('4c8a9d3b-5be5-4b92-b862-1f82a2d19d08', 'fa56c62c-1bbf-4a49-a14e-0068dcb68d86', 200, 'child', '1', 0, 0, 23901174, 4579254, '2021-02-04 13:08:39', NULL),
('4eca0e10-98cf-461e-ab06-7a67d9eecd6d', '3068952b-c667-4c94-9bc1-8db61dc4d44b', 100, 'adult+child', ' ', 3, 24594058, 0, 0, '2021-02-04 13:03:49', NULL),
('59b499b2-a54e-4cd9-ac85-898b39c84287', 'b7ddc11f-31da-44e5-bdb9-23db298dac04', 200, 'child ', '8', 0, 0, 23901452, 9254620, '2021-02-04 13:46:53', NULL),
('5ae14a58-0305-4247-b31f-399aae0632bd', '17ec8006-84b6-41e7-aedf-349b83efcca1', 100, 'child', ' ', 0, 18151789, 0, 0, '2021-02-04 12:54:21', NULL),
('6cd66409-d221-4bbe-b99d-6b6059d7a6c2', 'd958ce8c-9214-44f6-aa4d-59a8cede1d88', 200, 'child', '5', 0, 0, 23901350, 12001799, '2021-02-04 13:15:39', NULL),
('6e25bd12-b545-4d9c-aebb-8e513646d5f9', 'bf4f1e2c-ce9e-448a-83bf-7e1145d2f8de', 300, 'child', '0', 0, 0, 23901498, 0, '2021-02-04 14:57:31', NULL),
('7a53e65d-45fd-460b-b542-1a0e441fa7e2', '9b11d8a4-a2ae-4346-b6f9-83b55353475e', 200, 'adult', '7', 0, 0, 23901370, 11999259, '2021-02-04 13:46:14', NULL),
('7b309ef0-b2d3-4b36-9f1c-48c99e2fa8b0', '4c4f4aae-0a7d-4e36-9484-78c935f07fa4', 200, 'adult', '3', 0, 0, 23901191, 9254596, '2021-02-04 13:13:21', NULL),
('7c2c84f3-1f36-413c-93eb-ddfec3960120', '634aee0e-8a9a-4c55-8efc-95ff06aa948d', 200, 'adult', '1-evening', 0, 0, 26726910, 13015150, '2021-02-04 13:07:19', NULL),
('7dc17b12-1c15-41f8-ad48-d4f7b076a476', '24cb3944-236e-49b6-bb46-f3bfe4dc3676', 300, 'adult', '0', 0, 0, 23901494, 0, '2021-02-04 14:57:00', NULL),
('7e31cba5-4e85-44b7-9ce4-8c25d6e657fa', '347d6a24-758b-4cc3-8900-6ce9d3e1e070', 300, 'adult', '0', 0, 0, 23901505, 0, '2021-02-04 14:55:35', NULL),
('8184b213-1a55-4b6b-9213-0086bc085393', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 100, 'adult', ' ', 1, 21973512, 0, 0, '2021-02-04 12:55:30', NULL),
('886ae138-7d37-4087-bc29-44caccfa183c', '0b6a45d7-6d84-4f9f-b1d2-fbc490c5a60b', 100, 'child', '0', 0, 21585596, 0, 0, '2021-02-04 14:53:31', NULL),
('93524ddc-d3ac-44a5-a0dd-8ef6df384167', 'ab4edbb1-3dc7-40c4-9f82-faed43e34705', 200, 'adult', '5', 0, 0, 2390134, 12001797, '2021-02-04 13:41:39', NULL),
('939e414a-5cd0-4d01-badd-3b36dbd75937', 'b41076b2-0f9d-4ad0-9790-b040f28ea536', 200, 'adult', '8', 0, 0, 23901439, 9254616, '2021-02-04 13:47:57', NULL),
('976ac6ea-0909-4c55-b175-a79df329e7da', '309be581-4dcf-4b33-8d0e-d7b51dd3844c', 300, 'child', '0', 0, 0, 23901514, 0, '2021-02-04 14:56:23', NULL),
('a19cb29d-33e9-4a90-b2fe-c7ade0aae2b0', 'e41cab29-37f3-4e05-916b-64ae00d6e0f5', 200, 'child', '6', 0, 0, 23901359, 9254611, '2021-02-04 13:43:05', NULL),
('ad09de69-b060-4c23-b2a1-b875b718b85d', '98efb9e0-b996-4e44-8c01-887ce9e09da0', 200, 'child', '10', 0, 0, 2390146, 9254590, '2021-02-04 13:48:37', NULL),
('ad2eb540-4e65-4a5b-9273-ce5cd09cc70e', 'fc285538-b95f-4b04-982e-9728852681ec', 100, 'child', '0', 0, 28537345, 0, 0, '2021-02-04 14:54:53', NULL),
('b1c11b79-6fc0-4af3-9f3e-0f34b0f524c6', '16dddc39-6cc6-4a3a-aa5d-db5960673ed6', 100, 'adult', '0', 0, 28537343, 0, 0, '2021-02-04 14:54:08', NULL),
('bb934655-0adf-4694-8d02-df364d40c4e1', '7d9eff11-c45f-493c-8685-0750e5a4eec3', 200, 'adult', '4', 0, 0, 23901244, 12001788, '2021-02-04 13:14:54', NULL),
('cd18ae1c-3ad4-4509-b570-499431723e83', '99f047a1-0331-49e9-aa4e-5a39063b7f2f', 100, 'adult', ' ', 2, 28736046, 0, 0, '2021-02-04 12:59:57', NULL),
('cf765859-55e3-4e53-acb8-8a19c52bb4d8', '153b2fff-4a9f-4289-8434-3b1ed72c8611', 100, 'adult', ' ', 3, 24594051, 0, 0, '2021-02-04 13:02:10', NULL),
('e3fdaf5e-6bd6-4f78-86bd-ed363ce7bc74', '41491d6c-4bdc-430a-99ec-019fc1abf102', 200, 'adult', '1', 0, 0, 23901169, 4579247, '2021-02-04 13:09:41', NULL),
('e489a5ef-017c-4c46-a5ad-b5b343d9f33a', '14a7a65c-77e6-4f9a-9d4a-a21966841ba1', 200, 'child', '2', 0, 0, 23901184, 4579305, '2021-02-04 13:10:46', NULL),
('e8bb25f0-5d75-48cc-bf34-f5556e826f55', 'f840b292-6cac-4b7f-b373-23165726bbde', 200, 'child', '4', 0, 0, 23901253, 12001790, '2021-02-04 13:14:07', NULL),
('eb754e17-e5cf-4b42-80ac-5359fd220439', 'e9a51c5a-6cea-4486-ab2d-36d8b3680fdb', 100, 'adult', ' ', 0, 18151785, 0, 0, '2021-02-04 12:52:15', NULL),
('f05e3ec0-73f2-4c01-8d80-7996fb61aaa7', '0e2760d7-87ae-4477-841c-168ad557850c', 100, 'adult+child', ' ', 2, 28736046, 0, 0, '2021-02-04 13:01:20', NULL),
('fdc47018-30a4-445a-b7bd-15a021953ece', '35f028ec-512b-4ff9-b077-8179c6eef588', 200, 'adult', '2', 0, 0, 23901177, 4579301, '2021-02-04 13:11:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promocode`
--

CREATE TABLE `promocode` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_count` int DEFAULT NULL,
  `holded_count` int DEFAULT NULL,
  `ordered_count` int DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quota`
--

CREATE TABLE `quota` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `is_depends_on_date` smallint UNSIGNED NOT NULL,
  `has_values_by_days` smallint UNSIGNED DEFAULT NULL,
  `sort` int UNSIGNED NOT NULL,
  `is_active` smallint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quota`
--

INSERT INTO `quota` (`id`, `group_id`, `name`, `is_depends_on_date`, `has_values_by_days`, `sort`, `is_active`) VALUES
('189dc276-f10a-47c9-8d56-6a1ef68ac2f6', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', '{\"test\": \"test\"}', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quota_group`
--

CREATE TABLE `quota_group` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `sort` int UNSIGNED NOT NULL,
  `is_active` smallint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quota_group`
--

INSERT INTO `quota_group` (`id`, `name`, `sort`, `is_active`) VALUES
('189dc276-f10a-47c9-8d56-6a1ef68ac2f6', '{\"test\": \"test\"}', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quota_product`
--

CREATE TABLE `quota_product` (
  `quota_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quota_product`
--

INSERT INTO `quota_product` (`quota_id`, `product_id`) VALUES
('189dc276-f10a-47c9-8d56-6a1ef68ac2f6', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6');

-- --------------------------------------------------------

--
-- Table structure for table `quota_value`
--

CREATE TABLE `quota_value` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quota_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_quantity` int NOT NULL,
  `holded_quantity` int NOT NULL,
  `ordered_quantity` int NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quota_value`
--

INSERT INTO `quota_value` (`id`, `quota_id`, `total_quantity`, `holded_quantity`, `ordered_quantity`, `date_from`, `date_to`) VALUES
('189dc276-f10a-47c9-8d56-6a1ef68ac2f6', '189dc276-f10a-47c9-8d56-6a1ef68ac2f6', 15, 0, 0, '2021-03-12', '2021-03-19');

-- --------------------------------------------------------

--
-- Table structure for table `rate_plan`
--

CREATE TABLE `rate_plan` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `description` json NOT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `food_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  `shown_from` date NOT NULL,
  `shown_to` date NOT NULL,
  `is_shown_with_packets` tinyint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rate_plan`
--

INSERT INTO `rate_plan` (`id`, `hotel_id`, `name`, `description`, `group_id`, `food_type_id`, `tax_type_id`, `is_active`, `active_from`, `active_to`, `shown_from`, `shown_to`, `is_shown_with_packets`) VALUES
('18f8de96-6aef-464f-97e1-3c20808b2382', 'fbe3a871-69fe-432e-9a76-d5119bd84c7d', '{\"ru\": \"тариф 5\"}', '{\"ru\": \" описание rate 5\"}', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', 1, '2021-04-27', '2021-04-28', '2021-01-01', '2021-09-30', 1),
('2cc5dc48-4e8b-4870-8215-e6ef4c46e062', '0689aff9-eb6d-48c6-9b14-b1f4423523cf', '{\"ru\": \"тариф 6\"}', '{\"ru\": \" описание rate 6\"}', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', 1, '2021-04-11', '2021-04-17', '2021-01-01', '2021-04-01', 1),
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"rate 2\"}', '{\"ru\": \" описание rate 2\"}', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', 1, '2021-03-29', '2021-04-03', '2021-01-01', '2021-08-01', 1),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"rate 1\"}', '{\"ru\": \" описание rate 1\"}', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', 1, '2021-03-28', '2021-04-03', '2020-12-01', '2021-09-01', 1),
('6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"rate 3\"}', '{\"ru\": \" описание rate 3\"}', '6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', 1, '2021-03-28', '2021-04-01', '2021-03-14', '2021-04-10', 1),
('6fa4ad8d-d0d8-41e9-b3e0-35376b6244c8', '0689aff9-eb6d-48c6-9b14-b1f4423523cf', '{\"ru\": \"rate 3\"}', '{\"ru\": \" описание rate 3\"}', '6582b0da-b8d0-4618-a763-864358706ce1', '18924058-1e6e-4bbc-a785-e5505515f74c', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 1, '2021-04-12', '2021-04-30', '2021-01-01', '2021-09-30', 1),
('8cda5b04-7f83-4b76-a19f-4c129d88770f', '9e889411-b12f-45c0-93ef-05ecd58e900d', '{\"ru\": \"тариф 2\"}', '{\"ru\": \" описание тариф 2\"}', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '6582b0da-b8d0-4618-a763-864358706ce1', 1, '2021-04-05', '2021-04-10', '2021-04-02', '2021-04-29', NULL),
('9fe8e83a-67d3-4a49-84db-79474a1df4ca', 'f09bf7ed-2a7d-4da5-8703-05a52f8b1ceb', '{\"ru\": \"тариф 7\"}', '{\"ru\": \" описание rate 7\"}', '6582b0da-b8d0-4618-a763-864358706ce1', 'b15b4686-aa1b-4606-97ca-6f241554bd82', '33be2c1d-3a56-45c9-9540-57763103f912', 1, '2021-03-16', '2021-04-17', '2021-01-01', '2021-04-01', 1),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"rate 4\"}', '{\"ru\": \" описание rate 4\"}', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 1, '2021-04-12', '2021-04-22', '2021-03-25', '2021-05-13', 1),
('b6b8e477-f382-4a6f-af87-8fa0bded3a75', '31fa0a0f-fbcc-444c-90de-283431a8c980', '{\"ru\": \"тариф 7\"}', '{\"ru\": \" описание rate 7\"}', 'b15b4686-aa1b-4606-97ca-6f241554bd82', '6582b0da-b8d0-4618-a763-864358706ce1', '18924058-1e6e-4bbc-a785-e5505515f74c', 1, '2021-02-01', '2021-06-30', '2021-01-01', '2021-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rate_plans`
--

CREATE TABLE `rate_plans` (
  `id` int NOT NULL,
  `hotel_id` int DEFAULT NULL,
  `name` json DEFAULT NULL,
  `groupId` int DEFAULT NULL,
  `foodTypeId` int DEFAULT NULL,
  `taxTypeId` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `activeFrom` date DEFAULT NULL,
  `activeTo` date DEFAULT NULL,
  `shownFrom` date DEFAULT NULL,
  `shownTo` date DEFAULT NULL,
  `isShownWithPackets` tinyint(1) DEFAULT NULL,
  `isShownWithoutPackets` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rate_plans_rooms`
--

CREATE TABLE `rate_plans_rooms` (
  `id` int NOT NULL,
  `ratePlanId` int DEFAULT NULL,
  `roomId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rate_plan_group`
--

CREATE TABLE `rate_plan_group` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `sort` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rate_plan_group`
--

INSERT INTO `rate_plan_group` (`id`, `name`, `sort`) VALUES
('18924058-1e6e-4bbc-a785-e5505515f74c', '{\"ru\": \"rate plan 5\"}', 1),
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"rate plan 2\"}', 1),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"rate plan 1\"}', 1),
('6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"rate plan 3\"}', 1),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"rate plan 4\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rate_plan_groups`
--

CREATE TABLE `rate_plan_groups` (
  `id` int NOT NULL,
  `name` json DEFAULT NULL,
  `sort` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rate_plan_price`
--

CREATE TABLE `rate_plan_price` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate_plan_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_place_value` int UNSIGNED DEFAULT NULL,
  `hotel_extra_place_type_age_range_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `value` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rate_plan_price`
--

INSERT INTO `rate_plan_price` (`id`, `rate_plan_id`, `room_id`, `main_place_value`, `hotel_extra_place_type_age_range_id`, `date`, `value`) VALUES
('0a3f05ad-f364-4b20-8255-da69a2595f7a', '2cc5dc48-4e8b-4870-8215-e6ef4c46e062', '33be2c7d-3a56-45c9-9540-57763103f912', 1500, '33be2c1d-3a56-45c9-9540-57763103f912', '2021-04-06', 4000),
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', 2000, '33be2c1d-3a56-45c9-9540-57763103f912', '2021-03-30', 700),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', 1000, '33be2c7d-3a56-45c9-9540-57763103f912', '2021-03-30', 500),
('394fc87b-c23e-4824-a046-07c02e6ec230', '18f8de96-6aef-464f-97e1-3c20808b2382', '33be2c1d-3a56-45c9-9540-57763103f912', 3000, '33be2c7d-3a56-45c9-9540-57763103f912', '2021-04-05', 500),
('7a7febf3-d1f1-4643-98ae-55809f8ae287', '9fe8e83a-67d3-4a49-84db-79474a1df4ca', '6582b0da-b8d0-4618-a763-864358706ce1', 1000, '33be2c1d-3a56-45c9-9540-57763103f912', '2021-04-11', 700),
('a4ffa5a1-7208-4361-b6a7-7a54909a4f87', '8cda5b04-7f83-4b76-a19f-4c129d88770f', '33be2c7d-3a56-45c9-9540-57763103f912', 800, '33be2c1d-3a56-45c9-9540-57763103f912', '2021-04-13', 1300);

-- --------------------------------------------------------

--
-- Table structure for table `rate_plan_room`
--

CREATE TABLE `rate_plan_room` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate_plan_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rate_plan_room`
--

INSERT INTO `rate_plan_room` (`id`, `rate_plan_id`, `room_id`) VALUES
('2cc5dc48-4e8b-4870-8215-e6ef4c46e062', '2cc5dc48-4e8b-4870-8215-e6ef4c46e062', '33be2c7d-3a56-45c9-9540-57763103f912'),
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912'),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912'),
('6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1'),
('8cda5b04-7f83-4b76-a19f-4c129d88770f', '8cda5b04-7f83-4b76-a19f-4c129d88770f', '33be2c1d-3a56-45c9-9540-57763103f912'),
('ac69268b-a773-4de8-b117-137b4a0d3eb1', '9fe8e83a-67d3-4a49-84db-79474a1df4ca', '33be2c7d-3a56-45c9-9540-57763103f912'),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82'),
('e8f8de96-6aef-464f-97e1-3c20808b2382', '6fa4ad8d-d0d8-41e9-b3e0-35376b6244c8', '33be2c1d-3a56-45c9-9540-57763103f912');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `payment_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `provider` smallint UNSIGNED NOT NULL,
  `external_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `external_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `payment_id`, `provider`, `external_id`, `external_url`, `status`, `created_at`) VALUES
('1e60851f-9fa8-41d3-ae43-29b477beb90f', '8489e630-7209-4dce-8468-059520191f9e', 100, '1416a54a-2c92-45a3-a528-0fbb5f5f8d1c', NULL, 100, '2021-03-17 18:58:49'),
('5166d875-a7da-4be4-a985-3d283e7f6416', '069aaa3e-a753-47c7-83da-5984f4628112', 100, '39418c50-b03d-4e9f-b4fa-ba9a477d57dd', 'https://lk.platformaofd.ru/web/noauth/cheque?fn=9999078900006190&fp=1272833149&i=3335', 100, '2021-03-18 11:49:53'),
('779e3e04-13e1-4351-84ea-f5f8ba154d9e', 'f4a52e37-a0ef-43c2-9f06-3ff123cc4e78', 100, '96e58cc1-818a-4131-92db-f47896e8d07a', NULL, 100, '2021-03-17 11:11:27'),
('939f47c0-d53b-494e-a00f-656c10a4dcf3', 'a66a9458-0c36-4542-9d3a-e33db04791fb', 100, 'c73d2625-2bca-4179-95d7-89186b8bced5', 'https://lk.platformaofd.ru/web/noauth/cheque?fn=9999078900006286&fp=692047610&i=10231', 100, '2021-03-15 05:44:53'),
('aa81b998-640e-4697-b51f-d81d17db6fb6', '8efa5b9c-49ed-4bb1-ab33-073c5e2d1fb8', 100, '93c8b47f-85d6-4bcc-b4a2-05d025ddde4c', NULL, 100, '2021-03-17 13:02:15'),
('e74388a9-8b0b-429a-8fd9-d045186f6b6a', 'a3682b46-4ce5-4d93-834f-238733be9844', 100, '13ba8666-beb4-43f0-9b55-ac20c1c80dfc', NULL, 100, '2021-03-17 15:53:10'),
('f2dda830-c51b-4a74-8562-70331e55390e', 'f0fecc78-2000-4384-b3af-87ebe7c1461e', 100, '5bdb706a-4cc0-4d22-a681-23d5a5ddec10', NULL, 100, '2021-03-18 11:52:43');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `area` int DEFAULT NULL,
  `main_place_count` int DEFAULT NULL,
  `extra_place_count` int DEFAULT NULL,
  `zero_place_count` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `hotel_id`, `name`, `description`, `area`, `main_place_count`, `extra_place_count`, `zero_place_count`) VALUES
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"семейный\"}', '{\"ru\": \" описание комнаты 2\"}', 40, 2, 2, 2),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"стандартный\"}', '{\"ru\": \" описание комнаты 1\"}', 20, 1, 1, 1),
('6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"luxe\"}', '{\"ru\": \" описание комнаты 3\"}', 30, 3, 3, 3),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"комната 4\"}', '{\"ru\": \" описание комнаты 4\"}', 50, 4, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `room_facility`
--

CREATE TABLE `room_facility` (
  `room_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facility_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `room_main_place_variant`
--

CREATE TABLE `room_main_place_variant` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_main_place_variant`
--

INSERT INTO `room_main_place_variant` (`id`, `room_id`, `value`) VALUES
('33be2c1d-3a56-45c9-9540-57763103f912', '33be2c1d-3a56-45c9-9540-57763103f912', 2),
('33be2c7d-3a56-45c9-9540-57763103f912', '33be2c7d-3a56-45c9-9540-57763103f912', 1),
('6582b0da-b8d0-4618-a763-864358706ce1', '6582b0da-b8d0-4618-a763-864358706ce1', 3),
('b15b4686-aa1b-4606-97ca-6f241554bd82', 'b15b4686-aa1b-4606-97ca-6f241554bd82', 4);

-- --------------------------------------------------------

--
-- Table structure for table `room_photo`
--

CREATE TABLE `room_photo` (
  `room_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `room_video`
--

CREATE TABLE `room_video` (
  `room_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json DEFAULT NULL,
  `description` json DEFAULT NULL,
  `tariff_description` json DEFAULT NULL,
  `inner_title` json DEFAULT NULL,
  `inner_description` json DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `picture_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_icon_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_background_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_icon_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_background_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommendation_sort` int UNSIGNED NOT NULL,
  `is_active` smallint UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `code`, `name`, `description`, `tariff_description`, `inner_title`, `inner_description`, `created_at`, `updated_at`, `picture_id`, `web_icon_id`, `web_background_id`, `mobile_icon_id`, `mobile_background_id`, `recommendation_sort`, `is_active`) VALUES
('33be5c7d-3a56-45c9-9540-57763103f912', 'summer_seasonal_walking_ticket', '{\"ru\": \"Летние прогулочные билеты\"}', '{\"ru\": \"Прогулочный билет на всё лето\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Летние прогулочные билеты\"}', '{\"ru\": \"Прогулочный билет на всё лето\"}', '2021-02-03 15:59:10', NULL, NULL, NULL, '33be5c7d-3a56-45c9-9540-57763103f912', NULL, '33be5c7d-3a56-45c9-9540-57763103f912', 0, 1),
('3b560e37-944f-4517-aaad-97daa5c447df', 'museum', '{\"ru\": \"Музей Археологии\"}', '{\"ru\": \"Этнопарк Моя Россия на Роза Хутор приглашает в 11 павильонов, посвященных регионам нашей страны. Это Россия в миниатюре, с тематическими развлечениями, мастер-классами и представлениями. Как в машине времени вы совершите увлекательное путешествие в прошлое, оживающее с помощью самых современных технологий.\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Музей Археологии\"}', '{\"ru\": \"Этнопарк Моя Россия на Роза Хутор приглашает в 11 павильонов, посвященных регионам нашей страны. Это Россия в миниатюре, с тематическими развлечениями, мастер-классами и представлениями. Как в машине времени вы совершите увлекательное путешествие в прошлое, оживающее с помощью самых современных технологий.\"}', '2021-02-03 15:33:44', NULL, NULL, NULL, '3b560e37-944f-4517-aaad-97daa5c447df', NULL, '3b560e37-944f-4517-aaad-97daa5c447df', 0, 1),
('71fda3ad-bb21-49bc-81fd-df2ca9512fc1', 'seasonal_skipass', '{\"ru\": \"Сезонный ски-пасс\"}', '{\"ru\": \"Описание\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Сезонный ски-пасс\"}', '{\"ru\": \"Описание\"}', '2021-02-03 16:02:00', NULL, NULL, NULL, '71fda3ad-bb21-49bc-81fd-df2ca9512fc1', NULL, '71fda3ad-bb21-49bc-81fd-df2ca9512fc1', 0, 1),
('8d26384a-5098-4f8a-b3d8-d9a8832e4747', 'rodelban', '{\"ru\": \"Билеты на родельбан\"}', '{\"ru\": \"Аттракцион находится в Горной олимпийской деревне (1100 м) за рестораном \\\"Берлога\\\". Доехать до аттракциона можно на канатной дороге Олимпия или по автодороге до Горной Олимпийской деревни.\"}', '{\"ru\": \"В билет не входит подъем на канатных дорогах. Билет действителен в течение 7 дней с даты, указанной при оформлении, но не позднее 15 ноября 2020.\"}', '{\"ru\": \"Билеты на родельбан\"}', '{\"ru\": \"Аттракцион находится в Горной олимпийской деревне (1100 м) за рестораном \\\"Берлога\\\". Доехать до аттракциона можно на канатной дороге Олимпия или по автодороге до Горной Олимпийской деревни.\"}', '2021-02-03 15:55:44', NULL, NULL, NULL, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', NULL, '8d26384a-5098-4f8a-b3d8-d9a8832e4747', 0, 1),
('96d76c1f-128b-44b9-b0a0-3558b61b2ddd', 'annual_skipass', '{\"ru\": \"Годовой ски-пасс\"}', '{\"ru\": \"Описание\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Годовой ски-пасс\"}', '{\"ru\": \"Описание\"}', '2021-02-03 16:06:42', NULL, NULL, NULL, '96d76c1f-128b-44b9-b0a0-3558b61b2ddd', NULL, '96d76c1f-128b-44b9-b0a0-3558b61b2ddd', 0, 1),
('a4741b9e-e677-4fb1-8187-84ff8b52e23d', 'yeti_park', '{\"ru\": \"Йети Парк\"}', '{\"ru\": \"Самый высокогорный семейный парк развлечений «Йети Парк» на «Роза Хутор». Здесь можно увидеть следы самого Йети, посидеть на его любимом стуле, отыскать сокровища на золотом прииске, а самые смелые смогут испытать свои силы в веревочном городке «Йети Джунгли». В летнем сезоне 2020 года работает до 8 ноября включительно.\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Йети Парк\"}', '{\"ru\": \"Самый высокогорный семейный парк развлечений «Йети Парк» на «Роза Хутор». Здесь можно увидеть следы самого Йети, посидеть на его любимом стуле, отыскать сокровища на золотом прииске, а самые смелые смогут испытать свои силы в веревочном городке «Йети Джунгли». В летнем сезоне 2020 года работает до 8 ноября включительно.\"}', '2021-02-03 15:58:11', NULL, NULL, NULL, 'a4741b9e-e677-4fb1-8187-84ff8b52e23d', NULL, 'a4741b9e-e677-4fb1-8187-84ff8b52e23d', 0, 1),
('ad9c6903-7ec8-4da1-b1e0-69a96b39e7fb', 'day_on_rosa_walking_ticket', '{\"ru\": \"Прогулочные \\\"День на Роза Хутор\\\"\"}', '{\"ru\": \"Билет с неограниченным доступом на канатные дороги Олимпия, Заповедный Лес, Кавказский экспресс, Волчья скала, Эдельвейс, Крокус в течение 1 дня, при условии их открытия по погоде. Абонемент также включает комплексный обед в буфет-баре Берлога (Горная Олимпийская Деревня, 1100 м), и прокат велосипеда (1 час) в пункте проката в Роза Долине (ул. Олимпийская, 35).\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Прогулочные \\\"День на Роза Хутор\\\"\"}', '{\"ru\": \"Билет с неограниченным доступом на канатные дороги Олимпия, Заповедный Лес, Кавказский экспресс, Волчья скала, Эдельвейс, Крокус в течение 1 дня, при условии их открытия по погоде. Абонемент также включает комплексный обед в буфет-баре Берлога (Горная Олимпийская Деревня, 1100 м), и прокат велосипеда (1 час) в пункте проката в Роза Долине (ул. Олимпийская, 35).\"}', '2021-02-03 16:00:09', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
('ae74e939-8259-48e8-9992-9ad030d347a2', 'ethno_complex', '{\"ru\": \"Билеты в этнокомплекс Моя Россия\"}', '{\"ru\": \"Этнопарк Моя Россия  на Роза Хутор приглашает в 11 павильонов, посвященных регионам нашей страны. Это Россия в миниатюре, с тематическими развлечениями, мастер-классами и представлениями.\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Билеты в этнокомплекс Моя Россия\"}', '{\"ru\": \"Этнопарк Моя Россия  на Роза Хутор приглашает в 11 павильонов, посвященных регионам нашей страны. Это Россия в миниатюре, с тематическими развлечениями, мастер-классами и представлениями.\"}', '2021-02-03 15:56:51', NULL, NULL, NULL, 'ae74e939-8259-48e8-9992-9ad030d347a2', NULL, 'ae74e939-8259-48e8-9992-9ad030d347a2', 0, 1),
('b212cc4e-03b2-47df-9c3c-95bda13eee0f', 'morning_walking_ticket', '{\"ru\": \"Утро на Пике\"}', '{\"ru\": \"Полюбоваться рассветом вы можете на любом уровне курорта Роза Хутор, но именно с высоты 2320 метров можно увидеть красоту, доступную немногим.\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Утро на Пике\"}', '{\"ru\": \"Полюбоваться рассветом вы можете на любом уровне курорта Роза Хутор, но именно с высоты 2320 метров можно увидеть красоту, доступную немногим.\"}', '2021-02-03 16:01:15', NULL, NULL, NULL, 'b212cc4e-03b2-47df-9c3c-95bda13eee0f', NULL, 'b212cc4e-03b2-47df-9c3c-95bda13eee0f', 0, 1),
('f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 'skipass', '{\"ru\": \"Скипассы\"}', '{\"ru\": \"Приобретайте скипасы на несколько дней\"}', '{\"ru\": \"Описание тарифа\"}', '{\"ru\": \"Скипассы\"}', '{\"ru\": \"Приобретайте скипасы на несколько дней\"}', '2021-02-03 16:05:47', NULL, NULL, NULL, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', NULL, 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `section_settings`
--

CREATE TABLE `section_settings` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `section_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `controls` json NOT NULL,
  `is_online_only` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `is_activation_date_required` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `active_days` smallint UNSIGNED NOT NULL,
  `is_not_quantitative` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section_settings`
--

INSERT INTO `section_settings` (`id`, `section_id`, `controls`, `is_online_only`, `is_activation_date_required`, `active_days`, `is_not_quantitative`, `created_at`, `updated_at`) VALUES
('3a717ede-f005-46e0-a755-b36c746f007a', 'a4741b9e-e677-4fb1-8187-84ff8b52e23d', '[{\"name\": \"activationDate\", \"type\": \"date\", \"label\": {\"en\": \"Date\", \"ru\": \"Дата посещения\"}}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 1, 0, 0, '2021-02-10 11:40:54', NULL),
('42123ad1-786b-4f21-b09f-39fc83515a2b', 'f26c42cf-f8b0-40db-a537-fbb2e2ad45c9', '[{\"name\": \"activationDate\", \"type\": \"date\", \"label\": {\"en\": \"Date\", \"ru\": \"Дата посещения\"}}, {\"name\": \"amountOfDays\", \"type\": \"droplist\", \"label\": {\"en\": \"Amount of days\", \"ru\": \"Количество дней\"}, \"values\": [{\"key\": \"1-evening\", \"value\": {\"en\": \"1 day (evening)\", \"ru\": \"1 день (вечер)\"}}, {\"key\": \"1\", \"value\": {\"en\": \"2 days\", \"ru\": \"1 день\"}}, {\"key\": \"2\", \"value\": {\"en\": \"2 days\", \"ru\": \"2 дня\"}}, {\"key\": \"3\", \"value\": {\"en\": \"3 days out of 4\", \"ru\": \"3 дня из 4\"}}, {\"key\": \"4\", \"value\": {\"en\": \"4 days out of 5\", \"ru\": \"4 дня из 5\"}}, {\"key\": \"5\", \"value\": {\"en\": \"5 days out of 6\", \"ru\": \"5 дней из 6\"}}, {\"key\": \"6\", \"value\": {\"en\": \"6 days out of 8\", \"ru\": \"6 дней из 8\"}}, {\"key\": \"7\", \"value\": {\"en\": \"7 days out of 9\", \"ru\": \"7 дней из 9\"}}, {\"key\": \"8\", \"value\": {\"en\": \"8 days out of 11\", \"ru\": \"8 дней из 11\"}}, {\"key\": \"10\", \"value\": {\"en\": \"10 days out of 14\", \"ru\": \"10 дей из 14\"}}]}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 1, 0, 1, '2021-02-10 11:45:03', NULL),
('54004510-310c-43cc-a2c8-e05da9aca28b', '71fda3ad-bb21-49bc-81fd-df2ca9512fc1', '[{\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 0, 0, 1, '2021-02-10 11:44:28', NULL),
('5d33d0be-e33b-4859-a440-44c67dec0c3f', '33be5c7d-3a56-45c9-9540-57763103f912', '[{\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 0, 0, 0, '2021-02-10 11:42:38', NULL),
('702fb494-2c18-4d3a-9991-19605de96daf', 'b212cc4e-03b2-47df-9c3c-95bda13eee0f', '[{\"name\": \"activationDate\", \"type\": \"date\", \"label\": {\"en\": \"Date\", \"ru\": \"Дата посещения\"}}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 1, 0, 0, '2021-02-10 11:43:46', NULL),
('78e61fe2-0047-40ac-ba2c-0b1bb0d1d66e', '96d76c1f-128b-44b9-b0a0-3558b61b2ddd', '[{\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 0, 0, 1, '2021-02-10 11:46:07', NULL),
('79143073-a537-46f3-9ce6-bc5dd7db0606', '8d26384a-5098-4f8a-b3d8-d9a8832e4747', '[{\"name\": \"activationDate\", \"type\": \"date\", \"label\": {\"en\": \"Date\", \"ru\": \"Дата посещения\"}}, {\"name\": \"rounds\", \"type\": \"droplist\", \"label\": {\"en\": \"Rounds\", \"ru\": \"Количество кругов\"}, \"values\": [{\"key\": \"1\", \"value\": {\"en\": \"1 round\", \"ru\": \"1 круг\"}}, {\"key\": \"2\", \"value\": {\"en\": \"2 rounds\", \"ru\": \"2 круга\"}}, {\"key\": \"3\", \"value\": {\"en\": \"3 rounds\", \"ru\": \"3 круга\"}}]}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult + child\", \"ru\": \"Взрослый + детский\"}, \"valueKey\": \"adult+child\"}]', 1, 1, 0, 0, '2021-02-10 11:39:24', NULL),
('86595c21-2e17-447c-ac47-864cda6332b2', 'ad9c6903-7ec8-4da1-b1e0-69a96b39e7fb', '[{\"name\": \"activationDate\", \"type\": \"date\", \"label\": {\"en\": \"Date\", \"ru\": \"Дата посещения\"}}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 1, 0, 0, '2021-02-10 11:43:08', NULL),
('c3ffff4f-f4a9-4baf-97ce-85e5b3f58741', '3b560e37-944f-4517-aaad-97daa5c447df', '[{\"name\": \"activationDate\", \"type\": \"date\", \"label\": {\"en\": \"Date\", \"ru\": \"Дата посещения\"}}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 1, 7, 0, '2021-02-10 11:39:03', NULL),
('ffa233eb-61e2-426f-a593-704db33f601b', 'ae74e939-8259-48e8-9992-9ad030d347a2', '[{\"name\": \"activationDate\", \"type\": \"date\", \"label\": {\"en\": \"Date\", \"ru\": \"Дата посещения\"}}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Adult\", \"ru\": \"Взрослый\"}, \"valueKey\": \"adult\"}, {\"max\": 20, \"min\": 1, \"name\": \"age\", \"type\": \"quantity\", \"label\": {\"en\": \"Child\", \"ru\": \"Детский\"}, \"valueKey\": \"child\"}]', 0, 1, 0, 0, '2021-02-10 11:40:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE `storage` (
  `id` int NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `subdir` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `icon` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_svg` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`, `icon`, `icon_svg`) VALUES
('06d8be64-2109-437d-9fc1-2d8bcf9dc424', '{\"ru\": \"рекомендуем для семей с детьми\"}', NULL, NULL),
('672bf553-10e6-4e70-9ac6-da56248847f7', '{\"ru\": \"близко к подъемникам\"}', NULL, NULL),
('c54dbea9-9d9d-4247-a106-d60fc9aa7541', '{\"ru\": \"активный отдых\"}', NULL, NULL),
('d0a0caf9-712b-4792-bbff-65d4ad7aa60f', '{\"ru\": \"рекомендуем для бизнес встреч\"}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tax_type`
--

CREATE TABLE `tax_type` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` json NOT NULL,
  `value` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tax_type`
--

INSERT INTO `tax_type` (`id`, `name`, `value`) VALUES
('18924058-1e6e-4bbc-a785-e5505515f74c', '{\"ru\": \"tax type 5\"}', 1),
('33be2c1d-3a56-45c9-9540-57763103f912', '{\"ru\": \"tax type 2\"}', 1),
('33be2c7d-3a56-45c9-9540-57763103f912', '{\"ru\": \"tax type 1\"}', 1),
('6582b0da-b8d0-4618-a763-864358706ce1', '{\"ru\": \"tax type 3\"}', 1),
('b15b4686-aa1b-4606-97ca-6f241554bd82', '{\"ru\": \"tax type 4\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_types`
--

CREATE TABLE `tax_types` (
  `id` int NOT NULL,
  `value` int DEFAULT NULL,
  `name` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `role` smallint UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_hash` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
  `sex` smallint UNSIGNED DEFAULT NULL,
  `status` smallint UNSIGNED NOT NULL,
  `reset_password_token_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_token_expired_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_confirmed_service_letters` smallint UNSIGNED NOT NULL DEFAULT '0',
  `is_confirmed_mailing` smallint UNSIGNED NOT NULL DEFAULT '0',
  `is_email_verified` smallint UNSIGNED NOT NULL DEFAULT '0',
  `verify_email_token_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify_email_token_expired_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_phone_verified` smallint UNSIGNED NOT NULL DEFAULT '0',
  `verify_phone_token_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify_phone_token_expired_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `level_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role`, `email`, `phone`, `password_hash`, `last_name`, `first_name`, `middle_name`, `birth_date`, `sex`, `status`, `reset_password_token_value`, `reset_password_token_expired_at`, `is_confirmed_service_letters`, `is_confirmed_mailing`, `is_email_verified`, `verify_email_token_value`, `verify_email_token_expired_at`, `is_phone_verified`, `verify_phone_token_value`, `verify_phone_token_expired_at`, `created_at`, `updated_at`, `level_id`) VALUES
('035ab59f-743c-4252-ba9b-13eabefde20d', 100, 'zlobin@zlobin.zlo', '95481218446614', '$argon2i$v=19$m=65536,t=4,p=1$enliWWN3LjdjczFHT1BBMg$3vqyco59NKxWD44XhCY3P3D10JyamRIgLXqbryMfhgQ', 'Злобин', 'Злобин', 'Сергеевич', '2021-03-17', 100, 100, NULL, NULL, 0, 0, 0, 'de672fd0-317e-4eb0-a296-f2b8e9c3009e', '2021-03-25 10:41:07', 0, NULL, NULL, '2021-03-24 10:41:07', '2021-03-24 10:41:07', NULL),
('040a1a61-7338-41e9-b954-5b0248223619', 100, 'sky1d2o1ba@mail.ru', '9013вы24', '$argon2i$v=19$m=65536,t=4,p=1$VmVZeUhWQ3Q0enVTem5zTA$tsmPpqJeN+yf4Jt6Oc8gEiZ0vAWVPHy7Xptk3H4uBzI', 'Таня', 'Таня', 'string', '2021-02-21', 100, 100, NULL, NULL, 1, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 17:46:04', NULL, NULL),
('046b18eb-4a09-49e1-ac67-b3e673bfb161', 100, 'test@trewer.rur', 're34234324', '$argon2i$v=19$m=65536,t=4,p=1$MVVJSnF0ZnVMR29sRVpjMw$q1wAos1V5mRDGxCpPvCwSPqpc75IGIlS6oJKeqp7kM0', 'test', 'test', 'test', '2021-03-17', 100, 100, NULL, NULL, 0, 0, 0, 'db8643c4-bd24-4f4d-b0db-5f9fa0d15732', '2021-03-26 10:51:31', 0, NULL, NULL, '2021-03-25 10:51:30', '2021-03-25 10:51:31', NULL),
('07dc0bcb-89d6-4b3b-b4f4-ba5ae3f094c6', 100, '90.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$SHNPV1VoU3UueEwzN2p3Tg$SZc7FXp9PnvcCe6o6kUsFOarkNfN1X5ebvjRrtxhTyo', 'Демьянов', 'Демьян', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 11:59:31', NULL, NULL),
('07fafaa0-86f5-43ce-8007-12ec17d75c1f', 100, '312312@32312.ru', '23123123123', '$argon2i$v=19$m=65536,t=4,p=1$MGdWZThYamxOdk9YcFpaeg$1hGvChUGHIBe3rgylo5XQVsKbwfIeAqPz3OBch83zOU', '12312312', '12312312', '3123', '2021-03-18', 100, 100, NULL, NULL, 0, 0, 0, 'af0a8075-93fc-4a95-a082-97ee56341e36', '2021-03-23 07:16:04', 0, NULL, NULL, '2021-03-22 07:16:04', '2021-03-22 07:16:04', NULL),
('0b8532fa-daef-467d-bd6c-08447607f55a', 100, 'test345@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bGpVUXgycGJlOUpERXMvWQ$dWTCm2AWZyG0pqW3SStyVPS6I3Sz6vdb1UFkybMuSdk', 'Иванов', 'ИВан', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-20 14:14:56', NULL, NULL),
('0ebba7a5-f04d-4ebb-a155-6fe14622376b', 100, 'test@test.ts', '0000000000000', '$argon2i$v=19$m=65536,t=4,p=1$UHYvRzdLRkxTWXB4NkxqYw$2Ifff2NcOlbvwAVtYWlfGgW6pdcPuDeFOSessFno5w8', 'лоплол', 'лолоа', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-11 11:58:47', NULL, NULL),
('1155e9ae-7309-4ec8-be0b-4f244e4c550e', 100, 'dzfrfrfr@rt.ru', '1211244444', '$argon2i$v=19$m=65536,t=4,p=1$NERXcEdDc05YQzRURlptWQ$a9cuQzi3OV02YzbSbmoYfWjax3F2nCUBrINEaHgCS/c', '121212', '121212', '121212', '2021-02-01', 200, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 14:32:20', NULL, NULL),
('12a66a06-a4a5-49a4-8d41-a24bd9e8b0c2', 100, '97.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$QmxWejRVcE1XbU1qMWsvLg$KlP4WsjDWiTHcgJNHuJFVMNoSGZcGa1EBBr1lkmPdAg', 'Максимов', 'Максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 13:48:04', '2021-03-02 13:48:10', NULL),
('137ae7c6-01e2-4450-934a-1505b59d2f1a', 100, 'Super@Super.Super', '33', '$argon2i$v=19$m=65536,t=4,p=1$czQ0Rzh6VG1EcjE5dGdodg$P/UwQcmhiSoFNOIBzNIdW9oSzP0ej1t1Wo6qhbfceOg', 'Super', 'Super', 'Super', '2021-03-04', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-13 19:06:15', NULL, NULL),
('168041e5-b5d8-446d-aac0-a4507c5e30f1', 100, '72.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$VEc2djBSNzEwYnlMa0JrYw$/ym8LhE3coClfe19rZJOoVnwn1EsTQOKquiJ/x5xJyk', 'Максименко', 'Максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 11:53:43', NULL, NULL),
('16992a4c-cd47-4731-b483-ea08cc6d6ea8', 100, '142.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$YW82RWdEZDNYcFlBMjJOcQ$ymHzVe4YhcQT6CQrSpkBsHc+vghZlG0KWrbUAHVkESc', 'Олегов', 'Олег', NULL, NULL, NULL, 200, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-17 08:56:27', '2021-03-17 12:54:24', NULL),
('17ea6f61-a17b-4a59-8e26-1a763510db4a', 100, 'lajdfs@ldkajf.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$eExtTFNoSXdrTlpIS2QyOA$19g7VAzbU6QzYbYaN358S+TzkMUmDmWoKf7EQpABwOM', 'lajdfs', 'lajdfs', NULL, '2000-01-15', NULL, 100, NULL, NULL, 0, 0, 0, 'f244081d-56c7-483c-b45e-69cf81392597', '2021-04-10 11:19:29', 0, NULL, NULL, '2021-04-09 11:19:29', '2021-04-09 11:19:29', NULL),
('188132e7-8e77-4561-96e7-f2ef1c04cc84', 100, 'skyd2o1ba@mail.ru', '90124', '$argon2i$v=19$m=65536,t=4,p=1$MVNHUUxJRS95a3FUUC5NOA$IeV7mFI43gAfpEjSK/USgYwvJbPOE1eT9AiHi36bkng', 'Таня', 'Таня', 'string', '2000-02-02', 100, 100, NULL, NULL, 1, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 17:39:28', NULL, NULL),
('1890ddef-d30b-481d-a6f0-bbf5a29b170f', 100, 'asfdljk@sdjf.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Zm1VRy5POWFkUHNTSnR0aQ$cOq5BfI5rpVtkMmYIdGOFdChoOPTAdzhhJYtPcVUdBA', 'dsflkj', 'dsflkj', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-01 08:50:14', NULL, NULL),
('1937468c-ebbe-4990-a339-c85296ee1cdd', 100, 'tetst@stest.ru', '79517715651', '$argon2i$v=19$m=65536,t=4,p=1$dHJPZVFEdlltbkpOS0o4eA$fjJtCwmVccmC+AtUt/3lZJNraH+uplb6ZO2Pf4o1pGY', 'sdf', 'sdf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 1, NULL, NULL, '2021-03-12 13:55:41', '2021-03-12 14:02:20', NULL),
('19f182d9-2dd1-49d9-88ef-19979cf49e99', 100, '129.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$MDIzem1ML3RIdWNxUGY1MQ$EpZbSF9rz2aen6Zrg3quy/M7l+Mtz2IbtUWJ0BBvrzk', 'Маратов', 'Марат', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-12 13:17:50', NULL, NULL),
('1dadace1-d808-42e0-9e9d-2ce3c3021163', 100, '78.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$QVIuTDZDZ29qQWhSN2hIVg$tHWS6tObgA/VMZIOTXCcYxgzkfUK44/t9mEIqG4rURc', 'Оков', 'Оков', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 06:20:03', NULL, NULL),
('1e628710-d9e1-463c-a0d5-9123a429951d', 100, 'nastya.mityanina@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bXhmdlptN05DZ3hhLk01RA$tEfvhHawqcX/c4N6jU6ic6BB4cI89UwoQDgIm6EeqUs', 'Митянина', 'Митянина', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 11:06:26', NULL, NULL),
('1ec92e41-d359-4ce2-8977-0a652e0d8466', 200, 'zakharov@dreamheads.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$ZkRCYVlZcFlTdmd3c2kuVA$azdQ5g3DDEvFDIx4QrOfD+Gd3rZ8tCe2E7endNdlfVY', 'zakharov', 'zakharov', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-11 11:09:20', NULL, NULL),
('1f598387-41aa-4642-aeef-fcf552263a10', 100, 'Redozubova_NB@rosaski.com', '6666666', '$argon2i$v=19$m=65536,t=4,p=1$Y2s0aktScDc4YVNBUnFvag$04nFs5enbhSMGIVGkuxAVeUH49R6PgT/vAbn87ZaEcI', 'Редозубова', 'Редозубова', 'Борисовна', '1989-05-23', 200, 200, '608b4ebc-c9ef-4b6c-9fa4-75b145674779', '2021-03-10 05:40:23', 0, 0, 0, '6941d35a-3db4-4cd9-8202-229c664bbca2', '2021-03-10 06:18:53', 0, 'e521eba4-f65f-455c-b64e-18711011f3c7', '2021-03-10 06:15:36', '2021-03-09 05:34:47', '2021-03-09 06:22:21', NULL),
('20594f35-038c-4aaa-8f6b-6c881fd17b6e', 100, 'test2@test.ru', '12333', '$argon2i$v=19$m=65536,t=4,p=1$NEVzaTdwWkMyMWZpYVZoeg$T8DQiXkPN2OvSFB6JoLEh0/jS6vBB2h6/aIkcfvNNFE', 'test', 'test', 'test', '2021-03-17', NULL, 100, NULL, NULL, 0, 0, 0, '4be2e5fe-018c-4712-be79-5f6a4d7e0c21', '2021-03-23 07:09:48', 0, NULL, NULL, '2021-03-22 07:09:48', '2021-03-22 07:09:48', NULL),
('21f805b2-7b7c-4509-8638-66a0b6d0e1ac', 100, '64.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$UHBJWnplUFFoNjVoUjF5ZA$rLYw5ZXRdVN7M42kQUtaUUSljjiwnfvd1kMYuHvDnaA', 'Максимов', 'Максимов', 'Максимович', '1997-02-01', 200, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-23 12:09:34', '2021-02-24 05:41:24', NULL),
('247bed1f-68ab-4a7c-976c-2c06335e5898', 100, '103.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$UUdHNXNQOTZ6VVVBN1RIZw$TbrqAOshsn7CyXoAtFdEfUTDmRll8EXdLnmciFJbYls', 'Дамиров', 'Дамир', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 06:31:59', '2021-03-05 06:32:02', NULL),
('250eaf6c-7a5d-4bd9-923d-47be78439196', 100, '100.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Tmx6a253M252UnZWMUxCOQ$oaHVIe/mFVassxss9FlluwX5PVCVAr8uv1n9cq9LaA0', 'fghgh', 'dfgfg', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 14:35:26', '2021-03-02 14:35:30', NULL),
('26f572b1-31d3-4005-96e8-39a1361b12b3', 100, 'dzass@ds.ru', '12121', '$argon2i$v=19$m=65536,t=4,p=1$WVlOdThqMHFIWm41Si5hbA$aJ+MbESvmA2keEZ+suFs+Noe6NkOm8l/1AlIN54Ei7s', '1212', '1212', '1212', '2021-02-01', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 14:25:58', NULL, NULL),
('2791241e-3a47-4ca1-bbf3-1f093f641f91', 100, '74armani@gmail.com', '89999999999', '$argon2i$v=19$m=65536,t=4,p=1$T3IxcENqQW92L1JKcG5hRw$Q9F99k+Bx9tJ9QBSbkNFuSxBqfEvzuz+cDcH/xduzFE', 'igor', 'igor', 'igor', '1994-02-06', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-06 10:23:37', NULL, NULL),
('27b9eeed-aee2-4f1f-ad13-59abdc97c819', 100, 'sds@sdds.rtr', NULL, '$argon2i$v=19$m=65536,t=4,p=1$blNjaXJQZHFKUzNHZi50SA$p+jbn/kRoLIMFUl8kxdjDTnP5eq/p27IKyWMkpU6bsU', 'sdsdsd', 'sdsdsd', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 08:27:24', '2021-03-05 08:27:28', NULL),
('28ec9225-ec92-4af9-81c9-c3d9fb1d67e0', 100, 'et@er.ur', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Qll2THNwd0IuQzd3bm9QWQ$VUGbW8nYO8IGhNMyH1Hudu2tMXszhbhqfzwfANsEuNA', 'sdfa', 'sdfa', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 13:19:11', NULL, NULL),
('290ac6c2-bb39-4740-acca-dbb0d84bd427', 100, '69.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$VFIyTThYM0UzRWlER0NlSQ$i2wqNt+W9+BKoPMqS79Kz3aa9TJ4bAqntsU01LR15qY', 'Олежко', 'Олег', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 11:32:06', NULL, NULL),
('295e2afa-bf1f-4d60-98fe-ebb944f14d4e', 100, 'sky1ba@mail.ru', '901324', '$argon2i$v=19$m=65536,t=4,p=1$WllKOW5wcDQwV1hOZ05HYw$mau5WCo/z+Nr6xb6w+HVGBGHgqvFAyrfuuNIYGCeNZU', 'Таня', 'Таня', 'string', '2021-02-21', 100, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 17:48:56', NULL, NULL),
('2a23cd11-66d1-4c8f-9c81-60992e8fbe14', 100, 'qweqwe@mail.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$SnZvSTc0b3pZeTZZcmlyTg$yRsYOl3rdjhcYuf6PMywMcVWufxGMQW7FpD5f99Z5XQ', 'ываыва', 'выаыва', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 15:17:05', '2021-03-05 15:17:07', NULL),
('2bd07951-6b22-4021-bacb-16027acdf085', 100, 'test102@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$MHdodXFBUWdPYjYuMTFYbw$ygDwrVINlbSaabCme87nU1naIGtA0nfMPEVt40A501E', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-11 12:53:52', NULL, NULL),
('2bfed563-132d-48e1-b113-f549b43c4322', 100, 'tets@era.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cU1wMTRKWXBUdjhzUy5QZg$ytV0+fiK2yjOZzSqGmRgD5NNYNZx3py7HIGbkIjpZOw', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-03 05:31:07', NULL, NULL),
('2c5dfe16-5d21-4601-bfdc-679b7c89b9c1', 100, 'test-executor@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$TFY3TUpDUjljVHRqRGVWRA$UWs3qfc9CLqWu9ZRZncXTMD2gxuIXViRV2nxtswsuRg', 'test1', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, '7bb4adda-4a18-4254-8e05-93bdd7dd7254', '2021-03-26 11:40:29', 0, NULL, NULL, '2021-03-25 11:40:29', '2021-03-25 11:40:29', NULL),
('2d4209ae-a7d3-4525-9f6f-747d15133f4c', 100, 'dz@dz.ru', '11', '$argon2i$v=19$m=65536,t=4,p=1$NlFWL1JobDg0dUNmem9vdg$LM6csbPBt2MzVFxXBCHl3Fa6lsfjewLfYk1wDyelv/8', '11', '11', '11', '2021-02-02', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 14:22:13', NULL, NULL),
('2db7e8cd-d259-44a8-9c71-37205ffc20a1', 100, 'aldjfs@asdflj.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$d3k3ZVliT3V3RWFiNWRXYw$V1UaQ7i2gtRS/T4XOxFTnvRxbTcDcHY4BMi8L13r6PQ', 'aldjfs', 'aldjfs', NULL, '1989-11-01', NULL, 100, NULL, NULL, 0, 0, 0, '0a4a8d81-8465-4267-b62a-963eb0d7da91', '2021-04-07 09:15:30', 0, NULL, NULL, '2021-04-06 09:15:30', '2021-04-06 09:15:30', NULL),
('2f3f8baf-8c78-4f90-8a4c-9f3d35b97b57', 100, 'sky131121a@mail.ru', '1', '$argon2i$v=19$m=65536,t=4,p=1$bUV0dVhkTExYZXNTZ2dSVw$Vio3lYNCCqrvzIurWp5vlld/kfOCuV31F1TcHfiPzjU', 'ф3', 'ф3', '47586итграпигарпигапварпврпрапгрвгаипавпарпарпиарпмапнапрнрмо', '1991-12-12', 100, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 18:05:22', NULL, NULL),
('2f44aaf3-cf55-47a1-bd26-0e97e4704390', 100, '66.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$U016TjZzLjV4ajJrVnJ5Lw$0QMbGbDTNpX+DxW1COWmzr6jY1fEYCqKV74pWgb+6Wk', 'Олегов', 'Олег', NULL, NULL, NULL, 100, 'e3b51352-e2e3-4db4-97d1-5d27f943e34c', '2021-02-25 11:12:24', 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 09:57:14', '2021-02-24 11:12:24', NULL),
('2f5121e9-77d0-41d3-9e9e-1494b979469b', 100, 'sdsdf@dfesdf.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$c1puOFFYU09VVDBYbjVJZQ$s9twuUjFFBEJ2Kwc32y6wSH6Y+N9sVf/U9uHK5DCMDk', 'Редозубова', 'Наталья', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-09 06:50:38', '2021-03-09 06:50:43', NULL),
('305abe94-9792-449f-bd4e-5ad61ecc5cd6', 100, 'latayix984@igoqu.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bXQxSWxmQ0pTbGlNYzRoSA$QLDDRBUClhOWAaQsZGjuBWtBUib3L42b3fImCIXnZSo', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, '374ae8cf-3c45-467f-afb4-528df3e30018', '2021-03-05 12:39:04', 0, NULL, NULL, '2021-03-04 12:33:14', '2021-03-04 12:39:04', NULL),
('324d18f1-8035-4ecd-85c2-3a88fe13063f', 100, 'msdfsdf@sdfsdf.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$R2J6a1NXcy42T25HbVFGTw$Hh+Cb7uh1LzDRe832r5FYmbLAP5tkm5Ho4F8UoEEQzw', 'зимин', 'Михаил', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-06 14:38:15', '2021-03-06 14:38:20', NULL),
('32a00c16-2ce9-4470-a033-7b00334119dd', 200, 'vovan2hikkan@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WkNHSlYueWpYZ2ZuU0NkTg$qAyDpeMhqk0fkpT0nqRYMh1OgHazAjjp720jq2wB8r0', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-26 12:39:48', NULL, NULL),
('32e178a7-8904-4846-9eaa-0fca9d534345', 100, 'dz@rt.ru', '12112', '$argon2i$v=19$m=65536,t=4,p=1$SG1JVEFPOWsvdWk3bDV6Lg$MvZJUAfVFZed0g2n6kfb1YirJxCKwh1RRCwLsl9Xa2I', '121212', '121212', '121212', '2021-02-03', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 14:31:01', NULL, NULL),
('33826dfc-3fce-457f-b9da-6d4aa8fff655', 100, 'asdasd@adas.asda', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cmV6SUJpeTRqODhYUGUxZQ$L5oKYFjSPMNf9SpWxyRXS45Jc55Y2qn2w2dlfyE/DUM', 'asdasdas', 'asdsda', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-18 06:06:51', '2021-03-18 06:06:57', NULL),
('33a1b771-a9e0-466c-bb48-24d3efaf7d1e', 100, 'bawet92982@macosnine.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$LjdxV0pWVEVGeXRQSkdsVA$QJI8FKFHgEfdUJthnLJooeMhj7L+t/KQAhn/OBbOafQ', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-04 07:30:42', NULL, NULL),
('34cab49b-f299-494c-bce7-a9d719dad2b2', 100, 'test@test.ru', 'test2', '$argon2i$v=19$m=65536,t=4,p=1$QkpVQXE1djZlbTF1TkhuOQ$QrqpZm42/PjB4OWWuIYI8N6Uf2NzuXbrxHgPp3SDlks', 'test', 'test', 'test', '2021-02-20', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-20 06:21:00', NULL, NULL),
('356e41a4-0dad-457f-8b4c-c69ae608345b', 100, 'test@test48.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$T2xVc3gvR2pQeER5VVNjcg$f/8zZ8keiAt1GKpnGDVai6b7svudRtrbqjWZKPcTq8k', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 08:48:35', NULL, NULL),
('35ac966d-0108-4a55-90a6-f3b45d78d8fe', 100, 'ptsvbnwevhvawscowd@upived.online', NULL, '$argon2i$v=19$m=65536,t=4,p=1$MkVVUi51LmxRdlh0OTF5ZA$ZNnS4gxCzqZ08Yhzxn6AXZgKW2Xup+BNMVd4TyNoJ8k', 'aaa', 'aaa', NULL, NULL, NULL, 200, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 13:02:51', '2021-02-25 13:03:55', NULL),
('3821b2cc-c9a7-4790-9b37-d6842a2d3024', 100, 'ljasdf@adfs.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$eFdTbktLRldaZ3BYVGJwcQ$uucETFfg5Iv+VEERGlSV7UFXR6kRSe7W8s5eR4DQgSs', 'sadfjk', 'sadfjk', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-26 09:39:02', NULL, NULL),
('395fb206-2304-4af1-bab9-2f780136812e', 100, '111.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$MjNUd0tjZXhXZ0R1NHVVdQ$ahmKKttYyeDpTe0IFxoRN3Qsq9ZLA5V1wRZmzjcctuc', 'Дэлон', 'Ален', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 10:44:41', '2021-03-05 10:44:43', NULL),
('3b1acdb4-5d79-4d5e-8d80-4b569fc0b565', 100, 'sadfl@afds.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Wmg0RDVoV3ZKYVJ4M0VZWQ$rz2Ru+uHT7zLTjV938VmHnWDrR7PQw5F9hGuTpNhB8o', 'sadfl', 'sadfl', NULL, '1989-10-03', NULL, 100, NULL, NULL, 0, 0, 0, '7808229c-dd12-4b3e-a95c-2314334fd6cb', '2021-04-07 11:21:33', 0, NULL, NULL, '2021-04-06 11:21:33', '2021-04-06 11:21:33', NULL),
('3cf2c7e5-64c4-4289-b36d-10b1277a61ea', 100, 'adkjfl@fadjl.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$TmdCMWtpS1kvbFpkUk9pcw$y3NRmHh4nR6sPLBm0Rfnr9YWW2AmjlWNGGQLOJCt4wA', 'sdflka', 'asdfjl', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-13 12:58:31', NULL, NULL),
('3d5d9ece-e18d-411a-b069-a9f9f29b13ab', 100, 'asdf@adsf.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$VHFjTW1tRnp0NndRSnNiOA$IdaTA9sQQx8U2z4r8dgxm+vDBYGb3PnJTK0nbNfvvPM', 'asdflj', 'sdf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-16 06:04:19', NULL, NULL),
('3e6e1256-f9a6-4771-bda0-c5f8599bdf6e', 100, 'aleksey+1@laletin.me', NULL, '$argon2i$v=19$m=65536,t=4,p=1$U0R2b2VIWHQvTE9hc2dwSA$M6iA6BrmTONJkS73CLy5iL8+EQiDUaPqewJW41APd3A', 'string', 'string', 'string', '2021-02-19', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 10:21:50', NULL, NULL),
('3f72ba8f-3386-4ed4-8999-2938a66af09d', 100, 'etst1@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$TFgvYjNpdXNTWWRIY29YSw$dY6TdHx0wUo/t2X2FVDJkFHxe1iJQezQh1m7QqxBlAU', 'aa', 'aa', NULL, NULL, NULL, 200, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 13:06:14', '2021-02-25 13:14:42', NULL),
('40a5cb4b-f753-485b-b6b3-b99affbf33d5', 100, 'test@tes.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$dzlGN2FoOFJNWlhtV1lFbw$6bUb3oVYQWabQBIPVgFNEqEhg+1GmniqqJmIAmWozb0', 'sdf', 'sdf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-12 10:27:55', NULL, NULL),
('46666f3e-dbdd-416d-9235-c76c0843552b', 100, 'uskov@dreamheads.ru', '79265528774', '$argon2i$v=19$m=65536,t=4,p=1$YXRSdW9raklSMC50NWhSWA$M1BsHyAgb9o+JA3IArzRy6NZHDTHgDzSqhLQonwXp+w', 'Усков', 'Сергей', NULL, '1921-03-03', NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 14:48:13', NULL, NULL),
('48d382e0-5dd5-4fb0-badf-5e7339988d6a', 100, 'aksdfj@asfj.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bDJmck4xQ1ZsLnlXL1FGMw$53+9ydvlu/+gUQwIsz+x8NOEQ5rhJGuMFnmeH5HwIs4', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-03 10:27:10', NULL, NULL),
('495224d5-5806-4525-925d-5a73f5c681d8', 100, 'teste@afs.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$dXFhVUY2Li5GMS8uLnpJaA$J0Q2Bv1eiG0WsqVLFJTH5PPoiJZ0EcOiAoIlb0a6pn0', 'sdaf', 'asdflkas', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-09 10:45:18', NULL, NULL),
('4b2eb24a-9ead-48ab-84de-50ce7d0e6d11', 100, '88.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$VDg2T3NpQnF6NFdvVXVuaQ$6mUeFCorHogT6uTCean0Z7LTFIqqfno0Jf0XGpcFX+w', 'Ренатов', 'Ренат', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 11:19:12', NULL, NULL),
('53b70f6d-ddc6-456d-91ca-96d80052660f', 100, '122.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$UnQyM1JsT3J0aS9RSnc5OQ$HEvr/0MPzAfGA5im5GW0tSWnMva13YxYswJWu6e9yE0', 'Аристархов', 'Аристарх', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-10 08:59:16', '2021-03-10 08:59:21', NULL),
('559faf20-83e1-447a-8609-62d6603ae7da', 100, '87.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cDU3Ni4xZXAwT3BrSzBSQw$gm2dodhgtVmVoZJNlzBx4tqa8bp2zY7e+6COmxLwkBI', 'Денисов', 'Денис', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 11:14:40', NULL, NULL),
('56db4649-62b7-4723-8a9c-85db8fbe7234', 100, '63.zbrsk@mg.dreamdev.space', '12211221122', '$argon2i$v=19$m=65536,t=4,p=1$RzVNUGloUXVyemZ5N2dNRQ$Wsukv5eNmWB4bE3WkTNH/vZy3ATJ07n8oGZgrVnEmGM', 'Глебов', 'Глебов', NULL, '2009-02-01', NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-22 12:50:08', NULL, NULL),
('56f88a3d-481b-4e45-b85e-1a2aa631f50d', 100, 'sky1321a@mail.ru', '1djgk5', '$argon2i$v=19$m=65536,t=4,p=1$c0NYVG1GMVNSYktpamNDRQ$0I811gcceA7uD2k5tXHLU72jGDCzegjntYKA6IRKFmE', 'Таня', 'Таня', 'олала', '1991-12-12', 100, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 18:18:50', NULL, NULL),
('588af053-240f-461b-a2e1-74b3a1e56a1b', 100, '135.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WW1WL3JiLzdDMVJ6RS4ydw$d62hIo6Z59/7G9tz+kusTo8ZvUyaxLBXLQSb37Fxqow', 'Еленова', 'Елена', NULL, NULL, NULL, 200, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-15 06:54:54', '2021-03-17 12:57:25', NULL),
('58b6bce3-746b-42e0-b81a-ce4e4199ffa4', 200, 'vovan222hikkan@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$ZEZqYlhCNFBVNHoyMnN2cQ$CKGmiBxSlpd6TKxBgaflwD7sny/WhQRsCGyf7mRcBrk', 'test1', 'test', NULL, NULL, NULL, 100, 'e996fd70-e693-4acd-847a-65da38a7e9a7', '2021-03-20 08:28:44', 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-12 08:05:41', '2021-03-19 08:28:44', NULL),
('5bc305f9-45e7-4f32-8b6e-c89b0bb51859', 100, 'safd@sfa.ru', '89517715657', '$argon2i$v=19$m=65536,t=4,p=1$MmtaeDl1VXcwdFVySlQ1cw$yeLMY5QYokPS/u5jC6pKaG6IHB8lQBDtjpIRyXbJu64', 'adf', 'adf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 09:32:14', '2021-03-02 18:42:50', NULL),
('5c5d696b-a194-4a14-a745-4c868518f9cf', 100, '74.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Q3V3M3I1N082NURGVGlCWg$C3MAoeVShPe87FBb+WUavtYEGDM1vCpkCI3ss5Fmzx4', 'максимовски', 'максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 12:00:56', NULL, NULL),
('5d23565d-0454-4af4-a41d-34f4ce58f29c', 100, 'qfm24268@zwoho.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$TkVEancwR21lc3U5Y1p4bw$6QDuXwDA42mxGo1VWL1RlTFJ8ODvzKMEJuHNNi2Uzbg', 'asf', 'asf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, 'fdee3583-973d-4ee8-b3df-4c341dc19d9c', '2021-03-12 12:10:42', 0, NULL, NULL, '2021-03-11 05:46:55', '2021-03-11 12:10:43', NULL),
('5d856a72-fee7-4a2d-a990-9d5ca5e73ef2', 100, '3@34.ru', '3213', '$argon2i$v=19$m=65536,t=4,p=1$V1BrRk1rNnBUZ05uVEd6RA$0DqxJ1Mxi7LCmseW5ONs1lCphkMfrTjYCSa0lD3XjN0', '33213', '33213', '33213', '2021-03-04', 100, 100, NULL, NULL, 0, 0, 0, '1819ab70-d82e-4eda-b9c3-64dcb6236fcf', '2021-03-25 10:36:03', 0, NULL, NULL, '2021-03-24 10:36:03', '2021-03-24 10:36:03', NULL),
('5f6132d3-793d-4b5a-8eda-08362adbaadd', 200, 'admin1.mzimin@mg.dreamdev.space', '+791577744433', '$argon2i$v=19$m=65536,t=4,p=1$TjJwMS5YV092VDhNRkZNQg$eQX4Qca9F+dZy6QbZ+40VRbw2XGGT7ScrD/RPi4s2t8', 'Админ', 'Админ', 'Админович', '1986-03-03', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-11 12:31:58', NULL, NULL),
('61bef744-5c97-4fb8-a856-7bc178ae8fe3', 100, 'test456@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bTltYlhOd3lVMERhbGFhYg$Mw9o/dgbK/aAMaj9PrXmG+IDg2yNw1So7N+fFzsf0OU', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 13:26:24', NULL, NULL),
('620cebec-7c13-4161-8505-9859d726abe9', 100, 'shem.kamir@forloans.org', NULL, '$argon2i$v=19$m=65536,t=4,p=1$SjJWbjBPMzVaVTd0dVNUbg$r9MAbyMC19/zf8tAdeOV7atpNm9466g/WhMbItS+6Ac', 'Qew', 'Asd', 'Zcx', NULL, 100, 100, NULL, NULL, 0, 0, 0, 'a3ce4f07-4b32-46b2-a9ff-2a0b71c5d708', '2021-04-08 07:56:46', 0, NULL, NULL, '2021-04-07 07:56:46', '2021-04-07 07:56:46', NULL),
('63652bac-7c70-4f0a-806f-d23345609271', 100, '61.zbrsk@mg.dreamdev.space', '12121212121', '$argon2i$v=19$m=65536,t=4,p=1$Ym1LUDQxOG1tMUF3VEtieA$DITadc5fgdDL/NT8r4kYO8NB+sF31bYQNKtIXNGWFoc', 'Петров', 'Петров', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-22 12:00:30', NULL, NULL),
('63f2d790-3165-4516-9461-b7681a7be833', 100, '115.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$M0JOcUtRL2luL0cvN1dBcg$hKOM5T6PespjgCtE95q6yf7b7rN2xg2ykZ/rz8xDMP8', 'Максимов', 'Максим', 'Максимович', NULL, NULL, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 13:30:09', '2021-03-17 09:12:18', NULL),
('642837b7-6846-4d0c-a7ce-b0bc909d6c58', 100, 'dfa@asfdl.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$MXhLYnVSdnNVVnBSZXNFRQ$Fvflyud/33uaJ+ZIjxhQgVMaztw/9DBvElAeCzbM9gU', 'igor', 'igor', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 13:46:06', '2021-03-05 13:46:10', NULL),
('65c7c0c7-226f-4631-a89e-f471e1da4c2f', 100, 'sdvvfdsgdf@tets.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$UjVlYmNnRlpqb0FOemNaYQ$31hGfA7z39Nu8uVmpinqnu0JpFIK8Tvw74QZVuimla8', 'dsf', 'test', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-16 15:31:37', '2021-03-16 15:31:42', NULL),
('65da37f4-79f6-41e6-841b-2cec513b9bbb', 100, 'vovanfhikkan@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cTBZU25UUlBXb0pLWlB2Tg$qUS6BEGav+x3+bqInKoWG0cgysTL3ymomxiOknP5f9o', 'test1', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-18 07:36:32', NULL, NULL),
('674e9ad0-4bab-41f5-9840-7f04fa0d38f8', 100, 'test1234@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$dUxQOVp1RGJPRC9HU0s4OA$mfuUvPuhqkOJzlwy3KF9RoRGkodTYoSjFyUbcL/QOEU', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 10:48:23', NULL, NULL),
('67d87920-0776-4c65-b046-7e1710b47e72', 100, 'Test@Test1.ru', 'Test1', '$argon2i$v=19$m=65536,t=4,p=1$UXBoLi84REFoZGJmNFd6dQ$sS1rH7Kp4obvEagfGgO5IiZOMK3lYxRwMH0rDfwLYaE', 'Test', 'Test', 'Test', '2021-02-20', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 23:40:04', NULL, NULL),
('684b0537-5623-460e-8919-3c2fc7b5dcbd', 100, '99.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$U3dlS2M1Y1B1Z1pHbEtVcA$fDQ3XyPUhCmTjnt5PpEFt+eO2sfbF4vRhHYolUkUN00', 'Максимов', 'Максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 14:27:15', '2021-03-02 14:27:17', NULL),
('6948fdba-11b4-4f1e-8f02-b9d765513e70', 100, '80.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$OHpCbEhQRVhVN0EwWGszRQ$MWWvNSHpwlM3oSxL9X4s4JHT5nMUilXUc5Uolv/mRkE', 'Омов', 'Омов', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 06:33:44', NULL, NULL),
('6a03367d-d6ba-4b14-bdd9-2d1a4b6ddd13', 100, 'asdf@f.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$V2REMDJ2TE9iSlpVYkEzYg$UdtmvHKRP4A8A8KcQkjDHQb10pW7yex64BVeynGkTSQ', 'sdf', 'sdf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-03 12:26:31', NULL, NULL),
('6a199751-6db8-4fc0-a2ec-160c601cc50c', 100, 'dsfsdf@mail.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$dUJRN2JsR0RQZHpvd21XVw$H0imlyrwYarH8eNjde2SejuETrTdetxe/JYtyi6VoXc', 'ваава', 'аавав', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 15:06:23', '2021-03-05 15:06:27', NULL),
('6a810a00-9f98-4f9b-9429-3e5339420b5a', 100, 'lena020419@yandex.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$VVV2QXMzRGIuWk9ILlNkNQ$N7lrsHzHwT86ME+i+n6F+buYjQS5W5SzKzHJjFpLOPo', 'Март', 'Лена', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-16 15:15:37', NULL, NULL),
('6aefb3ef-90e0-4d97-8f31-fe0dcdb6d048', 100, 'glad.87@mail.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$OGJVNGQ5Q2NDMTlVajhUTw$Py8RdAR2FXyy1kf6MuaiG2fcaKyZBY5rEaXj8bBJ8Gk', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-07 14:41:37', NULL, NULL),
('6b250f66-687c-4343-8930-95ddadef3df4', 100, '113.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$RDVSN0F1YmVhVnFzbm9LZA$Z9dzAFjEBlMd1eEo6jmsuuEwoJRDCTupyNRQoHGFR9o', 'ydtrydr', 'ertyrt', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 12:05:27', '2021-03-05 12:05:30', NULL),
('6cd1b662-3f8a-4cd8-a2c6-aa30618e7cd0', 100, '147.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cTFOZVpmTnZpT3lweUwvaw$te0F+8EsdSWXuXwNefkMo2SWx8d4jZvAJKGBd+d0w54', 'Пушистый', 'Котик', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, 'b590c1f8-bc61-4573-b7f5-0732e72b4fc9', '2021-03-20 15:10:36', 0, NULL, NULL, '2021-03-19 15:10:36', '2021-03-19 15:10:36', NULL),
('6d5e86d5-afbb-4826-a0ef-3a892ea27ad1', 100, '98.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WFNUdTV5NlZSZjFBR1F5RA$BPajdZN5m3ab8z+6vUzIFkh/C/JmrQ26a7Hia/J9KMQ', 'Максимов', 'Максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 14:23:06', '2021-03-02 14:23:10', NULL),
('6e4549c6-b9db-4602-85b6-a0bf3ddffb56', 100, 'sergey.a.uskov@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cVVnNWtJQjhjdGQ1Q1ZScw$1tma8TLxaYE1W8V4U6/UDRZH5OVhWSjdoTmvu7a4SaU', 'Тестов', 'Тест', NULL, NULL, NULL, 100, '294225ca-02db-48ff-a891-3fc7302f620f', '2021-03-12 12:39:28', 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 14:38:47', '2021-03-11 12:39:28', NULL),
('70fef0aa-3a94-4f2b-badd-4301ff663fb2', 100, 'af@sadf.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$N1BlSVhpdy5MLmNaNnkybw$TdRkNhg1x+wkFMAV74G3UewF9DSAf0UOxc44ZAJmbsM', 'djfsk', 'sldf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-10 05:39:13', NULL, NULL),
('7135c34e-3f87-4537-afef-8a13bdf2caa3', 100, 'dzrise@yandex.ru', '+79035836938', '$argon2i$v=19$m=65536,t=4,p=1$RjhTbmFKNWlFTnA0dHlRUw$cWOnOvCeZ1XqQiP6C8Xt0Yc2CyROTK7nfW4GPVr+Vzc', 'Захаров', 'Захаров', 'Михайлович', '2021-02-16', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 15:02:51', NULL, NULL),
('71d3b61a-1a63-45d3-bb2d-cf86dcc007e5', 100, 'mmm@mmm.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WWwxQVFsMVd1WVFWWVkuZw$D2ZMCf1sj/jc84e4QjZlyubv6hEgLAUajPML/FMslto', 'Зимин', 'Михаил', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-06 14:25:50', '2021-03-06 14:25:56', NULL),
('734000dc-8067-46c2-b845-32130aa74cc6', 100, 'sdf@rty.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$blRlWUs5U0FVZjJQYlpFeQ$UauEbf5upAYaUX0eUE8DuauaEgJpYXfAyNfmF5OMu+Y', 'ред', 'наталья', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-09 06:47:42', '2021-03-09 06:47:47', NULL),
('740c2c54-310f-4979-b855-2524a14ef225', 100, '86.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Rk5BTktaMS9veVBockVVbA$3lpgHHuT3E/5G9AgTticCAzUNCL2b9stL3L2xTldhDg', 'Алексеев', 'Алексей', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 11:01:50', NULL, NULL),
('7881edde-c928-461a-9175-34a6e8a599d5', 100, 'admin@admin.com', '+79154805578', '$argon2i$v=19$m=65536,t=4,p=1$LnBJRXhyLlU3azZkMTdhYQ$1rJTvlBpecSoqIGIzcEG1OjSCr5F50ah/3vQRYIaEyI', 'Админ', 'Админ', 'Админович', '1986-02-10', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:33:32', NULL, NULL),
('7bcddb3b-eeb9-48b1-b66e-231401c0e045', 100, 'u1111ser@example.com', 'stri2323ng', '$argon2i$v=19$m=65536,t=4,p=1$MWNYNXo4aFFnb1d5ZTV2Ng$G7jSXjy624Fp16WOe0LMdBTFLECoBUkpSNO5tjVoBz0', 'string', 'string', 'string', '2021-02-22', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-22 08:29:18', '2021-02-22 08:53:36', NULL),
('7f6d0f8e-82e2-4c18-8e78-d74fad31959b', 100, 'sefwerewr@mail.ru', '79063655403', '$argon2i$v=19$m=65536,t=4,p=1$WVYyV015VmtteTk1QXY1QQ$ZFiMUv+SKSvEycm51tF2mGU0I60Osq72+bgnnkc83ac', 'ыаы', 'ыаы', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, 'ab220ebe-210f-468d-99c0-bede1432af7e', '2021-03-12 14:33:30', '2021-03-11 14:33:01', '2021-03-11 14:33:30', NULL),
('80e947b4-06ea-4ba3-8a55-c45c555fbe78', 100, '4344@rgfg.tu', '2444556u78', '$argon2i$v=19$m=65536,t=4,p=1$N3FKMGNCU0ZTYmFjMHMxNQ$lle86g86eNgwlVKIc4S1TyX0eJ31fbmXjf3h3Mg9Km0', '3434', '3434', '4434', '2021-02-17', 200, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 14:33:16', NULL, NULL),
('815a78e1-dfce-478b-9db7-3fce74af2cce', 100, 'sergey.a.uskov@yandex.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WG9ValpaZFF5TEZuQjZtcw$PBB9UFBf/bE3a5yek0lwkz8SbnNanrQ7nQ4Nghy84YA', 'werew', 'ewer', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, '81462d56-08ed-48b7-9844-10d52a97cbb2', '2021-03-10 15:45:25', 0, NULL, NULL, '2021-03-09 15:41:09', '2021-03-09 15:45:25', NULL),
('820403c7-ab5e-41d7-945e-638349d6024e', 100, 'trmweceqvphmbgqcxo@twzhhq.com', '79771455671', '$argon2i$v=19$m=65536,t=4,p=1$NFBPWG1YYVM0ZnZHa2RiZw$or/vIkADXmXObUM9Y+dOwS9kO7xNle1l606Uq9+qDak', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '625ab0aa-e67d-4be9-86c1-56c540c9beb3', '2021-03-10 10:26:10', '2021-03-02 18:54:03', '2021-03-09 10:26:10', NULL),
('82fb0260-cc69-41c2-831e-ab9645c385ef', 100, '62.zbrsk@mg.dreamdev.space', '123123121212', '$argon2i$v=19$m=65536,t=4,p=1$MmJvejBaTXBTcncvZS5nbA$OoqGaTVn+UTquvzOGmMYUEOFLW1xgv4osq/4iBzAOd0', 'Борисов', 'Борисов', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-22 12:45:59', NULL, NULL),
('83eb5fd2-8b5f-4bc1-ab18-d365072b2ff9', 100, '67.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WUROMHhzbGd1NFp2SS5WQg$UH4x1WCkLmhAHr6259Mx3hErC3zSssyCDxD7hs7LDMs', 'Игорев', 'Игорь', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 10:39:30', NULL, NULL),
('843e18a3-e0cb-4ae3-a584-19b7f20d7c59', 100, '117.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Um1wSzFLdjUvTXlMN29PaQ$yz0z9z6Hkq9grjySID0IQzf3OKJwCraLXgsoZEX2zKs', 'Маратов', 'Марат', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-10 05:45:10', '2021-03-10 05:45:16', NULL),
('8514649f-ade4-4019-91cf-d8e378656a7e', 100, 'Lena01042019@yandex.ru', '79150088077', '$argon2i$v=19$m=65536,t=4,p=1$UkJaNjlpTnpaTy9JRXc3Vg$XLF0pKrqtQPmHlEEIWmtYSnQRVCHB3+mDGQARY9FuZk', 'рррр555', 'lя5', '555', NULL, NULL, 100, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '420802', '2021-04-01 14:18:38', '2021-03-30 08:00:07', '2021-03-31 14:18:38', NULL),
('860a72b8-1d71-4805-a357-647f0f9beaa5', 100, 'mzimin@dreamheads.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$YVhjUS55N1BWSVRJRm9FLw$8jN7F6i41ESvzJx+EDp7Av2iV+YfPXlKH/09Nf5iC7c', 'Зимин', 'Михаил', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-09 15:42:38', NULL, NULL),
('86e089e9-4e8e-4652-b896-a42f1dc2590e', 100, 'movijeb498@heroulo.com', '7951771565733', '$argon2i$v=19$m=65536,t=4,p=1$enV2ZFdPY3N1TXhNdnZWbw$6rZJBeMkH9n2negNy3fifu79mK8r1scC6nQufAdZeLM', 'aldjfs', 'aldjfs', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 1, NULL, NULL, 1, NULL, NULL, '2021-03-12 14:02:54', '2021-03-15 09:57:35', NULL),
('87f59790-51cc-447d-b6fe-273ff067edd9', 100, 'laletin@dreamheads.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$N2ZzblR6dHFzdzh3UnhESw$aL5ObpYjLZ43jv5ghGXAH6WzrKadx2D/AdK5xDoQQ7c', 'Лалетин', 'Алексей', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-06 06:20:12', '2021-03-06 06:20:16', NULL),
('88ff3a09-832e-4693-8b33-78d46288495c', 100, '101.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$b1hON0JVR3RXVkx6MEpUcg$ZVLvR+U3qygtjrlL47zbA9Lsa1N1eGHQti+9THkHxiI', 'Олегов', 'Олег', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 14:46:25', '2021-03-02 14:46:29', NULL),
('8956722f-8c97-416f-9b00-a2b374122fcf', 100, '74genesis@gmail.com', '79517715657', '$argon2i$v=19$m=65536,t=4,p=1$eDdDNjJNaWRUSXlKVXRTeg$RCaJtjVqeUn8u1LuUDpoJ4AUFVVjYvXZpd/GR6E3j5w', 'test', 'test', NULL, '1994-03-15', NULL, 100, NULL, NULL, 0, 0, 1, NULL, NULL, 1, NULL, NULL, '2021-03-04 12:07:08', '2021-04-09 11:07:52', NULL),
('89639e92-638d-4135-bbfe-ac88ba455ccd', 100, 'sdfdsfsdfs@mail.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$UUtnZFlqdzFMWVZ0cWNybg$KogU0EIZ9dTnXaySawmxvcIpIWo5DNBfbepisSRoqig', 'ываыва', 'выавыа', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 15:22:42', '2021-03-05 15:22:44', NULL),
('8a289c75-e45e-4969-b12d-66438a26618e', 100, 'dendfast@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cHhEei9ZTnc3NlpMOGJNcQ$TFXd1+XdM77mQifFJB7/OWWG3ZvdhFviOzRopsBLoOo', 'ivanov', 'ivan', 'ivanovich', NULL, NULL, 100, NULL, NULL, 0, 0, 0, '6bf3e95a-e1a4-4b6c-be81-2221f5508413', '2021-04-06 13:12:45', 0, NULL, NULL, '2021-04-05 13:12:45', '2021-04-05 13:12:45', NULL),
('8a3f54c5-113c-41da-8ffb-710462bb0233', 100, 'test101@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bFNRQVFXMzdBay9VdkEyYw$znntz8RxHjZZjk7RDgPCxg9w6J165Y00UGmOXwFP3SM', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-11 11:32:04', NULL, NULL),
('8d452bb6-84ac-41cf-a1f3-0b17d195bc5e', 100, 'sky131a@mail.ru', '', '$argon2i$v=19$m=65536,t=4,p=1$ZWh1YlNFNG9nLjZ1UUguaQ$Ua52j0asBE8dDcbrTEEbghNyFT/Hkd2QIN3XIDfNlXA', '5234', '5234', '47586итграпигарпигапварпврпрапгрвгаипавпарпарпиарпмапнапрнрмо', '2021-02-21', 100, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 18:02:15', NULL, NULL),
('8e5aae66-b12b-4bac-8fe3-04407ba1857d', 100, '41.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$OERrNUM2U1RsdWs0c0dkQQ$2Ur27IbQ1lIKZAT/zqXbcXVacILvrJtZl0aXwgg2364', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-08 14:31:17', NULL, NULL),
('903d29fd-9166-4dc3-81bd-4f798cdb582c', 200, 'skydoba@mail.ru', '904', '$argon2i$v=19$m=65536,t=4,p=1$VUlYLm5pdXYzMXZlVHpoMg$1NFsg5Mhnd7ZUkDQca0vubO/ZynumV+qFebgVY21H7Y', 'Таня', 'Таня', 'string', '2000-02-02', 100, 100, '875d5091-bd6f-455c-a2de-c6192de708a2', '2021-02-04 18:25:35', 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-02 17:28:45', '2021-02-03 18:25:35', NULL),
('9270a0f5-aed9-45ff-8501-e64319e6891a', 100, 'sky111ba@mail.ru', '901d2324', '$argon2i$v=19$m=65536,t=4,p=1$MkphZUt6ZjdwenB3U0hndA$tpOl1xUWFTw1FzZ5L2O9/q+H49bq8f4OwpOtU0fYdLw', 'Таdffdhfgdygfudygjdghghdguhdsghdsgfhdsgfhdsgfhsdgfhjsdgfhdsfdhfhdfhdsfsdfdsfdhsfhdsgfdsgfuiryt783456367567tiryhtfuydhguydguhdjhdgjdhgfhdghdghfjdgjfdgfdgfhdgfdjgfdfgfdgня', 'Таdffdhfgdygfudygjdghghdguhdsghdsgfhdsgfhdsgfhsdgfhjsdgfhdsfdhfhdfhdsfsdfdsfdhsfhdsgfdsgfuiryt783456367567tiryhtfuydhguydguhdjhdgjdhgfhdghdghfjdgjfdgfdgfhdgfdjgfdfgfdgня', 'string', '2021-02-21', 100, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 17:54:41', NULL, NULL),
('93c77cba-e896-4aac-9530-71d07d454351', 100, '68.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$RE04WEhPTmZRMk53U3BpTg$BQPzisAyQ6hSAE5acux+wSxxNw3GfqHqYLvDDsYp9mE', 'Олеговски', 'Олег', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 11:31:01', NULL, NULL),
('9516043f-dfc7-4ed1-9880-833f7518ac4b', 100, 'aldfjs@adfs.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$eXNkNTI1TW1PZi4zZlY0VA$Mr/mKUerdR91U81o/DndMLH2EEbHDM0uT8lSPNRcNYM', 'adkjsfl', 'adkjsfl', NULL, '1111-11-11', NULL, 100, NULL, NULL, 0, 0, 0, 'a0ee8f47-d14f-4805-8d06-78a21952fdf0', '2021-04-07 11:01:14', 0, NULL, NULL, '2021-04-06 11:01:14', '2021-04-06 11:01:14', NULL),
('95de8a66-d187-4a26-908a-de44fd1b9e46', 100, 'test104@tets.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$akhUQUQ5VGpEUGRESWR4QQ$XSqutO56BiIwZP+/5SIhTPH9da2f0nQdpxgzxHVEoAY', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-11 12:59:22', NULL, NULL),
('95eb8e89-6b1a-4843-bb85-c8932cef5613', 100, '3423@3123123.ru', '3333333', '$argon2i$v=19$m=65536,t=4,p=1$SXg4bWdBbDdKVlpJV0JqTQ$bWqPgCo6+Eou1/71PRpMqT4duewXWRVhwlSwcraieEA', '23123@3', '23123@3', 'rwerwe', '2021-03-17', 100, 100, NULL, NULL, 0, 0, 0, 'c6b16218-54c5-481a-a40b-499c8ed8d323', '2021-03-25 10:36:41', 0, NULL, NULL, '2021-03-24 10:36:41', '2021-03-24 10:36:41', NULL),
('979b0d5f-8806-4378-8313-1bc31d3fc0b1', 100, '75.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Vmt5QWFnRm9RcXU5UUxiaw$5UcMP1Elgzt8QF9k1VaKqxiTwE9vTtQ17qRW544JL2c', 'максимушкин', 'максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 12:01:56', NULL, NULL),
('98b8ae61-6b5d-49a0-876a-6321effe242a', 100, '77.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$UVJmY1F1OG5NUU9reWJZdg$zftMRDCVSNN+UEMLZDjSYlPrZOf5zPXDYcl1qOhoOdo', 'Онов', 'Онов', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 06:16:27', NULL, NULL),
('994064dd-1d99-4e15-97fa-77fb080b46f9', 100, 'didsoi@mail.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WFoxYWRHTTZ2WnRuSXRSMg$AfOhpAXXeoGdGrWTzZbRHJMoDVqD8SCknXqOtyaTrKc', 'dsfdf', 'апп', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, '2b2bd08b-2603-45a3-83b0-20d81aeb46bf', '2021-03-25 06:55:21', 0, NULL, NULL, '2021-03-24 06:55:21', '2021-03-24 06:55:54', NULL),
('9bd56fbe-6b3c-484b-84fe-2edd0f304272', 100, 'redozubova_nb@rosakhutor.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$V1FsNTBTS3FQSnVGUVFNRg$lEiSlh9/0LsaFVmoM2ayoRO78PVg9aGHoTboJOKiY2k', 'Ре', 'Наталья', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-09 06:36:08', NULL, NULL),
('9cc99095-fecc-4630-a3bc-210004571d8b', 100, '83.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$MnpWZS9PWEd1SHZtVU1tVQ$pc2/g5P63ugjT13kb5raBy6Y7KfZKiNDUajf759cGW0', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 09:57:51', NULL, NULL),
('9e75f55a-a02c-4691-83ab-dac571a806c1', 100, '65.zbrsk@mg.dreamdev.space', '12312312312', '$argon2i$v=19$m=65536,t=4,p=1$eUhsTTN2bzZSaVBoWjBiUA$e5GogOdkWS3HDFwvxaqlZ/Iz0NqxDV2J0BSw4rKCYRk', 'Петренко', 'Петренко', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 06:58:43', NULL, NULL),
('a32755c5-b77b-497e-904d-318cd1a6cd43', 100, 'fdg@yandex.ru', 'e344', '$argon2i$v=19$m=65536,t=4,p=1$d08wYzJnVUZrcUl1ZlB3bA$ymWn6tTxihWKhUHyf+sS5P3zFWPqUKS0EcI93ypwgR8', '5555555555555555555555555', '55555555', '5555555555555555555', '2021-03-17', 100, 100, NULL, NULL, 0, 0, 0, '90b76bb7-fca1-4adf-997a-2f13c6f0af5b', '2021-04-08 07:55:46', 0, NULL, NULL, '2021-04-07 07:55:46', '2021-04-07 07:55:46', NULL),
('a6f61ce6-f88d-464a-8c5f-dc3bb605d189', 100, '116.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$aW1YVWFtYTh1c1hnV1NxWQ$+smqr8JmaOzYwYg7CWQAkdDomDdLwXtJaQv98YAZeSU', 'Ильин', 'Илья', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 13:37:26', '2021-03-05 13:37:29', NULL),
('a936a449-c9dc-41f3-9d9d-4482bb50ac7e', 100, 'maksis9@yandex.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$OEZZZnRXTldJeTFreUJPRA$nU4aWlGkEVObhkA55aq5rlLvFfkLKvmHd1F6i5FA/UU', 'qwe', 'asd', 'zxc', NULL, 100, 100, NULL, NULL, 0, 0, 0, 'a4837087-d765-45b0-a27a-3588e3a1a9aa', '2021-04-07 13:59:40', 0, NULL, NULL, '2021-04-06 13:59:40', '2021-04-06 13:59:40', NULL),
('ac71579d-28b6-4ee3-8a2b-2bc1129fa17a', 100, 'sky1311ba@mail.ru', '9012d324', '$argon2i$v=19$m=65536,t=4,p=1$V3B2cktQNi9ZZnRIQ3RydA$Zgw7Mh+nPml/ygBEZi08au4QG6oFId7JpI+2gUrp6Lg', '5234', '5234', '43354', '2021-02-21', 100, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 18:00:49', NULL, NULL),
('aca6cf0f-937b-4aec-9a12-5dfdabcd71d4', 100, '42343421@4343.rt', '4234', '$argon2i$v=19$m=65536,t=4,p=1$SFVVcTNTTktiRUIxN0hnOA$OjR2TYPD08zVqNokw/sxenyf3PmcPdRogFpEeRXwoyM', '34232', '34232', '345', '2021-03-18', 100, 100, NULL, NULL, 0, 0, 0, '33b49ada-139d-43f8-9165-4db5b4913c70', '2021-03-25 10:52:53', 0, NULL, NULL, '2021-03-24 10:52:52', '2021-03-24 10:52:53', NULL),
('acb2f492-f370-44d3-86e1-4365f3899d97', 100, '89.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$a0hKNm1PM1gwbjF3YU1nVQ$kLDQ2QNDKjgD+fWZG7bhyOByBf2v1Tgk1d/2NNFo88A', 'Егоров', 'Егор', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 11:35:38', NULL, NULL),
('ad816613-d31e-430e-97fa-dc75eb3d3892', 100, 'aleksey+6@laletin.me', NULL, '$argon2i$v=19$m=65536,t=4,p=1$a0diMjRjSG5XMEVkUi9wdw$bjQtICaPGrFLhVnu2MYrcO1TJtbEk0Fs/Ypx+ZX4pOo', 'dsf', 'test', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-17 08:41:23', '2021-03-17 08:41:54', NULL),
('adaa3b0f-ddc6-4a3e-ba70-499ba62237a1', 100, 'sky1111ba@mail.ru', '901d324', '$argon2i$v=19$m=65536,t=4,p=1$TDUxRnZmUUR3elEwM3M0Yw$pjQBWyjK1n8KMdzPIuj022Y0wvX77u4cLWasaHCoiJw', 'Таня', 'Таня', 'string', '2021-02-21', 100, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-03 17:50:54', NULL, NULL),
('aebf148b-f443-4f39-8c49-9c7723708d42', 100, '1w1w@wewe1.ru', 'ewee', '$argon2i$v=19$m=65536,t=4,p=1$ZEVPSDgvazRVWHZXcDV4Yw$VFBQbpFNTq4lJYOjCAb6mlj1ndPNXV6vRfOyxQrIa5Y', '1w1w1', '1w1w1', '1w1w1w', '2021-02-16', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 14:29:29', NULL, NULL),
('af9f1a56-9f20-41be-80ac-5ec8dfd6e984', 100, '1212@2we.ru', '12121212', '$argon2i$v=19$m=65536,t=4,p=1$VHpLY2NSNmthZDl0SHJWMw$Xf/kLMrV7BgO+JQkqVlzbQzhrFU0ACL82+YQkLo+qNs', '1212', '1212', '112', '2021-02-10', 100, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 14:30:13', NULL, NULL),
('b06d3137-d3ce-4737-aaa2-3309a7d5d3a7', 100, 'pohare.pixel@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$T3JLLy5nNEhuYWNtMElQaw$0ROh9MTb1ZYqVcWImkqIVLZn9NcU7ZYIltzlf2NFOYk', 'ывы', 'ывыв', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 08:14:34', '2021-03-05 08:14:36', NULL),
('b0cf181a-e2d8-493f-bf65-bf1e3cd4a0b3', 100, 'aleksey@laletin.me', NULL, '$argon2i$v=19$m=65536,t=4,p=1$SEpNUk4wbXdhbUx1ZU9hSA$VVkWyRzG/LR5NlCnjjt/mlFdciq/LnCA1rwbnorwUf8', 'string', 'string', 'string', '2021-02-19', 100, 100, '452ee511-a2e6-4ba3-a631-e8aec54ad0b0', '2021-03-07 06:19:18', 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 10:17:31', '2021-03-06 06:19:19', NULL),
('b102f87b-43ed-4129-b62b-534c97dd1e31', 100, 'test@test46.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$ZW00b0Z6ZnBoM3NZODFPbg$eGF9cdenz/KNa0vMeyS00JnGAsLX2v51AdSpjwVLMwk', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 08:46:57', NULL, NULL),
('b2eccdc0-cc39-4605-97b3-399372965273', 100, 'test100@test.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$RWV3T1hMbTVOcm5USEpRcw$hxqM1Xe5wYkG1v6n1JMQqNqZ6Gqi+7NUZ764jdmCPt8', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-11 10:59:24', NULL, NULL),
('b2f7349c-c148-47d8-ac06-612e5f78762f', 100, 'maksis9@ya.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$QzFNUi9hUkF3VDI0bTFEcw$VUjTWCRI+6MVez3pni8cqYAvCVmBvzwi0U9I0il/CrQ', 'Qwe', 'Asd', 'Zxc', NULL, 100, 100, NULL, NULL, 0, 0, 0, '4851b611-a81b-4f33-8604-dffc8c766cc9', '2021-04-07 13:21:10', 0, NULL, NULL, '2021-04-06 13:21:10', '2021-04-06 13:21:10', NULL),
('b4cef9cd-b750-4eb1-b672-7ac8f17986c0', 100, '134.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bDVseElCMWhGTDI5RjloSw$stnG8DDEgiQgdqFe+Qu6o60qcFuH77qHxXAnEazSfIM', 'Зиновьева', 'Зина', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-15 05:42:29', NULL, NULL),
('b5d40428-ef2c-474a-b043-a84f773a104f', 100, 'DSFSDsaadF@MAIL.RU', NULL, '$argon2i$v=19$m=65536,t=4,p=1$R3VQWHhJOTNBYzQ0blZnWA$xxpPa9NLcHcfOaG2kbXSt07KJ47HNTH6gouOL/g4B+A', 'тест', 'тест', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, 'b1530f31-9546-4507-9fc9-1ff285743e36', '2021-03-07 14:27:56', 0, NULL, NULL, '2021-03-06 14:26:06', '2021-03-06 14:27:56', NULL),
('b6bfc2ea-7035-4c4b-a5b5-1831cd7be999', 100, 'hobeloj457@566dh.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$dXdna3UwakNSMUZiN29SNA$RGkFoKM75AT+skplUfhPAqL128z/aGcGjRYnS+JYnzw', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-04 08:40:51', NULL, NULL),
('b71a8ac9-025f-4935-8798-bb421cde80e8', 100, '23234@sada.df', NULL, '$argon2i$v=19$m=65536,t=4,p=1$ZjJ2NjNKSUd1VXRGQ3liLw$sk5q31NuhCrlDwZhHW45sx3yMMHIom1KQqf7Lv7olR8', 'asdasd', 'sdada', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-18 06:21:17', '2021-03-18 06:21:44', NULL),
('b74b0e14-40db-4189-8009-8e49eb0f6c8e', 100, 'sada@asda.sasda', NULL, '$argon2i$v=19$m=65536,t=4,p=1$NEh6SFRVbEJqRlZ1MTIyMg$8Q2OLI+e6dBVJ0JJhCwKlCS0cDY0pfVYRtTtVyPb83U', 'asdasd', 'wasda', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-18 06:18:42', '2021-03-18 06:19:09', NULL),
('b7c02953-d1b2-4bdb-86e1-a51361e39f89', 100, 'zlobin@zlobin.com', '9567587785', '$argon2i$v=19$m=65536,t=4,p=1$cWZ0aVgzV3JjVm4ucGk4dA$sDQPK365fBdzWBOJD9YwsXGnTLdT/GXF62oNTMzjoYQ', 'Злобин', 'Злобин', 'Сервеевич', '1996-03-07', NULL, 100, NULL, NULL, 0, 0, 0, 'fde46b0e-cadd-417d-ba7e-b0c9c4e45a3a', '2021-03-23 07:05:26', 0, NULL, NULL, '2021-03-22 07:05:26', '2021-03-22 07:05:26', NULL),
('b8cb3e0d-59e3-49f2-9067-9f6fe1f9ca9d', 100, '71.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$NGZlcThCZG9ZaVJ3V0JoVw$8gL44fK9/Ldb7Gv7g6GZgYS5WSAq5E91yumb1tXhWkQ', 'Максимов', 'Максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 11:48:26', NULL, NULL),
('b9fd77b2-27ea-41fd-9727-ab30822f1258', 100, '106.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$YVBFOFdpMm1aVnJiRXk0SQ$tozvQ7JG4XflqcN5bsPLGJ3ES8jar2jpSm+Zqn7BY8U', 'ДДД', 'ДД', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 06:43:55', '2021-03-05 06:43:58', NULL);
INSERT INTO `user` (`id`, `role`, `email`, `phone`, `password_hash`, `last_name`, `first_name`, `middle_name`, `birth_date`, `sex`, `status`, `reset_password_token_value`, `reset_password_token_expired_at`, `is_confirmed_service_letters`, `is_confirmed_mailing`, `is_email_verified`, `verify_email_token_value`, `verify_email_token_expired_at`, `is_phone_verified`, `verify_phone_token_value`, `verify_phone_token_expired_at`, `created_at`, `updated_at`, `level_id`) VALUES
('ba914421-c4ad-4d0a-ad19-f8cce344b3fa', 100, 'asdflj@aldf.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$TUtqSWQ2TDdRNDF2S2N5Nw$Y4qnfDrGqbxcJ0BDiwz0V8z5nrO6xCNIBj21F03G6C4', 'asdflj', 'asdflj', NULL, '1991-02-03', NULL, 100, NULL, NULL, 0, 0, 0, 'be7753c5-0a0f-4135-9725-228793685bc0', '2021-04-10 11:22:26', 0, NULL, NULL, '2021-04-09 11:22:26', '2021-04-09 11:22:44', NULL),
('bcacdb9d-2280-42e3-b8a6-a712ebc2e310', 100, 'ewrwer@324234.ru', '123123123123', '$argon2i$v=19$m=65536,t=4,p=1$M0Z2ZWZwQzl6clBuak5YZw$oByV61CmOsd2K7rik8eyKgP7a2fUlz+3TTLAP3A6ezs', '34324', '34324', '23', '2021-03-11', 100, 100, NULL, NULL, 0, 0, 0, 'fd5bd71c-0700-489c-becc-947394ef3b79', '2021-03-23 07:14:52', 0, NULL, NULL, '2021-03-22 07:14:52', '2021-03-22 07:14:52', NULL),
('befb80c4-87e3-4609-8eef-744c74dd0f29', 100, 'wre@ewer.tret', NULL, '$argon2i$v=19$m=65536,t=4,p=1$RC5hMGdUZkFCZS9zdmJqUw$2Dpr4RaFEe7P3Z28Hvs+hN7MV462oGBrYOHlYJJ5kYo', 'werw', 'wewr', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-17 12:25:20', '2021-03-17 12:28:45', NULL),
('bf841828-ce79-4560-b5c0-c3e82bf213fd', 100, 'test@34test.tu', NULL, '$argon2i$v=19$m=65536,t=4,p=1$TE5ISzJRYXF4bVNzcm9LSA$txRTjG8MI4sZhLHtBVfnsgh6OC2OWzw+lLNusmx7B8Y', 'Антонов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-19 10:43:49', NULL, NULL),
('c231656a-92e4-4ed6-b265-cb2b69c31700', 100, 'oa.zvonnikova@zbrsk.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$elUxL08uRFp1VjFqN2N6OA$Grf+QVeldWOgeQLms9QhUnNilwPUFOvDrJt5/tHXhZ0', 'Звонникова', 'Звонникова', NULL, NULL, 200, 100, NULL, NULL, 0, 0, 0, '70ba4ca0-6819-4486-adab-cc77f11d5f36', '2021-03-06 11:04:16', 0, NULL, NULL, '2021-03-05 10:56:54', '2021-03-05 11:05:14', NULL),
('c383e1b4-88a9-4c5a-afd2-26a2a8ebc869', 100, 'il.akhmadeyev@zbrsk.ru', '79123188468', '$argon2i$v=19$m=65536,t=4,p=1$OEhoZ2R2cDJka2c3TVp2SQ$A6S+otxzW89+hJuCITiw3mYDIlYvneXkBWu4P/ZNTUs', 'AKh', 'AKh', NULL, '2010-03-02', NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-26 09:50:17', NULL, NULL),
('c4fd6fdd-bf59-416c-b54d-3f20b05ef6fa', 100, 'erwerwe@mail.ru', '79154805578', '$argon2i$v=19$m=65536,t=4,p=1$ekcxLlRNVnN3S1F6dkQuSw$yWy/w4sqnDjkeUAgsMdg25Rrsinvhk1KwHC5ID4GfvU', 'выавыа', 'выавыа', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, '6dc76b17-b36d-4d72-80b4-e7ed151fba1c', '2021-03-12 14:27:42', '2021-03-11 14:27:03', '2021-03-11 14:27:42', NULL),
('c55be4c4-47dc-421b-b06e-752e613f7fa1', 100, 'test@tets.te', NULL, '$argon2i$v=19$m=65536,t=4,p=1$ZGhLcHhPMlhJY0xuWnRtVg$B11/hQiQTHBY8+ahAWapyOkVZ4vofzb6SFjWSdGYbzs', 'Апапап', 'Папапа', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 08:22:29', '2021-03-05 08:22:32', NULL),
('c589f048-a104-47f5-a679-febfaa57cb33', 100, '110.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cVpQdWJrcjFoalR5QzZIRw$BgsQTIfIi/1Gr9KMu0mybkL5DhvW2L0t5j5FqoIq5Mo', 'апрап', 'авпап', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 10:35:41', '2021-03-05 10:35:43', NULL),
('c6bca30b-2939-462c-9993-40e71c9a8e13', 100, 'test@test.ss', NULL, '$argon2i$v=19$m=65536,t=4,p=1$LjN1eWszZFp1ZFRVRDd4Mw$GC/P8sYOFJ+X/PyyJbubsyIM1K0VYM01Scvzx1WBaPk', 'aaa', 'aaaa', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-18 06:07:59', '2021-03-18 06:08:04', NULL),
('c799a607-9374-4912-9314-b4977c13e170', 100, 'fdgdfgdfgdf@mail.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$ZkxhVWk3bndjRlFHcjNXcw$43RItkaHEfL1oStPy95Q2EQ/xekLmx4PCbbJwOXZhB4', 'dsfdsf', 'sdfdsf', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 15:33:19', '2021-03-05 15:33:21', NULL),
('c8ab93ff-8d1a-4ed3-95f8-e42ee8117eea', 100, '102.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Lm5XTy9YcFp6MVQ5TS9Odw$JsPbbOx+q1dOArKgUg8b5BkgLrvsi1epdbm5sQ5gaZU', 'Редисов', 'Редисов', NULL, NULL, NULL, 100, NULL, NULL, 0, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 14:52:30', '2021-03-04 15:24:18', NULL),
('cc5dbe0d-7e23-41a7-ab60-ef955b3993fb', 100, '96.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Wk1LYVYxTThUQkpnRnNRcQ$Yivl9yR+o6kKuBjcIN+EmkWtqlf7u8n/eFL1/6+kCqE', 'Ивановски', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 10:19:10', '2021-03-02 10:19:12', NULL),
('cd8f1694-77e4-4842-87f0-7bf6fbe05e11', 100, '112.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$YTkvWktJeFlxWGZYZFFWTg$MU9qoPYQNe2DIuYM+AQizhl4Gw+zujOCmg4Ck4P2uRo', 'Ленина', 'Лена', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 11:42:38', '2021-03-05 11:42:41', NULL),
('cf878391-9432-4f19-9983-980c44c1d473', 100, '79.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bDJ0OXFIaWM1Z2JlQXBZMw$0+maxSMQcy4em55dW+eVKanWxOFXI9MPTytptCcvSVA', 'Олов', 'Олов', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 06:22:29', NULL, NULL),
('cfd400ef-772f-4b1e-a173-456bebc1eb7b', 100, '2565t@vmani.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$enNNczNvWGdia3owcEpaUA$uHJBjapMdKE0eBqtSGx5xtqcgFi4N0rcAL9gWxeIKYY', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, 'f402b222-02b3-4a9c-963b-a286e3cc1452', '2021-03-12 12:13:33', 0, NULL, NULL, '2021-03-11 12:12:59', '2021-03-11 12:13:33', NULL),
('d1fd2aa5-452c-4ae6-b9a5-018c52cfd128', 100, 'sdvvdddv@tets.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$REtLL2l4TDZuVVc3LmgwQQ$uHr3vkPYbhKvogMC/kHoSLbI/H41Z5nq19PNREknbeQ', 'dsf', 'test', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, '5a79684e-f36a-4af5-99c4-2df0a9629731', '2021-03-24 11:53:39', 0, NULL, NULL, '2021-03-23 11:53:39', '2021-03-23 11:54:11', NULL),
('d350c3dd-7295-45ac-8de6-8df9c4cc62de', 100, '132.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$S1hTS0NRcXp3SGJiRm91SA$jRtP/mmzkYxzpzWQ+/QqNlXtwt9lByjghuncPLSTjOk', 'парп', 'арпар', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-15 04:59:22', '2021-03-15 04:59:28', NULL),
('d46072ca-9c17-48bd-a82f-289f4c4550e8', 100, '146.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bDRtOGpRMGNUTmFLRWlRcg$82aUP/tX2scMYstTBTrUApAJ9agsGvMZcyBO64oHuZw', 'Котейкин', 'Миумиу', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-17 13:01:45', '2021-03-17 13:01:54', NULL),
('d54250af-941c-496d-97d6-c8ec867d47e9', 100, 'test@test.aa', '0010202102031', '$argon2i$v=19$m=65536,t=4,p=1$THpRN2RzbnpzQWJhWEh3UA$xEjwLqFYIJMnGCbUkE7SkOHU/9UizTAA9Ccaz6V4O1I', 'test', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, '170a618e-b0cc-409c-b8a4-0cea2eaaf67d', '2021-03-19 12:05:28', 0, NULL, NULL, '2021-03-18 12:05:28', '2021-03-18 12:05:28', NULL),
('d8002bff-daff-4b5f-9217-40662dd688eb', 100, 'sdvvv@tets.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$UGFtTFUxQ3M1bVZGQzBjaQ$2IRVyWd/fDAYpVzaNjkZWmXTUsApTHVgOMAQ2vc1PRo', 'dsf', 'test', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-16 15:31:12', '2021-03-16 15:31:17', NULL),
('d9540b4c-c20a-447b-b661-23e9aa703875', 100, '107.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$dXJxZ0tFclpER0lYQVFDLw$iK+P/eT5f6U/tkN28mPOLhmmMh0+UgWH8aI0v0Z7ymY', 'kkkk', 'kk', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 06:54:21', '2021-03-05 06:54:25', NULL),
('dbc7b0c2-b74e-4291-97e3-3837522a413a', 100, '1111111111111111@312312.rewr', '11111111111111111', '$argon2i$v=19$m=65536,t=4,p=1$L3ZmZTA1Tm1QemlMenZmWQ$LLiRFwDIiYtiagAugt3ltDR6cFBB1UTa9v0FHUizbrA', '3123123', '3123123', '111111111111111111', '2021-03-03', 100, 100, NULL, NULL, 1, 1, 0, '895c2598-d96d-43bf-8385-8b7d7e672aa3', '2021-03-19 11:30:30', 0, NULL, NULL, '2021-03-18 11:30:30', '2021-03-18 11:30:30', NULL),
('de075f06-ae46-459b-9260-8d37a3c2b108', 100, 'aldf@asfdj.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$djgxRFl3cjRPSmt5dW5iaQ$xX4RPfGd2g+AE19//fFbsjTl2FNLOXJVmivbHTlR6+Q', 'fsadkj', 'asdjkf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-16 08:28:36', NULL, NULL),
('de7a4145-b7ce-43b4-9210-e85bd45ad6cc', 100, 'hojese9687@igoqu.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$M0dUMXNjUFBQSTMzUHdVVg$6uwpNwo6bFFmU1k9wVJT/0VE37pcYg8bv8C3u61eFms', 'igor', 'igor', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 13:37:03', '2021-03-05 13:37:07', NULL),
('dff2a2ce-3c23-46b5-a18e-45766d7a6530', 100, 'test@fsd.ru', '79507370861', '$argon2i$v=19$m=65536,t=4,p=1$VjZxczd4VTRDODd2TjFQQQ$p3SnA42zSXJ83GojhLj45tL5tPMxb94A/03CXsOQkHw', 'sdf', 'sdf', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, '690c8638-818c-45c7-8b81-c475b31542e0', '2021-03-11 07:58:46', '2021-03-10 07:57:48', '2021-03-10 07:58:46', NULL),
('dffc3af2-7f2f-42c9-8c3d-a61c74b8368f', 100, '70.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$WGZ4aEk2MG5zSlV4SW5aVw$txqJHcvao30y4oy8NFuaKuZSdCfl9ffX8cwMieO5ITc', 'Олеговский', 'Олег', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 11:37:52', NULL, NULL),
('e1236b7f-c332-4a2b-84dc-44070d4dfa5c', 100, 'awewad@aad.eefsf', NULL, '$argon2i$v=19$m=65536,t=4,p=1$cTFQWnYwTTFkU0k4U0F1cw$iPSojuXnHnYKChxEoZuOZzI9U2kQPDNYdtRUdVx//rE', 'фывфв', 'цвфвцф', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-18 05:59:15', '2021-03-18 05:59:46', NULL),
('e362e4d0-58d8-4f6b-bdb0-a323b6f8ce00', 100, '95.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$SlZsVy4vemVFLlYwR3Z4Uw$y/s2iwTx8XYbkBD/FdDG3OsVnq1RkRGKA/YTXNVLuiA', 'Ивановски', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-02 09:10:25', '2021-03-02 09:10:29', NULL),
('e384fb37-b818-450c-96b9-a29e65ce0e73', 100, 'test@test47.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Y1dKVEdCb1hTUE1qM05NaA$RXZ77Gh6q4OHwW3M6nFCPbJztNav/e2eKdRusJvhIZQ', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 08:47:49', NULL, NULL),
('e3e8868f-56e6-4ea3-8dd5-c94f8494689e', 100, 'foo@bar.baz', '33333333', '$argon2i$v=19$m=65536,t=4,p=1$UVRDY1BsNTUvc1JqS2xnbg$nl/0TxTnpx37tquCzvkum54wXEzPZmickn7t7Y0uYWM', '3333333333333', '3333333333333', '3333333333333333', NULL, NULL, 100, NULL, NULL, 0, 0, 0, '23a80997-1c29-40b5-ba73-34d0885d333f', '2021-03-20 07:49:15', 0, NULL, NULL, '2021-03-19 07:49:15', '2021-03-19 07:49:15', NULL),
('e633abd5-048d-4400-b387-03e48cc75b52', 100, 'test@test45.ru', NULL, '$argon2i$v=19$m=65536,t=4,p=1$eEZUa2dPMDF5d28yZGM0Nw$G4uv9WwH87KoZ26UqzOSIzx0Uc34sS23XDtcCmK69nc', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-25 08:36:31', NULL, NULL),
('e80f9a5a-a674-42a7-a4e8-1c9988b7f8dc', 100, '42.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$ZGlGRTh5Y1BvcVU4MHNNag$29IIV7bdZt4ctyNqkJQqrTJ3kMrvZoTLB4pJLnceNg0', 'Иванов', 'Иван', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-08 14:32:31', NULL, NULL),
('e9c3b88c-0bed-49e8-857d-b6aa4fa1fa05', 100, '114.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$LkxxWmFNZmhEcWtQQ0hCcw$vDHoRdfMSSkkQmNzodyN+4E98TeNsj8maGT7aIjRdw8', 'Иванов', 'Иванов', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 13:29:25', NULL, NULL),
('ebab9bb0-aeba-4943-81a4-c3162f3394f0', 100, 'aleksey+3@laletin.me', NULL, '$argon2i$v=19$m=65536,t=4,p=1$Llp2eFdwZENnY3U2RGo4aA$Axn//X+NAztR5xd2qHYGv4k+bDu3VR102kvzlJf7X/A', 'hi', 'hi', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-12 14:38:42', '2021-03-12 14:38:47', NULL),
('edc9b6e6-fc78-4290-9823-d90f144ad1ae', 100, '144.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$VW5XanUvMnBBN0dOVzZmTw$OuzzNof04vMAdgJC0J3vVMPSzCPZY0ceaUoFojLJwOE', 'Иванов', 'Иллиарион', NULL, NULL, NULL, 200, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-17 11:08:22', '2021-03-17 12:52:57', NULL),
('efd67efa-2fcf-4287-9561-b2963ff5a2bd', 100, '105.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$bE4xekR0d0c2YXNuQWo4Zw$bXf58hZUfpaD1khZoSz/MRem2dCPLUtoI1E0d7rBtfE', 'Дамиров', 'Дамир', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 06:38:44', '2021-03-05 06:38:48', NULL),
('f226c887-e709-4656-a0b8-282e3b6c97fa', 100, 'sadfl@sdf.ru', '1111111111111', '$argon2i$v=19$m=65536,t=4,p=1$akJUb2VsUHVZR3QwbE1yRw$F7jpTPn8yjCBneYzVrFr6pFrAd/0IG3/GqcVfTZ9uls', 'sadfl', 'sadfl', NULL, '2000-01-23', NULL, 100, NULL, NULL, 0, 0, 0, '54fc47c9-4c71-4b79-b0cc-e65acb6cdc71', '2021-04-07 11:20:37', 0, NULL, NULL, '2021-04-06 11:20:37', '2021-04-06 11:20:37', NULL),
('f6ee88b9-936c-4242-8155-4c65a456686c', 100, 'skyd2oba@mail.ru', '9024', '$argon2i$v=19$m=65536,t=4,p=1$Lk43aXRUUnZHVlhFYm96VQ$kUFu2+Dmv5+nyCW2WobBBgJhe2KTys7z8FiSLstTUw0', 'Таня', 'Таня', 'string', '2000-02-02', 100, 100, NULL, NULL, 1, 0, 0, NULL, NULL, 0, NULL, NULL, '2021-02-02 17:29:57', NULL, NULL),
('f96c900d-8215-4f63-a2fb-427a08ffad34', 100, '73.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$RWtzUXZ3cklUQWFTczd4ZA$UWP3HLr936y+Pb8RWRVyDZeOgtAqY0T0+OUXnQ0ujrg', 'максимушкин', 'максим', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-24 11:59:08', NULL, NULL),
('fa680f9e-62a8-4a7b-a91c-a099d852bd01', 200, 'vovanhikkan@gmail.com', NULL, '$argon2i$v=19$m=65536,t=4,p=1$eEVZV08yUGlTYno0REloZA$ajITTrHzEGc9OWC/iw2v4MrR91aObxExbVWQGQqt090', 'test1', 'test', NULL, NULL, NULL, 100, NULL, NULL, 0, 0, 0, '55ba468e-5242-4c74-aeb0-60bb913acba4', '2021-03-25 12:49:55', 0, NULL, NULL, '2021-03-24 12:49:55', '2021-03-24 12:49:55', NULL),
('fcfcb4a3-9b19-4d0f-a2c8-83fafe3c583c', 100, 'www@adas.esfsf', NULL, '$argon2i$v=19$m=65536,t=4,p=1$NmVKVzBBQ0FvWk1id0xJLw$wBz+P7J116+6I5SHjSSS4ispY1nfy7hRP8BSnCAxfW0', 'www', 'www', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-18 06:08:56', '2021-03-18 06:09:02', NULL),
('fdf04558-40f5-4090-9242-0e8277feae81', 100, '108.zbrsk@mg.dreamdev.space', NULL, '$argon2i$v=19$m=65536,t=4,p=1$MW54VGpvbnF4YjJHMzdBVA$lKLeAQrmMtKtsNID7fPRDUIXFbDRWayLLAHicX+6Mkw', 'ара', 'апрапр', NULL, NULL, NULL, 100, NULL, NULL, 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-03-05 09:21:40', '2021-03-05 09:21:43', NULL),
('ff7724fe-9e1b-4f4a-9171-6e783b6b23c5', 100, 'user@example.com', 'string', '$argon2i$v=19$m=65536,t=4,p=1$ZnVOaEFVeklabU1vc29mbQ$lLqcVe2QVexHFIMw4PLR/r2nAmh/h6zwx3Nvif+qpu0', 'string', 'string', 'string', '2021-02-02', 100, 100, 'f59a4e87-d3e5-4d1e-95f5-fc7db54cebcb', '2021-02-03 17:33:11', 1, 1, 0, NULL, NULL, 0, NULL, NULL, '2021-02-02 17:15:02', '2021-02-02 17:33:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_refresh_token`
--

CREATE TABLE `user_refresh_token` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `token_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_expired_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_refresh_token`
--

INSERT INTO `user_refresh_token` (`id`, `user_id`, `token_value`, `token_expired_at`) VALUES
('007308b6-ceb2-4c31-95ea-507a27ea9f20', '6b250f66-687c-4343-8930-95ddadef3df4', '84551ef4-a3aa-4ff6-97b8-50cd43c4693b', '2022-03-05 12:05:29'),
('0288defd-9e10-43c2-8ca5-085a6f05a181', 'e1236b7f-c332-4a2b-84dc-44070d4dfa5c', '560b6e80-e0b2-4c2e-873d-05a4d8d60291', '2022-03-18 05:59:45'),
('06947f1e-1405-44bc-b8f0-210c65245c80', 'ebab9bb0-aeba-4943-81a4-c3162f3394f0', '12414131-7d5f-40b4-ad9e-5e72912af8c6', '2022-03-12 14:38:47'),
('07fd3822-8a69-4873-9914-009026089d52', 'efd67efa-2fcf-4287-9561-b2963ff5a2bd', '4973c9ae-065d-4df9-8318-0d70ebeb10b7', '2022-03-05 06:38:47'),
('099cf597-6096-4a51-81bb-df0a2bfefd62', '2a23cd11-66d1-4c8f-9c81-60992e8fbe14', '3b48a3e5-ba40-4627-b825-e0d5e1b7fe29', '2022-03-05 15:17:07'),
('0a373f2c-1eef-46cb-9794-b9c6491cacba', '71d3b61a-1a63-45d3-bb2d-cf86dcc007e5', '0a7de4bc-9e54-4de8-805d-1ec4cd83d5d1', '2022-03-06 14:25:56'),
('112b4837-c51c-4595-9f72-df457ef34b67', '843e18a3-e0cb-4ae3-a584-19b7f20d7c59', '86cdcee9-f8d5-40d7-a41c-c52d514c4e38', '2022-03-10 05:45:16'),
('192941c3-3a35-448f-9bdd-8184f87b4222', '247bed1f-68ab-4a7c-976c-2c06335e5898', '7f4c5443-b144-43e1-870b-e9ee68c6cf35', '2022-03-05 06:32:01'),
('21411ba4-82b5-4603-857d-9cb9f0d8c125', 'c55be4c4-47dc-421b-b06e-752e613f7fa1', 'e1b2e008-0745-4824-9bb8-2886cae29198', '2022-03-05 08:22:31'),
('25f64fe9-0835-4120-812d-195bf725a043', 'b5d40428-ef2c-474a-b043-a84f773a104f', '7881a93f-69dd-400d-b1c5-e0be9ffc31c8', '2022-03-06 14:26:11'),
('30ddab08-1b9f-4f9d-87eb-b009a4f8aa0b', '65c7c0c7-226f-4631-a89e-f471e1da4c2f', '2083eef3-79bc-41d3-bab4-457e3c459481', '2022-03-16 15:31:41'),
('34e6615c-490c-4a15-9a0d-90b2ba8c1ec6', 'fcfcb4a3-9b19-4d0f-a2c8-83fafe3c583c', 'f21036c6-4687-4cfc-85b5-cf565cae7e12', '2022-03-18 06:09:02'),
('40e8fdd8-cc57-4c98-8814-64f06618adb2', '734000dc-8067-46c2-b845-32130aa74cc6', '5e7de216-b469-464e-997d-c5ca25e3f33f', '2022-03-09 06:47:47'),
('41e4ea06-b473-4387-be74-6a965def8c91', '324d18f1-8035-4ecd-85c2-3a88fe13063f', '4b07d07b-efb9-4eef-ab29-c0bc1a4351ac', '2022-03-06 14:38:20'),
('456a5e8e-9d25-46ec-aafd-004f643bf88a', 'd350c3dd-7295-45ac-8de6-8df9c4cc62de', '4022715f-f607-41d0-b5ed-9129940c1631', '2022-03-15 04:59:28'),
('4a6ab09d-47f9-432d-9136-36cb8dccc12b', 'd9540b4c-c20a-447b-b661-23e9aa703875', '1d377ae8-8bd4-48f9-a5d8-69616d5cfbf1', '2022-03-05 06:54:24'),
('4e041b05-d4c0-4230-b386-19bdae86347e', '27b9eeed-aee2-4f1f-ad13-59abdc97c819', '266baf99-ed5b-42af-a6c7-6e5f6c05901e', '2022-03-05 08:27:27'),
('4e88dcb4-2085-4368-a432-09f0365191a0', 'c4fd6fdd-bf59-416c-b54d-3f20b05ef6fa', '5274fda8-c5d0-4cb7-9716-273795ce6329', '2022-03-11 14:27:06'),
('5036947c-99e1-498a-8d86-2affc316c98e', '395fb206-2304-4af1-bab9-2f780136812e', 'f852344e-6490-4883-bb9f-d9e5325abc80', '2022-03-05 10:44:43'),
('5f6ad591-8749-4a81-8f07-da49c626cf53', 'a6f61ce6-f88d-464a-8c5f-dc3bb605d189', '47df8c56-b9ae-4148-8edc-b81d420063cb', '2022-03-05 13:37:29'),
('6474efa1-0e3f-4440-85e3-c918eb5871c5', '33826dfc-3fce-457f-b9da-6d4aa8fff655', 'f2fccd5d-7417-4d60-9fd6-f23d6117d850', '2022-03-18 06:06:56'),
('64e06445-546f-4ba9-93d9-8acead46c707', '994064dd-1d99-4e15-97fa-77fb080b46f9', '0173c113-3f95-4a51-b8a8-2f65e93c27e0', '2022-03-24 06:55:53'),
('6566d2db-b335-418f-9cbc-283107d787fd', '53b70f6d-ddc6-456d-91ca-96d80052660f', 'c9455b77-b915-4953-abd7-dd1ffb1454d3', '2022-03-10 08:59:21'),
('667eebf0-a72c-4491-80c0-33319a9b9118', '2f5121e9-77d0-41d3-9e9e-1494b979469b', 'b6bf350c-805b-4eb9-8691-97542964ee16', '2022-03-09 06:50:42'),
('66aa53c0-d087-4d99-98e3-7d464b0841f6', 'cc5dbe0d-7e23-41a7-ab60-ef955b3993fb', '175d0116-9b4b-42da-a414-ecc328e96e54', '2022-03-02 10:19:11'),
('71802a6c-1f00-497c-90ae-5762b558568e', '250eaf6c-7a5d-4bd9-923d-47be78439196', '8fb2ad31-8dc1-4911-99e5-ca6d0183be62', '2022-03-02 14:35:29'),
('77d6f18f-8ae9-49cb-bdfb-06201a94d525', 'd1fd2aa5-452c-4ae6-b9a5-018c52cfd128', '2b64f1d4-f892-43cb-8447-888e7476b230', '2022-03-23 11:54:09'),
('7875b96b-cf41-40bc-b450-e0701a3c0e39', 'befb80c4-87e3-4609-8eef-744c74dd0f29', 'd0421d4f-9eb0-4fd4-99aa-9f91098cb6da', '2022-03-17 12:25:26'),
('7c53129e-2324-4133-8575-f7df4ba11c91', 'b06d3137-d3ce-4737-aaa2-3309a7d5d3a7', '977acf5b-cb0d-4706-8e8a-143ad9252dd0', '2022-03-05 08:14:36'),
('80d5aed3-f387-4d8a-945f-45b1a1ed8576', 'ad816613-d31e-430e-97fa-dc75eb3d3892', 'fb0b4417-e280-41d0-af36-3822f98fe6be', '2022-03-17 08:41:54'),
('81f2f611-f0a4-4687-937f-a846701fca18', 'd8002bff-daff-4b5f-9217-40662dd688eb', '26c72178-a9d6-4631-b434-565e75b982cb', '2022-03-16 15:31:17'),
('859fe1f8-dcbf-40d2-b7b7-8cae684553aa', '88ff3a09-832e-4693-8b33-78d46288495c', '290a9289-e39e-482e-8974-a8a7ee25ffaa', '2022-03-02 14:46:28'),
('88fdbfa1-666e-4b6f-be7b-811d3e2fde17', 'c589f048-a104-47f5-a679-febfaa57cb33', '9fa71418-c734-4c5f-a28b-ee4e179c19b7', '2022-03-05 10:35:43'),
('8b1f7a25-623a-4c70-a12e-391c8cbf933f', '12a66a06-a4a5-49a4-8d41-a24bd9e8b0c2', '308cf758-e495-40f0-99f0-888baf0162a4', '2022-03-02 13:48:10'),
('8f59df4d-c6dc-42f2-b339-74bd08b35c91', 'b71a8ac9-025f-4935-8798-bb421cde80e8', '3f6ceb63-1159-4f08-be97-200303798039', '2022-03-18 06:21:43'),
('963fd78d-6503-408b-92c5-76be86aaabef', '684b0537-5623-460e-8919-3c2fc7b5dcbd', '614c1cea-09fb-4969-8b9d-6634636cb96f', '2022-03-02 14:27:17'),
('97180ff1-e82f-4c4c-9773-1bc31e02e200', 'de7a4145-b7ce-43b4-9210-e85bd45ad6cc', 'b6fbd54f-39c6-4a4e-a166-fe16d5d4881b', '2022-03-05 13:37:06'),
('9e976eab-a59b-485e-9357-3b0ee6e8d7f6', 'b74b0e14-40db-4189-8009-8e49eb0f6c8e', '84839022-ddc8-4cc2-877e-310bb870a98d', '2022-03-18 06:19:09'),
('a57a8e0b-24e5-4b6d-9adb-5d18f07bf204', 'b9fd77b2-27ea-41fd-9727-ab30822f1258', 'd3ea35a3-bc74-43d7-9d66-3bbb0754cbac', '2022-03-05 06:43:58'),
('a8f2cc4e-1d97-4411-a28f-5789e17b86e6', '89639e92-638d-4135-bbfe-ac88ba455ccd', 'b5978a4d-8998-4995-bf82-d10275fa997a', '2022-03-05 15:22:44'),
('a94eff54-e125-47de-9600-3b53327dccba', '6e4549c6-b9db-4602-85b6-a0bf3ddffb56', '92b26fea-ba82-40f5-8a23-5b42d5e2ffc5', '2022-03-05 14:38:49'),
('ba9d4834-5c5d-4d27-9b70-6d8adf3a0b7a', 'cd8f1694-77e4-4842-87f0-7bf6fbe05e11', '277d5eb3-ecab-4204-9b08-d431f31ba87f', '2022-03-05 11:42:41'),
('bb1c8d25-868f-465b-abd9-d614253cd7eb', 'fdf04558-40f5-4090-9242-0e8277feae81', '5e1c4ae1-eac7-4826-9e67-8f402d525ed7', '2022-03-05 09:21:43'),
('c0e37edb-d095-474d-94b7-711e49b5d150', '642837b7-6846-4d0c-a7ce-b0bc909d6c58', '717b9a36-87f5-4569-8b96-4ca665e7e09e', '2022-03-05 13:46:09'),
('c69afa68-1d7c-4290-b0ba-048fa9edfc1f', 'c6bca30b-2939-462c-9993-40e71c9a8e13', 'f0521ccd-cf6b-4028-94cc-b3d7535ec8ec', '2022-03-18 06:08:04'),
('cba81fa1-c7ce-49e7-a06c-0ff5559d814a', 'edc9b6e6-fc78-4290-9823-d90f144ad1ae', '49bfa49b-5111-41d3-b050-aa743a373052', '2022-03-17 11:08:49'),
('cd6a738b-47ae-484f-8985-f74de4cdff16', '7f6d0f8e-82e2-4c18-8e78-d74fad31959b', 'c7c4560e-0666-4faf-8596-f73f04768041', '2022-03-11 14:33:04'),
('d1a4d555-2ff6-43c8-97b8-710c514c9e38', '6d5e86d5-afbb-4826-a0ef-3a892ea27ad1', '5cc96703-39cf-4419-b61a-e62c8f4e33f5', '2022-03-02 14:23:09'),
('d8307d1c-8b5b-4cbe-be61-77c3c347c646', 'e362e4d0-58d8-4f6b-bdb0-a323b6f8ce00', '185d8e61-9546-4c90-b247-d323b2c4406e', '2022-03-02 09:10:28'),
('ec7aeb66-6f17-4d15-880a-c6a1de19bae5', 'd46072ca-9c17-48bd-a82f-289f4c4550e8', '01686d3f-1569-4426-93c2-84fd1482ff9a', '2022-03-17 13:01:53'),
('ee0a73ee-bd7a-4837-ab75-9f0e98426e99', '87f59790-51cc-447d-b6fe-273ff067edd9', 'b251ff27-c355-44e8-99cf-9a8a6b1fd37d', '2022-03-06 06:20:15'),
('f0beaf33-795c-4de3-9526-fe900f6b8aa0', '6a199751-6db8-4fc0-a2ec-160c601cc50c', '7dca6cea-6ef3-4752-8390-0e750590df21', '2022-03-05 15:06:26'),
('fb1a35f3-e8a6-4293-a391-96a96366d01e', 'c799a607-9374-4912-9314-b4977c13e170', 'cfa108af-69b3-4f8c-81cf-e0f3eb95a0c1', '2022-03-05 15:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `youtube_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background_image_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_47CC8C92FE54D947` (`group_id`);

--
-- Indexes for table `actions_rate_plans`
--
ALTER TABLE `actions_rate_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `rate_plan_id` (`rate_plan_id`);

--
-- Indexes for table `action_group`
--
ALTER TABLE `action_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `action_level`
--
ALTER TABLE `action_level`
  ADD PRIMARY KEY (`action_id`,`level_id`),
  ADD KEY `IDX_7D2400AF9D32F035` (`action_id`),
  ADD KEY `IDX_7D2400AF5FB14BA7` (`level_id`);

--
-- Indexes for table `action_product`
--
ALTER TABLE `action_product`
  ADD PRIMARY KEY (`action_id`,`product_id`),
  ADD KEY `IDX_9E88C4C9D32F035` (`action_id`),
  ADD KEY `IDX_9E88C4C4584665A` (`product_id`);

--
-- Indexes for table `bed_type`
--
ALTER TABLE `bed_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_AADE868EA196242E` (`web_icon`),
  ADD KEY `IDX_AADE868E87A63139` (`mobile_icon`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `budget`
--
ALTER TABLE `budget`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bundle`
--
ALTER TABLE `bundle`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_A57B32FD77153098` (`code`),
  ADD KEY `IDX_A57B32FD8AD350AB` (`food_type_id`),
  ADD KEY `IDX_A57B32FD3D080E5C` (`web_background`);

--
-- Indexes for table `bundle_bundle_item`
--
ALTER TABLE `bundle_bundle_item`
  ADD PRIMARY KEY (`bundle_id`,`bundle_item_id`),
  ADD KEY `IDX_776F28CBF1FAD9D3` (`bundle_id`),
  ADD KEY `IDX_776F28CBE497EB27` (`bundle_item_id`);

--
-- Indexes for table `bundle_item`
--
ALTER TABLE `bundle_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_236C3EDE7E9E4C8C` (`photo_id`),
  ADD KEY `IDX_236C3EDEDA6A219` (`place_id`);

--
-- Indexes for table `bundle_rate_plan`
--
ALTER TABLE `bundle_rate_plan`
  ADD PRIMARY KEY (`bundle_id`,`rate_plan_id`),
  ADD KEY `IDX_6E1ED61DF1FAD9D3` (`bundle_id`),
  ADD KEY `IDX_6E1ED61D325B4D7A` (`rate_plan_id`);

--
-- Indexes for table `bundle_term`
--
ALTER TABLE `bundle_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9978FC4DF1FAD9D3` (`bundle_id`);

--
-- Indexes for table `card_type`
--
ALTER TABLE `card_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BA388B7A76ED395` (`user_id`);

--
-- Indexes for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F0FE25271AD5CDBF` (`cart_id`),
  ADD KEY `IDX_F0FE25274584665A` (`product_id`);

--
-- Indexes for table `cart_promocode`
--
ALTER TABLE `cart_promocode`
  ADD PRIMARY KEY (`cart_id`,`promocode_id`),
  ADD KEY `IDX_A68649991AD5CDBF` (`cart_id`),
  ADD KEY `IDX_A6864999C76C06D9` (`promocode_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_64C19C1BC1679AC` (`web_icon_id`),
  ADD UNIQUE KEY `UNIQ_64C19C1B4131AC7` (`mobile_icon_id`),
  ADD UNIQUE KEY `UNIQ_64C19C116D3E4EB` (`mobile_background_id`);

--
-- Indexes for table `category_place`
--
ALTER TABLE `category_place`
  ADD PRIMARY KEY (`category_id`,`place_id`),
  ADD KEY `IDX_9C2E1C8412469DE2` (`category_id`),
  ADD KEY `IDX_9C2E1C84DA6A219` (`place_id`);

--
-- Indexes for table `category_section`
--
ALTER TABLE `category_section`
  ADD PRIMARY KEY (`category_id`,`section_id`),
  ADD KEY `IDX_EAAB3A9112469DE2` (`category_id`),
  ADD KEY `IDX_EAAB3A91D823E37A` (`section_id`);

--
-- Indexes for table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `extra_place_types`
--
ALTER TABLE `extra_place_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_105994B2A85760C8` (`facility_group_id`),
  ADD KEY `IDX_105994B287A63139` (`mobile_icon`),
  ADD KEY `IDX_105994B2A196242E` (`web_icon`);

--
-- Indexes for table `facility_group`
--
ALTER TABLE `facility_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3E5F78AF87A63139` (`mobile_icon`),
  ADD KEY `IDX_3E5F78AFA196242E` (`web_icon`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_type`
--
ALTER TABLE `food_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3535ED964D218E` (`location_id`),
  ADD KEY `IDX_3535ED9FE616A99` (`logo_svg`),
  ADD KEY `IDX_3535ED9E48E9A13` (`logo`),
  ADD KEY `IDX_3535ED9C54C8C93` (`type_id`),
  ADD KEY `IDX_3535ED9694027A` (`hotel_brand_id`),
  ADD KEY `IDX_3535ED98AD350AB` (`food_type_id`);

--
-- Indexes for table `hotel_age_range`
--
ALTER TABLE `hotel_age_range`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B8852D1D3243BB18` (`hotel_id`);

--
-- Indexes for table `hotel_bed_type`
--
ALTER TABLE `hotel_bed_type`
  ADD PRIMARY KEY (`hotel_id`,`bed_type_id`),
  ADD KEY `IDX_E8BF54FC3243BB18` (`hotel_id`),
  ADD KEY `IDX_E8BF54FC8158330E` (`bed_type_id`);

--
-- Indexes for table `hotel_extra_place_type`
--
ALTER TABLE `hotel_extra_place_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_81FFED1F3243BB18` (`hotel_id`);

--
-- Indexes for table `hotel_extra_place_type_age_range`
--
ALTER TABLE `hotel_extra_place_type_age_range`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5EA6A6BBB7E943D0` (`hotel_extra_place_type_id`),
  ADD KEY `IDX_5EA6A6BBCF47D8EB` (`hotel_age_range_id`);

--
-- Indexes for table `hotel_facility`
--
ALTER TABLE `hotel_facility`
  ADD PRIMARY KEY (`hotel_id`,`facility_id`),
  ADD KEY `IDX_523846C03243BB18` (`hotel_id`),
  ADD KEY `IDX_523846C0A7014910` (`facility_id`);

--
-- Indexes for table `hotel_photo`
--
ALTER TABLE `hotel_photo`
  ADD PRIMARY KEY (`hotel_id`,`photo_id`),
  ADD KEY `IDX_F7634ADC3243BB18` (`hotel_id`),
  ADD KEY `IDX_F7634ADC7E9E4C8C` (`photo_id`);

--
-- Indexes for table `hotel_place`
--
ALTER TABLE `hotel_place`
  ADD PRIMARY KEY (`hotel_id`,`place_id`),
  ADD KEY `IDX_97C99D093243BB18` (`hotel_id`),
  ADD KEY `IDX_97C99D09DA6A219` (`place_id`);

--
-- Indexes for table `hotel_services`
--
ALTER TABLE `hotel_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `photo` (`photo`);

--
-- Indexes for table `hotel_service_quota_values`
--
ALTER TABLE `hotel_service_quota_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotelServiceId` (`hotelServiceId`);

--
-- Indexes for table `hotel_tag`
--
ALTER TABLE `hotel_tag`
  ADD PRIMARY KEY (`hotel_id`,`tag_id`),
  ADD KEY `IDX_7B0304FD3243BB18` (`hotel_id`),
  ADD KEY `IDX_7B0304FDBAD26311` (`tag_id`);

--
-- Indexes for table `hotel_type`
--
ALTER TABLE `hotel_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_video`
--
ALTER TABLE `hotel_video`
  ADD PRIMARY KEY (`hotel_id`,`video_id`),
  ADD KEY `IDX_9F1314E83243BB18` (`hotel_id`),
  ADD KEY `IDX_9F1314E829C1004E` (`video_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F5299398A76ED395` (`user_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_52EA1F098D9F6D38` (`order_id`),
  ADD KEY `IDX_52EA1F094584665A` (`product_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6D28840D8D9F6D38` (`order_id`);

--
-- Indexes for table `pay_system`
--
ALTER TABLE `pay_system`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_40D6DBC43DA5256D` (`image_id`);

--
-- Indexes for table `periods`
--
ALTER TABLE `periods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_741D53CDC93D69EA` (`background_id`);

--
-- Indexes for table `place_block`
--
ALTER TABLE `place_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2A6D8C017E9E4C8C` (`photo_id`),
  ADD UNIQUE KEY `UNIQ_2A6D8C0129C1004E` (`video_id`),
  ADD KEY `IDX_2A6D8C01DA6A219` (`place_id`);

--
-- Indexes for table `place_geo_point`
--
ALTER TABLE `place_geo_point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C33A5D76DA6A219` (`place_id`);

--
-- Indexes for table `place_photo`
--
ALTER TABLE `place_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BDC19F3BDA6A219` (`place_id`),
  ADD KEY `IDX_BDC19F3B93CB796C` (`file_id`);

--
-- Indexes for table `place_section`
--
ALTER TABLE `place_section`
  ADD PRIMARY KEY (`place_id`,`section_id`),
  ADD KEY `IDX_19BF6D14DA6A219` (`place_id`),
  ADD KEY `IDX_19BF6D14D823E37A` (`section_id`);

--
-- Indexes for table `place_video`
--
ALTER TABLE `place_video`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_D5B1C10F29C1004E` (`video_id`),
  ADD KEY `IDX_D5B1C10FDA6A219` (`place_id`);

--
-- Indexes for table `pps_groups`
--
ALTER TABLE `pps_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pps_periods`
--
ALTER TABLE `pps_periods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pps_prices`
--
ALTER TABLE `pps_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pps_products`
--
ALTER TABLE `pps_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D34A04ADD823E37A` (`section_id`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6B9459854584665A` (`product_id`);

--
-- Indexes for table `product_settings`
--
ALTER TABLE `product_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2EF5CA524584665A` (`product_id`);

--
-- Indexes for table `promocode`
--
ALTER TABLE `promocode`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `value` (`value`),
  ADD KEY `IDX_7C786E069D32F035` (`action_id`);

--
-- Indexes for table `quota`
--
ALTER TABLE `quota`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6C1C0FEDFE54D947` (`group_id`);

--
-- Indexes for table `quota_group`
--
ALTER TABLE `quota_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quota_product`
--
ALTER TABLE `quota_product`
  ADD PRIMARY KEY (`quota_id`,`product_id`),
  ADD KEY `IDX_6149FD2554E2C62F` (`quota_id`),
  ADD KEY `IDX_6149FD254584665A` (`product_id`);

--
-- Indexes for table `quota_value`
--
ALTER TABLE `quota_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E4C7F14654E2C62F` (`quota_id`);

--
-- Indexes for table `rate_plan`
--
ALTER TABLE `rate_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_225EE9723243BB18` (`hotel_id`),
  ADD KEY `IDX_225EE9728AD350AB` (`food_type_id`),
  ADD KEY `IDX_225EE97284042C99` (`tax_type_id`),
  ADD KEY `IDX_225EE972FE54D947` (`group_id`);

--
-- Indexes for table `rate_plans`
--
ALTER TABLE `rate_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `groupId` (`groupId`),
  ADD KEY `foodTypeId` (`foodTypeId`),
  ADD KEY `taxTypeId` (`taxTypeId`);

--
-- Indexes for table `rate_plans_rooms`
--
ALTER TABLE `rate_plans_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratePlanId` (`ratePlanId`),
  ADD KEY `roomId` (`roomId`);

--
-- Indexes for table `rate_plan_group`
--
ALTER TABLE `rate_plan_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rate_plan_groups`
--
ALTER TABLE `rate_plan_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rate_plan_price`
--
ALTER TABLE `rate_plan_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_33C52D3E325B4D7A` (`rate_plan_id`),
  ADD KEY `IDX_33C52D3E54177093` (`room_id`),
  ADD KEY `IDX_33C52D3E68A2C92D` (`hotel_extra_place_type_age_range_id`);

--
-- Indexes for table `rate_plan_room`
--
ALTER TABLE `rate_plan_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C64FD907325B4D7A` (`rate_plan_id`),
  ADD KEY `IDX_C64FD90754177093` (`room_id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5399B6454C3A3BB` (`payment_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_729F519B3243BB18` (`hotel_id`);

--
-- Indexes for table `room_facility`
--
ALTER TABLE `room_facility`
  ADD PRIMARY KEY (`room_id`,`facility_id`),
  ADD KEY `IDX_B0BF547654177093` (`room_id`),
  ADD KEY `IDX_B0BF5476A7014910` (`facility_id`);

--
-- Indexes for table `room_main_place_variant`
--
ALTER TABLE `room_main_place_variant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BCAC49D054177093` (`room_id`);

--
-- Indexes for table `room_photo`
--
ALTER TABLE `room_photo`
  ADD PRIMARY KEY (`room_id`,`photo_id`),
  ADD KEY `IDX_5E0B25B354177093` (`room_id`),
  ADD KEY `IDX_5E0B25B37E9E4C8C` (`photo_id`);

--
-- Indexes for table `room_video`
--
ALTER TABLE `room_video`
  ADD PRIMARY KEY (`room_id`,`video_id`),
  ADD KEY `IDX_367B7B8754177093` (`room_id`),
  ADD KEY `IDX_367B7B8729C1004E` (`video_id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2D737AEFEE45BDBF` (`picture_id`),
  ADD KEY `IDX_2D737AEFBC1679AC` (`web_icon_id`),
  ADD KEY `IDX_2D737AEF93BFB765` (`web_background_id`),
  ADD KEY `IDX_2D737AEFB4131AC7` (`mobile_icon_id`),
  ADD KEY `IDX_2D737AEF16D3E4EB` (`mobile_background_id`);

--
-- Indexes for table `section_settings`
--
ALTER TABLE `section_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7528FA99D823E37A` (`section_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storage`
--
ALTER TABLE `storage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_389B783659429DB` (`icon`),
  ADD KEY `IDX_389B783A012A554` (`icon_svg`);

--
-- Indexes for table `tax_type`
--
ALTER TABLE `tax_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_types`
--
ALTER TABLE `tax_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8D93D6495FB14BA7` (`level_id`);

--
-- Indexes for table `user_refresh_token`
--
ALTER TABLE `user_refresh_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_29C18CC5A76ED395` (`user_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7CC7DA2CE6DA28AA` (`background_image_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hotel_services`
--
ALTER TABLE `hotel_services`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_service_quota_values`
--
ALTER TABLE `hotel_service_quota_values`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `periods`
--
ALTER TABLE `periods`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pps_prices`
--
ALTER TABLE `pps_prices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate_plans`
--
ALTER TABLE `rate_plans`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate_plans_rooms`
--
ALTER TABLE `rate_plans_rooms`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate_plan_groups`
--
ALTER TABLE `rate_plan_groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `storage`
--
ALTER TABLE `storage`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_types`
--
ALTER TABLE `tax_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `action`
--
ALTER TABLE `action`
  ADD CONSTRAINT `FK_47CC8C92FE54D947` FOREIGN KEY (`group_id`) REFERENCES `action_group` (`id`);

--
-- Constraints for table `actions_rate_plans`
--
ALTER TABLE `actions_rate_plans`
  ADD CONSTRAINT `actions_rate_plans_ibfk_2` FOREIGN KEY (`rate_plan_id`) REFERENCES `rate_plans` (`id`);

--
-- Constraints for table `action_level`
--
ALTER TABLE `action_level`
  ADD CONSTRAINT `FK_7D2400AF5FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`),
  ADD CONSTRAINT `FK_7D2400AF9D32F035` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`);

--
-- Constraints for table `action_product`
--
ALTER TABLE `action_product`
  ADD CONSTRAINT `FK_9E88C4C4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_9E88C4C9D32F035` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`);

--
-- Constraints for table `bed_type`
--
ALTER TABLE `bed_type`
  ADD CONSTRAINT `FK_AADE868E87A63139` FOREIGN KEY (`mobile_icon`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_AADE868EA196242E` FOREIGN KEY (`web_icon`) REFERENCES `file` (`id`);

--
-- Constraints for table `bundle`
--
ALTER TABLE `bundle`
  ADD CONSTRAINT `bundle_ibfk_1` FOREIGN KEY (`food_type_id`) REFERENCES `food_type` (`id`),
  ADD CONSTRAINT `bundle_ibfk_2` FOREIGN KEY (`web_background`) REFERENCES `file` (`id`);

--
-- Constraints for table `bundle_bundle_item`
--
ALTER TABLE `bundle_bundle_item`
  ADD CONSTRAINT `bundle_bundle_item_ibfk_1` FOREIGN KEY (`bundle_id`) REFERENCES `bundle` (`id`),
  ADD CONSTRAINT `bundle_bundle_item_ibfk_2` FOREIGN KEY (`bundle_item_id`) REFERENCES `bundle_item` (`id`);

--
-- Constraints for table `bundle_item`
--
ALTER TABLE `bundle_item`
  ADD CONSTRAINT `bundle_item_ibfk_1` FOREIGN KEY (`photo_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `bundle_item_ibfk_2` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `bundle_rate_plan`
--
ALTER TABLE `bundle_rate_plan`
  ADD CONSTRAINT `bundle_rate_plan_ibfk_1` FOREIGN KEY (`bundle_id`) REFERENCES `bundle` (`id`),
  ADD CONSTRAINT `bundle_rate_plan_ibfk_2` FOREIGN KEY (`rate_plan_id`) REFERENCES `rate_plan` (`id`);

--
-- Constraints for table `bundle_term`
--
ALTER TABLE `bundle_term`
  ADD CONSTRAINT `FK_9978FC4DF1FAD9D3` FOREIGN KEY (`bundle_id`) REFERENCES `bundle` (`id`);

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `FK_BA388B7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD CONSTRAINT `FK_F0FE25271AD5CDBF` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  ADD CONSTRAINT `FK_F0FE25274584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `cart_promocode`
--
ALTER TABLE `cart_promocode`
  ADD CONSTRAINT `FK_A68649991AD5CDBF` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  ADD CONSTRAINT `FK_A6864999C76C06D9` FOREIGN KEY (`promocode_id`) REFERENCES `promocode` (`id`);

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `FK_64C19C116D3E4EB` FOREIGN KEY (`mobile_background_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_64C19C1B4131AC7` FOREIGN KEY (`mobile_icon_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_64C19C1BC1679AC` FOREIGN KEY (`web_icon_id`) REFERENCES `file` (`id`);

--
-- Constraints for table `category_place`
--
ALTER TABLE `category_place`
  ADD CONSTRAINT `FK_9C2E1C8412469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_9C2E1C84DA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `category_section`
--
ALTER TABLE `category_section`
  ADD CONSTRAINT `FK_EAAB3A9112469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_EAAB3A91D823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`);

--
-- Constraints for table `facility`
--
ALTER TABLE `facility`
  ADD CONSTRAINT `FK_105994B287A63139` FOREIGN KEY (`mobile_icon`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_105994B2A196242E` FOREIGN KEY (`web_icon`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_105994B2A85760C8` FOREIGN KEY (`facility_group_id`) REFERENCES `facility_group` (`id`);

--
-- Constraints for table `facility_group`
--
ALTER TABLE `facility_group`
  ADD CONSTRAINT `FK_3E5F78AF87A63139` FOREIGN KEY (`mobile_icon`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_3E5F78AFA196242E` FOREIGN KEY (`web_icon`) REFERENCES `file` (`id`);

--
-- Constraints for table `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `FK_3535ED964D218E` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`),
  ADD CONSTRAINT `FK_3535ED9694027A` FOREIGN KEY (`hotel_brand_id`) REFERENCES `brand` (`id`),
  ADD CONSTRAINT `FK_3535ED98AD350AB` FOREIGN KEY (`food_type_id`) REFERENCES `food_type` (`id`),
  ADD CONSTRAINT `FK_3535ED9C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `hotel_type` (`id`),
  ADD CONSTRAINT `FK_3535ED9E48E9A13` FOREIGN KEY (`logo`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_3535ED9FE616A99` FOREIGN KEY (`logo_svg`) REFERENCES `file` (`id`);

--
-- Constraints for table `hotel_age_range`
--
ALTER TABLE `hotel_age_range`
  ADD CONSTRAINT `FK_B8852D1D3243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`);

--
-- Constraints for table `hotel_bed_type`
--
ALTER TABLE `hotel_bed_type`
  ADD CONSTRAINT `FK_E8BF54FC3243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `FK_E8BF54FC8158330E` FOREIGN KEY (`bed_type_id`) REFERENCES `bed_type` (`id`);

--
-- Constraints for table `hotel_extra_place_type`
--
ALTER TABLE `hotel_extra_place_type`
  ADD CONSTRAINT `FK_81FFED1F3243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`);

--
-- Constraints for table `hotel_extra_place_type_age_range`
--
ALTER TABLE `hotel_extra_place_type_age_range`
  ADD CONSTRAINT `FK_5EA6A6BBB7E943D0` FOREIGN KEY (`hotel_extra_place_type_id`) REFERENCES `hotel_extra_place_type` (`id`),
  ADD CONSTRAINT `FK_5EA6A6BBCF47D8EB` FOREIGN KEY (`hotel_age_range_id`) REFERENCES `hotel_age_range` (`id`);

--
-- Constraints for table `hotel_facility`
--
ALTER TABLE `hotel_facility`
  ADD CONSTRAINT `FK_523846C03243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `FK_523846C0A7014910` FOREIGN KEY (`facility_id`) REFERENCES `facility` (`id`);

--
-- Constraints for table `hotel_photo`
--
ALTER TABLE `hotel_photo`
  ADD CONSTRAINT `FK_F7634ADC3243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `FK_F7634ADC7E9E4C8C` FOREIGN KEY (`photo_id`) REFERENCES `file` (`id`);

--
-- Constraints for table `hotel_place`
--
ALTER TABLE `hotel_place`
  ADD CONSTRAINT `FK_97C99D093243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `FK_97C99D09DA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `hotel_services`
--
ALTER TABLE `hotel_services`
  ADD CONSTRAINT `hotel_services_ibfk_2` FOREIGN KEY (`photo`) REFERENCES `storage` (`id`);

--
-- Constraints for table `hotel_service_quota_values`
--
ALTER TABLE `hotel_service_quota_values`
  ADD CONSTRAINT `hotel_service_quota_values_ibfk_1` FOREIGN KEY (`hotelServiceId`) REFERENCES `hotel_services` (`id`);

--
-- Constraints for table `hotel_tag`
--
ALTER TABLE `hotel_tag`
  ADD CONSTRAINT `FK_7B0304FD3243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `FK_7B0304FDBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);

--
-- Constraints for table `hotel_video`
--
ALTER TABLE `hotel_video`
  ADD CONSTRAINT `FK_9F1314E829C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  ADD CONSTRAINT `FK_9F1314E83243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`);

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_F5299398A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `FK_52EA1F094584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_52EA1F098D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `FK_6D28840D8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`);

--
-- Constraints for table `pay_system`
--
ALTER TABLE `pay_system`
  ADD CONSTRAINT `FK_40D6DBC43DA5256D` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`);

--
-- Constraints for table `place`
--
ALTER TABLE `place`
  ADD CONSTRAINT `FK_741D53CDC93D69EA` FOREIGN KEY (`background_id`) REFERENCES `file` (`id`);

--
-- Constraints for table `place_block`
--
ALTER TABLE `place_block`
  ADD CONSTRAINT `FK_2A6D8C0129C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  ADD CONSTRAINT `FK_2A6D8C017E9E4C8C` FOREIGN KEY (`photo_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_2A6D8C01DA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `place_geo_point`
--
ALTER TABLE `place_geo_point`
  ADD CONSTRAINT `FK_C33A5D76DA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `place_photo`
--
ALTER TABLE `place_photo`
  ADD CONSTRAINT `FK_BDC19F3B93CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_BDC19F3BDA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `place_section`
--
ALTER TABLE `place_section`
  ADD CONSTRAINT `FK_19BF6D14D823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`),
  ADD CONSTRAINT `FK_19BF6D14DA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `place_video`
--
ALTER TABLE `place_video`
  ADD CONSTRAINT `FK_D5B1C10F29C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  ADD CONSTRAINT `FK_D5B1C10FDA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04ADD823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`);

--
-- Constraints for table `product_price`
--
ALTER TABLE `product_price`
  ADD CONSTRAINT `FK_6B9459854584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `product_settings`
--
ALTER TABLE `product_settings`
  ADD CONSTRAINT `FK_2EF5CA524584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `promocode`
--
ALTER TABLE `promocode`
  ADD CONSTRAINT `FK_7C786E069D32F035` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`);

--
-- Constraints for table `quota`
--
ALTER TABLE `quota`
  ADD CONSTRAINT `FK_6C1C0FEDFE54D947` FOREIGN KEY (`group_id`) REFERENCES `quota_group` (`id`);

--
-- Constraints for table `quota_product`
--
ALTER TABLE `quota_product`
  ADD CONSTRAINT `FK_6149FD254584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_6149FD2554E2C62F` FOREIGN KEY (`quota_id`) REFERENCES `quota` (`id`);

--
-- Constraints for table `quota_value`
--
ALTER TABLE `quota_value`
  ADD CONSTRAINT `FK_E4C7F14654E2C62F` FOREIGN KEY (`quota_id`) REFERENCES `quota` (`id`);

--
-- Constraints for table `rate_plan`
--
ALTER TABLE `rate_plan`
  ADD CONSTRAINT `FK_225EE9723243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `FK_225EE97284042C99` FOREIGN KEY (`tax_type_id`) REFERENCES `tax_type` (`id`),
  ADD CONSTRAINT `FK_225EE9728AD350AB` FOREIGN KEY (`food_type_id`) REFERENCES `food_type` (`id`),
  ADD CONSTRAINT `FK_225EE972FE54D947` FOREIGN KEY (`group_id`) REFERENCES `rate_plan_group` (`id`);

--
-- Constraints for table `rate_plans`
--
ALTER TABLE `rate_plans`
  ADD CONSTRAINT `rate_plans_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `rate_plan_groups` (`id`),
  ADD CONSTRAINT `rate_plans_ibfk_4` FOREIGN KEY (`taxTypeId`) REFERENCES `tax_types` (`id`);

--
-- Constraints for table `rate_plans_rooms`
--
ALTER TABLE `rate_plans_rooms`
  ADD CONSTRAINT `rate_plans_rooms_ibfk_1` FOREIGN KEY (`ratePlanId`) REFERENCES `rate_plans` (`id`);

--
-- Constraints for table `rate_plan_price`
--
ALTER TABLE `rate_plan_price`
  ADD CONSTRAINT `FK_33C52D3E325B4D7A` FOREIGN KEY (`rate_plan_id`) REFERENCES `rate_plan` (`id`),
  ADD CONSTRAINT `FK_33C52D3E54177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FK_33C52D3E68A2C92D` FOREIGN KEY (`hotel_extra_place_type_age_range_id`) REFERENCES `hotel_extra_place_type_age_range` (`id`);

--
-- Constraints for table `rate_plan_room`
--
ALTER TABLE `rate_plan_room`
  ADD CONSTRAINT `FK_C64FD907325B4D7A` FOREIGN KEY (`rate_plan_id`) REFERENCES `rate_plan` (`id`),
  ADD CONSTRAINT `FK_C64FD90754177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`);

--
-- Constraints for table `receipt`
--
ALTER TABLE `receipt`
  ADD CONSTRAINT `FK_5399B6454C3A3BB` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`);

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `FK_729F519B3243BB18` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`);

--
-- Constraints for table `room_facility`
--
ALTER TABLE `room_facility`
  ADD CONSTRAINT `FK_B0BF547654177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FK_B0BF5476A7014910` FOREIGN KEY (`facility_id`) REFERENCES `facility` (`id`);

--
-- Constraints for table `room_main_place_variant`
--
ALTER TABLE `room_main_place_variant`
  ADD CONSTRAINT `FK_BCAC49D054177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`);

--
-- Constraints for table `room_photo`
--
ALTER TABLE `room_photo`
  ADD CONSTRAINT `FK_5E0B25B354177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FK_5E0B25B37E9E4C8C` FOREIGN KEY (`photo_id`) REFERENCES `file` (`id`);

--
-- Constraints for table `room_video`
--
ALTER TABLE `room_video`
  ADD CONSTRAINT `FK_367B7B8729C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`),
  ADD CONSTRAINT `FK_367B7B8754177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`);

--
-- Constraints for table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `FK_2D737AEF16D3E4EB` FOREIGN KEY (`mobile_background_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_2D737AEF93BFB765` FOREIGN KEY (`web_background_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_2D737AEFB4131AC7` FOREIGN KEY (`mobile_icon_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_2D737AEFBC1679AC` FOREIGN KEY (`web_icon_id`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_2D737AEFEE45BDBF` FOREIGN KEY (`picture_id`) REFERENCES `file` (`id`);

--
-- Constraints for table `section_settings`
--
ALTER TABLE `section_settings`
  ADD CONSTRAINT `FK_7528FA99D823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`);

--
-- Constraints for table `tag`
--
ALTER TABLE `tag`
  ADD CONSTRAINT `FK_389B783659429DB` FOREIGN KEY (`icon`) REFERENCES `file` (`id`),
  ADD CONSTRAINT `FK_389B783A012A554` FOREIGN KEY (`icon_svg`) REFERENCES `file` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D6495FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`);

--
-- Constraints for table `user_refresh_token`
--
ALTER TABLE `user_refresh_token`
  ADD CONSTRAINT `FK_29C18CC5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `FK_7CC7DA2CE6DA28AA` FOREIGN KEY (`background_image_id`) REFERENCES `file` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
