<?php
declare(strict_types=1);

use App\Service\MailerService;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;

return [

    MailerService::class => function (ContainerInterface $container) {
        $mailer = $container->get('settings')['mailer'];

        return new MailerService(new Swift_Mailer((new Swift_SmtpTransport($mailer['host'], $mailer['port']))
            ->setUsername($mailer['user'])
            ->setPassword($mailer['password'])
            ->setAuthMode($mailer['auth_mode'])
            ->setEncryption($mailer['encryption'])
            ->setStreamOptions([
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                ],
            ])), $mailer['mailing_addresses'], $mailer['name']);
    },

    'render' => function (ContainerInterface $container) {
        return $container->get(Twig::class);
    },
];
