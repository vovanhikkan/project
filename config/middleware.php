<?php

declare(strict_types=1);

use App\UI\Http\Middleware\AuthMiddleware;
use App\UI\Http\Middleware\JsonBodyParserMiddleware;
use App\UI\Http\Middleware\JwtAuthenticationMiddleware;
use Slim\App;

return static function (App $app): void {
    $app->add(JsonBodyParserMiddleware::class);
    $app->add(AuthMiddleware::class);
    $app->add(JwtAuthenticationMiddleware::class);
};