<?php

declare(strict_types=1);

use App\UI\Http\Action\v2 as ActionV2;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return static function (App $app): void {
    $app->options('/v2/{routes:.+}', function ($request, $response, $args) {
        return $response;
    });

    $app->get('/v2', ActionV2\IndexAction::class);

    $app->get('/api/v2/doc', ActionV2\SwaggerAction::class);
    $app->redirect('/api/v1/doc', '/api/v2/doc');

    $app->group('/v2/auth', function (Group $group) {
        $group->post('/reset-password/confirm', ActionV2\Auth\ResetPassword\ConfirmAction::class);
        $group->post('/reset-password/request', ActionV2\Auth\ResetPassword\RequestAction::class);
        $group->post('/login', ActionV2\Auth\LoginAction::class);
        $group->post('/refresh', ActionV2\Auth\RefreshAction::class);
        $group->post('/register', ActionV2\Auth\RegisterAction::class);
    });

    $app->group('/v2/bed-type', function (Group $group) {
        $group->get('', ActionV2\BedType\IndexAction::class);
        $group->post('', ActionV2\BedType\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\BedType\UpdateAction::class);
            $group2->get('', ActionV2\BedType\ViewAction::class);
            $group2->delete('', ActionV2\BedType\DeleteAction::class);
        });
    });

    $app->group('/v2/brand', function (Group $group) {
        $group->get('', ActionV2\Brand\IndexAction::class);
        $group->post('', ActionV2\Brand\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Brand\UpdateAction::class);
            $group2->get('', ActionV2\Brand\ViewAction::class);
            $group2->delete('', ActionV2\Brand\DeleteAction::class);
        });
    });

    $app->group('/v2/bundle', function (Group $group) {
        $group->get('', ActionV2\Bundle\IndexAction::class);
        $group->get('/search/product', ActionV2\Bundle\SearchAction::class);
        $group->get('/recommendation', ActionV2\Bundle\RecommendationAction::class);
        $group->post('', ActionV2\Bundle\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Bundle\UpdateAction::class);
            $group2->get('', ActionV2\Bundle\ViewAction::class);
            $group2->delete('', ActionV2\Bundle\DeleteAction::class);
        });
    });


    $app->group('/v2/bundle-term', function (Group $group) {
        $group->get('', ActionV2\BundleTerm\IndexAction::class);
        $group->post('', ActionV2\BundleTerm\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\BundleTerm\UpdateAction::class);
            $group2->get('', ActionV2\BundleTerm\ViewAction::class);
            $group2->delete('', ActionV2\BundleTerm\DeleteAction::class);
        });
    });

    $app->group('/v2/bundle-item', function (Group $group) {
        $group->get('', ActionV2\BundleItem\IndexAction::class);
        $group->post('', ActionV2\BundleItem\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\BundleItem\UpdateAction::class);
            $group2->get('', ActionV2\BundleItem\ViewAction::class);
            $group2->delete('', ActionV2\BundleItem\DeleteAction::class);
        });
    });


    $app->group('/v2/cart', function (Group $group) {
        $group->get('', ActionV2\Cart\IndexAction::class);
        $group->get('/active', ActionV2\Cart\ActiveAction::class);
        $group->post('/cart-item/product', ActionV2\Cart\AddItemProductAction::class);
        $group->post('/cart-item/room', ActionV2\Cart\AddItemRoomAction::class);
        $group->post('/cart-item/guest', ActionV2\Cart\AddItemRoomGuestAction::class);
        $group->post('/cart-item/bundle', ActionV2\Cart\AddItemBundleAction::class);
        $group->post('/card-num-check', ActionV2\Cart\CardNumCheckAction::class);
        $group->delete('/cart-item/product', ActionV2\Cart\RemoveItemProductAction::class);
        $group->delete('/cart-item/room', ActionV2\Cart\RemoveItemRoomAction::class);
        $group->delete('/cart-item/guest', ActionV2\Cart\RemoveItemRoomGuestAction::class);
        $group->delete('/cart-item/bundle', ActionV2\Cart\RemoveItemBundleAction::class);
        $group->post('/validate/product', ActionV2\Cart\ValidateProductAction::class);
        $group->post('/validate/room', ActionV2\Cart\ValidateRoomAction::class);
        $group->post('/validate/bundle', ActionV2\Cart\ValidateBundleAction::class);
        $group->group('/cart-item/{id}', function (Group $group2) {
            $group2->patch('/product', ActionV2\Cart\UpdateItemProductAction::class);
            $group2->patch('/room', ActionV2\Cart\UpdateItemRoomAction::class);
            $group2->patch('/bundle', ActionV2\Cart\UpdateItemBundleAction::class);
            $group2->patch('/change-quantity/product', ActionV2\Cart\ChangeItemQuantityProductAction::class);
            $group2->patch('/change-quantity/room', ActionV2\Cart\ChangeItemQuantityRoomAction::class);
            $group2->patch('/change-quantity/bundle', ActionV2\Cart\ChangeItemQuantityBundleAction::class);
        });
    });

    $app->group('/v2/category', function (Group $group) {
        $group->get('', ActionV2\Category\IndexAction::class);
        $group->post('', ActionV2\Category\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Category\UpdateAction::class);
            $group2->get('', ActionV2\Category\ViewAction::class);
        });
    });

    $app->group('/v2/food-type', function (Group $group) {
        $group->get('', ActionV2\FoodType\IndexAction::class);
        $group->post('', ActionV2\FoodType\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\FoodType\UpdateAction::class);
            $group2->get('', ActionV2\FoodType\ViewAction::class);
            $group2->delete('', ActionV2\FoodType\DeleteAction::class);
        });
    });

    $app->group('/v2/facility', function (Group $group) {
        $group->get('', ActionV2\Facility\IndexAction::class);
        $group->post('', ActionV2\Facility\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Facility\UpdateAction::class);
            $group2->get('', ActionV2\Facility\ViewAction::class);
            $group2->delete('', ActionV2\Facility\DeleteAction::class);
        });
    });

    $app->group('/v2/facility-group', function (Group $group) {
        $group->get('', ActionV2\FacilityGroup\IndexAction::class);
        $group->post('', ActionV2\FacilityGroup\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\FacilityGroup\UpdateAction::class);
            $group2->get('', ActionV2\FacilityGroup\ViewAction::class);
            $group2->delete('', ActionV2\FacilityGroup\DeleteAction::class);
        });
    });

    $app->group('/v2/hotel', function (Group $group) {
        $group->get('', ActionV2\Hotel\IndexAction::class);
        $group->get('/search', ActionV2\Hotel\SearchAction::class);
        $group->get('/search/count', ActionV2\Hotel\SearchCountAction::class);
        $group->get('/search-bundle', ActionV2\Hotel\SearchFilterBundleAction::class);
        $group->get('/type/all', ActionV2\Hotel\TypeAllAction::class);
        $group->group('/{id}', function (Group $group2) {
//            $group2->get('', ActionV2\Order\ViewAction::class);
            $group2->patch('', ActionV2\Hotel\UpdateAction::class);
            $group2->get('', ActionV2\Hotel\ViewAction::class);
            $group2->delete('', ActionV2\Hotel\DeleteAction::class);
            $group2->get('/search-accommodation', ActionV2\Hotel\SearchAccommodationAction::class);
        });
        $group->post('', ActionV2\Hotel\CreateAction::class);
//        $group->post('/{id:[0-9]+}/search-variations', HotelController::class . ':searchVariations');
    });

    $app->group('/v2/file', function (Group $group) {
        $group->get('/search', ActionV2\File\SearchAction::class);
        $group->post('/upload', ActionV2\File\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->delete('', ActionV2\File\DeleteAction::class);
        });
    });

    $app->group('/v2/order', function (Group $group) {
        $group->get('', ActionV2\Order\IndexAction::class);
        $group->post('/from-cart', ActionV2\Order\CreateFromCartAction::class);
        $group->post('/quick', ActionV2\Order\CreateQuickAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->get('', ActionV2\Order\ViewAction::class);
            $group2->get('/voucher', ActionV2\Order\VoucherAction::class);
        });
    });

    $app->group('/v2/payment', function (Group $group) {
        $group->get('/callback', ActionV2\Payment\CallbackAction::class);
        $group->post('', ActionV2\Payment\CreateAction::class);
        $group->get('', ActionV2\Payment\IndexAction::class);
    });

    $app->group('/v2/pay-system', function (Group $group) {
        $group->get('', ActionV2\PaySystem\IndexAction::class);
    });

    $app->group('/v2/place', function (Group $group) {
        $group->get('', ActionV2\Place\IndexAction::class);
        $group->post('', ActionV2\Place\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Place\UpdateAction::class);
            $group2->get('', ActionV2\Place\ViewAction::class);
        });
    });

    $app->group('/v2/label', function (Group $group) {
        $group->get('', ActionV2\Label\IndexAction::class);
        $group->post('', ActionV2\Label\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Label\UpdateAction::class);
            $group2->get('', ActionV2\Label\ViewAction::class);
        });
    });

    $app->group('/v2/label-type', function (Group $group) {
        $group->get('', ActionV2\LabelType\IndexAction::class);
        $group->post('', ActionV2\LabelType\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\LabelType\UpdateAction::class);
            $group2->get('', ActionV2\LabelType\ViewAction::class);
        });
    });

    $app->group('/v2/product', function (Group $group) {
        $group->get('', ActionV2\Product\IndexAction::class);
        $group->post('', ActionV2\Product\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->get('/price', ActionV2\Product\GetPriceAction::class);
            $group2->get('/quota', ActionV2\Product\GetQuotaAction::class);
            $group2->patch('', ActionV2\Product\UpdateAction::class);
            $group2->get('', ActionV2\Product\ViewAction::class);
            $group2->delete('', ActionV2\Product\DeleteAction::class);
        });
    });

    $app->group('/v2/receipt', function (Group $group) {
        $group->get('', ActionV2\Receipt\IndexAction::class);
    });

    $app->group('/v2/action-group', function (Group $group) {
        $group->get('', ActionV2\ActionGroup\IndexAction::class);
        $group->post('', ActionV2\ActionGroup\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\ActionGroup\UpdateAction::class);
        });
    });

    $app->group('/v2/quota-group', function (Group $group) {
        $group->get('', ActionV2\QuotaGroup\IndexAction::class);
        $group->post('', ActionV2\QuotaGroup\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\QuotaGroup\UpdateAction::class);
        });
    });

    $app->group('/v2/quota', function (Group $group) {
        $group->get('', ActionV2\Quota\IndexAction::class);
        $group->post('', ActionV2\Quota\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Quota\UpdateAction::class);
        });
    });
    $app->group('/v2/quota-value', function (Group $group) {
        $group->get('', ActionV2\QuotaValue\IndexAction::class);
        $group->post('', ActionV2\QuotaValue\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\QuotaValue\UpdateAction::class);
        });
    });

    $app->group('/v2/action', function (Group $group) {
        $group->get('', ActionV2\Action\IndexAction::class);
        $group->post('', ActionV2\Action\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Action\UpdateAction::class);
        });
    });

    $app->group('/v2/language', function (Group $group) {
        $group->get('', ActionV2\Language\IndexAction::class);
        $group->post('', ActionV2\Language\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Language\UpdateAction::class);
            $group2->delete('', ActionV2\Language\DeleteAction::class);
        });
    });

    $app->group('/v2/location', function (Group $group) {
        $group->get('', ActionV2\Location\IndexAction::class);
        $group->post('', ActionV2\Location\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->get('', ActionV2\Location\ViewAction::class);
            $group2->patch('', ActionV2\Location\UpdateAction::class);
            $group2->delete('', ActionV2\Location\DeleteAction::class);
        });
    });

    $app->group('/v2/promocode', function (Group $group) {
        $group->get('', ActionV2\Promocode\IndexAction::class);
        $group->post('', ActionV2\Promocode\CreateAction::class);
        $group->post('/generate', ActionV2\Promocode\GenerateAction::class);
        $group->post('/check', ActionV2\Promocode\CheckAction::class);
        $group->post('/activate', ActionV2\Promocode\ActivateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Promocode\UpdateAction::class);
        });
    });

    $app->group('/v2/section', function (Group $group) {
        $group->get('', ActionV2\Section\IndexAction::class);
        $group->post('', ActionV2\Section\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Section\UpdateAction::class);
            $group2->get('', ActionV2\Section\ViewAction::class);
            $group2->delete('', ActionV2\Section\DeleteAction::class);
        });
    });

    $app->group('/v2/section-settings', function (Group $group) {
        $group->get('', ActionV2\SectionSettings\IndexAction::class);
        $group->post('', ActionV2\SectionSettings\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\SectionSettings\UpdateAction::class);
            $group2->get('', ActionV2\SectionSettings\ViewAction::class);
            $group2->delete('', ActionV2\SectionSettings\DeleteAction::class);
        });
    });

    $app->group('/v2/travelline', function (Group $group) {
        $group->post('', ActionV2\TravelLine\IndexAction::class);
    });

    $app->group('/v2/user', function (Group $group) {
        $group->get('', ActionV2\User\IndexAction::class);
        $group->post('', ActionV2\User\CreateAction::class);
        $group->get('/me', ActionV2\User\MeAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->post('/verify-email/confirm', ActionV2\User\VerifyEmail\ConfirmAction::class);
            $group2->post('/verify-email/request', ActionV2\User\VerifyEmail\RequestAction::class);
            $group2->post('/verify-phone/confirm', ActionV2\User\VerifyPhone\ConfirmAction::class);
            $group2->post('/verify-phone/request', ActionV2\User\VerifyPhone\RequestAction::class);
            $group2->post('/block', ActionV2\User\BlockAction::class);
            $group2->post('/change-password', ActionV2\User\ChangePasswordAction::class);
            $group2->post('/change-profile', ActionV2\User\ChangeProfileAction::class);
        });
    });

    $app->group('/v2/video', function (Group $group) {
        $group->post('/upload', ActionV2\Video\CreateAction::class);
    });
    

    $app->group('/v2/budget', function (Group $group) {
        $group->get('', ActionV2\Budget\IndexAction::class);
        $group->post('', ActionV2\Budget\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Budget\UpdateAction::class);
            $group2->get('', ActionV2\Budget\ViewAction::class);
            $group2->delete('', ActionV2\Budget\DeleteAction::class);
        });
    });

    $app->group('/v2/review', function (Group $group) {
        $group->get('', ActionV2\Review\IndexAction::class);
        $group->post('', ActionV2\Review\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Review\UpdateAction::class);
            $group2->get('', ActionV2\Review\ViewAction::class);
            $group2->delete('', ActionV2\Review\DeleteAction::class);
        });
    });

    $app->group('/v2/hotel-type', function (Group $group) {
        $group->get('', ActionV2\HotelType\IndexAction::class);
        $group->post('', ActionV2\HotelType\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\HotelType\UpdateAction::class);
            $group2->get('', ActionV2\HotelType\ViewAction::class);
            $group2->delete('', ActionV2\HotelType\DeleteAction::class);
        });
    });

    $app->group('/v2/tax-type', function (Group $group) {
        $group->get('', ActionV2\TaxType\IndexAction::class);
        $group->post('', ActionV2\TaxType\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\TaxType\UpdateAction::class);
            $group2->get('', ActionV2\TaxType\ViewAction::class);
            $group2->delete('', ActionV2\TaxType\DeleteAction::class);
        });
    });

    $app->group('/v2/rate-plan-group', function (Group $group) {
        $group->get('', ActionV2\RatePlanGroup\IndexAction::class);
        $group->post('', ActionV2\RatePlanGroup\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\RatePlanGroup\UpdateAction::class);
            $group2->get('', ActionV2\RatePlanGroup\ViewAction::class);
            $group2->delete('', ActionV2\RatePlanGroup\DeleteAction::class);
        });
    });

    $app->group('/v2/hotel-extra-place-type', function (Group $group) {
        $group->get('', ActionV2\HotelExtraPlaceType\IndexAction::class);
        $group->post('', ActionV2\HotelExtraPlaceType\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\HotelExtraPlaceType\UpdateAction::class);
            $group2->get('', ActionV2\HotelExtraPlaceType\ViewAction::class);
            $group2->delete('', ActionV2\HotelExtraPlaceType\DeleteAction::class);
        });
    });

    $app->group('/v2/rate-plan-price', function (Group $group) {
        $group->get('', ActionV2\RatePlanPrice\IndexAction::class);
        $group->post('', ActionV2\RatePlanPrice\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\RatePlanPrice\UpdateAction::class);
            $group2->get('', ActionV2\RatePlanPrice\ViewAction::class);
            $group2->delete('', ActionV2\RatePlanPrice\DeleteAction::class);
        });
    });

    $app->group('/v2/rate-plan', function (Group $group) {
        $group->get('', ActionV2\RatePlan\IndexAction::class);
        $group->post('', ActionV2\RatePlan\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\RatePlan\UpdateAction::class);
            $group2->get('', ActionV2\RatePlan\ViewAction::class);
            $group2->delete('', ActionV2\RatePlan\DeleteAction::class);
        });
    });
    

    $app->group('/v2/faq', function (Group $group) {
        $group->get('', ActionV2\FAQ\IndexAction::class);
        $group->post('', ActionV2\FAQ\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\FAQ\UpdateAction::class);
            $group2->get('', ActionV2\FAQ\ViewAction::class);
            $group2->delete('', ActionV2\FAQ\DeleteAction::class);
        });
    });

    $app->group('/v2/hotel-age-range', function (Group $group) {
        $group->get('', ActionV2\HotelAgeRange\IndexAction::class);
        $group->post('', ActionV2\HotelAgeRange\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\HotelAgeRange\UpdateAction::class);
            $group2->get('', ActionV2\HotelAgeRange\ViewAction::class);
            $group2->delete('', ActionV2\HotelAgeRange\DeleteAction::class);
        });
    });


    $app->group('/v2/guest', function (Group $group) {
        $group->get('', ActionV2\Guest\IndexAction::class);
        $group->post('', ActionV2\Guest\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Guest\UpdateAction::class);
            $group2->get('', ActionV2\Guest\ViewAction::class);
            $group2->delete('', ActionV2\Guest\DeleteAction::class);
        });
    });

    $app->group('/v2/review-star-type', function (Group $group) {
        $group->get('', ActionV2\ReviewStarType\IndexAction::class);
        $group->post('', ActionV2\ReviewStarType\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\ReviewStarType\UpdateAction::class);
            $group2->get('', ActionV2\ReviewStarType\ViewAction::class);
            $group2->delete('', ActionV2\ReviewStarType\DeleteAction::class);
        });
    });

    $app->group('/v2/tag', function (Group $group) {
        $group->get('', ActionV2\Tag\IndexAction::class);
        $group->post('', ActionV2\Tag\CreateAction::class);
        $group->group('/{id}', function (Group $group2) {
            $group2->patch('', ActionV2\Tag\UpdateAction::class);
            $group2->get('', ActionV2\Tag\ViewAction::class);
            $group2->delete('', ActionV2\Tag\DeleteAction::class);
        });
    });
};
