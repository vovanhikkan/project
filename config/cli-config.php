<?php

use Doctrine\ORM\EntityManagerInterface;

/** @var ContainerInterface $container */
$container = require_once __DIR__. '/bootstrap.php';


/** @var EntityManagerInterface $em */
$em = $container->get(EntityManagerInterface::class);

$config = $em->getConfiguration();
$driver = $config->newDefaultAnnotationDriver([
    __DIR__ . '/../src/Hotel/Model',
    __DIR__ . '/../src/Place/Model',
    __DIR__ . '/../src/Packet/Model',
], false);
$config->setMetadataDriverImpl($driver);

$helperSet = new \Symfony\Component\Console\Helper\HelperSet([
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
]);

return $helperSet;