<?php

declare(strict_types=1);

use App\Data\Doctrine\Type;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\DBAL;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;

return [
    EntityManagerInterface::class => function () {
        $srcDir = __DIR__ . '/../src';
        $cacheDir = __DIR__ . '/../var/cache/doctrine';

        $config = Setup::createAnnotationMetadataConfiguration(
            [
//                $srcDir . '/Entity', // @todo for remove
                $srcDir . '/Auth/Model',
                $srcDir . '/Content/Model',
                $srcDir . '/Order/Model',
                $srcDir . '/Place/Model',
                $srcDir . '/Product/Model',
                $srcDir . '/Storage/Model',
            ],
            getenv('APP_ENV') === 'test' || getenv('APP_ENV') === 'dev',
            null,
            getenv('APP_ENV') === 'dev' ? null : new FilesystemCache($cacheDir),
            false
        );

        $config->setNamingStrategy(new UnderscoreNamingStrategy());

        // @todo for remove
        $config->addCustomStringFunction('JSON_CONTAINS', Scienta\DoctrineJsonFunctions\Query\AST\Functions\Mysql\JsonContains::class);
        $config->addCustomStringFunction('CAST', Oro\ORM\Query\AST\Functions\Cast::class);

        $types = [
            Type\AmountType::class,
            Type\BarcodeType::class,
            Type\CardNumType::class,
            Type\ColorType::class,
            Type\EmailType::class,
            Type\FirstNameType::class,
            Type\IdType::class,
            Type\LastNameType::class,
            Type\LatitudeType::class,
            Type\LongitudeType::class,
            Type\MiddleNameType::class,
            Type\PhoneType::class,
            Type\QuantityType::class,
            Type\SortType::class,
            Type\SvgType::class,
            Type\UrlType::class,
            Type\UuidType::class,

            Type\Auth\UserFirstNameType::class,
            Type\Auth\UserLastNameType::class,
            Type\Auth\UserMiddleNameType::class,
            Type\Auth\UserRoleType::class,
            Type\Auth\UserSexType::class,
            Type\Auth\UserStatusType::class,

            Type\Bundle\BundleDescriptionType::class,
            Type\Bundle\BundleDayCountType::class,
            Type\Bundle\BundleCodeType::class,
            Type\Bundle\BundleNameType::class,
            Type\Bundle\BundleItemNameType::class,
            Type\Bundle\BundleItemDescriptionType::class,
            Type\Bundle\BundleItemSubNameType::class,
            Type\Bundle\BundleTermDescriptionType::class,
            Type\Bundle\BundleWidthType::class,

            Type\Bundle\BundleProductActivationDateShiftType::class,
            Type\Bundle\BundleProductChildAgeFromType::class,
            Type\Bundle\BundleProductChildAgeToType::class,

            Type\Hotel\BedTypeNameType::class,

            Type\Content\CategoryNameType::class,
            Type\Content\TagNameType::class,
            Type\Content\CategoryDescriptionType::class,
            Type\Content\TagColorTextType::class,
            Type\Content\TagColorBorderType::class,

            Type\Hotel\HotelNameType::class,
            Type\Hotel\HotelDescriptionType::class,
            Type\Hotel\HotelStarCountType::class,
            Type\Hotel\HotelAddressType::class,
            Type\Hotel\HotelLinksType::class,
            Type\Hotel\HotelLatType::class,
            Type\Hotel\HotelLonType::class,
            Type\Hotel\FacilityNameType::class,
            Type\Hotel\FacilityGroupNameType::class,
            Type\Hotel\FoodTypeNameType::class,
            Type\Hotel\HotelTypeNameType::class,
            Type\Hotel\HotelVideoYoutubeCodeType::class,
            Type\Hotel\RoomVideoYoutubeCodeType::class,
            Type\Hotel\RoomNameType::class,
            Type\Hotel\BrandNameType::class,
            Type\Hotel\RoomDescriptionType::class,
            Type\Hotel\RoomExtraPlaceCountType::class,
            Type\Hotel\RoomMainPlaceCountType::class,
            Type\Hotel\RoomZeroPlaceCountType::class,
            Type\Hotel\RoomAreaType::class,
            Type\Hotel\RoomMainPlaceVariantValueType::class,
            Type\Hotel\HotelExtraPlaceNameType::class,
            Type\Hotel\HotelAgeRangeAgeFromType::class,
            Type\Hotel\HotelAgeRangeAgeToType::class,
            Type\Hotel\RatePlanDescriptionType::class,
            Type\Hotel\RatePlanExtraConditionType::class,
            Type\Hotel\RatePlanNameType::class,
            Type\Hotel\HotelCheckInTimeType::class,
            Type\Hotel\HotelCheckOutTimeType::class,
            Type\Hotel\HotelCancelPrepaymentType::class,
            Type\Hotel\HotelExtraBedsType::class,
            Type\Hotel\HotelPetsType::class,
            Type\Hotel\HotelExtraInfoType::class,
            Type\Hotel\HotelPaymentsType::class,
            Type\Hotel\HotelRatingType::class,

            Type\Hotel\HotelGuestAgeType::class,

            Type\Language\LanguageNameType::class,
            Type\Language\LanguageCodeType::class,

            Type\Location\LocationNameType::class,
            Type\Location\LocationAltitudeType::class,

            Type\Loyalty\LevelNameType::class,
            Type\Loyalty\ActionGroupNameType::class,
            Type\Loyalty\ActionNameType::class,
            Type\Loyalty\ActionDescriptionType::class,
            Type\Loyalty\ActionCashbackPointPercentageValueType::class,
            Type\Loyalty\ActionCashbackPointAbsoluteValueType::class,
            Type\Loyalty\ActionDiscountPercentageValueType::class,
            Type\Loyalty\ActionDiscountAbsoluteValueType::class,
            Type\Loyalty\ActionMaxDayCountType::class,
            Type\Loyalty\ActionMinDayCountType::class,
            Type\Loyalty\ActionMaxProductQuantityType::class,
            Type\Loyalty\ActionMinProductQuantityType::class,
            Type\Loyalty\ActionPromocodeLifetimeType::class,
            Type\Loyalty\ActionPromocodePrefixType::class,
            Type\Loyalty\ActionTotalCountType::class,
            Type\Loyalty\ActionHoldedCountType::class,
            Type\Loyalty\ActionOrderedCountType::class,
            Type\Loyalty\PromocodeTotalCountType::class,
            Type\Loyalty\PromocodeValueType::class,
            Type\Loyalty\PromocodeHoldedCountType::class,
            Type\Loyalty\PromocodeOrderedCountType::class,

            Type\Order\CartStatusType::class,
            Type\Order\OrderStatusType::class,
            Type\Order\PaymentStatusType::class,
            Type\Order\PaySystemCodeType::class,
            Type\Order\PaySystemDescriptionType::class,
            Type\Order\PaySystemNameType::class,
            Type\Order\PaySystemProviderType::class,
            Type\Order\ReceiptProviderType::class,
            Type\Order\ReceiptStatusType::class,
            Type\Order\CheckInTimeType::class,
            Type\Order\RequirementsType::class,

            Type\Place\BlockBodyType::class,
            Type\Place\BlockTitleType::class,
            Type\Place\GeoPointDescriptionType::class,
            Type\Place\PlaceAddressType::class,
            Type\Place\PlaceAgeRestrictionType::class,
            Type\Place\PlaceButtonTextType::class,
            Type\Place\PlaceCodeType::class,
            Type\Place\PlaceDescriptionType::class,
            Type\Place\PlaceLabelType::class,
            Type\Place\PlaceSubDescriptionType::class,
            Type\Place\PlaceSubTitleType::class,
            Type\Place\PlaceTitleType::class,
            Type\Place\PlaceWidthType::class,
            Type\Place\PlaceWorkingHoursDescriptionType::class,
            Type\Place\PlaceRatingType::class,
            Type\Place\PlaceRecommendedIdType::class,
            Type\Place\LabelContentType::class,
            Type\Place\LabelTypeDefaultContentType::class,

            Type\Product\CardTypeTypeType::class,
            Type\Product\ProductNameType::class,
            Type\Product\ProductSettingsAgeType::class,
            Type\Product\ProductSettingsAmountOfDaysType::class,
            Type\Product\ProductSettingsCardTypeType::class,
            Type\Product\ProductSettingsRoundsType::class,
            Type\Product\ProductTypeType::class,
            Type\Product\SectionCodeType::class,
            Type\Product\SectionDescriptionType::class,
            Type\Product\SectionInnerDescriptionType::class,
            Type\Product\SectionInnerTitleType::class,
            Type\Product\SectionNameType::class,
            Type\Product\SectionSettingsActiveDaysType::class,
            Type\Product\SectionSettingsControlsType::class,
            Type\Product\SectionTariffDescriptionType::class,
            Type\Product\QuotaValueOrderedQuantityType::class,
            Type\Product\QuotaValueHoldedQuantityType::class,
            Type\Product\QuotaValueTotalQuantityType::class,
            Type\Product\QuotaGroupNameType::class,
            Type\Product\QuotaNameType::class,

            Type\Storage\FileFileNameType::class,
            Type\Storage\FileFilePathType::class,
            Type\Storage\FileMimeTypeType::class,
            Type\Storage\FileNameType::class,
            Type\Storage\FileSizeType::class,
            Type\Storage\FileTypeType::class,
            Type\Storage\VideoYoutubeCodeType::class,

            Type\Hotel\BudgetNameType::class,
            Type\Hotel\BudgetBudgetFromType::class,
            Type\Hotel\BudgetBudgetToType::class,

            Type\Hotel\TaxTypeNameType::class,
            Type\Hotel\TaxTypeValueType::class,

            Type\Hotel\RatePlanGroupNameType::class,

            Type\FAQ\TitleType::class,
            Type\FAQ\BodyType::class,

            Type\Review\BodyType::class,
            Type\Review\RatingType::class,

            Type\Review\ReviewStarTypeNameType::class,

            Type\Review\ValueType::class,
            
            Type\Hotel\RoomQuotaNameType::class,
        ];

        foreach ($types as $class) {
            DBAL\Types\Type::addType($class::NAME, $class);
        }

        return EntityManager::create(['url' => getenv('DB_URL')], $config);
    }
];
