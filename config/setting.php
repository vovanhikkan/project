<?php

declare(strict_types=1);

return [
    'settings' => [
        'displayErrorDetails' => getenv('APP_ENV') === 'test' || getenv('APP_ENV') === 'dev',
        'mailer' => [
            'name'              => 'ROSAHUTOR',
            'host'              => getenv('MAILER_HOST'),
            'port'              => getenv('MAILER_PORT'),
            'user'              => getenv('MAILER_USERNAME'),
            'password'          => getenv('MAILER_PASSWORD'),
            'encryption'        => getenv('MAILER_ENCRYPTION'),
            'auth_mode'         => getenv('MAILER_AUTH_MODE'),
            'mailing_addresses' => getenv('MAILER_SENDER_ADDRESSES'),
        ],
        'jwt' => [
            'alg'    => 'HS256',
            'header' => 'X-Auth-Token',
            'regexp' => '/(.*)/',
            'path'   => '/api/v1',
            'ignore' => [
                '/test',
                '/api/v1/doc',
                '/api/v1/auth',
                '/api/v1/product',
                '/api/v1/section',
                '/api/v1/category',
                '/api/v1/place',
                '/api/v1/packet',
                '/api/v1/location',
                '/api/v1/hotel',
                '/api/v1/user/register',
                '/api/v1/user/forgotPassword',
                '/api/v1/user/resetPassword',
                '/api/v1/order/quick-create',
                '/admin',
            ],
            'secure' => false,
            'secret' => getenv('SECRET_KEY'),
        ],
    ],
];
