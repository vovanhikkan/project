<?php

declare(strict_types=1);

use App\Service\SessionService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

return [
    SessionService::class => function (ContainerInterface $container) {
        /** @var \App\Repository\SessionsRepository $repository */
        $repository = $container->get(EntityManagerInterface::class)->getRepository(Sessions::class);
        $connection = getenv('DB_URL');

        return new SessionService(new NativeSessionStorage([], new PdoSessionHandler($connection,
            [
                'db_table'        => 'sessions',
                'db_id_col'       => 'session',
                'db_data_col'     => 'data',
                'db_time_col'     => 'created',
                'db_lifetime_col' => 'expires',
            ]
        )), $repository);
    },
];
