<?php

declare(strict_types=1);

namespace App\TravelLine\Service;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Model\Room\Room;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Repository\RatePlanPriceRepository;
use App\Hotel\Repository\RatePlanRepository;
use App\Hotel\Service\RatePlanPrice\Updater;
use App\Order\Dto\OrderSearchDto;
use App\Order\Repository\OrderRepository;
use App\Hotel\Repository\RoomRepository;
use App\TravelLine\Dto\TravelLineRequestDto;
use App\UI\Http\Serializer;
use DateTimeImmutable;
use Psr\Log\LoggerInterface;


/**
 * TravelLine.
 */
class TravelLine
{
    private string $login;
    private string $password;
    private LoggerInterface $logger;
    private HotelRepository $hotelRepository;
    private OrderRepository $orderRepository;
    private RoomRepository $roomRepository;
    private RatePlanRepository $ratePlanRepository;
    private RatePlanPriceRepository $ratePlanPriceRepository;
    private Serializer $serializer;
    private Updater $updater;

    public function __construct(
        Updater $updater,
        LoggerInterface $logger,
        HotelRepository $hotelRepository,
        OrderRepository $orderRepository,
        RoomRepository $roomRepository,
        RatePlanRepository $ratePlanRepository,
        RatePlanPriceRepository $ratePlanPriceRepository
    ) {
        $this->updater = $updater;
        $this->login = getenv('TL_LOGIN');
        $this->password = getenv('TL_PASS');
        $this->logger = $logger;
        $this->hotelRepository = $hotelRepository;
        $this->orderRepository = $orderRepository;
        $this->roomRepository = $roomRepository;
        $this->ratePlanRepository = $ratePlanRepository;
        $this->ratePlanPriceRepository = $ratePlanPriceRepository;
        $this->serializer = new Serializer();
    }

    public function processRequest(TravelLineRequestDto $dto): array
    {
        $action = $dto->getAction();

        switch ($action) {
            case 'get-rooms-and-rate-plans':
                return $this->getRoomsAndRatePlans($dto->getData());
            case 'get-bookings':
                return $this->GetBookingsAction();
            case 'confirm-bookings':
                return $this->confirmBookingsAction($dto->getData());
            case 'update':
                return $this->updateAction($dto->getData());
            default:
                return [
                    'success' => 'false',
                    'errors' => [
                        'code' => 404,
                        'message' => 'Метод не найден'
                    ]
                ];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function getRoomsAndRatePlans(array $data): array {
        $hotel = $this->hotelRepository->get(new Uuid($data['hotelId']));
        $rooms = $hotel->getRooms();
        $result = [
            'hotelId' => $this->serializer->asUuid($hotel->getId()),
            'errors' => [],
            'warnings' => []
        ];
        /**
         * @var Room $room
         * @var RatePlan $ratePlan
         */
        foreach ($rooms as $room) {
            $ratePlans = $room->getRatePlans();
            $roomArray = [];
            $roomArray['roomsAndRatePlans'][] = [
                'roomTypeId' => $this->serializer->asUuid($room->getId()),
                'roomName' => $this->serializer->asArray($room->getName()),
                'ratePlans' => []
            ];

            foreach ($ratePlans as $ratePlan) {
                $roomArray['ratePlans'][] = [
                    'ratePlanId' => $this->serializer->asUuid($ratePlan->getId()),
                    'ratePlanName' => $this->serializer->asArray($ratePlan->getName()),
                ];
            }
            $roomArray['occupancies'] = [];
            $result['roomsAndRatePlans'][] = $roomArray;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getBookingsAction(): array {
        $orders = $this->orderRepository->fetchAllOrdersRoom();
        $bookings = [];

        foreach ($orders as $order) {
            $booking = [];
            $booking['number'] = $order->getId()->getValue();
            $booking['created'] = $order->getCreatedAt()->format('Y-m-d h:i:s');
            $booking['status'] = $order->getStatus();
            $booking['currencyCode'] = "RUB";
            $booking['paymentMethod'] = "";
            $booking['guestComment'] = "";
            $booking['additionalInfo'] = "";

            foreach ($order->getOrderItemsRoom() as $itemRoom) {
                $room = $itemRoom->getRoom();
                $result['roomTypeId'] = $room->getId()->getValue();
                $result['ratePlanId'] = "";
                $result['adults'] = "";
                $result['children'] = "";
                $result['commission'] = "";
                $result['guests'] = [];
                $result['bookingPerDayPrices'] = [];

                $bookingUser = $order->getUser();
                $result['customer'] = [];
                $result['customer']['firstName'] = $bookingUser->getFirstName() ? $bookingUser->getFirstName()->getValue() : null;
                $result['customer']['lastName'] = $bookingUser->getLastName() ? $bookingUser->getLastName()->getValue() : null;
                $result['customer']['middleName'] = $bookingUser->getMiddleName() ? $bookingUser->getMiddleName()->getValue() : null;
                $result['customer']['email'] = $bookingUser->getEmail() ? $bookingUser->getEmail()->getValue() : null;
                $result['customer']['phone'] = $bookingUser->getPhone() ? $bookingUser->getPhone()->getValue() : null;

                if (!isset($booking['hotelId'])) {
                    $booking['hotelId'] = $room->getHotel()->getId()->getValue();
                }

                if (!isset($booking['arrivalTime'])) {
                    $booking['arrivalTime'] = $itemRoom->getActivationDate();
                }

                $booking['roomStays'][] = $result;
            }

            $bookings[] = $booking;
        }

        return $bookings;
    }

    /**
     * @param array $data
     * @return array
     */
    public function confirmBookingsAction(array $data): array {
        $orders = [];

        foreach ($data['confirmBookings'] as $booking) {
            $order = $this->orderRepository->get(new Uuid($booking['number']));

            if ($order) {
//            $bookObj->Update(['UF_CONFIRMED' => true]);
                return ['success' => true];
            } else {
                return [
                    "success" => false,
                    "data" => [],
                    "warnings" => [],
                    "errors" => [
                        [
                            "code" => 1,
                            "message" => "No booking with that number"
                        ]
                    ],
                ];
            }
        }
    }

    public function makeError(int $code, string $message): array {
        return [
            "code" => $code,
            "message" => $message
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public function updateAction(array $data): array {
        $hotel = $this->hotelRepository->get(new Uuid($data['hotelId']));

        if (!$hotel) {
            $result = [
                "success" => false,
                "errors" => [
                    $this->makeError(361, 'Hotel with such credentials is not exist in channel')
                ],
            ];
            return $result;
        }

        $updates = $data["updates"];

        $rooms = $hotel->getRooms();
        $roomsMapExternalId = [];

        foreach ($rooms as $room) {
            $roomsExternalIds[$room->getId()->getValue()] = $room->getId()->getValue();
            $roomsMapExternalId[$room->getId()->getValue()] = $room;
        }

        //@todo добавить обработку квот
        foreach ($updates as $update) {
            $room = $this->roomRepository->get(new Uuid($update["roomTypeId"]));
            $ratePlans = $room->getRatePlans();
            /**
             * @var RatePlan $ratePlan
             */
            $readyRatePlans = [];
            foreach ($ratePlans as $ratePlan) {
                //@todo переделать на externalId возможно
                $readyRatePlans[$ratePlan->getId()->getValue()] = $ratePlan;
            }

            if (!$readyRatePlans[$update['ratePlanId']]) {
                return $this->makeError(436, 'Invalid rate plan');
            }

            $ratePlan = $readyRatePlans[$update['ratePlanId']];
            $ratePlanPrices = $ratePlan->getRatePlanPrices();

            $readyRatePlanPrices = [];
            foreach ($ratePlanPrices as $ratePlanPrice) {
                $readyRatePlanPrices[$ratePlanPrice->getId()->getValue()] = $ratePlanPrice;
            }

            foreach ($update['prices'] as $price) {
                if ($readyRatePlanPrices[$price['code']]) {
                    /**
                     * @var RatePlanPrice[] $readyRatePlanPrices
                     */
                    $this->updater->update(
                        $readyRatePlanPrices[$price['code']],
                        $readyRatePlanPrices[$price['code']]->getRatePlan(),
                        $readyRatePlanPrices[$price['code']]->getRoom(),
                        $readyRatePlanPrices[$price['code']]->getDate(),
                        $price['price'],
                        $readyRatePlanPrices[$price['code']]->getValue()
                    );
                }
            }
        }

        return [
            "success" => true,
        ];
    }
}
