<?php

declare(strict_types=1);

namespace App\TravelLine\Dto;

use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductPriceDto.
 */
class TravelLineRequestDto
{
    /**
     * @Assert\NotBlank()
     */
    private string $name;

    /**
     * @Assert\NotBlank()
     */
    private string $pass;

    /**
     * @Assert\NotBlank()
     */
    private string $action;
    private ?array $data = null;

    /**
     * TravelLineRequestDto constructor.
     * @param string $name
     * @param string $pass
     * @param string $action
     */
    public function __construct(string $name, string $pass, string $action)
    {
        $this->name = $name;
        $this->pass = $pass;
        $this->action = $action;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
}
