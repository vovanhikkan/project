<?php

declare(strict_types=1);

namespace App\Review\Command\ReviewStarType\Delete;

use App\Application\ValueObject\Uuid;
use App\Review\Service\ReviewStarType\Deleter;

class Handler
{
    private Deleter $deleter;


    public function __construct(Deleter $deleter)
    {
        $this->deleter = $deleter;
    }


    public function handle(Command $command): array
    {
        $this->deleter->delete(new Uuid($command->getId()));
                return  ['message' => 'ReviewStarType удалена'];
    }
}
