<?php

declare(strict_types=1);

namespace App\Review\Command\ReviewStarType\Update;

use App\Application\ValueObject\Uuid;
use App\Review\Model\ReviewStarType\Name;
use App\Review\Model\ReviewStarType\ReviewStarType;
use App\Review\Repository\ReviewStarTypeRepository;
use App\Review\Service\ReviewStarType\Updater;

class Handler
{
    private Updater $updater;
    private ReviewStarTypeRepository $reviewStarTypeRepository;


    public function __construct(Updater $updater, ReviewStarTypeRepository $reviewStarTypeRepository)
    {
        $this->reviewStarTypeRepository = $reviewStarTypeRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): ReviewStarType
    {
        $reviewStarType = $this->reviewStarTypeRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $reviewStarType,
            new Name($command->getName()),
        );
    }
}
