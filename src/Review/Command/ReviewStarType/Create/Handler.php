<?php

declare(strict_types=1);

namespace App\Review\Command\ReviewStarType\Create;

use App\Application\ValueObject\Uuid;
use App\Review\Model\ReviewStarType\ReviewStarType;
use App\Review\Model\ReviewStarType\Name;
use App\Review\Service\ReviewStarType\Creator;

class Handler
{
    private Creator $creator;

    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): ReviewStarType
    {
        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName())
        );
    }
}
