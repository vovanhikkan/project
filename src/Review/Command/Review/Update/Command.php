<?php

declare(strict_types=1);

namespace App\Review\Command\Review\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    /**
     * @Assert\NotBlank()
     *
     * private string $body;
     */
    private string $body;

    /**
     * @Assert\NotBlank()
     *
     * private int $rating;
     */
    private int $rating;
    
    
    public function __construct(string $id, string $body, int $rating)
    {
        $this->id = $id;
        $this->body = $body;
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }
    
    
}
