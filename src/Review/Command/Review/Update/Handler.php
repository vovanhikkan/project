<?php

declare(strict_types=1);

namespace App\Review\Command\Review\Update;

use App\Application\ValueObject\Uuid;
use App\Review\Model\Review\Body;
use App\Review\Model\Review\Rating;
use App\Review\Model\Review\Review;
use App\Review\Repository\ReviewRepository;
use App\Review\Service\Review\Updater;

class Handler
{
    private Updater $updater;
    private ReviewRepository $reviewRepository;


    public function __construct(Updater $updater, ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): Review
    {
        $review = $this->reviewRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $review,
            new Body($command->getBody()),
            new Rating($command->getRating())
        );
    }
}
