<?php

declare(strict_types=1);

namespace App\Review\Command\Review\Create;

use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use App\Hotel\Repository\HotelRepository;
use App\Place\Model\Place\Place;
use App\Place\Repository\PlaceRepository;
use App\Review\Dto\ReviewStarDto;
use App\Review\Model\Review\Body;
use App\Review\Model\Review\Rating;
use App\Review\Model\Review\Review;
use App\Review\Model\ReviewStar\ReviewStar;
use App\Review\Model\ReviewStar\Value;
use App\Review\Model\ReviewStarType\ReviewStarType;
use App\Review\Repository\ReviewStarTypeRepository;
use App\Review\Service\Review\Creator;

class Handler
{
    private Creator $creator;
    private UserRepository $userRepository;
    private PlaceRepository $placeRepository;
    private HotelRepository $hotelRepository;
    private ReviewStarTypeRepository $reviewStarTypeRepository;

    public function __construct(
        Creator $creator,
        UserRepository $userRepository,
        PlaceRepository $placeRepository,
        HotelRepository $hotelRepository,
        ReviewStarTypeRepository $reviewStarTypeRepository
    ) {
        $this->creator = $creator;
        $this->userRepository = $userRepository;
        $this->placeRepository = $placeRepository;
        $this->hotelRepository = $hotelRepository;
        $this->reviewStarTypeRepository = $reviewStarTypeRepository;
    }


    public function handle(Command $command): Review
    {
        if ($user = $command->getUser()) {
            $user = $this->userRepository->get(new Uuid($user));
        }

        $reviewStars = [];
        if ($command->getReviewStars()) {
            /** @var StarDto $starDto */
            foreach ($command->getReviewStars() as $starDto) {
                $reviewStars[] = new ReviewStarDto(
                    $this->reviewStarTypeRepository->get(new Uuid($starDto->getStarType())),
                    new Value($starDto->getValue()),
                );
            }
        }
        
        return $this->creator->create(
            Uuid::generate(),
            $user,
            new Body($command->getBody()),
            $command->getRating() !== null ? new Rating($command->getRating()) : null,
            $command->getPlace() !== null ? $this->placeRepository->get(new Uuid($command->getPlace())) : null,
            $command->getHotel() !== null ? $this->hotelRepository->get(new Uuid($command->getHotel())) : null,
            $reviewStars
        );
    }
}
