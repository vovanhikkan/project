<?php

declare(strict_types=1);

namespace App\Review\Command\Review\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * PriceDto.
 */
class StarDto
{
    /**
     * @Assert\NotBlank(message="Выберите тип оценки")
     */
    private string $starType;

    /**
     * @Assert\NotBlank(message="Заполните value")
     */
    private int $value;

    /**
     * StarDto constructor.
     * @param string $starType
     * @param int $value
     */
    public function __construct(string $starType, int $value)
    {
        $this->starType = $starType;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getStarType(): string
    {
        return $this->starType;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
  
}
