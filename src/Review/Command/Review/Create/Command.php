<?php

declare(strict_types=1);

namespace App\Review\Command\Review\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $user;

    /**
     * @Assert\NotBlank()
     */
    private string $body;

    private ?int $rating = null;
    private ?string $place = null;
    private ?string $hotel = null;
    private ?array $reviewStars = null;

    /**
     * Command constructor.
     * @param string $user
     * @param string $body
     */
    public function __construct(string $user, string $body)
    {
        $this->user = $user;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getUser() : string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getBody() : string
    {
        return $this->body;
    }

    /**
     * @return int|null $rating
     */
    public function getRating() : ?int
    {
        return $this->rating;
    }

    /**
     * @param int|null $rating
     */
    public function setRating(?int $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return string|null
     */
    public function getPlace(): ?string
    {
        return $this->place;
    }

    /**
     * @param string|null $place
     */
    public function setPlace(?string $place): void
    {
        $this->place = $place;
    }

    /**
     * @return string|null
     */
    public function getHotel(): ?string
    {
        return $this->hotel;
    }

    /**
     * @param string|null $hotel
     */
    public function setHotel(?string $hotel): void
    {
        $this->hotel = $hotel;
    }

    /**
     * @return array|null StarDto[]
     */
    public function getReviewStars(): ?array
    {
        return $this->reviewStars;
    }

    /**
     * @param array|null $reviewStars
     */
    public function setReviewStars(?array $reviewStars): void
    {
        $this->reviewStars = $reviewStars;
    }

}
