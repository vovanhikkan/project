<?php

declare(strict_types=1);

namespace App\Review\Model\ReviewStarType;

use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * ReviewStarType
 * @ORM\Entity()
 * @ORM\Table(name="review_star_type")
 */
class ReviewStarType
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;


    /**
     * @ORM\Column(type="review_star_type_name")
     */
    private ?Name $name = null;


    /**
     * ReviewStarType constructor.
     * @param Uuid $id
     * @param Name $name
     */
    public function __construct(Uuid $id)
    {
        $this->id = $id;
    }

    /**
     * @return Uuid
     */
    public function getId() : Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @param Name|null $name
     */
    public function setName(?Name $name): void
    {
        $this->name = $name;
    }
    
    public function update(?Name $name) : void
    {
        if ($name) {
            $this->name = $name;
        }
    }
}
