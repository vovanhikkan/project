<?php

declare(strict_types=1);

namespace App\Review\Model\Review;

use App\Application\ValueObject\IntegerValueObject;

/**
 * Rating
 */
final class Rating extends IntegerValueObject
{
}
