<?php

declare(strict_types=1);

namespace App\Review\Model\Review;

use App\Application\ValueObject\TextValueObject;

/**
 * Age.
 */
final class Body extends TextValueObject
{
}
