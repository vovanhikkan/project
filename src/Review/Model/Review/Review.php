<?php

declare(strict_types=1);

namespace App\Review\Model\Review;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Hotel\Model\Hotel\Hotel;
use App\Place\Model\Place\Place;
use App\Review\Model\ReviewStar\ReviewStar;
use App\Review\Model\ReviewStar\Value;
use App\Review\Model\ReviewStarType\ReviewStarType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;

/**
 * Review.
 * @ORM\Entity()
 * @ORM\Table(name="review")
 */
class Review
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Auth\Model\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Place\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id")
     */
    private ?Place $place = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private ?Hotel $hotel = null;

    /**
     * @ORM\Column(type="review_review_body")
     */
    private Body $body;

//    /**
//     * @ORM\Column(type="review_review_rating")
//     */
//    private ?Rating $rating = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Review\Model\ReviewStar\ReviewStar", mappedBy="review", cascade={"all"})
     */
    private Collection $reviewStars;

    /**
     * Review constructor.
     * @param Uuid $id
     * @param User $user
     * @param Body $body
     */
    public function __construct(Uuid $id, User $user, Body $body)
    {
        $this->id = $id;
        $this->user = $user;
        $this->body = $body;
        $this->createdAt = new DateTimeImmutable();
        $this->reviewStars = new ArrayCollection();
    }


    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Body
     */
    public function getBody(): Body
    {
        return $this->body;
    }

    /**
     * @return Rating|null
     */
    public function getRating(): ?Rating
    {
        return $this->rating;
    }

    /**
     * @param Rating|null $rating
     */
    public function setRating(?Rating $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return Place|null
     */
    public function getPlace(): ?Place
    {
        return $this->place;
    }

    /**
     * @return array
     */
    public function getReviewStars(): array
    {
        return $this->reviewStars->toArray();
    }

    /**
     * @param Uuid $id
     * @param ReviewStarType $starType
     * @param Value $value
     * @return ReviewStar
     */
    public function addReviewStar(
        Uuid $id,
        ReviewStarType $starType,
        Value $value
    ): ReviewStar {
        $reviewStar = new ReviewStar($id, $this, $starType, $value);
        $this->reviewStars->add($reviewStar);

        return $reviewStar;
    }


    /**
     * @param ReviewStar $reviewStar
     */
    public function removeReviewStar(ReviewStar $reviewStar): void
    {
        $this->reviewStars->removeElement($reviewStar);
    }

    /**
     * @param Place $place
     */
    public function setPlace(Place $place): void
    {
        $this->place = $place;
    }

    /**
     * @return Hotel|null
     */
    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    /**
     * @param Hotel $hotel
     */
    public function setHotel(Hotel $hotel): void
    {
        $this->hotel = $hotel;
    }

    public function update(
        Body $body,
        Rating $rating,
        ?Place $place,
        ?Hotel $hotel
    ): void {
        $isUpdated = false;
        if ($body) {
            $this->body = $body;
            $isUpdated = true;
        }

        if ($rating) {
            $this->rating = $rating;
            $isUpdated = true;
        }

        if ($place) {
            $this->place = $place;
            $this->hotel = null;
        }

        if ($hotel) {
            $this->hotel = $hotel;
            $this->place = null;
        }

        if ($isUpdated) {
            $this->updatedAt = new DateTimeImmutable();
        }
    }
}
