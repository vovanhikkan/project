<?php

declare(strict_types=1);

namespace App\Review\Model\ReviewStar;

use App\Application\ValueObject\Uuid;
use App\Review\Model\Review\Review;
use App\Review\Model\ReviewStarType\ReviewStarType;
use Doctrine\ORM\Mapping as ORM;

/**
 * ReviewStar
 * @ORM\Entity()
 * @ORM\Table(name="review_star")
 */
class ReviewStar
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;


    /**
     * @ORM\Column(type="review_review_star_value", name="value")
     */
    private Value $value;


    /**
     * @ORM\ManyToOne(targetEntity="App\Review\Model\Review\Review")
     * @ORM\JoinColumn(name="review_id", referencedColumnName="id")
     */
    private Review $review;

    /**
     * @ORM\ManyToOne(targetEntity="App\Review\Model\ReviewStarType\ReviewStarType")
     * @ORM\JoinColumn(name="review_star_type_id", referencedColumnName="id")
     */
    private ReviewStarType $reviewStarType;


    /**
     * ReviewStar constructor.
     * @param Uuid $id
     * @param Review $review
     * @param ReviewStarType $reviewStarType
     * @param Value $value
     */

    public function __construct(
        Uuid $id,
        Review $review,
        ReviewStarType $reviewStarType,
        Value $value
    ) {
        $this->id = $id;
        $this->review = $review;
        $this->reviewStarType = $reviewStarType;
        $this->value = $value;
    }

    /**
     * @return Uuid
     */
    public function getId() : Uuid
    {
        return $this->id;
    }

    /**
     * @return Review
     */
    public function getReview(): Review
    {
        return $this->review;
    }

    /**
     * @return ReviewStarType
     */
    public function getReviewStarType(): ReviewStarType
    {
        return $this->reviewStarType;
    }

    /**
     * @return Value
     */
    public function getValue(): Value
    {
        return $this->value;
    }
    
    public function update(Value $value) : void
    {
        $this->value = $value;
    }
}
