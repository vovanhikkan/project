<?php

declare(strict_types=1);

namespace App\Review\Model\ReviewStar;

use App\Application\ValueObject\IntegerValueObject;

/**
 * Class Value
 * @package App\Review\Model\ReviewStar
 */
final class Value extends IntegerValueObject
{
}
