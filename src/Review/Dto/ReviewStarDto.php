<?php

declare(strict_types=1);

namespace App\Review\Dto;

use App\Application\ValueObject\Uuid;
use App\Review\Model\ReviewStar\Value;
use App\Review\Model\ReviewStarType\ReviewStarType;

/**
 * ReviewStarDto.
 */
class ReviewStarDto
{
    private ?Uuid $id = null;
    private ReviewStarType $reviewStarType;
    private Value $value;


    /**
     * ReviewStarDto constructor.
     * @param ReviewStarType $reviewStarType
     * @param Value $value
     */
    public function __construct(ReviewStarType $reviewStarType, Value $value)
    {
        $this->reviewStarType = $reviewStarType;
        $this->value = $value;
    }


    public function setId(Uuid $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    /**
     * @return ReviewStarType
     */
    public function getReviewStarType(): ReviewStarType
    {
        return $this->reviewStarType;
    }

    /**
     * @return Value
     */
    public function getValue(): Value
    {
        return $this->value;
    }

}
