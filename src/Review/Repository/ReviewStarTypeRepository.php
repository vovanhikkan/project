<?php

declare(strict_types=1);

namespace App\Review\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Review\Model\ReviewStarType\ReviewStarType;

class ReviewStarTypeRepository extends AbstractRepository
{
    public function add(ReviewStarType $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('r');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): ReviewStarType
    {
        /** @var ReviewStarType|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('ReviewStarType with id %s not found', (string)$id));
        }

        return $model;
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    public function getModelClassName(): string
    {
        return ReviewStarType::class;
    }
}
