<?php

declare(strict_types=1);

namespace App\Review\Service\ReviewStarType;

use App\Data\Flusher;
use App\Review\Model\ReviewStarType\Name;
use App\Review\Model\ReviewStarType\ReviewStarType;
use App\Review\Repository\ReviewStarTypeRepository;

class Updater
{

    private Flusher $flusher;
    private ReviewStarTypeRepository $reviewStarTypeRepository;


    public function __construct(
        ReviewStarTypeRepository $reviewStarTypeRepository,
        Flusher $flusher
    ) {
        $this->reviewStarTypeRepository = $reviewStarTypeRepository;
        $this->flusher = $flusher;
    }


    public function update(ReviewStarType $reviewStarType, ?Name $name): ReviewStarType
    {
        $reviewStarType->update($name);
       
        $this->flusher->flush();
        return $reviewStarType;
    }
}
