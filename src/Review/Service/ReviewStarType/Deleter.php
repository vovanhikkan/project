<?php

declare(strict_types=1);

namespace App\Review\Service\ReviewStarType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Review\Repository\ReviewStarTypeRepository;

class Deleter
{

    private Flusher $flusher;
    private ReviewStarTypeRepository $reviewStarTypeRepository;


    public function __construct(
        ReviewStarTypeRepository $reviewStarTypeRepository,
        Flusher $flusher
    ) {
        $this->reviewStarTypeRepository = $reviewStarTypeRepository;
        $this->flusher = $flusher;
    }

    public function delete(Uuid $id): void
    {
        $this->reviewStarTypeRepository->delete($id);
        $this->flusher->flush();
    }
}
