<?php

declare(strict_types=1);

namespace App\Review\Service\ReviewStarType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Review\Model\ReviewStarType\Name;
use App\Review\Model\ReviewStarType\ReviewStarType;
use App\Review\Repository\ReviewStarTypeRepository;

class Creator
{
    private Flusher $flusher;
    private ReviewStarTypeRepository $reviewStarTypeRepository;


    public function __construct(ReviewStarTypeRepository $reviewStarTypeRepository, Flusher $flusher)
    {
        $this->reviewStarTypeRepository = $reviewStarTypeRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        ?Name $name
    ): ReviewStarType {
        $reviewStarType = new ReviewStarType(
            $id
        );

        if ($name) {
            $reviewStarType->setName($name);
        }

        $this->reviewStarTypeRepository->add($reviewStarType);
        $this->flusher->flush();

        return $reviewStarType;
    }
}
