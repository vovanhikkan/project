<?php
declare(strict_types=1);

namespace App\Review\Service\Review;

use App\Data\Flusher;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Repository\HotelRepository;
use App\Place\Model\Place\Place;
use App\Place\Model\Place\Rating as PlaceRating;
use App\Hotel\Model\Hotel\Rating as HotelRating;
use App\Place\Repository\PlaceRepository;
use App\Review\Repository\ReviewRepository;
use App\Place\Service\Place\Updater as PlaceUpdater;
use App\Hotel\Service\Hotel\Updater as HotelUpdater;

abstract class AbstractReviewService
{

    const RATING_ADD = 1;
    const RATING_CHANGE = 2;
    const RATING_REMOVE = 3;


    protected Flusher $flusher;
    protected ReviewRepository $reviewRepository;
    protected PlaceRepository $placeRepository;
    private PlaceUpdater $placeUpdater;
    protected HotelRepository $hotelRepository;
    private HotelUpdater $hotelUpdater;


    public function __construct(
        ReviewRepository $reviewRepository,
        PlaceRepository $placeRepository,
        HotelRepository $hotelRepository,
        Flusher $flusher,
        PlaceUpdater $placeUpdater,
        HotelUpdater $hotelUpdater
    ) {
        $this->reviewRepository = $reviewRepository;
        $this->placeRepository = $placeRepository;
        $this->hotelRepository = $hotelRepository;
        $this->flusher = $flusher;
        $this->placeUpdater = $placeUpdater;
        $this->hotelUpdater = $hotelUpdater;
    }

    protected function calcPlaceNewRating(Place $place, int $actionType, int $newRating, int $oldRating = -1) : void
    {

        if ($place->getRating()) {
            $placeRating = $place->getRating()->getValue();
        } else {
            $placeRating = ['1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0];
        }

        switch ($actionType) {
            case self::RATING_ADD:
                $placeRating[$newRating] += 1;
                break;
            case self::RATING_CHANGE:
                $placeRating[$oldRating] -= 1;
                $placeRating[$newRating] += 1;
                break;
            case self::RATING_REMOVE:
                $placeRating[$newRating] -= 1;
                break;
        }

        $this->placeUpdater->update($place, new PlaceRating($placeRating));
    }

    protected function calcHotelNewRating(Hotel $hotel, int $actionType, int $newRating, int $oldRating = -1) : void
    {
        if ($hotel->getRating()) {
            $hotelRating = $hotel->getRating()->getValue();
        } else {
            $hotelRating = ['1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0];
        }

        switch ($actionType) {
            case self::RATING_ADD:
                $hotelRating[$newRating] += 1;
                break;
            case self::RATING_CHANGE:
                $hotelRating[$oldRating] -= 1;
                $hotelRating[$newRating] += 1;
                break;
            case self::RATING_REMOVE:
                $hotelRating[$newRating] -= 1;
                break;
        }

        $this->hotelUpdater->updateRating($hotel, new HotelRating($hotelRating));
    }
}
