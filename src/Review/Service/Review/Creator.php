<?php

declare(strict_types=1);

namespace App\Review\Service\Review;

use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Hotel\Model\Hotel\Hotel;
use App\Place\Model\Place\Place;
use App\Review\Dto\ReviewStarDto;
use App\Review\Model\Review\Body;
use App\Review\Model\Review\Rating;
use App\Review\Model\Review\Review;
use App\Review\Model\ReviewStarType\ReviewStarType;

class Creator extends AbstractReviewService
{

    public function create(
        Uuid $id,
        User $user,
        Body $body,
        ?Rating $rating,
        ?Place $place,
        ?Hotel $hotel,
        ?array $reviewStars
    ): Review {
        
        $review = new Review(
            $id,
            $user,
            $body
        );
        
        if ($place) {
            $review->setPlace($place);
            if ($rating) {
                $review->setRating($rating);
            }
        }

        if (is_null($review->getPlace())) {
            if ($hotel) {
                $review->setHotel($hotel);
            }
        }

        if ($review->getPlace()) {
            $this->calcPlaceNewRating($place, AbstractReviewService::RATING_ADD, $rating->getValue());
        } elseif ($review->getHotel()) {

            /** @var ReviewStarDto $reviewStarDto */
            foreach ($reviewStars as $reviewStarDto) {
                $review->addReviewStar(
                    Uuid::generate(),
                    $reviewStarDto->getReviewStarType(),
                    $reviewStarDto->getValue()
                );
            }
        }

        $this->reviewRepository->add($review);
        $this->flusher->flush();

        return $review;
    }
}
