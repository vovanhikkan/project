<?php

declare(strict_types=1);

namespace App\Review\Service\Review;

use App\Data\Flusher;
use App\Place\Model\Place\Place;
use App\Review\Model\Review\Body;
use App\Review\Model\Review\Rating;
use App\Review\Model\Review\Review;
use App\Review\Repository\ReviewRepository;

class Updater extends AbstractReviewService
{

    /**
     * @param Review $review
     * @param Body $body
     * @param Rating $rating
     * @return Review
     */
    public function update(Review $review, Body $body, Rating $rating): Review
    {
        $place = null;
        $hotel = null;
        if ($review->getPlace()) {
            $place = $this->placeRepository->get($review->getPlace()->getId());
        } elseif ($review->getHotel()) {
            $hotel = $this->hotelRepository->get($review->getHotel()->getId());
        }

        $oldRating = $review->getRating();

        if ($place) {
            $this->calcPlaceNewRating(
                $place,
                AbstractReviewService::RATING_CHANGE,
                $rating->getValue(),
                $oldRating->getValue()
            );
        } elseif ($hotel) {
            $this->calcHotelNewRating(
                $hotel,
                AbstractReviewService::RATING_CHANGE,
                $rating->getValue(),
                $oldRating->getValue()
            );
        }

        $review->update($body, $rating, $place, $hotel);
       
        $this->flusher->flush();
        return $review;
    }
}
