<?php

declare(strict_types=1);

namespace App\Review\Service\Review;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Review\Repository\ReviewRepository;

class Deleter extends AbstractReviewService
{

    public function delete(Uuid $id): void
    {
        $review = $this->reviewRepository->get($id);
        $rating = $review->getRating();

        $place = null;
        $hotel = null;
        if ($review->getPlace()) {
            $place = $this->placeRepository->get($review->getPlace()->getId());
        } elseif ($review->getHotel()) {
            $hotel = $this->hotelRepository->get($review->getHotel()->getId());
        }


        if ($place) {
            $this->calcPlaceNewRating($place, AbstractReviewService::RATING_REMOVE, $rating->getValue());
        } elseif ($hotel) {
            $this->calcHotelNewRating($hotel, AbstractReviewService::RATING_REMOVE, $rating->getValue());
        }

        $this->reviewRepository->delete($id);
        $this->flusher->flush();
    }
}
