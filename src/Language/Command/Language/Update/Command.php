<?php

declare(strict_types=1);

namespace App\Language\Command\Language\Update;

/**
 * Command.
 */
class Command
{
    private string $id;
    private string $name;
    private string $code;

    /**
     * Command constructor.
     * @param string $id
     * @param string $name
     * @param string $code
     */
    public function __construct(string $id, string $name, string $code)
    {
        $this->id = $id;
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
