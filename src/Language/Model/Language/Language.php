<?php
declare(strict_types=1);

namespace App\Language\Model\Language;

use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @ORM\Table(name="language")
 * @ORM\Entity()
 */
class Language
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="language_language_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="language_language_code")
     */
    private Code $code;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * Language constructor.
     * @param Uuid $id
     * @param Name $name
     * @param Code $code
     * @param bool $isActive
     */
    public function __construct(Uuid $id, Name $name, Code $code, bool $isActive)
    {
        $this->id = $id;
        $this->name = $name;
        $this->code = $code;
        $this->isActive = $isActive;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Code
     */
    public function getCode(): Code
    {
        return $this->code;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function update(Name $name, Code $code)
    {
        $this->name = $name;
        $this->code = $code;
    }
}
