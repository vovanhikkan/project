<?php

declare(strict_types=1);

namespace App\Order\Service\Order;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Order\Model\Cart\Cart;
use App\Order\Model\Cart\CartItemBundle;
use App\Order\Model\Cart\CartItemRoomGuest;
use App\Order\Model\Order\Order;
use App\Order\Repository\OrderRepository;
use App\Order\Service\Cart\BundlePriceCalculator;
use App\Order\Service\Cart\RoomPriceCalculator;
use App\Pps\Service\External\OrderCreator;
use App\Product\Model\CardType\Type;
use App\Product\Model\Quota\Quota;
use App\Product\Model\QuotaValue\QuotaValue;
use App\Product\Repository\CardTypeRepository;
use App\Product\Service\Product\PriceDetector;
use Psr\Log\LoggerInterface;
use Throwable;

/**
 * Creator.
 */
class Creator
{
    private OrderRepository $orderRepository;
    private CardTypeRepository $cardTypeRepository;
    private PriceDetector $priceDetector;
    private Flusher $flusher;
    private LoggerInterface $logger;
    private OrderCreator $orderCreator;
    private RoomPriceCalculator $roomPriceCalculator;
    private BundlePriceCalculator $bundlePriceCalculator;

    public function __construct(
        OrderRepository $orderRepository,
        CardTypeRepository $cardTypeRepository,
        PriceDetector $priceDetector,
        Flusher $flusher,
        LoggerInterface $logger,
        OrderCreator $orderCreator,
        RoomPriceCalculator $roomPriceCalculator,
        BundlePriceCalculator $bundlePriceCalculator
    ) {
        $this->orderRepository = $orderRepository;
        $this->cardTypeRepository = $cardTypeRepository;
        $this->priceDetector = $priceDetector;
        $this->flusher = $flusher;
        $this->logger = $logger;
        $this->orderCreator = $orderCreator;
        $this->roomPriceCalculator = $roomPriceCalculator;
        $this->bundlePriceCalculator = $bundlePriceCalculator;
    }

    public function createFromCart(Cart $cart): Order
    {
        $order = new Order(Uuid::generate(), $cart->getUser());
        foreach ($cart->getCartItemsProduct() as $cartItem) {
//            $quotas = $cartItem->getProduct()->getProductQuotas();
//
//            foreach ($quotas as $quota) {
//                /**
//                 * @var Quota $quota
//                 */
//                $quotaValues = $quota->getQuotaValues();
//
//                foreach ($quotaValues as $quotaValue) {
//                    /**
//                     * @var QuotaValue $quotaValue
//                     */
//                    $quotaValue->updateHoldedQuantity($cartItem->getQuantity());
//                }
//            }

            $cardType = null;
            if ($cartItem->getProduct()->getProductSettings() !== null) {
                $cardType = $this->cardTypeRepository->fetchOneByType(new Type(
                    $cartItem->getProduct()->getProductSettings()->getCardType()->getValue()
                ));
            }

            $orderItem = $order->addOrderItemProduct(
                Uuid::generate(),
                $cartItem->getProduct(),
                $this->priceDetector->detect($cartItem->getProduct()),
                $cartItem->getQuantity(),
                $cartItem->getActivationDate(),
                $cartItem->getLastName(),
                $cartItem->getFirstName(),
                $cartItem->getMiddleName(),
                $cartItem->getBirthDate(),
                $cartItem->getCardNum(),
                $cardType
            );

            $orderItem->setActivationDate($cartItem->getActivationDate());
        }

        foreach ($cart->getCartItemsRoom() as $cartItem) {
            $room = $cartItem->getRoom();
            $bedType = $cartItem->getBedType();
            $ratePlan = $cartItem->getRatePlan();
            $foodType = $cartItem->getFoodType();
            $orderItem = $order->addOrderItemRoom(
                Uuid::generate(),
                $room,
                $bedType,
                $ratePlan,
                $foodType,
                $this->roomPriceCalculator->getMinPrice(
                    $room,
                    $ratePlan,
                    $cartItem->getDateFrom(),
                    $cartItem->getDateTo()
                ),
                $cartItem->getQuantity(),
                $cartItem->getDateFrom(),
                $cartItem->getDateTo(),
                $cartItem->getAdultCount(),
                $cartItem->getChildCount(),
                $cartItem->getActivationDate(),
            );

            $orderItem->setActivationDate($cartItem->getActivationDate());
        }

        /** @var CartItemRoomGuest $cartItem */
        foreach ($cart->getCartItemsRoomGuest() as $cartItem) {
            $room = $cartItem->getRoom();
            $guest = $cartItem->getGuest();
            $requirements = $cartItem->getRequirements();
            $orderItem = $order->addOrderItemRoomGuest(
                Uuid::generate(),
                $room,
                $guest,
                $requirements
            );
        }

        foreach ($cart->getCartItemsBundle() as $cartItem) {
            $bundleProduct = $cartItem->getBundleProduct();
            $orderItem = $order->addOrderItemBundle(
                Uuid::generate(),
                $bundleProduct,
                $this->bundlePriceCalculator->getPrice(
                    $bundleProduct,
                    $cartItem->getActivationDate()
                ),
                $cartItem->getQuantity(),
                $cartItem->getActivationDate(),
            );
        }

        $cart->order();


        $this->orderCreator->create($order);

        $order->setEnumNumber(new Id($this->orderRepository->getMaxEnum() + 1));
        $this->orderRepository->add($order);
        $this->flusher->flush();

        return $order;
    }
}
