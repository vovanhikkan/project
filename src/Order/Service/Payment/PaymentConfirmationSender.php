<?php

declare(strict_types=1);

namespace App\Order\Service\Payment;

use App\Application\ValueObject\Email;
use App\Auth\Model\User\User;
use App\Order\Model\Order\Order;
use App\Order\Model\Order\OrderItemBundle;
use App\Order\Model\Order\OrderItemProduct;
use App\Order\Model\Order\OrderItemRoom;
use App\Order\Model\Payment\Payment;
use App\Service\MailerService;
use Slim\Views\Twig;

/**
 * PaymentConfirmationSender.
 */
class PaymentConfirmationSender
{
    private Twig $twig;
    private MailerService $mailerService;

    public function __construct(
        Twig $twig,
        MailerService $mailerService
    ) {
        $this->twig = $twig;
        $this->mailerService = $mailerService;
    }

    public function send(User $user, Order $order): void
    {
        $orderItems = [];

        foreach ($order->getOrderItemsProduct() as $orderItem) {
            $tmp['name'] = $orderItem->getProduct()->getName()->getValue();
            $tmp['count'] = $orderItem->getQuantity()->getValue();
            $tmp['price'] = $orderItem->getPrice()->toRoubles() * $tmp['count'];
            $orderItems['products'][] = $tmp;
        }

        foreach ($order->getOrderItemsRoom() as $orderItem) {
            $tmp['name'] = $orderItem->getRoom()->getName()->getValue()['ru'];
            $tmp['bedType'] = $orderItem->getBedType()->getName()->getValue()['ru'];
            $tmp['count'] = $orderItem->getQuantity()->getValue();
            $tmp['price'] = $orderItem->getPrice()->toRoubles() * $tmp['count'];
            $orderItems['rooms'][] = $tmp;
        }

        foreach ($order->getOrderItemsBundle() as $orderItem) {
            $tmp['name'] = $orderItem->getBundleProduct()->getProduct()->getName()->getValue();
            $tmp['count'] = $orderItem->getQuantity()->getValue();
            $tmp['price'] = $orderItem->getPrice()->toRoubles() * $tmp['count'];
            $orderItems['bundles'][] = $tmp;
        }

        $body = $this->twig->fetch('payment/success.twig', [
            'userLastName' => $user->getLastName()->getValue(),
            'userFirstName' => $user->getFirstName()->getValue(),
            //'userMiddleName' => $user->getMiddleName()->getValue(),
            'orderId' => $order->getId()->getValue(),
            'orderCreateDate' => $order->getCreatedAt()->format('d.m.Y h:m'),
            'orderTotalSum' => $order->getAmount()->toRoubles(),
            'orderCurrency' => 'руб.',
            'orderItemsProducts' => $orderItems['products'],
            'orderItemsRooms' => $orderItems['rooms'],
            'orderItemsBundles' => $orderItems['bundles'],
            'url' => getenv('FRONT_URL'),
        ]);

        $this->mailerService->send(
            'Вы оплатили заказ на сайте “Роза Хутор”.',
            (string)$user->getEmail(),
            $body
        );
    }
}
