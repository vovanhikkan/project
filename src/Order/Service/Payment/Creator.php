<?php

declare(strict_types=1);

namespace App\Order\Service\Payment;

use App\Application\ValueObject\Url;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Order\Model\Order\Order;
use App\Order\Model\Payment\Payment;
use App\Order\Model\PaySystem\PaySystem;
use App\Order\Repository\PaymentRepository;
use App\Product\Model\Quota\Quota;
use App\Product\Model\QuotaValue\QuotaValue;

/**
 * Creator.
 */
class Creator
{
    private PaymentRepository $paymentRepository;
    private ProviderFactory $providerFactory;
    private Flusher $flusher;

    public function __construct(
        PaymentRepository $paymentRepository,
        ProviderFactory $providerFactory,
        Flusher $flusher
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->providerFactory = $providerFactory;
        $this->flusher = $flusher;
    }

    public function create(Uuid $id, Order $order, PaySystem $paySystem, Url $successUrl): Payment
    {
        $payment = new Payment($id, $order, $paySystem, $successUrl);

//        foreach ($order->getOrderItems() as $orderItem) {
//
//            $quotas = $orderItem->getProduct()->getProductQuotas();
//
//            foreach ($quotas as $quota) {
//                /**
//                 * @var Quota $quota
//                 */
//                $quotaValues = $quota->getQuotaValues();
//
//                foreach ($quotaValues as $quotaValue) {
//                    /**
//                     * @var QuotaValue $quotaValue
//                     */
//                    $quotaValue->updateOrderedQuantity($orderItem->getQuantity());
//                }
//            }
//        }

        $provider = $this->providerFactory->get($payment->getPaySystem()->getProvider());
        $dto = $provider->create($payment);
        $payment->setProviderData($dto->getExternalId(), $dto->getRedirectUrl());

        $this->paymentRepository->add($payment);
        $this->flusher->flush();

        return $payment;
    }
}
