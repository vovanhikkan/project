<?php

declare(strict_types=1);

namespace App\Order\Service\Payment;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Order\Model\Payment\Payment;
use App\Order\Model\Receipt\Provider;
use App\Order\Service\Receipt\Creator as ReceiptCreator;
use App\Pps\Service\External\CashListStatusChanger;
use App\Pps\Service\External\Client;

/**
 * CallbackProcessor.
 */
class CallbackProcessor
{
    private ProviderFactory $providerFactory;
    private Client $client;
    private Flusher $flusher;
    private ReceiptCreator $receiptCreator;
    private CashListStatusChanger $cashListStatusChanger;

    public function __construct(
        ProviderFactory $providerFactory,
        Client $client,
        Flusher $flusher,
        ReceiptCreator $receiptCreator,
        CashListStatusChanger $cashListStatusChanger
    ) {
        $this->providerFactory = $providerFactory;
        $this->client = $client;
        $this->flusher = $flusher;
        $this->receiptCreator = $receiptCreator;
        $this->cashListStatusChanger = $cashListStatusChanger;
    }

    public function process(Payment $payment): bool
    {
        if (!$payment->getStatus()->isNew()) {
            return false;
        }

        $provider = $this->providerFactory->get($payment->getPaySystem()->getProvider());
        $provider->processCallback($payment);

        if ($payment->getStatus()->isConfirmed()) {
            $this->flusher->flush();
            $this->receiptCreator->create(Uuid::generate(), $payment, Provider::atol());
            $cashListId = $payment->getOrder()->getCashListId();
            if ($cashListId->getValue() != 0) {
                $this->cashListStatusChanger->changeStatus(
                    $payment->getOrder()->getCashListId(),
                    CashListStatusChanger::STATUS_PAYED
                );
            }
            return true;
        }

        return false;
    }
}
