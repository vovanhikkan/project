<?php

declare(strict_types=1);

namespace App\Order\Service\Payment;

use App\Order\Exception\Payment\ProviderNotSupportedException;
use App\Order\Model\PaySystem\Provider;
use App\Order\Service\Payment\Provider\AlfaBank;
use Psr\Log\LoggerInterface;

/**
 * PaymentProviderFactory.
 */
class ProviderFactory
{
    private LoggerInterface $logger;
    private PaymentConfirmationSender $paymentConfirmationSender;

    public function __construct(
        LoggerInterface $logger,
        PaymentConfirmationSender $paymentConfirmationSender
    ) {
        $this->logger = $logger;
        $this->paymentConfirmationSender = $paymentConfirmationSender;
    }

    public function get(Provider $provider): ProviderInterface
    {
        switch (true) {
            case $provider->isAlfaBank():
                return new AlfaBank($this->logger, $this->paymentConfirmationSender);
            default:
                throw new ProviderNotSupportedException($provider);
        }
    }
}
