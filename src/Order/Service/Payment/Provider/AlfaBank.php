<?php

declare(strict_types=1);

namespace App\Order\Service\Payment\Provider;

use App\Order\Dto\PaymentProviderCreateDto;
use App\Order\Model\Payment\Payment;
use App\Order\Service\Payment\ProviderInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use App\Order\Service\Payment\PaymentConfirmationSender;

/**
 * AlfaBank.
 */
class AlfaBank implements ProviderInterface
{
    private LoggerInterface $logger;
    private string $username;
    private string $password;
    private string $returnUrl;
    private ?Client $client = null;
    private PaymentConfirmationSender $paymentConfirmationSender;

    public function __construct(
        LoggerInterface $logger,
        PaymentConfirmationSender $paymentConfirmationSender
    ) {
        $this->logger = $logger;
        $this->username = getenv('ALFA_BANK_USERNAME');
        $this->password = getenv('ALFA_BANK_PASSWORD');
        $this->returnUrl = getenv('PAYMENT_RETURN_URL');
        $this->paymentConfirmationSender = $paymentConfirmationSender;
    }

    public function create(Payment $payment): PaymentProviderCreateDto
    {
        $payload = [
            'userName' => $this->username,
            'password' => $this->password,
            'orderNumber' => (string)$payment->getId(),
            'amount' => $payment->getOrder()->getAmount()->getValue() * 100,
            'returnUrl' => $this->returnUrl.'?paymentId='.$payment->getId()->getValue()
        ];

        $result = null;
        try {
            $response = $this->getClient()->post('/ab/rest/register.do', ['query' => $payload]);
            $content = $response->getBody()->getContents();
            $result = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
            $this->logger->debug('create payment success', $result);
        } catch (GuzzleException $e) {
            $this->logger->debug('fail create payment', [$e->getMessage()]);
        }

        return new PaymentProviderCreateDto(
            (string)$result['orderId'],
            (string)$result['formUrl']
        );
    }

    public function getStatus(Payment $payment): int
    {
        $payload = [
            'userName' => $this->username,
            'password' => $this->password,
            'orderId' => $payment->getExternalId(),
        ];

        $result = null;
        try {
            $response = $this->getClient()->post('/ab/rest/getOrderStatus.do', ['query' => $payload]);
            $content = $response->getBody()->getContents();
            $result = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        } catch (GuzzleException $e) {
            $this->logger->debug('fail check payment', [$e->getMessage()]);
            return -1;
        }

        return $result['OrderStatus'] ?? -1;
    }

    public function processCallback(Payment $payment): void
    {
        $status = $this->getStatus($payment);
        if ($status === 2) {
            $payment->confirm();

            $order = $payment->getOrder();
            $user = $order->getUser();
            $this->paymentConfirmationSender->send($user, $order);
        }
    }

    private function getClient(): Client
    {
        if ($this->client === null) {
            $this->client = new Client([
                'base_uri' => 'https://web.rbsuat.com',
                'timeout'  => 5,
            ]);
        }

        return $this->client;
    }
}
