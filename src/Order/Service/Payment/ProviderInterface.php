<?php

declare(strict_types=1);

namespace App\Order\Service\Payment;

use App\Order\Dto\PaymentProviderCreateDto;
use App\Order\Model\Payment\Payment;

/**
 * ProviderInterface.
 */
interface ProviderInterface
{
    public function create(Payment $payment): PaymentProviderCreateDto;

    public function getStatus(Payment $payment): int;

    public function processCallback(Payment $payment): void;
}
