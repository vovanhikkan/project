<?php

declare(strict_types=1);

namespace App\Order\Service\Voucher;

/**
 * Pdf.
 */
class Pdf extends \mikehaertl\wkhtmlto\Pdf
{
    public function addHeader(string $header): Pdf
    {
        $options['input'] = '--header-html';
        $options['inputArg'] = $this->ensureUrlOrFile($header, self::TYPE_HTML);
        $this->_objects[] = $this->ensureUrlOrFileOptions($options);

        return $this;
    }

    public function addFooter(string $footer): Pdf
    {
        $options['input'] = '--footer-html';
        $options['inputArg'] = $this->ensureUrlOrFile($footer, self::TYPE_HTML);
        $this->_objects[] = $this->ensureUrlOrFileOptions($options);

        return $this;
    }
}
