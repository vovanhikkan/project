<?php

declare(strict_types=1);

namespace App\Order\Service\Voucher;

use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Picqer\Barcode\BarcodeGeneratorPNG;

/**
 * BarcodeGenerator.
 */
class BarcodeGenerator
{
    public function generateBarcode(string $code, int $height): string
    {
        $generator = new BarcodeGeneratorPNG();
        return (string)base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128_A, 1, $height));
    }

    public function generateQr(string $code, int $height): string
    {
        $renderer = new ImageRenderer(
            new RendererStyle($height),
            new SvgImageBackEnd()
        );
        $writer = new Writer($renderer);
        $qr = $writer->writeString($code);

        return (string)preg_replace("/^.*\n/", '', $qr);
    }
}
