<?php

declare(strict_types=1);

namespace App\Order\Service\Voucher;

use App\Application\Exception\DomainException;
use App\Order\Model\Order\Order;
use RuntimeException;
use Slim\Views\Twig;

/**
 * Generator.
 */
class Generator
{
    private string $voucherPath;
    private Twig $twig;
    private BarcodeGenerator $barcodeGenerator;

    public function __construct(Twig $twig, BarcodeGenerator $barcodeGenerator)
    {
        $this->voucherPath = getenv('VOUCHER_PATH');
        $this->twig = $twig;
        $this->barcodeGenerator = $barcodeGenerator;
    }

    public function getOrGenerate(Order $order): string
    {
        if (!$order->getStatus()->isPayed()) {
            throw new DomainException('Заказ не был оплачен');
        }

        $filePath = $this->generateVoucherPath($order);
        if (file_exists($filePath)) {
            return $filePath;
        }

        return $this->generate($order);
    }

    private function generate(Order $order): string
    {
        $filePath = $this->generateVoucherPath($order);

        $pdf = new Pdf();

        $data = [];

        $rootPath = realpath(__DIR__ . '/../../../../templates/voucher');
        $data['rootPath'] = $rootPath;

        $data['info'] = $this->generateInfoData($order);

        $header = $this->twig->fetch('voucher/header.twig', $data);
        $pdf->addHeader($header);

        $data['items'] = $this->generateItems($order);
        $body = $this->twig->fetch('voucher/voucher.twig', $data);
        $pdf->addPage($body);

        $footer = $this->twig->fetch('voucher/footer.twig', $data);
        $pdf->addFooter($footer);

        $result = $pdf->saveAs($filePath);

        if ($result !== true) {
            throw new RuntimeException('Не удалось сгенерировать ваучер: ' . $pdf->getError());
        }

        return $filePath;
    }

    private function generateVoucherPath(Order $order): string
    {
        $path = $this->voucherPath . DIRECTORY_SEPARATOR . $order->getCreatedAt()->format('Y-m');
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $filePath = $path . DIRECTORY_SEPARATOR . (string)$order->getId() . '.pdf';

        return $filePath;
    }

    private function generateInfoData(Order $order): array
    {
        return [
            'date' => $order->getCreatedAt()->format('d.m.Y'),
            'number' => $order->getCashListId()->getValue(),
            'fullName' => implode(' ', array_filter(
                [
                    (string)$order->getUser()->getLastName(),
                    (string)$order->getUser()->getFirstName(),
                    $order->getUser()->getMiddleName() !== null ? (string)$order->getUser()->getMiddleName() : null
                ]
            )),
            'phone' => (string)$order->getUser()->getPhone(),
            'email' => (string)$order->getUser()->getEmail(),
            'qrCodeImg' => $order->getBarcode() !== null
                ? $this->barcodeGenerator->generateQr((string)$order->getBarcode(), 170)
                : null,
            'barcode' => $order->getBarcode() !== null ? (string)$order->getBarcode() : null,
            'barcodeImg' => $order->getBarcode() !== null
                ? $this->barcodeGenerator->generateBarcode((string)$order->getBarcode(), 40)
                : null,
        ];
    }

    private function generateItems(Order $order): array
    {
        $items = [];
        foreach ($order->getOrderItemsProduct() as $orderItem) {
            $product = $orderItem->getProduct();
            $items[] = [
                'name' => (string)$product->getName(),
                'quantity' => $orderItem->getQuantity()->getValue(),
                'date' => $orderItem->getActivationDate() !== null
                    ? $orderItem->getActivationDate()->format('d.m.Y')
                    : '—',
                'isSkiPass' => $product->getType()->isSkiPass(),
                'isYetiPark' => $product->getType()->isYetiPark(),
                'isRodelban' => $product->getType()->isRodelban(),
                'isMuseum' => $product->getType()->isMuseum(),
            ];
        }

        return $items;
    }
}
