<?php

declare(strict_types=1);

namespace App\Order\Service\PaySystem;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Order\Model\PaySystem\Code;
use App\Order\Model\PaySystem\Description;
use App\Order\Model\PaySystem\Name;
use App\Order\Model\PaySystem\PaySystem;
use App\Order\Model\PaySystem\Provider;
use App\Order\Repository\PaySystemRepository;
use App\Storage\Model\File\File;

/**
 * Creator.
 */
class Creator
{
    private PaySystemRepository $paySystemRepository;
    private Flusher $flusher;

    public function __construct(
        PaySystemRepository $paySystemRepository,
        Flusher $flusher
    ) {
        $this->paySystemRepository = $paySystemRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Uuid $id,
        Name $name,
        Provider $provider,
        ?Description $description,
        ?Code $code,
        ?File $image
    ): PaySystem {
        $paySystem = new PaySystem($id, $name, $provider, $description, $code, $image);

        $this->paySystemRepository->add($paySystem);
        $this->flusher->flush();

        return $paySystem;
    }
}
