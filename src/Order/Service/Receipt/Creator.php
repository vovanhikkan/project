<?php

declare(strict_types=1);

namespace App\Order\Service\Receipt;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Order\Model\Payment\Payment;
use App\Order\Model\Receipt\Provider;
use App\Order\Model\Receipt\Receipt;
use App\Order\Repository\ReceiptRepository;

/**
 * Creator.
 */
class Creator
{
    private ReceiptRepository $receiptRepository;
    private ProviderFactory $providerFactory;
    private Flusher $flusher;

    public function __construct(
        ReceiptRepository $receiptRepository,
        ProviderFactory $providerFactory,
        Flusher $flusher
    ) {
        $this->receiptRepository = $receiptRepository;
        $this->providerFactory = $providerFactory;
        $this->flusher = $flusher;
    }

    public function create(Uuid $id, Payment $payment, Provider $provider): Receipt
    {
        $receipt = new Receipt($id, $payment, $provider);

        $provider = $this->providerFactory->get($receipt->getProvider());
        $dto = $provider->create($receipt);
        $receipt->setProviderData($dto->getExternalId());

        $this->receiptRepository->add($receipt);
        $this->flusher->flush();

        return $receipt;
    }
}
