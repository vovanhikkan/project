<?php

declare(strict_types=1);

namespace App\Order\Service\Receipt;

use App\Order\Exception\Payment\ProviderNotSupportedException;
use App\Order\Model\Receipt\Provider;
use App\Order\Service\Receipt\Provider\Atol;
use Psr\Log\LoggerInterface;

/**
 * ProviderFactory.
 */
class ProviderFactory
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function get(Provider $provider): ProviderInterface
    {
        switch (true) {
            case $provider->isAtol():
                return new Atol($this->logger);
            default:
                throw new ProviderNotSupportedException($provider);
        }
    }
}
