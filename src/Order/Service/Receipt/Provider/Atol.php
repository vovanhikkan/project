<?php

declare(strict_types=1);

namespace App\Order\Service\Receipt\Provider;

use App\Order\Dto\ReceiptProviderCreateDto;
use App\Order\Dto\ReceiptProviderStatusDto;
use App\Order\Model\Order\OrderItemBundle;
use App\Order\Model\Order\OrderItemProduct;
use App\Order\Model\Order\OrderItemRoom;
use App\Order\Model\Receipt\Receipt;
use App\Order\Service\Receipt\ProviderInterface;
use DateTimeImmutable;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Atol.
 */
class Atol implements ProviderInterface
{
    private LoggerInterface $logger;
    private string $url;
    private string $companyEmail;
    private string $companyTin;
    private string $companyShop;
    private string $groupCode;
    private string $login;
    private string $password;
    private ?Client $client = null;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->url = getenv('ATOL_URL');
        $this->companyEmail = getenv('ATOL_COMPANY_EMAIL');
        $this->companyTin = getenv('ATOL_COMPANY_TIN');
        $this->companyShop = getenv('ATOL_COMPANY_SHOP');
        $this->groupCode = getenv('ATOL_GROUP_CODE');
        $this->login = getenv('ATOL_LOGIN');
        $this->password = getenv('ATOL_PASSWORD');
    }

    public function create(Receipt $receipt): ReceiptProviderCreateDto
    {
        $payment = $receipt->getPayment();

        $data = [
            'timestamp' => (new DateTimeImmutable('now', new \DateTimeZone('+0300')))
                ->format('d.m.Y H:i:s'),
            'external_id' => (string)$payment->getId(),
            'receipt' => [
                'client' => [
                    'email' => (string)$payment->getOrder()->getUser()->getEmail(),
                ],
                'company' => [
                    'email' => $this->companyEmail,
                    'inn' => $this->companyTin,
                    'payment_address' => $this->companyShop
                ],
                'payments' => [[
                    'type' => 1,
                    'sum' => $payment->getOrder()->getAmount()->toRoubles()
                ]],
                'total' => $payment->getOrder()->getAmount()->toRoubles()
            ]
        ];

        $itemsProduct = array_map(
            function (OrderItemProduct $orderItem) {
                return [
                    'name' => (string)$orderItem->getProduct()->getName(),
                    'price' => $orderItem->getAmount()->toRoubles(),
                    'quantity' => (float)$orderItem->getQuantity()->getValue(),
                    'sum' => $orderItem->getAmount()->toRoubles(),
                    'payment_method' => 'full_payment',
                    'payment_object' => 'service',
                    'vat' => ['type' => 'none']
                ];
            },
            $payment->getOrder()->getOrderItemsProduct()
        );

        $itemsRoom = array_map(
            function (OrderItemRoom $orderItem) {
                return [
                    'name' => 'Номер '.(string)$orderItem->getRoom()->getName()->getValue()['ru'].
                                '. Тип спального места: '.(string)$orderItem->getBedType()->getName()->getValue()['ru'],
                    'price' => $orderItem->getAmount()->toRoubles(),
                    'quantity' => (float)$orderItem->getQuantity()->getValue(),
                    'sum' => $orderItem->getAmount()->toRoubles(),
                    'payment_method' => 'full_payment',
                    'payment_object' => 'service',
                    'vat' => ['type' => 'none']
                ];
            },
            $payment->getOrder()->getOrderItemsRoom()
        );

        $itemsBundle = array_map(
            function (OrderItemBundle $orderItem) {
                return [
                    'name' => (string)$orderItem->getBundleProduct()->getProduct()->getName()->getValue(),
                    'price' => $orderItem->getAmount()->toRoubles(),
                    'quantity' => (float)$orderItem->getQuantity()->getValue(),
                    'sum' => $orderItem->getAmount()->toRoubles(),
                    'payment_method' => 'full_payment',
                    'payment_object' => 'service',
                    'vat' => ['type' => 'none']
                ];
            },
            $payment->getOrder()->getOrderItemsBundle()
        );

        $data['receipt']['items'] = array_merge($itemsProduct, $itemsRoom, $itemsBundle);

        $result = null;
        try {
            $response = $this->getClient()
                ->post(
                    'possystem/v4/' . $this->groupCode . '/sell',
                    [
                        'headers' => [
                            'Token' => $this->getToken(),
                        ],
                        'json' => $data,
                    ]
                );
            $content = (string)$response->getBody();
            $result = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
            $this->logger->debug('create receipt success', ['request' => $data, 'response' => $result]);
        } catch (GuzzleException $e) {
            $this->logger->debug('fail create receipt', ['request' => $data, 'error' => $e->getMessage()]);
        }

        return new ReceiptProviderCreateDto($result['uuid']);
    }

    public function getStatus(Receipt $receipt): ReceiptProviderStatusDto
    {
        $response = $this->getClient()
            ->get(
                'possystem/v4/' . $this->groupCode . '/report/' . $receipt->getExternalId(),
                [
                    'headers' => [
                        'Token' => $this->getToken(),
                    ],
                ]
            );

        $content = (string)$response->getBody();
        $result = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        return new ReceiptProviderStatusDto(
            $result['status'],
            isset($result['payload']['ofd_receipt_url']) ? $result['payload']['ofd_receipt_url'] : null
        );
    }

    private function getToken(): string
    {
        $response = $this->getClient()
            ->post(
                'possystem/v4/getToken',
                [
                    'json' => [
                        'login' => $this->login,
                        'pass' => $this->password
                   ]
                ]
            );

        $result = json_decode((string)$response->getBody(), true);

        return $result['token'];
    }

    private function getClient(): Client
    {
        if ($this->client === null) {
            $this->client = new Client(['base_uri' => $this->url]);
        }

        return $this->client;
    }
}
