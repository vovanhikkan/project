<?php

declare(strict_types=1);

namespace App\Order\Service\Receipt;

use App\Data\Flusher;
use App\Order\Model\Receipt\Receipt;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function updateExternalUrl(
        Receipt $receipt,
        string $externalUrl
    ): void {
        $receipt->setExternalUrl($externalUrl);
        $this->flusher->flush();
    }
}
