<?php

declare(strict_types=1);

namespace App\Order\Service\Receipt;

use App\Order\Dto\ReceiptProviderCreateDto;
use App\Order\Dto\ReceiptProviderStatusDto;
use App\Order\Model\Receipt\Receipt;

/**
 * ProviderInterface.
 */
interface ProviderInterface
{
    public function create(Receipt $receipt): ReceiptProviderCreateDto;

    public function getStatus(Receipt $receipt): ReceiptProviderStatusDto;
}
