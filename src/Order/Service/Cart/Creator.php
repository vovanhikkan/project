<?php

declare(strict_types=1);

namespace App\Order\Service\Cart;

use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Data\Flusher;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;

/**
 * Creator.
 */
class Creator
{
    private CartRepository $cartRepository;
    private Flusher $flusher;

    public function __construct(
        CartRepository $cartRepository,
        Flusher $flusher
    ) {
        $this->cartRepository = $cartRepository;
        $this->flusher = $flusher;
    }

    public function create(User $user): Cart
    {
        $cart = new Cart(Uuid::generate(), $user);

        $this->cartRepository->add($cart);
        $this->flusher->flush();

        return $cart;
    }
}
