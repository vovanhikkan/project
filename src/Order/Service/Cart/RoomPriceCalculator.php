<?php

declare(strict_types=1);

namespace App\Order\Service\Cart;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Model\Room\Room;
use App\Hotel\Repository\RoomRepository;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;

/**
 * RoomPriceCalculator.
 */
class RoomPriceCalculator
{
    private RoomRepository $roomRepository;

    private array $dates = [];
    private ?RatePlan $ratePlan = null;

    public function __construct(
        RoomRepository $roomRepository
    ) {
        $this->roomRepository = $roomRepository;
    }

    public function getMinPrice(
        Room $room,
        RatePlan $ratePlan,
        DateTimeImmutable $start,
        DateTimeImmutable $end
    ) : ?Amount {

        $this->ratePlan = $ratePlan;

        $period = new DatePeriod($start, new DateInterval('P1D'), $end->modify('+1 day'));

        $this->dates = [];
        foreach ($period as $date) {
            $this->dates[] = $date->format('Y-m-d');
        }

        $result = $this->getRoomMinPrices($room);

        $tmp = [];
        foreach ($result['prices'] as $date) {
            $tmp['mainPlaceValue'] += $date['mainPlaceValue'];
            $tmp['value'] += $date['value'];
        }

        if (empty($tmp)) {
            return null;
        }

        return new Amount($tmp['mainPlaceValue']);
    }

    private function getRoomMinPrices(Room $room): array
    {
        /**
         * @var Room $room
         */
        $readyRoom = [];
        $tmp = $this->getReadyRatePlans([$this->ratePlan]);
        if (count($tmp) == count($this->dates)) {
            $readyRoom = [
                'name' => $room->getName()->getValue(),
                'prices' => $tmp
            ];
        }

        return $readyRoom;
    }

    private function getReadyRatePlans(array $ratePlans): array
    {
        /**
         * @var RatePlan $ratePlan
         */
        $tmp = [];
        foreach ($ratePlans as $ratePlan) {
            foreach ($this->dates as $date) {
                $dateVO = new DateTimeImmutable($date);
                if ($ratePlan->getActiveFrom() <= $dateVO && $ratePlan->getActiveTo() >= $dateVO) {
                    $tmp[$date][] = $this->processPrices($ratePlan->getRatePlanPrices(), $dateVO);
                }
            }
        }

        $readyRatePlans = [];
        foreach ($tmp as $key => $date) {
            if (count($date)) {
                $readyRatePlans[$key] = $this->minPrice($date);
            }
        }

        return $readyRatePlans;
    }

    private function processPrices(array $ratePlanPrices, DateTimeImmutable $date): array
    {
        /**
         * @var RatePlanPrice $ratePlanPrice
         */
        $readyPrices = [];
        foreach ($ratePlanPrices as $ratePlanPrice) {
            $interval = $date->diff($ratePlanPrice->getDate());
            if ($interval->days == 0) {
                if ($ratePlanPrice->getMainPlaceValue() != null && $ratePlanPrice->getValue() != null
                ) {
                    $readyPrices[] = [
                        "mainPlaceValue" => $ratePlanPrice->getMainPlaceValue()->getValue(),
                        "value" => $ratePlanPrice->getValue()->getValue()
                    ];
                }
            }

        }

        if (count($readyPrices)) {
            return $this->minPrice($readyPrices);
        }
        return [];
    }

    private function minPrice(array $items): array
    {
        $min = 0;
        foreach ($items as $key => $price) {
            if ($min == 0) {
                $min = $price;
            }

            if (isset($min['mainPlaceValue']) && $min['mainPlaceValue'] > $price['mainPlaceValue']) {
                $min = $price;
            }
        }

        return $min;
    }
}
