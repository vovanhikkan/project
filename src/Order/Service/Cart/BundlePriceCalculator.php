<?php

declare(strict_types=1);

namespace App\Order\Service\Cart;

use App\Application\ValueObject\Amount;
use App\Bundle\Model\BundleProduct\BundleProduct;
use DateTimeImmutable;

/**
 * Class BundlePriceCalculator
 * @package App\Order\Service\Cart
 */
class BundlePriceCalculator
{

    public function getPrice(
        BundleProduct $bundleProduct,
        DateTimeImmutable $date
    ) : ?Amount {

        if (is_null($bundleProduct)) {
            return null;
        }

        $result = [];
        foreach ($bundleProduct->getProduct()->getProductPrices() as $productPrice) {
            if ($productPrice->getDateStart() <= $date && $productPrice->getDateEnd() >= $date) {
                $result['bundleProductId'] = $bundleProduct->getId()->getValue();
                $result['price'] = $productPrice->getPrice()->getValue();
            }
        }

        return new Amount($result['price']);
    }

}
