<?php

declare(strict_types=1);

namespace App\Order\Service\Cart;

use App\Auth\Model\User\User;
use App\Data\Flusher;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;

/**
 * Factory.
 */
class Factory
{
    private CartRepository $cartRepository;
    private Flusher $flusher;
    private Creator $creator;
    private RoomPriceCalculator $roomPriceCalculator;

    public function __construct(
        CartRepository $cartRepository,
        Flusher $flusher,
        Creator $creator,
        RoomPriceCalculator $roomPriceCalculator
    ) {
        $this->cartRepository = $cartRepository;
        $this->flusher = $flusher;
        $this->creator = $creator;
        $this->roomPriceCalculator = $roomPriceCalculator;
    }

    public function getActiveOrCreate(User $user): Cart
    {
        $cart = $this->cartRepository->fetchOneActiveByUser($user);
        if ($cart === null) {
            $cart = $this->creator->create($user);
        }

        return $cart;
    }
}
