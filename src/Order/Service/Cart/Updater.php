<?php

declare(strict_types=1);

namespace App\Order\Service\Cart;

use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\MiddleName;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleProduct\BundleProduct;
use App\Data\Flusher;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\Room\Room;
use App\Order\Model\Cart\Cart;
use App\Order\Model\Cart\CartItemBundle;
use App\Order\Model\Cart\CartItemProduct;
use App\Order\Model\Cart\CartItemRoom;
use App\Order\Model\Order\Requirements;
use App\Product\Model\Product\Product;
use DateTimeImmutable;
use InvalidArgumentException;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function addCartItemProduct(
        Cart $cart,
        Product $product,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null,
        ?LastName $lastName = null,
        ?FirstName $firstName = null,
        ?MiddleName $middleName = null,
        ?DateTimeImmutable $birthDate = null,
        ?CardNum $cardNum = null
    ): void {
        $cart->addCartItemProduct(
            Uuid::generate(),
            $product,
            $quantity,
            $activationDate,
            $lastName,
            $firstName,
            $middleName,
            $birthDate,
            $cardNum
        );
        $this->flusher->flush();
    }

    public function addCartItemRoom(
        Cart $cart,
        Room $room,
        BedType $bedType,
        RatePlan $ratePlan,
        FoodType $foodType,
        Quantity $quantity,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateTo,
        Quantity $adultCount,
        Quantity $childCount,
        ?DateTimeImmutable $activationDate = null
    ): void {
        $cart->addCartItemRoom(
            Uuid::generate(),
            $room,
            $bedType,
            $ratePlan,
            $foodType,
            $quantity,
            $dateFrom,
            $dateTo,
            $adultCount,
            $childCount,
            $activationDate
        );
        $this->flusher->flush();
    }


    public function addCartItemRoomGuest(
        Cart $cart,
        Room $room,
        Guest $guest,
        ?Requirements $requirements
    ): void {
        $cart->addCartItemRoomGuest(
            Uuid::generate(),
            $room,
            $guest,
            $requirements
        );
        $this->flusher->flush();
    }

    public function addCartItemBundle(
        Cart $cart,
        BundleProduct $bundleProduct,
        Quantity $quantity,
        DateTimeImmutable $activationDate
    ): void {
        $cart->addCartItemBundle(
            Uuid::generate(),
            $bundleProduct,
            $quantity,
            $activationDate
        );
        $this->flusher->flush();
    }

    public function changeCartItemProductQuantity(Cart $cart, CartItemProduct $cartItem, Quantity $quantity): void
    {
        $cartItem->changeQuantity($quantity);
        $this->flusher->flush();
    }

    public function changeCartItemRoomQuantity(Cart $cart, CartItemRoom $cartItem, Quantity $quantity): void
    {
        $cartItem->changeQuantity($quantity);
        $this->flusher->flush();
    }

    public function changeCartItemBundleQuantity(Cart $cart, CartItemBundle $cartItem, Quantity $quantity): void
    {
        $cartItem->changeQuantity($quantity);
        $this->flusher->flush();
    }

    public function updateCartItemProduct(
        Cart $cart,
        CartItemProduct $cartItemProduct,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null,
        ?LastName $lastName = null,
        ?FirstName $firstName = null,
        ?MiddleName $middleName = null,
        ?DateTimeImmutable $birthDate = null,
        ?CardNum $cardNum = null
    ): void {
        $cartItemProduct->update(
            $quantity,
            $activationDate,
            $lastName,
            $firstName,
            $middleName,
            $birthDate,
            $cardNum
        );
        $this->flusher->flush();
    }

    public function updateCartItemRoom(
        Cart $cart,
        CartItemRoom $cartItemRoom,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ): void {
        $cartItemRoom->update(
            $quantity,
            $activationDate
        );
        $this->flusher->flush();
    }

    public function updateCartItemBundle(
        Cart $cart,
        CartItemBundle $cartItemBundle,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ): void {
        $cartItemBundle->update(
            $quantity,
            $activationDate
        );
        $this->flusher->flush();
    }

    public function removeCartItemProduct(Cart $cart, array $cartItems): void
    {
        foreach ($cartItems as $cartItem) {
            $cart->removeCartItemProduct($cartItem);
        }
        $this->flusher->flush();
    }

    public function removeCartItemRoom(Cart $cart, array $cartItems): void
    {
        foreach ($cartItems as $cartItem) {
            $cart->removeCartItemRoom($cartItem);
        }
        $this->flusher->flush();
    }

    public function removeCartItemRoomGuest(Cart $cart, array $cartItems): void
    {
        foreach ($cartItems as $cartItem) {
            $cart->removeCartItemRoomGuest($cartItem);
        }
        $this->flusher->flush();
    }

    public function removeCartItemBundle(Cart $cart, array $cartItems): void
    {
        foreach ($cartItems as $cartItem) {
            $cart->removeCartItemBundle($cartItem);
        }
        $this->flusher->flush();
    }
}
