<?php

declare(strict_types=1);

namespace App\Order\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Order\Dto\OrderSearchDto;
use App\Order\Model\Order\Order;

/**
 * OrderRepository.
 */
class OrderRepository extends AbstractRepository
{
    public function add(Order $order): void
    {
        $this->entityManager->persist($order);
    }

    /**
     * @param OrderSearchDto|null $searchDto
     * @return Order[]
     */
    public function fetchAll(?OrderSearchDto $searchDto = null): array
    {
        $qb = $this->entityRepository->createQueryBuilder('o');
        $qb->orderBy('o.createdAt', 'DESC');

        if ($searchDto !== null) {
            if ($searchDto->userId !== null) {
                $qb
                    ->innerJoin('o.user', 'u')
                    ->andWhere('u.id = :userId')
                    ->setParameter('userId', (string)$searchDto->userId)
                ;
            }
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Uuid $roomId
     * @return Order|null
     */
    public function findLastOrderByRoomId(Uuid $roomId) : ?Order
    {

        $qb = $this->entityRepository->createQueryBuilder('o');
        $qb->innerJoin('o.orderItemsRoom', 'r')
            ->andWhere('r.room = :roomId')
            ->setParameter('roomId', $roomId->getValue())
            ->orderBy('o.createdAt', 'DESC')
            ->setMaxResults(1);
        
        return $qb->getQuery()->getResult()[0];
    }

    /**
     * @param OrderSearchDto|null
     * @return Order[]
     */
    public function fetchAllOrdersRoom(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('o');
        $qb->innerJoin('o.orderItemsRoom', 'r');
        $qb->orderBy('o.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Order
    {
        /** @var Order|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Order with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxEnum(): int
    {
        return (int)$this->entityRepository->createQueryBuilder('o')
            ->select('MAX(o.enumNumber)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return Order::class;
    }
}
