<?php

declare(strict_types=1);

namespace App\Order\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Order\Model\PaySystem\PaySystem;

/**
 * PaySystemRepository.
 */
class PaySystemRepository extends AbstractRepository
{
    public function add(PaySystem $model): void
    {
        $this->entityManager->persist($model);
    }

    /**
     * @return PaySystem[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('ps');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): PaySystem
    {
        /** @var PaySystem|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('PaySystem with id %s not found', (string)$id));
        }

        return $model;
    }

    protected function getModelClassName(): string
    {
        return PaySystem::class;
    }
}
