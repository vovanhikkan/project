<?php

declare(strict_types=1);

namespace App\Order\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Order\Model\Cart\Cart;
use App\Order\Model\Cart\Status;
use App\Order\Model\PaySystem\PaySystem;

/**
 * CartRepository.
 */
class CartRepository extends AbstractRepository
{
    public function add(Cart $cart): void
    {
        $this->entityManager->persist($cart);
    }

    /**
     * @return Cart[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('c');
        $qb->orderBy('c.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Cart
    {
        /** @var Cart|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Cart with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getActiveByUser(User $user): ?Cart
    {
        $model = $this->fetchOneActiveByUser($user);
        if ($model === null) {
            throw new NotFoundException(sprintf('Cart with status active not found'));
        }

        return $model;
    }

    public function fetchOneActiveByUser(User $user): ?Cart
    {
        /** @var Cart|null $model */
        $model = $this->entityRepository->findOneBy([
            'user' => $user,
            'status' => Status::ACTIVE
        ]);
        return $model;
    }

    protected function getModelClassName(): string
    {
        return Cart::class;
    }
}
