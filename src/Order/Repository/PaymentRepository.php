<?php

declare(strict_types=1);

namespace App\Order\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Order\Model\Payment\Payment;

/**
 * PaymentRepository.
 */
class PaymentRepository extends AbstractRepository
{
    public function add(Payment $payment): void
    {
        $this->entityManager->persist($payment);
    }

    /**
     * @return Payment[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('p');
        $qb->orderBy('p.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Payment
    {
        /** @var Payment|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Payment with id %s not found', (string)$id));
        }

        return $model;
    }

    protected function getModelClassName(): string
    {
        return Payment::class;
    }
}
