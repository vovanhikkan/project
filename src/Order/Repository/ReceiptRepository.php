<?php

declare(strict_types=1);

namespace App\Order\Repository;

use App\Application\Repository\AbstractRepository;
use App\Order\Model\Receipt\Receipt;

/**
 * ReceiptRepository.
 */
class ReceiptRepository extends AbstractRepository
{
    public function add(Receipt $receipt): void
    {
        $this->entityManager->persist($receipt);
    }

    /**
     * @return Receipt[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('r');
        $qb->orderBy('r.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    protected function getModelClassName(): string
    {
        return Receipt::class;
    }
}
