<?php

declare(strict_types=1);

namespace App\Order\Exception\Payment;

use Exception;

/**
 * PaymentProviderNotFoundException.
 */
class ProviderNotSupportedException extends Exception
{
}
