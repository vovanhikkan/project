<?php

declare(strict_types=1);

namespace App\Order\Model\Order;

use App\Application\ValueObject\EnumValueObject;

/**
 * Status.
 *
 * @method static new()
 * @method static payed()
 */
final class Status extends EnumValueObject
{
    const NEW = 100;
    const PAYED = 200;

    public function isNew(): bool
    {
        return $this->value === self::NEW;
    }

    public function isPayed(): bool
    {
        return $this->value === self::PAYED;
    }
}
