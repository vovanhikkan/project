<?php

declare(strict_types=1);

namespace App\Order\Model\Order;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\MiddleName;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\Product;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemProduct.
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_item_product")
 */
class OrderItemProduct
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderItems")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Product\Model\Product\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private Product $product;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Amount $price;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT(5) UNSIGNED NOT NULL")
     */
    private Quantity $quantity;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Amount $amount;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $activationDate = null;

    /**
     * @ORM\Column(type="last_name", nullable=true)
     */
    private ?LastName $lastName = null;

    /**
     * @ORM\Column(type="first_name", nullable=true)
     */
    private ?FirstName $firstName = null;

    /**
     * @ORM\Column(type="middle_name", nullable=true)
     */
    private ?MiddleName $middleName = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $birthDate = null;

    /**
     * @ORM\Column(type="card_num", nullable=true)
     */
    private ?CardNum $cardNum = null;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED DEFAULT NULL")
     */
    private ?Amount $cardPrice = null;

    /**
     * @ORM\Column(type="id", nullable=true)
     */
    private ?Id $clientId = null;

    /**
     * @ORM\Column(type="id", nullable=true)
     */
    private ?Id $cashItemId = null;

    public function __construct(
        Uuid $id,
        Order $order,
        Product $product,
        Amount $price,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null,
        ?LastName $lastName = null,
        ?FirstName $firstName = null,
        ?MiddleName $middleName = null,
        ?DateTimeImmutable $birthDate = null,
        ?CardNum $cardNum = null
    ) {
        $this->id = $id;
        $this->order = $order;
        $this->product = $product;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->birthDate = $birthDate;
        $this->cardNum = $cardNum;
        $this->amount = new Amount($this->price->getValue() * $this->quantity->getValue());
    }

    public function setActivationDate(?DateTimeImmutable $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function changeClientId(Id $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function changeCashItemId(Id $cashItemId): void
    {
        $this->cashItemId = $cashItemId;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getPrice(): Amount
    {
        return $this->price;
    }

    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getActivationDate(): ?DateTimeImmutable
    {
        return $this->activationDate;
    }

    public function getLastName(): ?LastName
    {
        return $this->lastName;
    }

    public function getFirstName(): ?FirstName
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?MiddleName
    {
        return $this->middleName;
    }

    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    public function getCardNum(): ?CardNum
    {
        return $this->cardNum;
    }

    public function getClientId(): ?Id
    {
        return $this->clientId;
    }

    public function getCashItemId(): ?Id
    {
        return $this->cashItemId;
    }

    /**
     * @return Amount|null
     */
    public function getCardPrice(): ?Amount
    {
        return $this->cardPrice;
    }

    /**
     * @param Amount|null $cardPrice
     */
    public function setCardPrice(?Amount $cardPrice): void
    {
        $this->cardPrice = $cardPrice;
    }
}
