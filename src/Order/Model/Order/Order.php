<?php

declare(strict_types=1);

namespace App\Order\Model\Order;

use App\Application\Exception\InvalidStatusTransitionException;
use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Barcode;
use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\MiddleName;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleProduct\BundleProduct;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\Room\Room;
use App\Hotel\Model\Guest\Guest;
use App\Product\Model\CardType\CardType;
use App\Product\Model\Product\Product;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order.
 *
 * @ORM\Entity()
 * @ORM\Table(name="`order`")
 */
class Order
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Auth\Model\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Amount $amount;

    /**
     * @ORM\Column(type="id", nullable=true)
     */
    private ?Id $cashListId = null;

    /**
     * @ORM\Column(type="bar_code", nullable=true)
     */
    private ?Barcode $barcode = null;

    /**
     * @ORM\Column(type="order_order_status", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Status $status;

    /**
     * @ORM\Column(type="id")
     */
    private ?Id $enumNumber = null;

    /**
     * @ORM\OneToMany(targetEntity="OrderItemProduct", mappedBy="order", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $orderItemsProduct;

    /**
     * @ORM\OneToMany(targetEntity="OrderItemRoom", mappedBy="order", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $orderItemsRoom;

    /**
     * @ORM\OneToMany(targetEntity="OrderItemRoomGuest", mappedBy="order", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $orderItemsRoomGuest;

    /**
     * @ORM\OneToMany(targetEntity="OrderItemBundle", mappedBy="order", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $orderItemsBundle;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Order\Model\Payment\Payment",
     *      mappedBy="order",
     *      cascade={"all"},
     *      orphanRemoval=true
     *     )
     */
    private Collection $payments;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\Guest\Guest")
     * @ORM\JoinTable(name="order_guest",
     *  joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="guest_id", referencedColumnName="id")}
     * )
     */
    private Collection $guests;

    /**
     * @ORM\Column(type="order_order_check_in_time")
     */
    private ?CheckInTime $checkInTime = null;

    /**
     * @ORM\Column(type="cart_order_requirements")
     */
    private ?Requirements $requirements = null;

    public function __construct(Uuid $id, User $user)
    {
        $this->id = $id;
        $this->user = $user;
        $this->amount = new Amount(0);
        $this->status = Status::new();
        $this->orderItemsProduct = new ArrayCollection();
        $this->orderItemsRoom = new ArrayCollection();
        $this->orderItemsRoomGuest = new ArrayCollection();
        $this->orderItemsBundle = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
        $this->guests = new ArrayCollection();
    }

    public function addOrderItemProduct(
        Uuid $id,
        Product $product,
        Amount $price,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null,
        ?LastName $lastName = null,
        ?FirstName $firstName = null,
        ?MiddleName $middleName = null,
        ?DateTimeImmutable $birthDate = null,
        ?CardNum $cardNum = null,
        ?CardType $cardType = null
    ): OrderItemProduct {
        $orderItem = new OrderItemProduct(
            $id,
            $this,
            $product,
            $price,
            $quantity,
            $activationDate,
            $lastName,
            $firstName,
            $middleName,
            $birthDate,
            $cardNum
        );
        $this->orderItemsProduct->add($orderItem);

        $this->amount = new Amount(
            $this->amount->getValue() +
            $orderItem->getAmount()->getValue() +
            ($cardType !== null ? $cardType->getPrice()->getValue() : 0)
        );

        $orderItem->setCardPrice(new Amount($cardType !== null ? $cardType->getPrice()->getValue() : 0));

        return $orderItem;
    }

    public function addOrderItemRoom(
        Uuid $id,
        Room $room,
        BedType $bedType,
        RatePlan $ratePlan,
        Foodtype $foodType,
        Amount $price,
        Quantity $quantity,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateTo,
        Quantity $adultCount,
        Quantity $childCount,
        ?DateTimeImmutable $activationDate = null
    ): OrderItemRoom {

        $canBeCanceledDate = $ratePlan->getCanBeCanceledDate();

        $orderItem = new OrderItemRoom(
            $id,
            $this,
            $room,
            $bedType,
            $ratePlan,
            $foodType,
            $price,
            $quantity,
            $dateFrom,
            $dateTo,
            $canBeCanceledDate,
            $adultCount,
            $childCount,
            $activationDate
        );

        $this->amount = new Amount($this->amount->getValue() +
            + $orderItem->getAmount()->getValue());

        $this->orderItemsRoom->add($orderItem);

        return $orderItem;
    }

    public function addOrderItemRoomGuest(
        Uuid $id,
        Room $room,
        Guest $guest,
        ?Requirements $requirements
    ): OrderItemRoomGuest {

        $orderItem = new OrderItemRoomGuest(
            $id,
            $this,
            $room,
            $guest
        );

        if ($requirements) {
            $this->update($requirements);
        }

        $this->orderItemsRoomGuest->add($orderItem);

        return $orderItem;
    }

    public function addOrderItemBundle(
        Uuid $id,
        BundleProduct $bundleProduct,
        Amount $price,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ): OrderItemBundle {
        $orderItem = new OrderItemBundle(
            $id,
            $this,
            $bundleProduct,
            $price,
            $quantity,
            $activationDate
        );

        $this->amount = new Amount($this->amount->getValue() +
            + $orderItem->getAmount()->getValue());

        $this->orderItemsBundle->add($orderItem);

        return $orderItem;
    }

    public function changeCashListId(Id $cashListId): void
    {
        $this->cashListId = $cashListId;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function changeBarcode(Barcode $barcode): void
    {
        $this->barcode = $barcode;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function pay(): void
    {
        if (!$this->status->isNew()) {
            throw new InvalidStatusTransitionException($this->status->getName(), Status::payed()->getName());
        }

        $this->status = Status::payed();
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getCashListId(): ?Id
    {
        return $this->cashListId;
    }

    public function getBarcode(): ?Barcode
    {
        return $this->barcode;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return OrderItemProduct[]
     */
    public function getOrderItemsProduct(): array
    {
        return $this->orderItemsProduct->toArray();
    }

    /**
     * @return OrderItemRoom[]
     */
    public function getOrderItemsRoom(): array
    {
        return $this->orderItemsRoom->toArray();
    }

    /**
     * @return array
     */
    public function getOrderItemsRoomGuest() : array
    {
        return $this->orderItemsRoomGuest->toArray();
    }

    /**
     * @return OrderItemBundle[]
     */
    public function getOrderItemsBundle(): array
    {
        return $this->orderItemsBundle->toArray();
    }

    /**
     * @return array
     */
    public function getGuests()
    {
        return $this->guests->toArray();
    }

    /**
     * @param Guest $guest
     */
    public function addGuest(Guest $guest) : void
    {
        $this->guests->add($guest);
    }

    /**
     * @return Id|null
     */
    public function getEnumNumber(): ?Id
    {
        return $this->enumNumber;
    }

    /**
     * @param Id|null $enumNumber
     */
    public function setEnumNumber(?Id $enumNumber): void
    {
        $this->enumNumber = $enumNumber;
    }

    /**
     * @return array
     */
    public function getPayments(): array
    {
        return $this->payments->toArray();
    }

    /**
     * @return CheckInTime|null
     */
    public function getCheckInTime(): ?CheckInTime
    {
        return $this->checkInTime;
    }

    /**
     * @param CheckInTime|null $checkInTime
     */
    public function setCheckInTime(?CheckInTime $checkInTime): void
    {
        $this->checkInTime = $checkInTime;
    }

    /**
     * @return Requirements|null
     */
    public function getRequirements(): ?Requirements
    {
        return $this->requirements;
    }

    /**
     * @param Requirements|null $requirements
     */
    public function setRequirements(?Requirements $requirements): void
    {
        $this->requirements = $requirements;
    }

    public function update(Requirements $requirements)
    {
        $this->requirements = $requirements;
    }
}
