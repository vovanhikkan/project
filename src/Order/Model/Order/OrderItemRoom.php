<?php

declare(strict_types=1);

namespace App\Order\Model\Order;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\Room\Room;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemRoom.
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_item_room")
 */
class OrderItemRoom
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderItems")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     */
    private Room $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\BedType\BedType")
     * @ORM\JoinColumn(name="bed_type_id", referencedColumnName="id", nullable=false)
     */
    private BedType $bedType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\RatePlan\RatePlan")
     * @ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id", nullable=false)
     */
    private RatePlan $ratePlan;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\FoodType\FoodType")
     * @ORM\JoinColumn(name="food_type_id", referencedColumnName="id", nullable=false)
     */
    private FoodType $foodType;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $dateFrom;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $dateTo;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Amount $price;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT(5) UNSIGNED NOT NULL")
     */
    private Quantity $quantity;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT UNSIGNED")
     */
    private Quantity $adultCount;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT UNSIGNED")
     */
    private Quantity $childCount;


    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Amount $amount;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $activationDate = null;

    /**
     * @ORM\Column(type="id", nullable=true)
     */
    private ?Id $clientId = null;

    /**
     * @ORM\Column(type="id", nullable=true)
     */
    private ?Id $cashItemId = null;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $canBeCanceledDate;

    public function __construct(
        Uuid $id,
        Order $order,
        Room $room,
        BedType $bedType,
        RatePlan $ratePlan,
        FoodType $foodType,
        Amount $price,
        Quantity $quantity,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateTo,
        DateTimeImmutable $canBeCanceledDate,
        Quantity $adultCount,
        Quantity $childCount,
        ?DateTimeImmutable $activationDate = null
    ) {
        $this->id = $id;
        $this->order = $order;
        $this->room = $room;
        $this->bedType = $bedType;
        $this->ratePlan = $ratePlan;
        $this->foodType = $foodType;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->adultCount = $adultCount;
        $this->childCount = $childCount;
        $this->activationDate = $activationDate;
        $this->amount = new Amount($this->price->getValue() * $this->quantity->getValue());
        $this->canBeCanceledDate = $canBeCanceledDate;
    }

    public function setActivationDate(?DateTimeImmutable $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function changeClientId(Id $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function changeCashItemId(Id $cashItemId): void
    {
        $this->cashItemId = $cashItemId;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return BedType
     */
    public function getBedType(): BedType
    {
        return $this->bedType;
    }

    /**
     * @return RatePlan
     */
    public function getRatePlan(): RatePlan
    {
        return $this->ratePlan;
    }

    /**
     * @return FoodType
     */
    public function getFoodType(): FoodType
    {
        return $this->foodType;
    }

    /**
     * @return Quantity
     */
    public function getChildCount(): Quantity
    {
        return $this->childCount;
    }

    /**
     * @return Quantity
     */
    public function getAdultCount(): Quantity
    {
        return $this->adultCount;
    }

    public function getPrice(): Amount
    {
        return $this->price;
    }

    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getActivationDate(): ?DateTimeImmutable
    {
        return $this->activationDate;
    }

    public function getClientId(): ?Id
    {
        return $this->clientId;
    }

    public function getCashItemId(): ?Id
    {
        return $this->cashItemId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCanBeCanceledDate(): DateTimeImmutable
    {
        return $this->canBeCanceledDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateFrom(): DateTimeImmutable
    {
        return $this->dateFrom;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateTo(): DateTimeImmutable
    {
        return $this->dateTo;
    }
}
