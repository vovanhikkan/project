<?php

declare(strict_types=1);

namespace App\Order\Model\Order;


use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Model\Room\Room;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemRoomGuest.
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_item_room_guest")
 */
class OrderItemRoomGuest
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderItems")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     */
    private Room $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Guest\Guest")
     * @ORM\JoinColumn(name="guest_id", referencedColumnName="id", nullable=false)
     */
    private Guest $guest;


    public function __construct(
        Uuid $id,
        Order $order,
        Room $room,
        Guest $guest
    ) {
        $this->id = $id;
        $this->order = $order;
        $this->room = $room;
        $this->guest = $guest;
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return Guest
     */
    public function getGuest(): Guest
    {
        return $this->guest;
    }

}
