<?php

declare(strict_types=1);

namespace App\Order\Model\Order;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleProduct\BundleProduct;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemBundle.
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_item_bundle")
 */
class OrderItemBundle
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderItems")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Bundle\Model\BundleProduct\BundleProduct")
     * @ORM\JoinColumn(name="bundle_product_id", referencedColumnName="id", nullable=false)
     */
    private BundleProduct $bundleProduct;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Amount $price;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT(5) UNSIGNED NOT NULL")
     */
    private Quantity $quantity;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Amount $amount;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $activationDate = null;

    /**
     * @ORM\Column(type="id", nullable=true)
     */
    private ?Id $clientId = null;

    /**
     * @ORM\Column(type="id", nullable=true)
     */
    private ?Id $cashItemId = null;

    public function __construct(
        Uuid $id,
        Order $order,
        BundleProduct $bundleProduct,
        Amount $price,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ) {
        $this->id = $id;
        $this->order = $order;
        $this->bundleProduct = $bundleProduct;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
        $this->amount = new Amount($this->price->getValue() * $this->quantity->getValue());
    }

    public function setActivationDate(?DateTimeImmutable $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function changeClientId(Id $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function changeCashItemId(Id $cashItemId): void
    {
        $this->cashItemId = $cashItemId;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return BundleProduct
     */
    public function getBundleProduct(): BundleProduct
    {
        return $this->bundleProduct;
    }

    public function getPrice(): Amount
    {
        return $this->price;
    }

    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getActivationDate(): ?DateTimeImmutable
    {
        return $this->activationDate;
    }

    public function getClientId(): ?Id
    {
        return $this->clientId;
    }

    public function getCashItemId(): ?Id
    {
        return $this->cashItemId;
    }
}
