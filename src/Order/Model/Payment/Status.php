<?php

declare(strict_types=1);

namespace App\Order\Model\Payment;

use App\Application\ValueObject\EnumValueObject;

/**
 * Status.
 *
 * @method static new()
 * @method static confirmed()
 * @method static canceled()
 */
final class Status extends EnumValueObject
{
    const NEW = 100;
    const CONFIRMED = 200;
    const CANCELED = 300;

    public function isNew(): bool
    {
        return $this->value === self::NEW;
    }

    public function isConfirmed(): bool
    {
        return $this->value === self::CONFIRMED;
    }
}
