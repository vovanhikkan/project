<?php

declare(strict_types=1);

namespace App\Order\Model\Payment;

use App\Application\Exception\InvalidStatusTransitionException;
use App\Application\ValueObject\Url;
use App\Application\ValueObject\Uuid;
use App\Order\Model\Order\Order;
use App\Order\Model\PaySystem\PaySystem;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Payment.
 *
 * @ORM\Entity()
 * @ORM\Table(name="payment")
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Order\Model\Order\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Order\Model\PaySystem\PaySystem")
     * @ORM\JoinColumn(name="pay_system_id", referencedColumnName="id", nullable=false)
     */
    private PaySystem $paySystem;

    /**
     * @ORM\Column(type="url")
     */
    private Url $successUrl;

    /**
     * @ORM\Column(type="string")
     */
    private string $externalId;

    /**
     * @ORM\Column(type="string")
     */
    private string $redirectUrl;

    /**
     * @ORM\Column(type="order_payment_status", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Status $status;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Order\Model\Receipt\Receipt",
     *      mappedBy="payment",
     *      cascade={"all"},
     *      orphanRemoval=true
     *     )
     */
    private Collection $receipts;

    public function __construct(Uuid $id, Order $order, PaySystem $paySystem, Url $successUrl)
    {
        $this->id = $id;
        $this->order = $order;
        $this->paySystem = $paySystem;
        $this->successUrl = $successUrl;
        $this->status = Status::new();
        $this->createdAt = new DateTimeImmutable();
    }

    public function setProviderData(string $externalId, string $redirectUrl)
    {
        $this->externalId = $externalId;
        $this->redirectUrl = $redirectUrl;
    }

    public function confirm(): void
    {
        if (!$this->status->isNew()) {
            throw new InvalidStatusTransitionException($this->status->getName(), Status::confirmed()->getName());
        }

        $this->status = Status::confirmed();
        $this->order->pay();
    }

    public function cancel(): void
    {
        if (!$this->status->isNew()) {
            throw new InvalidStatusTransitionException($this->status->getName(), Status::confirmed()->getName());
        }

        $this->status = Status::canceled();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getPaySystem(): PaySystem
    {
        return $this->paySystem;
    }

    public function getSuccessUrl(): Url
    {
        return $this->successUrl;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function getReceipts(): array
    {
        return $this->receipts->toArray();
    }
}
