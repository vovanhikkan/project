<?php

declare(strict_types=1);

namespace App\Order\Model\PaySystem;

use App\Application\ValueObject\EnumValueObject;

/**
 * Provider.
 *
 * @method static alfa_bank()
 */
final class Provider extends EnumValueObject
{
    const ALFA_BANK = 100;

    public function isAlfaBank(): bool
    {
        return $this->value === self::ALFA_BANK;
    }
}
