<?php

declare(strict_types=1);

namespace App\Order\Model\PaySystem;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * PaySystem.
 *
 * @ORM\Entity()
 * @ORM\Table(name="pay_system")
 */
class PaySystem
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="order_pay_system_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="order_pay_system_provider", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Provider $provider;

    /**
     * @ORM\Column(type="order_pay_system_description", nullable=true)
     */
    private ?Description $description = null;

    /**
     * @ORM\Column(type="order_pay_system_code", nullable=true)
     */
    private ?Code $code = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
     */
    private ?File $image = null;

    public function __construct(
        Uuid $id,
        Name $name,
        Provider $provider,
        ?Description $description,
        ?Code $code,
        ?File $image
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->provider = $provider;
        $this->description = $description;
        $this->code = $code;
        $this->image = $image;
        $this->createdAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getProvider(): Provider
    {
        return $this->provider;
    }

    public function getDescription(): ?Description
    {
        return $this->description;
    }

    public function getCode(): ?Code
    {
        return $this->code;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }
}
