<?php

declare(strict_types=1);

namespace App\Order\Model\PaySystem;

use App\Application\ValueObject\StringValueObject;

/**
 * Code.
 */
final class Code extends StringValueObject
{
}
