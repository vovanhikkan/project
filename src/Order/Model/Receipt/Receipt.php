<?php

declare(strict_types=1);

namespace App\Order\Model\Receipt;

use App\Application\ValueObject\Uuid;
use App\Order\Model\Payment\Payment;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Receipt.
 *
 * @ORM\Entity()
 * @ORM\Table(name="receipt")
 */
class Receipt
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Order\Model\Payment\Payment")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id", nullable=false)
     */
    private Payment $payment;

    /**
     * @ORM\Column(type="order_receipt_provider", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Provider $provider;

    /**
     * @ORM\Column(type="string")
     */
    private string $externalId;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $externalUrl = null;

    /**
     * @ORM\Column(type="order_receipt_status", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Status $status;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    public function __construct(Uuid $id, Payment $payment, Provider $provider)
    {
        $this->id = $id;
        $this->payment = $payment;
        $this->provider = $provider;
        $this->status = Status::new();
        $this->createdAt = new DateTimeImmutable();
    }

    public function setProviderData(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getProvider(): Provider
    {
        return $this->provider;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function getExternalUrl(): ?string
    {
        return $this->externalUrl;
    }

    public function setExternalUrl($externalUrl): void
    {
        $this->externalUrl = $externalUrl;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}
