<?php

declare(strict_types=1);

namespace App\Order\Model\Receipt;

use App\Application\ValueObject\EnumValueObject;

/**
 * Provider.
 *
 * @method static atol()
 */
final class Provider extends EnumValueObject
{
    const ATOL = 100;

    public function isAtol(): bool
    {
        return $this->value === self::ATOL;
    }
}
