<?php

declare(strict_types=1);

namespace App\Order\Model\Receipt;

use App\Application\ValueObject\EnumValueObject;

/**
 * Status.
 *
 * @method static new()
 * @method static success()
 * @method static failed()
 */
final class Status extends EnumValueObject
{
    const NEW = 100;
    const SUCCESS = 200;
    const FAILED = 300;

    public function isNew(): bool
    {
        return $this->value === self::NEW;
    }
}
