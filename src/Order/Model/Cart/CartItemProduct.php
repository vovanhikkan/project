<?php

declare(strict_types=1);

namespace App\Order\Model\Cart;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\MiddleName;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\Product;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CartItemProduct.
 *
 * @ORM\Entity()
 * @ORM\Table(name="cart_item_product")
 */
class CartItemProduct
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="cartItems")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", nullable=false)
     */
    private Cart $cart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Product\Model\Product\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private Product $product;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT(5) UNSIGNED NOT NULL")
     */
    private Quantity $quantity;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $activationDate = null;

    /**
     * @ORM\Column(type="last_name", nullable=true)
     */
    private ?LastName $lastName = null;

    /**
     * @ORM\Column(type="first_name", nullable=true)
     */
    private ?FirstName $firstName = null;

    /**
     * @ORM\Column(type="middle_name", nullable=true)
     */
    private ?MiddleName $middleName = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $birthDate = null;

    /**
     * @ORM\Column(type="card_num", nullable=true)
     */
    private ?CardNum $cardNum = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isNewCard = null;

    public function __construct(
        Uuid $id,
        Cart $cart,
        Product $product,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null,
        ?LastName $lastName = null,
        ?FirstName $firstName = null,
        ?MiddleName $middleName = null,
        ?DateTimeImmutable $birthDate = null,
        ?CardNum $cardNum = null
    ) {
        $this->id = $id;
        $this->cart = $cart;
        $this->product = $product;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->birthDate = $birthDate;
        $this->cardNum = $cardNum;
        $this->createdAt = new DateTimeImmutable();
    }

    public function changeQuantity(Quantity $quantity): void
    {
        $this->quantity = $quantity;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function update(
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null,
        ?LastName $lastName = null,
        ?FirstName $firstName = null,
        ?MiddleName $middleName = null,
        ?DateTimeImmutable $birthDate = null,
        ?CardNum $cardNum = null
    ): void {
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->birthDate = $birthDate;
        $this->cardNum = $cardNum;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?DateTimeImmutable
    {
        return $this->activationDate;
    }

    public function getLastName(): ?LastName
    {
        return $this->lastName;
    }

    public function getFirstName(): ?FirstName
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?MiddleName
    {
        return $this->middleName;
    }

    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    public function getCardNum(): ?CardNum
    {
        return $this->cardNum;
    }

    /**
     * @return bool|null
     */
    public function getIsNewCard(): ?bool
    {
        return $this->isNewCard;
    }

    /**
     * @param bool|null $isNewCard
     */
    public function setIsNewCard(?bool $isNewCard): void
    {
        $this->isNewCard = $isNewCard;
    }
}
