<?php

declare(strict_types=1);

namespace App\Order\Model\Cart;

use App\Application\ValueObject\EnumValueObject;

/**
 * Status.
 *
 * @method static active()
 * @method static ordered()
 */
class Status extends EnumValueObject
{
    const ACTIVE = 100;
    const ORDERED = 200;

    public function isActive(): bool
    {
        return $this->value === self::ACTIVE;
    }

    public function isOrdered(): bool
    {
        return $this->value === self::ORDERED;
    }
}
