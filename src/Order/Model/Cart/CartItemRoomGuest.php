<?php

declare(strict_types=1);

namespace App\Order\Model\Cart;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Model\Room\Room;
use App\Order\Model\Order\Requirements;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CartItemRoomGuest.
 *
 * @ORM\Entity()
 * @ORM\Table(name="cart_item_room_guest")
 */
class CartItemRoomGuest
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="cartItems")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", nullable=false)
     */
    private Cart $cart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     */
    private Room $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Guest\Guest")
     * @ORM\JoinColumn(name="guest_id", referencedColumnName="id", nullable=false)
     */
    private Guest $guest;

    /**
     * @ORM\Column(type="cart_order_requirements")
     */
    private ?Requirements $requirements = null;


    public function __construct(
        Uuid $id,
        Cart $cart,
        Room $room,
        Guest $guest
    ) {
        $this->id = $id;
        $this->cart = $cart;
        $this->room = $room;
        $this->guest = $guest;
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }


    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Cart
     */
    public function getCart(): Cart
    {
        return $this->cart;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return Guest
     */
    public function getGuest(): Guest
    {
        return $this->guest;
    }

    /**
     * @return Requirements|null
     */
    public function getRequirements(): ?Requirements
    {
        return $this->requirements;
    }

    /**
     * @param Requirements|null $requirements
     */
    public function setRequirements(?Requirements $requirements): void
    {
        $this->requirements = $requirements;
    }
}
