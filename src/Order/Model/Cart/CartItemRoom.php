<?php

declare(strict_types=1);

namespace App\Order\Model\Cart;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\Room\Room;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CartItemRoom.
 *
 * @ORM\Entity()
 * @ORM\Table(name="cart_item_room")
 */
class CartItemRoom
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="cartItems")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", nullable=false)
     */
    private Cart $cart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     */
    private Room $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\BedType\BedType")
     * @ORM\JoinColumn(name="bed_type_id", referencedColumnName="id", nullable=false)
     */
    private BedType $bedType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\RatePlan\RatePlan")
     * @ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id", nullable=false)
     */
    private RatePlan $ratePlan;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\FoodType\FoodType")
     * @ORM\JoinColumn(name="food_type_id", referencedColumnName="id", nullable=false)
     */
    private FoodType $foodType;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $dateFrom;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $dateTo;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT(5) UNSIGNED NOT NULL")
     */
    private Quantity $quantity;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT UNSIGNED")
     */
    private Quantity $adultCount;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT UNSIGNED")
     */
    private Quantity $childCount;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $activationDate = null;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $canBeCanceledDate;

    public function __construct(
        Uuid $id,
        Cart $cart,
        Room $room,
        BedType $bedType,
        RatePlan $ratePlan,
        FoodType $foodType,
        Quantity $quantity,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateTo,
        DateTimeImmutable $canBeCanceledDate,
        Quantity $adultCount,
        Quantity $childCount,
        ?DateTimeImmutable $activationDate = null
    ) {
        $this->id = $id;
        $this->cart = $cart;
        $this->room = $room;
        $this->bedType = $bedType;
        $this->ratePlan = $ratePlan;
        $this->foodType = $foodType;
        $this->quantity = $quantity;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->canBeCanceledDate = $canBeCanceledDate;
        $this->activationDate = $activationDate;
        $this->createdAt = new DateTimeImmutable();
        $this->adultCount = $adultCount;
        $this->childCount = $childCount;
    }

    public function changeQuantity(Quantity $quantity): void
    {
        $this->quantity = $quantity;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function update(
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ): void {
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return BedType
     */
    public function getBedType(): BedType
    {
        return $this->bedType;
    }

    /**
     * @return RatePlan
     */
    public function getRatePlan(): RatePlan
    {
        return $this->ratePlan;
    }

    /**
     * @return FoodType
     */
    public function getFoodType(): FoodType
    {
        return $this->foodType;
    }

    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?DateTimeImmutable
    {
        return $this->activationDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateFrom(): DateTimeImmutable
    {
        return $this->dateFrom;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateTo(): DateTimeImmutable
    {
        return $this->dateTo;
    }

    /**
     * @return Quantity
     */
    public function getAdultCount(): Quantity
    {
        return $this->adultCount;
    }

    /**
     * @return Quantity
     */
    public function getChildCount(): Quantity
    {
        return $this->childCount;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCanBeCanceledDate(): DateTimeImmutable
    {
        return $this->canBeCanceledDate;
    }
}
