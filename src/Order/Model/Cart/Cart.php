<?php

declare(strict_types=1);

namespace App\Order\Model\Cart;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\MiddleName;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleProduct\BundleProduct;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\Room\Room;
use App\Loyalty\Model\Promocode\Promocode;
use App\Order\Model\Order\Requirements;
use App\Product\Model\Product\CardType;
use App\Product\Model\Product\Product;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use phpDocumentor\Reflection\Types\This;

/**
 * Cart.
 *
 * @ORM\Entity()
 * @ORM\Table(name="cart")
 */
class Cart
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Auth\Model\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @ORM\Column(type="order_cart_status", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Status $status;

    /**
     * @ORM\OneToMany(targetEntity="CartItemProduct", mappedBy="cart", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $cartItemsProduct;

    /**
     * @ORM\OneToMany(targetEntity="CartItemRoom", mappedBy="cart", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $cartItemsRoom;

    /**
     * @ORM\OneToMany(targetEntity="CartItemRoomGuest", mappedBy="cart", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $cartItemsRoomGuest;

    /**
     * @ORM\OneToMany(targetEntity="CartItemBundle", mappedBy="cart", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $cartItemsBundle;

    /**
     * @ORM\ManyToMany(targetEntity="App\Loyalty\Model\Promocode\Promocode", inversedBy="carts")
     */
    private ?Collection $promocodes = null;

    public function __construct(Uuid $id, User $user)
    {
        $this->id = $id;
        $this->user = $user;
        $this->status = Status::active();
        $this->cartItemsProduct = new ArrayCollection();
        $this->cartItemsRoom = new ArrayCollection();
        $this->cartItemsRoomGuest = new ArrayCollection();
        $this->cartItemsBundle = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
    }

    public function addCartItemProduct(
        Uuid $id,
        Product $product,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null,
        ?LastName $lastName = null,
        ?FirstName $firstName = null,
        ?MiddleName $middleName = null,
        ?DateTimeImmutable $birthDate = null,
        ?CardNum $cardNum = null
    ): CartItemProduct {
        $cardTypeValue = $product->getProductSettings()->getCardType()->getValue();

        /** @var CartItemProduct $cartItem */
        foreach ($this->cartItemsProduct as $cartItem) {
            if ($cartItem->getProduct() === $product &&
                (!$product->getProductSettings() || $cardTypeValue === CardType::NONE)) {
                $cartItem->changeQuantity(new Quantity($cartItem->getQuantity()->getValue() + $quantity->getValue()));
                return $cartItem;
            }
        }

        $isNewCard = false;

        if ($cardTypeValue === CardType::PLASTIC && $cardNum === null) {
            $isNewCard = true;
        }
        if ($cardTypeValue === CardType::PLASTIC_FREE) {
            $isNewCard = true;
        }

        $cartItem = new CartItemProduct(
            $id,
            $this,
            $product,
            $quantity,
            $activationDate,
            $lastName,
            $firstName,
            $middleName,
            $birthDate,
            $cardNum
        );

        $cartItem->setIsNewCard($isNewCard);

        $this->cartItemsProduct->add($cartItem);

        return $cartItem;
    }

    public function addCartItemRoom(
        Uuid $id,
        Room $room,
        BedType $bedType,
        RatePlan $ratePlan,
        FoodType $foodType,
        Quantity $quantity,
        DateTimeImmutable $dateFrom,
        DateTimeImmutable $dateTo,
        Quantity $adultCount,
        Quantity $childCount,
        ?DateTimeImmutable $activationDate = null
    ): CartItemRoom {

        /** @var CartItemRoom $cartItem */
        foreach ($this->cartItemsRoom as $cartItem) {
            if ($cartItem->getRoom() === $room) {
                $cartItem->changeQuantity(new Quantity($cartItem->getQuantity()->getValue() + $quantity->getValue()));
                return $cartItem;
            }
        }

        $canBeCanceledDate = $ratePlan->getCanBeCanceledDate();

        $cartItem = new CartItemRoom(
            $id,
            $this,
            $room,
            $bedType,
            $ratePlan,
            $foodType,
            $quantity,
            $dateFrom,
            $dateTo,
            $canBeCanceledDate,
            $adultCount,
            $childCount,
            $activationDate
        );

        $this->cartItemsRoom->add($cartItem);

        return $cartItem;
    }

    public function addCartItemRoomGuest(
        Uuid $id,
        Room $room,
        Guest $guest,
        ?Requirements $requirements
    ): CartItemRoomGuest {

        /** @var CartItemRoomGuest $cartItem */
        foreach ($this->cartItemsRoomGuest as $cartItem) {
            if ($cartItem->getGuest() === $guest) {
                return $cartItem;
            }
        }

        $cartItem = new CartItemRoomGuest(
            $id,
            $this,
            $room,
            $guest
        );

        if ($requirements) {
            $cartItem->setRequirements($requirements);
        }

        $this->cartItemsRoomGuest->add($cartItem);

        return $cartItem;
    }

    public function addCartItemBundle(
        Uuid $id,
        BundleProduct $bundleProduct,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ): CartItemBundle {

        /** @var CartItemBundle $cartItem */
        foreach ($this->cartItemsBundle as $cartItem) {
            if ($cartItem->getBundleProduct() === $bundleProduct) {
                $cartItem->changeQuantity(new Quantity($cartItem->getQuantity()->getValue() + $quantity->getValue()));
                return $cartItem;
            }
        }

        $cartItem = new CartItemBundle(
            $id,
            $this,
            $bundleProduct,
            $quantity,
            $activationDate
        );

        $this->cartItemsBundle->add($cartItem);

        return $cartItem;
    }

    public function removeCartItemProduct(CartItemProduct $cartItem): void
    {
        $this->cartItemsProduct->removeElement($cartItem);
    }

    public function removeCartItemRoom(CartItemRoom $cartItem): void
    {
        $this->cartItemsRoom->removeElement($cartItem);
    }

    public function removeCartItemRoomGuest(CartItemRoomGuest $cartItem): void
    {
        $this->cartItemsRoomGuest->removeElement($cartItem);
    }

    public function removeCartItemBundle(CartItemBundle $cartItem): void
    {
        $this->cartItemsBundle->removeElement($cartItem);
    }

    public function order(): void
    {
        $this->status = Status::ordered();
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return CartItemProduct[]
     */
    public function getCartItemsProduct(): array
    {
        return $this->cartItemsProduct->toArray();
    }

    /**
     * @return CartItemRoom[]
     */
    public function getCartItemsRoom(): array
    {
        return $this->cartItemsRoom->toArray();
    }

    /**
     * @return array
     */
    public function getCartItemsRoomGuest()
    {
        return $this->cartItemsRoomGuest->toArray();
    }

    /**
     * @return CartItemBundle[]
     */
    public function getCartItemsBundle(): array
    {
        return $this->cartItemsBundle->toArray();
    }

    public function addPromocode(Promocode $promocode): self
    {
        foreach ($this->promocodes as $promocodeItem) {
            if ($promocodeItem === $promocode) {
                throw new InvalidArgumentException('Промокод уже был добавлен в корзину');
            }
        }
        $this->promocodes->add($promocode);
        return $this;
    }
}
