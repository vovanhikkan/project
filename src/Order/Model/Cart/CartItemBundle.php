<?php

declare(strict_types=1);

namespace App\Order\Model\Cart;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleProduct\BundleProduct;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CartItemBundle.
 *
 * @ORM\Entity()
 * @ORM\Table(name="cart_item_bundle")
 */
class CartItemBundle
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="cartItems")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", nullable=false)
     */
    private Cart $cart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Bundle\Model\BundleProduct\BundleProduct")
     * @ORM\JoinColumn(name="bundle_product_id", referencedColumnName="id", nullable=false)
     */
    private BundleProduct $bundleProduct;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT(5) UNSIGNED NOT NULL")
     */
    private Quantity $quantity;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $activationDate = null;

    public function __construct(
        Uuid $id,
        Cart $cart,
        BundleProduct $bundleProduct,
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ) {
        $this->id = $id;
        $this->cart = $cart;
        $this->bundleProduct = $bundleProduct;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
        $this->createdAt = new DateTimeImmutable();
    }

    public function changeQuantity(Quantity $quantity): void
    {
        $this->quantity = $quantity;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function update(
        Quantity $quantity,
        ?DateTimeImmutable $activationDate = null
    ): void {
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    /**
     * @return BundleProduct
     */
    public function getBundleProduct(): BundleProduct
    {
        return $this->bundleProduct;
    }

    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?DateTimeImmutable
    {
        return $this->activationDate;
    }
}
