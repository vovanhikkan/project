<?php

declare(strict_types=1);

namespace App\Order\Command\Order\CreateFromCart;

use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Order\Model\Order\Order;
use App\Order\Repository\CartRepository;
use App\Order\Service\Order\Creator;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private CartRepository $cartRepository;
    private Creator $creator;

    public function __construct(
        UserRepository $userRepository,
        CartRepository $cartRepository,
        Creator $creator
    ) {
        $this->userRepository = $userRepository;
        $this->cartRepository = $cartRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): Order
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cart = $this->cartRepository->getActiveByUser($user);

        return $this->creator->createFromCart($cart);
    }
}
