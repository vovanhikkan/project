<?php

declare(strict_types=1);

namespace App\Order\Command\Order\CreateFromCart;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }
}
