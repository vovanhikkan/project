<?php

declare(strict_types=1);

namespace App\Order\Command\Order\CreateQuick;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * ItemProductDto.
 */
class ItemProductDto
{
    /**
     * @Assert\NotBlank(message="Выберите продукт")
     */
    private string $productId;

    /**
     * @Assert\GreaterThan(value="0", message="Количество должно быть больше 0")
     */
    private int $quantity;

    /**
     * @Assert\Date()
     */
    private ?string $activationDate = null;

    private ?string $lastName = null;

    private ?string $firstName = null;

    private ?string $middleName = null;

    /**
     * @Assert\Date()
     */
    private ?string $birthDate = null;

    private ?string $cardNum = null;

    public function __construct(string $productId, int $quantity)
    {
        $this->productId = $productId;
        $this->quantity = $quantity;
    }

    public function setActivationDate(string $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function setMiddleName(string $middleName): void
    {
        $this->middleName = $middleName;
    }

    public function setBirthDate(string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    public function setCardNum(string $cardNum): void
    {
        $this->cardNum = $cardNum;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?string
    {
        return $this->activationDate;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    public function getCardNum(): ?string
    {
        return $this->cardNum;
    }
}
