<?php

declare(strict_types=1);

namespace App\Order\Command\Order\CreateQuick;

use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\Email;
use App\Application\ValueObject\MiddleName;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\FirstName;
use App\Auth\Model\User\LastName;
use App\Auth\Model\User\Role;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\AuthTokenManager;
use App\Auth\Service\User\Creator as UserCreator;
use App\Auth\Service\User\PasswordGenerator;
use App\Auth\Service\User\PasswordManager;
use App\Auth\Service\User\TokenGenerator;
use App\Bundle\Repository\BundleProductRepository;
use App\Bundle\Repository\BundleRepository;
use App\Data\Flusher;
use App\Data\TransactionManager;
use App\Hotel\Model\Guest\Age;
use App\Hotel\Repository\BedTypeRepository;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Repository\GuestRepository;
use App\Hotel\Repository\RatePlanRepository;
use App\Hotel\Repository\RoomRepository;
use App\Hotel\Service\Guest\Creator;
use App\Order\Model\Order\Order;
use App\Order\Model\Order\Requirements;
use App\Order\Service\Cart\Creator as CartCreator;
use App\Order\Service\Cart\Updater as CartUpdater;
use App\Order\Service\Order\Creator as OrderCreator;
use App\Product\Repository\ProductRepository;
use DateInterval;
use DateTimeImmutable;
use InvalidArgumentException;
use Throwable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private UserCreator $userCreator;
    private PasswordGenerator $passwordGenerator;
    private ProductRepository $productRepository;
    private RoomRepository $roomRepository;
    private BedTypeRepository $bedTypeRepository;
    private RatePlanRepository $ratePlanRepository;
    private FoodTypeRepository $foodTypeRepository;
    private BundleProductRepository $bundleProductRepository;
    private GuestRepository $guestRepository;
    private CartCreator $cartCreator;
    private CartUpdater $cartUpdater;
    private OrderCreator $orderCreator;
    private Creator $guestCreator;
    private TokenGenerator $tokenGenerator;
    private PasswordManager $passwordManager;
    private AuthTokenManager $authTokenManager;
    private Flusher $flusher;
    private TransactionManager $transactionManager;

    public function __construct(
        UserRepository $userRepository,
        UserCreator $userCreator,
        PasswordGenerator $passwordGenerator,
        ProductRepository $productRepository,
        RoomRepository $roomRepository,
        BedTypeRepository $bedTypeRepository,
        RatePlanRepository $ratePlanRepository,
        FoodTypeRepository $foodTypeRepository,
        BundleProductRepository $bundleProductRepository,
        GuestRepository $guestRepository,
        CartCreator $cartCreator,
        CartUpdater $cartUpdater,
        OrderCreator $orderCreator,
        Creator $guestCreator,
        TokenGenerator $tokenGenerator,
        PasswordManager $passwordManager,
        AuthTokenManager $authTokenManager,
        Flusher $flusher,
        TransactionManager $transactionManager
    ) {
        $this->userRepository = $userRepository;
        $this->userCreator = $userCreator;
        $this->passwordGenerator = $passwordGenerator;
        $this->productRepository = $productRepository;
        $this->roomRepository = $roomRepository;
        $this->bedTypeRepository = $bedTypeRepository;
        $this->ratePlanRepository = $ratePlanRepository;
        $this->foodTypeRepository = $foodTypeRepository;
        $this->bundleProductRepository = $bundleProductRepository;
        $this->guestRepository = $guestRepository;
        $this->cartCreator = $cartCreator;
        $this->cartUpdater = $cartUpdater;
        $this->orderCreator = $orderCreator;
        $this->tokenGenerator = $tokenGenerator;
        $this->passwordManager = $passwordManager;
        $this->authTokenManager = $authTokenManager;
        $this->flusher = $flusher;
        $this->transactionManager = $transactionManager;
        $this->guestCreator = $guestCreator;
    }

    public function handle(Command $command): array
    {
        $this->transactionManager->beginTransaction();

        try {
            $password = $this->passwordGenerator->generate();
            $user = $this->processUser($command, $password);
            $order = $this->processOrder($command, $user);

            $refreshToken = $this->tokenGenerator->generate(
                new DateTimeImmutable(),
                new DateInterval('P1Y')
            );

            $user->login(
                $password,
                $this->passwordManager,
                $refreshToken
            );

            $data = [
                'alg' => 'RS256',
                'typ' => 'JWT',
                'exp' => time() + 3600 * 24 * 30,
                'id' => (string)$user->getId()
            ];

            $result = [
                'accessToken' => $this->authTokenManager->encode($data),
                'refreshToken' => $refreshToken->getValue(),
            ];

            if (!is_null($order)) {
                $result['id'] = $order->getId()->getValue();
            }

            $this->flusher->flush();
            $this->transactionManager->commit();

            return $result;
        } catch (Throwable $e) {
            $this->transactionManager->rollBack();
            throw $e;
        }
    }

    private function processUser(Command $command, string $password): User
    {
        $email = new Email($command->getEmail());

        if ($this->userRepository->existByEmail($email)) {
            throw new InvalidArgumentException('Пользователь с таким email уже существует, авторизируйтесь');
        }

        return $this->userCreator->create(
            Uuid::generate(),
            Role::customer(),
            $email,
            null,
            $password,
            new LastName($command->getLastName()),
            new FirstName($command->getFirstName()),
            null,
            null,
            null,
            true,
            true
        );
    }

    private function processOrder(Command $command, User $user): Order
    {
        $cart = $this->cartCreator->create($user);

        foreach ($command->getItemsProduct() as $itemDto) {
            $product = $this->productRepository->get(new Uuid($itemDto->getProductId()));
            $this->cartUpdater->addCartItemProduct(
                $cart,
                $product,
                new Quantity($itemDto->getQuantity()),
                $itemDto->getActivationDate() !== null
                    ? new DateTimeImmutable($itemDto->getActivationDate()) : null,
                $itemDto->getLastName() !== null
                    ? new \App\Application\ValueObject\LastName($itemDto->getLastName()) : null,
                $itemDto->getFirstName() !== null
                    ? new \App\Application\ValueObject\FirstName($itemDto->getFirstName()) : null,
                $itemDto->getMiddleName() !== null
                    ? new MiddleName($itemDto->getMiddleName()) : null,
                $itemDto->getBirthDate() !== null
                    ? new DateTimeImmutable($itemDto->getBirthDate()) : null,
                $itemDto->getCardNum() !== null
                    ? new CardNum($itemDto->getCardNum()) : null
            );
        }

        foreach ($command->getItemsRoom() as $itemDto) {
            $room = $this->roomRepository->get(new Uuid($itemDto->getRoomId()));
            $bedType = $this->bedTypeRepository->get(new Uuid($itemDto->getBedTypeId()));
            $ratePlan = $this->ratePlanRepository->get(new Uuid($itemDto->getRatePlanId()));
            $foodType = $this->foodTypeRepository->get(new Uuid($itemDto->getFoodTypeId()));
            $this->cartUpdater->addCartItemRoom(
                $cart,
                $room,
                $bedType,
                $ratePlan,
                $foodType,
                new Quantity($itemDto->getQuantity()),
                new DateTimeImmutable($itemDto->getDateFrom()),
                new DateTimeImmutable($itemDto->getDateTo()),
                new Quantity($itemDto->getAdultCount()),
                new Quantity($itemDto->getChildCount()),
                $itemDto->getActivationDate() !== null
                    ? new DateTimeImmutable($itemDto->getActivationDate()) : null,
            );
        }

        foreach ($command->getItemsRoomGuest() as $itemDto) {
            $room = $this->roomRepository->get(new Uuid($itemDto->getRoomId()));
            $requirements = null;
            if ($itemDto->getRequirements()) {
                $requirements = new Requirements($itemDto->getRequirements());
            }
            $searchGuestResult = $this->guestRepository->getGuest($itemDto);

            if (empty($searchGuestResult)) {
                $guest = $this->guestCreator->create(
                    Uuid::generate(),
                    new \App\Application\ValueObject\FirstName($itemDto->getFirstName()),
                    new \App\Application\ValueObject\LastName($itemDto->getLastName()),
                    new Age($itemDto->getAge()),
                    $itemDto->isMainGuest()
                );
            } else {
                $guest = array_shift($searchGuestResult);
            }

            $this->cartUpdater->addCartItemRoomGuest(
                $cart,
                $room,
                $guest,
                $requirements
            );
        }

        foreach ($command->getItemsBundleProduct() as $itemDto) {
            $bundleProduct = $this->bundleProductRepository->get(new Uuid($itemDto->getBundleProductId()));
            $this->cartUpdater->addCartItemBundle(
                $cart,
                $bundleProduct,
                new Quantity($itemDto->getQuantity()),
                $itemDto->getActivationDate() !== null
                    ? new DateTimeImmutable($itemDto->getActivationDate()) : null,
            );
        }

        return $this->orderCreator->createFromCart($cart);
    }
}
