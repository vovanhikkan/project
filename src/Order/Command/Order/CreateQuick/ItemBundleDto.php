<?php

declare(strict_types=1);

namespace App\Order\Command\Order\CreateQuick;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * ItemProductDto.
 */
class ItemBundleDto
{
    /**
     * @Assert\NotBlank(message="Выберите продукт")
     */
    private string $bundleProductId;

    /**
     * @Assert\GreaterThan(value="0", message="Количество должно быть больше 0")
     */
    private int $quantity;

    /**
     * @Assert\Date()
     */
    private ?string $activationDate = null;

    public function __construct(string $bundleProductId, int $quantity)
    {
        $this->bundleProductId = $bundleProductId;
        $this->quantity = $quantity;
    }

    public function setActivationDate(string $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function getBundleProductId(): string
    {
        return $this->bundleProductId;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?string
    {
        return $this->activationDate;
    }
}
