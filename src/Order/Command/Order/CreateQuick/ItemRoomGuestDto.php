<?php

declare(strict_types=1);

namespace App\Order\Command\Order\CreateQuick;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ItemRoomGuestDto
 * @package App\Order\Command\Order\CreateQuick
 */
class ItemRoomGuestDto
{
    /**
     * @Assert\NotBlank(message="Выберите продукт")
     */
    private string $roomId;

    /**
     * @Assert\NotBlank(message="Ошибка! не указано имя гостя")
     */
    private string $firstName;

    /**
     * @Assert\NotBlank(message="Ошибка! не указана фамилия гостя")
     */
    private string $lastName;

    /**
     * @Assert\NotBlank(message="Ошибка! не указан возраст")
     */
    private int $age;

    private bool $mainGuest;

    private ?array $requirements = null;



    public function __construct(
        string $roomId,
        string $firstName,
        string $lastName,
        int $age

    ) {
        $this->roomId = $roomId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->age = $age;
        $this->mainGuest = false;
    }


    public function getRoomId(): string
    {
        return $this->roomId;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return bool
     */
    public function isMainGuest(): bool
    {
        return $this->mainGuest;
    }

    /**
     * @param bool $mainGuest
     */
    public function setMainGuest(bool $mainGuest): void
    {
        $this->mainGuest = $mainGuest;
    }

    /**
     * @return array|null
     */
    public function getRequirements(): ?array
    {
        return $this->requirements;
    }

    /**
     * @param array|null $requirements
     */
    public function setRequirements(?array $requirements): void
    {
        $this->requirements = $requirements;
    }

}
