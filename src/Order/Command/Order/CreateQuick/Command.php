<?php

declare(strict_types=1);

namespace App\Order\Command\Order\CreateQuick;

use App\Application\Exception\InvalidArgumentException;
use Assert\Assertion;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните e-mail")
     */
    private string $email;

    /**
     * @Assert\NotBlank(message="Заполните фамилию")
     */
    private string $lastName;

    /**
     * @Assert\NotBlank(message="Заполните имя")
     */
    private string $firstName;

    /**
     * @Assert\Valid()
     */
    private array $itemsProduct;

    /**
     * @Assert\Valid()
     */
    private array $itemsRoom;

    /**
     * @Assert\Valid()
     */
    private array $itemsRoomGuest;

    /**
     * @Assert\Valid()
     */
    private array $itemsBundleProduct;

    public function __construct(
        string $email,
        string $lastName,
        string $firstName,
        array $itemsProduct,
        array $itemsRoom,
        array $itemsRoomGuest,
        array $itemsBundleProduct
    ) {
        Assertion::allIsInstanceOf($itemsProduct, ItemProductDto::class);
        Assertion::allIsInstanceOf($itemsRoom, ItemRoomDto::class);
        Assertion::allIsInstanceOf($itemsRoomGuest, ItemRoomGuestDto::class);
        Assertion::allIsInstanceOf($itemsBundleProduct, ItemBundleDto::class);

        if (empty($itemsProduct) && empty($itemsRoom) && empty($itemsBundleProduct)) {
            throw new InvalidArgumentException(sprintf('Quick order no items'));
        }

        $this->email = $email;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->itemsProduct = $itemsProduct;
        $this->itemsRoom = $itemsRoom;
        $this->itemsRoomGuest = $itemsRoomGuest;
        $this->itemsBundleProduct = $itemsBundleProduct;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return ItemProductDto[]
     */
    public function getItemsProduct(): array
    {
        return $this->itemsProduct;
    }

    /**
     * @return ItemRoomDto[]
     */
    public function getItemsRoom(): array
    {
        return $this->itemsRoom;
    }

    /**
     * @return ItemRoomGuestDto[]
     */
    public function getItemsRoomGuest(): array
    {
        return $this->itemsRoomGuest;
    }

    /**
     * @return ItemBundleDto[]
     */
    public function getItemsBundleProduct(): array
    {
        return $this->itemsBundleProduct;
    }
}
