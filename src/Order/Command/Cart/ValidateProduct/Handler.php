<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\ValidateProduct;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;
use App\Order\Service\Cart\Updater;
use App\Product\Repository\ProductRepository;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private ProductRepository $productRepository;

    public function __construct(
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    public function handle(Command $command): array
    {
        foreach ($command->getCartItems() as $cartItem) {
            /**
             * @var CartItemProductDto $cartItem
             */
            $product = $this->productRepository->get(new Uuid($cartItem->getProduct()));
            if (!$product->isActive()) {
                $cartItem->setIsInvalidProduct(true);
            }

            if (!$product->getSection()->isActive()) {
                $cartItem->setIsInvalidSection(true);
            }

            if ($cartItem->getActivationDate() !== null && $cartItem->getActivationDate() !== date('Y-m-d')) {
                $cartItem->setIsInvalidActivationDate(true);
            }

            $cartItem->setProductObject($product);
        }

        return $command->getCartItems();
    }
}
