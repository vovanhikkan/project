<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\ValidateProduct;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $cartItems;

    /**
     * Command constructor.
     * @param array $cartItems
     */
    public function __construct(array $cartItems)
    {
        $this->cartItems = $cartItems;
    }

    /**
     * @return array
     */
    public function getCartItems(): array
    {
        return $this->cartItems;
    }
}
