<?php
declare(strict_types=1);

namespace App\Order\Command\Cart\ValidateProduct;

use App\Product\Model\Product\Product;
use Symfony\Component\Validator\Constraints as Assert;

class CartItemProductDto
{
    /**
     * @Assert\NotBlank()
     */
    private string $product;

    /**
     * @Assert\NotBlank()
     */
    private int $quantity;

    /**
     * @Assert\NotBlank()
     */
    private string $activationDate;

    private bool $isInvalidSection = false;
    private bool $isInvalidProduct = false;
    private bool $isInvalidActivationDate = false;
    private ?Product $productObject = null;

    /**
     * CartItemProductDto constructor.
     * @param string $product
     * @param int $quantity
     * @param string $activationDate
     */
    public function __construct(string $product, int $quantity, string $activationDate)
    {
        $this->product = $product;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getActivationDate(): string
    {
        return $this->activationDate;
    }

    /**
     * @param bool $isInvalidSection
     */
    public function setIsInvalidSection(bool $isInvalidSection): void
    {
        $this->isInvalidSection = $isInvalidSection;
    }

    /**
     * @param bool $isInvalidProduct
     */
    public function setIsInvalidProduct(bool $isInvalidProduct): void
    {
        $this->isInvalidProduct = $isInvalidProduct;
    }

    /**
     * @param bool $isInvalidActivationDate
     */
    public function setIsInvalidActivationDate(bool $isInvalidActivationDate): void
    {
        $this->isInvalidActivationDate = $isInvalidActivationDate;
    }

    /**
     * @return bool
     */
    public function getIsInvalidSection(): bool
    {
        return $this->isInvalidSection;
    }

    /**
     * @return bool
     */
    public function getIsInvalidProduct(): bool
    {
        return $this->isInvalidProduct;
    }

    /**
     * @return bool
     */
    public function getIsInvalidActivationDate(): bool
    {
        return $this->isInvalidActivationDate;
    }

    /**
     * @return Product|null
     */
    public function getProductObject(): ?Product
    {
        return $this->productObject;
    }

    /**
     * @param Product|null $productObject
     */
    public function setProductObject(?Product $productObject): void
    {
        $this->productObject = $productObject;
    }
}
