<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\ChangeQuantityBundle;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;
use App\Order\Service\Cart\Updater;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private CartRepository $cartRepository;
    private Updater $updater;

    public function __construct(
        UserRepository $userRepository,
        CartRepository $cartRepository,
        Updater $updater
    ) {
        $this->userRepository = $userRepository;
        $this->cartRepository = $cartRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): Cart
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cart = $this->cartRepository->getActiveByUser($user);

        $cartItem = null;
        foreach ($cart->getCartItemsBundle() as $_cartItem) {
            if ($_cartItem->getId()->getValue() === $command->getCartItemId()) {
                $cartItem = $_cartItem;
                break;
            }
        }

        if ($cartItem === null) {
            throw new NotFoundException();
        }

        $this->updater->changeCartItemBundleQuantity($cart, $cartItem, new Quantity($command->getQuantity()));

        return $cart;
    }
}
