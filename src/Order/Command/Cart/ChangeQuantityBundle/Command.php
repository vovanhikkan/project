<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\ChangeQuantityBundle;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * @Assert\NotBlank()
     */
    private string $cartItemId;

    /**
     * @Assert\GreaterThan(value="0", message="Количество должно быть больше 0")
     */
    private int $quantity;

    public function __construct(string $userId, string $cartItemId, int $quantity)
    {
        $this->userId = $userId;
        $this->cartItemId = $cartItemId;
        $this->quantity = $quantity;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getCartItemId(): string
    {
        return $this->cartItemId;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
