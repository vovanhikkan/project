<?php
declare(strict_types=1);

namespace App\Order\Command\Cart\ValidateBundle;

use App\Bundle\Model\Bundle\Bundle;
use Symfony\Component\Validator\Constraints as Assert;

class CartItemBundleDto
{
    /**
     * @Assert\NotBlank()
     */
    private string $bundle;

    /**
     * @Assert\NotBlank()
     */
    private int $quantity;

    /**
     * @Assert\NotBlank()
     */
    private string $activationDate;

    private bool $isInvalidActivationDate = false;
    private ?Bundle $bundleObject = null;

    /**
     * CartItemBundleDto constructor.
     * @param string $bundle
     * @param int $quantity
     * @param string $activationDate
     */
    public function __construct(string $bundle, int $quantity, string $activationDate)
    {
        $this->bundle = $bundle;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
    }

    /**
     * @return string
     */
    public function getBundle(): string
    {
        return $this->bundle;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getActivationDate(): string
    {
        return $this->activationDate;
    }

    /**
     * @param bool $isInvalidActivationDate
     */
    public function setIsInvalidActivationDate(bool $isInvalidActivationDate): void
    {
        $this->isInvalidActivationDate = $isInvalidActivationDate;
    }

    /**
     * @return bool
     */
    public function getIsInvalidActivationDate(): bool
    {
        return $this->isInvalidActivationDate;
    }

    /**
     * @return Bundle|null
     */
    public function getBundleObject(): ?Bundle
    {
        return $this->bundleObject;
    }

    /**
     * @param Bundle|null $bundleObject
     */
    public function setBundleObject(?Bundle $bundleObject): void
    {
        $this->bundleObject = $bundleObject;
    }
}
