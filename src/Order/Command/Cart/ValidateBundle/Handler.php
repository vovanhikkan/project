<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\ValidateBundle;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Bundle\Repository\BundleRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;
use App\Order\Service\Cart\Updater;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private BundleRepository $bundleRepository;

    public function __construct(
        BundleRepository $bundleRepository
    ) {
        $this->bundleRepository = $bundleRepository;
    }

    public function handle(Command $command): array
    {
        foreach ($command->getCartItems() as $cartItem) {
            /**
             * @var CartItemBundleDto $cartItem
             */
            $bundle = $this->bundleRepository->get(new Uuid($cartItem->getBundle()));

            if ($cartItem->getActivationDate() !== null && $cartItem->getActivationDate() !== date('Y-m-d')) {
                $cartItem->setIsInvalidActivationDate(true);
            }

            $cartItem->setBundleObject($bundle);
        }

        return $command->getCartItems();
    }
}
