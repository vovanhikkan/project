<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\UpdateItemProduct;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * @Assert\NotBlank()
     */
    private string $cartItemId;

    /**
     * @Assert\GreaterThan(value="0", message="Количество должно быть больше 0")
     */
    private int $quantity;

    /**
     * @Assert\Date()
     */
    private ?string $activationDate = null;

    private ?string $lastName = null;

    private ?string $firstName = null;

    private ?string $middleName = null;

    /**
     * @Assert\Date()
     */
    private ?string $birthDate = null;

    private ?string $cardNum = null;

    public function __construct(string $userId, string $cartItemId, int $quantity)
    {
        $this->userId = $userId;
        $this->cartItemId = $cartItemId;
        $this->quantity = $quantity;
    }

    public function setActivationDate(string $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function setMiddleName(string $middleName): void
    {
        $this->middleName = $middleName;
    }

    public function setBirthDate(string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    public function setCardNum(string $cardNum): void
    {
        $this->cardNum = $cardNum;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getCartItemId(): string
    {
        return $this->cartItemId;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?string
    {
        return $this->activationDate;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    public function getCardNum(): ?string
    {
        return $this->cardNum;
    }
}
