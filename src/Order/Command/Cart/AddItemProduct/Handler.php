<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemProduct;

use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\MiddleName;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Data\TransactionManager;
use App\Order\Model\Cart\Cart;
use App\Order\Service\Cart\Factory;
use App\Order\Service\Cart\Updater;
use App\Product\Repository\ProductRepository;
use DateTimeImmutable;
use Throwable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private Factory $factory;
    private ProductRepository $productRepository;
    private Updater $updater;
    private TransactionManager $transactionManager;

    public function __construct(
        UserRepository $userRepository,
        Factory $factory,
        ProductRepository $productRepository,
        Updater $updater,
        TransactionManager $transactionManager
    ) {
        $this->userRepository = $userRepository;
        $this->factory = $factory;
        $this->productRepository = $productRepository;
        $this->updater = $updater;
        $this->transactionManager = $transactionManager;
    }

    public function handle(Command $command): Cart
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cartItemsProduct = $command->getCartItemsProduct();

        $this->transactionManager->beginTransaction();
        try {
            $cart = $this->factory->getActiveOrCreate($user);

            foreach ($cartItemsProduct as $cartItem) {
                $product = $this->productRepository->get(new Uuid($cartItem->getProductId()));
                $this->updater->addCartItemProduct(
                    $cart,
                    $product,
                    new Quantity($cartItem->getQuantity()),
                    $cartItem->getActivationDate() !== null
                        ? new DateTimeImmutable($cartItem->getActivationDate()) : null,
                    $cartItem->getLastName() !== null ? new LastName($cartItem->getLastName()) : null,
                    $cartItem->getFirstName() !== null ? new FirstName($cartItem->getFirstName()) : null,
                    $cartItem->getMiddleName() !== null ? new MiddleName($cartItem->getMiddleName()) : null,
                    $cartItem->getBirthDate() !== null ? new DateTimeImmutable($cartItem->getBirthDate()) : null,
                    $cartItem->getCardNum() !== null ? new CardNum($cartItem->getCardNum()) : null,
                );
            }

            $this->transactionManager->commit();
        } catch (Throwable $e) {
            $this->transactionManager->rollBack();
            throw $e;
        }

        return $cart;
    }
}
