<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemProduct;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $cartItemsProduct;

    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * Command constructor.
     * @param array $cartItemsProduct
     * @param string $userId
     */
    public function __construct(array $cartItemsProduct, string $userId)
    {
        $this->cartItemsProduct = $cartItemsProduct;
        $this->userId = $userId;
    }

    /**
     * @return CartItemProductDto[]
     */
    public function getCartItemsProduct(): array
    {
        return $this->cartItemsProduct;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

}
