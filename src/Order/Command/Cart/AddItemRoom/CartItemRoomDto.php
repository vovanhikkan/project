<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemRoom;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * CartItemRoomDto.
 */
class CartItemRoomDto
{
    /**
     * @Assert\NotBlank(message="Ошибка! не указана комната")
     */
    private string $roomId;

    /**
     * @Assert\NotBlank(message="Ошибка! не указан тип спального мест")
     */
    private string $bedTypeId;

    /**
     * @Assert\NotBlank(message="Ошибка! не указан тариф")
     */
    private string $ratePlanId;

    /**
     * @Assert\NotBlank(message="Ошибка! не указан тип питания")
     */
    private string $foodTypeId;

    /**
     * @Assert\NotBlank(message="Ошибка! не указана дата заезда")
     */
    private string $dateFrom;

    /**
     * @Assert\NotBlank(message="Ошибка! не указана дата выезда")
     */
    private string $dateTo;

    /**
     * @Assert\NotBlank(message="Ошибка! не указано количество номеров")
     */
    private int $quantity;

    /**
     * @Assert\NotBlank(message="Ошибка! не указано количество взрослых гостей")
     */
    private int $adultCount;

    /**
     * @Assert\NotBlank(message="Ошибка! нет параметра количество детей")
     */
    private int $childCount;

    /**
     * @Assert\Date()
     */
    private ?string $activationDate = null;

    public function __construct(
        string $roomId,
        string $bedTypeId,
        string $ratePlanId,
        string $foodTypeId,
        int $quantity,
        string $dateFrom,
        string $dateTo,
        int $adultCount,
        int $childCount
    ) {
        $this->roomId = $roomId;
        $this->bedTypeId = $bedTypeId;
        $this->ratePlanId = $ratePlanId;
        $this->foodTypeId = $foodTypeId;
        $this->quantity = $quantity;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->adultCount = $adultCount;
        $this->childCount = $childCount;
    }

    public function setActivationDate(string $activationDate): void
    {
        $this->activationDate = $activationDate;
    }

    public function getRoomId(): string
    {
        return $this->roomId;
    }

    /**
     * @return string
     */
    public function getBedTypeId(): string
    {
        return $this->bedTypeId;
    }

    /**
     * @return string
     */
    public function getRatePlanId(): string
    {
        return $this->ratePlanId;
    }

    /**
     * @return string
     */
    public function getFoodTypeId(): string
    {
        return $this->foodTypeId;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?string
    {
        return $this->activationDate;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @return string
     */
    public function getDateTo(): string
    {
        return $this->dateTo;
    }

    /**
     * @return int
     */
    public function getAdultCount(): int
    {
        return $this->adultCount;
    }

    /**
     * @return int
     */
    public function getChildCount(): int
    {
        return $this->childCount;
    }
}
