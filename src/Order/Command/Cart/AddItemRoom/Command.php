<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemRoom;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $cartItemsRoom;

    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * Command constructor.
     * @param array $cartItemsRoom
     * @param string $userId
     */
    public function __construct(array $cartItemsRoom, string $userId)
    {
        $this->cartItemsRoom = $cartItemsRoom;
        $this->userId = $userId;
    }

    /**
     * @return CartItemRoomDto[]
     */
    public function getCartItemsRoom(): array
    {
        return $this->cartItemsRoom;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

}
