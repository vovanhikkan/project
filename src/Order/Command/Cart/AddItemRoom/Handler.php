<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemRoom;

use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Data\TransactionManager;
use App\Hotel\Repository\BedTypeRepository;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Repository\RatePlanRepository;
use App\Hotel\Repository\RoomRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Service\Cart\Factory;
use App\Order\Service\Cart\Updater;
use DateTimeImmutable;
use Throwable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private Factory $factory;
    private RoomRepository $roomRepository;
    private BedTypeRepository $bedTypeRepository;
    private RatePlanRepository $ratePlanRepository;
    private FoodTypeRepository $foodTypeRepository;
    private Updater $updater;
    private TransactionManager $transactionManager;

    public function __construct(
        UserRepository $userRepository,
        Factory $factory,
        RoomRepository $roomRepository,
        BedTypeRepository $bedTypeRepository,
        RatePlanRepository $ratePlanRepository,
        FoodTypeRepository $foodTypeRepository,
        Updater $updater,
        TransactionManager $transactionManager
    ) {
        $this->userRepository = $userRepository;
        $this->factory = $factory;
        $this->roomRepository = $roomRepository;
        $this->bedTypeRepository = $bedTypeRepository;
        $this->ratePlanRepository = $ratePlanRepository;
        $this->foodTypeRepository = $foodTypeRepository;
        $this->updater = $updater;
        $this->transactionManager = $transactionManager;
    }

    public function handle(Command $command): Cart
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cartItemsRoom = $command->getCartItemsRoom();

        $this->transactionManager->beginTransaction();
        try {
            $cart = $this->factory->getActiveOrCreate($user);

            foreach ($cartItemsRoom as $cartItem) {
                $room = $this->roomRepository->get(new Uuid($cartItem->getRoomId()));
                $bedType = $this->bedTypeRepository->get(new Uuid($cartItem->getBedTypeId()));
                $ratePlan = $this->ratePlanRepository->get(new Uuid($cartItem->getRatePlanId()));
                $foodType = $this->foodTypeRepository->get(new Uuid($cartItem->getFoodTypeId()));
                $this->updater->addCartItemRoom(
                    $cart,
                    $room,
                    $bedType,
                    $ratePlan,
                    $foodType,
                    new Quantity($cartItem->getQuantity()),
                    new DateTimeImmutable($cartItem->getDateFrom()),
                    new DateTimeImmutable($cartItem->getDateTo()),
                    new Quantity($cartItem->getAdultCount()),
                    new Quantity($cartItem->getChildCount()),
                    $cartItem->getActivationDate() !== null
                        ? new DateTimeImmutable($cartItem->getActivationDate()) : null,
                );
            }

            $this->transactionManager->commit();
        } catch (Throwable $e) {
            $this->transactionManager->rollBack();
            throw $e;
        }

        return $cart;
    }
}
