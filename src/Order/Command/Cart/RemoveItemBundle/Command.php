<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\RemoveItemBundle;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * @Assert\NotBlank()
     */
    private array $ids;

    /**
     * Command constructor.
     * @param string $userId
     * @param array $ids
     */
    public function __construct(string $userId, array $ids)
    {
        $this->userId = $userId;
        $this->ids = $ids;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return array
     */
    public function getIds(): array
    {
        return $this->ids;
    }
}
