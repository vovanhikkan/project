<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\RemoveItemProduct;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;
use App\Order\Service\Cart\Updater;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private CartRepository $cartRepository;
    private Updater $updater;

    public function __construct(
        UserRepository $userRepository,
        CartRepository $cartRepository,
        Updater $updater
    ) {
        $this->userRepository = $userRepository;
        $this->cartRepository = $cartRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): Cart
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cart = $this->cartRepository->getActiveByUser($user);
        $cartItems = [];

        foreach ($cart->getCartItemsProduct() as $_cartItem) {
            if (in_array($_cartItem->getId()->getValue(), $command->getIds())) {
                $cartItems[] = $_cartItem;
            }
        }

        if (!count($cartItems)) {
            throw new NotFoundException();
        }

        $this->updater->removeCartItemProduct($cart, $cartItems);

        return $cart;
    }
}
