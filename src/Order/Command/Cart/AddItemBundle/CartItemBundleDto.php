<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemBundle;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * CartItemBundleDto.
 */
class CartItemBundleDto
{
    /**
     * @Assert\NotBlank()
     */
    private string $bundleProductId;

    /**
     * @Assert\NotBlank()
     */
    private int $quantity;

    /**
     * @Assert\Date()
     */
    private string $activationDate;

    public function __construct(
        string $bundleProductId,
        int $quantity,
        string $activationDate
    ) {
        $this->bundleProductId = $bundleProductId;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
    }

    public function getBundleProductId(): string
    {
        return $this->bundleProductId;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getActivationDate(): ?string
    {
        return $this->activationDate;
    }
}
