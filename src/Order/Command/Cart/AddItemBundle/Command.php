<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemBundle;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $cartItemsBundle;

    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * Command constructor.
     * @param array $cartItemsBundle
     * @param string $userId
     */
    public function __construct(array $cartItemsBundle, string $userId)
    {
        $this->cartItemsBundle = $cartItemsBundle;
        $this->userId = $userId;
    }

    /**
     * @return CartItemBundleDto[]
     */
    public function getCartItemsBundle(): array
    {
        return $this->cartItemsBundle;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

}
