<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemBundle;

use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Bundle\Repository\BundleProductRepository;
use App\Data\TransactionManager;
use App\Bundle\Repository\BundleRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Service\Cart\Factory;
use App\Order\Service\Cart\Updater;
use DateTimeImmutable;
use Throwable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private Factory $factory;
    private BundleProductRepository $bundleProductRepository;
    private Updater $updater;
    private TransactionManager $transactionManager;

    public function __construct(
        UserRepository $userRepository,
        Factory $factory,
        BundleProductRepository $bundleProductRepository,
        Updater $updater,
        TransactionManager $transactionManager
    ) {
        $this->userRepository = $userRepository;
        $this->factory = $factory;
        $this->bundleProductRepository = $bundleProductRepository;
        $this->updater = $updater;
        $this->transactionManager = $transactionManager;
    }

    public function handle(Command $command): Cart
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cartItemsBundle = $command->getCartItemsBundle();

        $this->transactionManager->beginTransaction();
        try {
            $cart = $this->factory->getActiveOrCreate($user);

            foreach ($cartItemsBundle as $cartItem) {
                $bundleProduct = $this->bundleProductRepository->get(new Uuid($cartItem->getBundleProductId()));
                $this->updater->addCartItemBundle(
                    $cart,
                    $bundleProduct,
                    new Quantity($cartItem->getQuantity()),
                    new DateTimeImmutable($cartItem->getActivationDate())
                );
            }

            $this->transactionManager->commit();
        } catch (Throwable $e) {
            $this->transactionManager->rollBack();
            throw $e;
        }

        return $cart;
    }
}
