<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemBundleRoom;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $cartItemsBundleRoom;

    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * Command constructor.
     * @param array $cartItemsRoom
     * @param string $userId
     */
    public function __construct(array $cartItemsBundleRoom, string $userId)
    {
        $this->cartItemsBundleRoom = $cartItemsBundleRoom;
        $this->userId = $userId;
    }

    /**
     * @return CartItemBundleRoomDto[]
     */
    public function getCartItemsBundleRoom(): array
    {
        return $this->cartItemsBundleRoom;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

}
