<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemBundleRoom;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * CartItemRoomDto.
 */
class CartItemBundleRoomDto
{
    /**
     * @Assert\NotBlank(message="Ошибка! не указана комната")
     */
    private ?string $roomId = null;

    /**
     * @Assert\NotBlank(message="Ошибка! не указан тип спального мест")
     */
    private ?string $bedTypeId = null;

    /**
     * @Assert\NotBlank(message="Ошибка! не указан тариф")
     */
    private ?string $ratePlanId = null;

    /**
     * @Assert\NotBlank(message="Ошибка! не указан тип питания")
     */
    private ?string $foodTypeId = null;

    /**
     * @Assert\NotBlank(message="Ошибка! не указана дата заезда")
     */
    private ?string $dateFrom = null;

    /**
     * @Assert\NotBlank(message="Ошибка! не указана дата выезда")
     */
    private ?string $dateTo = null;

    /**
     * @Assert\NotBlank(message="Ошибка! не указано количество номеров")
     */
    private ?int $quantity = null;

    /**
     * @Assert\NotBlank(message="Ошибка! не указано количество взрослых гостей")
     */
    private int $adultCount;

    /**
     * @Assert\NotBlank(message="Ошибка! нет параметра количество детей")
     */
    private int $childCount;

    /**
     * @Assert\Date()
     */
    private string $activationDate;

    /**
     * @Assert\NotBlank()
     */
    private string $bundleProductId;

    /**
     * @Assert\NotBlank()
     */
    private int $bundleQuantity;

    public function __construct(
        string $bundleProductId,
        int $bundleQuantity,
        string $activationDate,
        int $adultCount,
        int $childCount
    ) {
        $this->bundleProductId = $bundleProductId;
        $this->bundleQuantity = $bundleQuantity;
        $this->adultCount = $adultCount;
        $this->childCount = $childCount;
        $this->activationDate = $activationDate;
    }

    /**
     * @return string
     */
    public function getBundleProductId(): string
    {
        return $this->bundleProductId;
    }

    /**
     * @return int
     */
    public function getBundleQuantity(): int
    {
        return $this->bundleQuantity;
    }

    /**
     * @return int
     */
    public function getAdultCount(): int
    {
        return $this->adultCount;
    }

    /**
     * @return int
     */
    public function getChildCount(): int
    {
        return $this->childCount;
    }

    /**
     * @return string
     */
    public function getActivationDate(): string
    {
        return $this->activationDate;
    }

    /**
     * @param string|null $roomId
     */
    public function setRoomId(?string $roomId): void
    {
        $this->roomId = $roomId;
    }

    /**
     * @return string|null
     */
    public function getRoomId(): ?string
    {
        return $this->roomId;
    }

    /**
     * @param string|null $ratePlanId
     */
    public function setRatePlanId(?string $ratePlanId): void
    {
        $this->ratePlanId = $ratePlanId;
    }

    /**
     * @return string|null
     */
    public function getRatePlanId(): ?string
    {
        return $this->ratePlanId;
    }

    /**
     * @param string|null $bedTypeId
     */
    public function setBedTypeId(?string $bedTypeId): void
    {
        $this->bedTypeId = $bedTypeId;
    }

    /**
     * @return string|null
     */
    public function getBedTypeId(): ?string
    {
        return $this->bedTypeId;
    }

    /**
     * @param string|null $foodTypeId
     */
    public function setFoodTypeId(?string $foodTypeId): void
    {
        $this->foodTypeId = $foodTypeId;
    }

    /**
     * @return string|null
     */
    public function getFoodTypeId(): ?string
    {
        return $this->foodTypeId;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param string|null $dateFrom
     */
    public function setDateFrom(?string $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return string|null
     */
    public function getDateFrom(): ?string
    {
        return $this->dateFrom;
    }

    /**
     * @param string|null $dateTo
     */
    public function setDateTo(?string $dateTo): void
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return string|null
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }
}
