<?php
declare(strict_types=1);

namespace App\Order\Command\Cart\ValidateRoom;

use App\Hotel\Model\Room\Room;
use Symfony\Component\Validator\Constraints as Assert;

class CartItemRoomDto
{
    /**
     * @Assert\NotBlank()
     */
    private string $room;

    /**
     * @Assert\NotBlank()
     */
    private string $bedType;

    /**
     * @Assert\NotBlank()
     */
    private int $quantity;

    /**
     * @Assert\NotBlank()
     */
    private string $activationDate;

    private bool $isInvalidActivationDate = false;
    private ?Room $roomObject = null;

    /**
     * CartItemRoomDto constructor.
     * @param string $room
     * @param int $quantity
     * @param string $activationDate
     */
    public function __construct(string $room, string $bedType, int $quantity, string $activationDate)
    {
        $this->room = $room;
        $this->bedType = $bedType;
        $this->quantity = $quantity;
        $this->activationDate = $activationDate;
    }

    /**
     * @return string
     */
    public function getRoom(): string
    {
        return $this->room;
    }

    /**
     * @return string
     */
    public function getBedType(): string
    {
        return $this->bedType;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getActivationDate(): string
    {
        return $this->activationDate;
    }

    /**
     * @param bool $isInvalidActivationDate
     */
    public function setIsInvalidActivationDate(bool $isInvalidActivationDate): void
    {
        $this->isInvalidActivationDate = $isInvalidActivationDate;
    }

    /**
     * @return bool
     */
    public function getIsInvalidActivationDate(): bool
    {
        return $this->isInvalidActivationDate;
    }

    /**
     * @return Room|null
     */
    public function getRoomObject(): ?Room
    {
        return $this->roomObject;
    }

    /**
     * @param Room|null $roomObject
     */
    public function setRoomObject(?Room $roomObject): void
    {
        $this->roomObject = $roomObject;
    }
}
