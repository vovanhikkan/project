<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\ValidateRoom;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Hotel\Repository\RoomRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;
use App\Order\Service\Cart\Updater;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private RoomRepository $roomRepository;

    public function __construct(
        RoomRepository $roomRepository
    ) {
        $this->roomRepository = $roomRepository;
    }

    public function handle(Command $command): array
    {
        foreach ($command->getCartItems() as $cartItem) {
            /**
             * @var CartItemRoomDto $cartItem
             */
            $room = $this->roomRepository->get(new Uuid($cartItem->getRoom()));

            if ($cartItem->getActivationDate() !== null && $cartItem->getActivationDate() !== date('Y-m-d')) {
                $cartItem->setIsInvalidActivationDate(true);
            }

            $cartItem->setRoomObject($room);
        }

        return $command->getCartItems();
    }
}
