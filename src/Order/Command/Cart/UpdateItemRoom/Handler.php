<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\UpdateItemRoom;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Order\Model\Cart\Cart;
use App\Order\Repository\CartRepository;
use App\Order\Service\Cart\Updater;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private CartRepository $cartRepository;
    private Updater $updater;

    public function __construct(
        UserRepository $userRepository,
        CartRepository $cartRepository,
        Updater $updater
    ) {
        $this->userRepository = $userRepository;
        $this->cartRepository = $cartRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): Cart
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cart = $this->cartRepository->getActiveByUser($user);

        $cartItem = null;
        foreach ($cart->getCartItemsRoom() as $_cartItem) {
            if ($_cartItem->getId()->getValue() === $command->getCartItemId()) {
                $cartItem = $_cartItem;
                break;
            }
        }

        if ($cartItem === null) {
            throw new NotFoundException();
        }

        $this->updater->updateCartItemRoom(
            $cart,
            $cartItem,
            new Quantity($command->getQuantity()),
            null,
            $command->getActivationDate() !== null
                ? new DateTimeImmutable($command->getActivationDate()) : null,
        );

        return $cart;
    }
}
