<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\CardNumCheck;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $cardNum;

    public function __construct(string $cardNum)
    {
        $this->cardNum = $cardNum;
    }

    public function getCardNum(): string
    {
        return $this->cardNum;
    }
}
