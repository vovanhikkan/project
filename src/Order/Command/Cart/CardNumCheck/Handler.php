<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\CardNumCheck;

use App\Application\ValueObject\CardNum;
use App\Pps\Service\External\CardChecker;

/**
 * Handler.
 */
class Handler
{
    private CardChecker $cardChecker;

    public function __construct(CardChecker $cardChecker)
    {
        $this->cardChecker = $cardChecker;
    }

    public function handle(Command $command): bool
    {
        return $this->cardChecker->check(new CardNum($command->getCardNum())) !== null;
    }
}
