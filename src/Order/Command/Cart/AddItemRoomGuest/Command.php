<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemRoomGuest;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $cartItemsRoomGuest;

    /**
     * @Assert\NotBlank()
     */
    private string $userId;

    /**
     * Command constructor.
     * @param array $cartItemsRoomGuest
     * @param string $userId
     */
    public function __construct(array $cartItemsRoomGuest, string $userId)
    {
        $this->cartItemsRoomGuest = $cartItemsRoomGuest;
        $this->userId = $userId;
    }

    /**
     * @return array
     */
    public function getCartItemsRoomGuest(): array
    {
        return $this->cartItemsRoomGuest;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

}
