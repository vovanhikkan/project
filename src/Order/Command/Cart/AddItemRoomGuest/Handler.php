<?php

declare(strict_types=1);

namespace App\Order\Command\Cart\AddItemRoomGuest;

use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Data\TransactionManager;
use App\Hotel\Model\Guest\Age;
use App\Hotel\Repository\GuestRepository;
use App\Hotel\Repository\RoomRepository;
use App\Hotel\Service\Guest\Creator;
use App\Order\Model\Cart\Cart;
use App\Order\Model\Order\Requirements;
use App\Order\Service\Cart\Factory;
use App\Order\Service\Cart\Updater;
use DateTimeImmutable;
use Throwable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private Factory $factory;
    private RoomRepository $roomRepository;
    private GuestRepository $guestRepository;
    private Creator $guestCreator;
    private Updater $updater;
    private TransactionManager $transactionManager;

    public function __construct(
        UserRepository $userRepository,
        Factory $factory,
        RoomRepository $roomRepository,
        GuestRepository $guestRepository,
        Creator $guestCreator,
        Updater $updater,
        TransactionManager $transactionManager
    ) {
        $this->userRepository = $userRepository;
        $this->factory = $factory;
        $this->roomRepository = $roomRepository;
        $this->guestRepository = $guestRepository;
        $this->guestCreator = $guestCreator;
        $this->updater = $updater;
        $this->transactionManager = $transactionManager;
    }

    public function handle(Command $command): Cart
    {
        $user = $this->userRepository->get(new Uuid($command->getUserId()));
        $cartItemsRoomGuest = $command->getCartItemsRoomGuest();

        $this->transactionManager->beginTransaction();
        try {
            $cart = $this->factory->getActiveOrCreate($user);

            /** @var CartItemRoomGuestDto $roomGuestDto */
            foreach ($cartItemsRoomGuest as $roomGuestDto) {
                $room = $this->roomRepository->get(new Uuid($roomGuestDto->getRoomId()));
                $requirements = null;
                if ($roomGuestDto->getRequirements()) {
                    $requirements = new Requirements($roomGuestDto->getRequirements());
                }

                $searchGuestResult = $this->guestRepository->getGuest($roomGuestDto);

                if (empty($searchGuestResult)) {
                    $guest = $this->guestCreator->create(
                        Uuid::generate(),
                        new FirstName($roomGuestDto->getFirstName()),
                        new LastName($roomGuestDto->getLastName()),
                        new Age($roomGuestDto->getAge()),
                        $roomGuestDto->isMainGuest()
                    );
                } else {
                    $guest = array_shift($searchGuestResult);
                }

                $this->updater->addCartItemRoomGuest(
                    $cart,
                    $room,
                    $guest,
                    $requirements
                );
            }

            $this->transactionManager->commit();
        } catch (Throwable $e) {
            $this->transactionManager->rollBack();
            throw $e;
        }

        return $cart;
    }
}
