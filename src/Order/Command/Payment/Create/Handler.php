<?php

declare(strict_types=1);

namespace App\Order\Command\Payment\Create;

use App\Application\ValueObject\Url;
use App\Application\ValueObject\Uuid;
use App\Order\Model\Payment\Payment;
use App\Order\Model\Payment\Provider;
use App\Order\Repository\OrderRepository;
use App\Order\Repository\PaySystemRepository;
use App\Order\Service\Payment\Creator;

/**
 * Handler.
 */
class Handler
{
    private OrderRepository $orderRepository;
    private PaySystemRepository $paySystemRepository;
    private Creator $creator;

    public function __construct(
        OrderRepository $orderRepository,
        PaySystemRepository $paySystemRepository,
        Creator $creator
    ) {
        $this->orderRepository = $orderRepository;
        $this->paySystemRepository = $paySystemRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): Payment
    {
        $order = $this->orderRepository->get(new Uuid($command->getOrderId()));
        $paySystem = $this->paySystemRepository->get(new Uuid($command->getPaySystemId()));

        return $this->creator->create(Uuid::generate(), $order, $paySystem, new Url($command->getSuccessUrl()));
    }
}
