<?php

declare(strict_types=1);

namespace App\Order\Command\Payment\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $orderId;

    /**
     * @Assert\NotBlank()
     */
    private string $paySystemId;

    /**
     * @Assert\NotBlank()
     */
    private string $successUrl;

    public function __construct(string $orderId, string $paySystemId, string $successUrl)
    {
        $this->orderId = $orderId;
        $this->paySystemId = $paySystemId;
        $this->successUrl = $successUrl;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getPaySystemId(): string
    {
        return $this->paySystemId;
    }

    public function getSuccessUrl(): string
    {
        return $this->successUrl;
    }
}
