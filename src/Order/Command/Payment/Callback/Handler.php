<?php

declare(strict_types=1);

namespace App\Order\Command\Payment\Callback;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Uuid;
use App\Order\Model\Payment\Payment;
use App\Order\Repository\PaymentRepository;
use App\Order\Service\Payment\CallbackProcessor;

/**
 * Handler.
 */
class Handler
{
    private PaymentRepository $paymentRepository;
    private CallbackProcessor $callbackProcessor;

    public function __construct(
        PaymentRepository $paymentRepository,
        CallbackProcessor $callbackProcessor
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->callbackProcessor = $callbackProcessor;
    }

    public function handle(Command $command): ?Payment
    {
        try {
            $payment = $this->paymentRepository->get(new Uuid($command->getId()));
        } catch (NotFoundException $e) {
            return null;
        }

        if ($this->callbackProcessor->process($payment)) {
            return $payment;
        }

        return null;
    }
}
