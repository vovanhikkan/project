<?php

declare(strict_types=1);

namespace App\Order\Dto;

/**
 * PaymentProviderCreateDto.
 */
class PaymentProviderCreateDto
{
    private string $externalId;
    private string $redirectUrl;

    public function __construct(string $externalId, string $redirectUrl)
    {
        $this->externalId = $externalId;
        $this->redirectUrl = $redirectUrl;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }
}
