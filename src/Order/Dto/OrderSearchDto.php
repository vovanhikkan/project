<?php

declare(strict_types=1);

namespace App\Order\Dto;

use App\Application\ValueObject\Uuid;

/**
 * OrderSearchDto.
 */
class OrderSearchDto
{
    public ?Uuid $userId = null;
}
