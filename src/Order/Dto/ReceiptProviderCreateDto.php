<?php

declare(strict_types=1);

namespace App\Order\Dto;

/**
 * ReceiptProviderCreateDto.
 */
class ReceiptProviderCreateDto
{
    private string $externalId;

    public function __construct(string $externalId)
    {
        $this->externalId = $externalId;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }
}
