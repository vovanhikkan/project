<?php

declare(strict_types=1);

namespace App\Order\Dto;

/**
 * ReceiptProviderStatusDto.
 */
class ReceiptProviderStatusDto
{
    private string $status;
    private ?string $url;

    public function __construct(string $status, ?string $url)
    {
        $this->status = $status;
        $this->url = $url;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }
}
