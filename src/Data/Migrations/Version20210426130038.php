<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210426130038 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item
                RENAME TO cart_item_product
        ');

        $this->addSql('CREATE TABLE cart_item_room (
		        id CHAR(36) NOT NULL,
		        cart_id CHAR(36) NOT NULL,
		        room_id CHAR(36) NOT NULL,
		        quantity SMALLINT(5) UNSIGNED NOT NULL,
		        created_at DATETIME NOT NULL,
		        updated_at DATETIME DEFAULT NULL,
		        INDEX IDX_605132FF1AD5CDBF (cart_id),
		        INDEX IDX_605132FF4584665A (room_id),
		        PRIMARY KEY(id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            ALTER TABLE order_item
                RENAME TO order_item_product
        ');

        $this->addSql('CREATE TABLE order_item_room (
		        id CHAR(36) NOT NULL,
		        order_id CHAR(36) NOT NULL,
		        room_id CHAR(36) NOT NULL,
		        price BIGINT(20) UNSIGNED NOT NULL,
		        quantity SMALLINT(5) UNSIGNED NOT NULL,
		        amount BIGINT(20) UNSIGNED NOT NULL,
		        INDEX IDX_6286E988D9F6D38 (order_id),
		        INDEX IDX_6286E984584665A (room_id),
		        PRIMARY KEY(id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
