<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210311044904 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item
                ADD last_name VARCHAR(255) DEFAULT NULL AFTER activation_date,
                ADD first_name VARCHAR(255) DEFAULT NULL AFTER last_name,
                ADD middle_name VARCHAR(255) DEFAULT NULL AFTER first_name,
                ADD birth_date DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\' AFTER middle_name,
                ADD card_num VARCHAR(255) DEFAULT NULL AFTER birth_date
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE order_item
                DROP last_name,
                DROP first_name,
                DROP middle_name,
                DROP birth_date,
                DROP card_num
        ');
    }
}
