<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210414065641 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE review (
		        id CHAR(36) PRIMARY KEY,
                user_id CHAR(36),
                place_id CHAR(36),
		        body TEXT NOT NULL,
		        rating TINYINT UNSIGNED NOT NULL,
		        created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_794381C6A76ED395 (user_id), 
                INDEX IDX_794381C6DA6A219 (place_id), 
                CONSTRAINT FK_794381C6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id),
                CONSTRAINT FK_794381C6DA6A219 FOREIGN KEY (place_id) REFERENCES place (id) 
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            ALTER TABLE place
                ADD COLUMN rating JSON DEFAULT NULL AFTER is_active
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE review');

        $this->addSql('
            ALTER TABLE place
                DROP COLUMN rating
        ');
    }
}
