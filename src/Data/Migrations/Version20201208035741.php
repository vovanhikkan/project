<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201208035741 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE user (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                role SMALLINT(3) UNSIGNED NOT NULL,
                email VARCHAR(255) NOT NULL,
                phone VARCHAR(255) DEFAULT NULL,
                password_hash VARCHAR(128) NOT NULL,
                last_name VARCHAR(255) NOT NULL,
                first_name VARCHAR(255) NOT NULL,
                middle_name VARCHAR(255) DEFAULT NULL,
                birth_date DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\',
                sex SMALLINT(3) UNSIGNED DEFAULT NULL,
                status SMALLINT(3) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE user');
    }
}
