<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210310205847 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE order_item
                ADD last_name VARCHAR(255) DEFAULT NULL AFTER activation_date,
                ADD first_name VARCHAR(255) DEFAULT NULL AFTER last_name,
                ADD middle_name VARCHAR(255) DEFAULT NULL AFTER first_name,
                ADD birth_date DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\' AFTER middle_name,
                ADD client_id INT(11) UNSIGNED DEFAULT NULL AFTER birth_date,
                ADD cash_item_id INT(11) UNSIGNED DEFAULT NULL AFTER client_id
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE order_item
                DROP last_name,
                DROP first_name,
                DROP middle_name,
                DROP birth_date,
                DROP client_id,
                DROP cash_item_id
        ');
    }
}
