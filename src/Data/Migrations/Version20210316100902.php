<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316100902 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE baskets;
            DROP TABLE basket_items;
            DROP TABLE orders;
            DROP TABLE invoice;
            DROP TABLE pay_systems;
            DROP TABLE places_blocks;
            DROP TABLE places_geo_points;
            DROP TABLE places_photos;
            DROP TABLE places_sections;
            DROP TABLE places_videos;
            DROP TABLE packet_items;
            DROP TABLE packets_rate_plans;
            DROP TABLE packets;
            DROP TABLE reviews;
            DROP TABLE places;
            DROP TABLE products;
            DROP TABLE prices;
            DROP TABLE sessions;
            DROP TABLE users_properties;
            DROP TABLE refresh_token;
            DROP TABLE users;
        ');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
