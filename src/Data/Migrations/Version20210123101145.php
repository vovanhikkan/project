<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210123101145 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE user
            ADD is_confirmed_service_letters SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER reset_password_token_expired_at,
            ADD is_confirmed_mailing SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER is_confirmed_service_letters
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE user
            DROP is_confirmed_service_letters,
            DROP is_confirmed_mailing
        ');
    }
}
