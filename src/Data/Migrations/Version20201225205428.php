<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201225205428 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        // Уровень пользователя в программе лояльности

        $this->addSql('CREATE TABLE IF NOT EXISTS levels (
            id int PRIMARY KEY AUTO_INCREMENT,
            name json
        )');

        // Тарифы

        $this->addSql('CREATE TABLE IF NOT EXISTS rate_plans (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            name json,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id)
        )');

        // Actions - скидки и промокоды

        $this->addSql('CREATE TABLE IF NOT EXISTS action_groups (
            id int PRIMARY KEY AUTO_INCREMENT,
            name json
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS actions (
            id int PRIMARY KEY AUTO_INCREMENT,
            group_id int,
            name json,
            description json,
            viewFrom date,
            shownFrom date,
            shownTo date,
            discountAbsoluteValue float,
            discountPercentageValue float,
            cashbackPointsAbsoluteValue float,
            cashbackPointsPercentageValue float,
            minRoomDaysCount int,
            maхRoomDaysCount int,
            bookingFrom int,
            bookingTo int,
            minProductQuantity int,
            maxProductQuantity int,
            activationDateFrom date,
            activationDateTo date,
            hasLevels bool,
            hasPromocodes bool,
            promocodeLifetime int,
            promocodePrefix varchar(20),
            isActive bool,
            totalCount int,
            activatedCount int,
            FOREIGN KEY (group_id) REFERENCES action_groups (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS promocodes (
            id int PRIMARY KEY AUTO_INCREMENT,
            action_id int,
            value varchar(50),
            totalCount int,
            activatedCount int,
            expiredAt datetime,
            FOREIGN KEY (action_id) REFERENCES actions (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS actions_products (
            id int PRIMARY KEY AUTO_INCREMENT,
            action_id int,
            product_id int,
            FOREIGN KEY (action_id) REFERENCES actions (id),
            FOREIGN KEY (product_id) REFERENCES products (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS actions_rate_plans (
            id int PRIMARY KEY AUTO_INCREMENT,
            action_id int,
            rate_plan_id int,
            FOREIGN KEY (action_id) REFERENCES actions (id),
            FOREIGN KEY (rate_plan_id) REFERENCES rate_plans (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS actions_levels (
            id int PRIMARY KEY AUTO_INCREMENT,
            action_id int,
            level_id int,
            FOREIGN KEY (action_id) REFERENCES actions (id),
            FOREIGN KEY (level_id) REFERENCES levels (id)
        )');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
