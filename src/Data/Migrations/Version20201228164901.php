<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201228164901 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS hotel_types (
            id int PRIMARY KEY AUTO_INCREMENT,
            name json,
            sort int,
            isActive bool
        )');

        $this->addSql('ALTER TABLE hotels
            ADD COLUMN typeId int,
            ADD FOREIGN KEY (typeId) REFERENCES hotel_types (id)
        ');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
