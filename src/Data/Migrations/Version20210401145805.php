<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210401145805 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE bundle_term (
                id CHAR(36) PRIMARY KEY,
                bundle_id CHAR(36),
                description JSON NOT NULL,
                sort INT UNSIGNED,
                INDEX IDX_9978FC4DF1FAD9D3 (bundle_id),
                CONSTRAINT FK_9978FC4DF1FAD9D3 FOREIGN KEY (bundle_id) REFERENCES bundle (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE bundle_term;
        ');
    }
}
