<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201228103832 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
       $this->addSql('ALTER TABLE packets
            ADD COLUMN isActive bool
        ');

        $this->addSql('CREATE TABLE IF NOT EXISTS packet_items (
            id int PRIMARY KEY AUTO_INCREMENT,
            title json,
            subTitle json,
            description json,
            sort int,
            isActive bool,
            photoId int,
            placeId bigint,
            packetId int,
            FOREIGN KEY (photoId) REFERENCES storage (id),
            FOREIGN KEY (placeId) REFERENCES places (id),
            FOREIGN KEY (packetId) REFERENCES packets (id)
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
