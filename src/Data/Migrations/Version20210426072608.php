<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210426072608 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE guest (
                id CHAR(36) NOT NULL,
                first_name VARCHAR(255) NOT NULL,
                last_name VARCHAR(255) NOT NULL,
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');


        $this->addSql('
            CREATE TABLE user_guest (
                user_id CHAR(36),
                guest_id CHAR(36),
                INDEX IDX_4E75616DA76ED395 (user_id), 
                INDEX IDX_4E75616D9A4AA658 (guest_id),
                CONSTRAINT FK_4E75616DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id),
                CONSTRAINT FK_4E75616D9A4AA658 FOREIGN KEY (guest_id) REFERENCES guest (id),
                PRIMARY KEY (`user_id`,`guest_id`)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE order_guest (
                order_id CHAR(36),
                guest_id CHAR(36),
                INDEX IDX_2F29EEC88D9F6D38 (order_id), 
                INDEX IDX_2F29EEC89A4AA658 (guest_id),
                CONSTRAINT FK_2F29EEC88D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id),
                CONSTRAINT FK_2F29EEC89A4AA658 FOREIGN KEY (guest_id) REFERENCES guest (id),
                PRIMARY KEY (`order_id`,`guest_id`)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            ALTER TABLE `order`
                ADD COLUMN check_in_time JSON AFTER enum_number,
                ADD COLUMN requirements JSON AFTER check_in_time
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE user_guest;
        ');

        $this->addSql('
            DROP TABLE order_guest;
        ');

        $this->addSql('
            DROP TABLE guest;
        ');

        $this->addSql('
            ALTER TABLE `order`
                DROP COLUMN check_in_time,
                DROP COLUMN requirements
        ');
    }
}
