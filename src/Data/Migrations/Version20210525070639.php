<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210525070639 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                MODIFY COLUMN works_from TIME,
                MODIFY COLUMN works_to TIME
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                MODIFY COLUMN works_from DATETIME,
                MODIFY COLUMN works_to DATETIME
        ');
    }
}
