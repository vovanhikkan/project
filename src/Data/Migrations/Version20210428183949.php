<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428183949 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE budget 
                ADD COLUMN is_active TINYINT(1) DEFAULT 1
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE budget 
                DROP COLUMN is_active
        ');
    }
}
