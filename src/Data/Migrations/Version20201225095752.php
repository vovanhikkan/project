<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201225095752 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE places_blocks
        ADD COLUMN photo int,
        ADD COLUMN video_youtube_code varchar(50),
        ADD COLUMN video_background int,
        ADD FOREIGN KEY (photo) REFERENCES storage (id),
        ADD FOREIGN KEY (video_background) REFERENCES storage (id)
        ');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
