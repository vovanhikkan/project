<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201222140214 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels (
            id int PRIMARY KEY AUTO_INCREMENT,
            name JSON,
            description JSON,
            starCount int,
            location_id bigint,
            address JSON,
            logo_svg int,
            logo int,
            links JSON,
            lat decimal(9, 6),
            lon decimal(9, 6),
            FOREIGN KEY (location_id) REFERENCES locations (id),
            FOREIGN KEY (logo_svg) REFERENCES storage (id),
            FOREIGN KEY (logo) REFERENCES storage (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels_photos (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            photo int,
            sort int,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id),
            FOREIGN KEY (photo) REFERENCES storage (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels_videos (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            youtube_code varchar(50),
            background int,
            sort int,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id),
            FOREIGN KEY (background) REFERENCES storage (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS rooms (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            name JSON,
            description JSON,
            square int,
            mainPlaceCount int,
            extraPlaceCount int,
            zeroPlaceCount int,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id)
        )');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
