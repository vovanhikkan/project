<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201107150322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                 CREATE TABLE IF NOT EXISTS `periods` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `pps_settings` json NOT NULL,
                    `start_date` date NOT NULL,
                    `finish_date` date NOT NULL,
                    `is_active` boolean NOT NULL DEFAULT 1
                );
                
                CREATE TABLE IF NOT EXISTS `pps_groups` (
                    `id` bigint PRIMARY KEY NOT NULL,
                    `name` varchar(255) NOT NULL,
                    `module_id` bigint NOT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `pps_periods` (
                    `id` bigint PRIMARY KEY NOT NULL,
                    `name` varchar(255) NOT NULL,
                    `start_date` date NOT NULL,
                    `finish_date` date NOT NULL,
                    `is_active` boolean NOT NULL DEFAULT 1
                );
                
                CREATE TABLE IF NOT EXISTS `pps_prices` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `period_id` bigint NOT NULL,
                    `product_id` bigint NOT NULL,
                    `price` float NOT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `pps_products` (
                    `id` bigint PRIMARY KEY NOT NULL,
                    `name` varchar(255) NOT NULL,
                    `group_id` bigint NOT NULL,
                    `is_active` boolean NOT NULL DEFAULT 1
                );
                
                CREATE TABLE IF NOT EXISTS `prices` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `product_id` int,
                    `period_id` int,
                    `price` float NOT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `products` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `section_id` int NOT NULL,
                    `settings` json DEFAULT NULL,
                    `pps_settings` json DEFAULT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `sections` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `is_active` boolean NOT NULL DEFAULT 1,
                    `code` varchar(255),
                    `name` varchar(255),
                    `description` text,
                    `tariff_description` text,
                    `picture` int,
                    `settings` json NOT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `order_product_list` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `order_id` int,
                    `product_id` int,
                    `quantity` int,
                    `base_price` float,
                    `price` float,
                    `currency` varchar(255),
                    `option` varchar(255),
                    `active_from` date,
                    FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
                    FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
                );
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
