<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225074122 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE section
                MODIFY COLUMN name JSON,
                MODIFY COLUMN description JSON,
                MODIFY COLUMN tariff_description JSON,
                MODIFY COLUMN inner_title JSON,
                MODIFY COLUMN inner_description JSON,
                ADD COLUMN picture_id CHAR(36),
                ADD COLUMN web_icon_id CHAR(36),
                ADD COLUMN web_background_id CHAR(36),
                ADD COLUMN mobile_icon_id CHAR(36),
                ADD COLUMN mobile_background_id CHAR(36),
                ADD COLUMN recommendation_sort INT UNSIGNED NOT NULL,
                ADD COLUMN is_active SMALLINT UNSIGNED NOT NULL DEFAULT 1
        ');
        $this->addSql('CREATE INDEX IDX_2D737AEFEE45BDBF ON section (picture_id)');
        $this->addSql('CREATE INDEX IDX_2D737AEFBC1679AC ON section (web_icon_id)');
        $this->addSql('CREATE INDEX IDX_2D737AEF93BFB765 ON section (web_background_id)');
        $this->addSql('CREATE INDEX IDX_2D737AEFB4131AC7 ON section (mobile_icon_id)');
        $this->addSql('CREATE INDEX IDX_2D737AEF16D3E4EB ON section (mobile_background_id)');
        $this->addSql('
            ALTER TABLE section 
                ADD CONSTRAINT FK_2D737AEFEE45BDBF FOREIGN KEY (picture_id) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE section 
                ADD CONSTRAINT FK_2D737AEFBC1679AC FOREIGN KEY (web_icon_id) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE section 
                ADD CONSTRAINT FK_2D737AEF93BFB765 FOREIGN KEY (web_background_id) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE section 
                ADD CONSTRAINT FK_2D737AEFB4131AC7 FOREIGN KEY (mobile_icon_id) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE section 
                ADD CONSTRAINT FK_2D737AEF16D3E4EB FOREIGN KEY (mobile_background_id) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE categories_sections
                DROP CONSTRAINT categories_sections_ibfk_2
        ');
        $this->addSql('
            ALTER TABLE places_sections
                DROP CONSTRAINT places_sections_ibfk_2
        ');
        $this->addSql('DROP TABLE IF EXISTS sections');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE section
                DROP CONSTRAINT FK_2D737AEFEE45BDBF
        ');
        $this->addSql('
            ALTER TABLE section
                DROP CONSTRAINT FK_2D737AEFBC1679AC
        ');
        $this->addSql('
            ALTER TABLE section
                DROP CONSTRAINT FK_2D737AEF93BFB765
        ');
        $this->addSql('
            ALTER TABLE section
                DROP CONSTRAINT FK_2D737AEFB4131AC7
        ');
        $this->addSql('
            ALTER TABLE section
                DROP CONSTRAINT FK_2D737AEF16D3E4EB
        ');
        $this->addSql('
            ALTER TABLE section
                DROP INDEX IDX_2D737AEFEE45BDBF
        ');
        $this->addSql('
            ALTER TABLE section
                DROP INDEX IDX_2D737AEFBC1679AC
        ');
        $this->addSql('
            ALTER TABLE section
                DROP INDEX IDX_2D737AEF93BFB765
        ');
        $this->addSql('
            ALTER TABLE section
                DROP INDEX IDX_2D737AEFB4131AC7
        ');
        $this->addSql('
            ALTER TABLE section
                DROP INDEX IDX_2D737AEF16D3E4EB
        ');
        $this->addSql('
            ALTER TABLE section
                MODIFY COLUMN name VARCHAR(255),
                MODIFY COLUMN description TEXT,
                MODIFY COLUMN tariff_description TEXT,
                MODIFY COLUMN inner_title VARCHAR(255),
                MODIFY COLUMN inner_description TEXT,
                DROP COLUMN picture_id,
                DROP COLUMN web_icon_id,
                DROP COLUMN web_background_id,
                DROP COLUMN mobile_icon_id,
                DROP COLUMN mobile_background_id,
                DROP COLUMN recommendation_sort,
                DROP COLUMN is_active
        ');
        $this->addSql('CREATE TABLE sections (
              id INT PRIMARY KEY  
            )
        ');
        $this->addSql('
            ALTER TABLE categories_sections
                ADD CONSTRAINT categories_sections_ibfk_2 FOREIGN KEY (section_id) REFERENCES sections (id)
        ');
        $this->addSql('
            ALTER TABLE places_sections
                ADD CONSTRAINT places_sections_ibfk_2 FOREIGN KEY (section_id) REFERENCES sections (id)
        ');
    }
}
