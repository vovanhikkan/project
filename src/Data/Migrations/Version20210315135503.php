<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210315135503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE receipt
                ADD COLUMN external_url VARCHAR(255) AFTER external_id
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE receipt
                DROP COLUMN external_url
        ');
    }
}
