<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210317144426 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item
                ADD COLUMN is_new_card TINYINT(1) UNSIGNED AFTER card_num
        ');
        $this->addSql('
            ALTER TABLE order_item
                ADD COLUMN card_price BIGINT UNSIGNED AFTER card_num
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item
                DROP COLUMN is_new_card
        ');
        $this->addSql('
            ALTER TABLE order_item
                DROP COLUMN card_price
        ');
    }
}
