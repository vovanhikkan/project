<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210601071144 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {

        $this->addSql('
            ALTER TABLE action 
                ADD COLUMN holded_count INT NULL AFTER total_count,
                ADD COLUMN ordered_count INT NULL AFTER holded_count,
                ADD COLUMN ordered_from DATE NULL AFTER shown_to,
                ADD COLUMN ordered_to DATE NULL AFTER ordered_from,
                ADD COLUMN bundle_activation_from DATE AFTER activated_count,
                ADD COLUMN bundle_activation_to DATE AFTER bundle_activation_from,
                ADD COLUMN first_booking_day_from DATE AFTER max_day_count,
                ADD COLUMN first_booking_day_to DATE AFTER first_booking_day_from,
                ADD COLUMN last_booking_day_from DATE AFTER first_booking_day_to,
                ADD COLUMN last_booking_day_to DATE AFTER last_booking_day_from,
                RENAME COLUMN activation_date_from TO product_activation_from, 
                RENAME COLUMN activation_date_to TO product_activation_to
        ');

        $this->addSql('
            ALTER TABLE action 
                DROP COLUMN activated_count,
                DROP COLUMN booking_from,
                DROP COLUMN booking_to
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE action 
                DROP COLUMN holded_count,
                DROP COLUMN ordered_count,
                DROP COLUMN ordered_from,
                DROP COLUMN ordered_to,
                DROP COLUMN bundle_activation_from,
                DROP COLUMN bundle_activation_to,
                DROP COLUMN first_booking_day_from,
                DROP COLUMN first_booking_day_to,
                DROP COLUMN last_booking_day_from,
                DROP COLUMN last_booking_day_to
        ');

        $this->addSql('
            ALTER TABLE action 
                RENAME COLUMN product_activation_from TO activation_date_from, 
                RENAME COLUMN product_activation_to TO activation_date_to,
                ADD COLUMN booking_from DATE AFTER max_day_count,
                ADD COLUMN booking_to DATE AFTER booking_from,
                ADD COLUMN activated_count INT NULL AFTER total_count
        ');
    }
}
