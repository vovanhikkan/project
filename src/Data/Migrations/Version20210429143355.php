<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210429143355 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE order_item_room
                ADD COLUMN activation_date DATE AFTER amount,
                ADD client_id INT(11) UNSIGNED DEFAULT NULL AFTER activation_date,
                ADD cash_item_id INT(11) UNSIGNED DEFAULT NULL AFTER client_id
        ');
        $this->addSql('
            ALTER TABLE order_item_room
                ALTER COLUMN price SET DEFAULT 0,
                ALTER COLUMN amount SET DEFAULT 0
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
