<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210607093540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_room
                ADD COLUMN rate_plan_id CHAR(36) after bed_type_id    
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                ADD COLUMN rate_plan_id CHAR(36) after bed_type_id    
        ');

        $this->addSql('ALTER TABLE cart_item_room 
            ADD INDEX IDX_605132FF325B4D7A (rate_plan_id),            
            ADD CONSTRAINT FK_605132FF325B4D7A FOREIGN KEY (rate_plan_id) REFERENCES rate_plan (id);
        ');

        $this->addSql('ALTER TABLE order_item_room 
            ADD INDEX IDX_6286E98325B4D7A (rate_plan_id),            
            ADD CONSTRAINT FK_6286E98325B4D7A FOREIGN KEY (rate_plan_id) REFERENCES rate_plan (id);
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_room
                DROP CONSTRAINT FK_605132FF325B4D7A,
                DROP INDEX IDX_605132FF325B4D7A;
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP CONSTRAINT FK_6286E98325B4D7A,
                DROP INDEX IDX_6286E98325B4D7A;
        ');

        $this->addSql('
            ALTER TABLE cart_item_room
                DROP COLUMN rate_plan_id;
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP COLUMN rate_plan_id;
        ');

    }
}
