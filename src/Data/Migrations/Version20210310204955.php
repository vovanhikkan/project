<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use App\Product\Model\Product\Type;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210310204955 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE product ADD type SMALLINT(3) UNSIGNED DEFAULT NULL AFTER id');
        $this->addSql('UPDATE product SET type = ?', [Type::NONE]);
        $this->addSql('ALTER TABLE product CHANGE type type SMALLINT(3) UNSIGNED NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE product DROP type');
    }
}
