<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201223193210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE hotels ADD COLUMN
        params JSON DEFAULT NULL');

        $this->addSql('ALTER TABLE facility_groups ADD COLUMN
        isShownInHotel BOOL DEFAULT NULL');

        $this->addSql('ALTER TABLE facility_groups ADD COLUMN
        isShownInRoom BOOL DEFAULT NULL');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
