<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210226050314 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE cart_item ADD activation_date DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\' AFTER quantity');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE cart_item DROP activation_date');
    }
}
