<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210303051858 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE `order` 
            ADD cash_list_id INT(11) UNSIGNED NOT NULL AFTER amount
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `order` DROP cash_list_id');
    }
}
