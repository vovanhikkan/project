<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210415134847 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE tag
                ADD COLUMN color_text VARCHAR(255),
                ADD COLUMN color_border VARCHAR(255);
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE tag
                DROP COLUMN color_text,
                DROP COLUMN color_border;
        ');
    }
}
