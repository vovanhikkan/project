<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210421074034 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE rate_plan_price 
                DROP CONSTRAINT FK_33C52D3E68A2C92D,
                DROP INDEX IDX_33C52D3E68A2C92D,
                DROP COLUMN hotel_extra_place_type_age_range_id
        ');
        

        $this->addSql('
            ALTER TABLE hotel_extra_place_type_age_range 
                DROP COLUMN id
        ');

        $this->addSql('
            ALTER TABLE hotel_extra_place_type_age_range 
                ADD PRIMARY KEY (hotel_extra_place_type_id,  hotel_age_range_id)
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
