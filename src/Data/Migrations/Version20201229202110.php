<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201229202110 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('DROP TABLE hotels_age_ranges_extra_place_types');

        $this->addSql('RENAME TABLE hotels_extra_place_types TO extra_place_types');

        $this->addSql('ALTER TABLE extra_place_types
            DROP FOREIGN KEY extra_place_types_ibfk_1,
            DROP COLUMN hotel_id,
            ADD COLUMN sort int
        ');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
