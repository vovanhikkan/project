<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210415103404 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE facility
                ADD COLUMN is_shown_in_search TINYINT(1) UNSIGNED AFTER is_shown_in_room_list;
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE facility
                DROP COLUMN is_shown_in_search;
        ');
    }
}
