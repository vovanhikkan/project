<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210405165106 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle
                ADD COLUMN width_type SMALLINT UNSIGNED
        ');
        $this->addSql('
            ALTER TABLE bundle_item
                ADD COLUMN width_type SMALLINT UNSIGNED
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle
                DROP COLUMN width_type
        ');
        $this->addSql('
            ALTER TABLE bundle_item
                DROP COLUMN width_type
        ');
    }
}
