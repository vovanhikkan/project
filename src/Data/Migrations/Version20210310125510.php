<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210310125510 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE `order`
                ADD COLUMN enum_number INT UNSIGNED AFTER status
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE `order`
                DROP COLUMN enum_number
        ');
    }
}
