<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324164137 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE room_main_place_variant (
                id CHAR(36) PRIMARY KEY,
                room_id CHAR(36) NOT NULL,
                value TINYINT UNSIGNED NOT NULL,
                INDEX IDX_BCAC49D054177093 (room_id),
                CONSTRAINT FK_BCAC49D054177093 FOREIGN KEY (room_id) REFERENCES room (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_extra_place_type (
                id CHAR(36) PRIMARY KEY,
                hotel_id CHAR(36) NOT NULL,
                name JSON NOT NULL,
                INDEX IDX_81FFED1F3243BB18 (hotel_id),
                CONSTRAINT FK_81FFED1F3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_age_range (
                id CHAR(36) PRIMARY KEY,
                hotel_id CHAR(36) NOT NULL,
                age_from TINYINT UNSIGNED NOT NULL,
                age_to TINYINT UNSIGNED NOT NULL,
                INDEX IDX_B8852D1D3243BB18 (hotel_id),
                CONSTRAINT FK_B8852D1D3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_extra_place_type_age_range (
                id CHAR(36) PRIMARY KEY,
                hotel_extra_place_type_id CHAR(36) NOT NULL,
                hotel_age_range_id CHAR(36) NOT NULL,
                INDEX IDX_5EA6A6BBB7E943D0 (hotel_extra_place_type_id),
                CONSTRAINT FK_5EA6A6BBB7E943D0 FOREIGN KEY (hotel_extra_place_type_id) REFERENCES hotel_extra_place_type (id),
                INDEX IDX_5EA6A6BBCF47D8EB (hotel_age_range_id),
                CONSTRAINT FK_5EA6A6BBCF47D8EB FOREIGN KEY (hotel_age_range_id) REFERENCES hotel_age_range (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE tax_type (
                id CHAR(36) PRIMARY KEY,   
                name JSON NOT NULL,  
                value TINYINT UNSIGNED NOT NULL
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE rate_plan_group (
                id CHAR(36) PRIMARY KEY,   
                name JSON NOT NULL,  
                sort INT UNSIGNED NOT NULL
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE rate_plan (
                id CHAR(36) PRIMARY KEY,   
                hotel_id CHAR(36) NOT NULL,  
                name JSON NOT NULL,  
                description JSON NOT NULL,  
                group_id CHAR(36) NOT NULL,
                food_type_id CHAR(36) NOT NULL,
                tax_type_id CHAR(36) NOT NULL,
                is_active TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
                active_from DATE NOT NULL,
                active_to DATE NOT NULL,
                shown_from DATE NOT NULL,
                shown_to DATE NOT NULL,
                is_shown_with_packets TINYINT(1) UNSIGNED,
                INDEX IDX_225EE9723243BB18 (hotel_id),
                INDEX IDX_225EE9728AD350AB (food_type_id),
                INDEX IDX_225EE97284042C99 (tax_type_id),
                INDEX IDX_225EE972FE54D947 (group_id),
                CONSTRAINT FK_225EE9723243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
                CONSTRAINT FK_225EE9728AD350AB FOREIGN KEY (food_type_id) REFERENCES food_type (id),
                CONSTRAINT FK_225EE97284042C99 FOREIGN KEY (tax_type_id) REFERENCES tax_type (id),
                CONSTRAINT FK_225EE972FE54D947 FOREIGN KEY (group_id) REFERENCES rate_plan_group (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE rate_plan_room (
                id CHAR(36) PRIMARY KEY,
                rate_plan_id CHAR(36) NOT NULL,
                room_id CHAR(36) NOT NULL,
                INDEX IDX_C64FD907325B4D7A (rate_plan_id),
                INDEX IDX_C64FD90754177093 (room_id),
                CONSTRAINT FK_C64FD907325B4D7A FOREIGN KEY (rate_plan_id) REFERENCES rate_plan (id),
                CONSTRAINT FK_C64FD90754177093 FOREIGN KEY (room_id) REFERENCES room (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            ALTER TABLE bed_type 
                ADD COLUMN web_icon CHAR(36),
                ADD INDEX IDX_AADE868EA196242E (web_icon),
                ADD CONSTRAINT FK_AADE868EA196242E FOREIGN KEY (web_icon) REFERENCES file (id),
                ADD COLUMN mobile_icon CHAR(36),
                ADD INDEX IDX_AADE868E87A63139 (mobile_icon),
                ADD CONSTRAINT FK_AADE868E87A63139 FOREIGN KEY (mobile_icon) REFERENCES file (id)
        ');

        $this->addSql('
            CREATE TABLE rate_plan_price (
                id CHAR(36) PRIMARY KEY,
                rate_plan_id CHAR(36) NOT NULL,
                room_id CHAR(36) NOT NULL,
                main_place_value INT UNSIGNED,
                hotel_extra_place_type_age_range_id CHAR(36),
                date DATE NOT NULL,
                value INT UNSIGNED,
                INDEX IDX_33C52D3E325B4D7A (rate_plan_id),
                INDEX IDX_33C52D3E54177093 (room_id),
                INDEX IDX_33C52D3E68A2C92D (hotel_extra_place_type_age_range_id),
                CONSTRAINT FK_33C52D3E325B4D7A FOREIGN KEY (rate_plan_id) REFERENCES rate_plan (id),
                CONSTRAINT FK_33C52D3E54177093 FOREIGN KEY (room_id) REFERENCES room (id),
                CONSTRAINT FK_33C52D3E68A2C92D FOREIGN KEY (hotel_extra_place_type_age_range_id) REFERENCES hotel_extra_place_type_age_range (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE hotel_extra_place_type_age_range;
            DROP TABLE room_main_place_variant;
            DROP TABLE hotel_extra_place_type;
            DROP TABLE hotel_age_range;
            DROP TABLE tax_type;
            DROP TABLE rate_plan_group;
            DROP TABLE rate_plan;
            DROP TABLE rate_plan_room;
            DROP TABLE rate_plan_price;
        ');

        $this->addSql('
            ALTER TABLE bed_type
                DROP CONSTRAINT FK_AADE868EA196242E,
                DROP CONSTRAINT FK_AADE868E87A63139,
                DROP INDEX IDX_AADE868EA196242E,
                DROP INDEX IDX_AADE868E87A63139,
                DROP COLUMN web_icon,
                DROP COLUMN mobile_icon,
        ');
    }
}
