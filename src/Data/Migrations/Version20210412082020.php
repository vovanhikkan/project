<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210412082020 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        
        $this->addSql('ALTER TABLE budget MODIFY budget_from INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE budget MODIFY budget_to INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE budget MODIFY sort INT UNSIGNED NOT NULL');
        
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE budget');

    }
}
