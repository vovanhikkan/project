<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210602101929 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_room
                ADD COLUMN bed_type_id CHAR(36) after room_id    
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                ADD COLUMN bed_type_id CHAR(36) after room_id    
        ');

        $this->addSql('ALTER TABLE cart_item_room 
            ADD INDEX IDX_605132FF8158330E (bed_type_id),            
            ADD CONSTRAINT FK_605132FF1AD5CDBF FOREIGN KEY (cart_id) REFERENCES cart (id),
            ADD CONSTRAINT FK_605132FF4584665A FOREIGN KEY (room_id) REFERENCES room (id),
            ADD CONSTRAINT FK_605132FF8158330E FOREIGN KEY (bed_type_id) REFERENCES bed_type (id);
        ');

        $this->addSql('ALTER TABLE order_item_room ADD INDEX IDX_6286E988158330E (bed_type_id);');
        $this->addSql('ALTER TABLE order_item_room ADD CONSTRAINT FK_6286E988D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_item_room ADD CONSTRAINT FK_6286E984584665A FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE order_item_room ADD CONSTRAINT FK_6286E988158330E FOREIGN KEY (bed_type_id) REFERENCES bed_type (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_room
                DROP CONSTRAINT FK_605132FF1AD5CDBF,
                DROP CONSTRAINT FK_605132FF4584665A,
                DROP CONSTRAINT FK_605132FF8158330E,
                DROP INDEX IDX_605132FF8158330E;
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP CONSTRAINT FK_6286E988D9F6D38,
                DROP CONSTRAINT FK_6286E984584665A,
                DROP CONSTRAINT FK_6286E988158330E,
                DROP INDEX IDX_6286E988158330E;
        ');

        $this->addSql('
            ALTER TABLE cart_item_room
                DROP COLUMN bed_type_id;
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP COLUMN bed_type_id;
        ');
    }
}
