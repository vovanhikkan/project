<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210519075156 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE rate_plan
                ADD COLUMN is_can_be_canceled SMALLINT UNSIGNED NOT NULL
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                ADD COLUMN is_can_be_canceled SMALLINT UNSIGNED NOT NULL
        ');

        $this->addSql('
            CREATE TABLE room_quota (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                is_active TINYINT(1) NOT NULL,
                sort INT UNSIGNED DEFAULT NULL
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE room_quota_rate_plan (
                rate_plan_id CHAR(36),
                quota_id CHAR(36),
                sort INT UNSIGNED DEFAULT NULL,
                PRIMARY KEY (rate_plan_id, quota_id),
                INDEX IDX_D6208DDC325B4D7A (rate_plan_id),
                INDEX IDX_D6208DDC54E2C62F (quota_id),
                CONSTRAINT FK_D6208DDC325B4D7A FOREIGN KEY (rate_plan_id) REFERENCES rate_plan (id),
                CONSTRAINT FK_D6208DDC54E2C62F FOREIGN KEY (quota_id) REFERENCES room_quota (id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE room_quota_value (
                id CHAR(36) PRIMARY KEY,
                room_quota_id CHAR(36),
                is_stopped_stop TINYINT NOT NULL,
                total_quantity INT NOT NULL,
                holded_quantity INT NOT NULL,
                ordered_quantity INT NOT NULL,
                canceled_quantity INT NOT NULL,
                date date NOT NULL,
                INDEX IDX_F0F25F79F40406EB (room_quota_id),
                CONSTRAINT FK_F0F25F79F40406EB FOREIGN KEY (room_quota_id) REFERENCES room_quota (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE rate_plan
                DROP COLUMN is_can_be_canceled;
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP COLUMN is_can_be_canceled;
        ');

        $this->addSql('
            DROP TABLE room_quota_rate_plan;
            DROP TABLE room_quota_value;
            DROP TABLE room_quota;
        ');


    }
}
