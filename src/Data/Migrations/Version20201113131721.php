<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201113131721 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                 ALTER TABLE `categories` DROP COLUMN web_gradient_color_1;
                 ALTER TABLE `categories` DROP COLUMN web_gradient_color_2;
                 ALTER TABLE `categories` ADD web_gradient_color_1 varchar(9);
                 ALTER TABLE `categories` ADD web_gradient_color_2 varchar(9);
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
