<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210608095556 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }


    public function up(Schema $schema) : void
    {

        $this->addSql('
            ALTER TABLE cart_item_room
                ADD COLUMN food_type_id CHAR(36) after rate_plan_id,
                ADD COLUMN can_be_canceled_date date after date_to,
                ADD COLUMN adult_count SMALLINT UNSIGNED after food_type_id,
                ADD COLUMN child_count SMALLINT UNSIGNED after adult_count
        ');
        $this->addSql('
            ALTER TABLE order_item_room
                ADD COLUMN can_be_canceled_date date after cash_item_id,
                ADD COLUMN food_type_id CHAR(36) after rate_plan_id,
                ADD COLUMN date_from date after activation_date,
                ADD COLUMN date_to date after date_from,
                ADD COLUMN adult_count SMALLINT UNSIGNED after food_type_id,
                ADD COLUMN child_count SMALLINT UNSIGNED after adult_count
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP COLUMN is_can_be_canceled;
        ');

        $this->addSql('ALTER TABLE cart_item_room 
            ADD INDEX IDX_605132FF8AD350AB (food_type_id),            
            ADD CONSTRAINT FK_605132FF8AD350AB FOREIGN KEY (food_type_id) REFERENCES food_type (id);
        ');

        $this->addSql('ALTER TABLE order_item_room 
            ADD INDEX IDX_6286E988AD350AB (food_type_id),            
            ADD CONSTRAINT FK_6286E988AD350AB FOREIGN KEY (food_type_id) REFERENCES food_type (id);
        ');
    }

    public function down(Schema $schema) : void
    {

        $this->addSql('
            ALTER TABLE cart_item_room
                DROP CONSTRAINT FK_605132FF8AD350AB,
                DROP INDEX IDX_605132FF8AD350AB;
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP CONSTRAINT FK_6286E988AD350AB,
                DROP INDEX IDX_6286E988AD350AB;
        ');

        $this->addSql('
            ALTER TABLE cart_item_room
                DROP COLUMN can_be_canceled_date,
                DROP COLUMN food_type_id,
                DROP COLUMN adult_count,
                DROP COLUMN child_count    
        ');

        $this->addSql('
            ALTER TABLE order_item_room
                DROP COLUMN can_be_canceled_date,
                DROP COLUMN food_type_id,
                DROP COLUMN adult_count,
                DROP COLUMN child_count,
                DROP COLUMN date_from,
                DROP COLUMN date_to
        ');


        $this->addSql('
            ALTER TABLE order_item_room
                ADD COLUMN is_can_be_canceled SMALLINT UNSIGNED NOT NULL AFTER cash_item_id    
        ');

    }
}
