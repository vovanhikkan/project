<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210203105244 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE product_settings (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                product_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                card_type SMALLINT(3) UNSIGNED NOT NULL,
                age VARCHAR(255) NOT NULL,
                amount_of_days VARCHAR(255) NOT NULL,
                rounds SMALLINT(3) UNSIGNED NOT NULL,
                pps_product_id INT(11) UNSIGNED NOT NULL,
                new_card_pps_product_id INT(11) UNSIGNED NOT NULL,
                old_card_pps_product_id INT(11) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                UNIQUE INDEX UNIQ_2EF5CA524584665A (product_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE product_settings ADD CONSTRAINT FK_2EF5CA524584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE product_settings');
    }
}
