<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520141100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                DROP COLUMN label_id,
                DROP CONSTRAINT FK_741D53CD33B92F39,
                DROP INDEX IDX_741D53CD33B92F39;
        ');
        $this->addSql('
            ALTER TABLE place
                ADD COLUMN label_id CHAR(36) AFTER age_restriction,
                ADD INDEX IDX_741D53CD33B92F39 (label_id),
                ADD CONSTRAINT FK_741D53CD33B92F39 FOREIGN KEY (label_id) REFERENCES label (id);                
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                DROP COLUMN label_id,
                DROP CONSTRAINT FK_741D53CD33B92F39,
                DROP INDEX IDX_741D53CD33B92F39;
        ');
        $this->addSql('
            ALTER TABLE place
                ADD COLUMN label_id CHAR(36) AFTER age_restriction,
                ADD INDEX IDX_741D53CD33B92F39 (label_id),
                ADD CONSTRAINT FK_741D53CD33B92F39 FOREIGN KEY (label_id) REFERENCES label_type (id);                
        ');
    }
}
