<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419155751 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE bundle_product (
                id CHAR(36) PRIMARY KEY,
                bundle_id CHAR(36),
                product_id CHAR(36),
                activation_date_shift INT DEFAULT 0,
                is_for_adult INT,
                is_for_children INT,
                child_age_from INT,
                child_age_to INT,
                quantity INT
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE bundle_product;
        ');
    }
}
