<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210416102035 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle 
                ADD COLUMN min_child_price INT UNSIGNED
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle 
                DROP COLUMN min_child_price
        ');
    }
}
