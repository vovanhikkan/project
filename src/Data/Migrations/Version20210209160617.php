<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210209160617 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE video (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                youtube_code VARCHAR(255) NOT NULL,
                background_image_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                UNIQUE INDEX UNIQ_7CC7DA2CE6DA28AA (background_image_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2CE6DA28AA FOREIGN KEY (background_image_id) REFERENCES file (id)');

        $this->addSql('
            CREATE TABLE place_block (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                place_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                title JSON NOT NULL, body JSON NOT NULL,
                photo_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\',
                video_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\',
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_2A6D8C01DA6A219 (place_id),
                UNIQUE INDEX UNIQ_2A6D8C017E9E4C8C (photo_id),
                UNIQUE INDEX UNIQ_2A6D8C0129C1004E (video_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE place_block ADD CONSTRAINT FK_2A6D8C01DA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE place_block ADD CONSTRAINT FK_2A6D8C017E9E4C8C FOREIGN KEY (photo_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE place_block ADD CONSTRAINT FK_2A6D8C0129C1004E FOREIGN KEY (video_id) REFERENCES video (id)');

        $this->addSql('
            CREATE TABLE place_geo_point (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                place_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                latitude DECIMAL(10, 8) UNSIGNED NOT NULL,
                longitude DECIMAL(11, 8) UNSIGNED NOT NULL,
                description JSON NOT NULL,
                sort INT(11) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_C33A5D76DA6A219 (place_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE place_geo_point ADD CONSTRAINT FK_C33A5D76DA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');

        $this->addSql('
            CREATE TABLE place_video (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                place_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                video_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                sort INT(11) UNSIGNED NOT NULL,
                INDEX IDX_D5B1C10FDA6A219 (place_id),
                UNIQUE INDEX UNIQ_D5B1C10F29C1004E (video_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE place_video ADD CONSTRAINT FK_D5B1C10FDA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE place_video ADD CONSTRAINT FK_D5B1C10F29C1004E FOREIGN KEY (video_id) REFERENCES video (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE place_block');
        $this->addSql('DROP TABLE place_geo_point');
        $this->addSql('DROP TABLE place_video');
        $this->addSql('DROP TABLE video');
    }
}
