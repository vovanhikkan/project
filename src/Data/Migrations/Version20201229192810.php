<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201229192810 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE tax_types (
            id int PRIMARY KEY AUTO_INCREMENT,
            value int,
            name json
        )');

        $this->addSql('ALTER TABLE rate_plans
            ADD COLUMN foodTypeId int,
            ADD COLUMN taxTypeId int,
            ADD COLUMN isActive bool,
            ADD COLUMN activeFrom date,
            ADD COLUMN activeTo date,
            ADD COLUMN shownFrom date,
            ADD COLUMN shownTo date,
            ADD COLUMN isShownWithPackets bool,
            ADD COLUMN isShownWithoutPackets bool,
            ADD FOREIGN KEY (foodTypeId) REFERENCES food_types (id),
            ADD FOREIGN KEY (taxTypeId) REFERENCES tax_types (id)
        ');

        $this->addSql('CREATE TABLE packets_rate_plans (
            id int PRIMARY KEY AUTO_INCREMENT,
            packetId int,
            ratePlanId int,
            FOREIGN KEY (packetId) REFERENCES packets (id),
            FOREIGN KEY (ratePlanId) REFERENCES rate_plans (id)
        )');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
