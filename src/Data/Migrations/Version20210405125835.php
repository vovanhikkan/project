<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210405125835 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE tag (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                icon CHAR(36),
                icon_svg CHAR(36),
                INDEX IDX_389B783659429DB (icon),
                INDEX IDX_389B783A012A554 (icon_svg),
                CONSTRAINT FK_389B783659429DB FOREIGN KEY (icon) REFERENCES file (id),
                CONSTRAINT FK_389B783A012A554 FOREIGN KEY (icon_svg) REFERENCES file (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE hotel_tag (
                hotel_id CHAR(36),
                tag_id CHAR(36),
                INDEX IDX_7B0304FD3243BB18 (hotel_id),
                INDEX IDX_7B0304FDBAD26311 (tag_id),
                CONSTRAINT FK_7B0304FD3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
                CONSTRAINT FK_7B0304FDBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id),
                PRIMARY KEY (hotel_id, tag_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE hotel_tag;
            DROP TABLE tag;
        ');
    }
}
