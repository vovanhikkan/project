<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210415100641 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE budget
                MODIFY COLUMN budget_from INT,
                MODIFY COLUMN budget_to INT
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE budget
                MODIFY COLUMN budget_from INT UNSIGNED,
                MODIFY COLUMN budget_to INT UNSIGNED
        ');
    }
}
