<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210106142711 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE payment (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                order_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                provider SMALLINT(3) UNSIGNED NOT NULL,
                external_id VARCHAR(255) NOT NULL,
                redirect_url VARCHAR(255) NOT NULL,
                status SMALLINT(3) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_6D28840D8D9F6D38 (order_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D8D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE payment');
    }
}
