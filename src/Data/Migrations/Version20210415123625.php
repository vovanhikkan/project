<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210415123625 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
            ADD COLUMN hotel_photo CHAR(36),
            ADD INDEX IDX_3535ED9F7634ADC (hotel_photo)
        ');
        
        $this->addSql('
        ALTER TABLE hotel ADD CONSTRAINT FK_3535ED9F7634ADC FOREIGN KEY (hotel_photo) REFERENCES file (id)
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
            DROP CONSTRAINT FK_3535ED9F7634ADC,
            DROP INDEX IDX_3535ED9F7634ADC,
            DROP COLUMN hotel_photo
        ');
    }
}
