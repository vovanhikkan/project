<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210416155125 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle
                DROP CONSTRAINT bundle_ibfk_1,
                DROP INDEX IDX_A57B32FD8AD350AB,
                DROP COLUMN food_type_id
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
