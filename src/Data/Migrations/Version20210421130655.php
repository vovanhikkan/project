<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210421130655 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE category
                DROP COLUMN description,
                DROP COLUMN has_sections,
                DROP COLUMN has_places,
                DROP CONSTRAINT FK_64C19C116D3E4EB,
                DROP CONSTRAINT FK_64C19C1B4131AC7,
                DROP CONSTRAINT FK_64C19C1BC1679AC,
                DROP CONSTRAINT UNIQ_64C19C116D3E4EB,
                DROP CONSTRAINT UNIQ_64C19C1B4131AC7,
                DROP CONSTRAINT UNIQ_64C19C1BC1679AC
        ');

        $this->addSql('
            ALTER TABLE category
                ADD CONSTRAINT FK_64C19C1BC1679AC FOREIGN KEY (web_icon_id) REFERENCES file (id),
                ADD CONSTRAINT FK_64C19C1B4131AC7 FOREIGN KEY (mobile_icon_id) REFERENCES file (id),
                ADD CONSTRAINT FK_64C19C116D3E4EB FOREIGN KEY (mobile_background_id) REFERENCES file (id)
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
