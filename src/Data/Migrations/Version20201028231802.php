<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028231802 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'create tables "storage"';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                CREATE TABLE IF NOT EXISTS  `storage` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `date` timestamp,
                    `width` int,
                    `height` int,
                    `content_type` varchar(255),
                    `subdir` varchar(255),
                    `filename` varchar(255),
                    `original_name` varchar(255)
                );
            SQL
        );

    }

    public function down(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                DROP TABLE `storage`;
            SQL
        );
    }
}
