<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520120640 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle
		        ADD shown_from DATETIME,
		        ADD shown_to DATETIME,
		        ADD first_booking_day_from DATE,
		        ADD first_booking_day_to DATE;
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle
		        DROP COLUMN shown_from,
		        DROP COLUMN shown_to,
		        DROP COLUMN first_booking_day_from,
		        DROP COLUMN first_booking_day_to;
        ');
    }
}
