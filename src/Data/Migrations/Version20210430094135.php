<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210430094135 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE cart_item_bundle (
		        id CHAR(36) NOT NULL,
		        cart_id CHAR(36) NOT NULL,
		        bundle_id CHAR(36) NOT NULL,
		        quantity SMALLINT(5) UNSIGNED NOT NULL,
		        activation_date DATE,
		        created_at DATETIME NOT NULL,
		        updated_at DATETIME DEFAULT NULL,
		        INDEX IDX_7051BE721AD5CDBF (cart_id),
		        INDEX IDX_7051BE72F1FAD9D3 (bundle_id),
		        PRIMARY KEY(id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('CREATE TABLE order_item_bundle (
		        id CHAR(36) NOT NULL,
		        order_id CHAR(36) NOT NULL,
		        bundle_id CHAR(36) NOT NULL,
		        price BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
		        quantity SMALLINT(5) UNSIGNED NOT NULL,
		        amount BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
                activation_date DATE,
                client_id INT(11) UNSIGNED DEFAULT NULL,
                cash_item_id INT(11) UNSIGNED DEFAULT NULL,
		        INDEX IDX_383038B48D9F6D38 (order_id),
		        INDEX IDX_383038B4F1FAD9D3 (bundle_id),
		        PRIMARY KEY(id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
