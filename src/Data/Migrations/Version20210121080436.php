<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121080436 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE brand (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                is_active TINYINT(1) UNSIGNED DEFAULT 1 NOT NULL
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            ALTER TABLE hotel
                ADD COLUMN hotel_brand_id CHAR(36),
                ADD INDEX IDX_3535ED9694027A (hotel_brand_id),
                ADD CONSTRAINT FK_3535ED9694027A FOREIGN KEY (hotel_brand_id) REFERENCES brand (id)
        ');
        $this->addSql('
            ALTER TABLE hotel
                ADD COLUMN food_type_id CHAR(36) NULL,
                ADD INDEX IDX_3535ED98AD350AB (food_type_id),
                ADD CONSTRAINT FK_3535ED98AD350AB FOREIGN KEY (food_type_id) REFERENCES food_type (id)
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
                DROP CONSTRAINT FK_3535ED9694027A,
                DROP INDEX IDX_3535ED9694027A,
                DROP COLUMN hotel_brand_id
        ');
        $this->addSql('
            ALTER TABLE hotel
                DROP CONSTRAINT FK_3535ED98AD350AB,
                DROP INDEX IDX_3535ED98AD350AB,
                DROP COLUMN food_type_id
        ');
        $this->addSql('
            DROP TABLE brand
        ');
    }
}
