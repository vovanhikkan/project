<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210518062659 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                RENAME COLUMN working_hours TO working_hours_description;
        ');

        $this->addSql('
            ALTER TABLE place
                ADD COLUMN works_from DATETIME AFTER working_hours_description,
                ADD COLUMN works_to DATETIME AFTER works_from,
                ADD COLUMN is_full_day SMALLINT UNSIGNED NOT NULL AFTER working_hours_description;
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                RENAME COLUMN working_hours_description TO working_hours;
        ');

        $this->addSql('
            ALTER TABLE place
                DROP COLUMN is_full_day,
                DROP COLUMN works_from,
                DROP COLUMN works_to;
        ');
        

    }
}
