<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324204057 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE bundle (
                id CHAR(36) PRIMARY KEY,
                code VARCHAR(255) NOT NULL,
                name JSON NOT NULL,
                description JSON NULL,
                day_count INT UNSIGNED,
                min_price INT UNSIGNED,
                profit INT UNSIGNED,
                food_type_id CHAR(36),
                is_with_accommodation TINYINT(1) UNSIGNED NOT NULL,
                sort INT UNSIGNED,
                web_background CHAR(36),
                is_active TINYINT(1) UNSIGNED,
                UNIQUE INDEX UNIQ_A57B32FD77153098 (code),
                INDEX IDX_A57B32FD8AD350AB (food_type_id),
                INDEX IDX_A57B32FD3D080E5C (web_background),
                FOREIGN KEY FK_A57B32FD8AD350AB (food_type_id) REFERENCES food_type (id),
                FOREIGN KEY FK_A57B32FD3D080E5C (web_background) REFERENCES file (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE bundle_item (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                sub_name JSON NOT NULL,
                description JSON NULL,
                sort INT UNSIGNED,
                is_active TINYINT(1) UNSIGNED,
                photo_id CHAR(36),
                place_id CHAR(36),
                INDEX IDX_236C3EDE7E9E4C8C (photo_id),
                INDEX IDX_236C3EDEDA6A219 (place_id),
                FOREIGN KEY FK_236C3EDE7E9E4C8C (photo_id) REFERENCES file (id),
                FOREIGN KEY FK_236C3EDEDA6A219 (place_id) REFERENCES place (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE bundle_rate_plan (
                bundle_id CHAR(36),
                rate_plan_id CHAR(36),
                PRIMARY KEY (bundle_id, rate_plan_id),
                INDEX IDX_6E1ED61DF1FAD9D3 (bundle_id),
                INDEX IDX_6E1ED61D325B4D7A (rate_plan_id),
                FOREIGN KEY FK_6E1ED61DF1FAD9D3 (bundle_id) REFERENCES bundle (id),
                FOREIGN KEY FK_6E1ED61D325B4D7A (rate_plan_id) REFERENCES rate_plan (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE bundle_bundle_item (
                bundle_id CHAR(36),
                bundle_item_id CHAR(36),
                sort INT UNSIGNED,
                PRIMARY KEY (bundle_id, bundle_item_id),
                INDEX IDX_776F28CBF1FAD9D3 (bundle_id),
                INDEX IDX_776F28CBE497EB27 (bundle_item_id),
                FOREIGN KEY FK_776F28CBF1FAD9D3 (bundle_id) REFERENCES bundle (id),
                FOREIGN KEY FK_776F28CBE497EB27 (bundle_item_id) REFERENCES bundle_item (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE bundle_rate_plan;
            DROP TABLE bundle_bundle_item;
            DROP TABLE bundle;
            DROP TABLE bundle_item;
        ');
    }
}
