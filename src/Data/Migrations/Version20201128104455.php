<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201128104455 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
                CREATE TABLE IF NOT EXISTS `categories_places`
                (
                    `id`          bigint PRIMARY KEY AUTO_INCREMENT,
                    `category_id` bigint DEFAULT NULL,
                    `place_id`  bigint DEFAULT NULL,
                    `sort`        int DEFAULT NULL,
                    FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
                    FOREIGN KEY (`place_id`) REFERENCES `places` (`id`)
                );
                
                RENAME TABLE `places_reviews` TO `reviews`;
            SQL
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
