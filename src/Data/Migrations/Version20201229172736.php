<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201229172736 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE rate_plans
            DROP COLUMN priceValue');

        $this->addSql('CREATE TABLE rate_plans_rooms (
            id int PRIMARY KEY AUTO_INCREMENT,
            ratePlanId int,
            roomId int,
            FOREIGN KEY (ratePlanId) REFERENCES rate_plans (id),
            FOREIGN KEY (roomId) REFERENCES rooms (id)
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
