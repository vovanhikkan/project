<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505100253 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
                DROP COLUMN rating
        ');

        $this->addSql('
            ALTER TABLE review
                MODIFY rating TINYINT UNSIGNED NULL
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
                ADD COLUMN rating JSON DEFAULT NULL
        ');

        $this->addSql('
            ALTER TABLE review
                MODIFY rating TINYINT UNSIGNED NOT NULL
        ');
    }
}
