<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210531094846 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE room_tag (
                room_id CHAR(36),
                tag_id CHAR(36),
                INDEX IDX_10F0A9E54177093 (room_id),
                INDEX IDX_10F0A9EBAD26311 (tag_id),
                CONSTRAINT FK_10F0A9E54177093 FOREIGN KEY (room_id) REFERENCES room (id),
                CONSTRAINT FK_10F0A9EBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id),
                PRIMARY KEY (room_id, tag_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE room_tag;
        ');

    }
}
