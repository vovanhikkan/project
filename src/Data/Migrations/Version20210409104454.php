<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210409104454 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE hotel
            ADD COLUMN check_in_time DATETIME COMMENT \'(DC2Type:datetime_immutable)\',
            ADD COLUMN check_out_time DATETIME COMMENT \'(DC2Type:datetime_immutable)\',
            ADD COLUMN cancel_prepayment JSON,
            ADD COLUMN extra_beds JSON,
            ADD COLUMN pets JSON,
            ADD COLUMN extra_info JSON,
            ADD COLUMN payments JSON
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
                DROP COLUMN check_in_time,
                DROP COLUMN check_out_time,
                DROP COLUMN cancel_prepayment,
                DROP COLUMN extra_beds,
                DROP COLUMN pets,
                DROP COLUMN extra_info,
                DROP COLUMN payments
        ');
    }
}
