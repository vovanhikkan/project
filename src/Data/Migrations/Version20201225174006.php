<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201225174006 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Квоты (на продукты)';
    }

    public function up(Schema $schema) : void
    {

        $this->addSql('CREATE TABLE IF NOT EXISTS quota_groups (
            id int PRIMARY KEY AUTO_INCREMENT,
            name varchar(255)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS quotas (
            id int PRIMARY KEY AUTO_INCREMENT,
            group_id int,
            name varchar(255),
            startDate date,
            endDate date,
            hasValuesByDate bool,
            FOREIGN KEY (group_id) REFERENCES quota_groups (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS quotas_products (
            id int PRIMARY KEY AUTO_INCREMENT,
            quota_id int,
            product_id int,
            FOREIGN KEY (quota_id) REFERENCES quotas (id),
            FOREIGN KEY (product_id) REFERENCES products (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS quota_values (
            id int PRIMARY KEY AUTO_INCREMENT,
            quota_id int,
            totalQuantity int,
            occupiedQuantity int,
            quotaDate date,
            FOREIGN KEY (quota_id) REFERENCES quotas (id)
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
