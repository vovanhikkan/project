<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210210145948 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE place ADD background_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\' AFTER width');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDC93D69EA FOREIGN KEY (background_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_741D53CDC93D69EA ON place (background_id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CDC93D69EA');
        $this->addSql('DROP INDEX UNIQ_741D53CDC93D69EA ON place');
        $this->addSql('ALTER TABLE place DROP background_id');
    }
}
