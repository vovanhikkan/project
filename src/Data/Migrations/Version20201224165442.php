<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201224165442 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {

        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS rooms_facilities (
            id int PRIMARY KEY AUTO_INCREMENT,
            room_id int,
            facility_id int,
            FOREIGN KEY (room_id) REFERENCES rooms (id),
            FOREIGN KEY (facility_id) REFERENCES facilities (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels_facilities (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            facility_id int,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id),
            FOREIGN KEY (facility_id) REFERENCES facilities (id)
        )');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
