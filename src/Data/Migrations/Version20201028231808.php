<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028231808 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create tables "sessions" and "settings"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
                 CREATE TABLE IF NOT EXISTS  `sessions` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `session` varchar(128),
                    `expires` int,
                    `data` blob,
                    `created` int
                 );
                
                CREATE TABLE IF NOT EXISTS  `settings` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `module` varchar(255),
                    `key` varchar(255),
                    `value` varchar(255),
                    `description` varchar(255)
                );
            SQL
        );

    }

    public function down(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
                DROP TABLE `sessions`;
                
                DROP TABLE `settings`;
            SQL
        );
    }
}
