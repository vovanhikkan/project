<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210120032456 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE place (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                title JSON NOT NULL,
                description JSON NOT NULL,
                sub_title JSON DEFAULT NULL,
                sub_description JSON DEFAULT NULL,
                working_hours JSON NOT NULL,
                age_restriction JSON DEFAULT NULL,
                label JSON DEFAULT NULL,
                address JSON DEFAULT NULL,
                phone VARCHAR(255) DEFAULT NULL,
                code VARCHAR(255) DEFAULT NULL,
                min_price BIGINT(20) DEFAULT NULL,
                is_free SMALLINT(1) UNSIGNED NOT NULL,
                is_cable_way_required SMALLINT(1) UNSIGNED NOT NULL,
                button_text JSON NOT NULL, width INT DEFAULT NULL,
                is_active SMALLINT(1) UNSIGNED NOT NULL,
                sort INT(11) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE place_section (
                place_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                section_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                INDEX IDX_19BF6D14DA6A219 (place_id),
                INDEX IDX_19BF6D14D823E37A (section_id),
                PRIMARY KEY(place_id, section_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE place_section ADD CONSTRAINT FK_19BF6D14DA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE place_section ADD CONSTRAINT FK_19BF6D14D823E37A FOREIGN KEY (section_id) REFERENCES section (id)');

        $this->addSql('
            CREATE TABLE place_photo (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                place_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                file_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                sort INT(11) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_BDC19F3BDA6A219 (place_id),
                INDEX IDX_BDC19F3B93CB796C (file_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE place_photo ADD CONSTRAINT FK_BDC19F3BDA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE place_photo ADD CONSTRAINT FK_BDC19F3B93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE place_section');
        $this->addSql('DROP TABLE place_photo');
        $this->addSql('DROP TABLE place');
    }
}
