<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201211185712 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('UPDATE places SET
        title = NULL,
        subtitle = NULL,
        description = NULL,
        summary = NULL,
        button_text = NULL');

        $this->addSql('ALTER TABLE places
        MODIFY title JSON DEFAULT NULL,
        MODIFY subtitle JSON DEFAULT NULL,
        MODIFY description JSON DEFAULT NULL,
        MODIFY summary JSON DEFAULT NULL,
        MODIFY button_text JSON DEFAULT NULL');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
