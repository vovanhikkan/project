<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201208215847 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('CREATE TABLE facilities (
          id INT AUTO_INCREMENT NOT NULL,
          facility_group_id INT NOT NULL,
          name JSON NOT NULL,
          is_active TINYINT(1) DEFAULT \'1\' NOT NULL,
          INDEX IDX_ADE885D5A85760C8 (facility_group_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE TABLE facility_group (
          id INT AUTO_INCREMENT NOT NULL,
          name JSON NOT NULL,
          is_active TINYINT(1) DEFAULT \'1\' NOT NULL,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('ALTER TABLE
          facilities
        ADD
          CONSTRAINT FK_ADE885D5A85760C8 FOREIGN KEY (facility_group_id) REFERENCES facility_group (id)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE facilities');
        $this->addSql('DROP TABLE facility_group');
    }
}
