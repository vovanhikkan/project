<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210528071928 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_bundle
                DROP INDEX IDX_7051BE72F1FAD9D3
        ');

        $this->addSql('
            ALTER TABLE cart_item_bundle
                RENAME COLUMN bundle_id TO bundle_product_id,
                ADD INDEX IDX_7051BE7298C55AAC (bundle_product_id),
                ADD CONSTRAINT FK_7051BE7298C55AAC FOREIGN KEY (bundle_product_id) REFERENCES bundle_product (id),
                ADD CONSTRAINT FK_7051BE721AD5CDBF FOREIGN KEY (cart_id) REFERENCES cart (id)
        ');

        $this->addSql('
            ALTER TABLE order_item_bundle
                DROP INDEX IDX_383038B4F1FAD9D3
        ');

        $this->addSql('
            ALTER TABLE order_item_bundle
                RENAME COLUMN bundle_id TO bundle_product_id,
                ADD INDEX IDX_383038B498C55AAC (bundle_product_id)
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_bundle
                DROP INDEX IDX_7051BE7298C55AAC,
                DROP CONSTRAINT FK_7051BE7298C55AAC,
                DROP CONSTRAINT FK_7051BE721AD5CDBF
        ');

        $this->addSql('
            ALTER TABLE cart_item_bundle
                RENAME COLUMN bundle_product_id TO bundle_id,
                ADD INDEX IDX_7051BE72F1FAD9D3 (bundle_id)
        ');

        $this->addSql('
            ALTER TABLE order_item_bundle
                DROP INDEX IDX_383038B498C55AAC
        ');

        $this->addSql('
            ALTER TABLE order_item_bundle
                RENAME COLUMN bundle_product_id TO bundle_id,
                ADD INDEX IDX_383038B4F1FAD9D3 (bundle_id)
        ');

    }
}
