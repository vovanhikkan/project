<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210305104309 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE order_item
                ADD COLUMN activation_date DATE AFTER amount
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE order_item
                DROP COLUMN activation_date
        ');
    }
}
