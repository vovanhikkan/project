<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201228094941 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS food_types (
            id int PRIMARY KEY AUTO_INCREMENT,
            name json,
            sort int
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS packets (
            id int PRIMARY KEY AUTO_INCREMENT,
            code varchar(255),
            title json,
            description json,
            dayCount json,
            minPrice int,
            profit int,
            foodTypeId int,
            isWithAccomodation bool,
            sort int,
            webBackground int,
            FOREIGN KEY (foodTypeId) REFERENCES food_types (id),
            FOREIGN KEY (webBackground) REFERENCES storage (id)
        )');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
