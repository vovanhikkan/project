<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210609062419 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE guest
                ADD COLUMN age SMALLINT after last_name,
                ADD COLUMN main_guest SMALLINT after age
        ');


        $this->addSql('CREATE TABLE cart_item_room_guest (
		        id CHAR(36) NOT NULL,
		        cart_id CHAR(36) NOT NULL,
		        room_id CHAR(36) NOT NULL,
		        guest_id CHAR(36) NOT NULL,
		        requirements JSON NULL,
		        created_at DATETIME NOT NULL,
		        updated_at DATETIME DEFAULT NULL,
		        INDEX IDX_22CC95641AD5CDBF (cart_id),
		        INDEX IDX_22CC956454177093 (room_id),
		        INDEX IDX_22CC95649A4AA658 (guest_id),
		        PRIMARY KEY(id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('ALTER TABLE cart_item_room_guest      
            ADD CONSTRAINT FK_22CC95641AD5CDBF FOREIGN KEY (cart_id) REFERENCES cart (id),
            ADD CONSTRAINT FK_22CC956454177093 FOREIGN KEY (room_id) REFERENCES room (id),
            ADD CONSTRAINT FK_22CC95649A4AA658 FOREIGN KEY (guest_id) REFERENCES guest (id)
        ');

        $this->addSql('CREATE TABLE order_item_room_guest (
		        id CHAR(36) NOT NULL,
		        order_id CHAR(36) NOT NULL,
		        room_id CHAR(36) NOT NULL,
		        guest_id CHAR(36) NOT NULL,
		        created_at DATETIME NOT NULL,
		        updated_at DATETIME DEFAULT NULL,
		        INDEX IDX_90B3D0C88D9F6D38 (order_id),
		        INDEX IDX_90B3D0C854177093 (room_id),
		        INDEX IDX_90B3D0C89A4AA658 (guest_id),
		        PRIMARY KEY(id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('ALTER TABLE order_item_room_guest      
            ADD CONSTRAINT FK_90B3D0C88D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id),
            ADD CONSTRAINT FK_90B3D0C854177093 FOREIGN KEY (room_id) REFERENCES room (id),
            ADD CONSTRAINT FK_90B3D0C89A4AA658 FOREIGN KEY (guest_id) REFERENCES guest (id)
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE guest
                DROP COLUMN age,
                DROP COLUMN main_guest
        ');

        $this->addSql('
            DROP TABLE cart_item_room_guest
        ');

        $this->addSql('
           DROP TABLE order_item_room_guest
        ');
    }
}
