<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201214085522 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE user_refresh_token (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                token_value VARCHAR(255) DEFAULT NULL,
                token_expired_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_29C18CC5A76ED395 (user_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE user_refresh_token ADD CONSTRAINT FK_29C18CC5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('
            ALTER TABLE user
            ADD reset_password_token_value VARCHAR(255) DEFAULT NULL AFTER status,
            ADD reset_password_token_expired_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\' AFTER reset_password_token_value
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE user
            DROP reset_password_token_value,
            DROP reset_password_token_expired_at
        ');

        $this->addSql('DROP TABLE user_refresh_token');
    }
}
