<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210226032446 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE product_price
            ADD is_active TINYINT(1) UNSIGNED NOT NULL AFTER date_end,
            ADD pps_period_id INT(11) UNSIGNED NOT NULL AFTER is_active
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE product_price DROP is_active, DROP pps_period_id');
    }
}
