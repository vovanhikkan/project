<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201211134420 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('ALTER TABLE facilities
        ADD icon INT DEFAULT NULL,
        ADD sort INT DEFAULT 1,
        ADD is_shown_in_room_list TINYINT(1) DEFAULT NULL');

        $this->addSql('ALTER TABLE facilities
        ADD CONSTRAINT FK_ADE885D5659429DB FOREIGN KEY (icon) REFERENCES storage (id)');

        $this->addSql('CREATE INDEX IDX_ADE885D5659429DB ON facilities (icon)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
