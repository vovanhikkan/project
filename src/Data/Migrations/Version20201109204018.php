<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201109204018 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
                 CREATE TABLE IF NOT EXISTS `baskets` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `order_id` int DEFAULT NULL,
                    `user_id` int DEFAULT NULL,
                    `base_price` float,
                    `discount` float,
                    `price` float,
                    `promocode` varchar(255),
                    `promocode_discount` float,
                    `given_points` int,
                    `given_points_discount` float,
                    `cost_to_pay` float,
                    `received_points` int
                );
                
                CREATE TABLE  IF NOT EXISTS `basket_items` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `basket_id` int,
                    `product_id` int,
                    `quantity` int,
                    `price_id` int,
                    `base_price` float DEFAULT NULL,
                    `disqount` float DEFAULT NULL,
                    `price` float DEFAULT NULL,
                    `settings` json
                );
                
                CREATE TABLE  IF NOT EXISTS `orders` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `status_id` int,
                    `first_name` varchar(255),
                    `last_name` varchar(255),
                    `email` varchar(255),
                    `phone` varchar(30),
                    `created_at` datetime,
                    `updated_at` datetime,
                    `comment` text,
                    `manager_comment` text,
                    `pps_settings` json,
                    `travelline_settings` json
                );
            SQL
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
