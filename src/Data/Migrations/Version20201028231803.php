<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028231803 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'create tables "pay_systems", and "invoice"';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                CREATE TABLE IF NOT EXISTS  `pay_systems` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `name` varchar(50),
                    `code` varchar(50),
                    `type` varchar(50),
                    `storage_id` int,
                    `description` varchar(255),
                    `hint` varchar(255),
                    `parent` int,
                    FOREIGN KEY (`storage_id`) REFERENCES `storage` (`id`)
                );
                CREATE TABLE IF NOT EXISTS  `invoice` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `order_id` int,
                    `pay_system_id` int,
                    `amount` float,
                    `currency` varchar(50),
                    `type` varchar(50),
                    `comment` varchar(255),
                    `transaction_id` varchar(100),
                    `status` varchar(50),
                    `bank_responce` varchar(255),
                    FOREIGN KEY (`pay_system_id`) REFERENCES `pay_systems` (`id`)
                );
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                DROP TABLE `pay_systems`;   
                DROP TABLE `invoice`;
            SQL
        );
    }
}
