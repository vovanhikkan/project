<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419081820 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE hotel MODIFY check_in_time JSON');
        $this->addSql('ALTER TABLE hotel MODIFY check_out_time JSON');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
                DROP COLUMN check_in_time,
                DROP COLUMN check_out_time
        ');

        $this->addSql('ALTER TABLE hotel
            ADD COLUMN check_in_time DATETIME COMMENT \'(DC2Type:datetime_immutable)\' AFTER food_type_id,
            ADD COLUMN check_out_time DATETIME COMMENT \'(DC2Type:datetime_immutable)\' AFTER food_type_id
            ');

    }
}
