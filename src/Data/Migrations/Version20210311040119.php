<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210311040119 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE order_item ADD card_num VARCHAR(255) DEFAULT NULL AFTER birth_date');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE order_item DROP card_num');
    }
}
