<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use App\Application\ValueObject\Uuid;
use App\Product\Model\CardType\Type;
use DateTimeImmutable;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210303142821 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE card_type (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                type SMALLINT(3) UNSIGNED NOT NULL,
                price BIGINT(20) UNSIGNED DEFAULT NULL,
                pps_product_id INT(11) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql(
            'INSERT INTO card_type VALUES (?, ?, ?, ?, ?, ?)',
            [
                (string)Uuid::generate(),
                Type::NONE,
                null,
                23554129,
                (new DateTimeImmutable())->format('Y-m-d H:i:s'),
                null
            ]
        );

        $this->addSql(
            'INSERT INTO card_type VALUES (?, ?, ?, ?, ?, ?)',
            [
                (string)Uuid::generate(),
                Type::PLASTIC,
                null,
                23554125,
                (new DateTimeImmutable())->format('Y-m-d H:i:s'),
                null
            ]
        );

        $this->addSql(
            'INSERT INTO card_type VALUES (?, ?, ?, ?, ?, ?)',
            [
                (string)Uuid::generate(),
                Type::PLASTIC_FREE,
                null,
                25297694,
                (new DateTimeImmutable())->format('Y-m-d H:i:s'),
                null
            ]
        );
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE card_type');
    }
}
