<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210216055358 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE user
            ADD is_email_verified SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER is_confirmed_mailing,
            ADD verify_email_token_value VARCHAR(255) DEFAULT NULL AFTER is_email_verified,
            ADD verify_email_token_expired_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\' AFTER verify_email_token_value,
            ADD is_phone_verified SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER verify_email_token_expired_at,
            ADD verify_phone_token_value VARCHAR(255) DEFAULT NULL AFTER is_phone_verified,
            ADD verify_phone_token_expired_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\' AFTER verify_phone_token_value
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE user
            DROP is_email_verified,
            DROP is_phone_verified,
            DROP verify_email_token_value,
            DROP verify_email_token_expired_at,
            DROP verify_phone_token_value,
            DROP verify_phone_token_expired_at
        ');
    }
}
