<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201228060256 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE section (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                code VARCHAR(255) NOT NULL,
                name VARCHAR(255) NOT NULL,
                description LONGTEXT NOT NULL,
                tariff_description LONGTEXT NOT NULL,
                inner_title VARCHAR(255) NOT NULL,
                inner_description LONGTEXT NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE product (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                section_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                name VARCHAR(255) NOT NULL,
                price BIGINT(20) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_D34A04ADD823E37A (section_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADD823E37A FOREIGN KEY (section_id) REFERENCES section (id)');

        $this->addSql('
            CREATE TABLE product_price (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                product_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                price BIGINT(20) UNSIGNED NOT NULL,
                date_start DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                date_end DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_6B9459854584665A (product_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE product_price ADD CONSTRAINT FK_6B9459854584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE section');
        $this->addSql('DROP TABLE product_price');
        $this->addSql('DROP TABLE product');
    }
}
