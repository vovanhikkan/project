<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210504140728 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE hotel_service (
                id CHAR(36) PRIMARY KEY,
                hotel_id CHAR(36) NOT NULL,
                is_active TINYINT(1) DEFAULT 1 NOT NULL,
                type SMALLINT UNSIGNED NOT NULL,
                name JSON NOT NULL,
                description JSON NOT NULL,
                photo_id CHAR(36),
                price_type SMALLINT UNSIGNED NOT NULL,
                is_available_by_days TINYINT(1) UNSIGNED NOT NULL,
                days SMALLINT UNSIGNED NOT NULL,
                is_price_not_depend TINYINT(1) UNSIGNED NOT NULL,
                is_quota_not_depend TINYINT(1) UNSIGNED NOT NULL,
                INDEX IDX_32D784FB3243BB18 (hotel_id),
                INDEX IDX_32D784FB7E9E4C8C (photo_id),
                CONSTRAINT FK_32D784FB3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
                CONSTRAINT FK_32D784FB7E9E4C8C FOREIGN KEY (photo_id) REFERENCES file (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_service_rate_plan (
                hotel_service_id CHAR(36),
                rate_plan_id CHAR(36),
                INDEX IDX_8FF3A53C9888430B (hotel_service_id),
                INDEX IDX_8FF3A53C325B4D7A (rate_plan_id),
                CONSTRAINT FK_8FF3A53C9888430B FOREIGN KEY (hotel_service_id) REFERENCES hotel_service (id),
                CONSTRAINT FK_8FF3A53C325B4D7A FOREIGN KEY (rate_plan_id) REFERENCES rate_plan (id),
                PRIMARY KEY (hotel_service_id, rate_plan_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_service_price (
                id CHAR(36) PRIMARY KEY,
                hotel_service_id CHAR(36),
                date DATE,
                value BIGINT UNSIGNED,
                INDEX IDX_E7AF81139888430B (hotel_service_id),
                CONSTRAINT FK_E7AF81139888430B FOREIGN KEY (hotel_service_id) REFERENCES hotel_service (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_service_quota (
                id CHAR(36) PRIMARY KEY,
                hotel_service_id CHAR(36),
                date DATE,
                holded_quantity BIGINT UNSIGNED,
                ordered_quantity BIGINT UNSIGNED,
                INDEX IDX_417BAC279888430B (hotel_service_id),
                CONSTRAINT FK_417BAC279888430B FOREIGN KEY (hotel_service_id) REFERENCES hotel_service (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE hotel_service_quota;
            DROP TABLE hotel_service_price;
            DROP TABLE hotel_service_rate_plan;
            DROP TABLE hotel_service;
        ');
    }
}
