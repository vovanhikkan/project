<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210106135631 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE `order` (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                amount BIGINT(20) UNSIGNED NOT NULL,
                status SMALLINT(3) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_F5299398A76ED395 (user_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('
            CREATE TABLE order_item (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                order_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                product_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                price BIGINT(20) UNSIGNED NOT NULL,
                quantity SMALLINT(5) UNSIGNED NOT NULL,
                amount BIGINT(20) UNSIGNED NOT NULL,
                INDEX IDX_52EA1F098D9F6D38 (order_id),
                INDEX IDX_52EA1F094584665A (product_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F098D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F094584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE order_item');
        $this->addSql('DROP TABLE `order`');
    }
}
