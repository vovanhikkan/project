<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201209062146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Поля price и base_price вносят путанницу в таблице price,
        поэтому переименовываются в value и base_value';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('ALTER TABLE prices
        RENAME COLUMN price TO value');

        $this->addSql('ALTER TABLE prices
        RENAME COLUMN base_price TO base_value');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->addSql('ALTER TABLE prices
        RENAME COLUMN value TO price');

        $this->addSql('ALTER TABLE prices
        RENAME COLUMN base_value TO base_price');

    }
}
