<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028231709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create tables "roles", "users", "users_properties" and "refresh_token"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
                CREATE TABLE IF NOT EXISTS  `roles` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `code` varchar(32),
                    `name` varchar(32)
                );
                CREATE TABLE IF NOT EXISTS  `users` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `email` varchar(50),
                    `password` varchar(128),
                    `active` boolean,
                    `firstName` varchar(128),
                    `middleName` varchar(128),
                    `lastName` varchar(128),
                    `phone` varchar(128),
                    `role_id` int,
                    `balance` float,
                    `resetHash` varchar(255),
                    FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
                );
                CREATE TABLE IF NOT EXISTS  `users_properties` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `uid` int,
                    `upd_subscr_queue` boolean,
                    `touragent` boolean,
                    `touragent_contract` int,
                    `was_confirmed` boolean,
                    `im_search` varchar(255),
                    FOREIGN KEY (`uid`) REFERENCES `users` (`id`)
                );
                CREATE TABLE IF NOT EXISTS `refresh_token` (
                    `id` int PRIMARY KEY AUTO_INCREMENT,
                    `uid` int,
                    `refreshToken` varchar(68) NOT NULL,
                    `ua` varchar(255) NOT NULL,
                    `ip` binary(16) NOT NULL,
                    `created_at` timestamp NOT NULL,
                    FOREIGN KEY (`uid`) REFERENCES `users` (`id`)
                );
            SQL
        );
    }

    public function down(Schema $schema) : void
    {

        $this->addSql(<<<'SQL'
                DROP TABLE `roles`;
                DROP TABLE `users`;
                DROP TABLE `users_properties`;
                DROP TABLE `refresh_token`;
            SQL
        );
    }
}
