<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201211162708 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('ALTER TABLE facilities
        RENAME COLUMN icon TO web_icon_svg');

        $this->addSql('ALTER TABLE facilities
        ADD mobile_icon INT DEFAULT NULL');

        $this->addSql('ALTER TABLE facilities
        ADD CONSTRAINT FK_ADE885D587A63139 FOREIGN KEY (mobile_icon) REFERENCES storage (id)');

        $this->addSql('ALTER TABLE facility_groups
        ADD web_icon_svg INT DEFAULT NULL,
        ADD mobile_icon INT DEFAULT NULL');

        $this->addSql('ALTER TABLE facility_groups
        ADD CONSTRAINT FK_5D592E0AB74DB4B9 FOREIGN KEY (web_icon_svg) REFERENCES storage (id)');

        $this->addSql('ALTER TABLE facility_groups
        ADD CONSTRAINT FK_5D592E0A87A63139 FOREIGN KEY (mobile_icon) REFERENCES storage (id)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
