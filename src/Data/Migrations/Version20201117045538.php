<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201117045538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                 ALTER TABLE `users` ADD birthday date;
                 ALTER TABLE `users` ADD mailing boolean DEFAULT false;
                 ALTER TABLE `users` ADD service_letters boolean DEFAULT false;
                 ALTER TABLE `users` ADD sex varchar(50);
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
