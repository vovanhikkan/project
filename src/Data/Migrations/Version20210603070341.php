<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210603070341 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE rate_plan
		        ADD extra_condition JSON null after description;
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE rate_plan
		        DROP COLUMN extra_condition;
        ');
    }
}
