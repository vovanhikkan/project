<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210212025954 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE pay_system (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                name JSON NOT NULL,
                provider SMALLINT(3) UNSIGNED NOT NULL,
                description JSON DEFAULT NULL,
                code VARCHAR(255) DEFAULT NULL,
                image_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\',
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                UNIQUE INDEX UNIQ_40D6DBC43DA5256D (image_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE pay_system ADD CONSTRAINT FK_40D6DBC43DA5256D FOREIGN KEY (image_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE payment ADD pay_system_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\' AFTER order_id, DROP provider');
    }

    public function down(Schema $schema) : void
    {
        $this->throwIrreversibleMigrationException();
    }
}
