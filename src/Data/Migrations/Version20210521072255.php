<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210521072255 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_room
                ADD COLUMN date_from date after activation_date,
                ADD COLUMN date_to date after date_from
        ');


    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE cart_item_room
                DROP COLUMN date_from,
                DROP COLUMN date_to
        ');

    }
}
