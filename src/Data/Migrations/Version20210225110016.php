<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225110016 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE category
                ADD COLUMN has_sections SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0,
                ADD COLUMN has_places SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0,
                MODIFY COLUMN name JSON NOT NULL,
                MODIFY COLUMN description JSON NOT NULL,
                ADD COLUMN is_active SMALLINT(1) UNSIGNED NOT NULL DEFAULT 1
        ');
        $this->addSql('DROP TABLE IF EXISTS categories_sections');
        $this->addSql('DROP TABLE IF EXISTS categories_places');

        $this->addSql('
            ALTER TABLE places
                DROP CONSTRAINT places_ibfk_1
        ');

        $this->addSql('DROP TABLE IF EXISTS categories');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE category
                DROP COLUMN has_sections,
                DROP COLUMN has_places,
                MODIFY COLUMN name VARCHAR(255) NOT NULL,
                MODIFY COLUMN description VARCHAR(255) NOT NULL,
                DROP COLUMN is_active
        ');
    }
}
