<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419133511 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE hotel
                DROP CONSTRAINT FK_3535ED98AD350AB,
                DROP INDEX IDX_3535ED98AD350AB
        ');
        
        $this->addSql('
            ALTER TABLE hotel DROP COLUMN food_type_id
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE hotel
                ADD COLUMN food_type_id CHAR(36) NULL AFTER hotel_brand_id,
                ADD INDEX IDX_3535ED98AD350AB (food_type_id)
            ');

        $this->addSql('
            ALTER TABLE hotel ADD CONSTRAINT FK_3535ED98AD350AB FOREIGN KEY (food_type_id) REFERENCES food_type (id)
        ');
    }
}
