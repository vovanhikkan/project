<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201218121225 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('UPDATE places SET working_hours = concat(\'{"ru": "\', working_hours, \'", "en": ""}\')');

        $this->addSql('ALTER TABLE places
        MODIFY COLUMN working_hours JSON DEFAULT NULL');

        $this->addSql('ALTER TABLE places
        ADD COLUMN address JSON DEFAULT NULL');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
