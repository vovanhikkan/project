<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201226154254 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS rate_plan_groups (
            id int PRIMARY KEY AUTO_INCREMENT,
            name json,
            sort int
        )');

        $this->addSql('ALTER TABLE rate_plans
        ADD COLUMN priceValue int,
        ADD COLUMN groupId int,
        ADD FOREIGN KEY (groupId) REFERENCES rate_plan_groups (id)
        ');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
