<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324120305 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE bed_type (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_bed_type (
                hotel_id CHAR(36),
                bed_type_id CHAR(36),
                INDEX IDX_E8BF54FC3243BB18 (hotel_id), 
                INDEX IDX_E8BF54FC8158330E (bed_type_id), 
                CONSTRAINT FK_E8BF54FC3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
                CONSTRAINT FK_E8BF54FC8158330E FOREIGN KEY (bed_type_id) REFERENCES bed_type (id), 
                PRIMARY KEY (hotel_id, bed_type_id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE hotel_bed_type;
        ');
        $this->addSql('
            DROP TABLE bed_type;
        ');
    }
}
