<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210604075636 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('
            ALTER TABLE rate_plan
                DROP COLUMN is_can_be_canceled    
        ');

        $this->addSql('
            ALTER TABLE rate_plan
                ADD COLUMN can_be_canceled_date DATE NULL AFTER is_shown_with_packets    
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE rate_plan
                DROP COLUMN can_be_canceled_date    
        ');

        $this->addSql('
            ALTER TABLE rate_plan
                ADD COLUMN is_can_be_canceled SMALLINT UNSIGNED NOT NULL AFTER is_shown_with_packets    
        ');
    }
}
