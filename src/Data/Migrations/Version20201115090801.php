<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201115090801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
                 ALTER TABLE `orders` ADD user_id int;
                 
                 ALTER TABLE `categories` ADD has_sections boolean;
                 ALTER TABLE `categories` ADD has_places boolean;
                 
                 CREATE TABLE IF NOT EXISTS `locations` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `is_active` boolean,
                    `name` varchar(255) DEFAULT NULL,
                    `sort` int DEFAULT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `places` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `title` varchar(255) DEFAULT NULL,
                    `summary` text DEFAULT NULL,
                    `subtitle` varchar(255) DEFAULT NULL,
                    `description` text DEFAULT NULL,
                    `background` int DEFAULT NULL,
                    `working_hours` varchar(255) DEFAULT NULL,
                    `age_restriction` varchar(255) DEFAULT NULL,
                    `phone` varchar(30) DEFAULT NULL,
                    `category_id` bigint DEFAULT NULL,
                    `is_active` boolean,
                    `code` varchar(30) DEFAULT NULL,
                    `sort` int DEFAULT 1,
                    `location_id` bigint DEFAULT NULL,
                    `is_free` boolean,
                    `is_cableway_required` boolean,
                    `rating` json DEFAULT NULL,
                    `button_text` varchar(255) DEFAULT NULL,
                    `width` int DEFAULT NULL,
                    FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
                    FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
                    FOREIGN KEY (`background`) REFERENCES `storage` (`id`)
                );
                
                CREATE TABLE IF NOT EXISTS `places_photos` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `place_id` bigint DEFAULT NULL,
                    `photo` int  DEFAULT NULL,
                    `sort` int DEFAULT NULL,
                    FOREIGN KEY (`place_id`) REFERENCES `places` (`id`),
                    FOREIGN KEY (`photo`) REFERENCES `storage` (`id`)
                );
                
                CREATE TABLE IF NOT EXISTS `places_blocks` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `place_id` bigint DEFAULT NULL,
                    `title` varchar(255) DEFAULT NULL,
                    `body` text DEFAULT NULL,
                    `params` json DEFAULT NULL,
                    FOREIGN KEY (`place_id`) REFERENCES `places` (`id`)
                );
                
                CREATE TABLE IF NOT EXISTS `places_sections` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `place_id` bigint DEFAULT NULL,
                    `section_id` int  DEFAULT NULL,
                    `sort` int DEFAULT NULL,
                    FOREIGN KEY (`place_id`) REFERENCES `places` (`id`),
                    FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`)
                );
                
                CREATE TABLE IF NOT EXISTS `places_geo_points` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `lat` decimal(9, 6) DEFAULT NULL,
                    `lon` decimal(9, 6) DEFAULT NULL,
                    `description` text DEFAULT NULL,
                    `sort` int DEFAULT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `places_reviews` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `user_id` int DEFAULT NULL,
                    `place_id` bigint DEFAULT NULL,
                    `body` text DEFAULT NULL,
                    `rating` int DEFAULT NULL,
                    `created_at` date DEFAULT NULL,
                    FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
                    FOREIGN KEY (`place_id`) REFERENCES `places` (`id`)
                );
                
                CREATE TABLE IF NOT EXISTS `places_videos` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `place_id` bigint DEFAULT NULL,
                    `youtube_code` varchar(50) DEFAULT NULL,
                    `background` int DEFAULT NULL,
                    `sort` int DEFAULT NULL,
                    FOREIGN KEY (`background`) REFERENCES `storage` (`id`),
                    FOREIGN KEY (`place_id`) REFERENCES `places` (`id`)
                );
            SQL
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
