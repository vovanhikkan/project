<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210416134526 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE actions_rate_plans;
            DROP TABLE rate_plans_rooms;
            DROP TABLE rate_plans;
            DROP TABLE rate_plan_groups;
            DROP TABLE tax_types;
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
