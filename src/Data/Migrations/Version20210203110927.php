<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210203110927 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE section_settings (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                section_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                controls JSON NOT NULL,
                is_online_only TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
                is_activation_date_required TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
                active_days SMALLINT(3) UNSIGNED NOT NULL,
                is_not_quantitative TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                UNIQUE INDEX UNIQ_7528FA99D823E37A (section_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE section_settings ADD CONSTRAINT FK_7528FA99D823E37A FOREIGN KEY (section_id) REFERENCES section (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE section_settings');
    }
}
