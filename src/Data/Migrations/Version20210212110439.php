<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210212110439 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE category_place (
                category_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                place_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                sort INT(11) UNSIGNED NOT NULL,
                INDEX IDX_9C2E1C8412469DE2 (category_id),
                INDEX IDX_9C2E1C84DA6A219 (place_id),
                PRIMARY KEY(category_id, place_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE category_place ADD CONSTRAINT FK_9C2E1C8412469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category_place ADD CONSTRAINT FK_9C2E1C84DA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');

        $this->addSql('
            CREATE TABLE category_section (
                category_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                section_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                sort INT(11) UNSIGNED NOT NULL,
                INDEX IDX_EAAB3A9112469DE2 (category_id),
                INDEX IDX_EAAB3A91D823E37A (section_id),
                PRIMARY KEY(category_id, section_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE category_section ADD CONSTRAINT FK_EAAB3A9112469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category_section ADD CONSTRAINT FK_EAAB3A91D823E37A FOREIGN KEY (section_id) REFERENCES section (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE category_place');
        $this->addSql('DROP TABLE category_section');
    }
}
