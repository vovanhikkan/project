<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210218073044 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE category
            ADD web_gradient_color1 VARCHAR(255) DEFAULT NULL AFTER description,
            ADD web_gradient_color2 VARCHAR(255) DEFAULT NULL AFTER web_gradient_color1,
            ADD web_icon_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\' AFTER web_gradient_color2,
            ADD web_icon_svg LONGTEXT DEFAULT NULL AFTER web_icon_id,
            ADD mobile_icon_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\' AFTER web_icon_svg,
            ADD mobile_background_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\' AFTER mobile_icon_id
        ');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1BC1679AC FOREIGN KEY (web_icon_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1B4131AC7 FOREIGN KEY (mobile_icon_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C116D3E4EB FOREIGN KEY (mobile_background_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C1BC1679AC ON category (web_icon_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C1B4131AC7 ON category (mobile_icon_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C116D3E4EB ON category (mobile_background_id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1BC1679AC');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1B4131AC7');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C116D3E4EB');
        $this->addSql('DROP INDEX UNIQ_64C19C1BC1679AC ON category');
        $this->addSql('DROP INDEX UNIQ_64C19C1B4131AC7 ON category');
        $this->addSql('DROP INDEX UNIQ_64C19C116D3E4EB ON category');
        $this->addSql('ALTER TABLE category DROP web_icon_id, DROP web_icon_svg, DROP mobile_icon_id, DROP mobile_background_id, DROP web_gradient_color1, DROP web_gradient_color2');
    }
}
