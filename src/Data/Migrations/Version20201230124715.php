<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201230124715 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('RENAME TABLE hotels_services TO hotel_services');

        $this->addSql('ALTER TABLE hotel_services
            DROP COLUMN priceValue,
            ADD COLUMN photo int,
            ADD COLUMN name json,
            ADD COLUMN hasPriceValuesByDate bool,
            ADD COLUMN hasQuotaValuesByDate bool,
            ADD FOREIGN KEY (photo) REFERENCES storage (id)
        ');

        $this->addSql('CREATE TABLE hotel_service_quota_values(
            id int PRIMARY KEY AUTO_INCREMENT,
            hotelServiceId int,
            totalQuantity int,
            occupiedQuantity int,
            quotaDate date,
            FOREIGN KEY (hotelServiceId) REFERENCES hotel_services (id)
        )');

        $this->addSql('CREATE TABLE hotel_service_price_values(
            id int PRIMARY KEY AUTO_INCREMENT,
            hotelServiceId int,
            value int,
            priceDate date,
            FOREIGN KEY (hotelServiceId) REFERENCES hotel_services (id)
        )');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
