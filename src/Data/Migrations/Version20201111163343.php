<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201111163343 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
                 ALTER TABLE `sections` ADD web_icon int DEFAULT NULL;
                 ALTER TABLE `sections` ADD web_background int DEFAULT NULL;
                 ALTER TABLE `sections` ADD mobile_icon int DEFAULT NULL;
                 ALTER TABLE `sections` ADD mobile_background int DEFAULT NULL;
                 ALTER TABLE `sections` ADD FOREIGN KEY (`web_icon`) REFERENCES `storage` (`id`);
                 ALTER TABLE `sections` ADD FOREIGN KEY (`web_background`) REFERENCES `storage` (`id`);
                 ALTER TABLE `sections` ADD FOREIGN KEY (`mobile_icon`) REFERENCES `storage` (`id`);
                 ALTER TABLE `sections` ADD FOREIGN KEY (`mobile_background`) REFERENCES `storage` (`id`);
                 
                 CREATE TABLE IF NOT EXISTS `categories` (
                    `id` bigint PRIMARY KEY AUTO_INCREMENT,
                    `title` varchar(255) DEFAULT NULL,
                    `description` text DEFAULT NULL,
                    `web_icon` int DEFAULT NULL,
                    `web_gradient_color_1` int DEFAULT NULL,
                    `web_gradient_color_2` int DEFAULT NULL,
                    `mobile_icon` int DEFAULT NULL,
                    `mobile_background` int DEFAULT NULL,
                    FOREIGN KEY (`web_icon`) REFERENCES `storage` (`id`),
                    FOREIGN KEY (`mobile_icon`) REFERENCES `storage` (`id`),
                    FOREIGN KEY (`mobile_background`) REFERENCES `storage` (`id`)
                );
                
                CREATE TABLE IF NOT EXISTS `categories_sections`
                (
                    `id`          bigint PRIMARY KEY AUTO_INCREMENT,
                    `category_id` bigint DEFAULT NULL,
                    `section_id`  int DEFAULT NULL,
                    `sort`        int DEFAULT NULL,
                    FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
                    FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`)
                );
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
