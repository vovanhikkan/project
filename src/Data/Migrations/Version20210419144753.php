<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419144753 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                MODIFY width SMALLINT UNSIGNED
        ');
        $this->addSql('
            UPDATE place
                SET width = null WHERE width IS NOT NULL
        ');
    }

    public function down(Schema $schema) : void
    {
    }
}
