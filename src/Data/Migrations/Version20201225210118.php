<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201225210118 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        // Hotel Services - услуги отелей

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels_services (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            priceType int,
            priceValue int,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id)
        )');

        // Возрастной диапазон

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels_age_ranges (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            min_age int,
            max_age int,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id)
        )');

        // Типы дополнительных мест - "детская кровать", "раскладушка", ...

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels_extra_place_types (
            id int PRIMARY KEY AUTO_INCREMENT,
            hotel_id int,
            name JSON,
            FOREIGN KEY (hotel_id) REFERENCES hotels (id)
        )');

        // Связь возрастных диапазонов и типов дополнительных мест
        //

        $this->addSql('CREATE TABLE IF NOT EXISTS hotels_age_ranges_extra_place_types (
            id int PRIMARY KEY AUTO_INCREMENT,
            age_range_id int,
            extra_place_type_id int,
            FOREIGN KEY (age_range_id) REFERENCES hotels_age_ranges (id),
            FOREIGN KEY (extra_place_type_id) REFERENCES hotels_extra_place_types (id)
        )');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
