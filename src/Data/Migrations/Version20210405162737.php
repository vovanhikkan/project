<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210405162737 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE hotel_place (
                hotel_id CHAR(36),
                place_id CHAR(36),
                INDEX IDX_97C99D093243BB18 (hotel_id),
                INDEX IDX_97C99D09DA6A219 (place_id),
                PRIMARY KEY(hotel_id, place_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE hotel_place ADD CONSTRAINT FK_97C99D093243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id)');
        $this->addSql('ALTER TABLE hotel_place ADD CONSTRAINT FK_97C99D09DA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE hotel_place');
    }
}
