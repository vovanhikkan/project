<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210220125341 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE payment
            ADD success_url VARCHAR(255) DEFAULT NULL AFTER pay_system_id
        ');

        $this->addSql('UPDATE payment SET success_url = ""');

        $this->addSql('
            ALTER TABLE payment
            CHANGE success_url success_url VARCHAR(255) NOT NULL
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE payment DROP success_url');
    }
}
