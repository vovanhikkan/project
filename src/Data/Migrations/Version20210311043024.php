<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210311043024 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `order` ADD barcode VARCHAR(255) DEFAULT NULL AFTER cash_list_id');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `order` DROP barcode');
    }
}
