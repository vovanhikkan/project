<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210423081447 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE review
            ADD COLUMN hotel_id CHAR(36) AFTER place_id,
            ADD INDEX IDX_794381C63243BB18 (hotel_id),
            ADD CONSTRAINT FK_794381C63243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id)
        ');

        $this->addSql(
            'CREATE TABLE review_star_type (
            id CHAR(36) PRIMARY KEY,
            name JSON NOT NULL
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        '
        );

        $this->addSql(
            'CREATE TABLE review_star (
            id CHAR(36) PRIMARY KEY,
            review_id CHAR(36) NOT NULL,
            review_star_type_id CHAR(36) NOT NULL ,
            value int NULL,
            INDEX IDX_2B6F170C3E2E969B (review_id),
            INDEX IDX_2B6F170CB61797B (review_star_type_id)            
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        '
        );
  
        $this->addSql('ALTER TABLE review_star ADD CONSTRAINT FK_2B6F170C3E2E969B FOREIGN KEY (review_id) REFERENCES review (id)');
        $this->addSql('ALTER TABLE review_star ADD CONSTRAINT FK_2B6F170CB61797B FOREIGN KEY (review_star_type_id) REFERENCES review_star_type (id)');

        $this->addSql('
            ALTER TABLE hotel
                ADD COLUMN rating JSON DEFAULT NULL
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE review
            DROP CONSTRAINT FK_794381C63243BB18,
            DROP INDEX IDX_794381C63243BB18,
            DROP COLUMN hotel_id
        ');

        $this->addSql('DROP TABLE review_star');

        $this->addSql('DROP TABLE review_star_type');



        $this->addSql('
            ALTER TABLE hotel
                DROP COLUMN rating
        ');
    }
}
