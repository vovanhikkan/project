<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210220104640 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE level (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE action_group (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                sort INT UNSIGNED NOT NULL
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE action (
                id CHAR(36) PRIMARY KEY, 
                group_id CHAR(36) NOT NULL, 
                name JSON NOT NULL,
                description JSON NOT NULL,
                shown_from DATE NOT NULL,
                shown_to DATE NOT NULL,
                discount_absolute_value INT,
                discount_percentage_value INT,
                cashback_point_absolute_value INT,
                cashback_point_percentage_value INT,
                min_day_count INT,
                max_day_count INT,
                booking_from DATE,
                booking_to DATE,
                min_product_quantity INT,
                max_product_quantity INT,
                activation_date_from DATE,
                activation_date_to DATE,
                has_levels SMALLINT UNSIGNED NOT NULL,
                has_promocodes SMALLINT UNSIGNED NOT NULL,
                promocode_lifetime BIGINT,
                promocode_prefix VARCHAR(255),
                is_active SMALLINT UNSIGNED NOT NULL,
                total_count INT NULL,
                activated_count INT NULL,
                sort INT UNSIGNED NOT NULL,
                INDEX IDX_47CC8C92FE54D947 (group_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE promocode (
                id CHAR(36) PRIMARY KEY , 
                action_id CHAR(36) NOT NULL,
                value CHAR(36) NOT NULL UNIQUE,
                total_count INT NULL,
                holded_count INT NULL,
                ordered_count INT NULL,
                expired_at DATETIME NULL,
                INDEX IDX_7C786E069D32F035 (action_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE action_product ( 
                action_id CHAR(36),
                product_id CHAR(36),
                INDEX IDX_9E88C4C9D32F035 (action_id),
                INDEX IDX_9E88C4C4584665A (product_id),
                PRIMARY KEY (action_id, product_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE action_level (
                action_id char(36),
                level_id char(36),
                INDEX IDX_7D2400AF9D32F035 (action_id),
                INDEX IDX_7D2400AF5FB14BA7 (level_id),
                PRIMARY KEY (action_id, level_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE cart_promocode (
                cart_id CHAR(36),
                promocode_id CHAR(36),
                INDEX IDX_A68649991AD5CDBF (cart_id), 
                INDEX IDX_A6864999C76C06D9 (promocode_id),
                PRIMARY KEY (cart_id, promocode_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE IF NOT EXISTS quota_group (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                sort INT(11) UNSIGNED NOT NULL,
                is_active SMALLINT(1) UNSIGNED NOT NULL
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE IF NOT EXISTS quota (
                id CHAR(36) PRIMARY KEY,
                group_id CHAR(36) NOT NULL,
                name JSON NOT NULL,
                is_depends_on_date SMALLINT(1) UNSIGNED NOT NULL,
                has_values_by_days SMALLINT(1) UNSIGNED,
                sort INT(11) UNSIGNED NOT NULL,
                is_active SMALLINT(1) UNSIGNED NOT NULL,
                INDEX IDX_6C1C0FEDFE54D947 (group_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE IF NOT EXISTS quota_value (
                id CHAR(36) PRIMARY KEY,
                quota_id CHAR(36) NOT NULL,
                total_quantity INT NOT NULL,
                holded_quantity INT NOT NULL,
                ordered_quantity INT NOT NULL,
                date_from DATE,
                date_to DATE,
                INDEX IDX_E4C7F14654E2C62F (quota_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE quota_product (
                quota_id CHAR(36), 
                product_id CHAR(36), 
                INDEX IDX_6149FD2554E2C62F (quota_id),
                INDEX IDX_6149FD254584665A (product_id),
                PRIMARY KEY (quota_id, product_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');


        $this->addSql('
            CREATE TABLE facility (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                is_active TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
                facility_group_id CHAR(36) NOT NULL,
                is_shown_in_room_list TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
                mobile_icon CHAR(36),
                web_icon CHAR(36), 
                sort INT(11),
                INDEX IDX_105994B2A85760C8 (facility_group_id),
                INDEX IDX_105994B287A63139 (mobile_icon),
                INDEX IDX_105994B2A196242E (web_icon)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');


        $this->addSql('
            CREATE TABLE facility_group (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                is_active TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
                is_shown_hotel TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
                is_shown_room TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
                mobile_icon CHAR(36),
                web_icon CHAR(36), 
                sort INT(11),
                INDEX IDX_3E5F78AF87A63139 (mobile_icon),
                INDEX IDX_3E5F78AFA196242E (web_icon)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE food_type (
                id CHAR(36) PRIMARY KEY,
                name JSON NOT NULL,
                sort INT(11),
                is_active TINYINT(1) UNSIGNED NOT NULL DEFAULT 1
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE hotel (
               id CHAR(36) PRIMARY KEY, 
               name JSON,
               description JSON,
               star_count INT(11),
               location_id CHAR(36),
               address JSON,
               logo_svg CHAR(36),
               logo CHAR(36),
               links JSON,
               lat DECIMAL(9,6),
               lon DECIMAL(9,6),
               type_id CHAR(36),
               INDEX IDX_3535ED964D218E (location_id),
               INDEX IDX_3535ED9FE616A99 (logo_svg),
               INDEX IDX_3535ED9E48E9A13 (logo),
               INDEX IDX_3535ED9C54C8C93 (type_id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');


        $this->addSql('
            CREATE TABLE hotel_photo (
                hotel_id CHAR(36),
                photo_id CHAR(36),
                INDEX IDX_F7634ADC3243BB18 (hotel_id),
                INDEX IDX_F7634ADC7E9E4C8C (photo_id),
                PRIMARY KEY (hotel_id, photo_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE hotel_type (
                id CHAR(36) PRIMARY KEY,
                name JSON,
                is_active TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
                sort INT(11)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE hotel_video (
                hotel_id CHAR(36),
                video_id CHAR(36),
                INDEX IDX_9F1314E83243BB18 (hotel_id),
                INDEX IDX_9F1314E829C1004E (video_id),
                PRIMARY KEY (hotel_id, video_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE room (
                id CHAR(36) PRIMARY KEY,
                hotel_id CHAR(36),
                name JSON,
                description JSON,
                area INT(11),
                main_place_count INT(11),
                extra_place_count INT(11),
                zero_place_count INT(11),
                INDEX IDX_729F519B3243BB18 (hotel_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE room_photo (
                room_id CHAR(36),
                photo_id CHAR(36),
                INDEX IDX_5E0B25B354177093 (room_id),
                INDEX IDX_5E0B25B37E9E4C8C (photo_id),
                PRIMARY KEY (room_id, photo_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE room_video (
                room_id CHAR(36),
                video_id CHAR(36),
                INDEX IDX_367B7B8754177093 (room_id),
                INDEX IDX_367B7B8729C1004E (video_id),
                PRIMARY KEY (room_id, video_id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE location (
                id CHAR(36) PRIMARY KEY,
                is_active SMALLINT(1) UNSIGNED NOT NULL DEFAULT 1,
                name JSON,
                sort INT(11),
                altitude INT
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE hotel_facility (
                hotel_id CHAR(36),
                facility_id CHAR(36),
                INDEX IDX_523846C03243BB18 (hotel_id),
                INDEX IDX_523846C0A7014910 (facility_id),
                PRIMARY KEY (hotel_id, facility_id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE room_facility (
                room_id CHAR(36),
                facility_id CHAR(36),
                INDEX IDX_B0BF547654177093 (room_id),
                INDEX IDX_B0BF5476A7014910 (facility_id),
                PRIMARY KEY (room_id, facility_id)
            )  DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('
            ALTER TABLE action
                ADD CONSTRAINT FK_47CC8C92FE54D947 FOREIGN KEY (group_id) REFERENCES action_group (id)
        ');
        $this->addSql('
            ALTER TABLE promocode
                ADD CONSTRAINT FK_7C786E069D32F035 FOREIGN KEY (action_id) REFERENCES action (id)
        ');
        $this->addSql('
            ALTER TABLE action_product 
                ADD CONSTRAINT FK_9E88C4C9D32F035 FOREIGN KEY (action_id) REFERENCES action (id),
                ADD CONSTRAINT FK_9E88C4C4584665A FOREIGN KEY (product_id) REFERENCES product (id)
        ');
        $this->addSql('
            ALTER TABLE action_level 
                ADD CONSTRAINT FK_7D2400AF9D32F035 FOREIGN KEY (action_id) REFERENCES action (id),
                ADD CONSTRAINT FK_7D2400AF5FB14BA7 FOREIGN KEY (level_id) REFERENCES level (id)
        ');
        $this->addSql('
            ALTER TABLE user 
                ADD COLUMN level_id CHAR(36),
                ADD INDEX IDX_8D93D6495FB14BA7 (level_id),
                ADD CONSTRAINT FK_8D93D6495FB14BA7 FOREIGN KEY (level_id) REFERENCES level (id)
        ');
        $this->addSql('
            ALTER TABLE cart_promocode 
                ADD CONSTRAINT FK_A68649991AD5CDBF FOREIGN KEY (cart_id) REFERENCES cart (id),
                ADD CONSTRAINT FK_A6864999C76C06D9 FOREIGN KEY (promocode_id) REFERENCES promocode (id)
        ');
        $this->addSql('
            ALTER TABLE quota
                ADD CONSTRAINT FK_6C1C0FEDFE54D947 FOREIGN KEY (group_id) REFERENCES quota_group (id)
        ');
        $this->addSql('
            ALTER TABLE quota_value
                ADD CONSTRAINT FK_E4C7F14654E2C62F FOREIGN KEY (quota_id) REFERENCES quota (id)
        ');
        $this->addSql('
            ALTER TABLE quota_product
                ADD CONSTRAINT FK_6149FD2554E2C62F FOREIGN KEY (quota_id) REFERENCES quota (id),
                ADD CONSTRAINT FK_6149FD254584665A FOREIGN KEY (product_id) REFERENCES product (id)
        ');
        $this->addSql('
            ALTER TABLE facility
                ADD CONSTRAINT FK_105994B2A85760C8 FOREIGN KEY (facility_group_id) REFERENCES facility_group (id),
                ADD CONSTRAINT FK_105994B287A63139 FOREIGN KEY (mobile_icon) REFERENCES file (id),
                ADD CONSTRAINT FK_105994B2A196242E FOREIGN KEY (web_icon) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE facility_group
                ADD CONSTRAINT FK_3E5F78AF87A63139 FOREIGN KEY (mobile_icon) REFERENCES file (id),
                ADD CONSTRAINT FK_3E5F78AFA196242E FOREIGN KEY (web_icon) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE hotel
                ADD CONSTRAINT FK_3535ED964D218E FOREIGN KEY (location_id) REFERENCES location (id),
                ADD CONSTRAINT FK_3535ED9FE616A99 FOREIGN KEY (logo_svg) REFERENCES file (id),
                ADD CONSTRAINT FK_3535ED9E48E9A13 FOREIGN KEY (logo) REFERENCES file (id),
                ADD CONSTRAINT FK_3535ED9C54C8C93 FOREIGN KEY (type_id) REFERENCES hotel_type (id)
        ');
        $this->addSql('
            ALTER TABLE hotel_photo
                ADD CONSTRAINT FK_F7634ADC3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
                ADD CONSTRAINT FK_F7634ADC7E9E4C8C FOREIGN KEY (photo_id) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE hotel_video
                ADD CONSTRAINT FK_9F1314E83243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
                ADD CONSTRAINT FK_9F1314E829C1004E FOREIGN KEY (video_id) REFERENCES video (id)
        ');
        $this->addSql('
            ALTER TABLE room
                ADD CONSTRAINT FK_729F519B3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id)
        ');
        $this->addSql('
            ALTER TABLE room_photo
                ADD CONSTRAINT FK_5E0B25B354177093 FOREIGN KEY (room_id) REFERENCES room (id),
                ADD CONSTRAINT FK_5E0B25B37E9E4C8C FOREIGN KEY (photo_id) REFERENCES file (id)
        ');
        $this->addSql('
            ALTER TABLE room_video
                ADD CONSTRAINT FK_367B7B8754177093 FOREIGN KEY (room_id) REFERENCES room (id),
                ADD CONSTRAINT FK_367B7B8729C1004E FOREIGN KEY (video_id) REFERENCES video (id)
        ');
        $this->addSql('
            ALTER TABLE hotel_facility
                ADD CONSTRAINT FK_523846C03243BB18 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
                ADD CONSTRAINT FK_523846C0A7014910 FOREIGN KEY (facility_id) REFERENCES facility (id)
        ');
        $this->addSql('
            ALTER TABLE room_facility
                ADD CONSTRAINT FK_B0BF547654177093 FOREIGN KEY (room_id) REFERENCES room (id),
                ADD CONSTRAINT FK_B0BF5476A7014910 FOREIGN KEY (facility_id) REFERENCES facility (id)
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            DROP TABLE IF EXISTS location
        ');
        $this->addSql('
            DROP TABLE IF EXISTS room_video
        ');
        $this->addSql('
            DROP TABLE IF EXISTS room_photo
        ');
        $this->addSql('
            DROP TABLE IF EXISTS room
        ');
        $this->addSql('
            DROP TABLE IF EXISTS hotel_video
        ');
        $this->addSql('
            DROP TABLE IF EXISTS hotel_type
        ');
        $this->addSql('
            DROP TABLE IF EXISTS hotel_photo
        ');
        $this->addSql('
            DROP TABLE IF EXISTS hotel
        ');
        $this->addSql('
            DROP TABLE IF EXISTS food_type;
       ');
        $this->addSql('
            DROP TABLE IF EXISTS room_facility
        ');
        $this->addSql('
            DROP TABLE IF EXISTS hotel_facility
        ');
        $this->addSql('
            DROP TABLE IF EXISTS facility
        ');
        $this->addSql('
            DROP TABLE IF EXISTS facility_group
        ');
        $this->addSql('
            DROP TABLE IF EXISTS quota_product;
            DROP TABLE IF EXISTS quota_value;
            DROP TABLE IF EXISTS quota;
            DROP TABLE IF EXISTS quota_group;
        ');
        $this->addSql('DROP TABLE IF EXISTS cart_promocode');
        $this->addSql('
            ALTER TABLE user 
                DROP CONSTRAINT FK_8D93D6495FB14BA7,
                DROP COLUMN level_id
        ');
        $this->addSql('DROP TABLE IF EXISTS action_level');
        $this->addSql('DROP TABLE IF EXISTS action_product');
        $this->addSql('DROP TABLE IF EXISTS promocode');
        $this->addSql('DROP TABLE IF EXISTS action');
        $this->addSql('DROP TABLE IF EXISTS action_group');
        $this->addSql('DROP TABLE IF EXISTS level');
    }
}
