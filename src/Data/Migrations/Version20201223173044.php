<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201223173044 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS rooms_photos (
            id int PRIMARY KEY AUTO_INCREMENT,
            room_id int,
            photo int,
            sort int,
            FOREIGN KEY (room_id) REFERENCES rooms (id),
            FOREIGN KEY (photo) REFERENCES storage (id)
        )');

        $this->addSql('CREATE TABLE IF NOT EXISTS rooms_videos (
            id int PRIMARY KEY AUTO_INCREMENT,
            room_id int,
            youtube_code varchar(50),
            background int,
            sort int,
            FOREIGN KEY (room_id) REFERENCES rooms (id),
            FOREIGN KEY (background) REFERENCES storage (id)
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
