<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210212043723 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE receipt (
                id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                payment_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
                provider SMALLINT(3) UNSIGNED NOT NULL,
                external_id VARCHAR(255) NOT NULL,
                status SMALLINT(3) UNSIGNED NOT NULL,
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\',
                INDEX IDX_5399B6454C3A3BB (payment_id),
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B6454C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE receipt');
    }
}
