<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210527071415 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE bundle_product 
        ADD INDEX IDX_25B2BDD4F1FAD9D3 (bundle_id),
        ADD INDEX IDX_25B2BDD44584665A (product_id),
        ADD CONSTRAINT FK_25B2BDD4F1FAD9D3 FOREIGN KEY (bundle_id) REFERENCES bundle (id),
        ADD CONSTRAINT FK_25B2BDD44584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE bundle_product
                DROP CONSTRAINT FK_25B2BDD4F1FAD9D3,
                DROP CONSTRAINT FK_25B2BDD44584665A,
                DROP INDEX IDX_25B2BDD4F1FAD9D3,
                DROP INDEX IDX_25B2BDD44584665A;
        ');

    }
}
