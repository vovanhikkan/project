<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210518113337 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE label_type (
                id CHAR(36) PRIMARY KEY,
                icon CHAR(36),
                svg_icon CHAR(36),
                default_color VARCHAR(255),
                default_content VARCHAR(255),
                INDEX IDX_40E6C2E5659429DB (icon),
                INDEX IDX_40E6C2E523273D34 (svg_icon),
                CONSTRAINT FK_40E6C2E5659429DB FOREIGN KEY (icon) REFERENCES file (id),
                CONSTRAINT FK_40E6C2E523273D34 FOREIGN KEY (svg_icon) REFERENCES file (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            CREATE TABLE label (
                id CHAR(36) PRIMARY KEY,
                label_type_id CHAR(36),
                color VARCHAR(255),
                content JSON,
                INDEX IDX_EA750E879CD25BA (label_type_id),
                CONSTRAINT FK_EA750E879CD25BA FOREIGN KEY (label_type_id) REFERENCES label_type (id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
        ');

        $this->addSql('
            ALTER TABLE place
                DROP COLUMN label;
        ');
        $this->addSql('
            ALTER TABLE place
                ADD COLUMN label_id CHAR(36) AFTER age_restriction,
                ADD INDEX IDX_741D53CD33B92F39 (label_id),
                ADD CONSTRAINT FK_741D53CD33B92F39 FOREIGN KEY (label_id) REFERENCES label_type (id);                
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                DROP COLUMN label_id,
                DROP CONSTRAINT FK_741D53CD33B92F39,
                DROP INDEX IDX_741D53CD33B92F39;
        ');
        $this->addSql('
            ALTER TABLE place
                ADD COLUMN label JSON DEFAULT NULL AFTER age_restriction;
        ');
        $this->addSql('
            DROP TABLE label;
        ');
        $this->addSql('
            DROP TABLE label_type;
        ');
    }
}
