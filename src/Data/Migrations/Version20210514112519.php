<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210514112519 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE category
                DROP COLUMN web_icon_svg;
        ');

        $this->addSql('
            ALTER TABLE category
                ADD COLUMN web_icon_svg CHAR(36) AFTER web_icon_id;
        ');
        $this->addSql('CREATE INDEX IDX_64C19C1B74DB4B9 ON category (web_icon_svg)');
        $this->addSql('
            ALTER TABLE category 
                ADD CONSTRAINT FK_64C19C1B74DB4B9 FOREIGN KEY (web_icon_svg) REFERENCES file (id);
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1B74DB4B9');
        $this->addSql('DROP INDEX IDX_64C19C1B74DB4B9 ON category');
        $this->addSql('
            ALTER TABLE category
                DROP COLUMN web_icon_svg;
        ');
        $this->addSql('
            ALTER TABLE category
                ADD web_icon_svg LONGTEXT DEFAULT NULL AFTER web_icon_id;
        ');
    }
}
