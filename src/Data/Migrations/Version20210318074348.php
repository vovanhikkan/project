<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210318074348 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE product
                DROP COLUMN price
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE product
                ADD COLUMN price BIGINT UNSIGNED
        ');
    }
}
