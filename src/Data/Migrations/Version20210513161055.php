<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210513161055 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                ADD location_id CHAR(36) DEFAULT NULL AFTER rating,
                ADD recommended_id JSON AFTER location_id
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE place
                DROP COLUMN location_id,
                DROP COLUMN recommended_id
        ');
    }
}
