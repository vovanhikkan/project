<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210304140259 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE product
                ADD COLUMN is_active TINYINT(1) NOT NULL DEFAULT 1
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE product
                DROP COLUMN is_active
        ');
    }
}
