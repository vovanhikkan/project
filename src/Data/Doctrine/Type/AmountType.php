<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Amount;

/**
 * AmountType.
 */
class AmountType extends IntegerType
{
    const NAME = 'amount';

    protected function getClassName(): string
    {
        return Amount::class;
    }
}
