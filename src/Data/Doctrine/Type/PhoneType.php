<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Phone;

/**
 * PhoneType.
 */
class PhoneType extends StringType
{
    const NAME = 'phone';

    protected function getClassName(): string
    {
        return Phone::class;
    }
}
