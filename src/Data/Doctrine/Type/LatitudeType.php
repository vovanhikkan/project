<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Latitude;

/**
 * LatitudeType.
 */
class LatitudeType extends FloatType
{
    const NAME = 'latitude';

    protected function getClassName(): string
    {
        return Latitude::class;
    }
}
