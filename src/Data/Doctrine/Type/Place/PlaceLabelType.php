<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\Label;

/**
 * PlaceLabelType.
 */
class PlaceLabelType extends ArrayType
{
    const NAME = 'place_place_label';

    protected function getClassName(): string
    {
        return Label::class;
    }
}
