<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\AgeRestriction;

/**
 * PlaceAgeRestrictionType.
 */
class PlaceAgeRestrictionType extends ArrayType
{
    const NAME = 'place_place_age_restriction';

    protected function getClassName(): string
    {
        return AgeRestriction::class;
    }
}
