<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Block\Body;

/**
 * BlockBodyType.
 */
class BlockBodyType extends ArrayType
{
    const NAME = 'place_block_body';

    protected function getClassName(): string
    {
        return Body::class;
    }
}
