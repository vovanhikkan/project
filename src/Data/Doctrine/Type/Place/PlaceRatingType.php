<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\Rating;

/**
 * PlaceRatingType.
 */
class PlaceRatingType extends ArrayType
{
    const NAME = 'place_place_rating';

    protected function getClassName(): string
    {
        return Rating::class;
    }
}
