<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\StringType;
use App\Place\Model\Place\Code;

/**
 * PlaceCodeType.
 */
class PlaceCodeType extends StringType
{
    const NAME = 'place_place_code';

    protected function getClassName(): string
    {
        return Code::class;
    }
}
