<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Block\Title;

/**
 * BlockTitleType.
 */
class BlockTitleType extends ArrayType
{
    const NAME = 'place_block_title';

    protected function getClassName(): string
    {
        return Title::class;
    }
}
