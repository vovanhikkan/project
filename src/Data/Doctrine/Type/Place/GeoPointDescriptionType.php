<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\GeoPoint\Description;

/**
 * GeoPointDescriptionType.
 */
class GeoPointDescriptionType extends ArrayType
{
    const NAME = 'place_geo_point_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
