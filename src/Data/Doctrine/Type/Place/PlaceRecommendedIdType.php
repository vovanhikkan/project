<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\RecommendedId;

/**
 * PlaceRecommendedIdType.
 */
class PlaceRecommendedIdType extends ArrayType
{
    const NAME = 'place_place_recommended_id';

    protected function getClassName(): string
    {
        return RecommendedId::class;
    }
}
