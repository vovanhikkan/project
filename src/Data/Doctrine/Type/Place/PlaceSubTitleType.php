<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\SubTitle;

/**
 * PlaceSubTitleType.
 */
class PlaceSubTitleType extends ArrayType
{
    const NAME = 'place_place_sub_title';

    protected function getClassName(): string
    {
        return SubTitle::class;
    }
}
