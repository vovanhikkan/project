<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\Description;

/**
 * PlaceDescriptionType.
 */
class PlaceDescriptionType extends ArrayType
{
    const NAME = 'place_place_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
