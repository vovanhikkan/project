<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\Title;

/**
 * PlaceTitleType.
 */
class PlaceTitleType extends ArrayType
{
    const NAME = 'place_place_title';

    protected function getClassName(): string
    {
        return Title::class;
    }
}
