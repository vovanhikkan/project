<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Label\Content;

/**
 * LabelContentType.
 */
class LabelContentType extends ArrayType
{
    const NAME = 'place_label_content';

    protected function getClassName(): string
    {
        return Content::class;
    }
}
