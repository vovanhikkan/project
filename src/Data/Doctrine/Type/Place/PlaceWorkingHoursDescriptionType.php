<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\WorkingHoursDescription;

/**
 * PlaceWorkingHoursType.
 */
class PlaceWorkingHoursDescriptionType extends ArrayType
{
    const NAME = 'place_place_working_hours_description';

    protected function getClassName(): string
    {
        return WorkingHoursDescription::class;
    }
}
