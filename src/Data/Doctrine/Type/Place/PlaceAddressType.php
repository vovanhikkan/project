<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\Address;

/**
 * PlaceAddressType.
 */
class PlaceAddressType extends ArrayType
{
    const NAME = 'place_place_address';

    protected function getClassName(): string
    {
        return Address::class;
    }
}
