<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\IntegerType;
use App\Place\Model\Place\Width;

/**
 * PlaceWidthType.
 */
class PlaceWidthType extends IntegerType
{
    const NAME = 'place_place_width';

    protected function getClassName(): string
    {
        return Width::class;
    }
}
