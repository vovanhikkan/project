<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\SubDescription;

/**
 * PlaceSubDescriptionType.
 */
class PlaceSubDescriptionType extends ArrayType
{
    const NAME = 'place_place_sub_description';

    protected function getClassName(): string
    {
        return SubDescription::class;
    }
}
