<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\ArrayType;
use App\Place\Model\Place\ButtonText;

/**
 * PlaceButtonTextType.
 */
class PlaceButtonTextType extends ArrayType
{
    const NAME = 'place_place_button_text';

    protected function getClassName(): string
    {
        return ButtonText::class;
    }
}
