<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Place;

use App\Data\Doctrine\Type\StringType;
use App\Place\Model\LabelType\DefaultContent;

/**
 * UserFirstNameType.
 */
class LabelTypeDefaultContentType extends StringType
{
    const NAME = 'place_label_type_default_content';

    protected function getClassName(): string
    {
        return DefaultContent::class;
    }
}
