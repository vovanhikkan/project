<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\MiddleName;

/**
 * MiddleNameType.
 */
class MiddleNameType extends StringType
{
    const NAME = 'middle_name';

    protected function getClassName(): string
    {
        return MiddleName::class;
    }
}
