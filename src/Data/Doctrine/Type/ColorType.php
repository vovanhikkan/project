<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Color;

/**
 * ColorType.
 */
class ColorType extends StringType
{
    const NAME = 'color';

    protected function getClassName(): string
    {
        return Color::class;
    }
}
