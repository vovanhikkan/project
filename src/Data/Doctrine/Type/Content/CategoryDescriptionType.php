<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Content;

use App\Content\Model\Category\Description;
use App\Data\Doctrine\Type\ArrayType;

/**
 * CategoryDescriptionType.
 */
class CategoryDescriptionType extends ArrayType
{
    const NAME = 'content_category_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
