<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Content;

use App\Content\Model\Tag\ColorBorder;
use App\Data\Doctrine\Type\StringType;

/**
 * TagColorBorderType.
 */
class TagColorBorderType extends StringType
{
    const NAME = 'content_tag_color_border';

    protected function getClassName(): string
    {
        return ColorBorder::class;
    }
}
