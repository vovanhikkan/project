<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Content;

use App\Content\Model\Tag\Name;
use App\Data\Doctrine\Type\ArrayType;

/**
 * TagNameType.
 */
class TagNameType extends ArrayType
{
    const NAME = 'content_tag_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
