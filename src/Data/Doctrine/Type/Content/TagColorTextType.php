<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Content;

use App\Content\Model\Tag\ColorText;
use App\Data\Doctrine\Type\StringType;

/**
 * TagColorTextType.
 */
class TagColorTextType extends StringType
{
    const NAME = 'content_tag_color_text';

    protected function getClassName(): string
    {
        return ColorText::class;
    }
}
