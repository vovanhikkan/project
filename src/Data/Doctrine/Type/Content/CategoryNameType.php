<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Content;

use App\Content\Model\Category\Name;
use App\Data\Doctrine\Type\ArrayType;

/**
 * CategoryNameType.
 */
class CategoryNameType extends ArrayType
{
    const NAME = 'content_category_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
