<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\Section\ActiveDays;

/**
 * SectionSettingsActiveDaysType.
 */
class SectionSettingsActiveDaysType extends IntegerType
{
    const NAME = 'product_section_settings_active_days';

    protected function getClassName(): string
    {
        return ActiveDays::class;
    }
}
