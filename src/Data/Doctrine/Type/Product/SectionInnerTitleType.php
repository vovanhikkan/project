<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\ArrayType;
use App\Product\Model\Section\InnerTitle;

/**
 * SectionInnerTitleType.
 */
class SectionInnerTitleType extends ArrayType
{
    const NAME = 'product_section_inner_title';

    protected function getClassName(): string
    {
        return InnerTitle::class;
    }
}
