<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\StringType;
use App\Product\Model\Product\Age;

/**
 * ProductSettingsAgeType.
 */
class ProductSettingsAgeType extends StringType
{
    const NAME = 'product_product_settings_age';

    protected function getClassName(): string
    {
        return Age::class;
    }
}
