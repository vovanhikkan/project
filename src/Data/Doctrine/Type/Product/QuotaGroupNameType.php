<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\ArrayType;
use App\Product\Model\QuotaGroup\Name;

/**
 * QuotaGroupNameType.
 */
class QuotaGroupNameType extends ArrayType
{
    const NAME = 'product_quota_group_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
