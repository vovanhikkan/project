<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\CardType\Type;

/**
 * CardTypeTypeType.
 */
class CardTypeTypeType extends IntegerType
{
    const NAME = 'product_card_type_type';

    protected function getClassName(): string
    {
        return Type::class;
    }
}
