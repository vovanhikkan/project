<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\ArrayType;
use App\Product\Model\Section\Controls;

/**
 * SectionSettingsControlsType.
 */
class SectionSettingsControlsType extends ArrayType
{
    const NAME = 'product_section_settings_controls';

    protected function getClassName(): string
    {
        return Controls::class;
    }
}
