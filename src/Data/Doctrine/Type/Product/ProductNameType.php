<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\StringType;
use App\Product\Model\Product\Name;

/**
 * ProductNameType.
 */
class ProductNameType extends StringType
{
    const NAME = 'product_product_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
