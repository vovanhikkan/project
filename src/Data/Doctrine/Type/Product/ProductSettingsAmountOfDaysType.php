<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\StringType;
use App\Product\Model\Product\AmountOfDays;

/**
 * ProductSettingsAmountOfDaysType.
 */
class ProductSettingsAmountOfDaysType extends StringType
{
    const NAME = 'product_product_settings_amount_of_days';

    protected function getClassName(): string
    {
        return AmountOfDays::class;
    }
}
