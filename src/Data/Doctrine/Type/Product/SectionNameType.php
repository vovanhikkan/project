<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\ArrayType;
use App\Product\Model\Section\Name;

/**
 * SectionNameType.
 */
class SectionNameType extends ArrayType
{
    const NAME = 'product_section_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
