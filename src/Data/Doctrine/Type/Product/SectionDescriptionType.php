<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\ArrayType;
use App\Product\Model\Section\Description;

/**
 * SectionDescriptionType.
 */
class SectionDescriptionType extends ArrayType
{
    const NAME = 'product_section_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
