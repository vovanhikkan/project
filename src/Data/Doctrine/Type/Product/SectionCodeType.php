<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\StringType;
use App\Product\Model\Section\Code;

/**
 * SectionCodeType.
 */
class SectionCodeType extends StringType
{
    const NAME = 'product_section_code';

    protected function getClassName(): string
    {
        return Code::class;
    }
}
