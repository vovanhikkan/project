<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\ArrayType;
use App\Product\Model\Section\TariffDescription;

/**
 * SectionTariffDescriptionType.
 */
class SectionTariffDescriptionType extends ArrayType
{
    const NAME = 'product_section_tariff_description';

    protected function getClassName(): string
    {
        return TariffDescription::class;
    }
}
