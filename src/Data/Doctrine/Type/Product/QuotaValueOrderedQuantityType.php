<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\QuotaValue\OrderedQuantity;

/**
 * QuotaValueOrderedQuantityType.
 */
class QuotaValueOrderedQuantityType extends IntegerType
{
    const NAME = 'product_quota_value_ordered_quantity';

    protected function getClassName(): string
    {
        return OrderedQuantity::class;
    }
}
