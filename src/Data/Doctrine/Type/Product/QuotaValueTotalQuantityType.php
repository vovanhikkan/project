<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\QuotaValue\TotalQuantity;

/**
 * QuotaValueTotalQuantityType.
 */
class QuotaValueTotalQuantityType extends IntegerType
{
    const NAME = 'product_quota_value_total_quantity';

    protected function getClassName(): string
    {
        return TotalQuantity::class;
    }
}
