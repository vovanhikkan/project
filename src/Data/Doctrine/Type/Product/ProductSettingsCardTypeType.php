<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\Product\CardType;

/**
 * ProductSettingsCardTypeType.
 */
class ProductSettingsCardTypeType extends IntegerType
{
    const NAME = 'product_product_settings_card_type';

    protected function getClassName(): string
    {
        return CardType::class;
    }
}
