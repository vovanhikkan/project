<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\ArrayType;
use App\Product\Model\Section\InnerDescription;

/**
 * SectionInnerDescriptionType.
 */
class SectionInnerDescriptionType extends ArrayType
{
    const NAME = 'product_section_inner_description';

    protected function getClassName(): string
    {
        return InnerDescription::class;
    }
}
