<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\QuotaValue\HoldedQuantity;

/**
 * QuotaValueHoldedQuantityType.
 */
class QuotaValueHoldedQuantityType extends IntegerType
{
    const NAME = 'product_quota_value_holded_quantity';

    protected function getClassName(): string
    {
        return HoldedQuantity::class;
    }
}
