<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\Product\Type;

/**
 * ProductTypeType.
 */
class ProductTypeType extends IntegerType
{
    const NAME = 'product_product_type';

    protected function getClassName(): string
    {
        return Type::class;
    }
}
