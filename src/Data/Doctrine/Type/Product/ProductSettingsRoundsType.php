<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Product;

use App\Data\Doctrine\Type\IntegerType;
use App\Product\Model\Product\Rounds;

/**
 * ProductSettingsRoundsType.
 */
class ProductSettingsRoundsType extends IntegerType
{
    const NAME = 'product_product_settings_rounds';

    protected function getClassName(): string
    {
        return Rounds::class;
    }
}
