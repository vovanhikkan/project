<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Location;

use App\Data\Doctrine\Type\IntegerType;
use App\Location\Model\Location\Altitude;

/**
 * LocationAltitudeType.
 */
class LocationAltitudeType extends IntegerType
{
    const NAME = 'location_location_altitude';

    protected function getClassName(): string
    {
        return Altitude::class;
    }
}
