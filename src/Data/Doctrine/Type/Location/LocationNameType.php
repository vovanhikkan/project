<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Location;

use App\Data\Doctrine\Type\ArrayType;
use App\Location\Model\Location\Name;

/**
 * LocationNameType.
 */
class LocationNameType extends ArrayType
{
    const NAME = 'location_location_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
