<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Hotel\Model\Hotel\StarCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * HotelStarCountType.
 */
class HotelStarCountType extends IntegerType
{
    const NAME = 'hotel_hotel_star_count';

    protected function getClassName(): string
    {
        return StarCount::class;
    }
}
