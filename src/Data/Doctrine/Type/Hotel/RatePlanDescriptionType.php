<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\RatePlan\Description;

/**
 * RatePlanDescriptionType.
 */
class RatePlanDescriptionType extends ArrayType
{
    const NAME = 'hotel_rate_plan_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
