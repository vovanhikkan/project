<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\BedType\Name;

/**
 * BedTypeNameType.
 */
class BedTypeNameType extends ArrayType
{
    const NAME = 'hotel_bed_type_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
