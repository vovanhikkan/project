<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\Name;

/**
 * HotelNameType.
 */
class HotelNameType extends ArrayType
{
    const NAME = 'hotel_hotel_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
