<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\HotelAgeRange\AgeFrom;

/**
 * HotelAgeRangeAgeFromType.
 */
class HotelAgeRangeAgeFromType extends IntegerType
{
    const NAME = 'hotel_hotel_age_range_age_from';

    protected function getClassName(): string
    {
        return AgeFrom::class;
    }
}
