<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\Guest\Age;

/**
 * Class HotelGuestAgeType
 * @package App\Data\Doctrine\Type\Hotel
 */
class HotelGuestAgeType extends IntegerType
{
    const NAME = 'hotel_guest_age';

    protected function getClassName(): string
    {
        return Age::class;
    }
}
