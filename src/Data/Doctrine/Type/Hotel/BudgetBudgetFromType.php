<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\Budget\BudgetFrom;

/**
 * BudgetBudgetFromType.
 */
class BudgetBudgetFromType extends IntegerType
{
    const NAME = 'hotel_budget_budget_from';

    protected function getClassName(): string
    {
        return BudgetFrom::class;
    }
}