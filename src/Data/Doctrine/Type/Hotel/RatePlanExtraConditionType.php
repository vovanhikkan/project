<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\RatePlan\ExtraCondition;

/**
 * Class RatePlanExtraConditionType
 * @package App\Data\Doctrine\Type\Hotel
 */
class RatePlanExtraConditionType extends ArrayType
{
    const NAME = 'hotel_rate_plan_extra_condition';

    protected function getClassName(): string
    {
        return ExtraCondition::class;
    }
}
