<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Facility\Name;

/**
 * FaclityNameType.
 */
class FacilityNameType extends ArrayType
{
    const NAME = 'hotel_facility_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
