<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\Links;

/**
 * HotelLinksType.
 */
class HotelLinksType extends ArrayType
{
    const NAME = 'hotel_hotel_links';

    protected function getClassName(): string
    {
        return Links::class;
    }
}
