<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Hotel\Model\Budget\BudgetTo;
use App\Data\Doctrine\Type\IntegerType;

/**
 * BudgetBudgetToType.
 */
class BudgetBudgetToType extends IntegerType
{
    const NAME = 'hotel_budget_budget_to';

    protected function getClassName(): string
    {
        return BudgetTo::class;
    }
}