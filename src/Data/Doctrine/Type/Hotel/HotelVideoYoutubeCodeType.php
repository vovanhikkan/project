<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Hotel\Model\HotelVideo\YoutubeCode;
use App\Data\Doctrine\Type\StringType;

/**
 * HotelVideoYoutubeCodeType.
 */
class HotelVideoYoutubeCodeType extends StringType
{
    const NAME = 'hotel_hotel_video_youtube_code';

    protected function getClassName(): string
    {
        return YoutubeCode::class;
    }
}
