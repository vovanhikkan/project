<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Hotel\Model\Room\Area;
use App\Data\Doctrine\Type\StringType;

/**
 * RoomAreaType.
 */
class RoomAreaType extends StringType
{
    const NAME = 'hotel_room_area';

    protected function getClassName(): string
    {
        return Area::class;
    }
}
