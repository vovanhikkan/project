<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\TaxType\Value;

/**
 * Class TaxTypeValueType
 * @package App\Data\Doctrine\Type\Hotel
 */
class TaxTypeValueType extends IntegerType
{
    const NAME = 'hotel_tax_type_value';

    protected function getClassName(): string
    {
        return Value::class;
    }
}