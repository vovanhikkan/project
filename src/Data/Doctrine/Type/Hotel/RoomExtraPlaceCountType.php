<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\Room\ExtraPlaceCount;

/**
 * RoomExtraPlaceCountType.
 */
class RoomExtraPlaceCountType extends IntegerType
{
    const NAME = 'hotel_room_extra_place_count';

    protected function getClassName(): string
    {
        return ExtraPlaceCount::class;
    }
}
