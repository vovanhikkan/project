<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Brand\Name;

/**
 * BrandNameType.
 */
class BrandNameType extends ArrayType
{
    const NAME = 'hotel_brand_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
