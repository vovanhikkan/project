<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Budget\Name;

/**
 * BudgetNameType.
 */
class BudgetNameType extends ArrayType
{
    const NAME = 'hotel_budget_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}