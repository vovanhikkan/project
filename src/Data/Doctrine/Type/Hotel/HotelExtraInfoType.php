<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\ExtraInfo;

/**
 * HotelExtraInfoType.
 */
class HotelExtraInfoType extends ArrayType
{
    const NAME = 'hotel_hotel_extra_info';

    protected function getClassName(): string
    {
        return ExtraInfo::class;
    }
}
