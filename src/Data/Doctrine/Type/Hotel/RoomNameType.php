<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Room\Name;

/**
 * RoomNameType.
 */
class RoomNameType extends ArrayType
{
    const NAME = 'hotel_room_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
