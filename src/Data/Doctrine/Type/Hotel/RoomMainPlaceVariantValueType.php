<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\RoomMainPlaceVariant\Value;

/**
 * RoomMainPlaceVariantValueType.
 */
class RoomMainPlaceVariantValueType extends IntegerType
{
    const NAME = 'hotel_room_main_place_variant_value';

    protected function getClassName(): string
    {
        return Value::class;
    }
}
