<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\CancelPrepayment;

/**
 * HotelCancelPrepaymentType.
 */
class HotelCancelPrepaymentType extends ArrayType
{
    const NAME = 'hotel_hotel_cancel_prepayment';

    protected function getClassName(): string
    {
        return CancelPrepayment::class;
    }
}
