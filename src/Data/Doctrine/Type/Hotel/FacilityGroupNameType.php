<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\FacilityGroup\Name;

/**
 * FaclityGroupNameType.
 */
class FacilityGroupNameType extends ArrayType
{
    const NAME = 'hotel_facility_group_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
