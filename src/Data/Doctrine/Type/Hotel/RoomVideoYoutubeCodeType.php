<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Hotel\Model\RoomVideo\YoutubeCode;
use App\Data\Doctrine\Type\StringType;

/**
 * RoomVideoYoutubeCodeType.
 */
class RoomVideoYoutubeCodeType extends StringType
{
    const NAME = 'hotel_room_video_youtube_code';

    protected function getClassName(): string
    {
        return YoutubeCode::class;
    }
}
