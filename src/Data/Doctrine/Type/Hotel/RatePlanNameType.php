<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\RatePlan\Name;

/**
 * RatePlanNameType.
 */
class RatePlanNameType extends ArrayType
{
    const NAME = 'hotel_rate_plan_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
