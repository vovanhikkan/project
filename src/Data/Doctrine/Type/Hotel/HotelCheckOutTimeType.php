<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\CheckOutTime;

/**
 * HotelExtraBedsType.
 */
class HotelCheckOutTimeType extends ArrayType
{
    const NAME = 'hotel_hotel_check_out_time';

    protected function getClassName(): string
    {
        return CheckOutTime::class;
    }
}
