<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\RatePlanGroup\Name;

/**
 * Class RatePlanGroupNameType
 * @package App\Data\Doctrine\Type\Hotel
 */
class RatePlanGroupNameType extends ArrayType
{
    const NAME = 'hotel_rate_plan_group_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
