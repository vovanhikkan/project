<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\RoomQuota\Name;

/**
 * Class RoomQuotaNameType
 * @package App\Data\Doctrine\Type\Hotel
 */
class RoomQuotaNameType extends ArrayType
{
    const NAME = 'hotel_room_quota_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
