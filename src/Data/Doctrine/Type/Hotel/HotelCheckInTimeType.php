<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\CheckInTime;

/**
 * HotelExtraBedsType.
 */
class HotelCheckInTimeType extends ArrayType
{
    const NAME = 'hotel_hotel_check_in_time';

    protected function getClassName(): string
    {
        return CheckInTime::class;
    }
}
