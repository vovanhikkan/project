<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Hotel\Model\Hotel\Lat;
use App\Data\Doctrine\Type\StringType;

/**
 * HotelLatType.
 */
class HotelLatType extends StringType
{
    const NAME = 'hotel_hotel_lat';

    protected function getClassName(): string
    {
        return Lat::class;
    }
}
