<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Room\Description;

/**
 * RoomDescriptionType.
 */
class RoomDescriptionType extends ArrayType
{
    const NAME = 'hotel_room_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
