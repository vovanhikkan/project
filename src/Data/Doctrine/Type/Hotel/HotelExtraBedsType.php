<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\ExtraBeds;

/**
 * HotelExtraBedsType.
 */
class HotelExtraBedsType extends ArrayType
{
    const NAME = 'hotel_hotel_extra_beds';

    protected function getClassName(): string
    {
        return ExtraBeds::class;
    }
}
