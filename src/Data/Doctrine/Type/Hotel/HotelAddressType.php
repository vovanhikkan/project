<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\Address;

/**
 * HotelAddressType.
 */
class HotelAddressType extends ArrayType
{
    const NAME = 'hotel_hotel_address';

    protected function getClassName(): string
    {
        return Address::class;
    }
}
