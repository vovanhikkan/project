<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\Payments;

/**
 * HotelPaymentsType.
 */
class HotelPaymentsType extends ArrayType
{
    const NAME = 'hotel_hotel_payments';

    protected function getClassName(): string
    {
        return Payments::class;
    }
}
