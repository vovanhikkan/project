<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\Room\MainPlaceCount;

/**
 * MainPlaceCountType.
 */
class RoomMainPlaceCountType extends IntegerType
{
    const NAME = 'hotel_room_main_place_count';

    protected function getClassName(): string
    {
        return MainPlaceCount::class;
    }
}
