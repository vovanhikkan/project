<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\TaxType\Name;

/**
 * Class TaxTypeNameType
 * @package App\Data\Doctrine\Type\Hotel
 */
class TaxTypeNameType extends ArrayType
{
    const NAME = 'hotel_tax_type_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
