<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\HotelAgeRange\AgeTo;

/**
 * HotelAgeRangeAgeToType.
 */
class HotelAgeRangeAgeToType extends IntegerType
{
    const NAME = 'hotel_hotel_age_range_age_to';

    protected function getClassName(): string
    {
        return AgeTo::class;
    }
}
