<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Hotel\Model\Hotel\Lon;
use App\Data\Doctrine\Type\StringType;

/**
 * HotelLonType.
 */
class HotelLonType extends StringType
{
    const NAME = 'hotel_hotel_lon';

    protected function getClassName(): string
    {
        return Lon::class;
    }
}
