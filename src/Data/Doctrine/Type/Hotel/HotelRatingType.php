<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\ArrayType;
use App\Hotel\Model\Hotel\Rating;

/**
 * Class HotelRatingType
 * @package App\Data\Doctrine\Type\Hotel
 */
class HotelRatingType extends ArrayType
{
    const NAME = 'hotel_hotel_rating';

    protected function getClassName(): string
    {
        return Rating::class;
    }
}
