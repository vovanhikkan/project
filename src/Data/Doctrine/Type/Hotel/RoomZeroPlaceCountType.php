<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Hotel;

use App\Data\Doctrine\Type\IntegerType;
use App\Hotel\Model\Room\ZeroPlaceCount;

/**
 * RoomZeroPlaceCountType.
 */
class RoomZeroPlaceCountType extends IntegerType
{
    const NAME = 'hotel_room_zero_place_count';

    protected function getClassName(): string
    {
        return ZeroPlaceCount::class;
    }
}
