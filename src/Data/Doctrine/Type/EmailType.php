<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Email;

/**
 * EmailType.
 */
class EmailType extends StringType
{
    const NAME = 'email';

    protected function getClassName(): string
    {
        return Email::class;
    }
}
