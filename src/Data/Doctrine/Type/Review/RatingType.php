<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Review;

use App\Data\Doctrine\Type\IntegerType;
use App\Review\Model\Review\Rating;

/**
 * Class RatingTypeType
 * @package App\Data\Doctrine\Type\Review
 */
class RatingType extends IntegerType
{
    const NAME = 'review_review_rating';

    protected function getClassName(): string
    {
        return Rating::class;
    }
}