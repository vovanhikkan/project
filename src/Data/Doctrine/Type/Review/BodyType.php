<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Review;

use App\Data\Doctrine\Type\TextType;
use App\Review\Model\Review\Body;

/**
 * Class BodyType
 * @package App\Data\Doctrine\Type\Review
 */
class BodyType extends TextType
{
    const NAME = 'review_review_body';

    protected function getClassName(): string
    {
        return Body::class;
    }
}