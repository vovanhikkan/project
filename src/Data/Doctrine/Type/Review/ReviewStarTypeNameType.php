<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Review;

use App\Data\Doctrine\Type\ArrayType;
use App\Review\Model\ReviewStarType\Name;

/**
 * Class ReviewStarTypeNameType
 * @package App\Data\Doctrine\Type\Review
 */
class ReviewStarTypeNameType extends ArrayType
{
    const NAME = 'review_star_type_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}