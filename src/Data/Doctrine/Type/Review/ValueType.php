<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Review;

use App\Data\Doctrine\Type\IntegerType;
use App\Review\Model\ReviewStar\Value;

/**
 * Class ValueType
 * @package App\Data\Doctrine\Type\Review
 */
class ValueType extends IntegerType
{
    const NAME = 'review_review_star_value';

    protected function getClassName(): string
    {
        return Value::class;
    }
}