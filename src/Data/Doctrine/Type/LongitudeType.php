<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Longitude;

/**
 * LongitudeType.
 */
class LongitudeType extends FloatType
{
    const NAME = 'longitude';

    protected function getClassName(): string
    {
        return Longitude::class;
    }
}
