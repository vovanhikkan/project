<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Barcode;

/**
 * BarcodeType.
 */
class BarcodeType extends StringType
{
    const NAME = 'bar_code';

    protected function getClassName(): string
    {
        return Barcode::class;
    }
}
