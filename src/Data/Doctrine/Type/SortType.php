<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Sort;

/**
 * SortType.
 */
class SortType extends IntegerType
{
    const NAME = 'sort';

    protected function getClassName(): string
    {
        return Sort::class;
    }
}
