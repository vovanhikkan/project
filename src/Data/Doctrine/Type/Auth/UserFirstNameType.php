<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Auth;

use App\Auth\Model\User\FirstName;
use App\Data\Doctrine\Type\StringType;

/**
 * UserFirstNameType.
 */
class UserFirstNameType extends StringType
{
    const NAME = 'auth_user_first_name';

    protected function getClassName(): string
    {
        return FirstName::class;
    }
}
