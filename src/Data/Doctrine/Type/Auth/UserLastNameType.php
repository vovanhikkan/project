<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Auth;

use App\Auth\Model\User\LastName;
use App\Data\Doctrine\Type\StringType;

/**
 * UserLastNameType.
 */
class UserLastNameType extends StringType
{
    const NAME = 'auth_user_last_name';

    protected function getClassName(): string
    {
        return LastName::class;
    }
}
