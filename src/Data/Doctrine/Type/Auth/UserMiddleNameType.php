<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Auth;

use App\Auth\Model\User\MiddleName;
use App\Data\Doctrine\Type\StringType;

/**
 * UserMiddleNameType.
 */
class UserMiddleNameType extends StringType
{
    const NAME = 'auth_user_middle_name';

    protected function getClassName(): string
    {
        return MiddleName::class;
    }
}
