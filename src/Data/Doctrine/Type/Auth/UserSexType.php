<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Auth;

use App\Auth\Model\User\Sex;
use App\Data\Doctrine\Type\IntegerType;

/**
 * UserSexType.
 */
class UserSexType extends IntegerType
{
    const NAME = 'auth_user_sex';

    protected function getClassName(): string
    {
        return Sex::class;
    }
}
