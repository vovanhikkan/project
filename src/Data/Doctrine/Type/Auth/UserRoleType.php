<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Auth;

use App\Auth\Model\User\Role;
use App\Data\Doctrine\Type\IntegerType;

/**
 * UserRoleType.
 */
class UserRoleType extends IntegerType
{
    const NAME = 'auth_user_role';

    protected function getClassName(): string
    {
        return Role::class;
    }
}
