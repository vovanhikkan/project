<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Auth;

use App\Auth\Model\User\Status;
use App\Data\Doctrine\Type\IntegerType;

/**
 * UserStatusType.
 */
class UserStatusType extends IntegerType
{
    const NAME = 'auth_user_status';

    protected function getClassName(): string
    {
        return Status::class;
    }
}
