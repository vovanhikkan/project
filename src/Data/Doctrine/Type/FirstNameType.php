<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\FirstName;

/**
 * FirstNameType.
 */
class FirstNameType extends StringType
{
    const NAME = 'first_name';

    protected function getClassName(): string
    {
        return FirstName::class;
    }
}
