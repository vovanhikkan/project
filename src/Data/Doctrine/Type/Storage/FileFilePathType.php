<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Storage;

use App\Data\Doctrine\Type\StringType;
use App\Storage\Model\File\FilePath;

/**
 * FileFilePathType.
 */
class FileFilePathType extends StringType
{
    const NAME = 'storage_file_file_path';

    protected function getClassName(): string
    {
        return FilePath::class;
    }
}
