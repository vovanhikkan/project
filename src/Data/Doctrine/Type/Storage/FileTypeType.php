<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Storage;

use App\Data\Doctrine\Type\IntegerType;
use App\Storage\Model\File\Type;

/**
 * FileTypeType.
 */
class FileTypeType extends IntegerType
{
    const NAME = 'storage_file_type';

    protected function getClassName(): string
    {
        return Type::class;
    }
}
