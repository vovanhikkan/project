<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Storage;

use App\Data\Doctrine\Type\StringType;
use App\Storage\Model\Video\YoutubeCode;

/**
 * VideoYoutubeCodeType.
 */
class VideoYoutubeCodeType extends StringType
{
    const NAME = 'storage_video_youtube_code';

    protected function getClassName(): string
    {
        return YoutubeCode::class;
    }
}
