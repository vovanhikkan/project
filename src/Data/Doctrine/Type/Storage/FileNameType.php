<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Storage;

use App\Data\Doctrine\Type\StringType;
use App\Storage\Model\File\Name;

/**
 * FileNameType.
 */
class FileNameType extends StringType
{
    const NAME = 'storage_file_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
