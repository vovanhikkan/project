<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Storage;

use App\Data\Doctrine\Type\IntegerType;
use App\Storage\Model\File\Size;

/**
 * FileSizeType.
 */
class FileSizeType extends IntegerType
{
    const NAME = 'storage_file_size';

    protected function getClassName(): string
    {
        return Size::class;
    }
}
