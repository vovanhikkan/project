<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Storage;

use App\Data\Doctrine\Type\StringType;
use App\Storage\Model\File\FileName;

/**
 * FileFileNameType.
 */
class FileFileNameType extends StringType
{
    const NAME = 'storage_file_file_name';

    protected function getClassName(): string
    {
        return FileName::class;
    }
}
