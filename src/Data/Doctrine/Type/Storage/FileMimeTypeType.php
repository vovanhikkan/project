<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Storage;

use App\Data\Doctrine\Type\StringType;
use App\Storage\Model\File\MimeType;

/**
 * FileMimeTypeType.
 */
class FileMimeTypeType extends StringType
{
    const NAME = 'storage_file_mime_type';

    protected function getClassName(): string
    {
        return MimeType::class;
    }
}
