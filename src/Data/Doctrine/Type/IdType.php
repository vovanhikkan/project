<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Id;

/**
 * IdType.
 */
class IdType extends IntegerType
{
    const NAME = 'id';

    protected function getClassName(): string
    {
        return Id::class;
    }
}
