<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\CardNum;

/**
 * CardNumType.
 */
class CardNumType extends StringType
{
    const NAME = 'card_num';

    protected function getClassName(): string
    {
        return CardNum::class;
    }
}
