<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\Bundle\WidthType;
use App\Data\Doctrine\Type\IntegerType;

/**
 * BundleWidthType.
 */
class BundleWidthType extends IntegerType
{
    const NAME = 'bundle_bundle_width';

    protected function getClassName(): string
    {
        return WidthType::class;
    }
}
