<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\Bundle\DayCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * BundleDayCountType.
 */
class BundleDayCountType extends IntegerType
{
    const NAME = 'bundle_bundle_day_count';

    protected function getClassName(): string
    {
        return DayCount::class;
    }
}
