<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\BundleItem\Description;
use App\Data\Doctrine\Type\ArrayType;

/**
 * BundleItemNameType.
 */
class BundleItemDescriptionType extends ArrayType
{
    const NAME = 'bundle_bundle_item_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
