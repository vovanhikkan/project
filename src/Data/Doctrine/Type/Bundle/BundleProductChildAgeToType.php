<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\BundleProduct\ChildAgeFrom;
use App\Bundle\Model\BundleProduct\ChildAgeTo;
use App\Data\Doctrine\Type\IntegerType;

/**
 * Class BundleProductChildAgeToType
 * @package App\Data\Doctrine\Type\Bundle
 */
class BundleProductChildAgeToType extends IntegerType
{
    const NAME = 'bundle_bundle_product_child_age_to';

    protected function getClassName(): string
    {
        return ChildAgeTo::class;
    }
}
