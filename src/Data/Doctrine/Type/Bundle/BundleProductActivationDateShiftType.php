<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\BundleProduct\ActivationDateShift;
use App\Data\Doctrine\Type\IntegerType;

/**
 * Class BundleProductActivationDateShiftType
 * @package App\Data\Doctrine\Type\Bundle
 */
class BundleProductActivationDateShiftType extends IntegerType
{
    const NAME = 'bundle_bundle_product_activation_date_shift';

    protected function getClassName(): string
    {
        return ActivationDateShift::class;
    }
}
