<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\Bundle\Description;
use App\Data\Doctrine\Type\ArrayType;

/**
 * BundleDescriptionType.
 */
class BundleDescriptionType extends ArrayType
{
    const NAME = 'bundle_bundle_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
