<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Data\Doctrine\Type\ArrayType;
use App\Bundle\Model\BundleItem\Name;

/**
 * BundleItemNameType.
 */
class BundleItemNameType extends ArrayType
{
    const NAME = 'bundle_bundle_item_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
