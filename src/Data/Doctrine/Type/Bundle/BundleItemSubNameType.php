<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\BundleItem\SubName;
use App\Data\Doctrine\Type\ArrayType;

/**
 * BundleItemSubNameType.
 */
class BundleItemSubNameType extends ArrayType
{
    const NAME = 'bundle_bundle_item_sub_name';

    protected function getClassName(): string
    {
        return SubName::class;
    }
}
