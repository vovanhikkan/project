<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\BundleProduct\ChildAgeFrom;
use App\Data\Doctrine\Type\IntegerType;

/**
 * Class BundleProductChildAgeFromType
 * @package App\Data\Doctrine\Type\Bundle
 */
class BundleProductChildAgeFromType extends IntegerType
{
    const NAME = 'bundle_bundle_product_child_age_from';

    protected function getClassName(): string
    {
        return ChildAgeFrom::class;
    }
}
