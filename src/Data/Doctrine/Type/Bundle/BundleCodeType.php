<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\Bundle\Code;
use App\Data\Doctrine\Type\StringType;

/**
 * BundleCodeType.
 */
class BundleCodeType extends StringType
{
    const NAME = 'bundle_bundle_code';

    protected function getClassName(): string
    {
        return Code::class;
    }
}
