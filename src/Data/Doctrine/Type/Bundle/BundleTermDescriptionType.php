<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Bundle;

use App\Bundle\Model\BundleTerm\Description;
use App\Data\Doctrine\Type\ArrayType;

/**
 * BundleTermDescriptionType.
 */
class BundleTermDescriptionType extends ArrayType
{
    const NAME = 'bundle_bundle_term_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
