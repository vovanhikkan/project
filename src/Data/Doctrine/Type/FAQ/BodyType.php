<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\FAQ;

use App\Data\Doctrine\Type\ArrayType;
use App\FAQ\Model\FAQ\Body;

/**
 * Class TitleType
 * @package App\Data\Doctrine\Type\FAQ
 */
class BodyType extends ArrayType
{
    const NAME = 'faq_body';

    protected function getClassName(): string
    {
        return Body::class;
    }
}