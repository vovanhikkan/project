<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\FAQ;

use App\Data\Doctrine\Type\ArrayType;
use App\FAQ\Model\FAQ\Title;

/**
 * Class TitleType
 * @package App\Data\Doctrine\Type\FAQ
 */
class TitleType extends ArrayType
{
    const NAME = 'faq_title';

    protected function getClassName(): string
    {
        return Title::class;
    }
}