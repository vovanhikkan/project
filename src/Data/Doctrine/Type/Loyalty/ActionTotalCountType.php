<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\TotalCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionTotalCountType.
 */
class ActionTotalCountType extends IntegerType
{
    const NAME = 'loyalty_action_total_count';

    protected function getClassName(): string
    {
        return TotalCount::class;
    }
}
