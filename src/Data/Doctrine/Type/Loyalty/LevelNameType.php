<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Data\Doctrine\Type\ArrayType;
use App\Loyalty\Model\Level\Name;

/**
 * LevelNameType.
 */
class LevelNameType extends ArrayType
{
    const NAME = 'loyalty_level_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
