<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Data\Doctrine\Type\ArrayType;
use App\Loyalty\Model\Action\Name;

/**
 * LoyaltyNameType.
 */
class ActionNameType extends ArrayType
{
    const NAME = 'loyalty_action_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
