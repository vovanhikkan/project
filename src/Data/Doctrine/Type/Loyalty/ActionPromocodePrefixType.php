<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\PromocodePrefix;
use App\Data\Doctrine\Type\StringType;

/**
 * ActionPromocodePrefixType.
 */
class ActionPromocodePrefixType extends StringType
{
    const NAME = 'loyalty_action_promocode_prefix';

    protected function getClassName(): string
    {
        return PromocodePrefix::class;
    }
}
