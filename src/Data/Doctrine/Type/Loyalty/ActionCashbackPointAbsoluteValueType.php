<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\CashbackPointAbsoluteValue;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionCashbackPointAbsoluteValueType.
 */
class ActionCashbackPointAbsoluteValueType extends IntegerType
{
    const NAME = 'loyalty_action_cashback_point_absolute_value';

    protected function getClassName(): string
    {
        return CashbackPointAbsoluteValue::class;
    }
}
