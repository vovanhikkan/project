<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\DiscountPercentageValue;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionDiscountPercentageValueType.
 */
class ActionDiscountPercentageValueType extends IntegerType
{
    const NAME = 'loyalty_action_discount_percentage_value';

    protected function getClassName(): string
    {
        return DiscountPercentageValue::class;
    }
}
