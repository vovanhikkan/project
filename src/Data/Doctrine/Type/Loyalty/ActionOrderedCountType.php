<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Data\Doctrine\Type\IntegerType;
use App\Loyalty\Model\Action\OrderedCount;

/**
 * ActionOrderedCountType.
 */
class ActionOrderedCountType extends IntegerType
{
    const NAME = 'loyalty_action_ordered_count';

    protected function getClassName(): string
    {
        return OrderedCount::class;
    }
}
