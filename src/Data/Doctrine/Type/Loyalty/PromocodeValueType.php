<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Promocode\Value;
use App\Data\Doctrine\Type\StringType;

/**
 * PromocodeValueType.
 */
class PromocodeValueType extends StringType
{
    const NAME = 'loyalty_promocode_value';

    protected function getClassName(): string
    {
        return Value::class;
    }
}
