<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\DiscountAbsoluteValue;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionDiscountAbsoluteValueType.
 */
class ActionDiscountAbsoluteValueType extends IntegerType
{
    const NAME = 'loyalty_action_discount_absolute_value';

    protected function getClassName(): string
    {
        return DiscountAbsoluteValue::class;
    }
}
