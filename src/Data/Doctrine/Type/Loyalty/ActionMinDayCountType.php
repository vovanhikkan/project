<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\MinDayCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionMinDayCountType.
 */
class ActionMinDayCountType extends IntegerType
{
    const NAME = 'loyalty_action_min_day_count';

    protected function getClassName(): string
    {
        return MinDayCount::class;
    }
}
