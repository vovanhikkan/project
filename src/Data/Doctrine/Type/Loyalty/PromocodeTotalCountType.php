<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Promocode\TotalCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * PromocodeTotalCountType.
 */
class PromocodeTotalCountType extends IntegerType
{
    const NAME = 'loyalty_promocode_total_count';

    protected function getClassName(): string
    {
        return TotalCount::class;
    }
}
