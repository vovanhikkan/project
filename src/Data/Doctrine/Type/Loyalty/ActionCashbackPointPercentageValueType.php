<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\CashbackPointPercentageValue;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionCashbackPointPercentageValueType.
 */
class ActionCashbackPointPercentageValueType extends IntegerType
{
    const NAME = 'loyalty_action_cashback_point_percentage_value';

    protected function getClassName(): string
    {
        return CashbackPointPercentageValue::class;
    }
}
