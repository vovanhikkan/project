<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Data\Doctrine\Type\ArrayType;
use App\Loyalty\Model\Action\Description;

/**
 * ActionDescriptionType.
 */
class ActionDescriptionType extends ArrayType
{
    const NAME = 'loyalty_action_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
