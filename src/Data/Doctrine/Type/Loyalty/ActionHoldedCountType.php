<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\HoldedCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * Class ActionHoldedCountType
 * @package App\Data\Doctrine\Type\Loyalty
 */
class ActionHoldedCountType extends IntegerType
{
    const NAME = 'loyalty_action_holded_count';

    protected function getClassName(): string
    {
        return HoldedCount::class;
    }
}
