<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\MaxProductQuantity;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionMaxProductQuantityType.
 */
class ActionMaxProductQuantityType extends IntegerType
{
    const NAME = 'loyalty_action_max_product_quantity';

    protected function getClassName(): string
    {
        return MaxProductQuantity::class;
    }
}
