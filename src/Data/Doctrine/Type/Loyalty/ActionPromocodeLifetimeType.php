<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\PromocodeLifetime;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionPromocodeLifetimeType.
 */
class ActionPromocodeLifetimeType extends IntegerType
{
    const NAME = 'loyalty_action_promocode_lifetime';

    protected function getClassName(): string
    {
        return PromocodeLifetime::class;
    }
}
