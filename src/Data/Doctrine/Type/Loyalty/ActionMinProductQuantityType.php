<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\MinProductQuantity;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionMinProductQuantityType.
 */
class ActionMinProductQuantityType extends IntegerType
{
    const NAME = 'loyalty_action_min_product_quantity';

    protected function getClassName(): string
    {
        return MinProductQuantity::class;
    }
}
