<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\ActivatedCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionActivatedCountType.
 */
class ActionActivatedCountType extends IntegerType
{
    const NAME = 'loyalty_action_activated_count';

    protected function getClassName(): string
    {
        return ActivatedCount::class;
    }
}
