<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Promocode\OrderedCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * PromocodeOrderedCountType.
 */
class PromocodeOrderedCountType extends IntegerType
{
    const NAME = 'loyalty_promocode_ordered_count';

    protected function getClassName(): string
    {
        return OrderedCount::class;
    }
}
