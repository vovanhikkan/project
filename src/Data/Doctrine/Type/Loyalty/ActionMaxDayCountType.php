<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Action\MaxDayCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * ActionMaxDayCountType.
 */
class ActionMaxDayCountType extends IntegerType
{
    const NAME = 'loyalty_action_max_day_count';

    protected function getClassName(): string
    {
        return MaxDayCount::class;
    }
}
