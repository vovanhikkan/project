<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Loyalty;

use App\Loyalty\Model\Promocode\HoldedCount;
use App\Data\Doctrine\Type\IntegerType;

/**
 * PromocodeHoldedCountType.
 */
class PromocodeHoldedCountType extends IntegerType
{
    const NAME = 'loyalty_promocode_holded_count';

    protected function getClassName(): string
    {
        return HoldedCount::class;
    }
}
