<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Quantity;

/**
 * QuantityType.
 */
class QuantityType extends IntegerType
{
    const NAME = 'quantity';

    protected function getClassName(): string
    {
        return Quantity::class;
    }
}
