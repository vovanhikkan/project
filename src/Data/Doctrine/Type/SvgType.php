<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\Svg;

/**
 * SvgType.
 */
class SvgType extends TextType
{
    const NAME = 'svg';

    protected function getClassName(): string
    {
        return Svg::class;
    }
}
