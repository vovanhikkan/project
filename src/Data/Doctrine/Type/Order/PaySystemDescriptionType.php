<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\ArrayType;
use App\Order\Model\PaySystem\Description;

/**
 * PaySystemDescriptionType.
 */
class PaySystemDescriptionType extends ArrayType
{
    const NAME = 'order_pay_system_description';

    protected function getClassName(): string
    {
        return Description::class;
    }
}
