<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\IntegerType;
use App\Order\Model\Receipt\Status;

/**
 * ReceiptStatusType.
 */
class ReceiptStatusType extends IntegerType
{
    const NAME = 'order_receipt_status';

    protected function getClassName(): string
    {
        return Status::class;
    }
}
