<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\ArrayType;
use App\Order\Model\Order\Requirements;

/**
 * Class RequirementsType
 * @package App\Data\Doctrine\Type\Order
 */
class RequirementsType extends ArrayType
{
    const NAME = 'cart_order_requirements';

    protected function getClassName(): string
    {
        return Requirements::class;
    }
}
