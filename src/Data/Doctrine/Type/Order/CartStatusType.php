<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\IntegerType;
use App\Order\Model\Cart\Status;

/**
 * CartStatusType.
 */
class CartStatusType extends IntegerType
{
    const NAME = 'order_cart_status';

    protected function getClassName(): string
    {
        return Status::class;
    }
}
