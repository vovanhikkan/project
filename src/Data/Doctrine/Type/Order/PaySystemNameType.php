<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\ArrayType;
use App\Order\Model\PaySystem\Name;

/**
 * PaySystemNameType.
 */
class PaySystemNameType extends ArrayType
{
    const NAME = 'order_pay_system_name';

    protected function getClassName(): string
    {
        return Name::class;
    }
}
