<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\StringType;
use App\Order\Model\PaySystem\Code;

/**
 * PaySystemCodeType.
 */
class PaySystemCodeType extends StringType
{
    const NAME = 'order_pay_system_code';

    protected function getClassName(): string
    {
        return Code::class;
    }
}
