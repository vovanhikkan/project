<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\IntegerType;
use App\Order\Model\Payment\Status;

/**
 * PaymentStatusType.
 */
class PaymentStatusType extends IntegerType
{
    const NAME = 'order_payment_status';

    protected function getClassName(): string
    {
        return Status::class;
    }
}
