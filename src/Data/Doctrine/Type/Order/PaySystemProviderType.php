<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\IntegerType;
use App\Order\Model\PaySystem\Provider;

/**
 * PaySystemProviderType.
 */
class PaySystemProviderType extends IntegerType
{
    const NAME = 'order_pay_system_provider';

    protected function getClassName(): string
    {
        return Provider::class;
    }
}
