<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\ArrayType;
use App\Order\Model\Order\CheckInTime;

/**
 * Class CheckInTimeType
 * @package App\Data\Doctrine\Type\Order
 */
class CheckInTimeType extends ArrayType
{
    const NAME = 'order_order_check_in_time';

    protected function getClassName(): string
    {
        return CheckInTime::class;
    }
}
