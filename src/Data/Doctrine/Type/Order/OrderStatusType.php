<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\IntegerType;
use App\Order\Model\Order\Status;

/**
 * OrderStatusType.
 */
class OrderStatusType extends IntegerType
{
    const NAME = 'order_order_status';

    protected function getClassName(): string
    {
        return Status::class;
    }
}
