<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type\Order;

use App\Data\Doctrine\Type\IntegerType;
use App\Order\Model\Receipt\Provider;

/**
 * ReceiptProviderType.
 */
class ReceiptProviderType extends IntegerType
{
    const NAME = 'order_receipt_provider';

    protected function getClassName(): string
    {
        return Provider::class;
    }
}
