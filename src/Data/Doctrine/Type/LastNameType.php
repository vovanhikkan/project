<?php

declare(strict_types=1);

namespace App\Data\Doctrine\Type;

use App\Application\ValueObject\LastName;

/**
 * LastNameType.
 */
class LastNameType extends StringType
{
    const NAME = 'last_name';

    protected function getClassName(): string
    {
        return LastName::class;
    }
}
