<?php

declare(strict_types=1);

namespace App\Bundle\Service\BundleTerm;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleTerm\BundleTerm;
use App\Bundle\Model\BundleTerm\Description;
use App\Bundle\Repository\BundleTermRepository;
use App\Data\Flusher;

class Creator
{
    private Flusher $flusher;
    private BundleTermRepository $bundleTermRepository;


    public function __construct(BundleTermRepository $bundleTermRepository, Flusher $flusher)
    {
        $this->bundleTermRepository = $bundleTermRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Bundle $bundle,
        Description $description
    ): BundleTerm {
        $bundleTerm = new BundleTerm(
            $id,
            $bundle,
            $description
        );

        $this->bundleTermRepository->add($bundleTerm);
        $this->flusher->flush();

        return $bundleTerm;
    }
}
