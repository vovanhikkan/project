<?php

declare(strict_types=1);

namespace App\Bundle\Service\BundleTerm;

use App\Application\ValueObject\Uuid;
use App\Bundle\Repository\BundleTermRepository;
use App\Data\Flusher;

class Deleter
{
    private Flusher $flusher;
    private BundleTermRepository $bundleTermRepository;


    public function __construct(BundleTermRepository $bundleTermRepository, Flusher $flusher)
    {
        $this->bundleTermRepository = $bundleTermRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->bundleTermRepository->delete($id);
        $this->flusher->flush();
    }
}
