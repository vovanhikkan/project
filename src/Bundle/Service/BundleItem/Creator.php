<?php

declare(strict_types=1);

namespace App\Bundle\Service\BundleItem;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleItem\BundleItem;
use App\Bundle\Model\BundleItem\Description;
use App\Bundle\Model\BundleItem\Name;
use App\Bundle\Model\BundleItem\SubName;
use App\Bundle\Repository\BundleItemRepository;
use App\Data\Flusher;
use App\Place\Model\Place\Place;
use App\Storage\Model\File\File;

class Creator
{
    private Flusher $flusher;
    private BundleItemRepository $bundleItemRepository;


    public function __construct(BundleItemRepository $bundleItemRepository, Flusher $flusher)
    {
        $this->bundleItemRepository = $bundleItemRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Name $name,
        SubName $subName,
        ?Description $description,
        ?File $photo,
        ?Place $place
    ): BundleItem {
        $bundleItem = new BundleItem(
            $id,
            $name,
            $subName
        );

        if ($description) {
            $bundleItem->setDescription($description);
        }

        if ($photo) {
            $bundleItem->setPhoto($photo);
        }

        if ($place) {
            $bundleItem->setPlace($place);
        }

        $this->bundleItemRepository->add($bundleItem);
        $this->flusher->flush();

        return $bundleItem;
    }
}
