<?php

declare(strict_types=1);

namespace App\Bundle\Service\BundleItem;

use App\Application\ValueObject\Uuid;
use App\Bundle\Repository\BundleItemRepository;
use App\Data\Flusher;

class Deleter
{
	private Flusher $flusher;
	private BundleItemRepository $bundleItemRepository;


	public function __construct(BundleItemRepository $bundleItemRepository, Flusher $flusher)
	{
		$this->bundleItemRepository = $bundleItemRepository;
		$this->flusher = $flusher;
	}


	public function delete(Uuid $id): void
	{
		$this->bundleItemRepository->delete($id);
		$this->flusher->flush();
	}
}
