<?php

declare(strict_types=1);

namespace App\Bundle\Service\Bundle;

use App\Bundle\Model\BundleProduct\BundleProduct;
use DateTimeImmutable;

class Filter
{
    public function do(?BundleProduct $bundleProduct, ?DateTimeImmutable $date)
    {
        if (is_null($bundleProduct)) {
            return [];
        }

        $result = [];
        foreach ($bundleProduct->getProduct()->getProductPrices() as $productPrice) {
            if ($productPrice->getDateStart() <= $date && $productPrice->getDateEnd() >= $date) {
                $result['bundleProductId'] = $bundleProduct->getId()->getValue();
                $result['price'] = $productPrice->getPrice()->toRoubles();
            }
        }

        return $result;
    }
}
