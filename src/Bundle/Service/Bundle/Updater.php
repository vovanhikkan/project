<?php

declare(strict_types=1);

namespace App\Bundle\Service\Bundle;

use App\Application\ValueObject\Amount;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\Bundle\Code;
use App\Bundle\Model\Bundle\DayCount;
use App\Bundle\Model\Bundle\Description;
use App\Bundle\Model\Bundle\Name;
use App\Bundle\Repository\BundleRepository;
use App\Data\Flusher;
use App\Storage\Model\File\File;

class Updater
{
    private Flusher $flusher;
    private BundleRepository $bundleRepository;


    public function __construct(BundleRepository $bundleRepository, Flusher $flusher)
    {
        $this->bundleRepository = $bundleRepository;
        $this->flusher = $flusher;
    }


    public function update(
        Bundle $bundle,
        ?Code $code,
        ?Name $name,
        ?bool $isWithAccommodation,
        ?Description $description,
        ?DayCount $dayCount,
        ?Amount $minPrice,
        ?Amount $profit,
        ?File $webBackground
    ): Bundle {

        $bundle->update(
            $code,
            $name,
            $isWithAccommodation,
            $description,
            $dayCount,
            $minPrice,
            $profit,
            $webBackground
        );

        $this->flusher->flush();
        return $bundle;
    }
}
