<?php

declare(strict_types=1);

namespace App\Bundle\Service\Bundle;

use App\Bundle\Repository\BundleRepository;

class RecommendationGetter
{
    private BundleRepository $bundleRepository;

    /**
     * RecommendationsGetter constructor.
     * @param BundleRepository $bundleRepository
     */
    public function __construct(BundleRepository $bundleRepository)
    {
        $this->bundleRepository = $bundleRepository;
    }

    public function getRecommendations(): array
    {
        return $this->bundleRepository->getRecommendations();
    }
}
