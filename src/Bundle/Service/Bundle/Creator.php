<?php

declare(strict_types=1);

namespace App\Bundle\Service\Bundle;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\Bundle\Code;
use App\Bundle\Model\Bundle\DayCount;
use App\Bundle\Model\Bundle\Description;
use App\Bundle\Model\Bundle\Name;
use App\Bundle\Repository\BundleRepository;
use App\Data\Flusher;
use App\Place\Repository\PlaceRepository;
use App\Storage\Model\File\File;

class Creator
{
    private Flusher $flusher;
    private BundleRepository $bundleRepository;


    public function __construct(BundleRepository $bundleRepository, Flusher $flusher)
    {
        $this->bundleRepository = $bundleRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Code $code,
        Name $name,
        bool $isWithAccommodation,
        ?Description $description,
        ?DayCount $dayCount,
        ?Amount $minPrice,
        ?Amount $profit,
        ?File $webBackground,
        ?array $items
    ): Bundle {
        $sort = $this->bundleRepository->getMaxSort() + 1;

        $bundle = new Bundle(
            $id,
            $code,
            $name,
            $isWithAccommodation,
            new Sort($sort),
            true
        );

        if ($description) {
            $bundle->setDescription($description);
        }

        if ($dayCount) {
            $bundle->setDayCount($dayCount);
        }

        if ($minPrice) {
            $bundle->setMinPrice($minPrice);
        }

        if ($profit) {
            $bundle->setProfit($profit);
        }

        if ($webBackground) {
            $bundle->setWebBackground($webBackground);
        }

        if ($items) {
            foreach ($items as $item) {
                $bundle->addItem($item);
            }
        }

        $this->bundleRepository->add($bundle);
        $this->flusher->flush();

        return $bundle;
    }
}
