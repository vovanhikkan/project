<?php

declare(strict_types=1);

namespace App\Bundle\Service\Bundle;

use App\Application\ValueObject\Uuid;
use App\Bundle\Repository\BundleRepository;
use App\Data\Flusher;

class Deleter
{
    private Flusher $flusher;
    private BundleRepository $bundleRepository;


    public function __construct(BundleRepository $bundleRepository, Flusher $flusher)
    {
        $this->bundleRepository = $bundleRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->bundleRepository->delete($id);
        $this->flusher->flush();
    }
}
