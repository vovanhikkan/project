<?php

declare(strict_types=1);

namespace App\Bundle\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleTerm\BundleTerm;

class BundleTermRepository extends AbstractRepository
{
    public function add(BundleTerm $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): BundleTerm
    {
        /** @var BundleTerm|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('BundleTerm with id %s not found', (string)$id));
        }

        return $model;
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return BundleTerm::class;
    }
}
