<?php

declare(strict_types=1);

namespace App\Bundle\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\Bundle\Name;

class BundleRepository extends AbstractRepository
{
    public function add(Bundle $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(bool $active): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b')->orderBy('b.sort');

        if ($active) {
            $qb->andWhere('b.isActive = true');
        }

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): Bundle
    {
        /** @var Bundle|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Bundle with id %s not found', (string)$id));
        }

        return $model;
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return Bundle::class;
    }

    public function getMaxSort(): int
    {
        return (int)$this->entityRepository->createQueryBuilder('b')
            ->select('MAX(b.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getRecommendations(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b')
            ->where('b.recommendationSort IS NOT NULL')
            ->orderBy('b.recommendationSort');

        return $qb->getQuery()->getResult();
    }

    public function getByNameOrNull(Name $name): ?Bundle
    {
        /** @var Bundle|null $model */
        $model = $this->entityRepository->findOneBy(['name' => $name->getValue()]);

        return $model;
    }
}
