<?php

declare(strict_types=1);

namespace App\Bundle\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Bundle\Dto\SearchDto;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\Bundle\Name;
use App\Bundle\Model\BundleProduct\BundleProduct;

class BundleProductRepository extends AbstractRepository
{

    public function fetchAll(bool $active): array
    {
        $qb = $this->entityRepository->createQueryBuilder('bp')->orderBy('bp.sort');


        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): BundleProduct
    {
        /** @var BundleProduct|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('BundleProduct with id %s not found', (string)$id));
        }

        return $model;
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return BundleProduct::class;
    }

    public function getMaxSort(): int
    {
        return (int)$this->entityRepository->createQueryBuilder('b')
            ->select('MAX(b.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function searchBundleProduct(SearchDto $searchDto)
    {
        $qb = $this->entityRepository->createQueryBuilder('bp');

        if ($bundleId = $searchDto->getBundleId()) {
            $qb->andWhere('bp.bundle = :bundleId')
                ->setParameter('bundleId', $bundleId);
        }

        $adultCount = $searchDto->getAdultCount();
        $childCount = $searchDto->getChildCount();

        if ($adultCount > 0) {
            $qb->andWhere('bp.isForAdult = 1');
        }

        if ($childCount > 0) {
            $qb->andWhere('bp.isForChildren = 1');

            if ($childAges = $searchDto->getChildAges()) {
                foreach ($childAges as $key => $childAge) {
                    $qb->andWhere('bp.childAgeFrom <= :childAge'.$key.' AND bp.childAgeTo >= :childAge'.$key)
                        ->setParameter('childAge'.$key, $childAge);
                }
            }
        }

        $sumPeople = $adultCount + $childCount;
        $qb->andWhere('bp.quantity >= :sumPeople')
            ->setParameter('sumPeople', $sumPeople);

        $queryResult = $qb->getQuery()->getResult();
        $bundleProduct = array_shift($queryResult);

        return $bundleProduct;
    }
}
