<?php

declare(strict_types=1);

namespace App\Bundle\Model\BundleTerm;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use Doctrine\ORM\Mapping as ORM;

/**
 * BundleTerm.
 * @ORM\Entity()
 * @ORM\Table(name="bundle_term")
 */
class BundleTerm
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Bundle\Model\Bundle\Bundle")
     * @ORM\JoinColumn(name="bundle_id", referencedColumnName="id")
     */
    private Bundle $bundle;

    /**
     * @ORM\Column(type="bundle_bundle_term_description")
     */
    private Description $description;

    /**
     * @ORM\Column(type="sort")
     */
    private ?Sort $sort;

    /**
     * BundleTerm constructor.
     * @param Uuid $id
     * @param Bundle $bundle
     * @param Description $description
     */
    public function __construct(Uuid $id, Bundle $bundle, Description $description)
    {
        $this->id = $id;
        $this->bundle = $bundle;
        $this->description = $description;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Bundle
     */
    public function getBundle(): Bundle
    {
        return $this->bundle;
    }

    /**
     * @return Description
     */
    public function getDescription(): Description
    {
        return $this->description;
    }

    /**
     * @return Sort|null
     */
    public function getSort(): ?Sort
    {
        return $this->sort;
    }

    public function update(?Bundle $bundle, ?Description $description)
    {
        if ($bundle) {
            $this->bundle = $bundle;
        }

        if ($description) {
            $this->description = $description;
        }
    }
}
