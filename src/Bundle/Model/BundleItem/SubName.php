<?php
declare(strict_types=1);

namespace App\Bundle\Model\BundleItem;

use App\Application\ValueObject\ArrayValueObject;

/**
 * SubName.
 */
final class SubName extends ArrayValueObject
{
}