<?php

declare(strict_types=1);

namespace App\Bundle\Model\BundleItem;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\WidthType;
use App\Place\Model\Place\Place;
use App\Storage\Model\File\File;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BundleItem.
 * @ORM\Entity()
 * @ORM\Table(name="bundle_item")
 */
class BundleItem
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="bundle_bundle_item_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="bundle_bundle_item_sub_name")
     */
    private SubName $subName;

    /**
     * @ORM\Column(type="bundle_bundle_item_description")
     */
    private Description $description;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     */
    private ?File $photo = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Place\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id")
     */
    private ?Place $place = null;

    /**
     * @ORM\Column(type="bundle_bundle_width")
     */
    private ?WidthType $widthType = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Bundle\Model\BundleItem\BundleItem")
     * @ORM\JoinTable(name="bundle_bundle_item")
     */
    private ?Collection $bundle;

    /**
     * BundleItem constructor.
     * @param Uuid $id
     * @param Name $name
     * @param SubName $subName
     */
    public function __construct(Uuid $id, Name $name, SubName $subName)
    {
        $this->id = $id;
        $this->name = $name;
        $this->subName = $subName;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return SubName
     */
    public function getSubName(): SubName
    {
        return $this->subName;
    }

    /**
     * @return Description
     */
    public function getDescription(): Description
    {
        return $this->description;
    }

    /**
     * @param Description $description
     */
    public function setDescription(Description $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @param Sort $sort
     */
    public function setSort(Sort $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return File|null
     */
    public function getPhoto(): ?File
    {
        return $this->photo;
    }

    /**
     * @param File|null $photo
     */
    public function setPhoto(?File $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return Place|null
     */
    public function getPlace(): ?Place
    {
        return $this->place;
    }

    /**
     * @param Place|null $place
     */
    public function setPlace(?Place $place): void
    {
        $this->place = $place;
    }

    public function update(?Name $name, ?SubName $subName, ?Description $description, ?File $photo, ?Place $place)
    {
        if ($name) {
            $this->name = $name;
        }

        if ($subName) {
            $this->subName = $subName;
        }

        if ($description) {
            $this->description = $description;
        }

        if ($photo) {
            $this->photo = $photo;
        }

        if ($place) {
            $this->place = $place;
        }
    }

    /**
     * @return WidthType|null
     */
    public function getWidthType(): ?WidthType
    {
        return $this->widthType;
    }

    /**
     * @param WidthType|null $widthType
     */
    public function setWidthType(?WidthType $widthType): void
    {
        $this->widthType = $widthType;
    }
}
