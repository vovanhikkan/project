<?php
declare(strict_types=1);

namespace App\Bundle\Model\BundleItem;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Name.
 */
final class Name extends ArrayValueObject
{
}