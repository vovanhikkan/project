<?php
declare(strict_types=1);

namespace App\Bundle\Model\Bundle;

use App\Application\ValueObject\StringValueObject;

/**
 * Code.
 */
final class Code extends StringValueObject
{
}