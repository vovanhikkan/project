<?php
declare(strict_types=1);

namespace App\Bundle\Model\Bundle;

use App\Application\ValueObject\IntegerValueObject;

/**
 * DayCount.
 */
final class DayCount extends IntegerValueObject
{
}