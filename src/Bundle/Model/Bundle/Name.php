<?php
declare(strict_types=1);

namespace App\Bundle\Model\Bundle;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Name.
 */
final class Name extends ArrayValueObject
{
}