<?php

declare(strict_types=1);

namespace App\Bundle\Model\Bundle;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleTerm\BundleTerm;
use App\Storage\Model\File\File;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Bundle.
 * @ORM\Entity()
 * @ORM\Table(name="bundle")
 */
class Bundle
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="bundle_bundle_code")
     */
    private Code $code;

    /**
     * @ORM\Column(type="bundle_bundle_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="bundle_bundle_description")
     */
    private ?Description $description = null;

    /**
     * @ORM\Column(type="bundle_bundle_day_count")
     */
    private ?DayCount $dayCount = null;

    /**
     * @ORM\Column(type="amount")
     */
    private ?Amount $minPrice = null;

    /**
     * @ORM\Column(type="amount")
     */
    private ?Amount $minChildPrice = null;

    /**
     * @ORM\Column(type="amount")
     */
    private ?Amount $profit = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isWithAccommodation;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="sort")
     */
    private ?Sort $recommendationSort = null;

    /**
     * @ORM\Column(type="bundle_bundle_width")
     */
    private ?WidthType $widthType = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="web_background", referencedColumnName="id")
     */
    private ?File $webBackground = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Bundle\Model\BundleItem\BundleItem", inversedBy="bundle")
     * @ORM\JoinTable(name="bundle_bundle_item",
     *  joinColumns={@ORM\JoinColumn(name="bundle_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="bundle_item_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private Collection $bundleItems;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\RatePlan\RatePlan", inversedBy="bundle")
     * @ORM\JoinTable(name="bundle_rate_plan",
     *  joinColumns={@ORM\JoinColumn(name="bundle_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id")}
     * )
     */
    private Collection $ratePlans;

    /**
     * @ORM\OneToMany(targetEntity="App\Bundle\Model\BundleTerm\BundleTerm", mappedBy="bundle")
     */
    private Collection $bundleTerms;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private ?DateTimeImmutable $shown_from = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private ?DateTimeImmutable $shown_to = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private ?DateTimeImmutable $first_booking_day_from = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private ?DateTimeImmutable $first_booking_day_to = null;

    /**
     * Bundle constructor.
     * @param Uuid $id
     * @param Code $code
     * @param Name $name
     * @param bool $isWithAccommodation
     * @param Sort $sort
     * @param bool $isActive
     */
    public function __construct(Uuid $id, Code $code, Name $name, bool $isWithAccommodation, Sort $sort, bool $isActive)
    {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->isWithAccommodation = $isWithAccommodation;
        $this->sort = $sort;
        $this->isActive = $isActive;
        $this->bundleItems = new ArrayCollection();
        $this->bundleTerms = new ArrayCollection();
        $this->ratePlans = new ArrayCollection();
    }

    /**
     * @return Description|null
     */
    public function getDescription(): ?Description
    {
        return $this->description;
    }

    /**
     * @param Description|null $description
     */
    public function setDescription(?Description $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DayCount|null
     */
    public function getDayCount(): ?DayCount
    {
        return $this->dayCount;
    }

    /**
     * @param DayCount|null $dayCount
     */
    public function setDayCount(?DayCount $dayCount): void
    {
        $this->dayCount = $dayCount;
    }

    /**
     * @return Amount|null
     */
    public function getMinChildPrice(): ?Amount
    {
        return $this->minChildPrice;
    }

    /**
     * @param Amount|null $minChildPrice
     */
    public function setMinChildPrice(?Amount $minChildPrice): void
    {
        $this->minChildPrice = $minChildPrice;
    }

    /**
     * @return Amount|null
     */
    public function getMinPrice(): ?Amount
    {
        return $this->minPrice;
    }

    /**
     * @param Amount|null $minPrice
     */
    public function setMinPrice(?Amount $minPrice): void
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return Amount|null
     */
    public function getProfit(): ?Amount
    {
        return $this->profit;
    }

    /**
     * @param Amount|null $profit
     */
    public function setProfit(?Amount $profit): void
    {
        $this->profit = $profit;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Code
     */
    public function getCode(): Code
    {
        return $this->code;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isWithAccommodation(): bool
    {
        return $this->isWithAccommodation;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }


    /**
     * @return File|null
     */
    public function getWebBackground(): ?File
    {
        return $this->webBackground;
    }

    /**
     * @return array
     */
    public function getRatePlans(): array
    {
        return $this->ratePlans->toArray();
    }


    /**
     * @param File|null $webBackground
     */
    public function setWebBackground(?File $webBackground): void
    {
        $this->webBackground = $webBackground;
    }

    public function update(
        ?Code $code,
        ?Name $name,
        ?bool $isWithAccommodation,
        ?Description $description,
        ?DayCount $dayCount,
        ?Amount $minPrice,
        ?Amount $profit,
        ?File $webBackground
    ) {
        if ($code) {
            $this->code = $code;
        }

        if ($name) {
            $this->name = $name;
        }

        if ($isWithAccommodation) {
            $this->isWithAccommodation = $isWithAccommodation;
        }

        if ($description) {
            $this->description = $description;
        }

        if ($dayCount) {
            $this->dayCount = $dayCount;
        }

        if ($minPrice) {
            $this->minPrice = $minPrice;
        }

        if ($profit) {
            $this->profit = $profit;
        }

        if ($webBackground) {
            $this->webBackground = $webBackground;
        }
    }

    public function getBundleItems(): array
    {
        return $this->bundleItems->toArray();
    }

    public function getBundleTerms(): array
    {
        return $this->bundleTerms->toArray();
    }

    /**
     * @return WidthType|null
     */
    public function getWidthType(): ?WidthType
    {
        return $this->widthType;
    }

    /**
     * @param WidthType|null $widthType
     */
    public function setWidthType(?WidthType $widthType): void
    {
        $this->widthType = $widthType;
    }

    public function addItem($item): void
    {
        $this->bundleItems->add($item);
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getShownFrom(): ?DateTimeImmutable
    {
        return $this->shown_from;
    }

    /**
     * @param DateTimeImmutable|null $shown_from
     */
    public function setShownFrom(?DateTimeImmutable $shown_from): void
    {
        $this->shown_from = $shown_from;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getShownTo(): ?DateTimeImmutable
    {
        return $this->shown_to;
    }

    /**
     * @param DateTimeImmutable|null $shown_to
     */
    public function setShownTo(?DateTimeImmutable $shown_to): void
    {
        $this->shown_to = $shown_to;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getFirstBookingDayFrom(): ?DateTimeImmutable
    {
        return $this->first_booking_day_from;
    }

    /**
     * @param DateTimeImmutable|null $first_booking_day_from
     */
    public function setFirstBookingDayFrom(?DateTimeImmutable $first_booking_day_from): void
    {
        $this->first_booking_day_from = $first_booking_day_from;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getFirstBookingDayTo(): ?DateTimeImmutable
    {
        return $this->first_booking_day_to;
    }

    /**
     * @param DateTimeImmutable|null $first_booking_day_to
     */
    public function setFirstBookingDayTo(?DateTimeImmutable $first_booking_day_to): void
    {
        $this->first_booking_day_to = $first_booking_day_to;
    }
}
