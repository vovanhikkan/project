<?php

declare(strict_types=1);

namespace App\Bundle\Model\BundleProduct;

use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Product\Model\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * BundleProduct.
 * @ORM\Entity()
 * @ORM\Table(name="bundle_product")
 */
class BundleProduct
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Bundle\Model\Bundle\Bundle")
     * @ORM\JoinColumn(name="bundle_id", referencedColumnName="id")
     */
    private Bundle $bundle;


    /**
     * @ORM\ManyToOne(targetEntity="App\Product\Model\Product\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private Product $product;

    /**
     * @ORM\Column(type="bundle_bundle_product_activation_date_shift")
     */
    private ?ActivationDateShift $activationDateShift = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isForAdult;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isForChildren;

    /**
     * @ORM\Column(type="bundle_bundle_product_child_age_from")
     */
    private ?ChildAgeFrom $childAgeFrom = null;

    /**
     * @ORM\Column(type="bundle_bundle_product_child_age_to")
     */
    private ?ChildAgeTo $childAgeTo = null;

    /**
     * @ORM\Column(type="quantity", columnDefinition="SMALLINT(5) UNSIGNED NOT NULL")
     */
    private ?Quantity $quantity = null;


    public function __construct(
        Uuid $id,
        Bundle $bundle,
        Product $product
    ) {
        $this->id = $id;
        $this->bundle = $bundle;
        $this->product = $product;
        $this->isForAdult = false;
        $this->isForChildren = false;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Bundle
     */
    public function getBundle(): Bundle
    {
        return $this->bundle;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return ActivationDateShift|null
     */
    public function getActivationDateShift(): ?ActivationDateShift
    {
        return $this->activationDateShift;
    }

    /**
     * @param ActivationDateShift|null $activationDateShift
     */
    public function setActivationDateShift(?ActivationDateShift $activationDateShift): void
    {
        $this->activationDateShift = $activationDateShift;
    }

    /**
     * @return bool
     */
    public function isForAdult(): bool
    {
        return $this->isForAdult;
    }

    /**
     * @param bool $isForAdult
     */
    public function setIsForAdult(bool $isForAdult): void
    {
        $this->isForAdult = $isForAdult;
    }

    /**
     * @return bool
     */
    public function isForChildren(): bool
    {
        return $this->isForChildren;
    }

    /**
     * @param bool $isForChildren
     */
    public function setIsForChildren(bool $isForChildren): void
    {
        $this->isForChildren = $isForChildren;
    }

    /**
     * @return ChildAgeFrom|null
     */
    public function getChildAgeFrom(): ?ChildAgeFrom
    {
        return $this->childAgeFrom;
    }

    /**
     * @param ChildAgeFrom|null $childAgeFrom
     */
    public function setChildAgeFrom(?ChildAgeFrom $childAgeFrom): void
    {
        $this->childAgeFrom = $childAgeFrom;
    }

    /**
     * @return ChildAgeTo|null
     */
    public function getChildAgeTo(): ?ChildAgeTo
    {
        return $this->childAgeTo;
    }

    /**
     * @param ChildAgeTo|null $childAgeTo
     */
    public function setChildAgeTo(?ChildAgeTo $childAgeTo): void
    {
        $this->childAgeTo = $childAgeTo;
    }

    /**
     * @return Quantity|null
     */
    public function getQuantity(): ?Quantity
    {
        return $this->quantity;
    }

    /**
     * @param Quantity|null $quantity
     */
    public function setQuantity(?Quantity $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function update(
        ?Bundle $bundle,
        ?Product $product,
        ?ActivationDateShift $activationDateShift,
        ?bool $isForAdult,
        ?bool $isForChildren,
        ?ChildAgeFrom $childAgeFrom,
        ?ChildAgeTo $childAgeTo,
        ?Quantity $quantity
    ) {
        if ($bundle) {
            $this->bundle = $bundle;
        }

        if ($product) {
            $this->product = $product;
        }

        if ($activationDateShift) {
            $this->activationDateShift = $activationDateShift;
        }

        if (!is_null($isForAdult)) {
            $this->isForAdult = $isForAdult;
        }

        if (!is_null($isForChildren)) {
            $this->isForChildren = $isForChildren;
        }

        if ($childAgeFrom) {
            $this->childAgeFrom = $childAgeFrom;
        }

        if ($childAgeTo) {
            $this->childAgeTo = $childAgeTo;
        }

        if ($activationDateShift) {
            $this->activationDateShift = $activationDateShift;
        }

        if ($quantity) {
            $this->quantity = $quantity;
        }
    }
}
