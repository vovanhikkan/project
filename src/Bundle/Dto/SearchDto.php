<?php

declare(strict_types=1);

namespace App\Bundle\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class SearchDto
{
    /**
     * @Assert\NotBlank
     */
    private string $bundleId;

    private ?string $date = null;
    private ?int $adultCount = null;
    private ?int $childCount = null;
    private ?array $childAges = null;

    public function __construct(string $bundleId)
    {
        $this->bundleId = $bundleId;
    }

    /**
     * @return string
     */
    public function getBundleId(): string
    {
        return $this->bundleId;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $dateFrom
     */
    public function setDate(?string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int|null
     */
    public function getAdultCount(): ?int
    {
        return $this->adultCount;
    }

    /**
     * @param int|null $adultCount
     */
    public function setAdultCount(?int $adultCount): void
    {
        $this->adultCount = $adultCount;
    }

    /**
     * @return int|null
     */
    public function getChildCount(): ?int
    {
        return $this->childCount;
    }

    /**
     * @param int|null $childCount
     */
    public function setChildCount(?int $childCount): void
    {
        $this->childCount = $childCount;
    }

    /**
     * @return array|null
     */
    public function getChildAges(): ?array
    {
        return $this->childAges;
    }

    /**
     * @param array|null $childAges
     */
    public function setChildAges(?array $childAges): void
    {
        $this->childAges = $childAges;
    }
}
