<?php

declare(strict_types=1);

namespace App\Bundle\Command\Bundle\Create;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\Bundle\Code;
use App\Bundle\Model\Bundle\DayCount;
use App\Bundle\Model\Bundle\Description;
use App\Bundle\Model\Bundle\Name;
use App\Bundle\Repository\BundleItemRepository;
use App\Bundle\Service\Bundle\Creator;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Creator $creator;
    private \App\Storage\Service\File\Creator $fileCreator;
    private BundleItemRepository $bundleItemRepository;


    public function __construct(Creator $creator, BundleItemRepository $bundleItemRepository, FileRepository $fileRepository, \App\Storage\Service\File\Creator $fileCreator)
    {
        $this->creator = $creator;
        $this->fileRepository = $fileRepository;
        $this->fileCreator = $fileCreator;
        $this->bundleItemRepository = $bundleItemRepository;
    }


    public function handle(Command $command): Bundle
    {
        if ($webBackground = $command->getWebBackground()) {
            $webBackground = $this->fileCreator->upload(Uuid::generate(), $command->getWebBackground());
        }

        $bundleItems = [];
        if ($bundleItemsIds = $command->getItems()) {
            foreach ($bundleItemsIds as $id) {
                $bundleItems[] = $this->bundleItemRepository->get(new Uuid($id));
            }
        }

        return $this->creator->create(
            Uuid::generate(),
            new Code($command->getCode()),
            new Name($command->getName()),
            $command->isWithAccommodation(),
            $command->getDescription() !== null ? new Description($command->getDescription()) : null,
            $command->getDayCount() !== null ? new DayCount($command->getDayCount()) : null,
            $command->getMinPrice() !== null ? new Amount($command->getMinPrice()) : null,
            $command->getProfit() !== null ? new Amount($command->getProfit()) : null,
            $webBackground,
            $bundleItems
        );
    }
}
