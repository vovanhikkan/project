<?php

declare(strict_types=1);

namespace App\Bundle\Command\Bundle\Create;

use Psr\Http\Message\UploadedFileInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $code;

    /**
     * @Assert\NotBlank()
     */
    private array $name;

    private ?array $description = null;

    private ?int $dayCount = null;

    private ?int $minPrice = null;

    private ?int $profit = null;

    private ?array $items = null;

    /**
     * @Assert\NotBlank()
     */
    private bool $isWithAccommodation;

    private ?UploadedFileInterface $webBackground = null;

    /**
     * Command constructor.
     * @param string $code
     * @param array $name
     * @param bool $isWithAccommodation
     */
    public function __construct(string $code, array $name, bool $isWithAccommodation)
    {
        $this->code = $code;
        $this->name = $name;
        $this->isWithAccommodation = $isWithAccommodation;
    }

    /**
     * @return array|null
     */
    public function getItems(): ?array
    {
        return $this->items;
    }

    /**
     * @param array|null $items
     */
    public function setItems(?array $items): void
    {
        $this->items = $items;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @param array|null $description
     */
    public function setDescription(?array $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int|null
     */
    public function getDayCount(): ?int
    {
        return $this->dayCount;
    }

    /**
     * @param int|null $dayCount
     */
    public function setDayCount(?int $dayCount): void
    {
        $this->dayCount = $dayCount;
    }

    /**
     * @return int|null
     */
    public function getMinPrice(): ?int
    {
        return $this->minPrice;
    }

    /**
     * @param int|null $minPrice
     */
    public function setMinPrice(?int $minPrice): void
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return int|null
     */
    public function getProfit(): ?int
    {
        return $this->profit;
    }

    /**
     * @param int|null $profit
     */
    public function setProfit(?int $profit): void
    {
        $this->profit = $profit;
    }

    /**
     * @return UploadedFileInterface|null
     */
    public function getWebBackground(): ?UploadedFileInterface
    {
        return $this->webBackground;
    }

    /**
     * @param UploadedFileInterface|null $webBackground
     */
    public function setWebBackground(?UploadedFileInterface $webBackground): void
    {
        $this->webBackground = $webBackground;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isWithAccommodation(): bool
    {
        return $this->isWithAccommodation;
    }
}
