<?php

declare(strict_types=1);

namespace App\Bundle\Command\Bundle\Update;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\Bundle\Code;
use App\Bundle\Model\Bundle\DayCount;
use App\Bundle\Model\Bundle\Description;
use App\Bundle\Model\Bundle\Name;
use App\Bundle\Repository\BundleRepository;
use App\Bundle\Service\Bundle\Updater;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Updater $updater;
    private BundleRepository $bundleRepository;
    private FileRepository $fileRepository;


    public function __construct(Updater $updater, BundleRepository $bundleRepository, FileRepository $fileRepository)
    {
        $this->bundleRepository = $bundleRepository;
        $this->updater = $updater;
        $this->fileRepository = $fileRepository;
    }


    public function handle(Command $command): Bundle
    {
        $bundle = $this->bundleRepository->get(new Uuid($command->getId()));

        if ($webBackground = $command->getWebBackground()) {
            $webBackground = $this->fileRepository->getFileOrNull($webBackground);
        }

        return $this->updater->update(
            $bundle,
            $command->getCode() !== null ? new Code($command->getCode()) : null,
            $command->getName() !== null ? new Name($command->getName()) : null,
            $command->getIsWithAccommodation() !== null ? $command->getIsWithAccommodation() : null,
            $command->getDescription() !== null ? new Description($command->getDescription()) : null,
            $command->getDayCount() !== null ? new DayCount($command->getDayCount()) : null,
            $command->getMinPrice() !== null ? new Amount($command->getMinPrice()) : null,
            $command->getProfit() !== null ? new Amount($command->getProfit()) : null,
            $webBackground
        );
    }
}
