<?php

declare(strict_types=1);

namespace App\Bundle\Command\Bundle\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?string $code = null;

    private ?array $name = null;

    private ?array $description = null;

    private ?int $dayCount = null;

    private ?int $minPrice = null;

    private ?int $profit = null;

    private ?bool $isWithAccommodation = null;

    private ?string $webBackground = null;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @param array|null $description
     */
    public function setDescription(?array $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int|null
     */
    public function getDayCount(): ?int
    {
        return $this->dayCount;
    }

    /**
     * @param int|null $dayCount
     */
    public function setDayCount(?int $dayCount): void
    {
        $this->dayCount = $dayCount;
    }

    /**
     * @return int|null
     */
    public function getMinPrice(): ?int
    {
        return $this->minPrice;
    }

    /**
     * @param int|null $minPrice
     */
    public function setMinPrice(?int $minPrice): void
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return int|null
     */
    public function getProfit(): ?int
    {
        return $this->profit;
    }

    /**
     * @param int|null $profit
     */
    public function setProfit(?int $profit): void
    {
        $this->profit = $profit;
    }

    /**
     * @return bool|null
     */
    public function getIsWithAccommodation(): ?bool
    {
        return $this->isWithAccommodation;
    }

    /**
     * @param bool|null $isWithAccommodation
     */
    public function setIsWithAccommodation(?bool $isWithAccommodation): void
    {
        $this->isWithAccommodation = $isWithAccommodation;
    }

    /**
     * @return string|null
     */
    public function getWebBackground(): ?string
    {
        return $this->webBackground;
    }

    /**
     * @param string|null $webBackground
     */
    public function setWebBackground(?string $webBackground): void
    {
        $this->webBackground = $webBackground;
    }
}
