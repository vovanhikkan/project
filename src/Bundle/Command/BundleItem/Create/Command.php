<?php

declare(strict_types=1);

namespace App\Bundle\Command\BundleItem\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $name;

    /**
     * @Assert\NotBlank()
     */
    private array $subName;

    private ?array $description = null;

    private ?string $photoId = null;

    private ?string $placeId = null;

    /**
     * Command constructor.
     * @param array $name
     * @param array $subName
     */
    public function __construct(array $name, array $subName)
    {
        $this->name = $name;
        $this->subName = $subName;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getSubName(): array
    {
        return $this->subName;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @param array|null $description
     */
    public function setDescription(?array $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getPhotoId(): ?string
    {
        return $this->photoId;
    }

    /**
     * @param string|null $photoId
     */
    public function setPhotoId(?string $photoId): void
    {
        $this->photoId = $photoId;
    }

    /**
     * @return string|null
     */
    public function getPlaceId(): ?string
    {
        return $this->placeId;
    }

    /**
     * @param string|null $placeId
     */
    public function setPlaceId(?string $placeId): void
    {
        $this->placeId = $placeId;
    }
}
