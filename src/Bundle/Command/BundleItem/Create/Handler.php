<?php

declare(strict_types=1);

namespace App\Bundle\Command\BundleItem\Create;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleItem\BundleItem;
use App\Bundle\Model\BundleItem\Description;
use App\Bundle\Model\BundleItem\Name;
use App\Bundle\Model\BundleItem\SubName;
use App\Bundle\Service\BundleItem\Creator;
use App\Place\Repository\PlaceRepository;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Creator $creator;
    private FileRepository $fileRepository;
    private PlaceRepository $placeRepository;


    public function __construct(Creator $creator, FileRepository $fileRepository, PlaceRepository $placeRepository)
    {
        $this->creator = $creator;
        $this->fileRepository = $fileRepository;
        $this->placeRepository = $placeRepository;
    }


    public function handle(Command $command): BundleItem
    {
        if ($photo = $command->getPhotoId()) {
            $photo = $this->fileRepository->getFileOrNull($command->getPhotoId());
        }

        if ($place = $command->getPlaceId()) {
            $place = $this->placeRepository->get(new Uuid($place));
        }

        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
            new SubName($command->getSubName()),
            $command->getDescription() !== null ? new Description($command->getDescription()) : null,
            $photo,
            $place
        );
    }
}
