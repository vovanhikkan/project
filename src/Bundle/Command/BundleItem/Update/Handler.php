<?php

declare(strict_types=1);

namespace App\Bundle\Command\BundleItem\Update;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleItem\BundleItem;
use App\Bundle\Model\BundleItem\Description;
use App\Bundle\Model\BundleItem\Name;
use App\Bundle\Model\BundleItem\SubName;
use App\Bundle\Repository\BundleItemRepository;
use App\Bundle\Service\BundleItem\Updater;
use App\Place\Repository\PlaceRepository;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Updater $updater;
    private BundleItemRepository $bundleItemRepository;
    private FileRepository $fileRepository;
    private PlaceRepository $placeRepository;


    public function __construct(Updater $updater, BundleItemRepository $bundleItemRepository, FileRepository $fileRepository, PlaceRepository $placeRepository)
    {
        $this->bundleItemRepository = $bundleItemRepository;
        $this->updater = $updater;
        $this->fileRepository = $fileRepository;
        $this->placeRepository = $placeRepository;
    }


    public function handle(Command $command): BundleItem
    {
        $bundleItem = $this->bundleItemRepository->get(new Uuid($command->getId()));

        if ($photo = $command->getPhotoId()) {
            $photo = $this->fileRepository->getFileOrNull($command->getPhotoId());
        }

        if ($place = $command->getPlaceId()) {
            $place = $this->placeRepository->get(new Uuid($place));
        }

        return $this->updater->update(
            $bundleItem,
            $command->getName() !== null ? new Name($command->getName()) : null,
            $command->getSubName() !== null ? new SubName($command->getSubName()) : null,
            $command->getDescription() !== null ? new Description($command->getDescription()) : null,
            $photo,
            $place
        );
    }
}
