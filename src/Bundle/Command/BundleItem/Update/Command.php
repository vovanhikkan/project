<?php

declare(strict_types=1);

namespace App\Bundle\Command\BundleItem\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;

    private ?array $name = null;

    private ?array $subName = null;

    private ?array $description = null;

    private ?string $photoId = null;

    private ?string $placeId = null;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @param array|null $subName
     */
    public function setSubName(?array $subName): void
    {
        $this->subName = $subName;
    }

    /**
     * @param array|null $description
     */
    public function setDescription(?array $description): void
    {
        $this->description = $description;
    }

    /**
     * @param string|null $photoId
     */
    public function setPhotoId(?string $photoId): void
    {
        $this->photoId = $photoId;
    }

    /**
     * @param string|null $placeId
     */
    public function setPlaceId(?string $placeId): void
    {
        $this->placeId = $placeId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @return array|null
     */
    public function getSubName(): ?array
    {
        return $this->subName;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getPhotoId(): ?string
    {
        return $this->photoId;
    }

    /**
     * @return string|null
     */
    public function getPlaceId(): ?string
    {
        return $this->placeId;
    }
}
