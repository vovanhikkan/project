<?php

declare(strict_types=1);

namespace App\Bundle\Command\BundleTerm\Create;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleTerm\BundleTerm;
use App\Bundle\Model\BundleTerm\Description;
use App\Bundle\Repository\BundleRepository;
use App\Bundle\Service\BundleTerm\Creator;

class Handler
{
    private Creator $creator;
    private BundleRepository $bundleRepository;


    public function __construct(Creator $creator, BundleRepository $bundleRepository)
    {
        $this->creator = $creator;
        $this->bundleRepository = $bundleRepository;
    }


    public function handle(Command $command): BundleTerm
    {
        $bundle = $this->bundleRepository->get(new Uuid($command->getBundle()));

        return $this->creator->create(
            Uuid::generate(),
            $bundle,
            new Description($command->getDescription())
        );
    }
}
