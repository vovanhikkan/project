<?php

declare(strict_types=1);

namespace App\Bundle\Command\BundleTerm\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $bundle;

    /**
     * @Assert\NotBlank()
     */
    private array $description;

    /**
     * Command constructor.
     * @param string $bundle
     * @param array $description
     */
    public function __construct(string $bundle, array $description)
    {
        $this->bundle = $bundle;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getBundle(): string
    {
        return $this->bundle;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }
}
