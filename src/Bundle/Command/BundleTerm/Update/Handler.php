<?php

declare(strict_types=1);

namespace App\Bundle\Command\BundleTerm\Update;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\BundleTerm\Description;
use App\Bundle\Model\BundleTerm\BundleTerm;
use App\Bundle\Repository\BundleRepository;
use App\Bundle\Repository\BundleTermRepository;
use App\Bundle\Service\BundleTerm\Updater;

class Handler
{
    private Updater $updater;
    private BundleTermRepository $bundleTermRepository;
    private BundleRepository $bundleRepository;


    public function __construct(Updater $updater, BundleTermRepository $bundleTermRepository, BundleRepository $bundleRepository)
    {
        $this->bundleTermRepository = $bundleTermRepository;
        $this->bundleRepository = $bundleRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): BundleTerm
    {
        $bundleTerm = $this->bundleTermRepository->get(new Uuid($command->getId()));
        if ($bundle = $command->getBundle()) {
            $bundle = $this->bundleRepository->get(new Uuid($bundle));
        }

        return $this->updater->update(
            $bundleTerm,
            $bundle,
            $command->getDescription() !== null ? new Description($command->getDescription()) : null
        );
    }
}
