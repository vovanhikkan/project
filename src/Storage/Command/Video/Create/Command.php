<?php

declare(strict_types=1);

namespace App\Storage\Command\Video\Create;

use Psr\Http\Message\UploadedFileInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $youtubeCode;

    /**
     * @Assert\NotBlank()
     */
    private UploadedFileInterface $backgroundImage;

    /**
     * Command constructor.
     * @param string $youtubeCode
     * @param UploadedFileInterface $backgroundImage
     */
    public function __construct(string $youtubeCode, UploadedFileInterface $backgroundImage)
    {
        $this->youtubeCode = $youtubeCode;
        $this->backgroundImage = $backgroundImage;
    }

    /**
     * @return string
     */
    public function getYoutubeCode(): string
    {
        return $this->youtubeCode;
    }

    /**
     * @return UploadedFileInterface
     */
    public function getBackgroundImage(): UploadedFileInterface
    {
        return $this->backgroundImage;
    }

}
