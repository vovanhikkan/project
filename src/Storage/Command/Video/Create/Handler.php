<?php

declare(strict_types=1);

namespace App\Storage\Command\Video\Create;

use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use App\Storage\Model\Video\Video;
use App\Storage\Model\Video\YoutubeCode;
use App\Storage\Service\Video\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;
    private \App\Storage\Service\File\Creator $fileCreator;

    public function __construct(Creator $creator, \App\Storage\Service\File\Creator $fileCreator)
    {
        $this->creator = $creator;
        $this->fileCreator = $fileCreator;
    }

    public function handle(Command $command): Video
    {
        $file = $this->fileCreator->upload(Uuid::generate(), $command->getBackgroundImage());

        return $this->creator->upload(
            Uuid::generate(),
            new YoutubeCode($command->getYoutubeCode()),
            $file
        );
    }
}
