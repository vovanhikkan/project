<?php

declare(strict_types=1);

namespace App\Storage\Command\File\Create;

use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use App\Storage\Service\File\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;

    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }

    public function handle(Command $command): File
    {

        return $this->creator->upload(
            Uuid::generate(),
            $command->getFile()
        );
    }
}
