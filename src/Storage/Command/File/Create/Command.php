<?php

declare(strict_types=1);

namespace App\Storage\Command\File\Create;

use Psr\Http\Message\UploadedFileInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private UploadedFileInterface $file;

    /**
     * Command constructor.
     * @param UploadedFileInterface $file
     */
    public function __construct(UploadedFileInterface $file)
    {
        $this->file = $file;
    }

    /**
     * @return UploadedFileInterface
     */
    public function getFile(): UploadedFileInterface
    {
        return $this->file;
    }

}
