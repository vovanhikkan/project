<?php

declare(strict_types=1);

namespace App\Storage\Command\File\Delete;

use App\Application\ValueObject\Uuid;
use App\Storage\Service\File\Deleter;

class Handler
{
    private Deleter $deleter;


    public function __construct(Deleter $deleter)
    {
        $this->deleter = $deleter;
    }


    public function handle(Command $command): array
    {
        $message = $this->deleter->delete(new Uuid($command->getId()));
                
        return $message; 
    }
}
