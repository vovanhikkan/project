<?php

declare(strict_types=1);

namespace App\Storage\Service\Video;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Storage\Model\File\File;
use App\Storage\Model\Video\Video;
use App\Storage\Model\Video\YoutubeCode;
use App\Storage\Repository\VideoRepository;

/**
 * Creator.
 */
class Creator
{
    private Flusher $flusher;
    private VideoRepository $videoRepository;

    public function __construct(
        Flusher $flusher,
        VideoRepository $videoRepository
    ) {
        $this->flusher = $flusher;
        $this->videoRepository = $videoRepository;
    }

    public function upload(
        Uuid $id,
        YoutubeCode $youtubeCode,
        File $backgroundImage
    ): Video {

        $video = new Video(
            $id,
            $youtubeCode,
            $backgroundImage
        );

        $this->videoRepository->add($video);
        $this->flusher->flush();

        return $video;
    }
}
