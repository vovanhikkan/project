<?php

declare(strict_types=1);

namespace App\Storage\Service\File;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Storage\Model\File\File;
use App\Storage\Model\File\MimeType;
use App\Storage\Model\File\Name;
use App\Storage\Model\File\Size;
use App\Storage\Model\File\Type;
use App\Storage\Repository\FileRepository;
use InvalidArgumentException;
use Psr\Http\Message\UploadedFileInterface;

/**
 * Creator.
 */
class Creator
{
    private FileRepository $fileRepository;
    private FileNameGenerator $fileNameGenerator;
    private FilePathGenerator $filePathGenerator;
    private Locator $locator;
    private Flusher $flusher;

    public function __construct(
        FileRepository $fileRepository,
        FileNameGenerator $fileNameGenerator,
        FilePathGenerator $filePathGenerator,
        Locator $locator,
        Flusher $flusher
    ) {
        $this->fileRepository = $fileRepository;
        $this->fileNameGenerator = $fileNameGenerator;
        $this->filePathGenerator = $filePathGenerator;
        $this->locator = $locator;
        $this->flusher = $flusher;
    }

    public function upload(
        Uuid $id,
        UploadedFileInterface $uploadedFile
    ): File {

        if ($uploadedFile->getError() !== UPLOAD_ERR_OK) {
            throw new InvalidArgumentException('Не удалось загрузить файл');
        }

        $clientFileName = (string)$uploadedFile->getClientFilename();
        $extension = mb_substr($clientFileName, mb_strrpos($clientFileName, '.') + 1);
        $fileName = $this->fileNameGenerator->generate($extension);
        $filePath = $this->filePathGenerator->generate($fileName);

        $file = new File(
            $id,
            new Type(Type::IMAGE),
            new Name($clientFileName),
            new MimeType((string)$uploadedFile->getClientMediaType()),
            new Size((int)$uploadedFile->getSize()),
            $fileName,
            $filePath
        );
        $this->fileRepository->add($file);

        $fullPath = $this->locator->getFullPath($file);
        $this->ensurePath(dirname($fullPath));
        $uploadedFile->moveTo($fullPath);

        $this->flusher->flush();

        return $file;
    }

    private function ensurePath(string $path): void
    {
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
    }
}
