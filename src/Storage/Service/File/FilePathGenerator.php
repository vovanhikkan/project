<?php

declare(strict_types=1);

namespace App\Storage\Service\File;

use App\Storage\Model\File\FileName;
use App\Storage\Model\File\FilePath;
use DateTimeImmutable;

/**
 * FilePathGenerator.
 */
class FilePathGenerator
{
    private string $storagePath;

    public function __construct()
    {
        $this->storagePath = getenv('STORAGE_PATH');
    }

    public function generate(FileName $fileName): FilePath
    {
        return new FilePath(
            (new DateTimeImmutable())->format('Y-m-d') . DIRECTORY_SEPARATOR . (string)$fileName
        );
    }
}
