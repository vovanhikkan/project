<?php

declare(strict_types=1);

namespace App\Storage\Service\File;

use App\Storage\Model\File\FileName;
use Ramsey\Uuid\Uuid;

/**
 * FileNameGenerator.
 */
class FileNameGenerator
{
    public function generate(string $extension): FileName
    {
        return new FileName(
            sprintf('%s.%s', Uuid::uuid4()->toString(), $extension)
        );
    }
}
