<?php

declare(strict_types=1);

namespace App\Storage\Service\File;

use App\Application\Exception\NotFoundException;
use App\Application\Exception\UnableToDeleteFileException;
use App\Application\ValueObject\Uuid;
use App\Storage\Repository\FileRepository;
use App\Data\Flusher;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

class Deleter
{
    private Flusher $flusher;
    private FileRepository $fileRepository;
    private Locator $locator;


    public function __construct(
        FileRepository $fileRepository,
        Flusher $flusher,
        Locator $locator
    ) {
        $this->fileRepository = $fileRepository;
        $this->flusher = $flusher;
        $this->locator = $locator;
    }


    public function delete(Uuid $id): array
    {
        $this->fileRepository->delete($id);
        
        $file = $this->fileRepository->get($id);
        $fullPath = $this->locator->getFullPath($file);
        
        
        
        if (file_exists($fullPath)) {
            unlink($fullPath);
            $message = 'Файл удален. ';
        } else {
            throw new NotFoundException();
        }

        try {
            $this->flusher->flush();
            $message .= 'Запись удалена';
        } catch (ForeignKeyConstraintViolationException $exception) {
            $message = 'File имеет внешние ключи не может быть удален';
        }

        return ['message' => $message];
    }
}
