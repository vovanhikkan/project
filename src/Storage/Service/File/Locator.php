<?php

declare(strict_types=1);

namespace App\Storage\Service\File;

use App\Storage\Model\File\File;

/**
 * Locator.
 */
class Locator
{
    private string $storagePath;
    private string $storageUrl;

    public function __construct()
    {
        $this->storagePath = getenv('STORAGE_PATH');
        $this->storageUrl = getenv('STORAGE_URL');
    }

    public function getFullPath(File $file): string
    {
        return $this->storagePath . DIRECTORY_SEPARATOR . (string)$file->getFilePath();
    }

    public function getAbsoluteUrl(File $file): string
    {
        return $this->storageUrl . '/' . (string)$file->getFilePath();
    }

    public function getFileContent(File $file): ?string
    {
        return file_get_contents($this->storagePath . DIRECTORY_SEPARATOR . $file->getFilePath()) ?
            file_get_contents($this->storagePath . DIRECTORY_SEPARATOR . $file->getFilePath()) :
            null;
    }
}
