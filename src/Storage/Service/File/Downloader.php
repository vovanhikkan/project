<?php

declare(strict_types=1);

namespace App\Storage\Service\File;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Storage\Model\File\File;
use App\Storage\Model\File\MimeType;
use App\Storage\Model\File\Name;
use App\Storage\Model\File\Size;
use App\Storage\Model\File\Type;
use App\Storage\Repository\FileRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use InvalidArgumentException;
use RuntimeException;

/**
 * Downloader.
 */
class Downloader
{
    private FileRepository $fileRepository;
    private FileNameGenerator $fileNameGenerator;
    private FilePathGenerator $filePathGenerator;
    private Locator $locator;
    private Flusher $flusher;

    public function __construct(
        FileRepository $fileRepository,
        FileNameGenerator $fileNameGenerator,
        FilePathGenerator $filePathGenerator,
        Locator $locator,
        Flusher $flusher
    ) {
        $this->fileRepository = $fileRepository;
        $this->fileNameGenerator = $fileNameGenerator;
        $this->filePathGenerator = $filePathGenerator;
        $this->locator = $locator;
        $this->flusher = $flusher;
    }

    public function download(string $url): File
    {
        try {
            $response = (new Client())->get($url);
        } catch (RequestException $e) {
            throw new RuntimeException('Не удалось скачать файл');
        }

        $data = $response->getBody()->getContents();
        $finfo = finfo_open();
        $mimeType = (string)finfo_buffer($finfo, $data, FILEINFO_MIME_TYPE);
        finfo_close($finfo);

        $extension = null;
        switch ($mimeType) {
            case 'image/jpeg':
            case 'image/jpg':
                $extension = 'jpg';
                break;
            case 'image/png':
                $extension = 'png';
                break;
            default:
                throw new InvalidArgumentException('Не удалось определить тип ' . $mimeType);
        }

        $fileName = $this->fileNameGenerator->generate($extension);
        $filePath = $this->filePathGenerator->generate($fileName);

        $file = new File(
            Uuid::generate(),
            Type::image(),
            new Name(Uuid::generate()->getValue()),
            new MimeType($mimeType),
            new Size(strlen($data)),
            $fileName,
            $filePath
        );
        $this->fileRepository->add($file);

        $fullPath = $this->locator->getFullPath($file);
        $this->ensurePath(dirname($fullPath));
        file_put_contents($fullPath, $data);

        $this->flusher->flush();

        return $file;
    }

    private function ensurePath(string $path): void
    {
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
    }
}
