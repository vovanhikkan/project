<?php

declare(strict_types=1);

namespace App\Storage\Model\File;

use App\Application\ValueObject\StringValueObject;

/**
 * Name.
 */
final class Name extends StringValueObject
{
}
