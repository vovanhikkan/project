<?php

declare(strict_types=1);

namespace App\Storage\Model\File;

use App\Application\ValueObject\StringValueObject;

/**
 * FilePath.
 */
final class FilePath extends StringValueObject
{
}
