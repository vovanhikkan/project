<?php

declare(strict_types=1);

namespace App\Storage\Model\File;

use App\Application\ValueObject\EnumValueObject;

/**
 * Type.
 *
 * @method static image()
 * @method static video()
 */
final class Type extends EnumValueObject
{
    const IMAGE = 100;
    const VIDEO = 200;
}
