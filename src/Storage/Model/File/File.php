<?php

declare(strict_types=1);

namespace App\Storage\Model\File;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * File.
 *
 * @ORM\Entity()
 * @ORM\Table(name="file")
 */
class File
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="storage_file_type", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Type $type;

    /**
     * @ORM\Column(type="storage_file_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="storage_file_mime_type")
     */
    private MimeType $mimeType;

    /**
     * @ORM\Column(type="storage_file_size", columnDefinition="BIGINT(20) UNSIGNED NOT NULL")
     */
    private Size $size;

    /**
     * @ORM\Column(type="storage_file_file_name")
     */
    private FileName $fileName;

    /**
     * @ORM\Column(type="storage_file_file_path")
     */
    private FilePath $filePath;

    public function __construct(
        Uuid $id,
        Type $type,
        Name $name,
        MimeType $mimeType,
        Size $size,
        FileName $fileName,
        FilePath $filePath
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->name = $name;
        $this->mimeType = $mimeType;
        $this->size = $size;
        $this->fileName = $fileName;
        $this->filePath = $filePath;
        $this->createdAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getMimeType(): MimeType
    {
        return $this->mimeType;
    }

    public function getSize(): Size
    {
        return $this->size;
    }

    public function getFileName(): FileName
    {
        return $this->fileName;
    }

    public function getFilePath(): FilePath
    {
        return $this->filePath;
    }
}
