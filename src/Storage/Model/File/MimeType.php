<?php

declare(strict_types=1);

namespace App\Storage\Model\File;

use App\Application\ValueObject\StringValueObject;

/**
 * MimeType.
 */
final class MimeType extends StringValueObject
{
}
