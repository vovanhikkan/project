<?php

declare(strict_types=1);

namespace App\Storage\Model\File;

use App\Application\ValueObject\IntegerValueObject;

/**
 * Size.
 */
final class Size extends IntegerValueObject
{
}
