<?php

declare(strict_types=1);

namespace App\Storage\Model\Video;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Video.
 *
 * @ORM\Entity()
 * @ORM\Table(name="video")
 */
class Video
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="storage_video_youtube_code")
     */
    private YoutubeCode $youtubeCode;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="background_image_id", referencedColumnName="id", nullable=false)
     */
    private File $backgroundImage;

    /**
     * Video constructor.
     * @param Uuid $id
     * @param YoutubeCode $youtubeCode
     * @param File $backgroundImage
     */
    public function __construct(Uuid $id, YoutubeCode $youtubeCode, File $backgroundImage)
    {
        $this->id = $id;
        $this->youtubeCode = $youtubeCode;
        $this->backgroundImage = $backgroundImage;
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return YoutubeCode
     */
    public function getYoutubeCode(): YoutubeCode
    {
        return $this->youtubeCode;
    }

    /**
     * @return File
     */
    public function getBackgroundImage(): File
    {
        return $this->backgroundImage;
    }
}
