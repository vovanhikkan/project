<?php

declare(strict_types=1);

namespace App\Storage\Model\Video;

use App\Application\ValueObject\StringValueObject;

/**
 * YoutubeCode.
 */
final class YoutubeCode extends StringValueObject
{
}
