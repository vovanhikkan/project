<?php

declare(strict_types=1);

namespace App\Storage\Repository;

use App\Application\Repository\AbstractRepository;
use App\Storage\Model\Video\Video;

/**
 * VideoRepository.
 */
class VideoRepository extends AbstractRepository
{
    public function add(Video $file): void
    {
        $this->entityManager->persist($file);
    }

    protected function getModelClassName(): string
    {
        return Video::class;
    }
}
