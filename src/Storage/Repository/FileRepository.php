<?php

declare(strict_types=1);

namespace App\Storage\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Storage\Dto\SearchDto;
use App\Storage\Model\File\File;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * FileRepository.
 */
class FileRepository extends AbstractRepository
{
    

    public function add(File $file): void
    {
        $this->entityManager->persist($file);
    }

    public function getFileOrNull(?string $id): ?File
    {
        if ($id === null) {
            return null;
        }

        /** @var File|null $model */
        $model = $this->entityRepository->find(new Uuid($id));

        return $model;
    }

    public function get(Uuid $id): File
    {
        /** @var File|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('File with id %s not found', (string)$id));
        }

        return $model;
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    public function searchFiles(SearchDto $searchDto)
    {
        $queryBuilder = $this->entityRepository->createQueryBuilder('f');
        
        if ($name = $searchDto->getName()) {
            $queryBuilder->where('f.name LIKE :name')
                ->setParameter('name', '%' . $name . '%');
        }

        $queryBuilder->orderBy('f.createdAt', 'DESC');
        
        if (!$limit = $searchDto->getLimit()) {
            $limit = $this->entityRepository->createQueryBuilder('f')
                ->select('COUNT(f.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
            
            
        $currentPage = $searchDto->getPage() ? : 1;
        $paginator = new Paginator($queryBuilder);
        $paginator->getQuery()
            ->setFirstResult($limit * ($currentPage - 1))
            ->setMaxResults($limit);
            
        return $paginator->getQuery()->getResult();
    }
    

    /**
     * @return int|mixed|string|null
     */
    public function getResult()
    {
        if ($this->paginator) {
            return $this->paginator->getQuery()->getResult();
        }

        if ($this->queryBuilder) {
            return $this->queryBuilder->getQuery()->getResult();
        }

        return null;
    }



    protected function getModelClassName(): string
    {
        return File::class;
    }
}
