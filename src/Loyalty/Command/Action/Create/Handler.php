<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Action\Create;

use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\Action\ActivatedCount;
use App\Loyalty\Model\Action\CashbackPointAbsoluteValue;
use App\Loyalty\Model\Action\CashbackPointPercentageValue;
use App\Loyalty\Model\Action\Description;
use App\Loyalty\Model\Action\DiscountAbsoluteValue;
use App\Loyalty\Model\Action\DiscountPercentageValue;
use App\Loyalty\Model\Action\HoldedCount;
use App\Loyalty\Model\Action\MaxDayCount;
use App\Loyalty\Model\Action\MaxProductQuantity;
use App\Loyalty\Model\Action\MinDayCount;
use App\Loyalty\Model\Action\MinProductQuantity;
use App\Loyalty\Model\Action\Name;
use App\Loyalty\Model\Action\OrderedCount;
use App\Loyalty\Model\Action\PromocodeLifetime;
use App\Loyalty\Model\Action\PromocodePrefix;
use App\Loyalty\Model\Action\TotalCount;
use App\Loyalty\Repository\ActionGroupRepository;
use App\Loyalty\Repository\ActionRepository;
use App\Loyalty\Service\Action\Creator;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private ActionRepository $actionRepository;
    private ActionGroupRepository $actionGroupRepository;
    private Creator $creator;

    public function __construct(
        ActionRepository $actionRepository,
        Creator $creator,
        ActionGroupRepository $actionGroupRepository
    ) {
        $this->actionRepository = $actionRepository;
        $this->actionGroupRepository = $actionGroupRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): Action
    {
        return $this->creator->create(
            new Name($command->getName()),
            new Description($command->getDescription()),
            new DateTimeImmutable($command->getShownFrom()),
            new DateTimeImmutable($command->getShownTo()),
            $command->getOrderedFrom() !== null ? new DateTimeImmutable($command->getOrderedFrom()) : null,
            $command->getOrderedTo() !== null ? new DateTimeImmutable($command->getOrderedTo()) : null,
            $command->getDiscountAbsoluteValue() !== null ?
                new DiscountAbsoluteValue($command->getDiscountAbsoluteValue()) : null,
            $command->getDiscountPercentageValue() !== null ?
                new DiscountPercentageValue($command->getDiscountPercentageValue()) : null,
            $command->getCashbackPointAbsoluteValue() !== null ?
                new CashbackPointAbsoluteValue($command->getCashbackPointAbsoluteValue()) : null,
            $command->getCashbackPointPercentageValue() !== null ?
                new CashbackPointPercentageValue($command->getCashbackPointPercentageValue()) : null,
            $command->getMinDayCount() !== null ? new MinDayCount($command->getMinDayCount()) : null,
            $command->getMaxDayCount() !== null ? new MaxDayCount($command->getMaxDayCount()) : null,
            $command->getFirstBookingDayFrom() !== null ?
                new DateTimeImmutable($command->getFirstBookingDayFrom()) : null,
            $command->getFirstBookingDayTo() !== null ? new DateTimeImmutable($command->getFirstBookingDayTo()) : null,
            $command->getLastBookingDayFrom() !== null ?
                new DateTimeImmutable($command->getLastBookingDayFrom()) : null,
            $command->getLastBookingDayTo() !== null ? new DateTimeImmutable($command->getLastBookingDayTo()) : null,
            $command->getMinProductQuantity() !== null ?
                new MinProductQuantity($command->getMinProductQuantity()) : null,
            $command->getMaxProductQuantity() !== null ?
                new MaxProductQuantity($command->getMaxProductQuantity()) : null,
            $command->getProductActivationFrom() !== null ?
                new DateTimeImmutable($command->getProductActivationFrom()) : null,
            $command->getProductActivationTo() !== null ?
                new DateTimeImmutable($command->getProductActivationTo()) : null,
            $command->isHasLevels(),
            $command->isHasPromocodes(),
            $command->getPromocodeLifetime() !== null ? new PromocodeLifetime($command->getPromocodeLifetime()) : null,
            $command->getPromocodePrefix() !== null ? new PromocodePrefix($command->getPromocodePrefix()) : null,
            $command->getTotalCount() !== null ? new TotalCount($command->getTotalCount()) : null,
            $command->getHoldedCount() !== null ? new HoldedCount($command->getHoldedCount()) : null,
            $command->getOrderedCount() !== null ? new OrderedCount($command->getOrderedCount()) : null,
            $command->getBundleActivationFrom() !== null ?
                new DateTimeImmutable($command->getBundleActivationFrom()) : null,
            $command->getBundleActivationTo() !== null ?
                new DateTimeImmutable($command->getBundleActivationTo()) : null,
            $this->actionGroupRepository->get(new Uuid($command->getGroup()))
        );
    }
}
