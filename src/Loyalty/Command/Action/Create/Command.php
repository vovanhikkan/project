<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Action\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните название")
     */
    private array $name;

    /**
     * @Assert\NotBlank()
     */
    private array $description;

    /**
     * @Assert\NotBlank()
     */
    private string $shownFrom;

    /**
     * @Assert\NotBlank()
     */
    private string $shownTo;

    private ?string $orderedFrom = null;

    private ?string $orderedTo = null;

    private ?int $discountAbsoluteValue = null;

    private ?int $discountPercentageValue = null;

    private ?int $cashbackPointAbsoluteValue = null;

    private ?int $cashbackPointPercentageValue = null;

    private ?int $minDayCount = null;

    private ?int $maxDayCount = null;

    private ?string $firstBookingDayFrom = null;

    private ?string $firstBookingDayTo = null;

    private ?string $lastBookingDayFrom = null;

    private ?string $lastBookingDayTo = null;

    private ?int $minProductQuantity = null;

    private ?int $maxProductQuantity = null;

    private ?string $productActivationFrom = null;

    private ?string $productActivationTo = null;


    private bool $hasLevels;
    private bool $hasPromocodes;

    private ?int $promocodeLifetime = null;

    private ?string $promocodePrefix = null;

    private ?int $totalCount = null;

    private ?int $holdedCount = null;

    private ?int $orderedCount = null;

    private ?string $bundleActivationFrom = null;

    private ?string $bundleActivationTo = null;

    /**
     * @Assert\NotBlank()
     */
    private string $group;

    /**
     * Command constructor.
     * @param array $name
     * @param array $description
     * @param string $shownFrom
     * @param string $shownTo
     * @param bool $hasLevels
     * @param bool $hasPromocodes
     * @param string $group
     */
    public function __construct(
        array $name,
        array $description,
        string $shownFrom,
        string $shownTo,
        bool $hasLevels,
        bool $hasPromocodes,
        string $group
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->shownFrom = $shownFrom;
        $this->shownTo = $shownTo;
        $this->hasLevels = $hasLevels;
        $this->hasPromocodes = $hasPromocodes;
        $this->group = $group;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return array|null
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getShownFrom(): string
    {
        return $this->shownFrom;
    }

    /**
     * @return string
     */
    public function getShownTo(): string
    {
        return $this->shownTo;
    }

    /**
     * @return string|null
     */
    public function getOrderedFrom(): ?string
    {
        return $this->orderedFrom;
    }

    /**
     * @return string|null
     */
    public function getOrderedTo(): ?string
    {
        return $this->orderedTo;
    }

    /**
     * @return int|null
     */
    public function getDiscountAbsoluteValue(): ?int
    {
        return $this->discountAbsoluteValue;
    }

    /**
     * @return int|null
     */
    public function getDiscountPercentageValue(): ?int
    {
        return $this->discountPercentageValue;
    }

    /**
     * @return int|null
     */
    public function getCashbackPointAbsoluteValue(): ?int
    {
        return $this->cashbackPointAbsoluteValue;
    }

    /**
     * @return int|null
     */
    public function getCashbackPointPercentageValue(): ?int
    {
        return $this->cashbackPointPercentageValue;
    }

    /**
     * @return int|null
     */
    public function getMinDayCount(): ?int
    {
        return $this->minDayCount;
    }

    /**
     * @return int|null
     */
    public function getMaxDayCount(): ?int
    {
        return $this->maxDayCount;
    }

    /**
     * @return string|null
     */
    public function getFirstBookingDayTo(): ?string
    {
        return $this->firstBookingDayTo;
    }

    /**
     * @return string|null
     */
    public function getFirstBookingDayFrom(): ?string
    {
        return $this->firstBookingDayFrom;
    }

    /**
     * @return string|null
     */
    public function getLastBookingDayFrom(): ?string
    {
        return $this->lastBookingDayFrom;
    }

    /**
     * @return string|null
     */
    public function getLastBookingDayTo(): ?string
    {
        return $this->lastBookingDayTo;
    }

    /**
     * @return int|null
     */
    public function getMinProductQuantity(): ?int
    {
        return $this->minProductQuantity;
    }

    /**
     * @return int|null
     */
    public function getMaxProductQuantity(): ?int
    {
        return $this->maxProductQuantity;
    }

    /**
     * @return string|null
     */
    public function getProductActivationFrom(): ?string
    {
        return $this->productActivationFrom;
    }

    /**
     * @return string|null
     */
    public function getProductActivationTo(): ?string
    {
        return $this->productActivationTo;
    }

    /**
     * @return bool
     */
    public function isHasLevels(): bool
    {
        return $this->hasLevels;
    }

    /**
     * @return bool
     */
    public function isHasPromocodes(): bool
    {
        return $this->hasPromocodes;
    }

    /**
     * @return int|null
     */
    public function getPromocodeLifetime(): ?int
    {
        return $this->promocodeLifetime;
    }

    /**
     * @return string|null
     */
    public function getPromocodePrefix(): ?string
    {
        return $this->promocodePrefix;
    }

    /**
     * @return int|null
     */
    public function getTotalCount(): ?int
    {
        return $this->totalCount;
    }

    /**
     * @return int|null
     */
    public function getHoldedCount(): ?int
    {
        return $this->holdedCount;
    }

    /**
     * @return int|null
     */
    public function getOrderedCount(): ?int
    {
        return $this->orderedCount;
    }

    /**
     * @return string|null
     */
    public function getBundleActivationFrom(): ?string
    {
        return $this->bundleActivationFrom;
    }

    /**
     * @return string|null
     */
    public function getBundleActivationTo(): ?string
    {
        return $this->bundleActivationTo;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @param string|null $orderedFrom
     */
    public function setOrderedFrom(?string $orderedFrom): void
    {
        $this->orderedFrom = $orderedFrom;
    }

    /**
     * @param string|null $orderedTo
     */
    public function setOrderedTo(?string $orderedTo): void
    {
        $this->orderedTo = $orderedTo;
    }

    /**
     * @param int|null $discountAbsoluteValue
     */
    public function setDiscountAbsoluteValue(?int $discountAbsoluteValue): void
    {
        $this->discountAbsoluteValue = $discountAbsoluteValue;
    }

    /**
     * @param int|null $discountPercentageValue
     */
    public function setDiscountPercentageValue(?int $discountPercentageValue): void
    {
        $this->discountPercentageValue = $discountPercentageValue;
    }

    /**
     * @param int|null $cashbackPointAbsoluteValue
     */
    public function setCashbackPointAbsoluteValue(?int $cashbackPointAbsoluteValue): void
    {
        $this->cashbackPointAbsoluteValue = $cashbackPointAbsoluteValue;
    }

    /**
     * @param int|null $cashbackPointPercentageValue
     */
    public function setCashbackPointPercentageValue(?int $cashbackPointPercentageValue): void
    {
        $this->cashbackPointPercentageValue = $cashbackPointPercentageValue;
    }

    /**
     * @param int|null $minDayCount
     */
    public function setMinDayCount(?int $minDayCount): void
    {
        $this->minDayCount = $minDayCount;
    }

    /**
     * @param int|null $maxDayCount
     */
    public function setMaxDayCount(?int $maxDayCount): void
    {
        $this->maxDayCount = $maxDayCount;
    }

    /**
     * @param string|null $firstBookingDayFrom
     */
    public function setFirstBookingDayFrom(?string $firstBookingDayFrom): void
    {
        $this->firstBookingDayFrom = $firstBookingDayFrom;
    }

    /**
     * @param string|null $firstBookingDayTo
     */
    public function setFirstBookingDayTo(?string $firstBookingDayTo): void
    {
        $this->firstBookingDayTo = $firstBookingDayTo;
    }

    /**
     * @param string|null $lastBookingDayFrom
     */
    public function setLastBookingDayFrom(?string $lastBookingDayFrom): void
    {
        $this->lastBookingDayFrom = $lastBookingDayFrom;
    }

    /**
     * @param string|null $lastBookingDayTo
     */
    public function setLastBookingDayTo(?string $lastBookingDayTo): void
    {
        $this->lastBookingDayTo = $lastBookingDayTo;
    }

    /**
     * @param int|null $minProductQuantity
     */
    public function setMinProductQuantity(?int $minProductQuantity): void
    {
        $this->minProductQuantity = $minProductQuantity;
    }

    /**
     * @param int|null $maxProductQuantity
     */
    public function setMaxProductQuantity(?int $maxProductQuantity): void
    {
        $this->maxProductQuantity = $maxProductQuantity;
    }

    /**
     * @param string|null $productActivationFrom
     */
    public function setProductActivationFrom(?string $productActivationFrom): void
    {
        $this->productActivationFrom = $productActivationFrom;
    }

    /**
     * @param string|null $productActivationTo
     */
    public function setProductActivationTo(?string $productActivationTo): void
    {
        $this->productActivationTo = $productActivationTo;
    }

    /**
     * @param int|null $promocodeLifetime
     */
    public function setPromocodeLifetime(?int $promocodeLifetime): void
    {
        $this->promocodeLifetime = $promocodeLifetime;
    }

    /**
     * @param string|null $promocodePrefix
     */
    public function setPromocodePrefix(?string $promocodePrefix): void
    {
        $this->promocodePrefix = $promocodePrefix;
    }

    /**
     * @param int|null $totalCount
     */
    public function setTotalCount(?int $totalCount): void
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @param int|null $holdedCount
     */
    public function setHoldedCount(?int $holdedCount): void
    {
        $this->holdedCount = $holdedCount;
    }

    /**
     * @param int|null $orderedCount
     */
    public function setOrderedCount(?int $orderedCount): void
    {
        $this->orderedCount = $orderedCount;
    }

    /**
     * @param string|null $bundleActivationFrom
     */
    public function setBundleActivationFrom(?string $bundleActivationFrom): void
    {
        $this->bundleActivationFrom = $bundleActivationFrom;
    }

    /**
     * @param string|null $bundleActivationTo
     */
    public function setBundleActivationTo(?string $bundleActivationTo): void
    {
        $this->bundleActivationTo = $bundleActivationTo;
    }

}
