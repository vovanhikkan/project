<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Activate;

use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Repository\PromocodeRepository;
use App\Loyalty\Service\Promocode\Updater;
use App\Order\Repository\CartRepository;

/**
 * Handler.
 */
class Handler
{
    private PromocodeRepository $promocodeRepository;
    private CartRepository $cartRepository;
    private Updater $updater;

    public function __construct(
        PromocodeRepository $promocodeRepository,
        CartRepository $cartRepository,
        Updater $updater
    ) {
        $this->promocodeRepository = $promocodeRepository;
        $this->cartRepository = $cartRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): ?Promocode
    {
        /**
         * @var Promocode $promocode
         */
        $promocode = $this->promocodeRepository->getByValue($command->getValue());
        $cart = $this->cartRepository->getActiveByUser($command->getUser());

        $this->updater->addPromocode($cart, $promocode);


        return $promocode;
    }
}
