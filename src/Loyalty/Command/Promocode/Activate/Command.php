<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Activate;

use App\Auth\Model\User\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Введите промокод")
     */
    private string $value;
    private ?User $user;

    /**
     * Command constructor.
     * @param string $value
     * @param User|null $user
     */
    public function __construct(string $value, ?User $user)
    {
        $this->value = $value;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }
}
