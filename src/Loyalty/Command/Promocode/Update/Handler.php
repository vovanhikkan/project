<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Update;

use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Promocode\HoldedCount;
use App\Loyalty\Model\Promocode\OrderedCount;
use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Model\Promocode\TotalCount;
use App\Loyalty\Model\Promocode\Value;
use App\Loyalty\Repository\ActionRepository;
use App\Loyalty\Repository\PromocodeRepository;
use App\Loyalty\Service\Promocode\Updater;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private PromocodeRepository $promocodeRepository;
    private ActionRepository $actionRepository;
    private Updater $updater;

    public function __construct(
        PromocodeRepository $promocodeRepository,
        ActionRepository $actionRepository,
        Updater $updater
    ) {
        $this->promocodeRepository = $promocodeRepository;
        $this->actionRepository = $actionRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): Promocode
    {
        $promocode = $this->promocodeRepository->get(new Uuid($command->getId()));

        $this->updater->update(
            $promocode,
            $command->getAction() ? $this->actionRepository->get(new Uuid($command->getAction())) : null,
            $command->getValue() ? new Value($command->getValue()) : null,
            $command->getTotalCount() ? new TotalCount($command->getTotalCount()) : null,
            $command->getHoldedCount() ? new HoldedCount($command->getHoldedCount()) : null,
            $command->getOrderedCount() ? new OrderedCount($command->getOrderedCount()) : null,
            $command->getExpiredAt() ? new DateTimeImmutable($command->getExpiredAt()) : null,
        );

        return $promocode;
    }
}
