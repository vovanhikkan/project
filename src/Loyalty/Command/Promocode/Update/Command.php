<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{

    /**
     * @Assert\NotBlank()
     */
    private string $id;

    private ?string $action;

    private ?string $value;

    private ?int $totalCount;

    private ?int $holdedCount;

    private ?int $orderedCount;

    private ?int $expiredAt;

    /**
     * Command constructor.
     * @param string $id
     * @param string|null $action
     * @param string|null $value
     * @param int|null $totalCount
     * @param int|null $holdedCount
     * @param int|null $orderedCount
     * @param int|null $expiredAt
     */
    public function __construct(string $id, ?string $action, ?string $value, ?int $totalCount, ?int $holdedCount, ?int $orderedCount, ?int $expiredAt)
    {
        $this->id = $id;
        $this->action = $action;
        $this->value = $value;
        $this->totalCount = $totalCount;
        $this->holdedCount = $holdedCount;
        $this->orderedCount = $orderedCount;
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @return int|null
     */
    public function getTotalCount(): ?int
    {
        return $this->totalCount;
    }

    /**
     * @return int|null
     */
    public function getHoldedCount(): ?int
    {
        return $this->holdedCount;
    }

    /**
     * @return int|null
     */
    public function getOrderedCount(): ?int
    {
        return $this->orderedCount;
    }

    /**
     * @return int|null
     */
    public function getExpiredAt(): ?int
    {
        return $this->expiredAt;
    }
}
