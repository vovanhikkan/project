<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Create;

use App\Application\ValueObject\DateTime;
use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Promocode\TotalCount;
use App\Loyalty\Model\Promocode\HoldedCount;
use App\Loyalty\Model\Promocode\OrderedCount;
use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Model\Promocode\Value;
use App\Loyalty\Repository\ActionRepository;
use App\Loyalty\Repository\PromocodeRepository;
use App\Loyalty\Service\Promocode\Creator;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private PromocodeRepository $promocodeRepository;
    private ActionRepository $actionRepository;
    private Creator $creator;

    public function __construct(PromocodeRepository $promocodeRepository, Creator $creator, ActionRepository $actionRepository)
    {
        $this->promocodeRepository = $promocodeRepository;
        $this->actionRepository = $actionRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): Promocode
    {
        return $this->creator->create(
            $this->actionRepository->get(new Uuid($command->getAction())),
            new Value($command->getValue()),
            new TotalCount($command->getTotalCount()),
            new HoldedCount($command->getHoldedCount()),
            new OrderedCount($command->getOrderedCount()),
            $command->getExpiredAt() ? new DateTimeImmutable($command->getExpiredAt()) : null,
        );
    }
}
