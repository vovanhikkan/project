<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Generate;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните акцию")
     */
    private string $action;

    private ?int $totalCount;

    /**
     * Command constructor.
     * @param string $action
     * @param int|null $totalCount
     */
    public function __construct(string $action, ?int $totalCount)
    {
        $this->action = $action;
        $this->totalCount = $totalCount;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return int|null
     */
    public function getTotalCount(): ?int
    {
        return $this->totalCount;
    }
}
