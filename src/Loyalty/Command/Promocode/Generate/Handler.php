<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Generate;

use App\Application\ValueObject\DateTime;
use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Promocode\TotalCount;
use App\Loyalty\Model\Promocode\HoldedCount;
use App\Loyalty\Model\Promocode\OrderedCount;
use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Model\Promocode\Value;
use App\Loyalty\Repository\ActionRepository;
use App\Loyalty\Repository\PromocodeRepository;
use App\Loyalty\Service\Promocode\Creator;

/**
 * Handler.
 */
class Handler
{
    private PromocodeRepository $promocodeRepository;
    private ActionRepository $actionRepository;
    private Creator $creator;

    public function __construct(PromocodeRepository $promocodeRepository, Creator $creator, ActionRepository $actionRepository)
    {
        $this->promocodeRepository = $promocodeRepository;
        $this->actionRepository = $actionRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): array
    {
        $result = [];
        $action = $this->actionRepository->get(new Uuid($command->getAction()));
        $result[] = $this->creator->create(
            $action,
            new Value($this->randomCode($action->getPromocodePrefix())),
            new TotalCount($command->getTotalCount()),
            new HoldedCount(0),
            new OrderedCount(0),
            null
        );

        return $result;
    }

    private function randomCode($prefix): string {
        $characters = '23456789ABCEFGHKMNPRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
