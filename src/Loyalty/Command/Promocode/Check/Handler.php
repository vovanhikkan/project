<?php

declare(strict_types=1);

namespace App\Loyalty\Command\Promocode\Check;

use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Repository\PromocodeRepository;

/**
 * Handler.
 */
class Handler
{
    private PromocodeRepository $promocodeRepository;

    public function __construct(PromocodeRepository $promocodeRepository)
    {
        $this->promocodeRepository = $promocodeRepository;
    }

    public function handle(Command $command): ?Promocode
    {
        /**
         * @var Promocode $promocode
         */
        $promocode = $this->promocodeRepository->getByValue($command->getValue());

        if (!$promocode) {
            return null;
        }

        $action = $promocode->getAction();

        if (!$action->isActive()) {
            return null;
        }

        if ($action->isHasLevels()) {
            if (!$command->getUser()) {
                return null;
            }
            $userLevel = $command->getUser()->getLevel();

            if (!in_array($userLevel, $action->getLevels())) {
                return null;
            }
        }

        return $promocode;
    }
}
