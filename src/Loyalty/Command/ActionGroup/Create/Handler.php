<?php

declare(strict_types=1);

namespace App\Loyalty\Command\ActionGroup\Create;

use App\Application\ValueObject\Sort;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\Loyalty\Model\ActionGroup\Name;
use App\Loyalty\Repository\ActionGroupRepository;
use App\Loyalty\Service\ActionGroup\Creator;

/**
 * Handler.
 */
class Handler
{
    private ActionGroupRepository $actionGroupRepository;
    private Creator $creator;

    public function __construct(ActionGroupRepository $actionGroupRepository, Creator $creator)
    {
        $this->actionGroupRepository = $actionGroupRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): ActionGroup
    {
        return $this->creator->create(
            new Name($command->getName())
        );
    }
}
