<?php

declare(strict_types=1);

namespace App\Loyalty\Command\ActionGroup\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните название")
     */
    private array $name;

    /**
     * Command constructor.
     * @param array $name
     */
    public function __construct(array $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }
}
