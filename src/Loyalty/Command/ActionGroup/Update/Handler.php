<?php

declare(strict_types=1);

namespace App\Loyalty\Command\ActionGroup\Update;

use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use App\Loyalty\Model\ActionGroup\Name;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\Loyalty\Repository\ActionGroupRepository;
use App\Loyalty\Service\ActionGroup\Updater;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private ActionGroupRepository $actionGroupRepository;
    private Updater $updater;

    public function __construct(
        ActionGroupRepository $actionGroupRepository,
        Updater $updater
    ) {
        $this->actionGroupRepository = $actionGroupRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): ActionGroup
    {
        $actionGroup = $this->actionGroupRepository->get(new Uuid($command->getId()));

        $this->updater->update(
            $actionGroup,
            new Name($command->getName()),
            new Sort($command->getSort()),
        );

        return $actionGroup;
    }
}
