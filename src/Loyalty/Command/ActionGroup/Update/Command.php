<?php

declare(strict_types=1);

namespace App\Loyalty\Command\ActionGroup\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;

    private ?array $name;

    private ?int $sort;

    /**
     * Command constructor.
     * @param string $id
     * @param string $name
     * @param int $sort
     */
    public function __construct(string $id, ?array $name, ?int $sort)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }
}
