<?php

declare(strict_types=1);

namespace App\Loyalty\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\ActionGroup\ActionGroup;

/**
 * ActionGroupRepository.
 */
class ActionGroupRepository extends AbstractRepository
{
    public function add(ActionGroup $actionGroup): void
    {
        $this->entityManager->persist($actionGroup);
    }

    /**
     * @return ActionGroup[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('a');
        $qb->orderBy('a.sort', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): ActionGroup
    {
        /** @var ActionGroup|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Action Group with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('ag')
            ->select('MAX(ag.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return ActionGroup::class;
    }
}
