<?php

declare(strict_types=1);

namespace App\Loyalty\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\ActionLevel\ActionLevel;
use App\Loyalty\Model\ActionProduct\ActionProduct;
use App\Loyalty\Model\Level\Level;
use App\Product\Model\Product\Product;
use Doctrine\ORM\Query\Expr\Join;

/**
 * ActionRepository.
 */
class ActionRepository extends AbstractRepository
{
    public function add(Action $actionGroup): void
    {
        $this->entityManager->persist($actionGroup);
    }

    /**
     * @return Action[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('a');
        $qb->orderBy('a.sort', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Action
    {
        /** @var Action|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Action with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getActionsByActivationDateAndLevelAndProduct(string $activationDate, ?Level $level, Product $product): array
    {
        $query = $this->entityRepository->createQueryBuilder('a')
            ->leftJoin(ActionLevel::class, 'al', Join::WITH, 'al.action = a.id')
            ->leftJoin(Level::class, 'l', Join::WITH, 'al.level = l.id')
            ->leftJoin(ActionProduct::class, 'ap', Join::WITH, 'ap.action = a.id')
            ->leftJoin(Product::class, 'p', Join::WITH, 'ap.product = p.id')
            ->orderBy('a.sort', 'DESC')
            ->andWhere(':activationDate between a.productActivationFrom AND a.productActivationTo')
            ->andWhere('a.isActive = 1')
            ->andWhere('p.id = :product')
            ->setParameter('activationDate', $activationDate)
            ->setParameter('product', $product->getId()->getValue());

        if ($level) {
            $query->andWhere('a.hasLevels = 0 OR l.id = :level')
                ->setParameter('level', (string)$level->getId());
        } else {
            $query->andWhere('a.hasLevels = 0');
        }
        return $query->getQuery()->getResult();
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('a')
            ->select('MAX(a.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return Action::class;
    }
}
