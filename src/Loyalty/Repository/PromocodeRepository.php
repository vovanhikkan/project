<?php

declare(strict_types=1);

namespace App\Loyalty\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Promocode\Promocode;

/**
 * PromocodeRepository.
 */
class PromocodeRepository extends AbstractRepository
{
    public function add(Promocode $promocode): void
    {
        $this->entityManager->persist($promocode);
    }

    /**
     * @return PromocodeRepository[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('a');
        $qb->orderBy('a.value', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Promocode
    {
        /** @var Promocode|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Promocode with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getByValue(string $value)
    {
        $qb = $this->entityRepository
            ->createQueryBuilder('p')
            ->where(':value = p.value')
            ->setParameter('value', $value);

        $model = $qb->getQuery()->getOneOrNullResult();

        if ($model === null) {
            throw new NotFoundException(sprintf('Promocode with value ' . $value . ' not found'));
        }

        return $model;
    }

    protected function getModelClassName(): string
    {
        return Promocode::class;
    }
}
