<?php

declare(strict_types=1);

namespace App\Loyalty\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\ActionLevel\ActionLevel;
use App\Loyalty\Model\Level\Level;

/**
 * ActionLevelRepository.
 */
class ActionLevelRepository extends AbstractRepository
{
    public function add(ActionLevel $actionGroup): void
    {
        $this->entityManager->persist($actionGroup);
    }

    /**
     * @return ActionLevel[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('a');
        $qb->orderBy('a.sort', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): ActionLevel
    {
        /** @var ActionLevel|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Action Group with id %s not found', (string)$id));
        }

        return $model;
    }

    public function findByActionAndLevel(Uuid $id, Level $level): array
    {
        return $this->entityRepository->findBy(['action' => $id, 'level' => $level->getId()]);
    }

    protected function getModelClassName(): string
    {
        return ActionLevel::class;
    }
}
