<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Promocode;

use App\Application\ValueObject\IntegerValueObject;

/**
 * HoldedCount.
 */
final class HoldedCount extends IntegerValueObject
{
}