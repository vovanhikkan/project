<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Promocode;

use App\Application\ValueObject\StringValueObject;

/**
 * Value.
 */
final class Value extends StringValueObject
{
}