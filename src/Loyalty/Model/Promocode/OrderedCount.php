<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Promocode;

use App\Application\ValueObject\IntegerValueObject;

/**
 * OrderedCount.
 */
final class OrderedCount extends IntegerValueObject
{
}