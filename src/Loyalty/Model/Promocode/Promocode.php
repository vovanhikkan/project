<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Promocode;

use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Action\Action;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;

/**
 * Promocode
 *
 * @ORM\Table(name="promocode")
 * @ORM\Entity()
 */
class Promocode
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Loyalty\Model\Action\Action")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     */
    private Action $action;

    /**
     * @ORM\Column(type="loyalty_promocode_value")
     */
    private Value $value;

    /**
     * @ORM\Column(type="loyalty_promocode_total_count", nullable=true)
     */
    private ?TotalCount $totalCount = null;

    /**
     * @ORM\Column(type="loyalty_promocode_holded_count", nullable=true)
     */
    private ?HoldedCount $holdedCount = null;

    /**
     * @ORM\Column(type="loyalty_promocode_ordered_count", nullable=true)
     */
    private ?OrderedCount $orderedCount = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $expiredAt = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Order\Model\Cart\Cart", mappedBy="promocodes")
     */
    private ?Collection $carts = null;

    /**
     * Promocode constructor.
     * @param Uuid $id
     * @param Action $action
     * @param Value $value
     */
    public function __construct(
        Uuid $id,
        Action $action,
        Value $value
    ) {
        $this->id = $id;
        $this->action = $action;
        $this->value = $value;
    }

    /**
     * @param TotalCount|null $totalCount
     */
    public function setTotalCount(?TotalCount $totalCount): void
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @param HoldedCount|null $holdedCount
     */
    public function setHoldedCount(?HoldedCount $holdedCount): void
    {
        $this->holdedCount = $holdedCount;
    }

    /**
     * @param OrderedCount|null $orderedCount
     */
    public function setOrderedCount(?OrderedCount $orderedCount): void
    {
        $this->orderedCount = $orderedCount;
    }

    /**
     * @param DateTimeImmutable|null $expiredAt
     */
    public function setExpiredAt(?DateTimeImmutable $expiredAt): void
    {
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Action
     */
    public function getAction(): Action
    {
        return $this->action;
    }

    /**
     * @return Value
     */
    public function getValue(): Value
    {
        return $this->value;
    }

    /**
     * @return TotalCount|null
     */
    public function getTotalCount(): ?TotalCount
    {
        return $this->totalCount;
    }

    /**
     * @return HoldedCount|null
     */
    public function getHoldedCount(): ?HoldedCount
    {
        return $this->holdedCount;
    }

    /**
     * @return OrderedCount|null
     */
    public function getOrderedCount(): ?OrderedCount
    {
        return $this->orderedCount;
    }

    /**
     * @return Collection|null
     */
    public function getCarts(): ?Collection
    {
        return $this->carts;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getExpiredAt(): ?DateTimeImmutable
    {
        return $this->expiredAt;
    }

    public function update(?Action $action, ?Value $value, ?TotalCount $totalCount, ?HoldedCount $holdedCount, ?OrderedCount $orderedCount, ?DateTimeImmutable $expiredAt)
    {
        if ($action) {
            $this->action = $action;
        }
        if ($value) {
            $this->value = $value;
        }
        if ($totalCount) {
            $this->totalCount = $totalCount;
        }
        if ($holdedCount) {
            $this->holdedCount = $holdedCount;
        }
        if ($orderedCount) {
            $this->orderedCount = $orderedCount;
        }
        if ($expiredAt) {
            $this->expiredAt = $expiredAt;
        }
    }
}