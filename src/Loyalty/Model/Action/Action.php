<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Action;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;

/**
 * Action
 *
 * @ORM\Entity()
 * @ORM\Table(name="action")
 */
class Action
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="loyalty_action_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="loyalty_action_description", nullable=true)
     */
    private ?Description $description = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $shownFrom;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $shownTo;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $orderedFrom = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $orderedTo = null;

    /**
     * @ORM\Column(type="loyalty_action_discount_absolute_value", nullable=true)
     */
    private ?DiscountAbsoluteValue $discountAbsoluteValue = null;

    /**
     * @ORM\Column(type="loyalty_action_discount_percentage_value", nullable=true)
     */
    private ?DiscountPercentageValue $discountPercentageValue = null;

    /**
     * @ORM\Column(type="loyalty_action_cashback_point_absolute_value", nullable=true)
     */
    private ?CashbackPointAbsoluteValue $cashbackPointAbsoluteValue = null;

    /**
     * @ORM\Column(type="loyalty_action_cashback_point_percentage_value", nullable=true)
     */
    private ?CashbackPointPercentageValue $cashbackPointPercentageValue = null;

    /**
     * @ORM\Column(type="loyalty_action_min_day_count", nullable=true)
     */
    private ?MinDayCount $minDayCount = null;

    /**
     * @ORM\Column(type="loyalty_action_max_day_count", nullable=true)
     */
    private ?MaxDayCount $maxDayCount = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $firstBookingDayFrom = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $firstBookingDayTo = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $lastBookingDayFrom = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $lastBookingDayTo = null;

    /**
     * @ORM\Column(type="loyalty_action_min_product_quantity", nullable=true)
     */
    private ?MinProductQuantity $minProductQuantity = null;

    /**
     * @ORM\Column(type="loyalty_action_max_product_quantity", nullable=true)
     */
    private ?MaxProductQuantity $maxProductQuantity = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $productActivationFrom = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $productActivationTo = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $hasLevels;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $hasPromocodes;

    /**
     * @ORM\Column(type="loyalty_action_promocode_lifetime", nullable=true)
     */
    private ?PromocodeLifetime $promocodeLifetime = null;

    /**
     * @ORM\Column(type="loyalty_action_promocode_prefix", nullable=true)
     */
    private ?PromocodePrefix $promocodePrefix = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="loyalty_action_total_count", nullable=true)
     */
    private ?TotalCount $totalCount = null;

    /**
     * @ORM\Column(type="loyalty_action_holded_count", nullable=true)
     */
    private ?HoldedCount $holdedCount = null;

    /**
     * @ORM\Column(type="loyalty_action_ordered_count", nullable=true)
     */
    private ?OrderedCount $orderedCount = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $bundleActivationFrom = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $bundleActivationTo = null;


    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\ManyToOne(targetEntity="App\Loyalty\Model\ActionGroup\ActionGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=false)
     */
    private ActionGroup $group;

    /**
     * @ORM\ManyToMany(targetEntity="App\Loyalty\Model\Level\Level", inversedBy="actions")
     */
    private ?Collection $levels = null;

    /**
     * Action constructor.
     * @param Uuid $id
     * @param Name $name
     * @param DateTimeImmutable $shownFrom
     * @param DateTimeImmutable $shownTo
     * @param bool $hasLevels
     * @param bool $hasPromocodes
     * @param bool $isActive
     * @param Sort $sort
     * @param ActionGroup $group
     */
    public function __construct(
        Uuid $id,
        Name $name,
        DateTimeImmutable $shownFrom,
        DateTimeImmutable $shownTo,
        bool $hasLevels,
        bool $hasPromocodes,
        bool $isActive,
        Sort $sort,
        ActionGroup $group
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->shownFrom = $shownFrom;
        $this->shownTo = $shownTo;
        $this->hasLevels = $hasLevels;
        $this->hasPromocodes = $hasPromocodes;
        $this->isActive = $isActive;
        $this->sort = $sort;
        $this->group = $group;
    }

    /**
     * @param Description|null $description
     */
    public function setDescription(?Description $description): void
    {
        $this->description = $description;
    }

    /**
     * @param DateTimeImmutable|null $orderedFrom
     */
    public function setOrderedFrom(?DateTimeImmutable $orderedFrom): void
    {
        $this->orderedFrom = $orderedFrom;
    }

    /**
     * @param DateTimeImmutable|null $orderedTo
     */
    public function setOrderedTo(?DateTimeImmutable $orderedTo): void
    {
        $this->orderedTo = $orderedTo;
    }

    /**
     * @param DiscountAbsoluteValue|null $discountAbsoluteValue
     */
    public function setDiscountAbsoluteValue(?DiscountAbsoluteValue $discountAbsoluteValue): void
    {
        $this->discountAbsoluteValue = $discountAbsoluteValue;
    }

    /**
     * @param DiscountPercentageValue|null $discountPercentageValue
     */
    public function setDiscountPercentageValue(?DiscountPercentageValue $discountPercentageValue): void
    {
        $this->discountPercentageValue = $discountPercentageValue;
    }

    /**
     * @param CashbackPointAbsoluteValue|null $cashbackPointAbsoluteValue
     */
    public function setCashbackPointAbsoluteValue(?CashbackPointAbsoluteValue $cashbackPointAbsoluteValue): void
    {
        $this->cashbackPointAbsoluteValue = $cashbackPointAbsoluteValue;
    }

    /**
     * @param CashbackPointPercentageValue|null $cashbackPointPercentageValue
     */
    public function setCashbackPointPercentageValue(?CashbackPointPercentageValue $cashbackPointPercentageValue): void
    {
        $this->cashbackPointPercentageValue = $cashbackPointPercentageValue;
    }

    /**
     * @param MinDayCount|null $minDayCount
     */
    public function setMinDayCount(?MinDayCount $minDayCount): void
    {
        $this->minDayCount = $minDayCount;
    }

    /**
     * @param MaxDayCount|null $maxDayCount
     */
    public function setMaxDayCount(?MaxDayCount $maxDayCount): void
    {
        $this->maxDayCount = $maxDayCount;
    }

    /**
     * @param DateTimeImmutable|null $firstBookingDayFrom
     */
    public function setFirstBookingDayFrom(?DateTimeImmutable $firstBookingDayFrom): void
    {
        $this->firstBookingDayFrom = $firstBookingDayFrom;
    }

    /**
     * @param DateTimeImmutable|null $firstBookingDayTo
     */
    public function setFirstBookingDayTo(?DateTimeImmutable $firstBookingDayTo): void
    {
        $this->firstBookingDayTo = $firstBookingDayTo;
    }

    /**
     * @param DateTimeImmutable|null $lastBookingDayFrom
     */
    public function setLastBookingDayFrom(?DateTimeImmutable $lastBookingDayFrom): void
    {
        $this->lastBookingDayFrom = $lastBookingDayFrom;
    }

    /**
     * @param DateTimeImmutable|null $lastBookingDayTo
     */
    public function setLastBookingDayTo(?DateTimeImmutable $lastBookingDayTo): void
    {
        $this->lastBookingDayTo = $lastBookingDayTo;
    }

    /**
     * @param MinProductQuantity|null $minProductQuantity
     */
    public function setMinProductQuantity(?MinProductQuantity $minProductQuantity): void
    {
        $this->minProductQuantity = $minProductQuantity;
    }

    /**
     * @param MaxProductQuantity|null $maxProductQuantity
     */
    public function setMaxProductQuantity(?MaxProductQuantity $maxProductQuantity): void
    {
        $this->maxProductQuantity = $maxProductQuantity;
    }

    /**
     * @param DateTimeImmutable|null $productActivationFrom
     */
    public function setProductActivationFrom(?DateTimeImmutable $productActivationFrom): void
    {
        $this->productActivationFrom = $productActivationFrom;
    }

    /**
     * @param DateTimeImmutable|null $productActivationTo
     */
    public function setProductActivationTo(?DateTimeImmutable $productActivationTo): void
    {
        $this->productActivationTo = $productActivationTo;
    }

    /**
     * @param PromocodeLifetime|null $promocodeLifetime
     */
    public function setPromocodeLifetime(?PromocodeLifetime $promocodeLifetime): void
    {
        $this->promocodeLifetime = $promocodeLifetime;
    }

    /**
     * @param PromocodePrefix|null $promocodePrefix
     */
    public function setPromocodePrefix(?PromocodePrefix $promocodePrefix): void
    {
        $this->promocodePrefix = $promocodePrefix;
    }

    /**
     * @param TotalCount|null $totalCount
     */
    public function setTotalCount(?TotalCount $totalCount): void
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @param HoldedCount|null $holdedCount
     */
    public function setHoldedCount(?HoldedCount $holdedCount): void
    {
        $this->holdedCount = $holdedCount;
    }

    /**
     * @param OrderedCount|null $orderedCount
     */
    public function setOrderedCount(?OrderedCount $orderedCount): void
    {
        $this->orderedCount = $orderedCount;
    }

    /**
     * @param DateTimeImmutable|null $bundleActivationFrom
     */
    public function setBundleActivationFrom(?DateTimeImmutable $bundleActivationFrom): void
    {
        $this->bundleActivationFrom = $bundleActivationFrom;
    }

    /**
     * @param DateTimeImmutable|null $bundleActivationTo
     */
    public function setBundleActivationTo(?DateTimeImmutable $bundleActivationTo): void
    {
        $this->bundleActivationTo = $bundleActivationTo;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Description|null
     */
    public function getDescription(): ?Description
    {
        return $this->description;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getShownFrom(): DateTimeImmutable
    {
        return $this->shownFrom;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getShownTo(): DateTimeImmutable
    {
        return $this->shownTo;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getOrderedFrom(): ?DateTimeImmutable
    {
        return $this->orderedFrom;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getOrderedTo(): ?DateTimeImmutable
    {
        return $this->orderedTo;
    }

    /**
     * @return DiscountAbsoluteValue|null
     */
    public function getDiscountAbsoluteValue(): ?DiscountAbsoluteValue
    {
        return $this->discountAbsoluteValue;
    }

    /**
     * @return DiscountPercentageValue|null
     */
    public function getDiscountPercentageValue(): ?DiscountPercentageValue
    {
        return $this->discountPercentageValue;
    }

    /**
     * @return CashbackPointAbsoluteValue|null
     */
    public function getCashbackPointAbsoluteValue(): ?CashbackPointAbsoluteValue
    {
        return $this->cashbackPointAbsoluteValue;
    }

    /**
     * @return CashbackPointPercentageValue|null
     */
    public function getCashbackPointPercentageValue(): ?CashbackPointPercentageValue
    {
        return $this->cashbackPointPercentageValue;
    }

    /**
     * @return MinDayCount|null
     */
    public function getMinDayCount(): ?MinDayCount
    {
        return $this->minDayCount;
    }

    /**
     * @return MaxDayCount|null
     */
    public function getMaxDayCount(): ?MaxDayCount
    {
        return $this->maxDayCount;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getFirstBookingDayFrom(): ?DateTimeImmutable
    {
        return $this->firstBookingDayFrom;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getFirstBookingDayTo(): ?DateTimeImmutable
    {
        return $this->firstBookingDayTo;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getLastBookingDayFrom(): ?DateTimeImmutable
    {
        return $this->lastBookingDayFrom;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getLastBookingDayTo(): ?DateTimeImmutable
    {
        return $this->lastBookingDayTo;
    }

    /**
     * @return MinProductQuantity|null
     */
    public function getMinProductQuantity(): ?MinProductQuantity
    {
        return $this->minProductQuantity;
    }

    /**
     * @return MaxProductQuantity|null
     */
    public function getMaxProductQuantity(): ?MaxProductQuantity
    {
        return $this->maxProductQuantity;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getProductActivationFrom(): ?DateTimeImmutable
    {
        return $this->productActivationFrom;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getProductActivationTo(): ?DateTimeImmutable
    {
        return $this->productActivationTo;
    }

    /**
     * @return bool
     */
    public function isHasLevels(): bool
    {
        return $this->hasLevels;
    }

    /**
     * @return bool
     */
    public function isHasPromocodes(): bool
    {
        return $this->hasPromocodes;
    }

    /**
     * @return PromocodeLifetime|null
     */
    public function getPromocodeLifetime(): ?PromocodeLifetime
    {
        return $this->promocodeLifetime;
    }

    /**
     * @return PromocodePrefix|null
     */
    public function getPromocodePrefix(): ?PromocodePrefix
    {
        return $this->promocodePrefix;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return TotalCount|null
     */
    public function getTotalCount(): ?TotalCount
    {
        return $this->totalCount;
    }

    /**
     * @return HoldedCount|null
     */
    public function getHoldedCount(): ?HoldedCount
    {
        return $this->holdedCount;
    }

    /**
     * @return OrderedCount|null
     */
    public function getOrderedCount(): ?OrderedCount
    {
        return $this->orderedCount;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getBundleActivationFrom(): ?DateTimeImmutable
    {
        return $this->bundleActivationFrom;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getBundleActivationTo(): ?DateTimeImmutable
    {
        return $this->bundleActivationTo;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return ActionGroup
     */
    public function getGroup(): ActionGroup
    {
        return $this->group;
    }

    public function getActivationDateFrom(): ?DateTimeImmutable
    {
        return $this->bundleActivationTo;
    }

    public function update(
        ?Name $name,
        ?Description $description,
        ?DateTimeImmutable $shownFrom,
        ?DateTimeImmutable $shownTo,
        ?DateTimeImmutable $orderedFrom,
        ?DateTimeImmutable $orderedTo,
        ?DiscountAbsoluteValue $discountAbsoluteValue,
        ?DiscountPercentageValue $discountPercentageValue,
        ?CashbackPointAbsoluteValue $cashbackPointAbsoluteValue,
        ?CashbackPointPercentageValue $cashbackPointPercentageValue,
        ?MinDayCount $minDayCount,
        ?MaxDayCount $maxDayCount,
        ?DateTimeImmutable $firstBookingDayFrom,
        ?DateTimeImmutable $firstBookingDayTo,
        ?DateTimeImmutable $lastBookingDayFrom,
        ?DateTimeImmutable $lastBookingDayTo,
        ?MinProductQuantity $minProductQuantity,
        ?MaxProductQuantity $maxProductQuantity,
        ?DateTimeImmutable $productActivationFrom,
        ?DateTimeImmutable $productActivationTo,
        ?bool $hasLevels,
        ?bool $hasPromocodes,
        ?PromocodeLifetime $promocodeLifetime,
        ?PromocodePrefix $promocodePrefix,
        ?bool $isActive,
        ?TotalCount $totalCount,
        ?HoldedCount $holdedCount,
        ?OrderedCount $orderedCount,
        ?DateTimeImmutable $bundleActivationFrom,
        ?DateTimeImmutable $bundleActivationTo,
        ?Sort $sort
    ) {
        if ($name) {
            $this->name = $name;
        }
        if ($description) {
            $this->description = $description;
        }
        if ($shownFrom) {
            $this->shownFrom = $shownFrom;
        }
        if ($shownTo) {
            $this->shownTo = $shownTo;
        }
        if ($orderedFrom) {
            $this->orderedFrom = $orderedFrom;
        }
        if ($orderedTo) {
            $this->orderedTo = $orderedTo;
        }
        if ($discountAbsoluteValue) {
            $this->discountAbsoluteValue = $discountAbsoluteValue;
        }
        if ($discountPercentageValue) {
            $this->discountPercentageValue = $discountPercentageValue;
        }
        if ($cashbackPointAbsoluteValue) {
            $this->cashbackPointAbsoluteValue = $cashbackPointAbsoluteValue;
        }
        if ($cashbackPointPercentageValue) {
            $this->cashbackPointPercentageValue = $cashbackPointPercentageValue;
        }
        if ($minDayCount) {
            $this->minDayCount = $minDayCount;
        }
        if ($maxDayCount) {
            $this->maxDayCount = $maxDayCount;
        }
        if ($firstBookingDayFrom) {
            $this->firstBookingDayFrom = $firstBookingDayFrom;
        }
        if ($firstBookingDayTo) {
            $this->firstBookingDayTo = $firstBookingDayTo;
        }
        if ($lastBookingDayFrom) {
            $this->lastBookingDayFrom = $lastBookingDayFrom;
        }
        if ($lastBookingDayTo) {
            $this->lastBookingDayTo = $lastBookingDayTo;
        }
        if ($minProductQuantity) {
            $this->minProductQuantity = $minProductQuantity;
        }
        if ($maxProductQuantity) {
            $this->maxProductQuantity = $maxProductQuantity;
        }
        if ($productActivationFrom) {
            $this->productActivationFrom = $productActivationFrom;
        }
        if ($productActivationTo) {
            $this->productActivationTo = $productActivationTo;
        }
        if ($hasLevels !== null) {
            $this->hasLevels = $hasLevels;
        }
        if ($hasPromocodes !== null) {
            $this->hasPromocodes = $hasPromocodes;
        }
        if ($promocodeLifetime) {
            $this->promocodeLifetime = $promocodeLifetime;
        }
        if ($promocodePrefix) {
            $this->promocodePrefix = $promocodePrefix;
        }
        if ($isActive !== null) {
            $this->isActive = $isActive;
        }
        if ($totalCount) {
            $this->totalCount = $totalCount;
        }
        if ($holdedCount) {
            $this->holdedCount = $holdedCount;
        }
        if ($orderedCount) {
            $this->orderedCount = $orderedCount;
        }
        if ($bundleActivationFrom) {
            $this->bundleActivationFrom = $bundleActivationFrom;
        }
        if ($bundleActivationTo) {
            $this->bundleActivationTo = $bundleActivationTo;
        }
        if ($sort) {
            $this->sort = $sort;
        }
    }

    public function getLevels(): array
    {
        return $this->levels->toArray();
    }
}
