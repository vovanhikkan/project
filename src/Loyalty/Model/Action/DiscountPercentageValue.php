<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Action;

use App\Application\ValueObject\IntegerValueObject;

/**
 * DiscountPercentageValue.
 */
final class DiscountPercentageValue extends IntegerValueObject
{
}