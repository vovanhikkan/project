<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Action;

use App\Application\ValueObject\IntegerValueObject;

/**
 * PromocodeLifetime.
 */
final class PromocodeLifetime extends IntegerValueObject
{
}