<?php
declare(strict_types=1);

namespace App\Loyalty\Model\ActionLevel;

use App\Application\ValueObject\Uuid;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\Level\Level;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActionLevel
 *
 * @ORM\Table(name="action_level")
 * @ORM\Entity()
 */
class ActionLevel
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Loyalty\Model\Action\Action")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     */
    private Action $action;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Loyalty\Model\Level\Level")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id")
     */
    private Level $level;

    /**
     * ActionLevel constructor.
     * @param Action $action
     * @param Level $level
     */
    public function __construct(Action $action, Level $level)
    {
        $this->action = $action;
        $this->level = $level;
    }

    /**
     * @return Action
     */
    public function getAction(): Action
    {
        return $this->action;
    }

    /**
     * @return Level
     */
    public function getLevel(): Level
    {
        return $this->level;
    }
}
