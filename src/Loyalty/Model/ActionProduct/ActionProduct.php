<?php
declare(strict_types=1);

namespace App\Loyalty\Model\ActionProduct;

use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\Product;
use App\Loyalty\Model\Action\Action;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActionProduct
 *
 * @ORM\Table(name="action_product")
 * @ORM\Entity()
 */
class ActionProduct
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Loyalty\Model\Action\Action")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     */
    private Action $action;

    /**
     * @ORM\ManyToOne(targetEntity="App\Product\Model\Product\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private Product $product;

    /**
     * ActionProduct constructor.
     * @param Uuid $id
     * @param Action $action
     * @param Product $product
     */
    public function __construct(Uuid $id, Action $action, Product $product)
    {
        $this->id = $id;
        $this->action = $action;
        $this->product = $product;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Action
     */
    public function getAction(): Action
    {
        return $this->action;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }
}
