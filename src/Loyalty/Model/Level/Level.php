<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Level;

use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * Level
 *
 * @ORM\Table(name="level")
 * @ORM\Entity()
 */
class Level
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="loyalty_level_name")
     */
    private Name $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Loyalty\Model\Action\Action", mappedBy="levels")
     */
    private ?Collection $actions = null;

    /**
     * Level constructor.
     * @param Uuid $id
     * @param Name $name
     */
    public function __construct(Uuid $id, Name $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

}
