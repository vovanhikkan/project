<?php
declare(strict_types=1);

namespace App\Loyalty\Model\Level;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Name.
 */
final class Name extends ArrayValueObject
{
}