<?php
declare(strict_types=1);

namespace App\Loyalty\Model\ActionGroup;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Level
 *
 * @ORM\Table(name="action_group")
 * @ORM\Entity()
 */
class ActionGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="loyalty_action_group_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * ActionGroup constructor.
     * @param Uuid $id
     * @param Name $name
     * @param Sort $sort
     */
    public function __construct(Uuid $id, Name $name, Sort $sort)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sort = $sort;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    public function update(Name $name, Sort $sort)
    {
        $this->name = $name;
        $this->sort = $sort;
    }
}