<?php
declare(strict_types=1);

namespace App\Loyalty\Model\ActionGroup;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Name.
 */
final class Name extends ArrayValueObject
{
}