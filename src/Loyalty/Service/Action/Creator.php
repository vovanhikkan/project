<?php

declare(strict_types=1);

namespace App\Loyalty\Service\Action;

use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use App\Data\Flusher;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\Action\CashbackPointAbsoluteValue;
use App\Loyalty\Model\Action\CashbackPointPercentageValue;
use App\Loyalty\Model\Action\Description;
use App\Loyalty\Model\Action\DiscountAbsoluteValue;
use App\Loyalty\Model\Action\DiscountPercentageValue;
use App\Loyalty\Model\Action\HoldedCount;
use App\Loyalty\Model\Action\MaxDayCount;
use App\Loyalty\Model\Action\MaxProductQuantity;
use App\Loyalty\Model\Action\MinDayCount;
use App\Loyalty\Model\Action\MinProductQuantity;
use App\Loyalty\Model\Action\Name;
use App\Loyalty\Model\Action\OrderedCount;
use App\Loyalty\Model\Action\PromocodeLifetime;
use App\Loyalty\Model\Action\PromocodePrefix;
use App\Loyalty\Model\Action\TotalCount;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\Loyalty\Repository\ActionRepository;
use DateTimeImmutable;

/**
 * Creator.
 */
class Creator
{
    private ActionRepository $actionRepository;
    private Flusher $flusher;

    public function __construct(ActionRepository $actionRepository, Flusher $flusher)
    {
        $this->actionRepository = $actionRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Name $name,
        Description $description,
        DateTimeImmutable $shownFrom,
        DateTimeImmutable $shownTo,
        ?DateTimeImmutable $orderedFrom,
        ?DateTimeImmutable $orderedTo,
        ?DiscountAbsoluteValue $discountAbsoluteValue,
        ?DiscountPercentageValue $discountPercentageValue,
        ?CashbackPointAbsoluteValue $cashbackPointAbsoluteValue,
        ?CashbackPointPercentageValue $cashbackPointPercentageValue,
        ?MinDayCount $minDayCount,
        ?MaxDayCount $maxDayCount,
        ?DateTimeImmutable $firstBookingDayFrom,
        ?DateTimeImmutable $firstBookingDayTo,
        ?DateTimeImmutable $lastBookingDayFrom,
        ?DateTimeImmutable $lastBookingDayTo,
        ?MinProductQuantity $minProductQuantity,
        ?MaxProductQuantity $maxProductQuantity,
        ?DateTimeImmutable $productActivationFrom,
        ?DateTimeImmutable $productActivationTo,
        bool $hasLevels,
        bool $hasPromocodes,
        ?PromocodeLifetime $promocodeLifetime,
        ?PromocodePrefix $promocodePrefix,
        ?TotalCount $totalCount,
        ?HoldedCount $holdedCount,
        ?OrderedCount $orderedCount,
        ?DateTimeImmutable $bundleActivationFrom,
        ?DateTimeImmutable $bundleActivationTo,
        ActionGroup $group
    ): Action {
        $sort = $this->actionRepository->getMaxSort() + 1;
        $action = new Action(
            Uuid::generate(),
            $name,
            $shownFrom,
            $shownTo,
            $hasLevels,
            $hasPromocodes,
            true,
            new Sort($sort),
            $group
        );

        if ($description) {
            $action->setDescription($description);
        }
        if ($orderedFrom) {
            $action->setOrderedFrom($orderedFrom);
        }
        if ($orderedTo) {
            $action->setOrderedTo($orderedTo);
        }
        if ($discountAbsoluteValue) {
            $action->setDiscountAbsoluteValue($discountAbsoluteValue);
        }
        if ($discountPercentageValue) {
            $action->setDiscountPercentageValue($discountPercentageValue);
        }
        if ($cashbackPointAbsoluteValue) {
            $action->setCashbackPointAbsoluteValue($cashbackPointAbsoluteValue);
        }
        if ($cashbackPointPercentageValue) {
            $action->setCashbackPointPercentageValue($cashbackPointPercentageValue);
        }
        if ($minDayCount) {
            $action->setMinDayCount($minDayCount);
        }
        if ($maxDayCount) {
            $action->setMaxDayCount($maxDayCount);
        }
        if ($firstBookingDayFrom) {
            $action->setFirstBookingDayFrom($firstBookingDayFrom);
        }
        if ($firstBookingDayTo) {
            $action->setFirstBookingDayTo($firstBookingDayTo);
        }
        if ($lastBookingDayFrom) {
            $action->setLastBookingDayFrom($lastBookingDayFrom);
        }
        if ($lastBookingDayTo) {
            $action->setLastBookingDayTo($lastBookingDayTo);
        }
        if ($minProductQuantity) {
            $action->setMinProductQuantity($minProductQuantity);
        }
        if ($maxProductQuantity) {
            $action->setMaxProductQuantity($maxProductQuantity);
        }
        if ($productActivationFrom) {
            $action->setProductActivationFrom($productActivationFrom);
        }
        if ($productActivationTo) {
            $action->setProductActivationTo($productActivationTo);
        }
        if ($promocodeLifetime) {
            $action->setPromocodeLifetime($promocodeLifetime);
        }
        if ($promocodePrefix) {
            $action->setPromocodePrefix($promocodePrefix);
        }
        if ($totalCount) {
            $action->setTotalCount($totalCount);
        }
        if ($holdedCount) {
            $action->setHoldedCount($holdedCount);
        }
        if ($orderedCount) {
            $action->setOrderedCount($orderedCount);
        }
        if ($bundleActivationFrom) {
            $action->setBundleActivationFrom($bundleActivationFrom);
        }
        if ($bundleActivationTo) {
            $action->setBundleActivationTo($bundleActivationTo);
        }

        $this->actionRepository->add($action);
        $this->flusher->flush();

        return $action;
    }
}
