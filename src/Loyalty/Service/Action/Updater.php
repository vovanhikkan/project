<?php

declare(strict_types=1);

namespace App\Loyalty\Service\Action;

use App\Application\ValueObject\Date;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use App\Data\Flusher;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\Action\ActivatedCount;
use App\Loyalty\Model\Action\CashbackPointAbsoluteValue;
use App\Loyalty\Model\Action\CashbackPointPercentageValue;
use App\Loyalty\Model\Action\Description;
use App\Loyalty\Model\Action\DiscountAbsoluteValue;
use App\Loyalty\Model\Action\DiscountPercentageValue;
use App\Loyalty\Model\Action\HoldedCount;
use App\Loyalty\Model\Action\MaxDayCount;
use App\Loyalty\Model\Action\MaxProductQuantity;
use App\Loyalty\Model\Action\MinDayCount;
use App\Loyalty\Model\Action\MinProductQuantity;
use App\Loyalty\Model\Action\Name;
use App\Loyalty\Model\Action\OrderedCount;
use App\Loyalty\Model\Action\PromocodeLifetime;
use App\Loyalty\Model\Action\PromocodePrefix;
use App\Loyalty\Model\Action\TotalCount;
use App\Loyalty\Repository\ActionRepository;
use DateTimeImmutable;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;
    private ActionRepository $actionRepository;

    public function __construct(ActionRepository $actionRepository, Flusher $flusher)
    {
        $this->flusher = $flusher;
        $this->actionRepository = $actionRepository;
    }

    public function update(
        Action $action,
        ?Name $name,
        ?Description $description,
        ?DateTimeImmutable $shownFrom,
        ?DateTimeImmutable $shownTo,
        ?DateTimeImmutable $orderedFrom,
        ?DateTimeImmutable $orderedTo,
        ?DiscountAbsoluteValue $discountAbsoluteValue,
        ?DiscountPercentageValue $discountPercentageValue,
        ?CashbackPointAbsoluteValue $cashbackPointAbsoluteValue,
        ?CashbackPointPercentageValue $cashbackPointPercentageValue,
        ?MinDayCount $minDayCount,
        ?MaxDayCount $maxDayCount,
        ?DateTimeImmutable $firstBookingDayFrom,
        ?DateTimeImmutable $firstBookingDayTo,
        ?DateTimeImmutable $lastBookingDayFrom,
        ?DateTimeImmutable $lastBookingDayTo,
        ?MinProductQuantity $minProductQuantity,
        ?MaxProductQuantity $maxProductQuantity,
        ?DateTimeImmutable $productActivationFrom,
        ?DateTimeImmutable $productActivationTo,
        ?bool $hasLevels,
        ?bool $hasPromocodes,
        ?PromocodeLifetime $promocodeLifetime,
        ?PromocodePrefix $promocodePrefix,
        ?bool $isActive,
        ?TotalCount $totalCount,
        ?HoldedCount $holdedCount,
        ?OrderedCount $orderedCount,
        ?DateTimeImmutable $bundleActivationFrom,
        ?DateTimeImmutable $bundleActivationTo,
        ?Sort $sort
    ): Action {
        $action->update(
            $name,
            $description,
            $shownFrom,
            $shownTo,
            $orderedFrom,
            $orderedTo,
            $discountAbsoluteValue,
            $discountPercentageValue,
            $cashbackPointAbsoluteValue,
            $cashbackPointPercentageValue,
            $minDayCount,
            $maxDayCount,
            $firstBookingDayFrom,
            $firstBookingDayTo,
            $lastBookingDayFrom,
            $lastBookingDayTo,
            $minProductQuantity,
            $maxProductQuantity,
            $productActivationFrom,
            $productActivationTo,
            $hasLevels,
            $hasPromocodes,
            $promocodeLifetime,
            $promocodePrefix,
            $isActive,
            $totalCount,
            $holdedCount,
            $orderedCount,
            $bundleActivationFrom,
            $bundleActivationTo,
            $sort
        );
        $this->flusher->flush();
        return $action;
    }
}
