<?php

declare(strict_types=1);

namespace App\Loyalty\Service\ActionGroup;

use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use App\Data\Flusher;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\Loyalty\Model\ActionGroup\Name;
use App\Loyalty\Repository\ActionGroupRepository;

/**
 * Creator.
 */
class Creator
{
    private ActionGroupRepository $actionGroupRepository;
    private Flusher $flusher;

    public function __construct(ActionGroupRepository $actionGroupRepository, Flusher $flusher)
    {
        $this->actionGroupRepository = $actionGroupRepository;
        $this->flusher = $flusher;
    }

    public function create(Name $name): ActionGroup
    {
        $sort = $this->actionGroupRepository->getMaxSort();
        $actionGroup = new ActionGroup(
            Uuid::generate(),
            $name,
            new Sort($sort + 1)
        );

        $this->actionGroupRepository->add($actionGroup);
        $this->flusher->flush();

        return $actionGroup;
    }
}
