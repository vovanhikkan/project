<?php

declare(strict_types=1);

namespace App\Loyalty\Service\ActionGroup;

use App\Application\ValueObject\DateTime;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use App\Data\Flusher;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\Loyalty\Model\ActionGroup\Name;
use App\Loyalty\Model\Promocode\HoldedCount;
use App\Loyalty\Model\Promocode\OrderedCount;
use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Model\Promocode\TotalCount;
use App\Loyalty\Model\Promocode\Value;
use App\Loyalty\Repository\ActionGroupRepository;

/**
 * Updater.
 */
class Updater
{
    private ActionGroupRepository $actionGroupRepository;
    private Flusher $flusher;

    public function __construct(ActionGroupRepository $actionGroupRepository, Flusher $flusher)
    {
        $this->actionGroupRepository = $actionGroupRepository;
        $this->flusher = $flusher;
    }

    public function update(ActionGroup $actionGroup, Name $name, Sort $sort): ActionGroup {
        $actionGroup->update(
            $name,
            $sort
        );
        $this->flusher->flush();
        return $actionGroup;
    }
}
