<?php

declare(strict_types=1);

namespace App\Loyalty\Service\Promocode;

use App\Data\Flusher;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\Promocode\HoldedCount;
use App\Loyalty\Model\Promocode\OrderedCount;
use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Model\Promocode\TotalCount;
use App\Loyalty\Model\Promocode\Value;
use App\Order\Model\Cart\Cart;
use DateTimeImmutable;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function update(
        Promocode $promocode,
        ?Action $action,
        ?Value $value,
        ?TotalCount $totalCount,
        ?HoldedCount $holdedCount,
        ?OrderedCount $orderedCount,
        ?DateTimeImmutable $expiredAt
    ): void {
        $promocode->update(
            $action,
            $value,
            $totalCount,
            $holdedCount,
            $orderedCount,
            $expiredAt
        );

        $this->flusher->flush();
    }
    public function addPromocode(
        Cart $cart,
        Promocode $promocode
    ): void {
        $cart->addPromocode($promocode);
        $this->flusher->flush();
    }
}
