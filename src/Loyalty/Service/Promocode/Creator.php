<?php

declare(strict_types=1);

namespace App\Loyalty\Service\Promocode;

use App\Application\ValueObject\DateTime;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Model\Promocode\HoldedCount;
use App\Loyalty\Model\Promocode\OrderedCount;
use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Model\Promocode\TotalCount;
use App\Loyalty\Model\Promocode\Value;
use App\Loyalty\Repository\PromocodeRepository;
use DateTimeImmutable;

/**
 * Creator.
 */
class Creator
{
    private PromocodeRepository $promocodeRepository;
    private Flusher $flusher;

    public function __construct(PromocodeRepository $promocodeRepository, Flusher $flusher)
    {
        $this->promocodeRepository = $promocodeRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Action $action,
        ?Value $value,
        ?TotalCount $totalCount,
        ?HoldedCount $holdedCount,
        ?OrderedCount $orderedCount,
        ?DateTimeImmutable $expiredAt
    ): Promocode
    {
        $promocode = new Promocode(
            Uuid::generate(),
            $action,
            $value
        );

        $promocode->setTotalCount($totalCount);
        $promocode->setHoldedCount($holdedCount);
        $promocode->setOrderedCount($orderedCount);
        $promocode->setExpiredAt($expiredAt);

        $this->promocodeRepository->add($promocode);
        $this->flusher->flush();

        return $promocode;
    }
}
