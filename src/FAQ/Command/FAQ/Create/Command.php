<?php

declare(strict_types=1);

namespace App\FAQ\Command\FAQ\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private array $title;
    /** @Assert\NotBlank() */
    private array $body;

    private ?int $sort = null;

    public function __construct(array $title, array $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param int|null $sort
     */
    public function setSort(?int $sort): void
    {
        $this->sort = $sort;
    }
}
