<?php

declare(strict_types=1);

namespace App\FAQ\Command\FAQ\Create;

use App\Application\ValueObject\Uuid;
use App\FAQ\Model\FAQ\FAQ;
use App\FAQ\Model\FAQ\Title;
use App\FAQ\Model\FAQ\Body;
use App\FAQ\Service\FAQ\Creator;

class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): FAQ
    {
        return $this->creator->create(
            Uuid::generate(),
            new Title($command->getTitle()),
            new Body($command->getBody())
        );
    }
}
