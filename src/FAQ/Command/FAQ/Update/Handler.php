<?php

declare(strict_types=1);

namespace App\FAQ\Command\FAQ\Update;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\FAQ\Model\FAQ\Body;
use App\FAQ\Model\FAQ\FAQ;
use App\FAQ\Model\FAQ\Title;
use App\FAQ\Repository\FAQRepository;
use App\FAQ\Service\FAQ\Updater;

class Handler
{
    private Updater $updater;
    private FAQRepository $fAQRepository;


    public function __construct(Updater $updater, FAQRepository $fAQRepository)
    {
        $this->fAQRepository = $fAQRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): FAQ
    {
        $fAQ = $this->fAQRepository->get(new Uuid($command->getId()));

        
        return $this->updater->update(
            $fAQ,
            $command->getTitle() ? new Title($command->getTitle()) : null,
            $command->getBody() ? new Body($command->getBody()) : null,
            $command->getSort() ? new Sort($command->getSort()) : null
        );
    }
}
