<?php

declare(strict_types=1);

namespace App\FAQ\Service\FAQ;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\FAQ\Model\FAQ\Body;
use App\FAQ\Model\FAQ\FAQ;
use App\FAQ\Model\FAQ\Title;
use App\FAQ\Repository\FAQRepository;

class Creator
{
    private Flusher $flusher;
    private FAQRepository $fAQRepository;


    public function __construct(FAQRepository $fAQRepository, Flusher $flusher)
    {
        $this->fAQRepository = $fAQRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Title $title,
        Body $body
    ): FAQ {
        $sort = $this->fAQRepository->getMaxSort() + 1;
        $fAQ = new FAQ(
            $id,
            $title,
            $body,
            new Sort($sort)
        );

        $this->fAQRepository->add($fAQ);
        $this->flusher->flush();

        return $fAQ;
    }
}
