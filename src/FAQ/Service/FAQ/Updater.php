<?php

declare(strict_types=1);

namespace App\FAQ\Service\FAQ;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\FAQ\Model\FAQ\Body;
use App\FAQ\Model\FAQ\FAQ;
use App\FAQ\Model\FAQ\Title;
use App\FAQ\Repository\FAQRepository;

class Updater
{
    private Flusher $flusher;
    private FAQRepository $fAQRepository;


    public function __construct(FAQRepository $fAQRepository, Flusher $flusher)
    {
        $this->fAQRepository = $fAQRepository;
        $this->flusher = $flusher;
    }


    public function update(
        FAQ $fAQ,
        ?Title $title,
        ?Body $body,
        ?Sort $sort
    ): FAQ {
        
        $fAQ->update($title, $body, $sort);
        $this->flusher->flush();
        return $fAQ;
    }
}
