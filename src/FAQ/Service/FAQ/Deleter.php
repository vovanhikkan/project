<?php

declare(strict_types=1);

namespace App\FAQ\Service\FAQ;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\FAQ\Repository\FAQRepository;

class Deleter
{
    private Flusher $flusher;
    private FAQRepository $fAQRepository;


    public function __construct(FAQRepository $fAQRepository, Flusher $flusher)
    {
        $this->fAQRepository = $fAQRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->fAQRepository->delete($id);
        $this->flusher->flush();
    }
}
