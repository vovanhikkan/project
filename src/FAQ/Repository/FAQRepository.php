<?php

declare(strict_types=1);

namespace App\FAQ\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\FAQ\Model\FAQ\FAQ;

class FAQRepository extends AbstractRepository
{
    public function add(FAQ $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('f');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): FAQ
    {
        /** @var FAQ|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('FAQ with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('a')
            ->select('MAX(a.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return FAQ::class;
    }
}
