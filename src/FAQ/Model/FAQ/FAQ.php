<?php

declare(strict_types=1);

namespace App\FAQ\Model\FAQ;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * FAQ.
 * @ORM\Entity()
 * @ORM\Table(name="faq")
 */
class FAQ
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="faq_title")
     */
    private Title $title;

    /**
     * @ORM\Column(type="faq_body")
     */
    private Body $body;
    
    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    public function __construct(Uuid $id, Title $title, Body $body, Sort $sort)
    {
        $this->id = $id;
        $this->title = $title;
        $this->body = $body;
        $this->sort = $sort;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Title
     */
    public function getTitle(): Title
    {
        return $this->title;
    }

    /**
     * @return Body
     */
    public function getBody(): Body
    {
        return $this->body;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @param Title|null $title
     * @param Body|null $body
     * @param Sort|null $sort
     */
    public function update(
        ?Title $title,
        ?Body $body,
        ?Sort $sort
    ) {
        if ($title) {
            $this->title = $title;
        }

        if ($body) {
            $this->body = $body;
        }

        if ($sort) {
            $this->sort = $sort;
        }
    }
}
