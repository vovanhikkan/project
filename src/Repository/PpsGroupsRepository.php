<?php

namespace App\Repository;

use App\Entity\PpsGroups;
use Doctrine\ORM\EntityRepository;

/**
 * PpsGroupsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * @method PpsGroups|null find($id, $lockMode = null, $lockVersion = null)
 * @method PpsGroups|null findOneBy(array $criteria, array $orderBy = null)
 * @method PpsGroups[]    findAll()
 * @method PpsGroups[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PpsGroupsRepository extends EntityRepository
{

}