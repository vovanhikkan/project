<?php

namespace App\Repository;

use App\Entity\CategoriesSections;
use Doctrine\ORM\EntityRepository;

/**
 * PpsPricesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * @method CategoriesSections|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriesSections|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriesSections[]    findAll()
 * @method CategoriesSections[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesSectionsRepository extends EntityRepository
{

}