<?php

declare(strict_types=1);

namespace App\Location\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Location\Model\Location\Location;

/**
 * LocationRepository.
 */
class LocationRepository extends AbstractRepository
{
    public function add(Location $language): void
    {
        $this->entityManager->persist($language);
    }

    /**
     * @return Location[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('l');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Location
    {
        /** @var Location|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Location with id %s not found', (string)$id));
        }

        return $model;
    }

    public function delete(Uuid $id)
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('l')
            ->select('MAX(l.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return Location::class;
    }
}
