<?php


namespace App\Location\Model\Location;

use App\Application\ValueObject\IntegerValueObject;

/**
 * Altitude.
 */
final class Altitude extends IntegerValueObject
{
}