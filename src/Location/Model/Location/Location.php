<?php

namespace App\Location\Model\Location;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Locations
 *
 * @ORM\Table(name="location")
 * @ORM\Entity
 */
class Location
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="location_location_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\Column(type="location_location_altitude")
     */
    private Altitude $altitude;

    /**
     * Location constructor.
     * @param Uuid $id
     * @param bool $isActive
     * @param Name $name
     * @param Sort $sort
     * @param Altitude $altitude
     */
    public function __construct(Uuid $id, bool $isActive, Name $name, Sort $sort, Altitude $altitude)
    {
        $this->id = $id;
        $this->isActive = $isActive;
        $this->name = $name;
        $this->sort = $sort;
        $this->altitude = $altitude;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return Altitude
     */
    public function getAltitude(): Altitude
    {
        return $this->altitude;
    }

    public function update(?Name $name, ?Altitude $altitude)
    {
        if ($name) {
            $this->name = $name;
        }

        if ($altitude) {
            $this->altitude = $altitude;
        }
    }
}
