<?php

declare(strict_types=1);

namespace App\Location\Command\Location\Update;

/**
 * Command.
 */
class Command
{
    private string $id;
    private array $name;
    private int $altitude;

    /**
     * Command constructor.
     * @param string $id
     * @param array $name
     * @param int $altitude
     */
    public function __construct(string $id, array $name, int $altitude)
    {
        $this->id = $id;
        $this->name = $name;
        $this->altitude = $altitude;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAltitude(): int
    {
        return $this->altitude;
    }

}
