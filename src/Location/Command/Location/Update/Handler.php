<?php

declare(strict_types=1);

namespace App\Location\Command\Location\Update;

use App\Application\ValueObject\Uuid;
use App\Location\Model\Location\Altitude;
use App\Location\Model\Location\Location;
use App\Location\Model\Location\Name;
use App\Location\Repository\LocationRepository;
use App\Location\Service\Location\Updater;

/**
 * Handler.
 */
class Handler
{
    private Updater $updater;
    private LocationRepository $locationRepository;

    public function __construct(Updater $updater, LocationRepository $locationRepository)
    {
        $this->updater = $updater;
        $this->locationRepository = $locationRepository;
    }

    public function handle(Command $command): Location
    {
        $location = $this->locationRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $location,
            new Name($command->getName()),
            new Altitude($command->getAltitude())
        );
    }
}
