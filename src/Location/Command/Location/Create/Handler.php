<?php

declare(strict_types=1);

namespace App\Location\Command\Location\Create;

use App\Location\Model\Location\Altitude;
use App\Location\Model\Location\Location;
use App\Location\Model\Location\Name;
use App\Location\Service\Location\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;

    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }

    public function handle(Command $command): Location
    {
        return $this->creator->create(
            new Name($command->getName()),
            new Altitude($command->getAltitude())
        );
    }
}
