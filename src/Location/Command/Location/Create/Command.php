<?php

declare(strict_types=1);

namespace App\Location\Command\Location\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $name;

    /**
     * @Assert\NotBlank()
     */
    private int $altitude;

    /**
     * Command constructor.
     * @param array $name
     * @param int $altitude
     */
    public function __construct(array $name, int $altitude)
    {
        $this->name = $name;
        $this->altitude = $altitude;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAltitude(): int
    {
        return $this->altitude;
    }
}
