<?php

declare(strict_types=1);

namespace App\Location\Command\Location\Delete;

use App\Application\ValueObject\Uuid;
use App\Location\Service\Location\Deleter;

/**
 * Handler.
 */
class Handler
{
    private Deleter $deleter;

    public function __construct(Deleter $deleter)
    {
        $this->deleter = $deleter;
    }

    public function handle(Command $command): array
    {
        $this->deleter->delete(new Uuid($command->getId()));
        return  ['message' => 'Локация удалена'];
    }
}
