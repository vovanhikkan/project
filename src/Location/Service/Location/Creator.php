<?php

declare(strict_types=1);

namespace App\Location\Service\Location;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Location\Model\Location\Altitude;
use App\Location\Model\Location\Location;
use App\Location\Model\Location\Name;
use App\Location\Repository\LocationRepository;

/**
 * Creator.
 */
class Creator
{
    private LocationRepository $locationRepository;
    private Flusher $flusher;

    public function __construct(LocationRepository $locationRepository, Flusher $flusher)
    {
        $this->locationRepository = $locationRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Name $name,
        Altitude $altitude
    ): Location {
        $sort = $this->locationRepository->getMaxSort() + 1;

        $location = new Location(
            Uuid::generate(),
            true,
            $name,
            new Sort($sort),
            $altitude
        );

        $this->locationRepository->add($location);
        $this->flusher->flush();

        return $location;
    }
}
