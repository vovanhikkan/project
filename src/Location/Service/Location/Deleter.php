<?php

declare(strict_types=1);

namespace App\Location\Service\Location;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Location\Repository\LocationRepository;

/**
 * Deleter.
 */
class Deleter
{
    private Flusher $flusher;
    private LocationRepository $locationRepository;

    public function __construct(Flusher $flusher, LocationRepository $locationRepository)
    {
        $this->flusher = $flusher;
        $this->locationRepository = $locationRepository;
    }

    public function delete(
        Uuid $id
    ): void {

        $this->locationRepository->delete($id);

        $this->flusher->flush();
    }
}
