<?php

declare(strict_types=1);

namespace App\Location\Service\Location;

use App\Data\Flusher;
use App\Location\Model\Location\Altitude;
use App\Location\Model\Location\Location;
use App\Location\Model\Location\Name;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function update(
        Location $location,
        ?Name $name,
        ?Altitude $altitude
    ): Location {

        $location->update($name, $altitude);

        $this->flusher->flush();

        return $location;
    }
}
