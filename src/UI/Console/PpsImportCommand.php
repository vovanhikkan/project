<?php

declare(strict_types=1);

namespace App\UI\Console;

use App\Product\Service\Pps\Importer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * PpsImportCommand.
 */
class PpsImportCommand extends Command
{
    private Importer $importer;

    public function __construct(Importer $importer)
    {
        parent::__construct();
        $this->importer = $importer;
    }

    protected function configure(): void
    {
        $this->setName('pps:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->importer->import();

        return self::SUCCESS;
    }
}
