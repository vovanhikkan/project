<?php

declare(strict_types=1);

namespace App\UI\Console;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Color;
use App\Application\ValueObject\Svg;
use App\Application\ValueObject\Uuid;
use App\Content\Model\Category\Description;
use App\Content\Model\Category\Name;
use App\Content\Service\Category\Creator as CategoryCreator;
use App\Place\Repository\PlaceRepository;
use App\Place\Service\Place\Creator;
use App\Product\Repository\SectionRepository;
use App\Storage\Service\File\Downloader;
use Symfony\Component\Console\Command\Command as ConsoleCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * MigrateCommand.
 */
class MigrateCommand extends ConsoleCommand
{
    private Creator $creator;
    private Downloader $downloader;
    private CategoryCreator $categoryCreator;
    private PlaceRepository $placeRepository;
    private SectionRepository $sectionRepository;

    public function __construct(
        Creator $creator,
        Downloader $downloader,
        CategoryCreator $categoryCreator,
        PlaceRepository $placeRepository,
        SectionRepository $sectionRepository
    ) {
        parent::__construct();
        $this->creator = $creator;
        $this->downloader = $downloader;
        $this->categoryCreator = $categoryCreator;
        $this->placeRepository = $placeRepository;
        $this->sectionRepository = $sectionRepository;
    }

    protected function configure()
    {
        $this->setName('migrate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
////        $places = file_get_contents('https://rosakhutor.dreamdev.space/api/v1/place');
////        $places = \json_decode($places, true);
////        $placeMap = [];
////        foreach ($places['data'] as $row) {
////            foreach ($this->placeRepository->fetchAll() as $place) {
////                $title1 = json_encode($place->getTitle()->getValue());
////                $title2 = json_encode($row['title']);
////                if ($title1 === $title2) {
////                    $placeMap[$row['id']] = $place;
////                    break;
////                }
////            }
////        }
//
//        $sections = file_get_contents('https://rosakhutor.dreamdev.space/api/v1/section/all');
//        $sections = \json_decode($sections, true);
//        $sectionMap = [];
//        foreach ($sections['data'] as $row) {
//            foreach ($this->sectionRepository->fetchAll() as $section) {
//                if ($row['title'] == $section->getName()->getValue()) {
//                    $sectionMap[$row['id']] = $section;
//                    break;
//                }
//            }
//        }
//
////        var_dump(count($sectionMap)); exit;
//
//        $data = file_get_contents('https://rosakhutor.dreamdev.space/api/v1/category');
//        $data = \json_decode($data, true);
//        foreach ($data['data'] as $row) {
//
//            $webIcon = null;
//            if (isset($row['webIcon'])) {
//                $webIcon = $this->downloader->download($row['webIcon']);
//            }
//
//            $mobileIcon = null;
//            if (isset($row['mobile_icon'])) {
//                $mobileIcon = $this->downloader->download($row['mobile_icon']);
//            }
//
//            $mobileBackground = null;
//            if (isset($row['mobile_background'])) {
//                $mobileBackground = $this->downloader->download($row['mobile_background']);
//            }
//
//            $sections = [];
//            foreach ($row['sections'] as $sectionId) {
//                $sections[] = $sectionMap[$sectionId];
//            }
//
//            $category = $this->categoryCreator->create(
//                new Name($row['title']),
//                new Description($row['description'] ?? ''),
//                isset($row['web_gradient_color_1']) ? new Color($row['web_gradient_color_1']) : null,
//                isset($row['web_gradient_color_2']) ? new Color($row['web_gradient_color_2']) : null,
//                $webIcon,
//                isset($row['web_icon_svg']) ? new Svg($row['web_icon_svg']) : null,
//                $mobileIcon,
//                $mobileBackground,
//                [],
//                $sections
//            );
//        }

//        $data = file_get_contents('https://rosakhutor.dreamdev.space/api/v1/place');
//        $data = \json_decode($data, true);
//        foreach ($data['data'] as $row) {
//
//            $file = null;
//            if (isset($row['background'])) {
//                $file = $this->downloader->download($row['background']);
//            }
//
//            $place = $this->creator->create(
//                Uuid::generate(),
//                new Title($row['title']),
//                new Description($row['description']),
//                null,
//                null,
//                new WorkingHours([]),
//                null,
//                $row['label'] !== null ? new Label($row['label']) : null,
//                null,
//                null,
//                $row['code'] !== null ? new Code($row['code']) : null,
//                $row['minPrice'] !== null ? Amount::fromRoubles($row['minPrice']) : null,
//                $row['isFree'],
//                $row['isCableWayRequired'],
//                new ButtonText($row['buttonText'] ?? []),
//                new Width($row['width']),
//                $file,
//                []
//            );
//        }

        return self::SUCCESS;
    }
}
