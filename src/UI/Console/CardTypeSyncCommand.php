<?php

declare(strict_types=1);

namespace App\UI\Console;

use App\Product\Service\CardType\Syncer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * CardTypeSyncCommand.
 */
class CardTypeSyncCommand extends Command
{
    private Syncer $syncer;

    public function __construct(Syncer $syncer)
    {
        parent::__construct();
        $this->syncer = $syncer;
    }

    protected function configure(): void
    {
        $this->setName('card-type:sync');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->syncer->sync();

        return self::SUCCESS;
    }
}
