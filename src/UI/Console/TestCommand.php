<?php

declare(strict_types=1);

namespace App\UI\Console;

use App\Pps\Service\Client\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * TestCommand.
 */
class TestCommand extends Command
{
    private Client $client;

    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    protected function configure(): void
    {
        $this->setName('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $res = $this->client->__soapCall('GetAllUsers', [], null, $this->client->getHeaders('GetAllUsers'));

        var_dump($res);


//        $this->client->getServices();

//        $this->importer->import();

        return self::SUCCESS;
    }
}
