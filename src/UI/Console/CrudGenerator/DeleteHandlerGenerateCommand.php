<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\ValueObject\Uuid;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class DeleteHandlerGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:delete-handler')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Command\\' . $entityName . '\Delete');
        $namespace->addUse('App\\' . $entityName . '\Service\\' . $entityName . '\Deleter');
        $namespace->addUse(Uuid::class);
        $class = $namespace->addClass('Handler');
        $class->addProperty('deleter')
            ->setPrivate()
            ->setType('App\\' . $entityName . '\Service\\' . $entityName . '\Deleter');

        $method = $class->addMethod('__construct');
        $method->addParameter('deleter')
            ->setType('App\\' . $entityName . '\Service\\' . $entityName . '\Deleter');
        $method->setBody("\$this->deleter = \$deleter;");

        $class->addMethod('handle')
            ->setReturnType('array')
            ->setBody("\$this->deleter->delete(new Uuid(\$command->getId()));
        return  ['message' => '" . $entityName . " удалена'];")
            ->addParameter('command')
            ->setType('App\\' . $entityName . '\Command\\' . $entityName . '\Delete\Command');

        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Command/' .
            $entityName . '/Delete/Handler.php', (string) $file);


        $output->writeln('Delete Handler Class successfully generated!');

        return self::SUCCESS;
    }
}
