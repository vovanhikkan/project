<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use Nette\PhpGenerator\PhpFile;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ModelGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:model')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Model\\' . $entityName);
        $namespace->addUse(TimestampableTrait::class);
        $namespace->addUse(Uuid::class);
        $namespace->addUse("Doctrine\ORM\Mapping as ORM");

        $class = $namespace->addClass($entityName);
        $class->addComment($entityName . '.');
        $class->addComment("@ORM\Entity()\n@ORM\Table(name=\"" . $this->camelToSnake($entityName) . "\")");
        $class->addTrait(TimestampableTrait::class);
        $class->addProperty('id')
            ->setType(Uuid::class)
            ->addComment("@ORM\Id()\n@ORM\Column(type=\"uuid\")")
            ->setPrivate();


        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Model/' .
            $entityName . '/' . $entityName . '.php', (string) $file);


        $output->writeln('Model Class successfully generated!');

        return self::SUCCESS;
    }

    private function camelToSnake($input): string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
