<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class UpdateServiceGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:update-service')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Service\\' . $entityName);
        $namespace->addUse('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');
        $namespace->addUse(Flusher::class);
        $namespace->addUse('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName);
        $namespace->addUse(Uuid::class);
        $class = $namespace->addClass('Updater');
        $class->addProperty('flusher')
            ->setPrivate()
            ->setType(Flusher::class);

        $class->addProperty(lcfirst($entityName) . "Repository")
            ->setPrivate()
            ->setType('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');

        $method = $class->addMethod("__construct");
        $method->addParameter(lcfirst($entityName) . "Repository")
            ->setType('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');
        $method->addParameter('flusher')
            ->setType(Flusher::class);
        $method->setBody("\$this->" . lcfirst($entityName) . "Repository = \$" .
            lcfirst($entityName) . "Repository;\n\$this->flusher = \$flusher;");



        $class->addMethod('update')
            ->setReturnType('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName)
            ->setBody("\$" . lcfirst($entityName) . "->update();\n\$this->flusher->flush();\nreturn $" .
                lcfirst($entityName) . ";")
            ->addParameter(lcfirst($entityName))
            ->setType('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName);

        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Service/' .
            $entityName . '/Updater.php', (string) $file);


        $output->writeln('Update Service Class successfully generated!');

        return self::SUCCESS;
    }
}
