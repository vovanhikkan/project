<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\Service\Validator;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\AuthContext;
use League\Tactician\CommandBus;
use Nette\PhpGenerator\PhpFile;
use Nette\Utils\FileSystem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ViewActionGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:view-action')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\UI\Http\Action\v2\\' . $entityName);
        $namespace->addUse(ResponseInterface::class);
        $namespace->addUse(ServerRequestInterface::class);
        $namespace->addUse(Validator::class);
        $namespace->addUse(Uuid::class);
        $namespace->addUse(CommandBus::class);
        $namespace->addUse(AuthContext::class);
        $namespace->addUse(LoggerInterface::class);
        $namespace->addUse('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');
        $class = $namespace->addClass('ViewAction');
        $class->addProperty(lcfirst($entityName) . "Repository")
            ->setType('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository')
            ->setPrivate();
        $method = $class->addMethod("__construct");
        $method->addParameter(lcfirst($entityName) . "Repository")
            ->setType('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');
        $method->addParameter("validator")
            ->setType(Validator::class);
        $method->addParameter("bus")
            ->setType(CommandBus::class);
        $method->addParameter("authContext")
            ->setType(AuthContext::class);
        $method->addParameter("logger")
            ->setType(LoggerInterface::class);
        $method->setBody("parent::__construct(\$validator, \$bus, \$authContext, \$logger);\n" .
            "\$this->" . lcfirst($entityName) . "Repository = \$" .
            lcfirst($entityName) . "Repository;");


        //swagger
        $class->addComment("@OA\Get(\n\tpath=\"/v2/" . $this->camelToSnake($entityName) . "/{id}\",\n\ttags={\"" .
            $entityName . "\"},\n\t@OA\Parameter(required=true, name=\"id\", in=\"path\")," .
            "\n\tsecurity={{\"bearerAuth\":{}}},\n\tdescription=\"Просмотр " . $entityName .
            "\",\n\t@OA\Response(\n\t\tresponse=\"201\",\n\t\t@OA\MediaType(\n\t\t\t" .
            "mediaType=\"application/json\",\n\t\t\t@OA\Schema(" .
            "\n\t\t\t\t@OA\Property(property=\"id\", type=\"string\", format=\"uuid\")\n\t\t\t)\n\t\t)\n\t)\n)");
        $class->setExtends("App\UI\Http\Action\\v2\\" . $entityName . "\Abstract" . $entityName . "Action");

        //handle
        $class
            ->addMethod('handle')
            ->setReturnType(ResponseInterface::class)
            ->setBody("\$item = \$this->" . lcfirst($entityName) .
                "Repository->get(new Uuid(\$this->resolveArg('id')))" .
                ";\n\$data = \$this->serializeItem(\$item);\n\nreturn \$this->asJson(\$data);")
            ->addParameter('request')
            ->setType(ServerRequestInterface::class);


        FileSystem::write(getenv('ROOT_PATH') . 'src/UI/Http/Action/v2/' .
            $entityName . '/ViewAction.php', (string) $file);


        $output->writeln('View Action Class successfully generated!');

        return self::SUCCESS;
    }

    private function camelToSnake($input): string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
