<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Nette\Utils\FileSystem;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class AbstractActionGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:abstract-action')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\UI\Http\Action\v2\\' . $entityName);
        $namespace->addUse('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName);
        $namespace->addUse(AbstractAction::class);
        $namespace->addUse(Validator::class);
        $namespace->addUse(CommandBus::class);
        $namespace->addUse(AuthContext::class);
        $namespace->addUse(LoggerInterface::class);

        $class = $namespace->addClass('Abstract' . $entityName . 'Action');
        $method = $class->addMethod("__construct");
        $method->addParameter("validator")
            ->setType(Validator::class);
        $method->addParameter("bus")
            ->setType(CommandBus::class);
        $method->addParameter("authContext")
            ->setType(AuthContext::class);
        $method->addParameter("logger")
            ->setType(LoggerInterface::class);
        $method->setBody("parent::__construct(\$validator, \$bus, \$authContext, \$logger);");
        $class->setExtends(AbstractAction::class)
            ->setAbstract();
        $class
            ->addMethod('serializeList')
            ->setProtected()
            ->setReturnType('array')
            ->setBody("return \$this->serializeItem(\$model);")
            ->addParameter('model')
            ->setType('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName);
        $class->addMethod('serializeItem')
            ->setProtected()
            ->setReturnType('array')
            ->setBody("\$serializer = \$this->serializer;\n\nreturn [\n\t'id' =>" .
                " \$serializer->asUuid(\$model->getId()),\n\t'name' => \$serializer->asArray(\$model->getName())\n];")
            ->addParameter('model')
            ->setType('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName);

        FileSystem::write(getenv('ROOT_PATH') . 'src/UI/Http/Action/v2/' .
            $entityName . '/Abstract' . $entityName . 'Action.php', (string) $file);


        $output->writeln('Abstraction Action Class successfully generated!');

        return self::SUCCESS;
    }
}
