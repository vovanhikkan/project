<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class UpdateCommandGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:update-command')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Command\\' . $entityName . '\Update');
        $namespace->addUse('Symfony\Component\Validator\Constraints as Assert');
        $namespace->addClass('Command')
            ->addProperty('id')
            ->setType('string')
            ->addComment("@Assert\NotBlank()\n\nprivate string \$id;")
            ->setPrivate();


        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Command/' .
            $entityName . '/Update/Command.php', (string) $file);


        $output->writeln('Update Command Class successfully generated!');

        return self::SUCCESS;
    }
}
