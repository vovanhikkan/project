<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\UI\Http\ParamsExtractor;
use Nette\PhpGenerator\PhpFile;
use Nette\Utils\FileSystem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteActionGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:delete-action')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\UI\Http\Action\v2\\' . $entityName);
        $namespace->addUse(ResponseInterface::class);
        $namespace->addUse(ServerRequestInterface::class);
        $namespace->addUse('App\\' . $entityName . '\Command\\' . $entityName . '\Delete\Command');
        $class = $namespace->addClass('DeleteAction');

        //swagger
        $class->addComment("@OA\Delete(\n\tpath=\"/v2/" . $this->camelToSnake($entityName) . "/{id}\",\n\ttags={\"" .
            $entityName . "\"},\n\t@OA\Parameter(required=true, name=\"id\", in=\"path\")," .
            "\n\tsecurity={{\"bearerAuth\":{}}},\n\tdescription=\"Удаление " . $entityName .
            "\",\n\t@OA\Response(\n\t\tresponse=\"201\",\n\t\t@OA\MediaType(\n\t\t\t" .
            "mediaType=\"application/json\",\n\t\t\t@OA\Schema(\n\t\t\t\t@OA\Property" .
            "(property=\"message\", type=\"string\")\n\t\t\t)\n\t\t)\n\t)\n)");
        $class->setExtends("App\UI\Http\Action\\v2\\" . $entityName . "\Abstract" . $entityName . "Action");

        //handle
        $class
            ->addMethod('handle')
            ->setReturnType(ResponseInterface::class)
            ->setBody("\$this->denyAccessNotAdministrator();\n\n\$command" .
                " = \$this->deserialize(\$request);\n\$this->validator->validate(\$command);\n" .
                "\n\$result = \$this->bus->handle(" .
                "\$command);\n\$data = \$result;\n\nreturn \$this->asJson(\$data, 201);")
            ->addParameter('request')
            ->setType(ServerRequestInterface::class);

        //deserialize
        $class
            ->addMethod('deserialize')
            ->setPrivate()
            ->setReturnType("App\\" . $entityName . "\Command\\" . $entityName . "\Delete\Command")
            ->setBody("return new Command(\n\$this->resolveArg('id')\n);")
            ->addParameter('request')
            ->setType(ServerRequestInterface::class);


        FileSystem::write(getenv('ROOT_PATH') . 'src/UI/Http/Action/v2/' .
            $entityName . '/DeleteAction.php', (string) $file);


        $output->writeln('Delete Action Class successfully generated!');

        return self::SUCCESS;
    }

    private function camelToSnake($input): string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
