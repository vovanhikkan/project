<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class DeleteCommandGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:delete-command')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Command\\' . $entityName . '\Delete');
        $namespace->addUse('Symfony\Component\Validator\Constraints as Assert');
        $class = $namespace->addClass('Command');
        $class->addProperty('id')
            ->setType('string')
            ->addComment("@Assert\NotBlank()")
            ->setPrivate();

        $class->addMethod('__construct')
            ->setBody("\$this->id = \$id;")
            ->addParameter('id')
            ->setType('string');

        $class->addMethod('getId')
            ->setBody("return \$this->id;")
            ->setReturnType('string');


        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Command/' .
            $entityName . '/Delete/Command.php', (string) $file);


        $output->writeln('Delete Command Class successfully generated!');

        return self::SUCCESS;
    }
}
