<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\ValueObject\Uuid;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class CreateHandlerGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:create-handler')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Command\\' . $entityName . '\Create');
        $namespace->addUse('App\\' . $entityName . '\Service\\' . $entityName . '\Creator');
        $namespace->addUse('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName);
        $namespace->addUse('App\\' . $entityName . '\Command\\' . $entityName . '\Create\Command');
        $namespace->addUse(Uuid::class);
        $class = $namespace->addClass('Handler');
        $class->addProperty('creator')
            ->setPrivate()
            ->setType('App\\' . $entityName . '\Service\\' . $entityName . '\Creator');

        $method = $class->addMethod('__construct');
        $method->addParameter('creator')
            ->setType('App\\' . $entityName . '\Service\\' . $entityName . '\Creator');
        $method->setBody('$this->creator = $creator;');

        $class->addMethod('handle')
            ->setReturnType('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName)
            ->setBody("return \$this->creator->create(\nUuid::generate()\n);")
            ->addParameter('command')
            ->setType('App\\' . $entityName . '\Command\\' . $entityName . '\Create\Command');

        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Command/' .
            $entityName . '/Create/Handler.php', (string) $file);


        $output->writeln('Create Handler Class successfully generated!');

        return self::SUCCESS;
    }
}
