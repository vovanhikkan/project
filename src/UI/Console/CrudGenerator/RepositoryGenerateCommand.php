<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class RepositoryGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:repository')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Repository');
        $namespace->addUse(AbstractRepository::class);
        $namespace->addUse('App\\' . $entityName . '\Model\\' . $entityName . "\\" . $entityName);
        $namespace->addUse(Uuid::class);
        $namespace->addUse(NotFoundException::class);
        $class = $namespace->addClass($entityName . 'Repository');
        $class->addExtend(AbstractRepository::class);

        $class->addMethod('add')
            ->setBody('$this->entityManager->persist($model);')
            ->setReturnType('void')
            ->addParameter('model')
            ->setType('App\\' . $entityName . '\Model\\' . $entityName . "\\" . $entityName);

        $class->addMethod('fetchAll')
            ->setBody("\$qb = \$this->entityRepository->createQueryBuilder('" . strtolower($entityName[0]) .
                "');\n\$qb->orderBy('" . strtolower($entityName[0]) . ".createdAt', 'DESC');\n\nreturn" .
                " \$qb->getQuery()->getResult();")
            ->setReturnType('array');

        $class->addMethod('get')
            ->setBody("/** @var " . $entityName . "|null \$model */\n\$model =" .
                " \$this->entityRepository->find(\$id);\nif (\$model === null)" .
                " {\nthrow new NotFoundException(sprintf('" .
                $entityName . " with id %s not found', (string)\$id));\n}\n\nreturn \$model;")
            ->setReturnType('App\\' . $entityName . '\Model\\' . $entityName . "\\" . $entityName)
            ->addParameter('id')
            ->setType(Uuid::class);

        $class->addMethod('delete')
            ->setBody("\$model = \$this->get(\$id);\n\$this->entityManager->remove(\$model);")
            ->setReturnType('void')->addParameter('id')->setType(Uuid::class);

        $class->addMethod('getModelClassName')
            ->setBody("return " . $entityName . "::class;")
            ->setReturnType('string');

        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Repository/' .
            $entityName . 'Repository.php', (string) $file);


        $output->writeln('Repository Class successfully generated!');

        return self::SUCCESS;
    }
}
