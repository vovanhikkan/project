<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * CrudGenerateCommand.
 */
class CrudGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:generate')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("===========================Generate Started===========================\r\n");

        $entityName = $input->getArgument('entity');
        $application = $this->getApplication();

        $modelCommand = $application->find('crud:model');
        $modelCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $abstractionActionCommand = $application->find('crud:abstract-action');
        $abstractionActionCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $createActionCommand = $application->find('crud:create-action');
        $createActionCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $indexActionCommand = $application->find('crud:index-action');
        $indexActionCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $updateActionCommand = $application->find('crud:update-action');
        $updateActionCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $deleteActionCommand = $application->find('crud:delete-action');
        $deleteActionCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $viewActionCommand = $application->find('crud:view-action');
        $viewActionCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $repositoryCommand = $application->find('crud:repository');
        $repositoryCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $createCommandCommand = $application->find('crud:create-command');
        $createCommandCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $createHandlerCommand = $application->find('crud:create-handler');
        $createHandlerCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $createServiceCommand = $application->find('crud:create-service');
        $createServiceCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $updateCommandCommand = $application->find('crud:update-command');
        $updateCommandCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $updateHandlerCommand = $application->find('crud:update-handler');
        $updateHandlerCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $updateServiceCommand = $application->find('crud:update-service');
        $updateServiceCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $deleteCommandCommand = $application->find('crud:delete-command');
        $deleteCommandCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $deleteHandlerCommand = $application->find('crud:delete-handler');
        $deleteHandlerCommand->run(new ArrayInput(['entity' => $entityName]), $output);

        $deleteServiceCommand = $application->find('crud:delete-service');
        $deleteServiceCommand->run(new ArrayInput(['entity' => $entityName]), $output);


        $output->writeln("\r\n===========================Generate Finished==========================");

        return self::SUCCESS;
    }
}
