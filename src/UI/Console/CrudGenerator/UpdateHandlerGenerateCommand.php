<?php

declare(strict_types=1);

namespace App\UI\Console\CrudGenerator;

use App\Application\ValueObject\Uuid;
use Nette\Utils\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\PhpGenerator\PhpFile;

class UpdateHandlerGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('crud:update-handler')
            ->addArgument('entity', InputArgument::REQUIRED, 'Entity name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityName = $input->getArgument('entity');

        $file = new PhpFile;
        $file->setStrictTypes();
        $namespace = $file->addNamespace('App\\' . $entityName . '\Command\\' . $entityName . '\Update');
        $namespace->addUse('App\\' . $entityName . '\Service\\' . $entityName . '\Updater');
        $namespace->addUse('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName);
        $namespace->addUse('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');
        $namespace->addUse('App\\' . $entityName . '\Command\\' . $entityName . '\Update\Command');
        $namespace->addUse(Uuid::class);
        $class = $namespace->addClass('Handler');
        $class->addProperty('updater')
            ->setPrivate()
            ->setType('App\\' . $entityName . '\Service\\' . $entityName . '\Updater');
        $class->addProperty(lcfirst($entityName) . "Repository")
            ->setPrivate()
            ->setType('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');

        $method = $class->addMethod('__construct');
        $method->addParameter('updater')
            ->setType('App\\' . $entityName . '\Service\\' . $entityName . '\Updater');
        $method->addParameter(lcfirst($entityName) . "Repository")
            ->setType('App\\' . $entityName . '\Repository\\' . $entityName . 'Repository');
        $method->setBody("\$this->" . lcfirst($entityName) . "Repository = \$" .
            lcfirst($entityName) . "Repository;\n\$this->updater = \$updater;");

        $class->addMethod('handle')
            ->setReturnType('App\\' . $entityName . '\Model\\' . $entityName . '\\' . $entityName)
            ->setBody("\$" . lcfirst($entityName) . " = \$this->" . lcfirst($entityName) .
                "Repository->get(new Uuid(\$command->getId()));\n\nreturn" .
                " \$this->updater->update(\n\t\$" . lcfirst($entityName) . "\n);")
            ->addParameter('command')
            ->setType('App\\' . $entityName . '\Command\\' . $entityName . '\Update\Command');

        FileSystem::write(getenv('ROOT_PATH') . "/src/" . $entityName . '/Command/' .
            $entityName . '/Update/Handler.php', (string) $file);


        $output->writeln('Update Handler Class successfully generated!');

        return self::SUCCESS;
    }
}
