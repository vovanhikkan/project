<?php

namespace App\UI\Http\Validators\User;

use App\Common\AbstractValidator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ForgotPasswordValidator extends AbstractValidator
{
    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @throws \App\Exception\AppValidateException
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $content = $request->getBody()->getContents();
        $params  = json_decode($content, true);

        $collection = new Assert\Collection([
            'fields'               => [
                'email' => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\Email(['message' => 'Не валидный формат email: {{ field }}']),
                ],
            ],
            'missingFieldsMessage' => 'Отсутсвуют обезательное поле {{ field }}',
            'allowExtraFields'     => true,
        ]);

        $errors = $this->validate($params, $collection);

        if ($errors) {
            $this->handlerError($errors, StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return $handler->handle($request);
    }
}