<?php

namespace App\UI\Http\Validators\User;

use App\Common\AbstractValidator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\Constraints as Assert;

class RegistrationUserValidation extends AbstractValidator
{
    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @throws \App\Exception\AppValidateException
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $content = $request->getBody()->getContents();
        $params  = json_decode($content, true);

        if (!$params) {
            $this->handlerError(['message' => 'Ошибка декодирования JSON'],StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        $collection = new Assert\Collection([
            'fields' => [
                'email' => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\Email(['message' => 'Не валидный формат email']),
                ],
                'firstName' => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\Length([
                        'min' => 2,
                        'minMessage' => 'Поле firstName не должно быть короче 2 символов',
                    ]),
                ],
                'lastName' => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\Length([
                        'min' => 2,
                        'minMessage' => 'Поле firstName не должно быть короче 2 символов',
                    ]),
                ],
                'sex' => new Assert\Optional([
                    new Assert\NotBlank(['message' => 'Поле sex не может быть пустым (если передано)']),
                    new Assert\Choice([
                        'choices' => [1, 2],
                        'message' => 'Поле sex может содержать только значения: {{ choices }}'
                    ]),
                ]),
                'birthdate' => new Assert\Optional([
                    new Assert\NotBlank(['message' => 'Поле birthdate не может быть пустым (если передано)']),
                    new Assert\Date(['message' => 'Поле birthdate должно быть датой в формате YYYY-MM-DD']),
                ]),
            ],
            'missingFieldsMessage' => 'Отсутсвуют обезательное поле {{ field }}',
            'allowExtraFields'     => true,
        ]);

        $errors = $this->validate($params, $collection);

        if ($errors) {
            $this->handlerError($errors, StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return $handler->handle($request);
    }
}