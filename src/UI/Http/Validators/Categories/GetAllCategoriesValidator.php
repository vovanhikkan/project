<?php

namespace App\UI\Http\Validators\Categories;

use App\Common\AbstractValidator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\Constraints as Assert;

class GetAllCategoriesValidator extends AbstractValidator
{
    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @throws \App\Exception\AppValidateException
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $collection = new Assert\Collection([
            'fields'               => [
                'by' => [
                    new Assert\Expression([
                        'expression' => 'value in [sections, places]',
                        'message'    => 'Параметр "by" должен быть "sections" или "places"',
                    ]),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                ],
            ],
            'missingFieldsMessage' => 'Отсутсвуют обезательное поле {{ field }}',
            'allowExtraFields'     => true,
        ]);

        $errors = $this->validate($request->getQueryParams(), $collection);

        if ($errors) {
            $this->handlerError($errors, StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return $handler->handle($request);
    }
}