<?php

namespace App\UI\Http\Validators\Orders;

use App\Common\AbstractValidator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Validator\Constraints as Assert;

class NewQuickOrderValidator extends AbstractValidator
{
    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @throws \App\Exception\AppValidateException
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $collection = new Assert\Collection([
            'fields'               => [
                'email'      => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                ],
                'phone'      => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                ],
                'first_name' => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                ],
                'last_name'  => [
                    new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                    new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                ],
                'basket'     => new Assert\Collection([
                    'fields'               => [
                        'items' => new Assert\All(['constraints' => [
                            new Assert\Collection([
                                'fields'               => [
                                    'productId'   => [
                                        new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                                        new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                                    ],
                                    'quantity'    => [
                                        new Assert\NotBlank(['message' => 'Поле {{ field }} не может быть пустым']),
                                        new Assert\NotNull(['message' => 'Поле {{ field }} не может быть пустым']),
                                    ],
                                ],
                            ]),
                        ]]),
                    ],
                    'missingFieldsMessage' => 'Отсутсвуют обезательное поле {{ field }}',
                    'allowExtraFields'     => true,
                ]),
            ],
            'missingFieldsMessage' => 'Отсутсвуют обезательное поле {{ field }}',
            'allowExtraFields'     => true,
        ]);

        $content = $request->getBody()->getContents();

        $errors = $this->validate(json_decode($content, true), $collection);

        if ($errors) {
            $this->handlerError($errors, StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return $handler->handle($request);
    }
}