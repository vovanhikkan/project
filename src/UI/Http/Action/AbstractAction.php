<?php

declare(strict_types=1);

namespace App\UI\Http\Action;

use App\Application\Service\Validator;
use App\Auth\Model\User\User;
use App\Auth\Service\AuthContext;
use App\Storage\Model\File\File;
use App\Storage\Service\File\Locator;
use App\UI\Http\Serializer;
use InvalidArgumentException;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpUnauthorizedException;
use Slim\Psr7\Stream;

/**
 * AbstractAction.
 */
abstract class AbstractAction
{
    protected Validator $validator;
    protected CommandBus $bus;
    protected AuthContext $authContext;
    protected LoggerInterface $logger;
    protected ServerRequestInterface $request;
    protected ResponseInterface $response;
    protected Serializer $serializer;
    protected Locator $locator;

    private array $args = [];

    public function __construct(
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        $this->validator = $validator;
        $this->bus = $bus;
        $this->logger = $logger;
        $this->authContext = $authContext;
        $this->serializer = new Serializer();
        $this->locator = $locator;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {

        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        return $this->handle($request);
    }

    abstract public function handle(ServerRequestInterface $request): ResponseInterface;

    protected function resolveArg(string $name): string
    {
        if (!isset($this->args[$name])) {
            throw new InvalidArgumentException("Could not resolve argument `{$name}`.");
        }

        return $this->args[$name];
    }

    protected function asJson(array $data, int $status = 200, array $headers = []): ResponseInterface
    {
        $json = json_encode($data, getenv('APP_ENV') === 'dev' ? JSON_PRETTY_PRINT : 0);
        $this->response->getBody()->write($json);

        $response = $this->response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($status)
        ;

        foreach ($headers as $name => $value) {
            $response = $response->withHeader($name, $value);
        }

        return $response;
    }

    protected function asEmpty(int $status = 204): ResponseInterface
    {
        return $this->response->withStatus($status);
    }

    protected function asFile(string $filePath, string $fileName): ResponseInterface
    {
        $fh = fopen($filePath, 'rb');
        $stream = new Stream($fh);

        return $this->response
            ->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Type', 'application/octet-stream')
            ->withHeader('Content-Disposition', 'attachment;filename="' . $fileName . '"')
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->withHeader('Content-Length', filesize($filePath))
            ->withBody($stream);
    }

    /**
     * @param File|null $file
     * @return array|null
     */
    protected function getFileData(?File $file)
    {
        if (!$file) {
            return null;
        }
        return [
            'name' => $file->getName()->getValue(),
            'fileUrl' => $this->locator->getAbsoluteUrl($file),
        ];
    }

    /**
     * @param File|null $file
     * @return array|null
     */
    protected function asSvg(?File $file)
    {
        if (!$file) {
            return null;
        }
        return [
            'name' => $file->getName()->getValue(),
            'fileUrl' => $this->locator->getAbsoluteUrl($file),
            'content' => $this->locator->getFileContent($file),
        ];
    }

    protected function denyAccessNotAuthenticated(): void
    {
        if (!$this->authContext->isAuthenticated()) {
            throw new HttpUnauthorizedException($this->request);
        }
    }

    protected function denyAccessNotAdministrator(): void
    {
        $this->denyAccessNotAuthenticated();

        if (!$this->getCurrentUser()->getRole()->isAdmin()) {
            throw new HttpUnauthorizedException($this->request);
        }
    }

    protected function getCurrentUser(): ?User
    {
        return $this->authContext->getCurrentUser();
    }

    protected function ensureUserIdAccess(string $id): void
    {
        $hasAccess = false;

        if (
            $this->getCurrentUser()->getRole()->isCustomer() &&
            (string)$this->getCurrentUser()->getId() === $id
        ) {
            $hasAccess = true;
        } elseif ($this->getCurrentUser()->getRole()->isAdmin()) {
            $hasAccess = true;
        }

        if (!$hasAccess) {
            throw new HttpUnauthorizedException($this->request);
        }
    }
}
