<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\LabelType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Place\Model\LabelType\LabelType;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractLabelTypeAction extends AbstractAction
{
	public function __construct(
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
		parent::__construct($validator, $bus, $authContext, $logger, $locator);
	}


	protected function serializeList(LabelType $model): array
	{
		return $this->serializeItem($model);
	}


	protected function serializeItem(LabelType $model): array
	{
		$serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'icon' => $this->locator->getAbsoluteUrl($model->getIcon()),
            'svgIcon' => $this->locator->getAbsoluteUrl($model->getSvgIcon()),
            'defaultColor' => $serializer->asStringOrNull($model->getDefaultColor()),
            'defaultContent' => $serializer->asString($model->getDefaultContent()),
        ];
	}
}
