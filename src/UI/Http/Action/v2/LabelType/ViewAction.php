<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\LabelType;

use App\Application\Service\Validator;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\AuthContext;
use App\Place\Repository\LabelTypeRepository;
use App\Storage\Service\File\Locator;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * @OA\Get(
 * 	path="/v2/label_type/{id}",
 * 	tags={"LabelType"},
 * 	@OA\Parameter(required=true, name="id", in="path"),
 * 	security={{"bearerAuth":{}}},
 * 	description="Просмотр LabelType",
 * 	@OA\Response(
 * 		response="201",
 * 	    description="",
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 * 				@OA\Property(property="id", type="string", format="uuid")
 * 			)
 * 		)
 * 	)
 * )
 */
class ViewAction extends AbstractLabelTypeAction
{
	private LabelTypeRepository $labelTypeRepository;


	public function __construct(
		LabelTypeRepository $labelTypeRepository,
		Validator $validator,
		CommandBus $bus,
		AuthContext $authContext,
		LoggerInterface $logger,
        Locator $locator
	) {
		parent::__construct($validator, $bus, $authContext, $logger, $locator);
		$this->labelTypeRepository = $labelTypeRepository;
	}


	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$item = $this->labelTypeRepository->get(new Uuid($this->resolveArg('id')));
		$data = $this->serializeItem($item);

		return $this->asJson($data);
	}
}
