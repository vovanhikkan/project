<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\LabelType;

use App\Place\Command\LabelType\Create\Command;
use App\Place\Model\LabelType\LabelType;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 * 	path="/v2/label_type",
 * 	tags={"LabelType"},
 * 	security={{"bearerAuth":{}}},
 * 	description="Добавление LabelType",
 * 	@OA\RequestBody(
 * 		required=true,
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema()
 * 		)
 * 	),
 * 	@OA\Response(
 * 		response="201",
 * 	    description="",
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema()
 * 		)
 * 	)
 * )
 */
class CreateAction extends AbstractLabelTypeAction
{
	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$this->denyAccessNotAdministrator();

		$command = $this->deserialize($request);
		$this->validator->validate($command);
		/** @var LabelType $result */
		$result = $this->bus->handle($command);
		$data = $this->serializeItem($result);

		return $this->asJson($data, 201);
	}


	private function deserialize(ServerRequestInterface $request): Command
	{
		$paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

		$command = new Command();

		return $command;
	}
}
