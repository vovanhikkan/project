<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\LabelType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Place\Repository\LabelTypeRepository;
use App\Storage\Service\File\Locator;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * @OA\Get(
 * 	path="/v2/label_type",
 * 	tags={"LabelType"},
 * 	security={{"bearerAuth":{}}},
 * 	description="Список LabelType",
 * 	@OA\Response(
 * 		response="201",
 * 	    description="",
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 * 				@OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="icon", type="string"),
 *              @OA\Property(property="svgIcon", type="string"),
 *              @OA\Property(property="defaultColor", type="string"),
 *              @OA\Property(property="defaultContent", type="string")
 * 			)
 * 		)
 * 	)
 * )
 */
class IndexAction extends AbstractLabelTypeAction
{
	private LabelTypeRepository $labelTypeRepository;


	public function __construct(
		LabelTypeRepository $labelTypeRepository,
		Validator $validator,
		CommandBus $bus,
		AuthContext $authContext,
		LoggerInterface $logger,
        Locator $locator
	) {
		parent::__construct($validator, $bus, $authContext, $logger, $locator);
		$this->labelTypeRepository = $labelTypeRepository;
	}


	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$items = $this->labelTypeRepository->fetchAll();
		$data = array_map([$this, 'serializeItem'], $items);

		return $this->asJson($data);
	}
}
