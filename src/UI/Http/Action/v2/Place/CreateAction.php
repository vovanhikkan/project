<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Place;

use App\Place\Command\Place\Create\Command;
use App\Place\Model\Place\Place;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/place",
 *     tags={"Места"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление места",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"title", "description", "workingHoursDescription", "worksFrom", "worksTo", "isFree", "isCableWayRequired", "buttonText", "sections"},
 *                 @OA\Property(property="title", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="subTitle", type="object"),
 *                 @OA\Property(property="subDescription", type="object"),
 *                 @OA\Property(property="workingHoursDescription", type="object"),
 *                 @OA\Property(property="isFullDay", type="boolean"),
 *                 @OA\Property(property="worksFrom", type="string"),
 *                 @OA\Property(property="worksTo", type="string"),
 *                 @OA\Property(property="ageRestriction", type="object"),
 *                 @OA\Property(property="label", type="object"),
 *                 @OA\Property(property="address", type="object"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="minPrice", type="integer"),
 *                 @OA\Property(property="isFree", type="boolean"),
 *                 @OA\Property(property="isCableWayRequired", type="boolean"),
 *                 @OA\Property(property="buttonText", type="object"),
 *                 @OA\Property(property="int", type="integer"),
 *                 @OA\Property(property="sections", type="array", @OA\Items(type="string", format="uuid")),
 *                 @OA\Property(property="categories", type="array", @OA\Items(type="string", format="uuid"))
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="title", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="subTitle", type="object"),
 *                 @OA\Property(property="subDescription", type="object"),
 *                 @OA\Property(property="workingHoursDescription", type="object"),
 *                 @OA\Property(property="worksFrom", type="string"),
 *                 @OA\Property(property="worksTo", type="string"),
 *                 @OA\Property(property="ageRestriction", type="object"),
 *                 @OA\Property(property="label", type="object"),
 *                 @OA\Property(property="address", type="object"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="minPrice", type="integer"),
 *                 @OA\Property(property="isFree", type="boolean"),
 *                 @OA\Property(property="isCableWayRequired", type="boolean"),
 *                 @OA\Property(property="buttonText", type="object"),
 *                 @OA\Property(property="int", type="integer"),
 *                 @OA\Property(property="sections", type="array", @OA\Items(type="string", format="uuid")),
 *                 @OA\Property(property="categories", type="array", @OA\Items(type="string", format="uuid"))
 *             ),
 *         )
 *     )
 * )
 */
class CreateAction extends AbstractPlaceAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Place $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('title'),
            $paramsExtractor->getSimpleArray('description'),
            $paramsExtractor->getSimpleArray('workingHoursDescription'),
            $paramsExtractor->getString('worksFrom'),
            $paramsExtractor->getString('worksTo'),
            $paramsExtractor->getBool('isFree'),
            $paramsExtractor->getBool('isCableWayRequired'),
            $paramsExtractor->getSimpleArray('buttonText'),
            $paramsExtractor->getSimpleArray('sections'),
            $paramsExtractor->getSimpleArray('categories'),
        );

        if ($paramsExtractor->has('subTitle')) {
            $command->setSubTitle($paramsExtractor->getSimpleArray('subTitle'));
        }

        if ($paramsExtractor->has('subDescription')) {
            $command->setSubDescription($paramsExtractor->getSimpleArray('subDescription'));
        }

        if ($paramsExtractor->has('ageRestriction')) {
            $command->setAgeRestriction($paramsExtractor->getSimpleArray('ageRestriction'));
        }

        if ($paramsExtractor->has('label')) {
            $command->setLabel($paramsExtractor->getSimpleArray('label'));
        }

        if ($paramsExtractor->has('address')) {
            $command->setAddress($paramsExtractor->getSimpleArray('address'));
        }

        if ($paramsExtractor->has('phone')) {
            $command->setPhone($paramsExtractor->getString('phone'));
        }

        if ($paramsExtractor->has('code')) {
            $command->setCode($paramsExtractor->getString('code'));
        }

        if ($paramsExtractor->has('minPrice')) {
            $command->setMinPrice($paramsExtractor->getInt('minPrice'));
        }

        if ($paramsExtractor->has('width')) {
            $command->setWidth($paramsExtractor->getInt('width'));
        }

        return $command;
    }
}
