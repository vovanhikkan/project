<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Place;

use App\Application\Service\Validator;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\AuthContext;
use App\Content\Model\Category\Category;
use App\Location\Model\Location\Location;
use App\Place\Model\Block\Block;
use App\Place\Model\GeoPoint\GeoPoint;
use App\Place\Model\Label\Label;
use App\Place\Model\Place\Place;
use App\Place\Model\Place\PlacePhoto;
use App\Place\Model\Place\PlaceVideo;
use App\Place\Repository\PlaceRepository;
use App\Product\Model\Section\Section;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Ds\Collection;
use League\Tactician\CommandBus;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * AbstractPlaceAction.
 */
abstract class AbstractPlaceAction extends AbstractAction
{
    protected PlaceRepository $placeRepository;

    public function __construct(
        PlaceRepository $placeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->placeRepository = $placeRepository;
    }

    protected function serializeList(Place $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'title' => $serializer->asArray($model->getTitle())
        ];
    }

    protected function serializeItem(Place $model): array
    {
        $serializer = $this->serializer;
        $location = $model->getLocation();

        if ($label = $model->getLabel()) {
            $labelType = $label->getLabelType();

            $label = [
                'id' => $serializer->asUuid($label->getId()),
                'labelType' => $labelType ? [
                    "id" => $serializer->asUuid($labelType->getId()),
                    "icon" => $this->getFileData($labelType->getIcon()),
                    "svg_icon" => $this->getFileData($labelType->getSvgIcon()),
                    "default_color" => $serializer->asString($labelType->getDefaultColor()),
                    "default_content" => $serializer->asString($labelType->getDefaultContent()),
                ] : null,
                'color' => $serializer->asString($label->getColor()),
                'content' => $serializer->asArray($label->getContent()),
            ];
        }

        $recommendedPlaces = $model->getRecommendedId() ? $this->placeRepository->fetchById($model->getRecommendedId()->getValue()) : [];

        $model->getWorksFrom() ? $worksFrom = $model->getWorksFrom()->format("H:i") : $worksFrom = null;
        $model->getWorksTo() ? $worksTo = $model->getWorksTo()->format("H:i") : $worksTo = null;

        return array_merge($this->serializeList($model), [
            'description' => $serializer->asArray($model->getDescription()),
            'subTitle' => $serializer->asArrayOrNull($model->getSubTitle()),
            'subDescription' => $serializer->asArrayOrNull($model->getSubDescription()),
            'workingHoursDescription' => $serializer->asArray($model->getWorkingHoursDescription()),
            'isFullDay' => $model->isFullDay(),
            'worksFrom' => $worksFrom,
            'worksTo' => $worksTo,
            'ageRestriction' => $serializer->asArrayOrNull($model->getAgeRestriction()),
            'label' => $label,
            'address' => $serializer->asArrayOrNull($model->getAddress()),
            'phone' => $serializer->asStringOrNull($model->getPhone()),
            'code' => $serializer->asStringOrNull($model->getCode()),
            'minPrice' => $serializer->asIntOrNull($model->getMinPrice()),
            'isFree' => $model->isFree(),
            'isCableWayRequired' => $model->isCableWayRequired(),
            'buttonText' => $serializer->asArray($model->getButtonText()),
            'width' => $serializer->asIntOrNull($model->getWidth()),
            'backgroundUrl' => $model->getBackground() !== null
                ? $this->locator->getAbsoluteUrl($model->getBackground()) : null,
            'location' => $location ? [
                'id' => $serializer->asUuid($location->getId()),
                'isActive' => $location->isActive(),
                'name' => $serializer->asArray($location->getName()),
                'sort' => $serializer->asInt($location->getSort()),
                'altitude' => $serializer->asInt($location->getAltitude()),
            ] : null,
            'recommendedPlaces' => array_map(
                function (Place $place) use ($serializer) {
                    $location = $place->getLocation();
                    if ($labelRecommendedPlace = $place->getLabel()) {
                        $labelTypeRecommendedPlace = $labelRecommendedPlace->getLabelType();
                        $labelSerialized = [
                            'id' => $serializer->asUuid($labelRecommendedPlace->getId()),
                            'labelType' => $labelTypeRecommendedPlace ? [
                                "id" => $serializer->asUuid($labelTypeRecommendedPlace->getId()),
                                "icon" => $this->getFileData($labelTypeRecommendedPlace->getIcon()),
                                "svg_icon" => $this->getFileData($labelTypeRecommendedPlace->getSvgIcon()),
                                "default_color" => $serializer->asString($labelTypeRecommendedPlace->getDefaultColor()),
                                "default_content" => $serializer->asString($labelTypeRecommendedPlace->getDefaultContent()),
                            ] : null,
                            'color' => $serializer->asString($labelRecommendedPlace->getColor()),
                            'content' => $serializer->asArray($labelRecommendedPlace->getContent()),
                        ];
                    }

                    return [
                        'id' => $serializer->asUuid($place->getId()),
                        'title' => $serializer->asArray($place->getTitle()),
                        'description' => $serializer->asArray($place->getDescription()),
                        'subTitle' => $serializer->asArrayOrNull($place->getSubTitle()),
                        'subDescription' => $serializer->asArrayOrNull($place->getSubDescription()),
                        'workingHours' => $serializer->asArray($place->getWorkingHoursDescription()),
                        'ageRestriction' => $serializer->asArrayOrNull($place->getAgeRestriction()),
                        'label' => $labelSerialized,
                        'address' => $serializer->asArrayOrNull($place->getAddress()),
                        'phone' => $serializer->asStringOrNull($place->getPhone()),
                        'code' => $serializer->asStringOrNull($place->getCode()),
                        'minPrice' => $serializer->asIntOrNull($place->getMinPrice()),
                        'isFree' => $place->isFree(),
                        'isCableWayRequired' => $place->isCableWayRequired(),
                        'buttonText' => $serializer->asArray($place->getButtonText()),
                        'width' => $serializer->asIntOrNull($place->getWidth()),
                        'backgroundUrl' => $place->getBackground() !== null
                            ? $this->locator->getAbsoluteUrl($place->getBackground()) : null,
                        'location' => $location ? [
                            'id' => $serializer->asUuid($location->getId()),
                            'isActive' => $location->isActive(),
                            'name' => $serializer->asArray($location->getName()),
                            'sort' => $serializer->asInt($location->getSort()),
                            'altitude' => $serializer->asInt($location->getAltitude()),
                        ] : null,
                    ];
                },
                $recommendedPlaces
            ),
            'sections' => array_map(
                function (Section $section) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($section->getId()),
                        'code' => $serializer->asString($section->getCode()),
                        'name' => $serializer->asArray($section->getName()),
                        'description' => $serializer->asArray($section->getDescription()),
                        'tariffDescription' => $serializer->asArray($section->getTariffDescription()),
                        'innerDescription' => $serializer->asArray($section->getInnerDescription()),
                        'webIcon' => $section->getWebIcon() !== null
                            ? $this->locator->getAbsoluteUrl($section->getWebIcon())
                            : null,
                        'webBackground' => $section->getWebBackground() !== null
                            ? $this->locator->getAbsoluteUrl($section->getWebBackground())
                            : null,
                        'mobileIcon' => $section->getMobileIcon() !== null
                            ? $this->locator->getAbsoluteUrl($section->getMobileIcon())
                            : null,
                        'mobileBackground' => $section->getMobileBackground() !== null
                            ? $this->locator->getAbsoluteUrl($section->getMobileBackground())
                            : null,
                        'picture' => $section->getPicture() !== null
                            ? $this->locator->getAbsoluteUrl($section->getPicture())
                            : null,
                        'recomendationSort' => $serializer->asInt($section->getRecommendationSort()),
                        'settings' => $section->getSectionSettings() !== null
                            ? [
                                'controls' => $this->serializer->asArray($section->getSectionSettings()->getControls()),
                                'isOnlineOnly' => $section->getSectionSettings()->isOnlineOnly(),
                                'isActivationDateRequired' =>
                                    $section->getSectionSettings()->isActivationDateRequired(),
                                'activeDays' =>
                                    $this->serializer->asInt($section->getSectionSettings()->getActiveDays()),
                                'isNotQuantitative' => $section->getSectionSettings()->isNotQuantitative(),
                            ] : null
                    ];
                },
                $model->getSections()
            ),
            'categories' => array_map(
                function (Category $category) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($category->getId()),
                        'name' => $serializer->asArray($category->getName()),
                        'webGradientColor1' => $serializer->asStringOrNull($category->getWebGradientColor1()),
                        'webGradientColor2' => $serializer->asStringOrNull($category->getWebGradientColor2()),
                        'webIcon' => $category->getWebIcon() !== null
                            ? $this->locator->getAbsoluteUrl($category->getWebIcon())
                            : null,
                        'webIconSvg' => $category->getWebIconSvg() !== null
                            ? $this->locator->getAbsoluteUrl($category->getWebIconSvg())
                            : null,
                        'mobileIcon' => $category->getMobileIcon() !== null
                            ? $this->locator->getAbsoluteUrl($category->getMobileIcon())
                            : null,
                        'mobileBackground' => $category->getMobileBackground() !== null
                            ? $this->locator->getAbsoluteUrl($category->getMobileBackground())
                            : null,
                    ];
                },
                $model->getCategories()
            ),
            'blocks' => array_map(
                function (Block $block) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($block->getId()),
                        'title' => $serializer->asArray($block->getTitle()),
                        'body' => $serializer->asArray($block->getBody()),
                        'photo' => $block->getPhoto() !== null
                            ? $this->locator->getAbsoluteUrl($block->getPhoto())
                            : null,
                        'video' => $block->getVideo() !== null
                            ? [
                                'id' => $serializer->asUuid($block->getVideo()->getId()),
                                'youtubeCode' => $serializer->asString($block->getVideo()->getYoutubeCode()),
                                'backgroundImage' => $block->getVideo()->getBackgroundImage() !== null
                                ? $this->locator->getAbsoluteUrl($block->getVideo()->getBackgroundImage())
                                    : null,
                            ] : null
                    ];
                },
                $model->getBlocks()
            ),
            'geoPoints' => array_map(
                function (GeoPoint $geoPoint) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($geoPoint->getId()),
                        'latitude' => $serializer->asFloat($geoPoint->getLatitude()),
                        'longitude' => $serializer->asFloat($geoPoint->getLongitude()),
                        'description' => $serializer->asArray($geoPoint->getDescription())
                    ];
                },
                $model->getGeoPoints()
            ),
            'photos' => array_map(
                function (PlacePhoto $placePhoto) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($placePhoto->getId()),
                        'file' => $placePhoto->getFile() !== null
                            ? $this->locator->getAbsoluteUrl($placePhoto->getFile())
                            : null,
                    ];
                },
                $model->getPlacePhotos()
            ),
            'videos' => array_map(
                function (PlaceVideo $placeVideo) use ($serializer) {
                    $youTubeCode = null;
                    $youTubeURL = $placeVideo->getVideo()->getYoutubeCode()->getValue();
                    if (preg_match(
                        "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/",
                        $youTubeURL,
                        $matches
                    )) {
                        $youTubeCode = $matches[1];
                    }
                    return [
                        'id' => $serializer->asUuid($placeVideo->getId()),
                        'youtubeCode' => $youTubeCode,
                        'backgroundImage' => $placeVideo->getVideo()->getBackgroundImage() !== null
                            ? $this->locator->getAbsoluteUrl($placeVideo->getVideo()->getBackgroundImage())
                            : null,
                    ];
                },
                $model->getPlaceVideos()
            )
        ]);
    }


    protected function isWorkingNow(ServerRequestInterface $request) : bool
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);

        if ($paramsExtractor->has('isWorkingNow')) {
            return $paramsExtractor->getBool('isWorkingNow');
        }

        return false;
    }
}
