<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Place;

use App\Place\Command\Place\Update\Command;
use App\Place\Model\Place\Place;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Patch(
 *     path="/v2/place/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Места"},
 *     security={{"bearerAuth":{}}},
 *     description="Редактирование места",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"title", "description", "workingHours", "isFree", "isCableWayRequired", "buttonText", "sections"},
 *                 @OA\Property(property="title", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="subTitle", type="object"),
 *                 @OA\Property(property="subDescription", type="object"),
 *                 @OA\Property(property="workingHoursDescription", type="object"),
 *                 @OA\Property(property="isFullDay", type="boolean"),
 *                 @OA\Property(property="worksFrom", type="string"),
 *                 @OA\Property(property="worksTo", type="string"),
 *                 @OA\Property(property="ageRestriction", type="object"),
 *                 @OA\Property(property="label", type="object"),
 *                 @OA\Property(property="address", type="object"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="minPrice", type="integer"),
 *                 @OA\Property(property="isFree", type="boolean"),
 *                 @OA\Property(property="isCableWayRequired", type="boolean"),
 *                 @OA\Property(property="buttonText", type="object"),
 *                 @OA\Property(property="int", type="integer"),
 *                 @OA\Property(property="sections", type="array", @OA\Items(type="string", format="uuid"))
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="title", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="subTitle", type="object"),
 *                 @OA\Property(property="subDescription", type="object"),
 *                 @OA\Property(property="workingHoursDescription", type="object"),
 *                 @OA\Property(property="isFullDay", type="boolean"),
 *                 @OA\Property(property="worksFrom", type="string"),
 *                 @OA\Property(property="worksTo", type="string"),
 *                 @OA\Property(property="ageRestriction", type="object"),
 *                 @OA\Property(property="label", type="object"),
 *                 @OA\Property(property="address", type="object"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="minPrice", type="integer"),
 *                 @OA\Property(property="isFree", type="boolean"),
 *                 @OA\Property(property="isCableWayRequired", type="boolean"),
 *                 @OA\Property(property="buttonText", type="object"),
 *                 @OA\Property(property="width", type="integer"),
 *                 @OA\Property(property="backgroundUrl", type="string"),
 *                 @OA\Property(property="location", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="isActive", type="boolean"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="sort", type="number"),
 *                          @OA\Property(property="altitude", type="number"),
 *                      )
 *                  ),
 *                 @OA\Property(property="recommendedPlaces", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="title", type="object"),
 *                          @OA\Property(property="description", type="object"),
 *                          @OA\Property(property="subTitle", type="object"),
 *                          @OA\Property(property="subDescription", type="object"),
 *                          @OA\Property(property="workingHours", type="object"),
 *                          @OA\Property(property="ageRestriction", type="object"),
 *                          @OA\Property(property="label", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="labelType", type="array",
 *                                      @OA\Items(
 *                                          @OA\Property(property="id", type="string", format="uuid"),
 *                                          @OA\Property(property="icon", type="object"),
 *                                          @OA\Property(property="svg_icon", type="object"),
 *                                          @OA\Property(property="default_color", type="object"),
 *                                          @OA\Property(property="default_content", type="object"),
 *                                      )
 *                                  ),
 *                                  @OA\Property(property="color", type="string"),
 *                                  @OA\Property(property="content", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="address", type="object"),
 *                          @OA\Property(property="phone", type="string"),
 *                          @OA\Property(property="code", type="string"),
 *                          @OA\Property(property="minPrice", type="number"),
 *                          @OA\Property(property="isFree", type="boolean"),
 *                          @OA\Property(property="isCableWayRequired", type="boolean"),
 *                          @OA\Property(property="buttonText", type="object"),
 *                          @OA\Property(property="width", type="number"),
 *                          @OA\Property(property="backgroundUrl", type="string"),
 *                          @OA\Property(property="location", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="isActive", type="boolean"),
 *                                  @OA\Property(property="name", type="object"),
 *                                  @OA\Property(property="sort", type="number"),
 *                                  @OA\Property(property="altitude", type="number"),
 *                              )
 *                          ),
 *                      )
 *                 ),
 *                 @OA\Property(property="sections", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="code", type="string"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="description", type="object"),
 *                          @OA\Property(property="tariffDescription", type="object"),
 *                          @OA\Property(property="innerDescription", type="object"),
 *                          @OA\Property(property="webIcon", type="string"),
 *                          @OA\Property(property="webBackground", type="string"),
 *                          @OA\Property(property="mobileIcon", type="string"),
 *                          @OA\Property(property="mobileBackground", type="string"),
 *                          @OA\Property(property="picture", type="string"),
 *                          @OA\Property(property="recomendationSort", type="string"),
 *                          @OA\Property(property="settings", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="controls", type="object"),
 *                                  @OA\Property(property="isOnlyOne", type="boolean"),
 *                                  @OA\Property(property="isActivationDateRequired", type="boolean"),
 *                                  @OA\Property(property="activeDays", type="number"),
 *                                  @OA\Property(property="isNotQuantitave", type="boolean"),
 *                              ),
 *                          ),
 *                      ),
 *                 ),
 *                  @OA\Property(property="categories", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="webGradientColor1", type="string"),
 *                          @OA\Property(property="webGradientColor2", type="string"),
 *                          @OA\Property(property="webIcon", type="string"),
 *                          @OA\Property(property="webIconSvg", type="string"),
 *                          @OA\Property(property="mobileIcon", type="string"),
 *                          @OA\Property(property="mobileBackground", type="string"),
 *                      ),
 *                 ),
 *                 @OA\Property(property="blocks", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="title", type="object"),
 *                          @OA\Property(property="body", type="object"),
 *                          @OA\Property(property="photo", type="string"),
 *                          @OA\Property(property="video", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="youtubeCode", type="string"),
 *                                  @OA\Property(property="backgroundImage", type="string"),
 *                              ),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(property="geoPoints", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="latitude", type="number"),
 *                          @OA\Property(property="longitude", type="number"),
 *                          @OA\Property(property="description", type="object"),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(property="photos", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="file", type="string"),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(property="videos", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="youtubeCode", type="string"),
 *                          @OA\Property(property="backgroundImage", type="string"),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(
 *                      property="width",
 *                      type="integer",
 *                      enum={100, 200, 300, 400},
 *                      description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *                 ),
 *             ),
 *         )
 *     )
 * )
 */
class UpdateAction extends AbstractPlaceAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Place $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getSimpleArray('title'),
            $paramsExtractor->getSimpleArray('description'),
            $paramsExtractor->getSimpleArray('workingHours'),
            $paramsExtractor->getBool('isFree'),
            $paramsExtractor->getBool('isCableWayRequired'),
            $paramsExtractor->getSimpleArray('buttonText'),
            $paramsExtractor->getSimpleArray('sections'),
            $paramsExtractor->getSimpleArray('categories'),
        );

        if ($paramsExtractor->has('subTitle')) {
            $command->setSubTitle($paramsExtractor->getSimpleArray('subTitle'));
        }

        if ($paramsExtractor->has('subDescription')) {
            $command->setSubDescription($paramsExtractor->getSimpleArray('subDescription'));
        }

        if ($paramsExtractor->has('ageRestriction')) {
            $command->setAgeRestriction($paramsExtractor->getSimpleArray('ageRestriction'));
        }

        if ($paramsExtractor->has('label')) {
            $command->setLabel($paramsExtractor->getSimpleArray('label'));
        }

        if ($paramsExtractor->has('address')) {
            $command->setAddress($paramsExtractor->getSimpleArray('address'));
        }

        if ($paramsExtractor->has('phone')) {
            $command->setPhone($paramsExtractor->getString('phone'));
        }

        if ($paramsExtractor->has('code')) {
            $command->setCode($paramsExtractor->getString('code'));
        }

        if ($paramsExtractor->has('minPrice')) {
            $command->setMinPrice($paramsExtractor->getInt('minPrice'));
        }

        if ($paramsExtractor->has('width')) {
            $command->setWidth($paramsExtractor->getInt('width'));
        }

        return $command;
    }
}
