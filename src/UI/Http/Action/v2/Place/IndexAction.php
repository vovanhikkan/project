<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Place;

use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/place",
 *     tags={"Места"},
 *     description="Список мест",
 *     @OA\RequestBody(
 *         required=false,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="isWorkingNow", type="boolean"),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="title", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="subTitle", type="object"),
 *                 @OA\Property(property="subDescription", type="object"),
 *                 @OA\Property(property="workingHoursDescription", type="object"),
 *                 @OA\Property(property="isFullDay", type="boolean"),
 *                 @OA\Property(property="worksFrom", type="string"),
 *                 @OA\Property(property="worksTo", type="string"),
 *                 @OA\Property(property="ageRestriction", type="object"),
 *                 @OA\Property(property="label", type="object"),
 *                 @OA\Property(property="address", type="object"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="minPrice", type="integer"),
 *                 @OA\Property(property="isFree", type="boolean"),
 *                 @OA\Property(property="isCableWayRequired", type="boolean"),
 *                 @OA\Property(property="buttonText", type="object"),
 *                 @OA\Property(property="width", type="integer"),
 *                 @OA\Property(property="backgroundUrl", type="string"),
 *                 @OA\Property(property="location", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="isActive", type="boolean"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="sort", type="number"),
 *                          @OA\Property(property="altitude", type="number"),
 *                      )
 *                  ),
 *                 @OA\Property(property="recommendedPlaces", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="title", type="object"),
 *                          @OA\Property(property="description", type="object"),
 *                          @OA\Property(property="subTitle", type="object"),
 *                          @OA\Property(property="subDescription", type="object"),
 *                          @OA\Property(property="workingHours", type="object"),
 *                          @OA\Property(property="ageRestriction", type="object"),
 *                          @OA\Property(property="label", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="labelType", type="array",
 *                                      @OA\Items(
 *                                          @OA\Property(property="id", type="string", format="uuid"),
 *                                          @OA\Property(property="icon", type="object"),
 *                                          @OA\Property(property="svg_icon", type="object"),
 *                                          @OA\Property(property="default_color", type="object"),
 *                                          @OA\Property(property="default_content", type="object"),
 *                                      )
 *                                  ),
 *                                  @OA\Property(property="color", type="string"),
 *                                  @OA\Property(property="content", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="address", type="object"),
 *                          @OA\Property(property="phone", type="string"),
 *                          @OA\Property(property="code", type="string"),
 *                          @OA\Property(property="minPrice", type="number"),
 *                          @OA\Property(property="isFree", type="boolean"),
 *                          @OA\Property(property="isCableWayRequired", type="boolean"),
 *                          @OA\Property(property="buttonText", type="object"),
 *                          @OA\Property(property="width", type="number"),
 *                          @OA\Property(property="backgroundUrl", type="string"),
 *                          @OA\Property(property="location", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="isActive", type="boolean"),
 *                                  @OA\Property(property="name", type="object"),
 *                                  @OA\Property(property="sort", type="number"),
 *                                  @OA\Property(property="altitude", type="number"),
 *                              )
 *                          ),
 *                      )
 *                 ),
 *                 @OA\Property(property="sections", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="code", type="string"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="description", type="object"),
 *                          @OA\Property(property="tariffDescription", type="object"),
 *                          @OA\Property(property="innerDescription", type="object"),
 *                          @OA\Property(property="webIcon", type="string"),
 *                          @OA\Property(property="webBackground", type="string"),
 *                          @OA\Property(property="mobileIcon", type="string"),
 *                          @OA\Property(property="mobileBackground", type="string"),
 *                          @OA\Property(property="picture", type="string"),
 *                          @OA\Property(property="recomendationSort", type="string"),
 *                          @OA\Property(property="settings", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="controls", type="object"),
 *                                  @OA\Property(property="isOnlyOne", type="boolean"),
 *                                  @OA\Property(property="isActivationDateRequired", type="boolean"),
 *                                  @OA\Property(property="activeDays", type="number"),
 *                                  @OA\Property(property="isNotQuantitave", type="boolean"),
 *                              ),
 *                          ),
 *                      ),
 *                 ),
 *                  @OA\Property(property="categories", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="webGradientColor1", type="string"),
 *                          @OA\Property(property="webGradientColor2", type="string"),
 *                          @OA\Property(property="webIcon", type="string"),
 *                          @OA\Property(property="webIconSvg", type="string"),
 *                          @OA\Property(property="mobileIcon", type="string"),
 *                          @OA\Property(property="mobileBackground", type="string"),
 *                      ),
 *                 ),
 *                 @OA\Property(property="blocks", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="title", type="object"),
 *                          @OA\Property(property="body", type="object"),
 *                          @OA\Property(property="photo", type="string"),
 *                          @OA\Property(property="video", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="youtubeCode", type="string"),
 *                                  @OA\Property(property="backgroundImage", type="string"),
 *                              ),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(property="geoPoints", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="latitude", type="number"),
 *                          @OA\Property(property="longitude", type="number"),
 *                          @OA\Property(property="description", type="object"),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(property="photos", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="file", type="string"),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(property="videos", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="youtubeCode", type="string"),
 *                          @OA\Property(property="backgroundImage", type="string"),
 *                          ),
 *                      ),
 *                 ),
 *                 @OA\Property(
 *                      property="width",
 *                      type="integer",
 *                      enum={100, 200, 300, 400},
 *                      description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *                 ),
 *             ),
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractPlaceAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($this->isWorkingNow($request)) {
            $items = $this->placeRepository->fetchAllOpened();
        } else {
            $items = $this->placeRepository->fetchAll();
        }

        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
