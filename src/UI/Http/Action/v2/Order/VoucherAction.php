<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Order;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/order/{id}/voucher",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Заказы"},
 *     security={{"bearerAuth":{}}},
 *     description="Получение ваучера по идентификатору заказа",
 *     @OA\Response(
 *         response="200",
 *         description="Возвращает pdf файл ваучера"
 *     )
 * )
 */
class VoucherAction extends AbstractOrderAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $request = $request->withAddedHeader(
            'Authorization',
            'Bearer ' . $request->getQueryParams()['token'] ?? ''
        );
        $this->authContext->handleRequest($request);

        $this->denyAccessNotAuthenticated();

        $item = $this->orderRepository->get(new Uuid($this->resolveArg('id')));
        $this->ensureAccess($item);
        $filePath = $this->generator->getOrGenerate($item);

        return $this->asFile($filePath, 'voucher.pdf');
    }
}
