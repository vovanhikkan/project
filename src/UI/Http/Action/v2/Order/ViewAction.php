<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Order;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/order/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Заказы"},
 *     description="Получение заказа по идентификатору",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="userId", type="string", format="uuid"),
 *                 @OA\Property(property="amount", type="number"),
 *                 @OA\Property(property="status", type="integer"),
 *                 @OA\Property(
 *                     property="itemsProduct",
 *                     type="array",
 *                     @OA\Items(
 *                         @OA\Property(property="id", type="string", format="uuid"),
 *                         @OA\Property(property="productId", type="string", format="uuid"),
 *                         @OA\Property(property="price", type="number"),
 *                         @OA\Property(property="quantity", type="integer"),
 *                         @OA\Property(property="amount", type="number"),
 *                         @OA\Property(property="activationDate", type="string", format="date")
 *                     )
 *                 ),
 *                  @OA\Property(
 *                      property="itemsRoom",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="hotelName", type="object"),
 *                          @OA\Property(property="roomId", type="string", format="uuid"),
 *                          @OA\Property(property="roomName", type="object"),
 *                          @OA\Property(property="roomPrice", type="number"),
 *                          @OA\Property(property="adultCount", type="number"),
 *                          @OA\Property(property="childCount", type="number"),
 *                          @OA\Property(property="bedTypeId", type="string", format="uuid"),
 *                          @OA\Property(property="bedTypeName", type="object"),
 *                          @OA\Property(property="ratePlanId", type="string", format="uuid"),
 *                          @OA\Property(property="ratePlanName", type="object"),
 *                          @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *                          @OA\Property(property="quantity", type="number"),
 *                          @OA\Property(property="dateFrom", type="string", format="date"),
 *                          @OA\Property(property="dateTo", type="string", format="date"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsGuest",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="roomId", type="string", format="uuid"),
 *                          @OA\Property(property="guestId", type="integer", format="uuid"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="age", type="string"),
 *                          @OA\Property(property="mainGuest", type="boolean"),
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsBundle",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="bundleId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date")
 *                      )
 *                  ),
 *                 @OA\Property(property="createdAt", type="string"),
 *                 @OA\Property(
 *                     property="receiptUrls",
 *                     type="array",
 *                     @OA\Items(type="string")
 *                 ),
 *             )
 *         )
 *     )
 * )
 */
class ViewAction extends AbstractOrderAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $item = $this->orderRepository->get(new Uuid($this->resolveArg('id')));
        $this->ensureAccess($item);
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
