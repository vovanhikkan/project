<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Order;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\Guest\Guest;
use App\Order\Model\Order\Order;
use App\Order\Model\Order\OrderItemBundle;
use App\Order\Model\Order\OrderItemProduct;
use App\Order\Model\Order\OrderItemRoom;
use App\Order\Model\Order\OrderItemRoomGuest;
use App\Order\Model\Payment\Payment;
use App\Order\Repository\OrderRepository;
use App\Order\Service\Voucher\Generator;
use App\Service\Receipt\UrlGetter;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpUnauthorizedException;

/**
 * Class AbstractOrderAction
 * @package App\UI\Http\Action\v2\Order
 */
abstract class AbstractOrderAction extends AbstractAction
{
    private UrlGetter $urlGetter;
    protected OrderRepository $orderRepository;
    protected Generator $generator;

    /**
     * AbstractOrderAction constructor.
     * @param UrlGetter $urlGetter
     * @param OrderRepository $orderRepository
     * @param Generator $generator
     * @param Validator $validator
     * @param CommandBus $bus
     * @param AuthContext $authContext
     * @param LoggerInterface $logger
     * @param Locator $locator
     */
    public function __construct(
        UrlGetter $urlGetter,
        OrderRepository $orderRepository,
        Generator $generator,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->urlGetter = $urlGetter;
        $this->orderRepository = $orderRepository;
        $this->generator = $generator;
    }


    protected function serializeItem(Order $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'userId' => $serializer->asUuid($model->getUser()->getId()),
            'amount' => $model->getAmount()->toRoubles(),
            'status' => $serializer->asInt($model->getStatus()),
            'enumNumber' => $serializer->asIntOrNull($model->getEnumNumber()),
            'itemsProduct' => array_map(
                function (OrderItemProduct $orderItem) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($orderItem->getId()),
                        'productId' => $serializer->asUuid($orderItem->getProduct()->getId()),
                        'price' => $orderItem->getPrice()->toRoubles(),
                        'quantity' => $serializer->asInt($orderItem->getQuantity()),
                        'amount' => $orderItem->getAmount()->toRoubles(),
                        'activationDate' => $serializer->asDateOrNull($orderItem->getActivationDate()),
                        'cardPrice' => $serializer->asIntOrNull($orderItem->getCardPrice()),
                        'isNewCard' => $serializer->asStringOrNull($orderItem->getCardNum()) === null,
                    ];
                },
                $model->getOrderItemsProduct()
            ),
            'itemsRoom' => array_map(
                function (OrderItemRoom $orderItem) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($orderItem->getId()),
                        'hotelName' => $serializer->asArray($orderItem->getRoom()->getHotel()->getName()),
                        'room' => [
                            'id' => (string)$orderItem->getRoom()->getId(),
                            'name' => $serializer->asArray($orderItem->getRoom()->getName()),
                        ],
                        'adultCount' => $orderItem->getAdultCount()->getValue(),
                        'childCount' => $orderItem->getChildCount()->getValue(),
                        'foodType' => [
                            'id' => $serializer->asUuid($orderItem->getFoodType()->getId()),
                            'name' => $serializer->asArray($orderItem->getFoodType()->getName())
                        ],
                        'bedType' => [
                            'id' => $serializer->asUuid($orderItem->getBedType()->getId()),
                            'name' => $serializer->asArray($orderItem->getBedType()->getName())
                        ],
                        'ratePlan' => [
                            'id' => $serializer->asUuid($orderItem->getRatePlan()->getId()),
                            'name' => $serializer->asArray($orderItem->getRatePlan()->getName()),
                        ],
                        'canBeCanceledDate' => $serializer->asDateOrNull($orderItem->getCanBeCanceledDate()),
                        'quantity' => $orderItem->getQuantity()->getValue(),
                        'dateFrom' => $orderItem->getDateFrom()->format('Y-m-d'),
                        'dateTo' => $orderItem->getDateTo()->format('Y-m-d'),
                        'activationDate' => $serializer->asDateOrNull($orderItem->getActivationDate()),
                        'isInvalidActivationDate' => $orderItem->getActivationDate() !== null &&
                            $orderItem->getActivationDate() !== date('Y-m-d'),
                        'price' => $orderItem->getAmount()->getValue()
                    ];
                },
                $model->getOrderItemsRoom()
            ),
            'itemsGuest' => array_values(
                array_map(
                    function (OrderItemRoomGuest $orderItemRoomGuest) use ($serializer) {
                        return [
                            'id' => $serializer->asUuid($orderItemRoomGuest->getId()),
                            'roomId' => $serializer->asUuid($orderItemRoomGuest->getRoom()->getId()),
                            'guestId' => $serializer->asUuid($orderItemRoomGuest->getGuest()->getId()),
                            'firstName' => $serializer->
                            asString($orderItemRoomGuest->getGuest()->getFirstName()),
                            'lastName' => $serializer->
                            asString($orderItemRoomGuest->getGuest()->getLastName()),
                            'age' => $serializer->asInt($orderItemRoomGuest->getGuest()->getAge()),
                            'mainGuest' => $orderItemRoomGuest->getGuest()->isMainGuest()
                        ];
                    },
                    $model->getOrderItemsRoomGuest()
                )
            ),
            'itemsBundle' => array_map(
                function (OrderItemBundle $orderItem) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($orderItem->getId()),
                        'bundleId' => $serializer->asUuid($orderItem->getBundle()->getId()),
                        'quantity' => $serializer->asInt($orderItem->getQuantity()),
                        'activationDate' => $serializer->asDateOrNull($orderItem->getActivationDate())
                    ];
                },
                $model->getOrderItemsBundle()
            ),
            'createdAt' => $serializer->asDateTime($model->getCreatedAt()),
            'receiptUrls' => array_map(
                function (Payment $payment) use ($serializer) {
                    $receipts = $payment->getReceipts();
                    $result = [];
                    foreach ($receipts as $receipt) {
                        $result[] =  $this->urlGetter->getExternalUrl($receipt);
                    }
                    return $result;
                },
                $model->getPayments()
            ),
            'guests' => array_map(
                function (Guest $guest) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($guest->getId())
                    ];
                },
                $model->getGuests()
            )
        ];
    }

    protected function ensureAccess(Order $order): void
    {
        $user = $this->getCurrentUser();

        if ($user->getRole()->isAdmin()) {
            return;
        }

        if ($user->getRole()->isCustomer() && $user === $order->getUser()) {
            return;
        }

        throw new HttpUnauthorizedException($this->request);
    }
}
