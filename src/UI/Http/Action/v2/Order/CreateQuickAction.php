<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Order;

use App\Order\Command\Order\CreateQuick\Command;
use App\Order\Command\Order\CreateQuick\ItemBundleDto;
use App\Order\Command\Order\CreateQuick\ItemProductDto;
use App\Order\Command\Order\CreateQuick\ItemRoomDto;
use App\Order\Command\Order\CreateQuick\ItemRoomGuestDto;
use App\Order\Model\Order\Order;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/order/quick",
 *     tags={"Заказы"},
 *     security={{"bearerAuth":{}}},
 *     description="Быстрое создание заказа",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"email", "lastName", "firstName"},
 *                 @OA\Property(property="email", type="string", format="email"),
 *                 @OA\Property(property="lastName", type="string"),
 *                 @OA\Property(property="firstName", type="string"),
 *                 @OA\Property(property="birthDate", type="string"),
 *                 @OA\Property(property="cardNum", type="string"),
 *                          @OA\Property(
 *                              property="itemsProduct",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="productId", type="string", format="uuid"),
 *                                  @OA\Property(property="quantity", type="number"),
 *                                  @OA\Property(property="activationDate", type="string", format="date")
 *                              )
 *                          ),
 *                          @OA\Property(
 *                              property="itemsRoom",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="roomId", type="string", format="uuid"),
 *                                  @OA\Property(property="bedTypeId", type="string", format="uuid"),
 *                                  @OA\Property(property="ratePlanId", type="string", format="uuid"),
 *                                  @OA\Property(property="foodTypeId", type="string", format="uuid"),
 *                                  @OA\Property(property="dateFrom", type="string", format="date"),
 *                                  @OA\Property(property="dateTo", type="string", format="date"),
 *                                  @OA\Property(property="quantity", type="number"),
 *                                  @OA\Property(property="adultCount", type="number"),
 *                                  @OA\Property(property="childCount", type="number"),
 *                                  @OA\Property(property="activationDate", type="string", format="date")
 *                              )
 *                          ),
 *                          @OA\Property(
 *                              property="itemsGuest",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="roomId", type="string", format="uuid"),
 *                                  @OA\Property(property="firstName", type="string"),
 *                                  @OA\Property(property="lastName", type="string"),
 *                                  @OA\Property(property="age", type="number"),
 *                                  @OA\Property(property="mainGuest", type="boolean"),
 *                                  @OA\Property(property="requirements", type="object")
 *                              )
 *                          ),
 *                          @OA\Property(
 *                              property="itemsBundle",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="bundleProductId", type="string", format="uuid"),
 *                                  @OA\Property(property="quantity", type="number"),
 *                                  @OA\Property(property="activationDate", type="string", format="date")
 *                              )
 *                          ),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="В ответе содержится идентификатор заказа",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                 @OA\Property(property="accessToken", type="string"),
 *                 @OA\Property(property="refreshToken", type="string"),
 *                 @OA\Property(property="id", type="string", format="uuid")
 *             )
 *          )
 *     )
 * )
 */
class CreateQuickAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var array $result */
        $result = $this->bus->handle($command);

        return $this->asJson($result);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $itemsProduct = [];
        $itemsRoom = [];
        $itemsRoomGuest = [];
        $itemsBundle = [];
        foreach ($paramsExtractor->getArray('itemsProduct') as $item) {
            $itemDto = new ItemProductDto(
                $item->getString('productId'),
                $item->getInt('quantity')
            );

            if ($item->has('activationDate')) {
                $itemDto->setActivationDate($item->getString('activationDate'));
            }

            if ($item->has('lastName')) {
                $itemDto->setLastName($item->getString('lastName'));
            }

            if ($item->has('firstName')) {
                $itemDto->setFirstName($item->getString('firstName'));
            }

            if ($item->has('middleName')) {
                $itemDto->setMiddleName($item->getString('middleName'));
            }

            if ($item->has('birthDate')) {
                $itemDto->setBirthDate($item->getString('birthDate'));
            }

            if ($item->has('cardNum')) {
                $itemDto->setCardNum($item->getString('cardNum'));
            }

            $itemsProduct[] = $itemDto;
        }

        foreach ($paramsExtractor->getArray('itemsRoom') as $item) {
            $itemDto = new ItemRoomDto(
                $item->getString('roomId'),
                $item->getString('bedTypeId'),
                $item->getString('ratePlanId'),
                $item->getString('foodTypeId'),
                $item->getInt('quantity'),
                $item->getString('dateFrom'),
                $item->getString('dateTo'),
                $item->getInt('adultCount'),
                $item->getInt('childCount')
            );

            if ($item->has('activationDate')) {
                $itemDto->setActivationDate($item->getString('activationDate'));
            }

            $itemsRoom[] = $itemDto;
        }

        foreach ($paramsExtractor->getArray('itemsGuest') as $item) {
            $itemDto = new ItemRoomGuestDto(
                $item->getString('roomId'),
                $item->getString('firstName'),
                $item->getString('lastName'),
                $item->getInt('age')
            );

            if ($item->has('mainGuest')) {
                $itemDto->setMainGuest($item->getBool('mainGuest'));
            }

            if ($item->has('requirements')) {
                $itemDto->setRequirements($item->getSimpleArray('requirements'));
            }

            $itemsRoomGuest[] = $itemDto;
        }


        foreach ($paramsExtractor->getArray('itemsBundle') as $item) {
            $itemDto = new ItemBundleDto(
                $item->getString('bundleProductId'),
                $item->getInt('quantity')
            );

            if ($item->has('activationDate')) {
                $itemDto->setActivationDate($item->getString('activationDate'));
            }

            $itemsBundle[] = $itemDto;
        }

        return new Command(
            $paramsExtractor->getString('email'),
            $paramsExtractor->getString('lastName'),
            $paramsExtractor->getString('firstName'),
            $itemsProduct,
            $itemsRoom,
            $itemsRoomGuest,
            $itemsBundle
        );
    }
}
