<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Order;

use App\Order\Command\Order\CreateFromCart\Command;
use App\Order\Model\Cart\Cart;
use App\Order\Model\Order\Order;
use App\UI\Http\Action\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/order/from-cart",
 *     tags={"Заказы"},
 *     security={{"bearerAuth":{}}},
 *     description="Создание заказа из текущей корзины",
 *     @OA\Response(
 *         response="201",
 *         description="В ответе содержится идентификатор заказа",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string")
 *              ),
 *          ),
 *     )
 * )
 */
class CreateFromCartAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Order $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        return new Command(
            (string)$this->getCurrentUser()->getId()
        );
    }

    protected function serializeItem(Order $model): array
    {
        return [
            'id' => (string)$model->getId(),
        ];
    }
}
