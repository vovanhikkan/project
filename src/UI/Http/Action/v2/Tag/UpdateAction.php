<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Tag;

use App\Content\Command\Tag\Update\Command;
use App\Content\Model\Tag\Tag;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/tag/{id}",
 *  tags={"Tag"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование Tag",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="icon", type="string"),
 *              @OA\Property(property="iconSvg", type="string"),
 *              @OA\Property(property="colorText", type="string"),
 *              @OA\Property(property="colorBorder", type="string")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid")
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractTagAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Tag $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getSimpleArray('name')
        );

        return $command;
    }
}
