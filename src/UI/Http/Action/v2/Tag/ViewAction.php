<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Tag;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/tag/{id}",
 *  tags={"Tag"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр Tag",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="icon", type="string"),
 *              @OA\Property(property="iconSvg", type="string"),
 *              @OA\Property(property="colorText", type="string"),
 *              @OA\Property(property="colorBorder", type="string")
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractTagAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->tagRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
