<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Tag;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Content\Model\Tag\Tag;
use App\Content\Repository\TagRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractTagAction extends AbstractAction
{
    protected TagRepository $tagRepository;
    
    public function __construct(
        TagRepository $tagRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->tagRepository = $tagRepository;
    }

    protected function serializeList(Tag $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => (string)$model->getId(),
            'name' => $serializer->asArray($model->getName())
        ];
    }


    protected function serializeItem(Tag $model): array
    {
        $serializer = $this->serializer;

        return array_merge($this->serializeList($model), [
            'icon' => $model->getIcon() !== null
                ? $this->locator->getAbsoluteUrl($model->getIcon())
                : null,
            'iconSvg' => $serializer->asTextOrNull($model->getIconSvg()),
        ]);
    }
}
