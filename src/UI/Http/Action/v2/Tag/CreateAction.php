<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Tag;

use App\Content\Command\Tag\Create\Command;
use App\Content\Model\Tag\Tag;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 * 	path="/v2/tag",
 * 	tags={"Tag"},
 * 	security={{"bearerAuth":{}}},
 * 	description="Добавление Tag",
 * 	@OA\RequestBody(
 * 		required=true,
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 *              @OA\Property(property="id", type="string"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="icon", type="string"),
 *              @OA\Property(property="iconSvg", type="string"),
 *              @OA\Property(property="colorText", type="string"),
 *              @OA\Property(property="colorBorder", type="string")
 *          )
 * 		)
 * 	),
 * 	@OA\Response(
 * 		response="201",
 * 	    description="",
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 *              @OA\Property(property="name", type="object")
 *          )
 * 		)
 * 	)
 * )
 */
class CreateAction extends AbstractTagAction
{
	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$this->denyAccessNotAdministrator();

		$command = $this->deserialize($request);
		$this->validator->validate($command);
		/** @var Tag $result */
		$result = $this->bus->handle($command);
		$data = $this->serializeItem($result);

		return $this->asJson($data, 201);
	}


	private function deserialize(ServerRequestInterface $request): Command
	{
		$paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

		$command = new Command(
            $paramsExtractor->getSimpleArray('name')
        );

		return $command;
	}
}
