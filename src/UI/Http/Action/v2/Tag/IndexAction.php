<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Tag;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/tag",
 *  tags={"Tag"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Tag",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="icon", type="string"),
 *              @OA\Property(property="iconSvg", type="string"),
 *              @OA\Property(property="colorText", type="string"),
 *              @OA\Property(property="colorBorder", type="string")
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractTagAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->tagRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
