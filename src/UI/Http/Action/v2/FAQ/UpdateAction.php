<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FAQ;

use App\FAQ\Command\FAQ\Update\Command;
use App\FAQ\Model\FAQ\FAQ;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/faq/{id}",
 *  tags={"FAQ"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование FAQ",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="title", type="object"),
 *              @OA\Property(property="body", type="object"),
 *              @OA\Property(property="sort", type="number"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="title", type="object"),
 *              @OA\Property(property="body", type="object"),
 *              @OA\Property(property="sort", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractFAQAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var FAQ $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );

        if ($paramsExtractor->has('title')) {
            $command->setTitle($paramsExtractor->getSimpleArray('title'));
        }

        if ($paramsExtractor->has('body')) {
            $command->setBody($paramsExtractor->getSimpleArray('body'));
        }

        if ($paramsExtractor->has('sort')) {
            $command->setSort($paramsExtractor->getInt('sort'));
        }

        return $command;
    }
}
