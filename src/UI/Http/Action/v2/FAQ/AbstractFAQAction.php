<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FAQ;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\FAQ\Model\FAQ\FAQ;
use App\FAQ\Repository\FAQRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractFAQAction
 * @package App\UI\Http\Action\v2\FAQ
 */
abstract class AbstractFAQAction extends AbstractAction
{
    protected FAQRepository $fAQRepository;

    public function __construct(
        FAQRepository $fAQRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->fAQRepository = $fAQRepository;
    }

    protected function serializeList(FAQ $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(FAQ $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'title' => $serializer->asArray($model->getTitle()),
            'body' => $serializer->asArray($model->getBody()),
            'sort' => $serializer->asInt($model->getSort())
        ];
    }
}
