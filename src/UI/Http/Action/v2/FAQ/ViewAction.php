<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FAQ;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/faq/{id}",
 *  tags={"FAQ"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр FAQ",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid")
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractFAQAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->fAQRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
