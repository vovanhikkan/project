<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FAQ;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/faq",
 *  tags={"FAQ"},
 *  security={{"bearerAuth":{}}},
 *  description="Список FAQ",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="title", type="object"),
 *              @OA\Property(property="body", type="object"),
 *              @OA\Property(property="sort", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractFAQAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->fAQRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
