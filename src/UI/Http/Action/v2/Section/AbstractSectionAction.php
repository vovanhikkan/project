<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Section;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Content\Model\Category\Category;
use App\Product\Model\Section\Section;
use App\Product\Repository\SectionRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractSectionAction.
 */
abstract class AbstractSectionAction extends AbstractAction
{
    protected SectionRepository $sectionRepository;

    public function __construct(
        SectionRepository $sectionRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->sectionRepository = $sectionRepository;
    }

    protected function serializeList(Section $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => (string)$model->getId(),
            'code' => (string)$model->getCode(),
            'name' => $serializer->asArray($model->getName()),
        ];
    }

    protected function serializeItem(Section $model): array
    {
        $serializer = $this->serializer;

        return array_merge($this->serializeList($model), [
            'description' => $serializer->asArray($model->getDescription()),
            'tariffDescription' => $serializer->asArray($model->getTariffDescription()),
            'innerTitle' => $serializer->asArray($model->getInnerTitle()),
            'innerDescription' => $serializer->asArray($model->getInnerDescription()),
            'webIcon' => $model->getWebIcon() !== null
                ? $this->locator->getAbsoluteUrl($model->getWebIcon())
                : null,
            'webBackground' => $model->getWebBackground() !== null
                ? $this->locator->getAbsoluteUrl($model->getWebBackground())
                : null,
            'mobileIcon' => $model->getMobileIcon() !== null
                ? $this->locator->getAbsoluteUrl($model->getMobileIcon())
                : null,
            'mobileBackground' => $model->getMobileBackground() !== null
                ? $this->locator->getAbsoluteUrl($model->getMobileBackground())
                : null,
            'picture' => $model->getPicture() !== null
                ? $this->locator->getAbsoluteUrl($model->getPicture())
                : null,
            'recomendationSort' => $serializer->asInt($model->getRecommendationSort()),
            'settings' => $model->getSectionSettings() !== null
                ? [
                    'controls' => $this->serializer->asArray($model->getSectionSettings()->getControls()),
                    'isOnlineOnly' => $model->getSectionSettings()->isOnlineOnly(),
                    'isActivationDateRequired' => $model->getSectionSettings()->isActivationDateRequired(),
                    'activeDays' => $this->serializer->asInt($model->getSectionSettings()->getActiveDays()),
                    'isNotQuantitative' => $model->getSectionSettings()->isNotQuantitative(),
                ] : null,
            'categories' => array_map(
                function (Category $category) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($category->getId()),
                        'name' => $serializer->asArray($category->getName()),
                        'webGradientColor1' => $serializer->asStringOrNull($category->getWebGradientColor1()),
                        'webGradientColor2' => $serializer->asStringOrNull($category->getWebGradientColor2()),
                        'webIcon' => $category->getWebIcon() !== null
                            ? $this->locator->getAbsoluteUrl($category->getWebIcon())
                            : null,
                        'webIconSvg' => $category->getWebIconSvg() !== null
                            ? $this->locator->getAbsoluteUrl($category->getWebIconSvg())
                            : null,
                        'mobileIcon' => $category->getMobileIcon() !== null
                            ? $this->locator->getAbsoluteUrl($category->getMobileIcon())
                            : null,
                        'mobileBackground' => $category->getMobileBackground() !== null
                            ? $this->locator->getAbsoluteUrl($category->getMobileBackground())
                            : null,
                    ];
                },
                $model->getCategories()
            ),
        ]);
    }
}
