<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Section;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/section",
 *     tags={"Секции"},
 *     description="Список секций",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="name", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="tariffDescription", type="object"),
 *                 @OA\Property(property="innerTitle", type="object"),
 *                 @OA\Property(property="innerDescription", type="object"),
 *                 @OA\Property(property="picture", type="string"),
 *                 @OA\Property(property="webIcon", type="string"),
 *                 @OA\Property(property="webIconSvg", type="string"),
 *                 @OA\Property(property="mobileIcon", type="string"),
 *                 @OA\Property(property="mobileBackground", type="string"),
 *                 @OA\Property(property="recomendationSort", type="string"),
 *                 @OA\Property(property="settings",
 *                     @OA\Items(
 *                         @OA\Property(property="controls", type="object"),
 *                         @OA\Property(property="isOnlyOne", type="boolean"),
 *                         @OA\Property(property="isActivationDateRequired", type="boolean"),
 *                         @OA\Property(property="activeDays", type="number"),
 *                         @OA\Property(property="isNotQuantitave", type="boolean")
 *                     )
 *                 ),
 *                 @OA\Property(property="categories", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="webGradientColor1", type="string"),
 *                          @OA\Property(property="webGradientColor2", type="string"),
 *                          @OA\Property(property="webIcon", type="string"),
 *                          @OA\Property(property="webIconSvg", type="string"),
 *                          @OA\Property(property="mobileIcon", type="string"),
 *                          @OA\Property(property="mobileBackground", type="string"),
 *                     ),
 *                ),
 *             ),
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractSectionAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->sectionRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
