<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Section;

use App\Product\Command\Section\Update\SettingsDto;
use App\Product\Command\Section\Update\Command;
use App\Product\Model\Section\Section;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Patch(
 *     path="/v2/section/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Секции"},
 *     security={{"bearerAuth":{}}},
 *     description="Редактирование секции",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"code", "name", "description", "tariffDescription", "innerTitle", "innerDescription"},
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="name", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="tariffDescription", type="object"),
 *                 @OA\Property(property="innerTitle", type="object"),
 *                 @OA\Property(property="innerDescription", type="object"),
 *                 @OA\Property(property="settings",
 *                     @OA\Items(
 *                         @OA\Property(property="controls", type="object"),
 *                         @OA\Property(property="isOnlyOne", type="boolean"),
 *                         @OA\Property(property="isActivationDateRequired", type="boolean"),
 *                         @OA\Property(property="activeDays", type="number"),
 *                         @OA\Property(property="isNotQuantitave", type="boolean")
 *                     )
 *                 )
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="code", type="string"),
 *                 @OA\Property(property="name", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="tariffDescription", type="object"),
 *                 @OA\Property(property="innerTitle", type="object"),
 *                 @OA\Property(property="innerDescription", type="object"),
 *                 @OA\Property(property="picture", type="string"),
 *                 @OA\Property(property="webIcon", type="string"),
 *                 @OA\Property(property="webIconSvg", type="string"),
 *                 @OA\Property(property="mobileIcon", type="string"),
 *                 @OA\Property(property="mobileBackground", type="string"),
 *                 @OA\Property(property="recomendationSort", type="string"),
 *                 @OA\Property(property="settings",
 *                     @OA\Items(
 *                         @OA\Property(property="controls", type="object"),
 *                         @OA\Property(property="isOnlyOne", type="boolean"),
 *                         @OA\Property(property="isActivationDateRequired", type="boolean"),
 *                         @OA\Property(property="activeDays", type="number"),
 *                         @OA\Property(property="isNotQuantitave", type="boolean")
 *                     )
 *                 ),
 *                 @OA\Property(property="categories", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="name", type="object"),
 *                          @OA\Property(property="webGradientColor1", type="string"),
 *                          @OA\Property(property="webGradientColor2", type="string"),
 *                          @OA\Property(property="webIcon", type="string"),
 *                          @OA\Property(property="webIconSvg", type="string"),
 *                          @OA\Property(property="mobileIcon", type="string"),
 *                          @OA\Property(property="mobileBackground", type="string"),
 *                     ),
 *                ),
 *             ),
 *         )
 *     )
 * )
 */
class UpdateAction extends AbstractSectionAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Section $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $settings = $paramsExtractor->getSimpleArray('settings');

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getString('code'),
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getSimpleArray('description'),
            $paramsExtractor->getSimpleArray('tariffDescription'),
            $paramsExtractor->getSimpleArray('innerTitle'),
            $paramsExtractor->getSimpleArray('innerDescription'),
            new SettingsDto(
                $settings['controls'],
                $settings['isOnlyOne'],
                $settings['isActivationDateRequired'],
                $settings['activeDays'],
                $settings['isNotQuantitative'],
            ),
            $paramsExtractor->getStringOrNull('picture'),
            $paramsExtractor->getStringOrNull('webIcon'),
            $paramsExtractor->getStringOrNull('webBackground'),
            $paramsExtractor->getStringOrNull('mobileIcon'),
            $paramsExtractor->getStringOrNull('mobileBackground')
        );
    }
}
