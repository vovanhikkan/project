<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\TravelLine;

use App\TravelLine\Dto\TravelLineRequestDto;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class IndexAction extends AbstractTravelLineAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $dto = $this->deserialize($request);
        return $this->asJson($this->travelLine->processRequest($dto));
    }

    public function deserialize(ServerRequestInterface $request)
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $user = $paramsExtractor->getString('username');
        $password = $paramsExtractor->getString('password');
        $action = $paramsExtractor->getString('action');
        $data = $paramsExtractor->getSimpleArray('data');

        $dto = new TravelLineRequestDto(
            $user,
            $password,
            $action
        );

        if ($data) {
            $dto->setData($data);
        }

        return $dto;
    }
}
