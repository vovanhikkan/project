<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\TravelLine;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Product\Model\Section\Section;
use App\Storage\Service\File\Locator;
use App\TravelLine\Service\TravelLine;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractSectionAction.
 */
abstract class AbstractTravelLineAction extends AbstractAction
{
    protected TravelLine $travelLine;

    public function __construct(
        TravelLine $travelLine,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->travelLine = $travelLine;
    }

    protected function serializeList(Section $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => (string)$model->getId(),
            'code' => (string)$model->getCode(),
            'name' => $serializer->asArray($model->getName()),
        ];
    }

    protected function serializeItem(Section $model): array
    {
    }
}
