<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ActiveAction.
 *
 * @OA\Get(
 *     path="/v2/cart/active",
 *     tags={"Корзина"},
 *     security={{"bearerAuth":{}}},
 *     description="Получение текущей активной корзины пользователя",
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится полный объект текущей корзины",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string"),
 *                  @OA\Property(
 *                      property="itemsProduct",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="middleName", type="string"),
 *                          @OA\Property(property="birthDate", type="string", format="date"),
 *                          @OA\Property(property="cardNum", type="string"),
 *                          @OA\Property(property="isInvalidSection", type="boolean"),
 *                          @OA\Property(property="isInvalidProduct", type="boolean"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                          @OA\Property(property="isNewCard", type="boolean"),
 *                          @OA\Property(
 *                              property="price",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="calculatedPriceValue", type="number"),
 *                                  @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                                  @OA\Property(property="discountAbsoluteValue", type="number"),
 *                                  @OA\Property(property="discountPercentageValue", type="number"),
 *                                  @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                                  @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                                  @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                                  @OA\Property(property="productId", type="string", format="uuid"),
 *                              )
 *                          )
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsRoom",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="hotel", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="room", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                                  @OA\Property(property="price", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="adultCount", type="number"),
 *                          @OA\Property(property="childCount", type="number"),
 *                          @OA\Property(property="bedTypeId", type="string", format="uuid"),
 *                          @OA\Property(property="bedTypeName", type="object"),
 *                          @OA\Property(property="ratePlanId", type="string", format="uuid"),
 *                          @OA\Property(property="ratePlanName", type="object"),
 *                          @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *                          @OA\Property(property="quantity", type="number"),
 *                          @OA\Property(property="dateFrom", type="string", format="date"),
 *                          @OA\Property(property="dateTo", type="string", format="date"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsGuest",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="roomId", type="string", format="uuid"),
 *                          @OA\Property(property="guestId", type="string", format="uuid"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="age", type="string"),
 *                          @OA\Property(property="mainGuest", type="boolean"),
 *                          @OA\Property(property="requirements", type="object"),
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsBundle",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="bundleId", type="string", format="uuid"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="dateFrom", type="string", format="date"),
 *                          @OA\Property(property="dateTo", type="string", format="date"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                          @OA\Property(property="price", type="number"),
 *                          @OA\Property(property="items", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                                  @OA\Property(property="description", type="object"),
 *                                  @OA\Property(property="photo", type="object"),
 *                                  @OA\Property(property="place", type="array",
 *                                      @OA\Items(
 *                                          @OA\Property(property="web_icon", type="object"),
 *                                          @OA\Property(property="mob_icon", type="object"),
 *                                          @OA\Property(property="web_icon_svg", type="object"),
 *                                          @OA\Property(property="web_gradient_color1", type="string"),
 *                                          @OA\Property(property="web_gradient_color2", type="string"),
 *                                      ),
 *                                  ),
 *                                  @OA\Property(property="placeId", type="string", format="uuid"),
 *                                  @OA\Property(property="widthType", type="number", format="uuid")
 *                              ),
 *                          ),
 *                      )
 *                  ),
 *              ),
 *          ),
 *     )
 * )
 */
class ActiveAction extends AbstractCartAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $item = $this->cartRepository->getActiveByUser($this->getCurrentUser());
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
