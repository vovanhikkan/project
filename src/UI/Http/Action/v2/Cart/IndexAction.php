<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/cart",
 *     tags={"Корзина"},
 *     description="Получение списка корзин всех пользователей",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Items(
 *                     @OA\Property(property="id", type="string", format="uuid"),
 *                     @OA\Property(
 *                         property="items_product",
 *                         type="array",
 *                         @OA\Items(
 *                             @OA\Property(property="id", type="string", format="uuid"),
 *                             @OA\Property(property="productId", type="string"),
 *                             @OA\Property(property="quantity", type="integer"),
 *                             @OA\Property(property="activationDate", type="string", format="date"),
 *                             @OA\Property(property="lastName", type="string"),
 *                             @OA\Property(property="firstName", type="string"),
 *                             @OA\Property(property="middleName", type="string"),
 *                             @OA\Property(property="birthDate", type="string", format="date"),
 *                             @OA\Property(property="cardNum", type="string"),
 *                             @OA\Property(property="isInvalidSection", type="boolean"),
 *                             @OA\Property(property="isInvalidProduct", type="boolean"),
 *                             @OA\Property(property="isInvalidActivationDate ", type="boolean"),
 *                             @OA\Property(property="isNewCard", type="boolean"),
 *                             @OA\Property(
 *                                 property="price",
 *                                 type="array",
 *                                 @OA\Items(
 *                                     @OA\Property(property="calculatedPriceValue", type="number"),
 *                                     @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                                     @OA\Property(property="discountAbsoluteValue", type="number"),
 *                                     @OA\Property(property="discountPercentageValue", type="number"),
 *                                     @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                                     @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                                     @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                                     @OA\Property(property="productId", type="string", format="uuid"),
 *                                 )
 *                             )
 *                           )
 *                        ),
 *                         @OA\Property(
 *                         property="items_room",
 *                         type="array",
 *                         @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="roomId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date")
 *                         )
 *                       ),
 *                         @OA\Property(
 *                         property="items_bundle",
 *                         type="array",
 *                         @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="bundleId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date")
 *                         )
 *                       )
 *                    )
 *                 )
 *             )
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractCartAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $items = $this->cartRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
