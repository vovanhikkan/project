<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use App\Application\ValueObject\Uuid;
use App\Bundle\Dto\SearchDto;
use App\Order\Command\Cart\AddItemBundleRoom\Command;
use App\Order\Command\Cart\AddItemBundleRoom\CartItemBundleRoomDto;
use App\Order\Model\Cart\Cart;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use DateTimeImmutable;

/**
 * AddItemBundleAction.
 *
 * @OA\Post(
 *     path="/v2/cart/cart-item/bundle",
 *     tags={"Корзина"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление продуктов в корзину",
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится полный объект текущей корзины",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(
 *                      property="itemsProduct",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="middleName", type="string"),
 *                          @OA\Property(property="birthDate", type="string", format="date"),
 *                          @OA\Property(property="cardNum", type="string"),
 *                          @OA\Property(property="isNewCard", type="boolean"),
 *                          @OA\Property(
 *                              property="price",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="calculatedPriceValue", type="number"),
 *                                  @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                                  @OA\Property(property="discountAbsoluteValue", type="number"),
 *                                  @OA\Property(property="discountPercentageValue", type="number"),
 *                                  @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                                  @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                                  @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                                  @OA\Property(property="productId", type="string", format="uuid"),
 *                              )
 *                          )
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsRoom",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="hotel", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="room", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                                  @OA\Property(property="price", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="adultCount", type="number"),
 *                          @OA\Property(property="childCount", type="number"),
 *                          @OA\Property(property="foodType", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="bedType", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="ratePlan", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="dateFrom", type="string", format="date"),
 *                          @OA\Property(property="dateTo", type="string", format="date"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean", format="date")
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsGuest",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="roomId", type="string", format="uuid"),
 *                          @OA\Property(property="guestId", type="string", format="uuid"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="age", type="string"),
 *                          @OA\Property(property="mainGuest", type="boolean"),
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsBundle",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="bundleId", type="string", format="uuid"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="dateFrom", type="string", format="date"),
 *                          @OA\Property(property="dateTo", type="string", format="date"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                          @OA\Property(property="price", type="number"),
 *                          @OA\Property(property="items", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                                  @OA\Property(property="description", type="object"),
 *                                  @OA\Property(property="photo", type="object"),
 *                                  @OA\Property(property="place", type="array",
 *                                      @OA\Items(
 *                                          @OA\Property(property="web_icon", type="object"),
 *                                          @OA\Property(property="mob_icon", type="object"),
 *                                          @OA\Property(property="web_icon_svg", type="object"),
 *                                          @OA\Property(property="web_gradient_color1", type="string"),
 *                                          @OA\Property(property="web_gradient_color2", type="string"),
 *                                      ),
 *                                  ),
 *                                  @OA\Property(property="placeId", type="string", format="uuid"),
 *                                  @OA\Property(property="widthType", type="number", format="uuid")
 *                              ),
 *                          ),
 *                      )
 *                  ),
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="items", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="bundleId", type="string", format="uuid"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="adultCount", type="number"),
 *                          @OA\Property(property="childCount", type="number"),
 *                          @OA\Property(property="childAges", type="array",
 *                              @OA\Items( type="integer")
 *                          ),
 *                          @OA\Property(property="quantity", type="number"),
 *                          @OA\Property(property="roomId", type="string", format="uuid"),
 *                          @OA\Property(property="ratePlanId", type="string", format="uuid"),
 *                          @OA\Property(property="dateFrom", type="string", format="date"),
 *                          @OA\Property(property="dateTo", type="string", format="date"),
 *
 *                      )
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class AddItemBundleAction extends AbstractCartAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Cart $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);


        $cartItems = [];
        foreach ($paramsExtractor->getArray('items') as $cartItem) {
            $searchDto = new SearchDto($cartItem->getString('bundleId'));

            if ($cartItem->has('activationDate')) {
                $searchDto->setDate($cartItem->getString('activationDate'));
            }
            $adultCount = 0;
            if ($cartItem->has('adultCount')) {
                $adultCount = $cartItem->getInt('adultCount');
                $searchDto->setAdultCount($adultCount);
            }
            $childCount = 0;
            if ($cartItem->has('childCount')) {
                $childCount = $cartItem->getInt('childCount');
                $searchDto->setChildCount($cartItem->getInt('childCount'));
            }
            if ($cartItem->has('childAges')) {
                $searchDto->setChildAges($cartItem->getSimpleArray('childAges'));
            }
            $quantity = 1;
            if ($cartItem->has('quantity')) {
                $quantity = $cartItem->getInt('quantity');
            }

            $date = $cartItem->getStringOrNull('activationDate');

            $this->validator->validate($searchDto);

            $model = $this->bundleProductRepository->searchBundleProduct($searchDto);

            $result = $this->filter->do(
                $model,
                $date !== null ? new DateTimeImmutable($date) : null
            );

            if (!empty($result)) {
                if (isset($result['bundleProductId'])) {
                    $cartItemDto = new CartItemBundleRoomDto(
                        $result['bundleProductId'],
                        $quantity,
                        $date,
                        $adultCount,
                        $childCount
                    );

                    if ($cartItem->has('roomId')) {
                        $cartItemDto->setRoomId($cartItem->getString('roomId'));
                    }

                    if ($cartItem->has('ratePlanId')) {
                        $ratePlanId = $cartItem->getString('ratePlanId');
                        $cartItemDto->setRatePlanId($ratePlanId);

                        $ratePlan = $this->ratePlanRepository->get(new Uuid($ratePlanId));
                        $cartItemDto->setFoodTypeId($ratePlan->getFoodType()->getId()->getValue());
                    }

                    $cartItemDto->setBedTypeId('33be2c7d-3a56-45c9-9540-57763103f912');

                    if ($cartItem->has('dateFrom')) {
                        $cartItemDto->setDateFrom($cartItem->getString('dateFrom'));
                    }

                    if ($cartItem->has('dateTo')) {
                        $cartItemDto->setDateTo($cartItem->getString('dateTo'));
                    }

                    $cartItemDto->setQuantity(1);

                    $cartItems[] = $cartItemDto;
                }
            }
        }

        return new Command(
            $cartItems,
            (string)$this->getCurrentUser()->getId()
        );
    }
}
