<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use App\Application\Service\Validator;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\AuthContext;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleItem\BundleItem;
use App\Bundle\Repository\BundleProductRepository;
use App\Bundle\Service\Bundle\Filter;
use App\Content\Model\Category\Category;
use App\Hotel\Repository\RatePlanRepository;
use App\Order\Command\Cart\ValidateBundle\CartItemBundleDto;
use App\Order\Command\Cart\ValidateRoom\CartItemRoomDto;
use App\Order\Command\Cart\ValidateProduct\CartItemProductDto;
use App\Order\Model\Cart\Cart;
use App\Order\Model\Cart\CartItemBundle;
use App\Order\Model\Cart\CartItemProduct;
use App\Order\Model\Cart\CartItemRoom;
use App\Order\Model\Cart\CartItemRoomGuest;
use App\Order\Repository\CartRepository;
use App\Order\Service\Cart\RoomPriceCalculator;
use App\Product\Model\CardType\Type;
use App\Product\Repository\CardTypeRepository;
use App\Service\Product\PriceCalculator;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;
use DateTimeImmutable;

/**
 * Class AbstractCartAction
 * @package App\UI\Http\Action\v2\Cart
 */
abstract class AbstractCartAction extends AbstractAction
{
    private PriceCalculator $priceCalculator;
    private CardTypeRepository $cardTypeRepository;
    protected CartRepository $cartRepository;
    private RoomPriceCalculator $roomPriceCalculator;
    protected BundleProductRepository $bundleProductRepository;
    protected RatePlanRepository $ratePlanRepository;
    protected Filter $filter;

    public function __construct(
        PriceCalculator $priceCalculator,
        CardTypeRepository $cardTypeRepository,
        CartRepository $cartRepository,
        BundleProductRepository $bundleProductRepository,
        RatePlanRepository $ratePlanRepository,
        Filter $filter,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator,
        RoomPriceCalculator $roomPriceCalculator
    ) {
        parent::__construct(
            $validator,
            $bus,
            $authContext,
            $logger,
            $locator
        );
        $this->priceCalculator = $priceCalculator;
        $this->cardTypeRepository = $cardTypeRepository;
        $this->cartRepository = $cartRepository;
        $this->bundleProductRepository = $bundleProductRepository;
        $this->ratePlanRepository = $ratePlanRepository;
        $this->filter = $filter;
        $this->roomPriceCalculator = $roomPriceCalculator;
    }

    protected function serializeItem(Cart $model): array
    {
        $serializer = $this->serializer;

        $result = [
            'id' => (string)$model->getId(),
            'itemsProduct' => array_values(
                array_map(
                    function (CartItemProduct $cartItem) use ($serializer) {

                        $cardType = $this->cardTypeRepository->fetchOneByType(
                            new Type($cartItem->getProduct()->getProductSettings()->getCardType()->getValue())
                        );

                        return [
                            'id' => (string)$cartItem->getId(),
                            'productId' => (string)$cartItem->getProduct()->getId(),
                            'quantity' => $cartItem->getQuantity()->getValue(),
                            'activationDate' => $serializer->asDateOrNull($cartItem->getActivationDate()),
                            'lastName' => $serializer->asStringOrNull($cartItem->getLastName()),
                            'firstName' => $serializer->asStringOrNull($cartItem->getFirstName()),
                            'middleName' => $serializer->asStringOrNull($cartItem->getMiddleName()),
                            'birthDate' => $serializer->asDateOrNull($cartItem->getBirthDate()),
                            'cardNum' => $serializer->asStringOrNull($cartItem->getCardNum()),
                            'isNewCard' => $cartItem->getIsNewCard(),
                            'cardPrice' => $serializer->asIntOrNull($cardType->getPrice()),
                            'isInvalidSection' => !$cartItem->getProduct()->getSection()->isActive(),
                            'isInvalidProduct' => !$cartItem->getProduct()->isActive(),
                            'isInvalidActivationDate' => $cartItem->getActivationDate() !== null &&
                                $cartItem->getActivationDate() !== date('Y-m-d'),
                            'price' => $this->priceCalculator->getPrice(
                                $this->getCurrentUser(),
                                $cartItem->getProduct(),
                                $serializer->asDateOrNull($cartItem->getActivationDate())
                            )
                        ];
                    },
                    $model->getCartItemsProduct()
                )
            ),
            'itemsRoom' => array_values(
                array_map(
                    function (CartItemRoom $cartItem) use ($serializer) {
                        $roomPrice = $this->roomPriceCalculator->getMinPrice(
                            $cartItem->getRoom(),
                            $cartItem->getRatePlan(),
                            $cartItem->getDateFrom(),
                            $cartItem->getDateTo()
                        );

                        return [
                            'id' => $serializer->asUuid($cartItem->getId()),
                            'hotel' => [
                                'id' => $serializer->asUuid($cartItem->getRoom()->getHotel()->getId()),
                                'name' => $serializer->asArray($cartItem->getRoom()->getHotel()->getName()),
                            ],
                            'hotelName' => $serializer->asArray($cartItem->getRoom()->getHotel()->getName()),
                            'room' => [
                                'id' => $serializer->asUuid($cartItem->getRoom()->getId()),
                                'name' => $serializer->asArray($cartItem->getRoom()->getName()),
                                'price' => $roomPrice->getValue(),
                            ],
                            'adultCount' => $cartItem->getAdultCount()->getValue(),
                            'childCount' => $cartItem->getChildCount()->getValue(),
                            'foodType' => [
                                'id' => $serializer->asUuid($cartItem->getFoodType()->getId()),
                                'name' => $serializer->asArray($cartItem->getFoodType()->getName())
                            ],
                            'bedType' => [
                                'id' => $serializer->asUuid($cartItem->getBedType()->getId()),
                                'name' => $serializer->asArray($cartItem->getBedType()->getName())
                            ],
                            'ratePlan' => [
                                'id' => $serializer->asUuid($cartItem->getRatePlan()->getId()),
                                'name' => $serializer->asArray($cartItem->getRatePlan()->getName()),
                            ],
                            'canBeCanceledDate' =>  $serializer->asDateOrNull($cartItem->getCanBeCanceledDate()),
                            'quantity' => $cartItem->getQuantity()->getValue(),
                            'dateFrom' => $cartItem->getDateFrom()->format('Y-m-d'),
                            'dateTo' => $cartItem->getDateTo()->format('Y-m-d'),
                            'activationDate' => $serializer->asDateOrNull($cartItem->getActivationDate()),
                            'isInvalidActivationDate' => $cartItem->getActivationDate() !== null &&
                                $cartItem->getActivationDate() !== date('Y-m-d'),
                            'price' => $roomPrice->getValue(),
                        ];
                    },
                    $model->getCartItemsRoom()
                )
            ),
            'itemsGuest' => array_values(
                array_map(
                    function (CartItemRoomGuest $cartItemRoomGuest) use ($serializer) {
                        return [
                            'id' => $serializer->asUuid($cartItemRoomGuest->getId()),
                            'roomId' => $serializer->asUuid($cartItemRoomGuest->getRoom()->getId()),
                            'guestId' => $serializer->asUuid($cartItemRoomGuest->getGuest()->getId()),
                            'firstName' => $serializer->
                            asString($cartItemRoomGuest->getGuest()->getFirstName()),
                            'lastName' => $serializer->
                            asString($cartItemRoomGuest->getGuest()->getLastName()),
                            'age' => $serializer->asInt($cartItemRoomGuest->getGuest()->getAge()),
                            'mainGuest' => $cartItemRoomGuest->getGuest()->isMainGuest(),
                            'requirements' => $serializer->asArray($cartItemRoomGuest->getRequirements())
                        ];
                    },
                    $model->getCartItemsRoomGuest()
                )
            ),

        ];



        $result['itemsBundle'] = array_values(
            array_map(
                function (CartItemBundle $cartItem) use ($serializer) {
                    $date = $cartItem->getActivationDate()->format('Y-m-d');
                    $searchResult = $this->filter->do(
                        $cartItem->getBundleProduct(),
                        $date !== null ? new DateTimeImmutable($date) : null
                    );
                    return [
                        'id' => $serializer->asUuid($cartItem->getId()),
                        'bundleId' => $serializer->asUuid($cartItem->getBundleProduct()->getBundle()->getId()),
                        'quantity' => $cartItem->getQuantity()->getValue(),
                        'dateFrom' => $serializer->asDateOrNull($cartItem->getActivationDate()),
                        'dateTo' => $serializer->asDateOrNull($cartItem->getActivationDate()->modify('+' . $cartItem->getBundleProduct()->getActivationDateShift() . 'days')),
                        'isInvalidActivationDate' => $cartItem->getActivationDate() !== null &&
                            $cartItem->getActivationDate() !== date('Y-m-d'),
                        'price' => $searchResult['price'],
                        'items' => array_values(
                            array_map(
                                function (BundleItem $bundleItem) use ($serializer) {
                                    $photo = $this->getFileData($bundleItem->getPhoto());
                                    $place = $bundleItem->getPlace();
                                    return [
                                        'id' => $serializer->asUuid($bundleItem->getId()),
                                        'name' => $serializer->asArray($bundleItem->getName()),
                                        'subName' => $serializer->asArray($bundleItem->getSubName()),
                                        'description' => $serializer->asArray($bundleItem->getDescription()),
                                        'photo' => $photo,
                                        'place' => $place !== null ? array_map(
                                            function (Category $category) use ($serializer) {
                                                return [
                                                    'web_icon' =>  $this->getFileData($category->getWebIcon()),
                                                    'mob_icon' =>  $this->getFileData($category->getMobileIcon()),
                                                    'web_icon_svg' => $this->getFileData($category->getWebIconSvg()),
                                                    'web_gradient_color1' => $serializer->asStringOrNull(
                                                        $category->getWebGradientColor1()
                                                    ),
                                                    'web_gradient_color2' => $serializer->asStringOrNull(
                                                        $category->getWebGradientColor2()
                                                    )
                                                ];
                                            },
                                            $place->getCategories()
                                        ) : [],
                                        'placeId' => $place !== null ? $serializer->asUuid($place->getId()) : null,
                                        'widthType' => $serializer->asIntOrNull($bundleItem->getWidthType())
                                    ];
                                },
                                $cartItem->getBundleProduct()->getBundle()->getBundleItems(),
                            )
                        )
                    ];
                },
                $model->getCartItemsBundle()
            )
        );

        return $result;
    }

    protected function serializeCartItemProduct(CartItemProductDto $model): array
    {
        return [
            'productId' => $model->getProduct(),
            'quantity' => $model->getQuantity(),
            'activationDate' => $model->getActivationDate(),
            'isInvalidSection' => $model->getIsInvalidSection(),
            'isInvalidProduct' => $model->getIsInvalidProduct(),
            'isInvalidActivationDate' => $model->getIsInvalidActivationDate(),
            'price' => $this->priceCalculator->getPrice(
                $this->getCurrentUser(),
                $model->getProductObject(),
                $model->getActivationDate()
            )
        ];
    }

    protected function serializeCartItemRoom(CartItemRoomDto $model): array
    {
        return [
            'roomId' => $model->getRoom(),
            'bedTypeId' => $model->getBedType(),
            'quantity' => $model->getQuantity(),
            'activationDate' => $model->getActivationDate(),
            'isInvalidActivationDate' => $model->getIsInvalidActivationDate(),
        ];
    }

    protected function serializeCartItemBundle(CartItemBundleDto $model): array
    {
        return [
            'bundleId' => $model->getBundle(),
            'quantity' => $model->getQuantity(),
            'activationDate' => $model->getActivationDate(),
            'isInvalidActivationDate' => $model->getIsInvalidActivationDate(),
        ];
    }
}
