<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use App\Order\Command\Cart\UpdateItemProduct\Command;
use App\Order\Model\Cart\Cart;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *     path="/v2/cart/cart-item/{id}/product",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Корзина"},
 *     security={{"bearerAuth":{}}},
 *     description="Изменение количества продукта в корзине",
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится полный объект текущей корзины",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string"),
 *                  @OA\Property(
 *                      property="items",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="middleName", type="string"),
 *                          @OA\Property(property="birthDate", type="string", format="date"),
 *                          @OA\Property(property="cardNum", type="string"),
 *                          @OA\Property(property="isInvalidSection", type="boolean"),
 *                          @OA\Property(property="isInvalidProduct", type="boolean"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                          @OA\Property(property="isNewCard", type="boolean"),
 *                             @OA\Property(
 *                                 property="price",
 *                                 type="array",
 *                                     @OA\Items(
 *                                         @OA\Property(property="calculatedPriceValue", type="number"),
 *                                         @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                                         @OA\Property(property="discountAbsoluteValue", type="number"),
 *                                         @OA\Property(property="discountPercentageValue", type="number"),
 *                                         @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                                         @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                                         @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                                         @OA\Property(property="productId", type="string", format="uuid"),
 *                                     )
 *                             )
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="quantity", type="integer"),
 *                  @OA\Property(property="activationDate", type="string", format="date"),
 *                  @OA\Property(property="lastName", type="string"),
*                   @OA\Property(property="firstName", type="string"),
 *                  @OA\Property(property="middleName", type="string"),
 *                  @OA\Property(property="birthDate", type="string", format="date"),
 *                  @OA\Property(property="cardNum", type="string")
 *              ),
 *          ),
 *     )
 * )
 */
class UpdateItemProductAction extends AbstractCartAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Cart $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            (string)$this->getCurrentUser()->getId(),
            $this->resolveArg('id'),
            $paramsExtractor->getInt('quantity')
        );

        if ($paramsExtractor->has('activationDate')) {
            $command->setActivationDate($paramsExtractor->getString('activationDate'));
        }

        if ($paramsExtractor->has('lastName')) {
            $command->setLastName($paramsExtractor->getString('lastName'));
        }

        if ($paramsExtractor->has('firstName')) {
            $command->setFirstName($paramsExtractor->getString('firstName'));
        }

        if ($paramsExtractor->has('middleName')) {
            $command->setMiddleName($paramsExtractor->getString('middleName'));
        }

        if ($paramsExtractor->has('birthDate')) {
            $command->setBirthDate($paramsExtractor->getString('birthDate'));
        }

        if ($paramsExtractor->has('cardNum')) {
            $command->setCardNum($paramsExtractor->getString('cardNum'));
        }

        return $command;
    }
}
