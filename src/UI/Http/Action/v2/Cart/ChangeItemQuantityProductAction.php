<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use App\Order\Command\Cart\ChangeQuantityProduct\Command;
use App\Order\Model\Cart\Cart;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ChangeItemQuantityProductAction.
 *
 * @OA\Patch(
 *     path="/v2/cart/cart-item/{id}/change-quantity/product",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Корзина"},
 *     security={{"bearerAuth":{}}},
 *     description="Изменение количества продукта в корзине",
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится полный объект текущей корзины",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string"),
 *                  @OA\Property(
 *                      property="items",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="isInvalidSection", type="boolean"),
 *                          @OA\Property(property="isInvalidProduct", type="boolean"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                          @OA\Property(property="isNewCard", type="boolean"),
 *                          @OA\Property(
 *                              property="price",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="calculatedPriceValue", type="number"),
 *                                  @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                                  @OA\Property(property="discountAbsoluteValue", type="number"),
 *                                  @OA\Property(property="discountPercentageValue", type="number"),
 *                                  @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                                  @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                                  @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                                  @OA\Property(property="productId", type="string", format="uuid"),
 *                              )
 *                          )
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="quantity", type="integer")
 *              ),
 *          ),
 *     )
 * )
 */
class ChangeItemQuantityProductAction extends AbstractCartAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Cart $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            (string)$this->getCurrentUser()->getId(),
            $this->resolveArg('id'),
            $paramsExtractor->getInt('quantity')
        );
    }
}
