<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use App\Order\Command\Cart\ValidateProduct\CartItemProductDto;
use App\Order\Command\Cart\ValidateProduct\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ValidateProductAction.
 *
 * @OA\Post(
 *     path="/v2/cart/validate/product",
 *     tags={"Корзина"},
 *     security={{"bearerAuth":{}}},
 *     description="Валидация корзины пользователя",
 *     @OA\RequestBody(
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(
 *                      property="items",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date")
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится полный объект текущей корзины",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(
 *                      property="items",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                             @OA\Property(property="isInvalidSection", type="boolean"),
 *                             @OA\Property(property="isInvalidProduct", type="boolean"),
 *                             @OA\Property(property="isInvalidActivationDate", type="boolean"),
 *                             @OA\Property(
 *                                 property="price",
 *                                 type="array",
 *                                     @OA\Items(
 *                                         @OA\Property(property="calculatedPriceValue", type="number"),
 *                                         @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                                         @OA\Property(property="discountAbsoluteValue", type="number"),
 *                                         @OA\Property(property="discountPercentageValue", type="number"),
 *                                         @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                                         @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                                         @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                                         @OA\Property(property="productId", type="string", format="uuid"),
 *                                     )
 *                             )
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 * )
 */
class ValidateProductAction extends AbstractCartAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);
        $data = array_map([$this, 'serializeCartItemProduct'], $result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $cartItems = [];
        foreach ($paramsExtractor->getArray('items') as $item) {
            $cartItems[] = new CartItemProductDto(
                $item->getString('productId'),
                $item->getInt('quantity'),
                $item->getString('activationDate')
            );
        }

        return new Command(
            $cartItems
        );
    }
}
