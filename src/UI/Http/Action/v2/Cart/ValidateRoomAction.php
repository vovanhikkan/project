<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use App\Order\Command\Cart\ValidateRoom\CartItemRoomDto;
use App\Order\Command\Cart\ValidateRoom\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ValidateRoomAction.
 *
 * @OA\Post(
 *     path="/v2/cart/validate/room",
 *     tags={"Корзина"},
 *     security={{"bearerAuth":{}}},
 *     description="Валидация корзины пользователя",
 *     @OA\RequestBody(
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(
 *                      property="items",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="roomId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date")
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится полный объект текущей корзины",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(
 *                      property="items",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="roomId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                             @OA\Property(property="isInvalidActivationDate", type="boolean")
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 * )
 */
class ValidateRoomAction extends AbstractCartAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);
        $data = array_map([$this, 'serializeCartItemRoom'], $result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $cartItems = [];
        foreach ($paramsExtractor->getArray('items') as $item) {
            $cartItems[] = new CartItemRoomDto(
                $item->getString('roomId'),
                $item->getInt('quantity'),
                $item->getString('activationDate')
            );
        }

        return new Command(
            $cartItems
        );
    }
}
