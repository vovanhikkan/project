<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Cart;

use App\Order\Command\Cart\AddItemProduct\CartItemProductDto;
use App\Order\Command\Cart\AddItemProduct\Command;
use App\Order\Model\Cart\Cart;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * AddItemProductAction.
 *
 * @OA\Post(
 *     path="/v2/cart/cart-item/product",
 *     tags={"Корзина"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление продуктов в корзину",
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится полный объект текущей корзины",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(
 *                      property="itemsProduct",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="middleName", type="string"),
 *                          @OA\Property(property="birthDate", type="string", format="date"),
 *                          @OA\Property(property="cardNum", type="string"),
 *                          @OA\Property(property="isNewCard", type="boolean"),
 *                          @OA\Property(
 *                              property="price",
 *                              type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="calculatedPriceValue", type="number"),
 *                                  @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                                  @OA\Property(property="discountAbsoluteValue", type="number"),
 *                                  @OA\Property(property="discountPercentageValue", type="number"),
 *                                  @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                                  @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                                  @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                                  @OA\Property(property="productId", type="string", format="uuid"),
 *                              )
 *                          )
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsRoom",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="hotel", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="room", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                                  @OA\Property(property="price", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="adultCount", type="number"),
 *                          @OA\Property(property="childCount", type="number"),
 *                          @OA\Property(property="foodType", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="bedType", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="ratePlan", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              )
 *                          ),
 *                          @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="dateFrom", type="string", format="date"),
 *                          @OA\Property(property="dateTo", type="string", format="date"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="isInvalidActivationDate", type="boolean", format="date")
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsGuest",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="roomId", type="string", format="uuid"),
 *                          @OA\Property(property="guestId", type="string", format="uuid"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="age", type="string"),
 *                          @OA\Property(property="mainGuest", type="boolean"),
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="itemsBundle",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string"),
 *                          @OA\Property(property="bundleId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date")
 *                      )
 *                  ),
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="items", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="productId", type="string"),
 *                          @OA\Property(property="quantity", type="integer"),
 *                          @OA\Property(property="activationDate", type="string", format="date"),
 *                          @OA\Property(property="lastName", type="string"),
 *                          @OA\Property(property="firstName", type="string"),
 *                          @OA\Property(property="middleName", type="string"),
 *                          @OA\Property(property="birthDate", type="string", format="date"),
 *                          @OA\Property(property="cardNum", type="string")
 *                      )
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class AddItemProductAction extends AbstractCartAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Cart $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $cartItems = [];
        foreach ($paramsExtractor->getArray('items') as $cartItem) {
            $cartItemDto = new CartItemProductDto(
                $cartItem->getString('productId'),
                $cartItem->getInt('quantity')
            );

            if ($cartItem->has('activationDate')) {
                $cartItemDto->setActivationDate($cartItem->getString('activationDate'));
            }

            if ($cartItem->has('lastName')) {
                $cartItemDto->setLastName($cartItem->getString('lastName'));
            }

            if ($cartItem->has('firstName')) {
                $cartItemDto->setFirstName($cartItem->getString('firstName'));
            }

            if ($cartItem->has('middleName')) {
                $cartItemDto->setMiddleName($cartItem->getString('middleName'));
            }

            if ($cartItem->has('birthDate')) {
                $cartItemDto->setBirthDate($cartItem->getString('birthDate'));
            }

            if ($cartItem->has('cardNum')) {
                $cartItemDto->setCardNum($cartItem->getString('cardNum'));
            }

            $cartItems[] = $cartItemDto;
        }

        return new Command(
            $cartItems,
            (string)$this->getCurrentUser()->getId()
        );
    }
}
