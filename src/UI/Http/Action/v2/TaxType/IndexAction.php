<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\TaxType;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/tax-type",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список TaxType",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="value", type="number")
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractTaxTypeAction
{
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->taxTypeRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
