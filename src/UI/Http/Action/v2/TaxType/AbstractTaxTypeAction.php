<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\TaxType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\TaxType\TaxType;
use App\Hotel\Repository\TaxTypeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractTaxTypeAction extends AbstractAction
{
    protected TaxTypeRepository $taxTypeRepository;

    public function __construct(
        TaxTypeRepository $taxTypeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->taxTypeRepository = $taxTypeRepository;
    }


    protected function serializeList(TaxType $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(TaxType $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'value' => $serializer->asInt($model->getValue())
        ];
    }
}
