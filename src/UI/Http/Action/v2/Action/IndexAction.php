<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/action",
 *     tags={"Акции"},
 *     description="Список акций",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="name", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="shownFrom", type="string", format="date"),
 *                 @OA\Property(property="shownTo", type="string", format="date"),
 *                 @OA\Property(property="orderedFrom", type="string", format="date"),
 *                 @OA\Property(property="orderedTo", type="string", format="date"),
 *                 @OA\Property(property="discountAbsoluteValue", type="number"),
 *                 @OA\Property(property="discountPercentageValue", type="number"),
 *                 @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                 @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                 @OA\Property(property="minDayCount", type="number"),
 *                 @OA\Property(property="maxDayCount", type="number"),
 *                 @OA\Property(property="firstBookingDayFrom", type="string", format="date"),
 *                 @OA\Property(property="firstBookingDayTo", type="string", format="date"),
 *                 @OA\Property(property="lastBookingDayFrom", type="string", format="date"),
 *                 @OA\Property(property="lastBookingDayTo", type="string", format="date"),
 *                 @OA\Property(property="minProductQuantity", type="number"),
 *                 @OA\Property(property="maxProductQuantity", type="number"),
 *                 @OA\Property(property="productActivationFrom", type="string", format="date"),
 *                 @OA\Property(property="productActivationTo", type="string", format="date"),
 *                 @OA\Property(property="hasLevels", type="boolean"),
 *                 @OA\Property(property="hasPromocodes", type="boolean"),
 *                 @OA\Property(property="promocodeLifetime", type="number"),
 *                 @OA\Property(property="promocodePrefix", type="string"),
 *                 @OA\Property(property="isActive", type="boolean"),
 *                 @OA\Property(property="totalCount", type="number"),
 *                 @OA\Property(property="holdedCount", type="number"),
 *                 @OA\Property(property="orderedCount", type="number"),
 *                 @OA\Property(property="bundleActivationFrom", type="string", format="date"),
 *                 @OA\Property(property="bundleActivationTo", type="string", format="date"),
 *                 @OA\Property(property="group", type="string", format="uuid"),
 *                 @OA\Property(property="sort", type="number"),
 *             ),
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractActionAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->actionRepository->fetchAll();
        $data = array_map([$this, 'serializeList'], $items);

        return $this->asJson($data);
    }
}
