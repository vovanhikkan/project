<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Action;

use App\Loyalty\Command\Action\Create\Command;
use App\Loyalty\Model\Action\Action;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/action",
 *     tags={"Акции"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление акций",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"name"},
 *                 @OA\Property(property="name", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="shownFrom", type="string", format="date"),
 *                 @OA\Property(property="shownTo", type="string", format="date"),
 *                 @OA\Property(property="orderedFrom", type="string", format="date"),
 *                 @OA\Property(property="orderedTo", type="string", format="date"),
 *                 @OA\Property(property="discountAbsoluteValue", type="number"),
 *                 @OA\Property(property="discountPercentageValue", type="number"),
 *                 @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                 @OA\Property(property="cashbackPointPervcentageValue", type="number"),
 *                 @OA\Property(property="minDayCount", type="number"),
 *                 @OA\Property(property="maxDayCount", type="number"),
 *                 @OA\Property(property="firstBookingDayFrom", type="string", format="date"),
 *                 @OA\Property(property="firstBookingDayTo", type="string", format="date"),
 *                 @OA\Property(property="lastBookingDayFrom", type="string", format="date"),
 *                 @OA\Property(property="lastBookingDayTo", type="string", format="date"),
 *                 @OA\Property(property="minProductQuantity", type="number"),
 *                 @OA\Property(property="maxProductQuantity", type="number"),
 *                 @OA\Property(property="productActivationFrom", type="string", format="date"),
 *                 @OA\Property(property="productActivationTo", type="string", format="date"),
 *                 @OA\Property(property="hasLevels", type="boolean"),
 *                 @OA\Property(property="hasPromocodes", type="boolean"),
 *                 @OA\Property(property="promocodeLifetime", type="number"),
 *                 @OA\Property(property="promocodePrefix", type="string"),
 *                 @OA\Property(property="totalCount", type="number"),
 *                 @OA\Property(property="holdedCount", type="number"),
 *                 @OA\Property(property="orderedCount", type="number"),
 *                 @OA\Property(property="bundleActivationFrom", type="string", format="date"),
 *                 @OA\Property(property="bundleActivationTo", type="string", format="date"),
 *                 @OA\Property(property="group", type="string", format="uuid"),
 *                 @OA\Property(property="sort", type="number"),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="name", type="object"),
 *                 @OA\Property(property="description", type="object"),
 *                 @OA\Property(property="shownFrom", type="string", format="date"),
 *                 @OA\Property(property="shownTo", type="string", format="date"),
 *                 @OA\Property(property="orderedFrom", type="string", format="date"),
 *                 @OA\Property(property="orderedTo", type="string", format="date"),
 *                 @OA\Property(property="discountAbsoluteValue", type="number"),
 *                 @OA\Property(property="discountPercentageValue", type="number"),
 *                 @OA\Property(property="cashbackPointAbsoluteValue", type="number"),
 *                 @OA\Property(property="cashbackPointPercentageValue", type="number"),
 *                 @OA\Property(property="minDayCount", type="number"),
 *                 @OA\Property(property="maxDayCount", type="number"),
 *                 @OA\Property(property="firstBookingDayFrom", type="string", format="date"),
 *                 @OA\Property(property="firstBookingDayTo", type="string", format="date"),
 *                 @OA\Property(property="lastBookingDayFrom", type="string", format="date"),
 *                 @OA\Property(property="lastBookingDayTo", type="string", format="date"),
 *                 @OA\Property(property="minProductQuantity", type="number"),
 *                 @OA\Property(property="maxProductQuantity", type="number"),
 *                 @OA\Property(property="productActivationFrom", type="string", format="date"),
 *                 @OA\Property(property="productActivationTo", type="string", format="date"),
 *                 @OA\Property(property="hasLevels", type="boolean"),
 *                 @OA\Property(property="hasPromocodes", type="boolean"),
 *                 @OA\Property(property="promocodeLifetime", type="number"),
 *                 @OA\Property(property="promocodePrefix", type="string"),
 *                 @OA\Property(property="isActive", type="boolean"),
 *                 @OA\Property(property="totalCount", type="number"),
 *                 @OA\Property(property="holdedCount", type="number"),
 *                 @OA\Property(property="orderedCount", type="number"),
 *                 @OA\Property(property="bundleActivationFrom", type="string", format="date"),
 *                 @OA\Property(property="bundleActivationTo", type="string", format="date"),
 *                 @OA\Property(property="group", type="string", format="uuid"),
 *                 @OA\Property(property="sort", type="number"),
 *             ),
 *         )
 *     )
 * )
 */
class CreateAction extends AbstractActionAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Action $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getSimpleArray('description'),
            $paramsExtractor->getString('shownFrom'),
            $paramsExtractor->getString('shownTo'),
            $paramsExtractor->getBool('hasLevels'),
            $paramsExtractor->getBool('hasPromocodes'),
            $paramsExtractor->getString('group'),
        );

        if ($paramsExtractor->has('orderedFrom')) {
            $command->setOrderedFrom($paramsExtractor->getString('orderedFrom'));
        }

        if ($paramsExtractor->has('orderedTo')) {
            $command->setOrderedTo($paramsExtractor->getString('orderedTo'));
        }

        if ($paramsExtractor->has('discountAbsoluteValue')) {
            $command->setDiscountAbsoluteValue($paramsExtractor->getInt('discountAbsoluteValue'));
        }

        if ($paramsExtractor->has('discountPercentageValue')) {
            $command->setDiscountPercentageValue($paramsExtractor->getInt('discountPercentageValue'));
        }

        if ($paramsExtractor->has('cashbackPointAbsoluteValue')) {
            $command->setCashbackPointAbsoluteValue($paramsExtractor->getInt('cashbackPointAbsoluteValue'));
        }

        if ($paramsExtractor->has('cashbackPointPercentageValue')) {
            $command->setCashbackPointPercentageValue($paramsExtractor->getInt('cashbackPointPercentageValue'));
        }

        if ($paramsExtractor->has('minDayCount')) {
            $command->setMinDayCount($paramsExtractor->getInt('minDayCount'));
        }

        if ($paramsExtractor->has('maxDayCount')) {
            $command->setMaxDayCount($paramsExtractor->getInt('maxDayCount'));
        }

        if ($paramsExtractor->has('firstBookingDayFrom')) {
            $command->setFirstBookingDayFrom($paramsExtractor->getString('firstBookingDayFrom'));
        }

        if ($paramsExtractor->has('firstBookingDayTo')) {
            $command->setFirstBookingDayTo($paramsExtractor->getString('firstBookingDayTo'));
        }

        if ($paramsExtractor->has('lastBookingDayFrom')) {
            $command->setLastBookingDayFrom($paramsExtractor->getString('lastBookingDayFrom'));
        }

        if ($paramsExtractor->has('lastBookingDayTo')) {
            $command->setLastBookingDayTo($paramsExtractor->getString('lastBookingDayTo'));
        }

        if ($paramsExtractor->has('minProductQuantity')) {
            $command->setMinProductQuantity($paramsExtractor->getInt('minProductQuantity'));
        }

        if ($paramsExtractor->has('lastBookingDayTo')) {
            $command->setMaxProductQuantity($paramsExtractor->getInt('maxProductQuantity'));
        }

        if ($paramsExtractor->has('productActivationFrom')) {
            $command->setProductActivationFrom($paramsExtractor->getString('productActivationFrom'));
        }

        if ($paramsExtractor->has('productActivationTo')) {
            $command->setProductActivationTo($paramsExtractor->getString('productActivationTo'));
        }

        if ($paramsExtractor->has('promocodeLifetime')) {
            $command->setPromocodeLifetime($paramsExtractor->getInt('promocodeLifetime'));
        }

        if ($paramsExtractor->has('promocodePrefix')) {
            $command->setPromocodePrefix($paramsExtractor->getString('promocodePrefix'));
        }

        if ($paramsExtractor->has('totalCount')) {
            $command->setTotalCount($paramsExtractor->getInt('totalCount'));
        }

        if ($paramsExtractor->has('holdedCount')) {
            $command->setHoldedCount($paramsExtractor->getInt('holdedCount'));
        }

        if ($paramsExtractor->has('orderedCount')) {
            $command->setOrderedCount($paramsExtractor->getInt('orderedCount'));
        }

        if ($paramsExtractor->has('bundleActivationFrom')) {
            $command->setBundleActivationFrom($paramsExtractor->getString('bundleActivationFrom'));
        }

        if ($paramsExtractor->has('bundleActivationTo')) {
            $command->setBundleActivationTo($paramsExtractor->getString('bundleActivationTo'));
        }

        return $command;
    }
}
