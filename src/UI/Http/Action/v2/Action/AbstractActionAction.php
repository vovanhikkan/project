<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Action;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Repository\ActionRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractActionAction.
 */
abstract class AbstractActionAction extends AbstractAction
{
    protected ActionRepository $actionRepository;

    public function __construct(
        ActionRepository $actionRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->actionRepository = $actionRepository;
    }


    protected function serializeList(Action $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(Action $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'description' => $serializer->asArray($model->getDescription()),
            'shownFrom' => $serializer->asDateOrNull($model->getShownFrom()),
            'shownTo' => $serializer->asDateOrNull($model->getShownTo()),
            'orderedFrom' => $serializer->asDateOrNull($model->getOrderedFrom()),
            'orderedTo' => $serializer->asDateOrNull($model->getOrderedTo()),
            'discountAbsoluteValue' => $serializer->asIntOrNull($model->getDiscountAbsoluteValue()),
            'discountPercentageValue' => $serializer->asIntOrNull($model->getDiscountPercentageValue()),
            'cashbackPointAbsoluteValue' => $serializer->asIntOrNull($model->getCashbackPointAbsoluteValue()),
            'cashbackPointPercentageValue' => $serializer->asIntOrNull($model->getCashbackPointPercentageValue()),
            'minDayCount' => $serializer->asIntOrNull($model->getMinDayCount()),
            'maxDayCount' => $serializer->asIntOrNull($model->getMaxDayCount()),
            'firstBookingDayFrom' => $serializer->asDateOrNull($model->getFirstBookingDayFrom()),
            'firstBookingDayTo' => $serializer->asDateOrNull($model->getFirstBookingDayTo()),
            'lastBookingDayFrom' => $serializer->asDateOrNull($model->getLastBookingDayFrom()),
            'lastBookingDayTo' => $serializer->asDateOrNull($model->getLastBookingDayTo()),
            'minProductQuantity' => $serializer->asIntOrNull($model->getMinProductQuantity()),
            'maxProductQuantity' => $serializer->asIntOrNull($model->getMaxProductQuantity()),
            'productActivationFrom' => $serializer->asDateOrNull($model->getProductActivationFrom()),
            'productActivationTo' => $serializer->asDateOrNull($model->getProductActivationTo()),
            'hasLevels' => $model->isHasLevels(),
            'hasPromocodes' => $model->isHasPromocodes(),
            'promocodeLifetime' => $serializer->asIntOrNull($model->getPromocodeLifetime()),
            'promocodePrefix' => $serializer->asStringOrNull($model->getPromocodePrefix()),
            'isActive' => $model->isActive(),
            'totalCount' => $serializer->asIntOrNull($model->getTotalCount()),
            'holdedCount' => $serializer->asIntOrNull($model->getHoldedCount()),
            'orderedCount' => $serializer->asIntOrNull($model->getOrderedCount()),
            'bundleActivationFrom' => $serializer->asDateOrNull($model->getBundleActivationFrom()),
            'bundleActivationTo' => $serializer->asDateOrNull($model->getBundleActivationTo()),
            'group' => $serializer->asUuid($model->getGroup()->getId()),
            'sort' => (string)$model->getSort()

        ];
    }
}
