<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FacilityGroup;

use App\Hotel\Command\FacilityGroup\Create\Command;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/facility-group",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление Facility group",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="isShownHotel", type="boolean"),
 *              @OA\Property(property="isShownRoom", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="isShownHotel", type="boolean"),
 *              @OA\Property(property="isShownRoom", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid")
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractFacilityGroupAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var FacilityGroup $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name')
        );

        if ($paramsExtractor->has('mobileIcon')) {
            $command->setMobileIcon($paramsExtractor->getStringOrNull('mobileIcon'));
        }

        if ($paramsExtractor->has('webIcon')) {
            $command->setWebIcon($paramsExtractor->getStringOrNull('webIcon'));
        }
        
        return $command;
    }
}
