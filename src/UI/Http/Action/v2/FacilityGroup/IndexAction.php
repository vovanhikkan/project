<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FacilityGroup;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/facility-group",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Facility group",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="isShownHotel", type="boolean"),
 *              @OA\Property(property="isShownRoom", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid")
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractFacilityGroupAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->facilityGroupRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
