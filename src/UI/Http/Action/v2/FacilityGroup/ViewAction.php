<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FacilityGroup;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/facility-group/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр Facility group",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="isShownHotel", type="boolean"),
 *              @OA\Property(property="isShownRoom", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid")
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractFacilityGroupAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->facilityGroupRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
