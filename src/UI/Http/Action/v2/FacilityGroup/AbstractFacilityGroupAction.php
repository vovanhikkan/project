<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FacilityGroup;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\Hotel\Repository\FacilityGroupRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractFacilityGroupAction
 * @package App\UI\Http\Action\v2\FacilityGroup
 */
abstract class AbstractFacilityGroupAction extends AbstractAction
{

    protected FacilityGroupRepository $facilityGroupRepository;

    public function __construct(
        FacilityGroupRepository $facilityGroupRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->facilityGroupRepository = $facilityGroupRepository;
    }

    protected function serializeList(FacilityGroup $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(FacilityGroup $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'isActive' => $model->getIsActive(),
            'isShownHotel' => $model->getIsShownHotel(),
            'isShownRoom' => $model->getIsShownRoom()
        ];
    }
}
