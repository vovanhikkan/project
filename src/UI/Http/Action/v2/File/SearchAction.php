<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\File;

use App\Exception\AppErrorsException;
use App\Storage\Dto\SearchDto;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

/**
 * SearchFile.
 *
 * @OA\Get(
 *     path="/v2/file/search",
 *     tags={"Файл"},
 *     description="Поиск по названиям файлов",
 *     @OA\Parameter(
 *          required=true,
 *          name="name",
 *          in="query",
 *         @OA\Schema(
 *            type="string",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="page",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="limit",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Response(
 *          response="201",
 *          description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="type", type="number"),
 *                  @OA\Property(property="name", type="string"),
 *                  @OA\Property(property="mime_type", type="string"),
 *                  @OA\Property(property="size", type="number"),
 *                  @OA\Property(property="file_name", type="string"),
 *                  @OA\Property(property="file_path", type="string")
 *              ),
 *          ),
 *     ),
 * )
 *
 * @param Request  $request
 * @param Response $response
 * @param array    $args
 *
 * @throws AppErrorsException
 * @return Response
 */
class SearchAction extends AbstractFileAction
{

    public function handle(ServerRequestInterface $request): Response
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);
        
        $searchDto = new SearchDto();
        if ($paramsExtractor->has('name')) {
            $searchDto->setName($paramsExtractor->getString('name'));
        }

        if ($paramsExtractor->has('page')) {
            $searchDto->setPage($paramsExtractor->getIntOrNull('page'));
        }

        if ($paramsExtractor->has('limit')) {
            $searchDto->setLimit($paramsExtractor->getIntOrNull('limit'));
        }

        $this->validator->validate($searchDto);

        $data = array_map([$this, 'serializeList'], $this->fileRepository->searchFiles($searchDto));

        return $this->asJson($data);
    }
}
