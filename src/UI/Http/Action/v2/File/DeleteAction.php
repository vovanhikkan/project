<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\File;

use App\Storage\Command\File\Delete\Command;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * DeleteAction
 *
 * @OA\Delete(
 *  path="/v2/file/{id}",
 *  tags={"Файл"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Удаление файла",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="message", type="string")
 *          )
 *      )
 *  )
 * )
 */
class DeleteAction extends AbstractFileAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);
        $data = $result;

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        return new Command(
            $this->resolveArg('id')
        );
    }
}
