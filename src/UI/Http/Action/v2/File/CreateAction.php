<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\File;

use App\Storage\Command\File\Create\Command;
use App\Storage\Model\File\File;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * CreateAction.
 *
 * @OA\Post(
 *     path="/v2/file/upload",
 *     tags={"Файл"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление файла",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(property="file", type="string", format="binary"),
 *             ),
 *          ),
 *     ),
 *     @OA\Response(
 *          response="201",
 *          description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *              @OA\Property(property="files", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="type", type="number"),
 *                      @OA\Property(property="name", type="string"),
 *                      @OA\Property(property="mime_type", type="string"),
 *                      @OA\Property(property="size", type="number"),
 *                      @OA\Property(property="file_name", type="string"),
 *                      @OA\Property(property="file_path", type="string")
 *                  )
 *              ),
 *              ),
 *          ),
 *     ),
 * )
 */
class CreateAction extends AbstractFileAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $commands = $this->deserialize($request);
        foreach ($commands as $command) {
            $this->validator->validate($command);
            /** @var File $result */
            $result = $this->bus->handle($command);
            $data['files'][] = $this->serializeItem($result);
        }

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): array
    {
        if (is_array($request->getUploadedFiles()['file'])) {
            $uploadedFiles = $request->getUploadedFiles()['file'];
        } else {
            $uploadedFiles[] = $request->getUploadedFiles()['file'];
        }
        
        $commands = [];
        foreach ($uploadedFiles as $file) {
            $commands[] = new Command($file);
        }

        return $commands;
    }
}
