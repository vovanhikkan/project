<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\File;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Storage\Model\File\File;
use App\Storage\Repository\FileRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractFileAction
 * @package App\UI\Http\Action\v2\File
 */
abstract class AbstractFileAction extends AbstractAction
{
    protected FileRepository $fileRepository;

    public function __construct(
        FileRepository $fileRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->fileRepository = $fileRepository;
    }

    protected function serializeList(File $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(File $model): array
    {
        $serializer = $this->serializer;
        
        return [
            'id' => (string)$model->getId(),
            'type' => $serializer->asInt($model->getType()),
            'name' => (string)$model->getName(),
            'mime_type' => (string)$model->getMimeType(),
            'size' => $serializer->asInt($model->getSize()),
            'file_name' => (string)$model->getFileName(),
            'file_path' => (string)$model->getFilePath()
        ];
    }
}
