<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Auth;

use App\Auth\Command\User\Register\Command;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/auth/register",
 *     tags={"Авторизация"},
 *     description="Регистрация",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"email", "lastName", "firstName"},
 *                 @OA\Property(property="email", type="string", format="email"),
 *                 @OA\Property(property="lastName", type="string"),
 *                 @OA\Property(property="firstName", type="string"),
 *                 @OA\Property(property="middleName", type="string"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="birthDate", type="string", format="date"),
 *                 @OA\Property(
 *                     property="sex",
 *                     type="integer",
 *                     enum={100, 200},
 *                     description="`100` — male, `200` — female"
 *                 ),
 *                 @OA\Property(property="serviceLetters", type="boolean"),
 *                 @OA\Property(property="mailing", type="boolean")
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="accessToken", type="string"),
 *                 @OA\Property(property="refreshToken", type="string")
 *             )
 *         )
 *     )
 * )
 */
class RegisterAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);

        return $this->asJson($result);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            trim($paramsExtractor->getString('email')),
            $paramsExtractor->getString('lastName'),
            $paramsExtractor->getString('firstName'),
            $paramsExtractor->getBool('serviceLetters'),
            $paramsExtractor->getBool('mailing')
        );

        if ($paramsExtractor->has('middleName')) {
            $command->setMiddleName($paramsExtractor->getStringOrNull('middleName'));
        }

        if ($paramsExtractor->has('phone')) {
            $command->setPhone($paramsExtractor->getStringOrNull('phone'));
        }

        if ($paramsExtractor->has('birthDate')) {
            $command->setBirthDate($paramsExtractor->getStringOrNull('birthDate'));
        }

        if ($paramsExtractor->has('sex')) {
            $command->setSex($paramsExtractor->getIntOrNull('sex'));
        }

        return $command;
    }
}
