<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Auth\ResetPassword;

use App\Auth\Command\User\ResetPassword\Request\Command;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/auth/reset-password/request",
 *     tags={"Авторизация"},
 *     description="Запрос восстановления пароля",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"email"},
 *                 @OA\Property(property="email", type="string", format="email")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=204,
 *         description=""
 *     )
 * )
 */
class RequestAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $this->bus->handle($command);

        return $this->asEmpty();
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('email')
        );
    }
}
