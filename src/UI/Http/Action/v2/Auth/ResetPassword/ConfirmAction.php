<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Auth\ResetPassword;

use App\Auth\Command\User\ResetPassword\Confirm\Command;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/auth/reset-password/confirm",
 *     tags={"Авторизация"},
 *     description="Подтверждение восстановления пароля",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"token", "password"},
 *                 @OA\Property(property="token", type="string"),
 *                 @OA\Property(property="password", type="string", format="password")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=204,
 *         description=""
 *     )
 * )
 */
class ConfirmAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $this->bus->handle($command);

        return $this->asEmpty();
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('token'),
            $paramsExtractor->getString('password'),
        );
    }
}
