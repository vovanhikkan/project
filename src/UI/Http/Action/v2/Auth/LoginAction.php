<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Auth;

use App\Auth\Command\User\Login\Command;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/auth/login",
 *     tags={"Авторизация"},
 *     description="Регистрация",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"email", "password"},
 *                 @OA\Property(property="email", type="string", format="email"),
 *                 @OA\Property(property="password", type="string", format="password")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="accessToken", type="string"),
 *                 @OA\Property(property="refreshToken", type="string")
 *             )
 *         )
 *     )
 * )
 */
class LoginAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var array $result */
        $result = $this->bus->handle($command);

        return $this->asJson($result);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('email'),
            $paramsExtractor->getString('password')
        );
    }
}
