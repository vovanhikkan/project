<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\HotelType\HotelType;
use App\Hotel\Repository\HotelTypeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractHotelTypeAction
 * @package App\UI\Http\Action\v2\HotelType
 */
abstract class AbstractHotelTypeAction extends AbstractAction
{
    protected HotelTypeRepository $hotelTypeRepository;

    public function __construct(
        HotelTypeRepository $hotelTypeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->hotelTypeRepository = $hotelTypeRepository;
    }

    protected function serializeList(HotelType $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(HotelType $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
        ];
    }
}
