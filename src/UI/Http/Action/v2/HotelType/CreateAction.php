<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelType;

use App\Hotel\Command\HotelType\Create\Command;
use App\Hotel\Model\HotelType\HotelType;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/hotel-type",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление HotelType",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object")
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractHotelTypeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var HotelType $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name')
        );

        return $command;
    }
}
