<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Bundle;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleItem\BundleItem;
use App\Bundle\Repository\BundleProductRepository;
use App\Bundle\Repository\BundleRepository;
use App\Bundle\Service\Bundle\Filter;
use App\Content\Model\Category\Category;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Storage\Service\File\Locator;
use App\Bundle\Model\BundleTerm\BundleTerm;
use App\Bundle\Service\Bundle\RecommendationGetter;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractBundleAction
 * @package App\UI\Http\Action\v2\Bundle
 */
abstract class AbstractBundleAction extends AbstractAction
{
    protected RecommendationGetter $recommendationGetter;
    protected BundleRepository $bundleRepository;
    protected BundleProductRepository $bundleProductRepository;
    protected Filter $filter;

    public function __construct(
        RecommendationGetter $recommendationGetter,
        BundleRepository $bundleRepository,
        BundleProductRepository $bundleProductRepository,
        Filter $filter,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->recommendationGetter = $recommendationGetter;
        $this->bundleRepository = $bundleRepository;
        $this->bundleProductRepository = $bundleProductRepository;
        $this->filter = $filter;
    }


    protected function serializeList(Bundle $model): array
    {
        $serializer = $this->serializer;

        $background = $this->getFileData($model->getWebBackground());

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'code' => $serializer->asString($model->getCode()),
            'description' => $model->getDescription() !== null ? $serializer->asArray($model->getDescription()) : null,
            'dayCount' => $serializer->asIntOrNull($model->getDayCount()),
            'minPrice' => $serializer->asIntOrNull($model->getMinPrice()),
            'minChildPrice' => $serializer->asIntOrNull($model->getMinChildPrice()),
            'profit' => $serializer->asIntOrNull($model->getProfit()),
            'isWithAccommodation' => $model->isWithAccommodation(),
            'webBackground' => $background,
            'widthType' => $serializer->asIntOrNull($model->getWidthType()),
            'shownFrom' => $serializer->asDateTimeOrNull($model->getShownFrom()),
            'shownTo' => $serializer->asDateTimeOrNull($model->getShownTo()),
            'firstBookingDayFrom' => $serializer->asDateTimeOrNull($model->getFirstBookingDayFrom()),
            'firstBookingDayTo' => $serializer->asDateTimeOrNull($model->getFirstBookingDayTo()),
        ];
    }

    protected function serializeItem(Bundle $model): array
    {
        $serializer = $this->serializer;

        return array_merge($this->serializeList($model), [
            'items' => array_map(
                function (BundleItem $bundleItem) use ($serializer) {
                    $photo = $this->getFileData($bundleItem->getPhoto());
                    $place = $bundleItem->getPlace();
                    return [
                        'id' => $serializer->asUuid($bundleItem->getId()),
                        'name' => $serializer->asArray($bundleItem->getName()),
                        'subName' => $serializer->asArray($bundleItem->getSubName()),
                        'description' => $serializer->asArray($bundleItem->getDescription()),
                        'photo' => $photo,
                        'place' => $place !== null ? array_map(
                            function (Category $category) use ($serializer) {
                                return [
                                    'web_icon' =>  $this->getFileData($category->getWebIcon()),
                                    'mob_icon' =>  $this->getFileData($category->getMobileIcon()),
                                    'web_icon_svg' => $this->getFileData($category->getWebIconSvg()),
                                    'web_gradient_color1' => $serializer->asStringOrNull(
                                        $category->getWebGradientColor1()
                                    ),
                                    'web_gradient_color2' => $serializer->asStringOrNull(
                                        $category->getWebGradientColor2()
                                    )
                                ];
                            },
                            $place->getCategories()
                        ) : [],
                        'placeId' => $place !== null ? $serializer->asUuid($place->getId()) : null,
                        'widthType' => $serializer->asIntOrNull($bundleItem->getWidthType())
                    ];
                },
                $model->getBundleItems(),
            ),
            'terms' => array_map(
                function (BundleTerm $bundleTerm) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($bundleTerm->getId()),
                        'description' => $serializer->asArray($bundleTerm->getDescription())
                    ];
                },
                $model->getBundleTerms()
            ),
            'ratePlans' => array_map(
                function (RatePlan $ratePlan) use ($serializer) {
                    return [
                        'name' => $serializer->asArray($ratePlan->getName()),
                        'minPrice' => $ratePlan->getMinPrice(),
                    ];
                },
                $model->getRatePlans()
            )
        ]);
    }

}
