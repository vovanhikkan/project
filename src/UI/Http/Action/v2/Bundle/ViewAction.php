<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Bundle;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bundle/{id}",
 *  tags={"Пакеты"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр Bundle",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="dayCount", type="int"),
 *              @OA\Property(property="minPrice", type="int"),
 *              @OA\Property(property="profit", type="int"),
 *              @OA\Property(property="isWithAccommodation", type="boolean"),
 *              @OA\Property(property="webBackground", type="string", type="uuid"),
 *              @OA\Property(
 *                  property="widthType",
 *                  type="integer",
 *                   enum={100, 200, 300, 400},
 *                   description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *              ),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="firstBookingDayFrom", type="string"),
 *              @OA\Property(property="firstBookingDayTo", type="string"),
 *              @OA\Property(property="items", type="object",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="subName", type="object"),
 *                      @OA\Property(property="description", type="object")
 *                  )
 *              ),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractBundleAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->bundleRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
