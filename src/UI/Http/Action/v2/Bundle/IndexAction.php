<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Bundle;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bundle",
 *  tags={"Пакеты"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Bundle",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="dayCount", type="int"),
 *              @OA\Property(property="minPrice", type="int"),
 *              @OA\Property(property="profit", type="int"),
 *              @OA\Property(property="isWithAccommodation", type="boolean"),
 *              @OA\Property(property="webBackground", type="string", type="uuid"),
 *              @OA\Property(
 *                  property="widthType",
 *                  type="integer",
 *                   enum={100, 200, 300, 400},
 *                   description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *              ),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="firstBookingDayFrom", type="string"),
 *              @OA\Property(property="firstBookingDayTo", type="string"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractBundleAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->bundleRepository->fetchAll(!$this->getCurrentUser()
            || !$this->getCurrentUser()->getRole()->isAdmin());
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
