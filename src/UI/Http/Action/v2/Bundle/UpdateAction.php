<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Bundle;

use App\Bundle\Command\Bundle\Update\Command;
use App\Bundle\Model\Bundle\Bundle;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/bundle/{id}",
 *  tags={"Пакеты"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование Bundle",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="dayCount", type="int"),
 *              @OA\Property(property="minPrice", type="int"),
 *              @OA\Property(property="profit", type="int"),
 *              @OA\Property(property="isWithAccommodation", type="boolean"),
 *              @OA\Property(property="webBackground", type="string", type="uuid"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="dayCount", type="int"),
 *              @OA\Property(property="minPrice", type="int"),
 *              @OA\Property(property="profit", type="int"),
 *              @OA\Property(property="isWithAccommodation", type="boolean"),
 *              @OA\Property(property="webBackground", type="string", type="uuid"),
 *              @OA\Property(
 *                  property="widthType",
 *                  type="integer",
 *                   enum={100, 200, 300, 400},
 *                   description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *              ),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="firstBookingDayFrom", type="string"),
 *              @OA\Property(property="firstBookingDayTo", type="string"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractBundleAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Bundle $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );

        if ($paramsExtractor->has('name')) {
            $command->setIsWithAccommodation($paramsExtractor->getBool('isWithAccommodation'));
        }

        if ($paramsExtractor->has('name')) {
            $command->setName($paramsExtractor->getSimpleArray('name'));
        }

        if ($paramsExtractor->has('code')) {
            $command->setCode($paramsExtractor->getString('code'));
        }

        if ($paramsExtractor->has('description')) {
            $command->setDescription($paramsExtractor->getSimpleArray('description'));
        }

        if ($paramsExtractor->has('dayCount')) {
            $command->setDayCount($paramsExtractor->getInt('dayCount'));
        }

        if ($paramsExtractor->has('minPrice')) {
            $command->setMinPrice($paramsExtractor->getInt('minPrice'));
        }

        if ($paramsExtractor->has('profit')) {
            $command->setProfit($paramsExtractor->getInt('profit'));
        }

        if ($paramsExtractor->has('webBackground')) {
            $command->setWebBackground($paramsExtractor->getString('webBackground'));
        }

        return $command;
    }
}
