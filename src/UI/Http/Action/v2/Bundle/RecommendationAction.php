<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Bundle;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bundle/recommendation",
 *  tags={"Пакеты"},
 *  security={{"bearerAuth":{}}},
 *  description="Список рекомендованных Bundle",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="dayCount", type="integer"),
 *              @OA\Property(property="minPrice", type="integer"),
 *              @OA\Property(property="profit", type="integer"),
 *              @OA\Property(property="foodType", type="string"),
 *              @OA\Property(property="isWithAccommodation", type="boolean"),
 *              @OA\Property(property="webBackground", type="string"),
 *          )
 *      )
 *  )
 * )
 */
class RecommendationAction extends AbstractBundleAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->bundleRepository->getRecommendations();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
