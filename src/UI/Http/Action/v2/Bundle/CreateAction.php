<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Bundle;

use App\Bundle\Command\Bundle\Create\Command;
use App\Bundle\Model\Bundle\Bundle;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/bundle",
 *  tags={"Пакеты"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление Bundle",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="multipart/form-data",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="dayCount", type="number"),
 *              @OA\Property(property="minPrice", type="number"),
 *              @OA\Property(property="profit", type="number"),
 *              @OA\Property(property="isWithAccommodation", type="boolean"),
 *              @OA\Property(property="webBackground", type="string", format="binary"),
 *              @OA\Property(property="items", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid")
 *                   )
 *              ),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="dayCount", type="int"),
 *              @OA\Property(property="minPrice", type="int"),
 *              @OA\Property(property="profit", type="int"),
 *              @OA\Property(property="isWithAccommodation", type="boolean"),
 *              @OA\Property(property="webBackground", type="string", type="uuid"),
 *              @OA\Property(
 *                  property="widthType",
 *                  type="integer",
 *                   enum={100, 200, 300, 400},
 *                   description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *              ),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractBundleAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Bundle $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getString('code'),
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getBool('isWithAccommodation'),
        );

        if ($paramsExtractor->has('description')) {
            $command->setDescription($paramsExtractor->getSimpleArray('description'));
        }

        if ($paramsExtractor->has('dayCount')) {
            $command->setDayCount($paramsExtractor->getInt('dayCount'));
        }

        if ($paramsExtractor->has('minPrice')) {
            $command->setMinPrice($paramsExtractor->getInt('minPrice'));
        }

        if ($paramsExtractor->has('profit')) {
            $command->setProfit($paramsExtractor->getInt('profit'));
        }

        if (isset($request->getUploadedFiles()['webBackground'])) {
            $command->setWebBackground($request->getUploadedFiles()['webBackground']);
        }

        if ($paramsExtractor->has('items')) {
            $command->setItems($paramsExtractor->getSimpleArray('items'));
        }

        return $command;
    }
}
