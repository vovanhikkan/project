<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Bundle;

use App\Bundle\Dto\SearchDto;
use App\UI\Http\ParamsExtractor;
use DateTimeImmutable;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

/**
 * SearchAction.
 *
 * @OA\Get(
 *     path="/v2/bundle/search/product",
 *     tags={"Пакеты"},
 *     description="Поиск product в bundles",
 *     @OA\Parameter(
 *          required=true,
 *          name="bundleId",
 *          in="query",
 *         @OA\Schema(
 *            type="string",
 *            format="uuid"
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="date",
 *          in="query",
 *         @OA\Schema(
 *            type="date",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="adultCount",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="childCount",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="childAges[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="integer"),
 *         ),
 *     ),
 * @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="bundleProductId", type="string", format="uuid"),
 *              @OA\Property(property="price", type="string"),
 *          )
 *      )
 *  )
 * )
 *
 * @param Request  $request
 * @param Response $response
 * @param array    $args
 *
 * @throws \App\Exception\AppErrorsException
 * @return Response
 */
class SearchAction extends AbstractBundleAction
{

    public function handle(ServerRequestInterface $request): Response
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);

        $searchDto = new SearchDto($paramsExtractor->getString('bundleId'));

        if ($paramsExtractor->has('date')) {
            $searchDto->setDate($paramsExtractor->getString('date'));
        }
        if ($paramsExtractor->has('adultCount')) {
            $searchDto->setAdultCount($paramsExtractor->getInt('adultCount'));
        }
        if ($paramsExtractor->has('childCount')) {
            $searchDto->setChildCount($paramsExtractor->getInt('childCount'));
        }
        if ($paramsExtractor->has('childAges')) {
            $searchDto->setChildAges($paramsExtractor->getSimpleArray('childAges'));
        }

        $date = $paramsExtractor->getStringOrNull('date');

        $this->validator->validate($searchDto);

        $model = $this->bundleProductRepository->searchBundleProduct($searchDto);

        $result = $this->filter->do(
            $model,
            $date !== null ? new DateTimeImmutable($date) : null
        );


        return $this->asJson($result);
    }
}
