<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User;

use App\Auth\Command\User\ChangeProfile\Command;
use App\Auth\Model\User\User;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/user/{id}/change-profile",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Пользователь"},
 *     security={{"bearerAuth":{}}},
 *     description="Редактирование профиля",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"email", "lastName", "firstName", "serviceLetters", "mailing"},
 *                 @OA\Property(property="email", type="string", format="email"),
 *                 @OA\Property(property="lastName", type="string"),
 *                 @OA\Property(property="firstName", type="string"),
 *                 @OA\Property(property="middleName", type="string"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="birthDate", type="string", format="date"),
 *                 @OA\Property(property="isEmailVerified", type="boolean"),
 *                 @OA\Property(property="isPhoneVerified", type="boolean"),
 *                 @OA\Property(
 *                     property="sex",
 *                     type="integer",
 *                     enum={100, 200},
 *                     description="`100` — male, `200` — female"
 *                 ),
 *                 @OA\Property(property="serviceLetters", type="boolean"),
 *                 @OA\Property(property="mailing", type="boolean")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="email", type="string", format="email"),
 *                 @OA\Property(property="lastName", type="string"),
 *                 @OA\Property(property="firstName", type="string"),
 *                 @OA\Property(property="middleName", type="string"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="birthDate", type="string", format="date"),
 *                 @OA\Property(
 *                     property="sex",
 *                     type="integer",
 *                     enum={100, 200},
 *                     description="`100` — male, `200` — female"
 *                 ),
 *                 @OA\Property(property="serviceLetters", type="boolean"),
 *                 @OA\Property(property="mailing", type="boolean")
 *             ),
 *         )
 *     )
 * )
 */
class ChangeProfileAction extends AbstractUserAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var User $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $id = $this->resolveArg('id');
        $this->ensureUserIdAccess($id);

        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $id,
            trim($paramsExtractor->getString('email')),
            $paramsExtractor->getString('lastName'),
            $paramsExtractor->getString('firstName'),
            $paramsExtractor->getBool('serviceLetters'),
            $paramsExtractor->getBool('mailing')
        );

        if ($paramsExtractor->has('middleName')) {
            $command->setMiddleName($paramsExtractor->getStringOrNull('middleName'));
        }

        if ($paramsExtractor->has('phone')) {
            $command->setPhone($paramsExtractor->getStringOrNull('phone'));
        }

        if ($paramsExtractor->has('birthDate')) {
            $command->setBirthDate($paramsExtractor->getStringOrNull('birthDate'));
        }

        if ($paramsExtractor->has('sex')) {
            $command->setSex($paramsExtractor->getIntOrNull('sex'));
        }

        return $command;
    }
}
