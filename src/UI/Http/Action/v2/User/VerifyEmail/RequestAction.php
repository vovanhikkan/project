<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User\VerifyEmail;

use App\Auth\Command\User\VerifyEmail\Request\Command;
use App\UI\Http\Action\v2\User\AbstractUserAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;
use Slim\Exception\HttpUnauthorizedException;

/**
 * @OA\Post(
 *     path="/v2/user/{id}/verify-email/request",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Пользователь"},
 *     security={{"bearerAuth":{}}},
 *     description="Запрос на подтверждение e-mail",
 *     @OA\Response(response="204", description="")
 * )
 */
class RequestAction extends AbstractUserAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $this->bus->handle($command);

        return $this->asEmpty();
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $id = $this->resolveArg('id');
        $this->ensureUserIdAccess($id);

        return new Command(
            $this->resolveArg('id')
        );
    }
}
