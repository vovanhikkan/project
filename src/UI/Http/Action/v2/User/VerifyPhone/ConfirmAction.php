<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User\VerifyPhone;

use App\Auth\Command\User\VerifyPhone\Confirm\Command;
use App\UI\Http\Action\v2\User\AbstractUserAction;
use App\UI\Http\ParamsExtractor;
use OpenApi\Annotations as OA;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/user/{id}/verify-phone/confirm",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Пользователь"},
 *     security={{"bearerAuth":{}}},
 *     description="Подтверждение телефона",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"token"},
 *                 @OA\Property(property="token", type="number")
 *             )
 *         )
 *     ),
 *     @OA\Response(response="204", description="")
 * )
 */
class ConfirmAction extends AbstractUserAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $this->bus->handle($command);

        return $this->asEmpty();
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('token')
        );
    }
}
