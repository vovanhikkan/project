<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User\VerifyPhone;

use App\Auth\Command\User\VerifyPhone\Request\Command;
use App\UI\Http\Action\v2\User\AbstractUserAction;
use OpenApi\Annotations as OA;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/user/{id}/verify-phone/request",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Пользователь"},
 *     security={{"bearerAuth":{}}},
 *     description="Запрос на подтверждение телефона",
 *     @OA\Response(response="204", description="")
 * )
 */
class RequestAction extends AbstractUserAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $this->bus->handle($command);

        return $this->asEmpty();
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $id = $this->resolveArg('id');
        $this->ensureUserIdAccess($id);

        return new Command(
            $this->resolveArg('id')
        );
    }
}
