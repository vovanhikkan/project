<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Get(
 *     path="/v2/user/me",
 *     tags={"Пользователь"},
 *     security={{"bearerAuth":{}}},
 *     description="Информация о текущем авторизованном пользователе",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="email", type="string", format="email"),
 *                 @OA\Property(property="lastName", type="string"),
 *                 @OA\Property(property="firstName", type="string"),
 *                 @OA\Property(property="middleName", type="string"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="birthDate", type="string", format="date"),
 *                 @OA\Property(property="isEmailVerified", type="boolean"),
 *                 @OA\Property(property="isPhoneVerified", type="boolean"),
 *                 @OA\Property(
 *                     property="sex",
 *                     type="integer",
 *                     enum={100, 200},
 *                     description="`100` — male, `200` — female"
 *                 ),
 *                 @OA\Property(property="serviceLetters", type="boolean"),
 *                 @OA\Property(property="mailing", type="boolean")
 *             )
 *         )
 *     )
 * )
 */
class MeAction extends AbstractUserAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $user = $this->getCurrentUser();
        $data = $this->serializeItem($user);

        return $this->asJson($data);
    }
}
