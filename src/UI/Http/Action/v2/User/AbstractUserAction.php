<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User;

use App\Application\Service\Validator;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\Guest\Guest;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractUserAction
 * @package App\UI\Http\Action\v2\User
 */
abstract class AbstractUserAction extends AbstractAction
{
    protected UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->userRepository = $userRepository;
    }

    protected function serializeItem(User $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => (string)$model->getId(),
            'email' => (string)$model->getEmail(),
            'phone' => $model->getPhone() !== null ? (string)$model->getPhone() : null,
            'lastName' => (string)$model->getLastName(),
            'firstName' => (string)$model->getFirstName(),
            'middleName' => $model->getMiddleName() !== null ? (string)$model->getMiddleName() : null,
            'birthDate' => $model->getBirthDate() !== null ? $model->getBirthDate()->format('Y-m-d') : null,
            'sex' => $model->getSex() !== null ? $model->getSex()->getValue() : null,
            'serviceLetters' => $model->isConfirmedServiceLetters(),
            'mailing' => $model->isConfirmedMailing(),
            'isEmailVerified' => $model->isEmailVerified(),
            'isPhoneVerified' => $model->isPhoneVerified(),
            'guests' => array_map(
                function (Guest $guest) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($guest->getId()),
                        'firstName' => $serializer->asString($guest->getFirstName()),
                        'lastName' => $serializer->asString($guest->getLastName())
                    ];
                },
                $model->getGuests()
            )
        ];
    }
}
