<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User;

use App\Auth\Command\User\ChangePassword\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/user/{id}/change-password",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Пользователь"},
 *     security={{"bearerAuth":{}}},
 *     description="Изменение пароля",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"passwordCurrent", "passwordNew"},
 *                 @OA\Property(property="passwordCurrent", type="string", format="password"),
 *                 @OA\Property(property="passwordNew", type="string", format="password")
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="204",
 *         description=""
 *     )
 * )
 */
class ChangePasswordAction extends AbstractUserAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $this->bus->handle($command);

        return $this->asEmpty();
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $id = $this->resolveArg('id');
        $this->ensureUserIdAccess($id);

        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $id,
            $paramsExtractor->getString('passwordCurrent'),
            $paramsExtractor->getString('passwordNew'),
        );
    }
}
