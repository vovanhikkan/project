<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\User;

use App\Auth\Command\User\Block\Command;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Post(
 *     path="/v2/user/{id}/block",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Пользователь"},
 *     security={{"bearerAuth":{}}},
 *     description="Блокирование",
 *     @OA\Response(
 *         response="204",
 *         description=""
 *     )
 * )
 */
class BlockAction extends AbstractUserAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $this->bus->handle($command);

        return $this->asEmpty();
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $id = $this->resolveArg('id');
        $this->ensureUserIdAccess($id);

        return new Command($id);
    }
}
