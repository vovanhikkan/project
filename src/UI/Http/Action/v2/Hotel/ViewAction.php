<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Hotel;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/hotel/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр Hotel",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="starCount", type="number"),
 *              @OA\Property(property="lat", type="number"),
 *              @OA\Property(property="lon", type="number"),
 *              @OA\Property(property="links", type="object"),
 *              @OA\Property(property="logo", type="string", format="uuid"),
 *              @OA\Property(property="logoSvg", type="string", format="uuid"),
 *              @OA\Property(property="hotelPhoto", type="string", format="uuid"),
 *              @OA\Property(property="video", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="youtube", type="string"),
 *                      @OA\Property(property="background", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="tags", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="icon",
 *                          @OA\Property(property="name", type="string"),
 *                          @OA\Property(property="fileUrl", type="string"),
 *                      ),
 *                      @OA\Property(property="iconSvg",
 *                          @OA\Property(property="name", type="string"),
 *                          @OA\Property(property="fileUrl", type="string"),
 *                      ),
 *                      @OA\Property(property="colorText", type="string"),
 *                      @OA\Property(property="colorBorder", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="termsOfPlacement", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="checkInTime", type="object"),
 *                      @OA\Property(property="checkOutTime", type="object"),
 *                      @OA\Property(property="cancelPrepayments", type="object"),
 *                      @OA\Property(property="extraBeds", type="object"),
 *                      @OA\Property(property="pets", type="object"),
 *                      @OA\Property(property="extraInfo", type="object"),
 *                      @OA\Property(property="payments", type="object"),
 *                  ),
 *              ),
 *              @OA\Property(property="location", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="rooms", type="object",
 *                  @OA\Property(property="photos", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="name", type="string", format="uuid"),
 *                          @OA\Property(property="photos", type="string", format="uuid"),
 *                      ),
 *                  ),
 *              ),
 *              @OA\Property(property="bundles", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="name", type="object", format="uuid"),
 *                      @OA\Property(property="webBackground", type="object",
 *                          @OA\Property(property="name", type="string", format="uuid"),
 *                          @OA\Property(property="fileUrl", type="string", format="uuid"),
 *                      ),
 *                  ),
 *              ),
 *              @OA\Property(property="facilities", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="object", format="uuid",
 *                          @OA\Property(property="name", type="object", format="uuid"),
 *                          @OA\Property(property="items", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="id", type="string", format="uuid"),
 *                                  @OA\Property(property="name", type="object"),
 *                              ),
 *                          ),
 *                      ),
 *                  ),
 *              ),
 *              @OA\Property(property="places", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="object", format="uuid",
 *                          @OA\Property(property="name", type="object", format="uuid"),
 *                          @OA\Property(property="workingHoursDescription", type="object"),
 *                          @OA\Property(property="isFullDay", type="boolean"),
 *                          @OA\Property(property="worksFrom", type="string"),
 *                          @OA\Property(property="worksTo", type="string"),
 *                          @OA\Property(property="minPrice", type="number"),
 *                          @OA\Property(property="categories", type="array",
 *                              @OA\Items(
 *                                  @OA\Property(property="webIcon", type="string", format="uuid"),
 *                                  @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *                                  @OA\Property(property="webGradientColor1", type="string"),
 *                                  @OA\Property(property="webGradientColor2", type="string"),
 *                                  @OA\Property(property="mobileBackground", type="string", format="uuid"),
 *                              ),
 *                          ),
 *                      ),
 *                  ),
 *              ),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractHotelAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->hotelRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }


}
