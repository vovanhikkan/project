<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Hotel;

use App\Hotel\Command\Hotel\Create\Command;
use App\Hotel\Command\Hotel\Create\TagDto;
use App\Hotel\Model\Hotel\Hotel;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/hotel",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление Hotel",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="starCount", type="number"),
 *              @OA\Property(property="location", type="string", format="uuid"),
 *              @OA\Property(property="address", type="object"),
 *              @OA\Property(property="logoSvg", type="string", format="uuid"),
 *              @OA\Property(property="logo", type="string", format="uuid"),
 *              @OA\Property(property="links", type="object"),
 *              @OA\Property(property="lat", type="number"),
 *              @OA\Property(property="lon", type="number"),
 *              @OA\Property(property="type", type="string", format="uuid"),
 *              @OA\Property(property="brand", type="string", format="uuid"),
 *              @OA\Property(property="tags", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="colorText", type="string"),
 *                      @OA\Property(property="colorBorder", type="string")
 *                  )
 *              ),
 *              @OA\Property(property="places", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid")
 *                   )
 *              ),
 *              @OA\Property(property="checkInTime", type="object"),
 *              @OA\Property(property="checkOutTime", type="object"),
 *              @OA\Property(property="cancelPrepayments", type="object"),
 *              @OA\Property(property="extraBeds", type="object"),
 *              @OA\Property(property="pets", type="object"),
 *              @OA\Property(property="extraInfo", type="object"),
 *              @OA\Property(property="payments", type="object"),
 *              @OA\Property(property="hotelPhoto", type="string", format="uuid"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="starCount", type="number"),
 *              @OA\Property(property="lat", type="number"),
 *              @OA\Property(property="lon", type="number"),
 *              @OA\Property(property="links", type="object"),
 *              @OA\Property(property="logo", type="string", format="uuid"),
 *              @OA\Property(property="logoSvg", type="string", format="uuid"),
 *              @OA\Property(property="hotelPhoto", type="string", format="uuid"),
 *              @OA\Property(property="video", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="youtube", type="string"),
 *                      @OA\Property(property="background", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="places", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="tags", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="icon",
 *                          @OA\Property(property="name", type="string"),
 *                          @OA\Property(property="fileUrl", type="string"),
 *                      ),
 *                      @OA\Property(property="iconSvg",
 *                          @OA\Property(property="name", type="string"),
 *                          @OA\Property(property="fileUrl", type="string"),
 *                      ),
 *                      @OA\Property(property="colorText", type="string"),
 *                      @OA\Property(property="colorBorder", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="termsOfPlacement", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="checkInTime", type="object"),
 *                      @OA\Property(property="checkOutTime", type="object"),
 *                      @OA\Property(property="cancelPrepayments", type="object"),
 *                      @OA\Property(property="extraBeds", type="object"),
 *                      @OA\Property(property="pets", type="object"),
 *                      @OA\Property(property="extraInfo", type="object"),
 *                      @OA\Property(property="payments", type="object"),
 *                  ),
 *              ),
 *              @OA\Property(property="location", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="rooms", type="object",
 *                  @OA\Property(property="photos", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="name", type="string", format="uuid"),
 *                          @OA\Property(property="photos", type="string", format="uuid"),
 *                      ),
 *                  ),
 *              ),
 *              @OA\Property(property="bundles", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object", format="uuid"),
 *              ),
 *              @OA\Property(property="webBackground", type="object",
 *                  @OA\Property(property="name", type="string", format="uuid"),
 *                  @OA\Property(property="fileUrl", type="string", format="uuid"),
 *              ),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractHotelAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Hotel $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getInt('starCount'),
            $paramsExtractor->getSimpleArray('address'),
            $paramsExtractor->getString('type')
        );

        if ($paramsExtractor->has('description')) {
            $command->setDescription($paramsExtractor->getSimpleArray('description'));
        }

        if ($paramsExtractor->has('location')) {
            $command->setLocation($paramsExtractor->getString('location'));
        }

        if ($paramsExtractor->has('logoSvg')) {
            $command->setLogoSvg($paramsExtractor->getString('logoSvg'));
        }

        if ($paramsExtractor->has('logo')) {
            $command->setLogo($paramsExtractor->getString('logo'));
        }
        
        if ($paramsExtractor->has('links')) {
            $command->setLinks($paramsExtractor->getSimpleArray('links'));
        }

        if ($paramsExtractor->has('lat')) {
            $command->setLat($paramsExtractor->getFloat('lat'));
        }

        if ($paramsExtractor->has('lon')) {
            $command->setLon($paramsExtractor->getFloat('lon'));
        }

        if ($paramsExtractor->has('brand')) {
            $command->setBrand($paramsExtractor->getString('brand'));
        }

        if ($paramsExtractor->has('tags')) {
            $tags = [];

            foreach ($paramsExtractor->getArray('tags') as $tag) {
                $tagDto = new TagDto($tag->getSimpleArray('name'));
                $tagDto->setColorText($tag->getStringOrNull('colorText'));
                $tagDto->setColorBorder($tag->getStringOrNull('colorBorder'));
                $tags[] = $tagDto;
            }

            $command->setTags($tags);
        }

        if ($paramsExtractor->has('places')) {
            $command->setPlaces($paramsExtractor->getSimpleArray('places'));
        }

        if ($paramsExtractor->has('checkInTime')) {
            $command->setCheckInTime($paramsExtractor->getSimpleArray('checkInTime'));
        }

        if ($paramsExtractor->has('checkOutTime')) {
            $command->setCheckOutTime($paramsExtractor->getSimpleArray('checkOutTime'));
        }

        if ($paramsExtractor->has('cancelPrepayment')) {
            $command->setCancelPrepayment($paramsExtractor->getSimpleArray('cancelPrepayment'));
        }

        if ($paramsExtractor->has('extraBeds')) {
            $command->setExtraBeds($paramsExtractor->getSimpleArray('extraBeds'));
        }

        if ($paramsExtractor->has('pets')) {
            $command->setPets($paramsExtractor->getSimpleArray('pets'));
        }

        if ($paramsExtractor->has('extraInfo')) {
            $command->setExtraInfo($paramsExtractor->getSimpleArray('extraInfo'));
        }

        if ($paramsExtractor->has('payments')) {
            $command->setPayments($paramsExtractor->getSimpleArray('payments'));
        }

        if ($paramsExtractor->has('hotelPhoto')) {
            $command->setHotelPhoto($paramsExtractor->getString('hotelPhoto'));
        }

        return $command;
    }
}
