<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Hotel;

use App\Hotel\Dto\SearchDto;
use App\UI\Http\ParamsExtractor;
use DateTimeImmutable;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

/**
 * SearchFilterAction.
 *
 * @OA\Get(
 *     path="/v2/hotel/search-filter",
 *     tags={"Отели"},
 *     description="Поиск по отелям",
 *     @OA\Parameter(
 *          required=false,
 *          name="dateFrom",
 *          in="query",
 *         @OA\Schema(
 *            type="string",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="dateTo",
 *          in="query",
 *         @OA\Schema(
 *            type="string",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="adultCount",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="childCount",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="childAges[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="integer"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="starCounts[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="integer"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="hotelTypes[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="string", format="uuid"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="foodTypes[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="string", format="uuid"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="bundles[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="string", format="uuid"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="hotelBrands[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items(type="string", format="uuid"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="facilities[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="string", format="uuid"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="bedTypes[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="string", format="uuid"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="priceFrom",
 *          in="query",
 *         @OA\Schema(type="number"),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="priceTo",
 *          in="query",
 *         @OA\Schema(type="number"),
 *     ),
 * @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="starCount", type="number"),
 *              @OA\Property(property="lat", type="number"),
 *              @OA\Property(property="lon", type="number"),
 *              @OA\Property(property="links", type="object"),
 *              @OA\Property(property="logo", type="string", format="uuid"),
 *              @OA\Property(property="logoSvg", type="string", format="uuid"),
 *              @OA\Property(property="hotelPhoto", type="string", format="uuid"),
 *              @OA\Property(property="video", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="youtube", type="string"),
 *                      @OA\Property(property="background", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="places", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="tags", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="icon",
 *                          @OA\Property(property="name", type="string"),
 *                          @OA\Property(property="fileUrl", type="string"),
 *                      ),
 *                      @OA\Property(property="iconSvg",
 *                          @OA\Property(property="name", type="string"),
 *                          @OA\Property(property="fileUrl", type="string"),
 *                      ),
 *                      @OA\Property(property="colorText", type="string"),
 *                      @OA\Property(property="colorBorder", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="terms_of_placement", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="checkInTime", type="object"),
 *                      @OA\Property(property="checkOutTime", type="object"),
 *                      @OA\Property(property="cancelPrepayments", type="object"),
 *                      @OA\Property(property="extraBeds", type="object"),
 *                      @OA\Property(property="pets", type="object"),
 *                      @OA\Property(property="extraInfo", type="object"),
 *                      @OA\Property(property="payments", type="object"),
 *                  ),
 *              ),
 *              @OA\Property(property="location", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="rooms", type="object",
 *                  @OA\Property(property="photos", type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="name", type="string", format="uuid"),
 *                          @OA\Property(property="photos", type="string", format="uuid"),
 *                      ),
 *                  ),
 *                  @OA\Property(property="minMainPlaceValue", type="number"),
 *                  @OA\Property(property="minValue", type="number"),
 *                  @OA\Property(property="hasOtherRooms", type="boolean"),
 *              ),
 *              @OA\Property(property="bundles", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object", format="uuid"),
 *              ),
 *              @OA\Property(property="webBackground", type="object",
 *                  @OA\Property(property="name", type="string", format="uuid"),
 *                  @OA\Property(property="fileUrl", type="string", format="uuid"),
 *              ),
 *          )
 *      )
 *  )
 * )
 *
 * @param Request  $request
 * @param Response $response
 * @param array    $args
 *
 * @throws \App\Exception\AppErrorsException
 * @return Response
 */
class SearchFilterAction extends AbstractHotelAction
{

    public function handle(ServerRequestInterface $request): Response
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);

//        $packetId = $request->getQueryParams()['packet'] ?? null;

        $searchDto = new SearchDto();
        if ($paramsExtractor->has('childAges')) {
            $searchDto->setChildAges($paramsExtractor->getSimpleArray('childAges'));
        }
        if ($paramsExtractor->has('childCount')) {
            $searchDto->setChildCount($paramsExtractor->getInt('childCount'));
        }
        if ($paramsExtractor->has('adultCount')) {
            $searchDto->setAdultCount($paramsExtractor->getInt('adultCount'));
        }
        if ($paramsExtractor->has('dateTo')) {
            $searchDto->setDateTo($paramsExtractor->getString('dateTo'));
        }
        if ($paramsExtractor->has('dateFrom')) {
            $searchDto->setDateFrom($paramsExtractor->getString('dateFrom'));
        }
        if ($paramsExtractor->has('starCounts')) {
            $searchDto->setStarCounts($paramsExtractor->getSimpleArray('starCounts'));
        }
        if ($paramsExtractor->has('hotelTypes')) {
            $searchDto->setHotelTypes($paramsExtractor->getSimpleArray('hotelTypes'));
        }
        if ($paramsExtractor->has('bundles')) {
            $searchDto->setBundles($paramsExtractor->getSimpleArray('bundles'));
        }
        if ($paramsExtractor->has('foodTypes')) {
            $searchDto->setFoodTypes($paramsExtractor->getSimpleArray('foodTypes'));
        }
        if ($paramsExtractor->has('hotelBrands')) {
            $searchDto->setBrands($paramsExtractor->getSimpleArray('hotelBrands'));
        }
        if ($paramsExtractor->has('facilities')) {
            $searchDto->setFacilities($paramsExtractor->getSimpleArray('facilities'));
        }
        if ($paramsExtractor->has('bedTypes')) {
            $searchDto->setBedTypes($paramsExtractor->getSimpleArray('bedTypes'));
        }
        if ($paramsExtractor->has('priceFrom')) {
            $searchDto->setPriceFrom($paramsExtractor->getInt('priceFrom'));
        }
        if ($paramsExtractor->has('priceTo')) {
            $searchDto->setPriceTo($paramsExtractor->getInt('priceTo'));
        }

        $this->validator->validate($searchDto);

        $models = $this->hotelRepository->searchHotels($searchDto);


        $dateFrom = $paramsExtractor->getStringOrNull('dateFrom');
        $dateTo = $paramsExtractor->getStringOrNull('dateTo');
        $priceFrom = $paramsExtractor->getIntOrNull('priceFrom');
        $priceTo = $paramsExtractor->getIntOrNull('priceTo');

        $data = array_map([$this, 'serializeHotelList'], $models);

        $data = $this->filter->do(
            $data,
            $models,
            $dateFrom !== null ? new DateTimeImmutable($dateFrom) : null,
            $dateTo !== null ? new DateTimeImmutable($dateTo) : null,
            $priceFrom,
            $priceTo
        );


        return $this->asJson($data);
    }
}
