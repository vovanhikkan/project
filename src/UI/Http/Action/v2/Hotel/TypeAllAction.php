<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Hotel;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

/**
 * TypeAllAction.
 *
 * @OA\Get(
 *     path="/v2/hotel/type/all",
 *     tags={"Отели"},
 *     description="Список всех типов отелей",
 *     @OA\Response(response="400", ref="#/components/responses/BadRequest"),
 *     @OA\Response(response="401", ref="#/components/responses/Unauthorized"),
 *     @OA\Response(response="500", ref="#/components/responses/ServerError"),
 * )
 *
 * @param Request  $request
 * @param Response $response
 * @param array    $args
 *
 * @throws \App\Exception\AppErrorsException
 * @return Response
 */
class TypeAllAction extends AbstractHotelAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->hotelTypeRepository->fetchAll();
        $data = array_map([$this, 'serializeHotelTypeList'], $items);

        return $this->asJson($data);
    }
}
