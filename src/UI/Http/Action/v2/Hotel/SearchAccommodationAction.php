<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Hotel;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\Tag;
use App\Hotel\Dto\SearchAccommodationDto;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\FoodType\FoodType;
use App\Storage\Model\File\File;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;
use DateTimeImmutable;

/**
 * SearchAccommodationAction.
 *
 * @OA\Get(
 *     path="/v2/hotel/{id}/search-accommodation",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Отели"},
 *     description="Поиск по отелям",
 *     @OA\Parameter(
 *          required=false,
 *          name="dateFrom",
 *          in="query",
 *         @OA\Schema(
 *            type="string",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="dateTo",
 *          in="query",
 *         @OA\Schema(
 *            type="string",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="canBeCanceled",
 *          in="query",
 *         @OA\Schema(
 *            type="int",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="adultCount",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="childCount",
 *          in="query",
 *         @OA\Schema(
 *            type="integer",
 *         ),
 *      ),
 *     @OA\Parameter(
 *          required=false,
 *          name="childAges[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="integer"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="foodTypes[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="string", format="uuid"),
 *         ),
 *     ),
 *     @OA\Parameter(
 *          required=false,
 *          name="facilities[]",
 *          in="query",
 *         @OA\Schema(
 *            type="array",
 *            @OA\Items( type="string", format="uuid"),
 *         ),
 *     ),
 * @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="area", type="number"),
 *              @OA\Property(property="mainPlaceCount", type="number"),
 *              @OA\Property(property="extraPlaceCount", type="number"),
 *              @OA\Property(property="zeroPlaceCount", type="number"),
 *              @OA\Property(property="photos", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="string"),
 *                      @OA\Property(property="fileUrl", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="bedTypes", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="object"),
 *                  ),
 *              ),
 *              @OA\Property(property="ratePlans", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="description", type="object"),
 *                      @OA\Property(property="extraCondition", type="object"),
 *                      @OA\Property(property="foodType", type="object"),
 *                      @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *                      @OA\Property(property="mainPlaceValue", type="number"),
 *                      @OA\Property(property="value", type="number"),
 *                      @OA\Property(property="roomsTotal", type="number"),
 *                      @OA\Property(property="roomsLeft", type="number"),
 *                  ),
 *              ),
 *              @OA\Property(property="facilities", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="icon", type="object"),
 *                      @OA\Property(property="items", type="array",
 *                          @OA\Items(
 *                              @OA\Property(property="id", type="string", format="uuid"),
 *                              @OA\Property(property="icon", type="object"),
 *                              @OA\Property(property="name", type="object"),
 *                          ),
 *                      ),
 *                  ),
 *              ),
 *              @OA\Property(property="tags", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="icon",
 *                          @OA\Property(property="name", type="string"),
 *                          @OA\Property(property="fileUrl", type="string"),
 *                      ),
 *                      @OA\Property(property="iconSvg", type="string"),
 *                      @OA\Property(property="colorText", type="string"),
 *                      @OA\Property(property="colorBorder", type="string"),
 *                  ),
 *              ),
 *              @OA\Property(property="lastOrder", type="string", format="date"),
 *          )
 *      )
 *  )
 * )
 *
 * @param Request  $request
 * @param Response $response
 * @param array    $args
 *
 * @throws \App\Exception\AppErrorsException
 * @return Response
 */
class SearchAccommodationAction extends AbstractHotelAction
{

    public function handle(ServerRequestInterface $request): Response
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);

        $searchDto = new SearchAccommodationDto(
            $this->resolveArg('id')
        );

        $searchParametersExist = false;

        if ($paramsExtractor->has('adultCount')) {
            $searchDto->setAdultCount($paramsExtractor->getInt('adultCount'));
            $searchParametersExist = true;
        }
        if ($paramsExtractor->has('childCount')) {
            $searchDto->setChildCount($paramsExtractor->getInt('childCount'));
            $searchParametersExist = true;
        }
        if ($paramsExtractor->has('childAges')) {
            $searchDto->setChildAges($paramsExtractor->getSimpleArray('childAges'));
            $searchParametersExist = true;
        }
        if ($paramsExtractor->has('dateTo')) {
            $dateTo = $paramsExtractor->getString('dateTo');
            $searchParametersExist = true;
        }
        if ($paramsExtractor->has('dateFrom')) {
            $dateFrom = $paramsExtractor->getString('dateFrom');
            $searchParametersExist = true;
        }

        $this->validator->validate($searchDto);

        $models = $this->roomRepository->searchAccommodation($searchDto);

        $roomFilteredData = [];
        if (!$searchParametersExist) {
            $finalData = array_map([$this, 'serializeSearchAccommodationRoomList'], $models);
        } else {
            for ($i = 0; $i < 2; $i++) {
                if ($i == 0) {
                    $canBeCanceled = true;
                } else {
                    $canBeCanceled = false;
                }
                /** @var FoodType $foodType */
                foreach ($this->foodTypeRepository->fetchAll() as $foodType) {
                    $data = array_map([$this, 'serializeSearchAccommodationRoomList'], $models);

                    $filterResult = $this->filterAccommodation->do(
                        $data,
                        $models,
                        $canBeCanceled,
                        $foodType,
                        $dateFrom !== null ? new DateTimeImmutable($dateFrom) : null,
                        $dateTo !== null ? new DateTimeImmutable($dateTo) : null,
                    );

                    if (!empty($filterResult)) {
                        foreach ($filterResult as $roomResult) {
                            if (!isset($roomFilteredData[$roomResult['id']])) {
                                $roomFilteredData[$roomResult['id']] = $roomResult;
                            } else {
                                $tmpRatePlans = array_merge($roomFilteredData[$roomResult['id']]['ratePlans'], $roomResult['ratePlans']);
                                $roomFilteredData[$roomResult['id']]['ratePlans'] = $tmpRatePlans;
                            }
                        }
                    }
                }
            }
        }
        if ($dateFrom !== null && $dateTo !== null) {
            $finalData = $this->serializeSearchResultWithDates($roomFilteredData);
        }

        return $this->asJson($finalData);
    }

    private function serializeSearchResultWithDates(array $rooms) : array
    {
        $serializer = $this->serializer;

        $result = [];
        foreach ($rooms as $room) {
            $tmp = [];
            $tmp['id'] = $room['id'];
            $tmp['name'] = $room['name'];
            $tmp['description'] = $room['description'];
            $tmp['area'] = $room['area'];
            $tmp['mainPlaceCount'] = $room['mainPlaceCount'];
            $tmp['extraPlaceCount'] = $room['extraPlaceCount'];
            $tmp['zeroPlaceCount'] = $room['zeroPlaceCount'];

            $tmp['photos'] = array_map(
                function (File $photo) {
                    return $this->getFileData($photo);
                },
                $room['photos']
            );

            $tmp['bedTypes'] = array_map(
                function (BedType $bedType) {
                    $tmpBedType['id'] = $bedType->getId()->getValue();
                    $tmpBedType['name'] = $bedType->getName()->getValue();
                    $tmpBedType['mobileIcon'] = $this->getFileData($bedType->getMobileIcon());
                    $tmpBedType['webIcon'] = $this->getFileData($bedType->getWebIcon());
                    return $tmpBedType;
                },
                $room['bedTypes']
            );


            foreach ($room['ratePlans'] as $key => $ratePlan) {
                $tmpRatePlan = [];
                $tmpRatePlan['id'] = $key;
                $tmpRatePlan['name'] = $ratePlan['name'];
                $tmpRatePlan['description'] = $ratePlan['description'];
                $tmpRatePlan['extraCondition'] = $ratePlan['extraCondition'];
                $tmpRatePlan['foodType'] = $ratePlan['foodType'];
                $tmpRatePlan['canBeCanceledDate'] = $ratePlan['canBeCanceledDate'];
                $tmpRatePlan['roomsTotal'] = $ratePlan['roomsTotal'];
                $tmpRatePlan['roomsLeft'] = $ratePlan['roomsLeft'];
                $tmpRatePlan['mainPlaceValue'] = $ratePlan['prices']['mainPlaceValue'];
                $tmp['ratePlans'][] = $tmpRatePlan;
            }

            $tmpFacilities = [];
            /** @var Facility $facility */
            foreach ($room['facilities'] as $facility) {
                $group = $facility->getFacilityGroup();
                if (isset($tmpFacilities[$group->getId()->getValue()])) {
                    $tmpFacilities[$group->getId()->getValue()]['items'][] = [
                        'id' => $serializer->asUuid($facility->getId()),
                        'name' => $serializer->asArray($facility->getName()),
                        'icon' => $this->getFileData($facility->getWebIcon())
                    ];
                } else {
                    $tmpFacilities[$group->getId()->getValue()]['id'] = $group->getId()->getValue();
                    $tmpFacilities[$group->getId()->getValue()]['name'] = $group->getName()->getValue();
                    $tmpFacilities[$group->getId()->getValue()]['icon'] = $this->getFileData($group->getWebIcon());
                    $tmpFacilities[$group->getId()->getValue()]['items'][] = [
                        'id' => $serializer->asUuid($facility->getId()),
                        'icon' => $this->getFileData($facility->getWebIcon()),
                        'name' => $serializer->asArray($facility->getName()),
                    ];
                }
            }

            $tmp['facilities'] = [];
            foreach ($tmpFacilities as $facility) {
                $tmp['facilities'][] = $facility;
            }

            $tmp['tags'] = array_map(
                function (Tag $tag) use ($serializer) {
                    return [
                        'name' =>  $serializer->asArray($tag->getName()),
                        'icon' =>  $this->getFileData($tag->getIcon()),
                        'iconSvg' =>  $serializer->asTextOrNull($tag->getIconSvg()),
                        'colorText' =>  $serializer->asStringOrNull($tag->getColorText()),
                        'colorBorder' =>  $serializer->asStringOrNull($tag->getColorBorder()),
                    ];
                },
                $room['tags']
            );

            $tmp['lastOrder'] = $room['lastOrder'];

            $result[] = $tmp;
        }

        return $result;
    }
}
