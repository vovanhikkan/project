<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Hotel;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Model\BundleItem\BundleItem;
use App\Content\Model\Category\Category;
use App\Content\Model\Tag\Tag;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\HotelFacility\HotelFacility;
use App\Hotel\Model\HotelType\HotelType;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\Room\Room;
use App\Hotel\Model\RoomMainPlaceVariant\RoomMainPlaceVariant;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Service\Hotel\FilterBundle;
use App\Hotel\Service\Hotel\FilterReview;
use App\Review\Model\Review\Review;
use App\Review\Model\ReviewStar\ReviewStar;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Repository\HotelTypeRepository;
use App\Hotel\Repository\RoomRepository;
use App\Hotel\Service\Hotel\Filter;
use App\Hotel\Service\Hotel\FilterAccommodation;
use App\Hotel\Service\Hotel\PriceCalculator;
use App\Storage\Model\File\File;
use App\Storage\Model\Video\Video;
use App\Place\Model\Place\Place;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractHotelAction
 * @package App\UI\Http\Action\v2\Hotel
 */
abstract class AbstractHotelAction extends AbstractAction
{

    protected HotelRepository $hotelRepository;
    protected RoomRepository $roomRepository;
    protected FilterAccommodation $filterAccommodation;
    protected PriceCalculator $priceCalculator;
    protected Filter $filter;
    protected HotelTypeRepository $hotelTypeRepository;
    protected FoodTypeRepository $foodTypeRepository;
    protected FilterReview $filterReview;
    protected FilterBundle $filterBundle;

    public function __construct(
        HotelRepository $hotelRepository,
        RoomRepository $roomRepository,
        FilterAccommodation $filterAccommodation,
        PriceCalculator $priceCalculator,
        Filter $filter,
        FilterReview $filterReview,
        HotelTypeRepository $hotelTypeRepository,
        FoodTypeRepository $foodTypeRepository,
        FilterBundle $filterBundle,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->hotelRepository = $hotelRepository;
        $this->roomRepository = $roomRepository;
        $this->filterAccommodation = $filterAccommodation;
        $this->priceCalculator = $priceCalculator;
        $this->filter = $filter;
        $this->hotelTypeRepository = $hotelTypeRepository;
        $this->foodTypeRepository = $foodTypeRepository;
        $this->filterReview = $filterReview;
        $this->filterBundle = $filterBundle;
    }

    protected function serializeCountResult(array $countResult): array
    {
        $result = [];
        foreach ($countResult['hotelType'] as $key => $hotelType) {
            $tmp = [];
            $tmp['id'] = $key;
            $tmp['count'] = $hotelType['count'];
            $tmp['name'] = $hotelType['name'];
            $result['hotelType'][] = $tmp;
        }

        foreach ($countResult['facility'] as $key => $facility) {
            $tmp = [];
            $tmp['id'] = $key;
            $tmp['count'] = $facility['count'];
            $tmp['name'] = $facility['name'];
            $result['facility'][] = $tmp;
        }

        foreach ($countResult['foodType'] as $key => $foodType) {
            $tmp = [];
            $tmp['id'] = $key;
            $tmp['count'] = $foodType['count'];
            $tmp['name'] = $foodType['name'];
            $result['foodType'][] = $tmp;
        }

        return $result;
    }

    protected function serializeHotelList(Hotel $model): array
    {
        $locationId = null;
        $locationName = null;

        if ($location = $model->getLocation()) {
            $locationId = $location->getId();
            $locationName = $location->getName();
        }


        $serializer = $this->serializer;

        $logo = $this->getFileData($model->getLogo());
        $logoSvg = $this->getFileData($model->getLogoSvg());
        $hotelPhoto = $this->getFileData($model->getHotelPhoto());

        $result = [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'description' => $serializer->asArrayOrNull($model->getDescription()),
            'address' => $serializer->asArrayOrNull($model->getAddress()),
            'starCount' => $serializer->asInt($model->getStarCount()),
            'lat' => (string) $model->getLat(),
            'lon' => (string) $model->getLon(),
            'links' => $serializer->asArrayOrNull($model->getLinks()),
            'hotelType' => $model->getType() !== null ? $serializer->asArray($model->getType()->getName()) : null,
            'logo' => $logo,
            'logoSvg' => $logoSvg,
            'hotelPhoto' => $hotelPhoto,
            'video' => array_map(
                function (Video $video) use ($serializer) {
                    return [
                        'youtube' =>  $serializer->asString($video->getYoutubeCode()),
                        'background' =>  $this->getFileData($video->getBackgroundImage()),
                    ];
                },
                $model->getVideos()
            ),
            'tags' => array_map(
                function (Tag $tag) use ($serializer) {
                    return [
                        'name' =>  $serializer->asArray($tag->getName()),
                        'icon' =>  $this->getFileData($tag->getIcon()),
                        'iconSvg' =>  $serializer->asTextOrNull($tag->getIconSvg()),
                        'colorText' =>  $serializer->asStringOrNull($tag->getColorText()),
                        'colorBorder' =>  $serializer->asStringOrNull($tag->getColorBorder()),
                    ];
                },
                $model->getTags()
            ),
            'termsOfPlacement' => [
                'checkInTime' => $serializer->asArrayOrNull($model->getCheckInTime()),
                'checkOutTime' => $serializer->asArrayOrNull($model->getCheckOutTime()),
                'cancelPrepayment' => $serializer->asArrayOrNull($model->getCancelPrepayment()),
                'extraBeds' => $serializer->asArrayOrNull($model->getExtraBeds()),
                'pets' => $serializer->asArrayOrNull($model->getPets()),
                'extraInfo' => $serializer->asArrayOrNull($model->getExtraInfo()),
                'payments' => $serializer->asArrayOrNull($model->getPayments())
                ]
        ];

        if ($location) {
            $result['location'] = [
                'id' =>  $serializer->asUuid($locationId),
                'name' => $serializer->asArray($locationName),
            ];
        }


        $result['rooms']['photos'] = [];
        $rooms = $model->getRooms();
        /**
         * @var Room $room
         */

        foreach ($rooms as $room) {
            $result['rooms']['photos'] = array_merge(
                $result['rooms']['photos'],
                array_map(
                    function (File $photo) {
                        return [
                        'name' => $photo->getName()->getValue(),
                        'photo' => $this->locator->getAbsoluteUrl($photo)
                        ];
                    },
                    $room->getPhotos()
                )
            );
        }

        /**
         * @var RatePlan $ratePlan
         */
        $result['bundles'] = [];
        if (count($ratePlans = $model->getRatePlans())) {
            foreach ($ratePlans as $ratePlan) {
                $result['bundles'] = array_merge($result['bundles'], array_map(
                    function (Bundle $bundle) use ($serializer) {
                        $bundleItems = $bundle->getBundleItems();

                        return [
                            'id' =>  $serializer->asUuid($bundle->getId()),
                            'name' =>  $serializer->asArray($bundle->getName()),
                            'webBackground' => $this->getFileData($bundle->getWebBackground()),
                            'bundleItems' => $bundleItems !== null ? array_map(
                                function (BundleItem $bundleItem) use ($serializer) {
                                    $place = $bundleItem->getPlace();

                                    return [
                                        'id' => $serializer->asUuid($bundleItem->getId()),
                                        'name' => $serializer->asArray($bundleItem->getName()),
                                        'subName' => $serializer->asArray($bundleItem->getSubName()),
                                        'description' => $serializer->asArray($bundleItem->getDescription()),
                                        'widthType' => $bundleItem->getWidthType() !== null ? $bundleItem->getWidthType()->getValue() : null,
                                        'placeId' => $serializer->asUuid($place->getId()),
                                        'place' => $place !== null ? array_map(
                                            function (Category $category) use ($serializer) {
                                                return [
                                                    'categoryId' => $serializer->asUuid($category->getId()),
                                                    'webIcon' =>  $this->getFileData($category->getWebIcon()),
                                                    'mobIcon' =>  $this->getFileData($category->getMobileIcon()),
                                                    'webIconSvg' => $this->getFileData($category->getWebIconSvg()),
                                                    'webGradientColor1' => $serializer->asStringOrNull(
                                                        $category->getWebGradientColor1()
                                                    ),
                                                    'webGradientColor2' => $serializer->asStringOrNull(
                                                        $category->getWebGradientColor2()
                                                    )
                                                ];
                                            },
                                            $place->getCategories()
                                        ) : []
                                    ];
                                },
                                $bundleItems
                            ) : [],
                        ];
                    },
                    $ratePlan->getBundles()
                ));
            }
        }

        $facilities =  $model->getFacilities();
        /**
         * @var Facility $facility
         */
        $result['facilities'] = [];
        foreach ($facilities as $facility) {
            $group = $facility->getFacilityGroup();
            if (isset($result['facilities'][$group->getId()->getValue()])) {
                $result['facilities'][$group->getId()->getValue()]['items'][] = [
                    'id' => $serializer->asUuid($facility->getId()),
                    'name' => $serializer->asArray($facility->getName()),
                    'icon' => $this->getFileData($facility->getWebIcon())
                ];
            } else {
                $result['facilities'][$group->getId()->getValue()]['name'] = $group->getName()->getValue();
                $result['facilities'][$group->getId()->getValue()]['icon'] = $this->getFileData($group->getWebIcon());
                $result['facilities'][$group->getId()->getValue()]['items'][] = [
                    'id' => $serializer->asUuid($facility->getId()),
                    'icon' => $this->getFileData($facility->getWebIcon()),
                    'name' => $serializer->asArray($facility->getName()),
                ];
            }
        }

        return $result;
    }


    protected function serializeItem(Hotel $model): array
    {
        $serializer = $this->serializer;

        $facilities =  $model->getFacilities();
        /**
         * @var Facility $facility
         */
        $result = [];
        foreach ($facilities as $facility) {
            $group = $facility->getFacilityGroup();
            if (isset($result[$group->getId()->getValue()])) {
                $result[$group->getId()->getValue()]['items'][] = [
                    'id' => $serializer->asUuid($facility->getId()),
                    'name' => $serializer->asArray($facility->getName()),
                    'icon' => $this->getFileData($facility->getWebIcon())
                ];
            } else {
                $result[$group->getId()->getValue()]['name'] = $group->getName()->getValue();
                $result[$group->getId()->getValue()]['icon'] = $this->getFileData($group->getWebIcon());
                $result[$group->getId()->getValue()]['items'][] = [
                    'id' => $serializer->asUuid($facility->getId()),
                    'icon' => $this->getFileData($facility->getWebIcon()),
                    'name' => $serializer->asArray($facility->getName()),
                ];
            }
        }

        return array_merge($this->serializeHotelList($model), [
            'facilities' => $result,
            'places' => $this->serializePlaceList($model),
            'reviews' => $this->serializeReviewList($model)
        ]);
    }


    protected function serializeReviewList(Hotel $model) : array
    {
        $result = [];

        if (count($reviews = $model->getReviews())) {
            foreach ($reviews as $review) {
                $result[$review->getId()->getValue()] = [
                    'stars' => array_map(
                        function (ReviewStar $reviewStar) {
                            return [
                                'reviewStarTypeId' => $reviewStar->getId()->getValue(),
                                'value' => $reviewStar->getValue()->getValue()
                            ];
                        },
                        $review->getReviewStars()
                    ),
                ];
            }
        }

        return $result;
    }


    protected function serializePlaceList(Hotel $model) : array
    {
        $serializer = $this->serializer;

        $places =  $model->getPlaces();
        /**
         * @var Place $place
         */
        $result = [];
        foreach ($places as $place) {
            $place->getWorksFrom() ? $worksFrom = $place->getWorksFrom()->format("H:i") : $worksFrom = null;
            $place->getWorksTo() ? $worksTo = $place->getWorksTo()->format("H:i") : $worksTo = null;
            $result[$place->getId()->getValue()] = [
                'name' =>  $serializer->asArray($place->getTitle()),
                'workingHoursDescription' =>  $serializer->asArray($place->getWorkingHoursDescription()),
                'worksFrom' => $worksFrom,
                'worksTo' => $worksTo,
                'minPrice' =>  $serializer->asIntOrNull($place->getMinPrice()),
                'background' => $this->getFileData($place->getBackground())
            ];

            $categories = $place->getCategories();
            /**
             * @var Category $category
             */
            foreach ($categories as $category) {
                $webGradientColor1 = $category->getWebGradientColor1() !== null ?
                    $category->getWebGradientColor1()->getValue() : null;
                $webGradientColor2 = $category->getWebGradientColor2() !== null ?
                    $category->getWebGradientColor2()->getValue() : null;
                
                $result[$place->getId()->getValue()]['categories'][] = [
                    'webIcon' =>  $this->getFileData($category->getMobileIcon()),
                    'mobileIcon' =>  $this->getFileData($category->getMobileIcon()),
                    'webGradientColor1' => $webGradientColor1,
                    'webGradientColor2' => $webGradientColor2,
                    'mobileBackground' =>  $this->getFileData($category->getMobileBackground())
                ];
            }
        }

        return $result;
    }


    protected function serializeRoomList(Room $model): array
    {

        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'description' => $serializer->asArray($model->getDescription()),
            'mainPlaceCount' => $serializer->asInt($model->getMainPlaceCount()),
            'extraPlaceCount' => $serializer->asInt($model->getExtraPlaceCount()),
            'zeroPlaceCount' => $serializer->asInt($model->getZeroPlaceCount()),
            'roomMainPlaceVariant' => array_map(
                function (RoomMainPlaceVariant $roomMainPlaceVariant) {
                    return [
                        'value' => $roomMainPlaceVariant->getValue()->getValue(),
                    ];
                },
                $model->getRoomMainPlaceVariant()
            ),
        ];
    }


    protected function serializeSearchAccommodationRoomList(Room $model): array
    {

        $serializer = $this->serializer;

        $result = [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'description' => $serializer->asArray($model->getDescription()),
            'area' => $serializer->asString($model->getArea()),
            'mainPlaceCount' => $serializer->asInt($model->getMainPlaceCount()),
            'extraPlaceCount' => $serializer->asInt($model->getExtraPlaceCount()),
            'zeroPlaceCount' => $serializer->asInt($model->getZeroPlaceCount()),
        ];

        $result['photos'] = array_map(
            function (File $photo) {
                return [
                    'name' => $photo->getName()->getValue(),
                    'photo' => $photo->getFilePath()->getValue()
                ];
            },
            $model->getPhotos()
        );

        $result['bedTypes'] = array_map(
            function (BedType $bedType) {
                $tmpBedType['name'] = $bedType->getName()->getValue();
                $tmpBedType['mobileIcon'] = $this->getFileData($bedType->getMobileIcon());
                $tmpBedType['webIcon'] = $this->getFileData($bedType->getWebIcon());
                return $tmpBedType;
            },
            $model->getHotel()->getBedTypes()
        );

        $result['tags'] = array_map(
            function (Tag $tag) use ($serializer) {
                return [
                    'name' =>  $serializer->asArray($tag->getName()),
                    'icon' =>  $this->getFileData($tag->getIcon()),
                    'iconSvg' =>  $serializer->asTextOrNull($tag->getIconSvg()),
                    'colorText' =>  $serializer->asStringOrNull($tag->getColorText()),
                    'colorBorder' =>  $serializer->asStringOrNull($tag->getColorBorder()),
                ];
            },
            $model->getTags()
        );


        $tmpFacilities = [];
        /** @var Facility $facility */
        foreach ($model->getFacilities() as $facility) {
            $group = $facility->getFacilityGroup();
            if (isset($tmpFacilities[$group->getId()->getValue()])) {
                $tmpFacilities[$group->getId()->getValue()]['items'][] = [
                    'id' => $serializer->asUuid($facility->getId()),
                    'name' => $serializer->asArray($facility->getName()),
                    'icon' => $this->getFileData($facility->getWebIcon())
                ];
            } else {
                $tmpFacilities[$group->getId()->getValue()]['id'] = $group->getId()->getValue();
                $tmpFacilities[$group->getId()->getValue()]['name'] = $group->getName()->getValue();
                $tmpFacilities[$group->getId()->getValue()]['icon'] = $this->getFileData($group->getWebIcon());
                $tmpFacilities[$group->getId()->getValue()]['items'][] = [
                    'id' => $serializer->asUuid($facility->getId()),
                    'icon' => $this->getFileData($facility->getWebIcon()),
                    'name' => $serializer->asArray($facility->getName()),
                ];
            }
        }

        $result['facilities'] = [];
        foreach ($tmpFacilities as $facility) {
            $result['facilities'][] = $facility;
        }


        return $result;
    }
    


    protected function serializeHotelTypeList(HotelType $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
        ];
    }
}
