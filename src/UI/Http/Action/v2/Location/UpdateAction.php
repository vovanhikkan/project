<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Location;

use App\Location\Command\Location\Update\Command;
use App\Location\Model\Location\Location;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *     path="/v2/location/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Локации"},
 *     security={{"bearerAuth":{}}},
 *     description="Редактирование локации",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="altitude", type="number")
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="altitude", type="number")
 *              ),
 *          ),
 *     )
 * )
 */
class UpdateAction extends AbstractLocationAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Location $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getInt('altitude')
        );
    }
}
