<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Location;

use App\Location\Command\Location\Create\Command;
use App\Location\Model\Location\Location;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/location",
 *     tags={"Локации"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление локации",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="altitude", type="number")
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="altitude", type="number")
 *              ),
 *          ),
 *     )
 * )
 */
class CreateAction extends AbstractLocationAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Location $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getInt('altitude')
        );
    }
}
