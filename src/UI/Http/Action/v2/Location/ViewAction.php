<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Location;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/location/{id}",
 *  tags={"Локации"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр локации",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="altitude", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractLocationAction
{
   
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->locationRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
