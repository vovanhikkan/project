<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Location;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * IndexAction.
 *
 * @OA\Get(
 *     path="/v2/location",
 *     tags={"Локации"},
 *     description="Список локаций",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="altitude", type="number")
 *              ),
 *          ),
 *     )
 * )
 */
class IndexAction extends AbstractLocationAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->locationRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
