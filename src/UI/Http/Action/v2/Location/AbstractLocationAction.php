<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Location;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Location\Model\Location\Location;
use App\Location\Repository\LocationRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractLocationAction
 * @package App\UI\Http\Action\v2\Location
 */
abstract class AbstractLocationAction extends AbstractAction
{
    protected LocationRepository $locationRepository;

    public function __construct(
        LocationRepository $locationRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->locationRepository = $locationRepository;
    }

    protected function serializeList(Location $model): array
    {
        $serializer = $this->serializer;
        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'altitude' => $serializer->asInt($model->getAltitude())
        ];
    }

    protected function serializeItem(Location $model): array
    {
        return $this->serializeList($model);
    }
}
