<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Location;

use App\Location\Command\Location\Delete\Command;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Delete (
 *     path="/v2/location/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Локации"},
 *     security={{"bearerAuth":{}}},
 *     description="Удаление локации",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="message", type="string"),
 *              ),
 *          ),
 *     ),
 * )
 */
class DeleteAction extends AbstractLocationAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);
        $data = $result;

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        return new Command(
            $this->resolveArg('id')
        );
    }
}
