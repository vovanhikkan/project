<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Budget;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\Budget\Budget;
use App\Hotel\Repository\BudgetRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractBudgetAction
 * @package App\UI\Http\Action\v2\Budget
 */
abstract class AbstractBudgetAction extends AbstractAction
{
    protected BudgetRepository $budgetRepository;
    
    public function __construct(
        BudgetRepository $budgetRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->budgetRepository = $budgetRepository;
    }
    
    protected function serializeList(Budget $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(Budget $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'budgetFrom' => $serializer->asInt($model->getBudgetFrom()),
            'budgetTo' => $serializer->asInt($model->getBudgetTo()),
            'sort' => $serializer->asInt($model->getSort())
        ];
    }
}
