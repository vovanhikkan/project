<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Budget;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/budget",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Budget",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="budgetFrom", type="number"),
 *              @OA\Property(property="budgetTo", type="number"),
 *              @OA\Property(property="sort", type="number")
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractBudgetAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if (!$this->getCurrentUser() || !$this->getCurrentUser()->getRole()->isAdmin()) {
            $items = $this->budgetRepository->fetchAllByActive();
        } else {
            $items = $this->budgetRepository->fetchAll();
        }

        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
