<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Budget;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/budget/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр Budget",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="budgetFrom", type="number"),
 *              @OA\Property(property="budgetTo", type="number"),
 *              @OA\Property(property="sort", type="number")
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractBudgetAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->budgetRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
