<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Budget;

use App\Hotel\Command\Budget\Create\Command;
use App\Hotel\Model\Budget\Budget;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/budget",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление Budget",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="budgetFrom", type="number"),
 *              @OA\Property(property="budgetTo", type="number")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="budgetFrom", type="number"),
 *              @OA\Property(property="budgetTo", type="number"),
 *              @OA\Property(property="sort", type="number")
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractBudgetAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Budget $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getInt('budgetFrom'),
            $paramsExtractor->getInt('budgetTo')
        );

        return $command;
    }
}
