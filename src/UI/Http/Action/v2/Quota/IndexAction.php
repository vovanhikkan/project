<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Quota;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * IndexAction.
 *
 * @OA\Get(
 *     path="/v2/quota",
 *     tags={"Квоты"},
 *     description="Список квот",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="groupId", type="number"),
 *                  @OA\Property(property="isDependsOnDate", type="boolean"),
 *                  @OA\Property(property="hasValuesByDays", type="boolean"),
 *                  @OA\Property(property="sort", type="number"),
 *                  @OA\Property(
 *                      property="products",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                      )
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class IndexAction extends AbstractQuotaAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->quotaRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
