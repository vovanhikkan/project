<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Quota;

use App\Product\Command\Quota\Create\Command;
use App\Product\Model\Quota\Quota;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/quota",
 *     tags={"Квоты"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление квоты",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="groupId", type="number"),
 *                  @OA\Property(property="isDependsOnDate", type="boolean"),
 *                  @OA\Property(property="hasValuesByDays", type="boolean"),
 *                  @OA\Property(property="sort", type="number"),
 *                  @OA\Property(
 *                      property="products",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="object"),
 *                  @OA\Property(property="groupId", type="string", format="uuid"),
 *                  @OA\Property(property="isDependsOnDate", type="boolean"),
 *                  @OA\Property(property="hasValuesByDays", type="boolean"),
 *                  @OA\Property(
 *                      property="products",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                      )
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class CreateAction extends AbstractQuotaAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Quota $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getString('groupId'),
            $paramsExtractor->getBool('isDependsOnDate'),
            $paramsExtractor->getBool('hasValuesByDays'),
            $paramsExtractor->getSimpleArray('products')
        );
    }
}
