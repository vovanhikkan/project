<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Quota;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Product\Model\Product\Product;
use App\Product\Model\Quota\Quota;
use App\Product\Repository\QuotaRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractQuotaAction.
 */
abstract class AbstractQuotaAction extends AbstractAction
{
    protected QuotaRepository $quotaRepository;

    public function __construct(
        QuotaRepository $quotaRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->quotaRepository = $quotaRepository;
    }

    protected function serializeList(Quota $model): array
    {
        $serializer = $this->serializer;
        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName())
        ];
    }

    protected function serializeItem(Quota $model): array
    {
        return array_merge($this->serializeList($model), [
            'group' => $model->getGroup()->getId()->getValue(),
            'isDependsOnDate' => $model->isDependsOnDate(),
            'hasValuesByDays' => $model->getHasValuesByDays(),
            'sort' => $model->getSort()->getValue(),
            'products' => array_map(
                function (Product $product) {
                    return [
                        'id' => (string)$product->getId(),
                        'name' => (string)$product->getName()
                    ];
                },
                $model->getProducts()
            )
        ]);
    }
}
