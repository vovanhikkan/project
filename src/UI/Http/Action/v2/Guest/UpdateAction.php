<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Guest;

use App\Hotel\Command\Guest\Update\Command;
use App\Hotel\Model\Guest\Guest;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/guest/{id}",
 *  tags={"Отели"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование Guest",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="firstName", type="string"),
 *              @OA\Property(property="lastName", type="string"),
 *              @OA\Property(property="age", type="number"),
 *              @OA\Property(property="mainGuest", type="boolean"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="firstName", type="string"),
 *              @OA\Property(property="lastName", type="string"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractGuestAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Guest $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command($this->resolveArg('id'));

        if ($paramsExtractor->has('firstName')) {
            $command->setFirstName($paramsExtractor->getString('firstName'));
        }

        if ($paramsExtractor->has('lastName')) {
            $command->setLastName($paramsExtractor->getString('lastName'));
        }

        if ($paramsExtractor->has('age')) {
            $command->setAge($paramsExtractor->getInt('age'));
        }

        if ($paramsExtractor->has('mainGuest')) {
            $command->setMainGuest($paramsExtractor->getBool('mainGuest'));
        }

        return $command;
    }
}
