<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Guest;

use App\Application\Service\Validator;
use App\Auth\Model\User\User;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Repository\GuestRepository;
use App\Order\Model\Order\Order;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractGuestAction
 * @package App\UI\Http\Action\v2\Guest
 */
abstract class AbstractGuestAction extends AbstractAction
{
    protected GuestRepository $guestRepository;

    public function __construct(
        GuestRepository $guestRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->guestRepository = $guestRepository;
    }

    protected function serializeList(Guest $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(Guest $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'firstName' => $serializer->asString($model->getFirstName()),
            'lastName' => $serializer->asString($model->getLastName()),
            'age' => $serializer->asInt($model->getAge()),
            'mainGuest' => $model->isMainGuest(),
            'users' => array_map(
                function (User $user) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($user->getId()),
                        'email' => $user->getEmail()->getValue(),
                    ];
                },
                $model->getUsers()
            ),
            'orders' => array_map(
                function (Order $order) use ($serializer) {
                    return [
                        'id' => $serializer->asUuid($order->getId())
                    ];
                },
                $model->getOrders()
            )
        ];
    }
}
