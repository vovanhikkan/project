<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Guest;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/guest/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр Guest",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="firstName", type="string"),
 *              @OA\Property(property="lastName", type="string"),
 *              @OA\Property(property="age", type="number"),
 *              @OA\Property(property="mainGuest", type="boolean"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractGuestAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->guestRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
