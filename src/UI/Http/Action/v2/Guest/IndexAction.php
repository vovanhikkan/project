<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Guest;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/guest",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Guest",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="firstName", type="string"),
 *              @OA\Property(property="lastName", type="string"),
 *              @OA\Property(property="age", type="number"),
 *              @OA\Property(property="mainGuest", type="boolean"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractGuestAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->guestRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
