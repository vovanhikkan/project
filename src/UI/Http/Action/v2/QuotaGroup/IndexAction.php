<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\QuotaGroup;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * IndexAction.
 *
 * @OA\Get(
 *     path="/v2/quota-group",
 *     tags={"Квоты"},
 *     description="Список квот",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="name", type="string")
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class IndexAction extends AbstractQuotaGroupAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->quotaGroupRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
