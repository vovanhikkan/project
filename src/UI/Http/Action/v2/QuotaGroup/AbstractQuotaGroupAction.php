<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\QuotaGroup;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\Product\Repository\QuotaGroupRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractQuotaGroupAction.
 */
abstract class AbstractQuotaGroupAction extends AbstractAction
{
    protected QuotaGroupRepository $quotaGroupRepository;

    public function __construct(
        QuotaGroupRepository $quotaGroupRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->quotaGroupRepository = $quotaGroupRepository;
    }

    protected function serializeList(QuotaGroup $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(QuotaGroup $model): array
    {
        $serializer = $this->serializer;
        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName())
        ];
    }
}
