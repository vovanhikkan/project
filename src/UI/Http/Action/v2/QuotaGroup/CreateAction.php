<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\QuotaGroup;

use App\Product\Command\QuotaGroup\Create\Command;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/quota-group",
 *     tags={"Квоты"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление группы квот",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *     ),
 *     )
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *          ),
 *     )
 * )
 */
class CreateAction extends AbstractQuotaGroupAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var QuotaGroup $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getSimpleArray('name')
        );
    }
}
