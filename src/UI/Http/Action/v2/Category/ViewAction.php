<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Category;

use App\Application\Service\Validator;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\AuthContext;
use App\Content\Repository\CategoryRepository;
use App\Storage\Service\File\Locator;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * @OA\Get(
 *     path="/v2/category/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Категории"},
 *     description="Просмотр категории",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                      @OA\Property(property="id", type="string"),
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="webGradientColor1", type="string"),
 *                      @OA\Property(property="webGradientColor2", type="string"),
 *                      @OA\Property(property="webIcon", type="string"),
 *                      @OA\Property(property="webIconSvg", type="string"),
 *                      @OA\Property(property="mobileIcon", type="string"),
 *                      @OA\Property(property="mobileBackground", type="string")
 *              ),
 *          ),
 *     )
 * )
 */
class ViewAction extends AbstractCategoryAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->categoryRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
