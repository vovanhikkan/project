<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Category;

use App\Content\Command\Category\Update\Command;
use App\Content\Model\Category\Category;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * UpdateAction.
 *
 * @OA\Patch(
 *     path="/v2/category/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Категории"},
 *     security={{"bearerAuth":{}}},
 *     description="Редактирование категории",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                      @OA\Property(property="id", type="string"),
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="webGradientColor1", type="string"),
 *                      @OA\Property(property="webGradientColor2", type="string"),
 *                      @OA\Property(property="webIcon", type="string"),
 *                      @OA\Property(property="webIconSvg", type="string"),
 *                      @OA\Property(property="mobileIcon", type="string"),
 *                      @OA\Property(property="mobileBackground", type="string")
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="object")
 *              ),
 *          ),
 *     )
 * )
 */
class UpdateAction extends AbstractCategoryAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Category $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getSimpleArray('name')
        );
    }
}
