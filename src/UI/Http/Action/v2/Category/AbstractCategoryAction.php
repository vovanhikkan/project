<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Category;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Content\Model\Category\Category;
use App\Content\Model\Category\CategoryPlace;
use App\Content\Model\Category\CategorySection;
use App\Content\Repository\CategoryRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractCategoryAction
 * @package App\UI\Http\Action\v2\Category
 */
abstract class AbstractCategoryAction extends AbstractAction
{
    protected CategoryRepository $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->categoryRepository = $categoryRepository;
    }

    protected function serializeList(Category $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => (string)$model->getId(),
            'name' => $serializer->asArray($model->getName())
        ];
    }

    protected function serializeItem(Category $model): array
    {
        $serializer = $this->serializer;

        return array_merge($this->serializeList($model), [
            'webGradientColor1' => $serializer->asStringOrNull($model->getWebGradientColor1()),
            'webGradientColor2' => $serializer->asStringOrNull($model->getWebGradientColor2()),
            'webIcon' => $model->getWebIcon() !== null
                ? $this->locator->getAbsoluteUrl($model->getWebIcon())
                : null,
            'webIconSvg' => $this->asSvg($model->getWebIconSvg()),
            'mobileIcon' => $model->getMobileIcon() !== null
                ? $this->locator->getAbsoluteUrl($model->getMobileIcon())
                : null,
            'mobileBackground' => $model->getMobileBackground() !== null
                ? $this->locator->getAbsoluteUrl($model->getMobileBackground())
                : null,
            'places' => array_map(
                function (CategoryPlace $categoryPlace) use ($serializer) {
                    return [
                        'placeId' => $serializer->asUuid($categoryPlace->getPlace()->getId()),
                        'sort' => $serializer->asInt($categoryPlace->getSort())
                    ];
                },
                $model->getCategoryPlaces()
            ),
            'sections' => array_map(
                function (CategorySection $categorySection) use ($serializer) {
                    return [
                        'sectionId' => $serializer->asUuid($categorySection->getSection()->getId()),
                        'sort' => $serializer->asInt($categorySection->getSort())
                    ];
                },
                $model->getCategorySections()
            ),
        ]);
    }
}
