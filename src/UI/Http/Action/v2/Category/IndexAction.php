<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Category;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/category",
 *     tags={"Категории"},
 *     description="Список категорий",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string"),
 *                      @OA\Property(property="name", type="object"),
 *                      @OA\Property(property="webGradientColor1", type="string"),
 *                      @OA\Property(property="webGradientColor2", type="string"),
 *                      @OA\Property(property="webIcon", type="string"),
 *                      @OA\Property(property="webIconSvg", type="string"),
 *                      @OA\Property(property="mobileIcon", type="string"),
 *                      @OA\Property(property="mobileBackground", type="string")
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class IndexAction extends AbstractCategoryAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->categoryRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
