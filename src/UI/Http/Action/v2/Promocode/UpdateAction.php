<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Promocode;

use App\Loyalty\Command\Promocode\Update\Command;
use App\Loyalty\Model\Promocode\Promocode;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *     path="/v2/promocode",
 *     tags={"Акции"},
 *     security={{"bearerAuth":{}}},
 *     description="Изменение промокода",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="value", type="string"),
 *                 @OA\Property(property="action", type="string", format="uuid"),
 *                 @OA\Property(property="totalCount", type="number"),
 *                 @OA\Property(property="holdedCount", type="number"),
 *                 @OA\Property(property="orderedCount", type="number"),
 *                 @OA\Property(property="expiredAt", type="string"),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="value", type="object"),
 *             ),
 *         )
 *     )
 * )
 */
class UpdateAction extends AbstractPromocodeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Promocode $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getString('action'),
            $paramsExtractor->getString('value'),
            $paramsExtractor->getInt('totalCount'),
            $paramsExtractor->getInt('holdedCount'),
            $paramsExtractor->getInt('orderedCount'),
            $paramsExtractor->getInt('expiredAt')
        );
    }
}
