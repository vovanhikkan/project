<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Promocode;

use App\Loyalty\Command\Promocode\Generate\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * GenerateAction.
 * @OA\Get(
 *     path="/v2/promocode/generate",
 *     tags={"Акции"},
 *     description="Сгенирировать промокод",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"action", "totalCount"},
 *                 @OA\Property(property="action", type="string", format="uuid"),
 *                 @OA\Property(property="totalCount", type="number"),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="value", type="object"),
 *             ),
 *         )
 *     )
 * )
 */
class GenerateAction extends AbstractPromocodeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        $result = $this->bus->handle($command);
        $data = array_map([$this, 'serializeList'], $result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('action'),
            $paramsExtractor->getInt('totalCount')
        );
    }
}
