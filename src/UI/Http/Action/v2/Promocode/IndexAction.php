<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Promocode;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/promocode",
 *     tags={"Акции"},
 *     description="Список промокодов",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="id", type="string", format="uuid"),
 *                 @OA\Property(property="value", type="object"),
 *             ),
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractPromocodeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->promocodeRepository->fetchAll();
        $data = array_map([$this, 'serializeList'], $items);

        return $this->asJson($data);
    }
}
