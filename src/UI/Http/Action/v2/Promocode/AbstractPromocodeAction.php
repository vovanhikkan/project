<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Promocode;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Loyalty\Model\Promocode\Promocode;
use App\Loyalty\Repository\PromocodeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractPromocodeAction.
 */
abstract class AbstractPromocodeAction extends AbstractAction
{
    protected PromocodeRepository $promocodeRepository;

    public function __construct(
        PromocodeRepository $promocodeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->promocodeRepository = $promocodeRepository;
    }

    protected function serializeList(Promocode $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(Promocode $model): array
    {
        return [
            'id' => (string)$model->getId(),
            'value' => (string)$model->getValue()
        ];
    }
}
