<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Promocode;

use App\Loyalty\Command\Promocode\Activate\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ActivateAction.
 * @OA\Get(
 *     path="/v2/promocode/activate",
 *     tags={"Акции"},
 *     description="Применить промокод",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"value"},
 *                 @OA\Property(property="value", type="string"),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="message", type="string"),
 *             ),
 *         )
 *     )
 * )
 */
class ActivateAction extends AbstractPromocodeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);
        $result = $this->bus->handle($command);
        $data = $result ? ['message'  => 'Промокод применен'] : ['message'  => 'Данного промокода не существует'];

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('promocode'),
            $this->getCurrentUser()
        );
    }
}
