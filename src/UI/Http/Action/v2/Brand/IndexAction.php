<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Brand;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Repository\BrandRepository;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * @OA\Get(
 *  path="/v2/brand",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Brand",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object")
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractBrandAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->brandRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
