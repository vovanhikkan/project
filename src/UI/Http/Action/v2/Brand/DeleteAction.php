<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Brand;

use App\Hotel\Command\Brand\Delete\Command;
use App\Hotel\Model\Brand\Brand;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Delete(
 *  path="/v2/brand/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Удаление Brand",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="message", type="string")
 *          )
 *      )
 *  )
 * )
 */
class DeleteAction extends AbstractBrandAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        $result = $this->bus->handle($command);
        $data = $result;

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        return new Command(
            $this->resolveArg('id')
        );
    }
}
