<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Brand;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\Brand\Brand;
use App\Hotel\Repository\BrandRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractBrandAction
 * @package App\UI\Http\Action\v2\Brand
 */
abstract class AbstractBrandAction extends AbstractAction
{
    protected BrandRepository $brandRepository;

    public function __construct(
        BrandRepository $brandRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->brandRepository = $brandRepository;
    }

    protected function serializeList(Brand $model): array
    {

        return $this->serializeItem($model);
    }

    protected function serializeItem(Brand $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
        ];
    }
}
