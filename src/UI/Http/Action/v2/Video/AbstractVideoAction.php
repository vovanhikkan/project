<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Video;

use App\Storage\Model\Video\Video;
use App\UI\Http\Action\AbstractAction;

/**
 * AbstractVideoAction.
 */
abstract class AbstractVideoAction extends AbstractAction
{
    
//    protected function serializeList(Video $model): array
//    {
//        return [
//            'id' => (string)$model->getId(),
//            'name' => (string)$model->getName()
//        ];
//    }

    protected function serializeItem(Video $model): array
    {
        return [
            'id' => (string)$model->getId(),
        ];
    }
}
