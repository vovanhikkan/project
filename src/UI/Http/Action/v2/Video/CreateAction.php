<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Video;

use App\Storage\Command\Video\Create\Command;
use App\Storage\Model\Video\Video;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * CreateAction.
 *
 * @OA\Post(
 *     path="/v2/video/upload",
 *     tags={"Видео"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление видео",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(property="backgroundImage", type="string", format="binary"),
 *                 @OA\Property(property="youtubeCode", type="string"),
 *             ),
 *          ),
 *     ),
 * )
 */
class CreateAction extends AbstractVideoAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Video $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('youtubeCode'),
            $request->getUploadedFiles()['backgroundImage']
        );
    }
}
