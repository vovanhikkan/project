<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Product;

use App\Product\Command\Product\Delete\Command;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Delete (
 *     path="/v2/product/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Продукты"},
 *     security={{"bearerAuth":{}}},
 *     description="Удаление продукта",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="message", type="string"),
 *              ),
 *          ),
 *     ),
 * )
 */
class DeleteAction extends AbstractProductAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);
        $data = $result;

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        return new Command(
            $this->resolveArg('id')
        );
    }
}
