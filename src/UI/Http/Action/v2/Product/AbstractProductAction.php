<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Product;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Product\Model\CardType\Type;
use App\Product\Model\Product\Product;
use App\Product\Model\Product\ProductPrice;
use App\Product\Repository\CardTypeRepository;
use App\Product\Repository\ProductRepository;
use App\Product\Repository\QuotaRepository;
use App\Service\Product\AvailableDatesService;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use DateTimeImmutable;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractProductAction.
 */
abstract class AbstractProductAction extends AbstractAction
{
    private CardTypeRepository $cardTypeRepository;
    private AvailableDatesService $availableDatesService;
    protected QuotaRepository $quotaRepository;
    protected ProductRepository $productRepository;

    public function __construct(
        CardTypeRepository $cardTypeRepository,
        AvailableDatesService $availableDatesService,
        QuotaRepository $quotaRepository,
        ProductRepository $productRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->cardTypeRepository = $cardTypeRepository;
        $this->availableDatesService = $availableDatesService;
        $this->quotaRepository = $quotaRepository;
        $this->productRepository = $productRepository;
    }

    protected function serializeList(Product $model): array
    {
        return [
            'id' => (string)$model->getId(),
            'name' => (string)$model->getName()
        ];
    }

    protected function serializeItem(Product $model): array
    {
        $cardTypePrice = null;
        if ($model->getProductSettings() !== null) {
            $cardType = $this->cardTypeRepository->fetchOneByType(
                new Type($model->getProductSettings()->getCardType()->getValue())
            );
            if ($cardType !== null) {
                $cardTypePrice = $cardType->getPrice() !== null ? $cardType->getPrice()->toRoubles() : null;
            }
        }

        $result = array_merge($this->serializeList($model), [
            'sectionId' => (string)$model->getSection()->getId(),
            'price' => $model->getPrice()->getValue(),
            'settings' => $model->getProductSettings() !== null
                ? [
                    'cardType' => $this->serializer->asInt($model->getProductSettings()->getCardType()),
                    'cardTypePrice' => $cardTypePrice,
                    'age' => $this->serializer->asString($model->getProductSettings()->getAge()),
                    'amountOfDays' => $this->serializer->asString($model->getProductSettings()->getAmountOfDays()),
                    'rounds' => $this->serializer->asInt($model->getProductSettings()->getRounds()),
                ] : null,
            'prices' => array_map(
                function (ProductPrice $productPrice) {
                    return [
                        'id' => (string)$productPrice->getId(),
                        'price' => $productPrice->getPrice()->getValue(),
                        'ppsPeriodId' => $productPrice->getPpsPeriodId()->getValue(),
                        'dateStart' => $productPrice->getDateStart()->format(DateTimeImmutable::ATOM),
                        'dateEnd' => $productPrice->getDateEnd()->format(DateTimeImmutable::ATOM),
                    ];
                },
                $model->getProductPrices()
            )
        ]);

        if ($model->getSection()->getSectionSettings()->isActivationDateRequired()) {
            $result['availableDates'] = $this->availableDatesService->getAvailableDates($model);
        }

        return $result;
    }
}
