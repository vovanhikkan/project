<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Product;

use App\Product\Command\Product\Update\SettingsDto;
use App\Product\Command\Product\Update\Command;
use App\Product\Command\Product\Update\PriceDto;
use App\Product\Model\Product\Product;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *     path="/v2/product/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Продукты"},
 *     security={{"bearerAuth":{}}},
 *     description="Редактирование продукта",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="sectionId", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="string"),
 *                  @OA\Property(property="price", type="number"),
 *                  @OA\Property(
 *                      property="settings",
 *                      @OA\Items(
 *                          @OA\Property(
 *                              property="cardType",
 *                              type="integer",
 *                              enum={100, 200, 300},
 *                              description="`100` — none, `200` — plastic, `300` — plastic free"
 *                          ),
 *                          @OA\Property(property="cardTypePrice", type="number"),
 *                          @OA\Property(property="age", type="string"),
 *                          @OA\Property(property="amountOfDays", type="string"),
 *                          @OA\Property(property="rounds", type="number")
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="prices",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="price", type="number"),
 *                          @OA\Property(property="ppsPeriodId", type="number"),
 *                          @OA\Property(property="dateStart", type="string", format="date"),
 *                          @OA\Property(property="dateEnd", type="string", format="date")
 *                      )
 *                  )
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="sectionId", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="string"),
 *                  @OA\Property(property="price", type="number"),
 *                  @OA\Property(property="settings",
 *                      @OA\Items(
 *                          @OA\Property(
 *                              property="cardType",
 *                              type="integer",
 *                              enum={100, 200, 300},
 *                              description="`100` — none, `200` — plastic, `300` — plastic free"
 *                          ),
 *                          @OA\Property(property="age", type="string"),
 *                          @OA\Property(property="amountOfDays", type="string"),
 *                          @OA\Property(property="rounds", type="number"),
 *                          @OA\Property(property="ppsProductId", type="number"),
 *                          @OA\Property(property="newCardPpsProductId", type="number"),
 *                          @OA\Property(property="oldCardPpsProductId", type="number"),
 *                      )
 *                  ),
 *                  @OA\Property(
 *                      property="prices",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="id", type="string", format="uuid"),
 *                          @OA\Property(property="price", type="number"),
 *                          @OA\Property(property="ppsPeriodId", type="number"),
 *                          @OA\Property(property="dateStart", type="string", format="date"),
 *                          @OA\Property(property="dateEnd", type="string", format="date")
 *                      )
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class UpdateAction extends AbstractProductAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Product $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $prices = [];
        foreach ($paramsExtractor->getArray('prices') as $item) {
            $priceDto = new PriceDto(
                $item->getFloat('price'),
                $item->getInt('ppsPeriodId'),
                $item->getString('dateStart'),
                $item->getString('dateEnd')
            );

            if ($item->has('id')) {
                $priceDto->setId($item->getString('id'));
            }

            $prices[] = $priceDto;
        }

        $settings = $paramsExtractor->getSimpleArray('settings');

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getString('sectionId'),
            $paramsExtractor->getString('name'),
            $paramsExtractor->getFloat('price'),
            $prices,
            new SettingsDto(
                $settings['cardType'],
                $settings['age'],
                $settings['amountOfDays'],
                $settings['rounds'],
                $settings['ppsProductId'],
                $settings['newCardPpsProductId'],
                $settings['oldCardPpsProductId'],
            )
        );
    }
}
