<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Product;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/product",
 *     tags={"Продукты"},
 *     description="Список продуктов",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="sectionId", type="string", format="uuid"),
 *                      @OA\Property(property="name", type="string"),
 *                      @OA\Property(property="price", type="number"),
 *                      @OA\Property(
 *                          property="prices",
 *                          type="array",
 *                          @OA\Items(
 *                              @OA\Property(property="id", type="string", format="uuid"),
 *                              @OA\Property(property="price", type="number"),
 *                              @OA\Property(property="ppsPeriodId", type="number"),
 *                              @OA\Property(property="dateStart", type="string", format="date"),
 *                              @OA\Property(property="dateEnd", type="string", format="date")
 *                          )
 *                      ),
 *                      @OA\Property(
 *                          property="settings",
 *                          @OA\Items(
 *                              @OA\Property(
 *                                  property="cardType",
 *                                  type="integer",
 *                                  enum={100, 200, 300},
 *                                  description="`100` — none, `200` — plastic, `300` — plastic free"
 *                              ),
 *                              @OA\Property(property="cardTypePrice", type="number"),
 *                              @OA\Property(property="age", type="string"),
 *                              @OA\Property(property="amountOfDays", type="string"),
 *                              @OA\Property(property="rounds", type="number")
 *                          )
 *                      )
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class IndexAction extends AbstractProductAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->productRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
