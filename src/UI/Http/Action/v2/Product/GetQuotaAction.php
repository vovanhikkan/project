<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Product;

use App\Product\Command\Product\GetQuota\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * GetQuotaAction.
 *
 * @OA\Get(
 *     path="/v2/product/{id}/quota",
 *     tags={"Продукты"},
 *     security={{"bearerAuth":{}}},
 *     description="Получить квоту",
 *     @OA\Parameter(
 *          required=false,
 *          name="activationDate",
 *          in="query",
 *      ),
 *     @OA\Parameter(
 *          required=true,
 *          name="id",
 *          in="path",
 *      ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="activationDate", type="string")
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class GetQuotaAction extends AbstractProductAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);

        return $this->asJson($result);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getStringOrNull('activationDate')
        );
    }
}
