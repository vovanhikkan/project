<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Product;

use App\Product\Command\Product\GetPrice\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * GetPriceAction.
 *
 * @OA\Get(
 *     path="/v2/product/{id}/price",
 *     tags={"Продукты"},
 *     security={{"bearerAuth":{}}},
 *     description="Получить цену продукта (для авторизованного юзера и нет будет разный результат).",
 *     @OA\Parameter(
 *          required=true,
 *          name="activationDate",
 *          in="query",
 *      ),
 *     @OA\Parameter(
 *          required=true,
 *          name="id",
 *          in="path",
 *      ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Items(
 *                      @OA\Property(property="productId", type="string", format="uuid"),
 *                      @OA\Property(property="priceValueWithoutDiscount", type="number"),
 *                      @OA\Property(property="calculatedPriceValue", type="number"),
 *                      @OA\Property(property="calculatedCashbackPointValue", type="number"),
 *                      @OA\Property(property="discountAbsoluteValue", type="integer"),
 *                      @OA\Property(property="discountPercentageValue", type="integer"),
 *                      @OA\Property(property="cashbackPointAbsoluteValue", type="integer"),
 *                      @OA\Property(property="cashbackPointPercentageValue", type="integer"),
 *                  )
 *              ),
 *          ),
 *     ),
 * )
 */
class GetPriceAction extends AbstractProductAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);

        return $this->asJson($result, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);
        $user = $this->getCurrentUser();
        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getString('activationDate'),
            $user ? $user : null
        );
    }
}
