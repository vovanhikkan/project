<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\QuotaValue;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * IndexAction.
 *
 * @OA\Get(
 *     path="/v2/quota-value",
 *     tags={"Квоты"},
 *     description="Список значений квот",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid")
 *                  )
 *              ),
 *          ),
 *     )
 * )
 */
class IndexAction extends AbstractQuotaValueAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->quotaValueRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
