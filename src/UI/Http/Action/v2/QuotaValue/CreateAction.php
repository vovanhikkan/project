<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\QuotaValue;

use App\Product\Command\QuotaValue\Create\Command;
use App\Product\Model\QuotaValue\QuotaValue;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/quota-value",
 *     tags={"Квоты"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление значения квоты",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="quota", type="string", format="uuid"),
 *                  @OA\Property(property="totalQuantity", type="number"),
 *                  @OA\Property(property="holdedQuantity", type="number"),
 *                  @OA\Property(property="orderedQuantity", type="number"),
 *                  @OA\Property(property="dateFrom", type="string"),
 *                  @OA\Property(property="dateTo", type="string"),
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="quota", type="string", format="uuid"),
 *                  @OA\Property(property="totalQuantity", type="number"),
 *                  @OA\Property(property="holdedQuantity", type="number"),
 *                  @OA\Property(property="orderedQuantity", type="number"),
 *                  @OA\Property(property="dateFrom", type="string"),
 *                  @OA\Property(property="dateTo", type="string"),
 *              ),
 *          ),
 *     )
 * )
 */
class CreateAction extends AbstractQuotaValueAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var QuotaValue $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('quota'),
            $paramsExtractor->getInt('totalQuantity'),
            $paramsExtractor->getInt('holdedQuantity'),
            $paramsExtractor->getInt('orderedQuantity'),
            $paramsExtractor->getStringOrNull('dateFrom'),
            $paramsExtractor->getStringOrNull('dateTo')
        );
    }
}
