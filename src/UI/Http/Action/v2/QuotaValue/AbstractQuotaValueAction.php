<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\QuotaValue;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Product\Model\QuotaValue\QuotaValue;
use App\Product\Repository\QuotaValueRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractQuotaValueAction.
 */
abstract class AbstractQuotaValueAction extends AbstractAction
{
    protected QuotaValueRepository $quotaValueRepository;

    public function __construct(
        QuotaValueRepository $quotaValueRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->quotaValueRepository = $quotaValueRepository;
    }
    
    protected function serializeList(QuotaValue $model): array
    {
        return [
            'id' => (string)$model->getId(),
        ];
    }

    protected function serializeItem(QuotaValue $model): array
    {
        return array_merge($this->serializeList($model), [
            'totalQuantity' => (string) $model->getTotalQuantity(),
            'holdedQuantity' => (string) $model->getHoldedQuantity(),
            'orderedQuantity' => (string) $model->getOrderedQuantity(),
            'dateFrom' => $model->getDateFrom(),
            'dateTo' => $model->getDateTo(),
            'quota' => (string) $model->getQuota()->getId()
        ]);
    }
}
