<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanGroup;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/rate-plan-group/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр RatePlanGroup",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="sort", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractRatePlanGroupAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->ratePlanGroupRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
