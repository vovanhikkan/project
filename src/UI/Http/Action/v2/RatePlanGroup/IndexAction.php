<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanGroup;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/rate-plan-group",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список RatePlanGroup",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="sort", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractRatePlanGroupAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->ratePlanGroupRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
