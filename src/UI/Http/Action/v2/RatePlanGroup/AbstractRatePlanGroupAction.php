<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanGroup;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Repository\RatePlanGroupRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractRatePlanGroupAction extends AbstractAction
{
    protected RatePlanGroupRepository $ratePlanGroupRepository;

    public function __construct(
        RatePlanGroupRepository $ratePlanGroupRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->ratePlanGroupRepository = $ratePlanGroupRepository;
    }


    protected function serializeList(RatePlanGroup $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(RatePlanGroup $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'sort' => $serializer->asInt($model->getSort())
        ];
    }
}
