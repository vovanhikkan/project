<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanGroup;

use App\Hotel\Command\RatePlanGroup\Create\Command;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/rate-plan-group",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление RatePlanGroup",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="sort", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractRatePlanGroupAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var RatePlanGroup $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        
        $command = new Command(
            $paramsExtractor->getSimpleArray('name')
        );

        return $command;
    }
}
