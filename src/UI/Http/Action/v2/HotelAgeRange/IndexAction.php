<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelAgeRange;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/hotel-age-range",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список HotelAgeRange",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="ageFrom", type="number"),
 *              @OA\Property(property="ageTo", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractHotelAgeRangeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->hotelAgeRangeRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
