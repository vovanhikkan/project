<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelAgeRange;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\Hotel\Repository\HotelAgeRangeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractHotelAgeRangeAction
 * @package App\UI\Http\Action\v2\HotelAgeRange
 */
abstract class AbstractHotelAgeRangeAction extends AbstractAction
{

    protected HotelAgeRangeRepository $hotelAgeRangeRepository;
    
    public function __construct(
        HotelAgeRangeRepository $hotelAgeRangeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->hotelAgeRangeRepository = $hotelAgeRangeRepository;
    }

    protected function serializeList(HotelAgeRange $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(HotelAgeRange $model): array
    {
        $serializer = $this->serializer;

        $result = [
            'id' => $serializer->asUuid($model->getId()),
            'hotel' => $serializer->asUuid($model->getHotel()->getId()),
            'ageFrom' => $serializer->asInt($model->getAgeFrom()),
            'ageTo' => $serializer->asInt($model->getAgeTo())
        ];
        

        return $result;
    }
}
