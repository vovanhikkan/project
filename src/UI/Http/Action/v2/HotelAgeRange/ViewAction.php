<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelAgeRange;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/hotel-age-range/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр HotelAgeRange",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="ageFrom", type="number"),
 *              @OA\Property(property="ageTo", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractHotelAgeRangeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->hotelAgeRangeRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
