<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelAgeRange;

use App\Hotel\Command\HotelAgeRange\Update\Command;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/hotel-age-range/{id}",
 *  tags={"Отели"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование HotelAgeRange",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="ageFrom", type="number"),
 *              @OA\Property(property="ageTo", type="number"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="ageFrom", type="number"),
 *              @OA\Property(property="ageTo", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractHotelAgeRangeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var HotelAgeRange $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );


        if ($paramsExtractor->has('hotel')) {
            $command->setHotel($paramsExtractor->getString('hotel'));
        }

        if ($paramsExtractor->has('ageFrom')) {
            $command->setAgeFrom($paramsExtractor->getInt('ageFrom'));
        }

        if ($paramsExtractor->has('ageTo')) {
            $command->setAgeTo($paramsExtractor->getInt('ageTo'));
        }

        return $command;
    }
}
