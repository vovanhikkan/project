<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelAgeRange;

use App\Hotel\Command\HotelAgeRange\Create\Command;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/hotel-age-range",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление HotelAgeRange",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="ageFrom", type="number"),
 *              @OA\Property(property="ageTo", type="number"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="ageFrom", type="number"),
 *              @OA\Property(property="ageTo", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractHotelAgeRangeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var HotelAgeRange $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getString('hotel'),
            $paramsExtractor->getInt('ageFrom'),
            $paramsExtractor->getInt('ageTo')
        );

        return $command;
    }
}
