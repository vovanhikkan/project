<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Label;

use App\Place\Command\Label\Create\Command;
use App\Place\Model\Label\Label;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 * 	path="/v2/label",
 * 	tags={"Label"},
 * 	security={{"bearerAuth":{}}},
 * 	description="Добавление Label",
 * 	@OA\RequestBody(
 * 		required=true,
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 *                      @OA\Property(property="id", type="string"),
 *                      @OA\Property(property="labelTypeId", type="string"),
 *                      @OA\Property(property="color", type="string"),
 *                      @OA\Property(property="content", type="object")
 *     )
 * 		)
 * 	),
 * 	@OA\Response(
 * 		response="201",
 * 	    description="",
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 *                  @OA\Property(property="id", type="string")
 * )
 * 		)
 * 	)
 * )
 */
class CreateAction extends AbstractLabelAction
{
	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$this->denyAccessNotAdministrator();

		$command = $this->deserialize($request);
		$this->validator->validate($command);
		/** @var Label $result */
		$result = $this->bus->handle($command);
		$data = $this->serializeItem($result);

		return $this->asJson($data, 201);
	}


	private function deserialize(ServerRequestInterface $request): Command
	{
		$paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('labelTypeId'),
            $paramsExtractor->getString('color'),
            $paramsExtractor->getSimpleArray('content')
        );
	}
}
