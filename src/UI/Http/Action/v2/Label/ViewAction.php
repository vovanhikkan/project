<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Label;

use App\Application\Service\Validator;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\AuthContext;
use App\Place\Repository\LabelRepository;
use App\Storage\Service\File\Locator;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * @OA\Get(
 * 	path="/v2/label/{id}",
 * 	tags={"Label"},
 * 	@OA\Parameter(required=true, name="id", in="path"),
 * 	security={{"bearerAuth":{}}},
 * 	description="Просмотр Label",
 * 	@OA\Response(
 * 		response="201",
 * 	    description="",
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 * 				@OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="labelTypeId", type="string"),
 *                      @OA\Property(property="color", type="string"),
 *                      @OA\Property(property="content", type="object")
 * 			)
 * 		)
 * 	)
 * )
 */
class ViewAction extends AbstractLabelAction
{
	private LabelRepository $labelRepository;


	public function __construct(
		LabelRepository $labelRepository,
		Validator $validator,
		CommandBus $bus,
		AuthContext $authContext,
		LoggerInterface $logger,
        Locator $locator
	) {
		parent::__construct($validator, $bus, $authContext, $logger, $locator);
		$this->labelRepository = $labelRepository;
	}


	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$item = $this->labelRepository->get(new Uuid($this->resolveArg('id')));
		$data = $this->serializeItem($item);

		return $this->asJson($data);
	}
}
