<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Label;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Place\Repository\LabelRepository;
use App\Storage\Service\File\Locator;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * @OA\Get(
 * 	path="/v2/label",
 * 	tags={"Label"},
 * 	security={{"bearerAuth":{}}},
 * 	description="Список Label",
 * 	@OA\Response(
 * 		response="201",
 * 	    description="",
 * 		@OA\MediaType(
 * 			mediaType="application/json",
 * 			@OA\Schema(
 * 				@OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="labelTypeId", type="string"),
 *              @OA\Property(property="color", type="string"),
 *              @OA\Property(property="content", type="object")
 * 			)
 * 		)
 * 	)
 * )
 */
class IndexAction extends AbstractLabelAction
{
	private LabelRepository $labelRepository;


	public function __construct(
		LabelRepository $labelRepository,
		Validator $validator,
		CommandBus $bus,
		AuthContext $authContext,
		LoggerInterface $logger,
        Locator $locator
	) {
		parent::__construct($validator, $bus, $authContext, $logger, $locator);
		$this->labelRepository = $labelRepository;
	}


	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$items = $this->labelRepository->fetchAll();
		$data = array_map([$this, 'serializeItem'], $items);

		return $this->asJson($data);
	}
}
