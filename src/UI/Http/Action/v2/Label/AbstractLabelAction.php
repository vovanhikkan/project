<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Label;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Place\Model\Label\Label;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractLabelAction extends AbstractAction
{
	public function __construct(
	    Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
		parent::__construct($validator, $bus, $authContext, $logger, $locator);
	}


	protected function serializeList(Label $model): array
	{
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId())
        ];
	}


	protected function serializeItem(Label $model): array
	{
		$serializer = $this->serializer;
		$labelType = $model->getLabelType();

		return [
            'id' => $serializer->asUuid($model->getId()),
			'labelType' => $labelType ? [
			    "id" => $serializer->asUuid($labelType->getId()),
			    "icon" => $this->getFileData($labelType->getIcon()),
			    "svg_icon" => $this->getFileData($labelType->getSvgIcon()),
			    "default_color" => $serializer->asString($labelType->getDefaultColor()),
			    "default_content" => $serializer->asString($labelType->getDefaultContent()),
            ] : null,
            'color' => $serializer->asString($model->getColor()),
            'content' => $serializer->asArray($model->getContent())
		];
	}
}
