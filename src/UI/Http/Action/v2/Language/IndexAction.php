<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Language;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * IndexAction.
 *
 * @OA\Get(
 *     path="/v2/language",
 *     tags={"Язык"},
 *     description="Список языков",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="string"),
 *                  @OA\Property(property="code", type="string")
 *              ),
 *          ),
 *     )
 * )
 */
class IndexAction extends AbstractLanguageAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->languageRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
