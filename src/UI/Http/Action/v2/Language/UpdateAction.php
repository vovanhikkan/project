<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Language;

use App\Language\Command\Language\Update\Command;
use App\Language\Model\Language\Language;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *     path="/v2/language/{id}",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     tags={"Язык"},
 *     security={{"bearerAuth":{}}},
 *     description="Редактирование языка",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="string"),
 *                  @OA\Property(property="code", type="string")
 *              ),
 *          ),
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="name", type="string"),
 *                  @OA\Property(property="code", type="string")
 *              ),
 *          ),
 *     )
 * )
 */
class UpdateAction extends AbstractLanguageAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Language $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getString('name'),
            $paramsExtractor->getString('code')
        );
    }
}
