<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Language;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Language\Model\Language\Language;
use App\Language\Repository\LanguageRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractLanguageAction
 * @package App\UI\Http\Action\v2\Language
 */
abstract class AbstractLanguageAction extends AbstractAction
{
    protected LanguageRepository $languageRepository;

    public function __construct(
        LanguageRepository $languageRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->languageRepository = $languageRepository;
    }

    protected function serializeList(Language $model): array
    {
        $serializer = $this->serializer;
        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asString($model->getName()),
            'code' => $serializer->asString($model->getCode())
        ];
    }

    protected function serializeItem(Language $model): array
    {
        return $this->serializeList($model);
    }
}
