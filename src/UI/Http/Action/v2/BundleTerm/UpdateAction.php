<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BundleTerm;

use App\Bundle\Command\BundleTerm\Update\Command;
use App\Bundle\Model\BundleTerm\BundleTerm;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/bundle-term/{id}",
 *  tags={"Пакеты"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование BundleTerm",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="bundle", type="string", format="uuid"),
 *              @OA\Property(property="description", type="object"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="bundle", type="string", format="uuid"),
 *              @OA\Property(property="description", type="object"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractBundleTermAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var BundleTerm $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );

        if ($paramsExtractor->has('bundle')) {
            $command->setBundle($paramsExtractor->getString('bundle'));
        }

        if ($paramsExtractor->has('description')) {
            $command->setDescription($paramsExtractor->getSimpleArray('description'));
        }

        return $command;
    }
}
