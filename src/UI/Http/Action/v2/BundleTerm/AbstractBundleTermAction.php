<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BundleTerm;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Bundle\Model\BundleTerm\BundleTerm;
use App\Bundle\Repository\BundleTermRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractBundleTermAction
 * @package App\UI\Http\Action\v2\BundleTerm
 */
abstract class AbstractBundleTermAction extends AbstractAction
{
    protected BundleTermRepository $bundleTermRepository;

    public function __construct(
        BundleTermRepository $bundleTermRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->bundleTermRepository = $bundleTermRepository;
    }

    protected function serializeList(BundleTerm $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(BundleTerm $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'description' => $serializer->asArray($model->getDescription()),
            'bundle' => $serializer->asUuid($model->getBundle()->getId()),
        ];
    }
}
