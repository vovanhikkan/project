<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BundleTerm;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bundle-term",
 *  tags={"Пакеты"},
 *  security={{"bearerAuth":{}}},
 *  description="Список BundleTerm",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="bundle", type="string", format="uuid"),
 *              @OA\Property(property="description", type="object"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractBundleTermAction
{
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->bundleTermRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
