<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BundleTerm;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bundle-term/{id}",
 *  tags={"Пакеты"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр BundleTerm",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="bundle", type="string", format="uuid"),
 *              @OA\Property(property="description", type="object"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractBundleTermAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->bundleTermRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
