<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FoodType;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/food-type",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список FoodType",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object")
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractFoodTypeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->foodTypeRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
