<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FoodType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Repository\FoodTypeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractFoodTypeAction
 * @package App\UI\Http\Action\v2\FoodType
 */
abstract class AbstractFoodTypeAction extends AbstractAction
{
    protected FoodTypeRepository $foodTypeRepository;

    public function __construct(
        FoodTypeRepository $foodTypeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->foodTypeRepository = $foodTypeRepository;
    }

    protected function serializeList(FoodType $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(FoodType $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName())
        ];
    }
}
