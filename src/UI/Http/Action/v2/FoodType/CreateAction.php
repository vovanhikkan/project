<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\FoodType;

use App\Hotel\Command\FoodType\Create\Command;
use App\Hotel\Model\FoodType\FoodType;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/food-type",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление FoodType",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *               @OA\Property(property="id", type="strng", format="uuid"),
 *               @OA\Property(property="name", type="object")
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractFoodTypeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var FoodType $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name')
        );

        return $command;
    }
}
