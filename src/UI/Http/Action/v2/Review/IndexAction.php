<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Review;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/review",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Review",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="body", type="string"),
 *              @OA\Property(property="rating", type="number"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(
 *                  property="reviewStars", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="reviewStarTypeId", type="string", format="uuid"),
 *                      @OA\Property(property="value", type="number"),
 *                  )
 *             ),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractReviewAction
{
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->reviewRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
