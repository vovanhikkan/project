<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Review;

use App\Application\Exception\InvalidArgumentException;
use App\Application\Exception\InvalidRoleDeleteReviewException;
use App\Review\Command\Review\Update\Command;
use App\Review\Model\Review\Review;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/review/{id}",
 *  tags={"Отели"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование Review",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="body", type="string"),
 *              @OA\Property(property="rating", type="number"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="body", type="string"),
 *              @OA\Property(property="rating", type="number"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(
 *                  property="reviewStars", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="reviewStarTypeId", type="string", format="uuid"),
 *                      @OA\Property(property="value", type="number"),
 *                  )
 *             ),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractReviewAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);

        if (!$this->getCurrentUser()->getRole()->isAdmin()) {
            //если пользователь удаляет не свой комментарий
            if ($command->getId() !== $this->getCurrentUser()->getId()->getValue()) {
                throw new InvalidRoleDeleteReviewException();
            }
        }
        
        $this->validator->validate($command);
        /** @var Review $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 200);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getString('body'),
            $paramsExtractor->getInt('rating')
        );

        return $command;
    }
}
