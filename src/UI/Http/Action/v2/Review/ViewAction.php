<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Review;

use App\Application\Service\Validator;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\AuthContext;
use App\Review\Repository\ReviewRepository;
use League\Tactician\CommandBus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * @OA\Get(
 *  path="/v2/review/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр Review",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="body", type="string"),
 *              @OA\Property(property="rating", type="number"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(
 *                  property="reviewStars", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="reviewStarTypeId", type="string", format="uuid"),
 *                      @OA\Property(property="value", type="number"),
 *                  )
 *             ),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractReviewAction
{
    private ReviewRepository $reviewRepository;


    public function __construct(
        ReviewRepository $reviewRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger
    ) {
        parent::__construct($validator, $bus, $authContext, $logger);
        $this->reviewRepository = $reviewRepository;
    }


    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->reviewRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
