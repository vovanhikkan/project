<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Review;

use App\Application\Exception\InvalidRoleDeleteReviewException;
use App\Review\Command\Review\Delete\Command;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Delete(
 *  path="/v2/review/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Удаление Review",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="message", type="string")
 *          )
 *      )
 *  )
 * )
 */
class DeleteAction extends AbstractReviewAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        
        if (!$this->getCurrentUser()->getRole()->isAdmin()) {
            //если пользователь удаляет не свой комментарий
            if ($command->getId() !== $this->getCurrentUser()->getId()->getValue()) {
                throw new InvalidRoleDeleteReviewException();
            }
        }
        
        
        $this->validator->validate($command);

        $result = $this->bus->handle($command);
        $data = $result;

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        return new Command(
            $this->resolveArg('id')
        );
    }
}
