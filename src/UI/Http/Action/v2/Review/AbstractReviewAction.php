<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Review;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Place\Model\Place\Rating;
use App\Review\Model\Review\Review;
use App\Review\Repository\ReviewRepository;
use App\Storage\Service\File\Locator;
use App\Review\Model\ReviewStar\ReviewStar;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractReviewAction extends AbstractAction
{
    protected ReviewRepository $reviewRepository;

    public function __construct(
        ReviewRepository $reviewRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->reviewRepository = $reviewRepository;
    }


    protected function serializeList(Review $model): array
    {
        $serializer = $this->serializer;

        $place = null;
        $hotel = null;

        if ($model->getPlace()) {
            $place = $serializer->asUuid($model->getPlace()->getId());
        }

        if ($model->getHotel()) {
            $hotel = $serializer->asUuid($model->getHotel()->getId());
        }

        return [
            'id' => $serializer->asUuid($model->getId()),
            'body' => $serializer->asText($model->getBody()),
            'rating' => $serializer->asIntOrNull($model->getRating()),
            'place' => $place,
            'hotel' => $hotel
        ];
    }


    protected function serializeItem(Review $model): array
    {
        $result = array_merge($this->serializeList($model), [
            'reviewStars' => array_map(
                function (ReviewStar $reviewStar) {
                    return [
                        'id' => (string)$reviewStar->getId(),
                        'reviewStarTypeId' => (string)$reviewStar->getReviewStarType()->getId(),
                        'value' => $reviewStar->getValue()->getValue()
                    ];
                },
                $model->getReviewStars()
            )
        ]);

        return $result;
    }
}
