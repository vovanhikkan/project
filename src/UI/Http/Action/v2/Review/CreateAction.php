<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Review;

use App\Application\Exception\InvalidArgumentException;
use App\Review\Command\Review\Create\Command;
use App\Review\Command\Review\Create\StarDto;
use App\Review\Dto\ReviewStarDto;
use App\Review\Model\Review\Review;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/review",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление Review",
 *  @OA\RequestBody(
 *      required=true,
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="body", type="string"),
 *              @OA\Property(property="rating", type="number"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(
 *                  property="reviewStars", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="reviewStarTypeId", type="string", format="uuid"),
 *                      @OA\Property(property="value", type="number"),
 *                  )
 *             ),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="body", type="string"),
 *              @OA\Property(property="rating", type="number"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(
 *                  property="reviewStars", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid"),
 *                      @OA\Property(property="reviewStarTypeId", type="string", format="uuid"),
 *                      @OA\Property(property="value", type="number"),
 *                  )
 *             ),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractReviewAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAuthenticated();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Review $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->getCurrentUser()->getId()->getValue(),
            $paramsExtractor->getString('body'),
        );


        if ($paramsExtractor->has('rating')) {
            $command->setRating($paramsExtractor->getInt('rating'));
        }
        
        if ($paramsExtractor->has('place')) {
            $command->setPlace($paramsExtractor->getString('place'));
        } elseif ($paramsExtractor->has('hotel')) {
            $command->setHotel($paramsExtractor->getString('hotel'));
        } else {
            throw new InvalidArgumentException();
        }

        $reviewStars = [];
        foreach ($paramsExtractor->getArray('reviewStars') as $star) {
            $reviewStars[] = new StarDto(
                $star->getString('reviewStarType'),
                $star->getInt('value')
            );
        }

        if ($reviewStars) {
            $command->setReviewStars($reviewStars);
        }
        
        return $command;
    }
}
