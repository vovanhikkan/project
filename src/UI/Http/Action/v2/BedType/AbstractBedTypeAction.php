<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BedType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Repository\BedTypeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractBedTypeAction
 * @package App\UI\Http\Action\v2\BedType
 */
abstract class AbstractBedTypeAction extends AbstractAction
{

    protected BedTypeRepository $bedTypeRepository;
    
    public function __construct(
        BedTypeRepository $bedTypeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->bedTypeRepository = $bedTypeRepository;
    }

    protected function serializeList(BedType $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(BedType $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'mobileIcon' =>  $this->getFileData($model->getMobileIcon()),
            'webIcon' =>  $this->getFileData($model->getWebIcon()),
        ];
    }
}
