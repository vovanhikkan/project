<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BedType;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bed-type",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список BedType",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractBedTypeAction
{
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->bedTypeRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
