<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BedType;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bed-type/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр BedType",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractBedTypeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->bedTypeRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
