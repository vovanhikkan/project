<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Facility;

use App\Hotel\Command\Facility\Create\Command;
use App\Hotel\Model\Facility\Facility;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/facility",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление Facility",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="facilityGroup", type="string", format="uuid"),
 *              @OA\Property(property="isShownInRoomList", type="boolean"),
 *              @OA\Property(property="isShownInSearch", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="facilityGroup", type="string", format="uuid"),
 *              @OA\Property(property="isShownInRoomList", type="boolean"),
 *              @OA\Property(property="isShownInSearch", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid"),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractFacilityAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Facility $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getString('facilityGroup'),
        );

        if ($paramsExtractor->has('isShownInRoomList')) {
            $command->setIsShownInRoomList($paramsExtractor->getBool('isShownInRoomList'));
        }

        if ($paramsExtractor->has('isShownInSearch')) {
            $command->setIsShownInSearch($paramsExtractor->getBool('isShownInSearch'));
        }

        if ($paramsExtractor->has('mobileIcon')) {
            $command->setMobileIcon($paramsExtractor->getStringOrNull('mobileIcon'));
        }

        if ($paramsExtractor->has('webIcon')) {
            $command->setWebIcon($paramsExtractor->getStringOrNull('webIcon'));
        }

        return $command;
    }
}
