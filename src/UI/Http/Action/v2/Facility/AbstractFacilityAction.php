<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Facility;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Repository\FacilityRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractFacilityAction
 * @package App\UI\Http\Action\v2\Facility
 */
abstract class AbstractFacilityAction extends AbstractAction
{

    protected FacilityRepository $facilityRepository;

    public function __construct(
        FacilityRepository $facilityRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->facilityRepository = $facilityRepository;
    }

    protected function serializeList(Facility $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(Facility $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'isShownInSearch' => $model->getIsShownInSearch()
        ];
    }
}
