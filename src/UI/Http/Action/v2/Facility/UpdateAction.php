<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Facility;

use App\Hotel\Command\Facility\Update\Command;
use App\Hotel\Model\Facility\Facility;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/facility/{id}",
 *  tags={"Facility"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование Facility",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="group", type="string", format="uuid"),
 *              @OA\Property(property="isShownInRoomList", type="boolean"),
 *              @OA\Property(property="isShownInSearch", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="group", type="string", format="uuid"),
 *              @OA\Property(property="isShownInRoomList", type="boolean"),
 *              @OA\Property(property="isShownInSearch", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractFacilityAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var Facility $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );


        if ($paramsExtractor->has('name')) {
            $command->setName($paramsExtractor->getSimpleArray('name'));
        }

        if ($paramsExtractor->has('group')) {
            $command->setFacilityGroup($paramsExtractor->getStringOrNull('group'));
        }

        if ($paramsExtractor->has('isShownInRoomList')) {
            $command->setIsShownInRoomList($paramsExtractor->getBool('isShownInRoomList'));
        }

        if ($paramsExtractor->has('isShownInSearch')) {
            $command->setIsShownInSearch($paramsExtractor->getBool('isShownInSearch'));
        }

        if ($paramsExtractor->has('mobileIcon')) {
            $command->setMobileIcon($paramsExtractor->getStringOrNull('mobileIcon'));
        }

        if ($paramsExtractor->has('webIcon')) {
            $command->setWebIcon($paramsExtractor->getStringOrNull('webIcon'));
        }

        return $command;
    }
}
