<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Facility;

use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/facility",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список Facility",
 *     @OA\Parameter(
 *          required=false,
 *          name="isShownInSearch",
 *          in="query",
 *         @OA\Schema(
 *            type="boolean",
 *         ),
 *      ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="group", type="string", format="uuid"),
 *              @OA\Property(property="isShownInRoomList", type="boolean"),
 *              @OA\Property(property="isShownInSearch", type="boolean"),
 *              @OA\Property(property="mobileIcon", type="string", format="uuid"),
 *              @OA\Property(property="webIcon", type="string", format="uuid"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractFacilityAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);
        $shown = null;
        if ($paramsExtractor->has('isShownInSearch')) {
            $shown = $paramsExtractor->getBool('isShownInSearch');
        }

        $items = $this->facilityRepository->fetchAllByShownSearch($shown);
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
