<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\PaySystem;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Order\Model\PaySystem\PaySystem;
use App\Order\Repository\PaySystemRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractPaySystemAction
 * @package App\UI\Http\Action\v2\PaySystem
 */
abstract class AbstractPaySystemAction extends AbstractAction
{
    protected PaySystemRepository $paySystemRepository;

    public function __construct(
        PaySystemRepository $paySystemRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->paySystemRepository = $paySystemRepository;
    }

    protected function serializeItem(PaySystem $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'description' => $serializer->asArrayOrNull($model->getDescription()),
            'code' => $serializer->asStringOrNull($model->getCode()),
            'imageUrl' => $model->getImage() !== null
                ? $this->locator->getAbsoluteUrl($model->getImage())
                : null
        ];
    }
}
