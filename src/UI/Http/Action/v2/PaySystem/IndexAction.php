<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\PaySystem;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/pay-system",
 *     tags={"Методы оплаты"},
 *     description="Список методов оплаты",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Items(
 *                     @OA\Property(property="id", type="string", format="uuid"),
 *                     @OA\Property(property="title", type="object"),
 *                     @OA\Property(property="description", type="object"),
 *                     @OA\Property(property="code", type="string"),
 *                     @OA\Property(property="imageUrl", type="string")
 *                 )
 *             )
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractPaySystemAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->paySystemRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
