<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Payment;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Order\Model\Payment\Payment;
use App\Order\Repository\PaymentRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractPaymentAction
 * @package App\UI\Http\Action\v2\Payment
 */
abstract class AbstractPaymentAction extends AbstractAction
{
    protected PaymentRepository $paymentRepository;

    public function __construct(
        PaymentRepository $paymentRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->paymentRepository = $paymentRepository;
    }

    protected function serializeItem(Payment $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'orderId' => $serializer->asUuid($model->getOrder()->getId()),
            'paySystemId' => $serializer->asUuid($model->getPaySystem()->getId()),
            'externalId' => $model->getExternalId(),
            'redirectUrl' => $model->getRedirectUrl(),
            'status' => $serializer->asInt($model->getStatus()),
            'createdAt' => $serializer->asDateTime($model->getCreatedAt())
        ];
    }
}
