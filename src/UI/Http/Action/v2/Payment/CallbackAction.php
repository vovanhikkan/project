<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Payment;

use App\Order\Command\Payment\Callback\Command;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * CallbackAction.
 */
class CallbackAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->logger->debug(
            'payment callback',
            [
                'method' => $request->getMethod(),
                'query' => $request->getQueryParams(),
                'body' => $request->getParsedBody()
            ]
        );

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        $result = $this->bus->handle($command);

        if ($result) {
            $data = ['successUrl' => $result->getSuccessUrl()->getValue()];
            return $this->asJson($data, 201);
        }

        return $this->asEmpty(201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getQueryParams() ?? []);

        return new Command(
            $paramsExtractor->getString('paymentId')
        );
    }
}
