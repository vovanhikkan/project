<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Payment;

use App\Order\Command\Payment\Create\Command;
use App\Order\Model\Payment\Payment;
use App\UI\Http\Action\AbstractAction;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/payment",
 *     tags={"Платежи"},
 *     description="Создание платежа для заказа",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="orderId", type="string", format="uuid"),
 *                  @OA\Property(property="paySystemId", type="string", format="uuid"),
 *                  @OA\Property(property="successUrl", type="string")
 *              ),
 *          ),
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="В ответе содержится URL для перехода на платёжный шлюз",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                  @OA\Property(property="redirectUrl", type="string")
 *              ),
 *          ),
 *     ),
 * )
 */
class CreateAction extends AbstractAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var Payment $result */
        $result = $this->bus->handle($command);
        $data = ['redirectUrl' => $result->getRedirectUrl()];

        return $this->asJson($data, 201);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getString('orderId'),
            $paramsExtractor->getString('paySystemId'),
            $paramsExtractor->getString('successUrl')
        );
    }
}
