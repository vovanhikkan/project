<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Payment;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/payment",
 *     tags={"Платежи"},
 *     description="Получение списка всех платежей",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Items(
 *                     @OA\Property(property="id", type="string", format="uuid"),
 *                     @OA\Property(property="orderId", type="string", format="uuid"),
 *                     @OA\Property(property="paySystemId", type="string", format="uuid"),
 *                     @OA\Property(property="externalId", type="string"),
 *                     @OA\Property(property="redirectUrl", type="string"),
 *                     @OA\Property(property="status", type="integer"),
 *                     @OA\Property(property="createdAt", type="string")
 *                 )
 *             )
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractPaymentAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $items = $this->paymentRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
