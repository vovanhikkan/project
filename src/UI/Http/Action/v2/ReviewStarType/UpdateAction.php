<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\ReviewStarType;

use App\Review\Command\ReviewStarType\Update\Command;
use App\Review\Model\ReviewStarType\ReviewStarType;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/review-star-type/{id}",
 *  tags={"Отели"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование ReviewStarType",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="isActive", type="boolean"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractReviewStarTypeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var ReviewStarType $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );

        if ($paramsExtractor->has('name')) {
            $command->setName($paramsExtractor->getSimpleArray('name'));
        }

        return $command;
    }
}
