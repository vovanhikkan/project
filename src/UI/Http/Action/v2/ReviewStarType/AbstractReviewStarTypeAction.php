<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\ReviewStarType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Review\Model\ReviewStarType\ReviewStarType;
use App\Review\Repository\ReviewStarTypeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractReviewStarTypeAction extends AbstractAction
{
    protected ReviewStarTypeRepository $reviewStarTypeRepository;

    public function __construct(
        ReviewStarTypeRepository $reviewStarTypeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->reviewStarTypeRepository = $reviewStarTypeRepository;
    }


    protected function serializeList(ReviewStarType $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(ReviewStarType $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName())
        ];
    }
}
