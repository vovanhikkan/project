<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\ReviewStarType;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/review-star-type",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список ReviewStarType",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="name", type="object"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractReviewStarTypeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->reviewStarTypeRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
