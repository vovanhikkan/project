<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelExtraPlaceType;

use App\Hotel\Command\HotelExtraPlaceType\Create\Command;
use App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/hotel-extra-place-type",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление HotelExtraPlaceType",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="hotelAgeRanges", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", type="string", format="uuid")
 *                   )
 *              ),

 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="hotel", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="hotelAgeRanges", type="object",
 *                  @OA\Property(property="ageFrom", type="string", format="uuid"),
 *                  @OA\Property(property="ageTo", type="object"),
 *              ),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractHotelExtraPlaceTypeAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var HotelExtraPlaceType $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getString('hotel'),
        );

        if ($paramsExtractor->has('hotelAgeRanges')) {
            $command->setAgeRanges($paramsExtractor->getSimpleArray('hotelAgeRanges'));
        }

        return $command;
    }
}
