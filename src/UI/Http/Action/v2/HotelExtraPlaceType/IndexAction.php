<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelExtraPlaceType;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

/**
 * IndexAction.
 *
 * @OA\Get(
 *     path="/v2/hotel-extra-place-type",
 *     tags={"Отели"},
 *     description="Список всех HotelExtraPlaceType",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="hotel", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="hotelAgeRanges", type="object",
 *                  @OA\Property(property="ageFrom", type="string", format="uuid"),
 *                  @OA\Property(property="ageTo", type="object"),
 *              ),
 *          )
 *      )
 *  )
 * )
 *
 * @param Request  $request
 * @param Response $response
 * @param array    $args
 *
 * @throws \App\Exception\AppErrorsException
 * @return Response
 */
class IndexAction extends AbstractHotelExtraPlaceTypeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->hotelExtraPlaceTypeRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
