<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelExtraPlaceType;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType;
use App\Hotel\Repository\HotelExtraPlaceTypeRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractHotelExtraPlaceTypeAction
 * @package App\UI\Http\Action\v2\HotelExtraPlaceType
 */
abstract class AbstractHotelExtraPlaceTypeAction extends AbstractAction
{
    protected HotelExtraPlaceTypeRepository $hotelExtraPlaceTypeRepository;

    public function __construct(
        HotelExtraPlaceTypeRepository $hotelExtraPlaceTypeRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->hotelExtraPlaceTypeRepository = $hotelExtraPlaceTypeRepository;
    }

    protected function serializeList(HotelExtraPlaceType $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(HotelExtraPlaceType $model): array
    {
        $serializer = $this->serializer;

        $hotelId = null;
        $hotelName = null;

        if ($hotel = $model->getHotel()) {
            $hotelId = $hotel->getId();
            $hotelName = $hotel->getName();
        }

        $result = [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'hotel' => $serializer->asUuid($model->getHotel()->getId()),

            'hotelAgeRanges' => array_map(
                function (HotelAgeRange $hotelAgeRange) use ($serializer) {
                    return [
                    'ageFrom' =>  $serializer->asInt($hotelAgeRange->getAgeFrom()),
                    'ageTo' =>  $serializer->asInt($hotelAgeRange->getAgeFrom())
                    ];
                },
                $model->getAgeRanges()
            )];

        if ($hotel) {
            $result['hotel'] = [
                'id' =>  $serializer->asUuid($hotelId),
                'name' => $serializer->asArray($hotelName),
            ];
        }

        return $result;
    }
}
