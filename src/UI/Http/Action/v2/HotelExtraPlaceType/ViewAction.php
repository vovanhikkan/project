<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\HotelExtraPlaceType;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/hotel-extra-place-type/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр HotelExtraPlaceType",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="hotel", type="object",
 *                  @OA\Property(property="id", type="string", format="uuid"),
 *                  @OA\Property(property="name", type="object"),
 *              ),
 *              @OA\Property(property="hotelAgeRanges", type="object",
 *                  @OA\Property(property="ageFrom", type="string", format="uuid"),
 *                  @OA\Property(property="ageTo", type="object"),
 *              ),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractHotelExtraPlaceTypeAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->hotelExtraPlaceTypeRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }


}
