<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\ActionGroup;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/action-group",
 *     tags={"Акции"},
 *     description="Список групп акций",
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="name", type="object"),
 *             ),
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractActionGroupAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->actionGroupRepository->fetchAll();
        $data = array_map([$this, 'serializeList'], $items);

        return $this->asJson($data);
    }
}
