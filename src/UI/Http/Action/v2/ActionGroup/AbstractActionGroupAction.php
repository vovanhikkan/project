<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\ActionGroup;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\Loyalty\Repository\ActionGroupRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractActionGroupAction
 * @package App\UI\Http\Action\v2\ActionGroup
 */
abstract class AbstractActionGroupAction extends AbstractAction
{

    protected ActionGroupRepository $actionGroupRepository;

    public function __construct(
        ActionGroupRepository $actionGroupRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->actionGroupRepository = $actionGroupRepository;
    }

    protected function serializeList(ActionGroup $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(ActionGroup $model): array
    {
        return [
            'id' => (string)$model->getId(),
            'name' => $model->getName()->getValue(),
            'sort' => (string)$model->getSort()
        ];
    }
}
