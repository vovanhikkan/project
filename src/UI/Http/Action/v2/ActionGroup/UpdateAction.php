<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\ActionGroup;

use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\Loyalty\Command\ActionGroup\Update\Command;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *     path="/v2/action-group",
 *     tags={"Акции"},
 *     security={{"bearerAuth":{}}},
 *     description="Изменение группы акций",
 *     @OA\Parameter(required=true, name="id", in="path"),
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"name"},
 *                 @OA\Property(property="name", type="string"),
 *                 @OA\Property(property="sort", type="number"),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="name", type="object"),
 *             ),
 *         )
 *     )
 * )
 */
class UpdateAction extends AbstractActionGroupAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var ActionGroup $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $this->resolveArg('id'),
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getInt('sort')
        );
    }
}
