<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\ActionGroup;

use App\Loyalty\Command\ActionGroup\Create\Command;
use App\Loyalty\Model\ActionGroup\ActionGroup;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *     path="/v2/action-group",
 *     tags={"Акции"},
 *     security={{"bearerAuth":{}}},
 *     description="Добавление группы акций",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 required={"name"},
 *                 @OA\Property(property="name", type="string"),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response="201",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(property="name", type="object"),
 *             ),
 *         )
 *     )
 * )
 */
class CreateAction extends AbstractActionGroupAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);

        /** @var ActionGroup $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data);
    }

    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        return new Command(
            $paramsExtractor->getSimpleArray('name')
        );
    }
}
