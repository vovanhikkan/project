<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlan;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Repository\RatePlanRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractRatePlanAction extends AbstractAction
{
    protected RatePlanRepository $ratePlanRepository;

    public function __construct(
        RatePlanRepository $ratePlanRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->ratePlanRepository = $ratePlanRepository;
    }


    protected function serializeList(RatePlan $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(RatePlan $model): array
    {
        $serializer = $this->serializer;


        return [
            'id' => $serializer->asUuid($model->getId()),
            'hotel' => $serializer->asUuid($model->getHotel()->getId()),
            'name' => $serializer->asArray($model->getName()),
            'description' => $serializer->asArray($model->getDescription()),
            'ratePlanGroup' => $serializer->asUuid($model->getRatePlanGroup()->getId()),
            'foodType' => $serializer->asUuid($model->getFoodType()->getId()),
            'taxType' => $serializer->asUuid($model->getTaxType()->getId()),
            'isActive' => $model->getIsActive(),
            'activeFrom' => $serializer->asDate($model->getActiveFrom()),
            'activeTo' => $serializer->asDate($model->getActiveTo()),
            'shownFrom' => $serializer->asDate($model->getShownFrom()),
            'shownTo' => $serializer->asDate($model->getShownTo()),
            'isShownWithPackets' => $model->getIsShownWithPackets(),
            'extraCondition' => $model->getExtraCondition() !== null
                ? $serializer->asArray($model->getExtraCondition()) : null,
            'canBeCanceledDate' => $model->getCanBeCanceledDate() !== null
                ? $serializer->asDate($model->getCanBeCanceledDate()) : null
        ];
    }
}
