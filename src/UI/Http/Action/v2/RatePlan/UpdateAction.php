<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlan;

use App\Hotel\Command\RatePlan\Update\Command;
use App\Hotel\Model\RatePlan\RatePlan;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/rate-plan/{id}",
 *  tags={"Отели"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование RatePlan",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="ratePlanGroup", type="string", format="uuid"),
 *              @OA\Property(property="foodType", type="string", format="uuid"),
 *              @OA\Property(property="taxType", type="string", format="uuid"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="activeFrom", type="string"),
 *              @OA\Property(property="activeTo", type="string"),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="isShownWithPackets", type="boolean"),
 *              @OA\Property(property="canBeCanceledDate", type="string"),
 *              @OA\Property(property="extraCondition", type="object"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="ratePlanGroup", type="string", format="uuid"),
 *              @OA\Property(property="foodType", type="string", format="uuid"),
 *              @OA\Property(property="taxType", type="string", format="uuid"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="activeFrom", type="string"),
 *              @OA\Property(property="activeTo", type="string"),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="isShownWithPackets", type="boolean"),
 *              @OA\Property(property="extraCondition", type="object"),
 *              @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractRatePlanAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var RatePlan $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );

        if ($paramsExtractor->has('hotel')) {
            $command->setHotel($paramsExtractor->getString('hotel'));
        }
        
        if ($paramsExtractor->has('name')) {
            $command->setName($paramsExtractor->getSimpleArray('name'));
        }

        if ($paramsExtractor->has('description')) {
            $command->setDescription($paramsExtractor->getSimpleArray('description'));
        }

        if ($paramsExtractor->has('ratePlanGroup')) {
            $command->setRatePlanGroup($paramsExtractor->getString('ratePlanGroup'));
        }

        if ($paramsExtractor->has('foodType')) {
            $command->setFoodType($paramsExtractor->getString('foodType'));
        }

        if ($paramsExtractor->has('taxType')) {
            $command->setTaxType($paramsExtractor->getString('taxType'));
        }
        
        if ($paramsExtractor->has('isActive')) {
            $command->setIsActive($paramsExtractor->getBool('isActive'));
        }

        if ($paramsExtractor->has('activeFrom')) {
            $command->setActiveFrom($paramsExtractor->getString('activeFrom'));
        }

        if ($paramsExtractor->has('activeTo')) {
            $command->setActiveTo($paramsExtractor->getString('activeTo'));
        }

        if ($paramsExtractor->has('shownFrom')) {
            $command->setShownFrom($paramsExtractor->getString('shownFrom'));
        }

        if ($paramsExtractor->has('shownTo')) {
            $command->setShownTo($paramsExtractor->getString('shownTo'));
        }

        if ($paramsExtractor->has('isShownWithPackets')) {
            $command->setIsShownWithPackets($paramsExtractor->getBool('isShownWithPackets'));
        }

        if ($paramsExtractor->has('canBeCanceledDate')) {
            $command->setCanBeCanceledDate($paramsExtractor->getString('canBeCanceledDate'));
        }

        if ($paramsExtractor->has('extraCondition')) {
            $command->setExtraCondition($paramsExtractor->getSimpleArray('extraCondition'));
        }

        return $command;
    }
}
