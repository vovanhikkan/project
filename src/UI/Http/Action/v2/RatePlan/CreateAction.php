<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlan;

use App\Hotel\Command\RatePlan\Create\Command;
use App\Hotel\Model\RatePlan\RatePlan;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/rate-plan",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление RatePlan",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="ratePlanGroup", type="string", format="uuid"),
 *              @OA\Property(property="foodType", type="string", format="uuid"),
 *              @OA\Property(property="taxType", type="string", format="uuid"),
 *              @OA\Property(property="activeFrom", type="string"),
 *              @OA\Property(property="activeTo", type="string"),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="extraCondition", type="object"),
 *              @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="ratePlanGroup", type="string", format="uuid"),
 *              @OA\Property(property="foodType", type="string", format="uuid"),
 *              @OA\Property(property="taxType", type="string", format="uuid"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="activeFrom", type="string"),
 *              @OA\Property(property="activeTo", type="string"),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="isShownWithPackets", type="boolean"),
 *              @OA\Property(property="extraCondition", type="object"),
 *              @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractRatePlanAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var RatePlan $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        
        $command = new Command(
            $paramsExtractor->getString('hotel'),
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getSimpleArray('description'),
            $paramsExtractor->getString('ratePlanGroup'),
            $paramsExtractor->getString('foodType'),
            $paramsExtractor->getString('taxType'),
            $paramsExtractor->getString('activeFrom'),
            $paramsExtractor->getString('activeTo'),
            $paramsExtractor->getString('shownFrom'),
            $paramsExtractor->getString('shownTo')
        );

        if ($paramsExtractor->has('isActive')) {
            $command->setIsActive($paramsExtractor->getBool('isActive'));
        }

        if ($paramsExtractor->has('isShownWithPackets')) {
            $command->setIsShownWithPackets($paramsExtractor->getBool('isShownWithPackets'));
        }

        if ($paramsExtractor->has('extraCondition')) {
            $command->setExtraCondition($paramsExtractor->getSimpleArray('extraCondition'));
        }

        if ($paramsExtractor->has('canBeCanceledDate')) {
            $command->setCanBeCanceledDate($paramsExtractor->getString('canBeCanceledDate'));
        }

        return $command;
    }
}
