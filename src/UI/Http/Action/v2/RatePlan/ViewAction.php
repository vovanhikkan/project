<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlan;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/rate-plan/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр RatePlan",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="ratePlanGroup", type="string", format="uuid"),
 *              @OA\Property(property="foodType", type="string", format="uuid"),
 *              @OA\Property(property="taxType", type="string", format="uuid"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="activeFrom", type="string"),
 *              @OA\Property(property="activeTo", type="string"),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="isShownWithPackets", type="boolean"),
 *              @OA\Property(property="extraCondition", type="object"),
 *              @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractRatePlanAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->ratePlanRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
