<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlan;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/rate-plan",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список RatePlan",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="hotel", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="ratePlanGroup", type="string", format="uuid"),
 *              @OA\Property(property="foodType", type="string", format="uuid"),
 *              @OA\Property(property="taxType", type="string", format="uuid"),
 *              @OA\Property(property="isActive", type="boolean"),
 *              @OA\Property(property="activeFrom", type="string"),
 *              @OA\Property(property="activeTo", type="string"),
 *              @OA\Property(property="shownFrom", type="string"),
 *              @OA\Property(property="shownTo", type="string"),
 *              @OA\Property(property="isShownWithPackets", type="boolean"),
 *              @OA\Property(property="extraCondition", type="object"),
 *              @OA\Property(property="canBeCanceledDate", type="string", format="date"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractRatePlanAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->ratePlanRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
