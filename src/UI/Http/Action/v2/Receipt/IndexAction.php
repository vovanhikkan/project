<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Receipt;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *     path="/v2/receipt",
 *     tags={"Чеки"},
 *     description="Получение списка всех чеков",
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Items(
 *                     @OA\Property(property="id", type="string", format="uuid"),
 *                     @OA\Property(property="paymentId", type="string", format="uuid"),
 *                     @OA\Property(property="externalId", type="string"),
 *                     @OA\Property(property="status", type="integer"),
 *                     @OA\Property(property="createdAt", type="string")
 *                 )
 *             )
 *         )
 *     )
 * )
 */
class IndexAction extends AbstractReceiptAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $items = $this->receiptRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
