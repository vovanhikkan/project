<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\Receipt;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Order\Model\Receipt\Receipt;
use App\Order\Repository\ReceiptRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * AbstractReceiptAction.
 */
abstract class AbstractReceiptAction extends AbstractAction
{
    protected ReceiptRepository $receiptRepository;

    public function __construct(
        ReceiptRepository $receiptRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->receiptRepository = $receiptRepository;
    }

    protected function serializeItem(Receipt $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'paymentId' => $serializer->asUuid($model->getPayment()->getId()),
            'provider' => $serializer->asInt($model->getProvider()),
            'externalId' => $model->getExternalId(),
            'status' => $serializer->asInt($model->getStatus()),
            'createdAt' => $serializer->asDateTime($model->getCreatedAt())
        ];
    }
}
