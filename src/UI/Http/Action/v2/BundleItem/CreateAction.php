<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BundleItem;

use App\Bundle\Command\BundleItem\Create\Command;
use App\Bundle\Model\BundleItem\BundleItem;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Post(
 *  path="/v2/bundle-item",
 *  tags={"Пакеты"},
 *  security={{"bearerAuth":{}}},
 *  description="Добавление BundleItem",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="subName", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="photo", type="string", format="uuid"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="subName", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="photo", type="string", format="uuid"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *              @OA\Property(
 *                  property="widthType",
 *                  type="integer",
 *                   enum={100, 200, 300, 400},
 *                   description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *              ),
 *          )
 *      )
 *  )
 * )
 */
class CreateAction extends AbstractBundleItemAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var BundleItem $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $paramsExtractor->getSimpleArray('name'),
            $paramsExtractor->getSimpleArray('subName'),
        );

        if ($paramsExtractor->has('description')) {
            $command->setDescription($paramsExtractor->getSimpleArray('description'));
        }

        if ($paramsExtractor->has('photo')) {
            $command->setPhotoId($paramsExtractor->getString('photo'));
        }

        if ($paramsExtractor->has('place')) {
            $command->setPlaceId($paramsExtractor->getString('place'));
        }

        return $command;
    }
}
