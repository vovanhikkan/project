<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BundleItem;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Bundle\Model\BundleItem\BundleItem;
use App\Bundle\Repository\BundleItemRepository;
use App\Content\Model\Category\Category;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractBundleItemAction
 * @package App\UI\Http\Action\v2\BundleItem
 */
abstract class AbstractBundleItemAction extends AbstractAction
{
    protected BundleItemRepository $bundleItemRepository;

    public function __construct(
        BundleItemRepository $bundleItemRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->bundleItemRepository = $bundleItemRepository;
    }
    
    protected function serializeList(BundleItem $model): array
    {
        return $this->serializeItem($model);
    }

    protected function serializeItem(BundleItem $model): array
    {
        $serializer = $this->serializer;

        $place = $model->getPlace();
        $result = [
            'id' => $serializer->asUuid($model->getId()),
            'name' => $serializer->asArray($model->getName()),
            'subName' => $serializer->asArray($model->getSubName()),
            'description' => $serializer->asArray($model->getDescription()),
            'widthType' => $model->getWidthType() !== null ? $model->getWidthType()->getValue() : null,
            'place' => $place !== null ? array_map(
                function (Category $category) use ($serializer) {
                    return [
                        'web_icon' =>  $this->getFileData($category->getWebIcon()),
                        'mob_icon' =>  $this->getFileData($category->getMobileIcon()),
                        'web_icon_svg' => $this->getFileData($category->getWebIconSvg()),
                        'web_gradient_color1' => $serializer->asStringOrNull($category->getWebGradientColor1()),
                        'web_gradient_color2' => $serializer->asStringOrNull($category->getWebGradientColor2())
                    ];
                },
                $place->getCategories()
            ): [],
        ];
        
        return $result;
    }
}
