<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\BundleItem;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/bundle-item/{id}",
 *  tags={"Пакеты"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр BundleItem",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", format="uuid"),
 *              @OA\Property(property="name", type="object"),
 *              @OA\Property(property="subName", type="object"),
 *              @OA\Property(property="description", type="object"),
 *              @OA\Property(property="photo", type="string", format="uuid"),
 *              @OA\Property(property="place", type="string", format="uuid"),
 *              @OA\Property(
 *                  property="widthType",
 *                  type="integer",
 *                   enum={100, 200, 300, 400},
 *                   description="`100` — short, `200` — medium, `300` — large, `400` — extra large"
 *              ),
 *          )
 *      )
 *  )
 * )
 */

class ViewAction extends AbstractBundleItemAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->bundleItemRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
