<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanPrice;

use App\Application\Service\Validator;
use App\Auth\Service\AuthContext;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Repository\RatePlanPriceRepository;
use App\Storage\Service\File\Locator;
use App\UI\Http\Action\AbstractAction;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;

abstract class AbstractRatePlanPriceAction extends AbstractAction
{
    protected RatePlanPriceRepository $ratePlanPriceRepository;
    
    public function __construct(
        RatePlanPriceRepository $ratePlanPriceRepository,
        Validator $validator,
        CommandBus $bus,
        AuthContext $authContext,
        LoggerInterface $logger,
        Locator $locator
    ) {
        parent::__construct($validator, $bus, $authContext, $logger, $locator);
        $this->ratePlanPriceRepository = $ratePlanPriceRepository;
    }


    protected function serializeList(RatePlanPrice $model): array
    {
        return $this->serializeItem($model);
    }


    protected function serializeItem(RatePlanPrice $model): array
    {
        $serializer = $this->serializer;

        return [
            'id' => $serializer->asUuid($model->getId()),
            'ratePlan' => $serializer->asUuid($model->getRatePlan()->getId()),
            'room' => $serializer->asUuid($model->getRoom()->getId()),
            'date' => $serializer->asDate($model->getDate()),
            'mainPlaceValue' => $serializer->asInt($model->getMainPlaceValue()),
            'value' => $serializer->asInt($model->getValue())
        ];
    }
}
