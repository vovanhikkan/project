<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanPrice;

use App\Hotel\Command\RatePlanPrice\Update\Command;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\UI\Http\ParamsExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Patch(
 *  path="/v2/rate-plan-price/{id}",
 *  tags={"Отели"},
 * @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Редактирование RatePlanPrice",
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="ratePlan", type="string", type="uuid"),
 *              @OA\Property(property="room", type="string", type="uuid"),
 *              @OA\Property(property="date", type="string"),
 *              @OA\Property(property="mainPlaceValue", type="number"),
 *              @OA\Property(property="value", type="number"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="ratePlan", type="string", type="uuid"),
 *              @OA\Property(property="room", type="string", type="uuid"),
 *              @OA\Property(property="date", type="string"),
 *              @OA\Property(property="mainPlaceValue", type="number"),
 *              @OA\Property(property="value", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class UpdateAction extends AbstractRatePlanPriceAction
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->denyAccessNotAdministrator();

        $command = $this->deserialize($request);
        $this->validator->validate($command);
        /** @var RatePlanPrice $result */
        $result = $this->bus->handle($command);
        $data = $this->serializeItem($result);

        return $this->asJson($data, 201);
    }


    private function deserialize(ServerRequestInterface $request): Command
    {
        $paramsExtractor = new ParamsExtractor($request->getParsedBody() ?? []);

        $command = new Command(
            $this->resolveArg('id'),
        );
        
        if ($paramsExtractor->has('ratePlan')) {
            $command->setRatePlan($paramsExtractor->getString('ratePlan'));
        }
        
        if ($paramsExtractor->has('room')) {
            $command->setRoom($paramsExtractor->getString('room'));
        }
        
        if ($paramsExtractor->has('date')) {
            $command->setDate($paramsExtractor->getString('date'));
        }
        
        if ($paramsExtractor->has('mainPlaceValue')) {
            $command->setMainPlaceValue($paramsExtractor->getInt('mainPlaceValue'));
        }

        if ($paramsExtractor->has('value')) {
            $command->setValue($paramsExtractor->getInt('value'));
        }

        return $command;
    }
}
