<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanPrice;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/rate-plan-price",
 *  tags={"Отели"},
 *  security={{"bearerAuth":{}}},
 *  description="Список RatePlanPrice",
 *  @OA\Response(
 *      response="200",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="ratePlan", type="string", type="uuid"),
 *              @OA\Property(property="room", type="string", type="uuid"),
 *              @OA\Property(property="date", type="string"),
 *              @OA\Property(property="mainPlaceValue", type="number"),
 *              @OA\Property(property="value", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class IndexAction extends AbstractRatePlanPriceAction
{
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $items = $this->ratePlanPriceRepository->fetchAll();
        $data = array_map([$this, 'serializeItem'], $items);

        return $this->asJson($data);
    }
}
