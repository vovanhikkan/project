<?php

declare(strict_types=1);

namespace App\UI\Http\Action\v2\RatePlanPrice;

use App\Application\ValueObject\Uuid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @OA\Get(
 *  path="/v2/rate-plan-price/{id}",
 *  tags={"Отели"},
 *  @OA\Parameter(required=true, name="id", in="path"),
 *  security={{"bearerAuth":{}}},
 *  description="Просмотр RatePlanPrice",
 *  @OA\Response(
 *      response="201",
 *      description="",
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string", type="uuid"),
 *              @OA\Property(property="ratePlan", type="string", type="uuid"),
 *              @OA\Property(property="room", type="string", type="uuid"),
 *              @OA\Property(property="date", type="string"),
 *              @OA\Property(property="mainPlaceValue", type="number"),
 *              @OA\Property(property="value", type="number"),
 *          )
 *      )
 *  )
 * )
 */
class ViewAction extends AbstractRatePlanPriceAction
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $item = $this->ratePlanPriceRepository->get(new Uuid($this->resolveArg('id')));
        $data = $this->serializeItem($item);

        return $this->asJson($data);
    }
}
