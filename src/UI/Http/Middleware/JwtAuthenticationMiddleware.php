<?php

declare(strict_types=1);

namespace App\UI\Http\Middleware;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Tuupola\Middleware\JwtAuthentication;

/**
 * JsonBodyParserMiddleware.
 */
class JwtAuthenticationMiddleware implements MiddlewareInterface
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $setting = $this->container->get('settings')['jwt'];

        $middleware = new JwtAuthentication([
            'header' => $setting['header'] ?? 'Authorization',
            'regexp' => $setting['regexp'] ?? '/Bearer\s+(.*)$/i',
            'path'   => $setting['path'] ?? '/',
            'ignore' => $setting['ignore'] ?? [],
            'secure' => $setting['secure'] ?? true,
            'secret' => $setting['secret'] ?? 'secret',
            'error'  => function ($response, $arguments) {
                return $response->withStatus(StatusCodeInterface::STATUS_UNAUTHORIZED)
                    ->withHeader("Content-Type", "application/json")
                    ->getBody()->write(json_encode([
                        'status'  => StatusCodeInterface::STATUS_UNAUTHORIZED,
                        'data'    => null,
                        'errors'  => [
                            [
                                'code'    => StatusCodeInterface::STATUS_UNAUTHORIZED,
                                'message' => 'Не валидный токен',
                            ],
                        ],
                        'warning' => null,
                    ], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
            },
        ]);

        return $middleware->process($request, $handler);
    }
}
