<?php

declare(strict_types=1);

namespace App\Application\Model;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * TimestampableTrait.
 */
trait TimestampableTrait
{
    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    protected DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    protected ?DateTimeImmutable $updatedAt = null;

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
