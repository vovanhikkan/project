<?php

declare(strict_types=1);

namespace App\Application\Model;

use App\Application\ValueObject\Sort;
use Doctrine\ORM\Mapping as ORM;

/**
 * SortableTrait.
 */
trait SortableTrait
{
    /**
     * @ORM\Column(type="sort", columnDefinition="INT(11) UNSIGNED NOT NULL")
     */
    protected Sort $sort;

    public function getSort(): Sort
    {
        return $this->sort;
    }
}
