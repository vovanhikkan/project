<?php

declare(strict_types=1);

namespace App\Application\Service;

use App\Application\ValueObject\Phone;
use GuzzleHttp\Client;

/**
 * SmsSender.
 */
class SmsSender
{
    private string $login;
    private string $password;

    public function __construct()
    {
        $this->login = getenv('SMS_LOGIN');
        $this->password = getenv('SMS_PASSWORD');
    }

    public function send(Phone $phone, string $message): void
    {
        $data = [
            'login' => $this->login,
            'psw' => $this->password,
            'phones' => (string)$phone,
            'mes' => $message,
        ];

        (new Client())->get('https://smsc.ru/sys/send.php', ['query' => $data]);
    }
}
