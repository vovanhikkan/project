<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * MiddleName.
 */
final class MiddleName extends StringValueObject
{
}
