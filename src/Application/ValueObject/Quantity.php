<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * Quantity.
 */
class Quantity extends IntegerValueObject
{
}
