<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * Longitude.
 */
final class Longitude extends FloatValueObject
{
}
