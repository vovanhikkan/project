<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * LastName.
 */
final class LastName extends StringValueObject
{
}
