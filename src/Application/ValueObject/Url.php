<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

use Assert\Assertion;

/**
 * Url.
 */
final class Url extends StringValueObject
{
    public function __construct(string $value)
    {
        parent::__construct($value);
        Assertion::url($this->value);
    }
}
