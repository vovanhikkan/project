<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * Id.
 */
final class Id extends IntegerValueObject
{
}
