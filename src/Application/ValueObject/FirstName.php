<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * FirstName.
 */
final class FirstName extends StringValueObject
{
}
