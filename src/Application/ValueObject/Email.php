<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

use Assert\Assertion;

/**
 * Email.
 */
final class Email extends StringValueObject
{
    public function __construct(string $value)
    {
        parent::__construct($value);
        Assertion::email($value, 'Неверный e-mail', $this->getPropertyPath());
    }

    public function getPropertyPath(): ?string
    {
        return 'email';
    }
}
