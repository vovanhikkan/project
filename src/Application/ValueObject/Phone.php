<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * Phone.
 */
final class Phone extends StringValueObject
{
}
