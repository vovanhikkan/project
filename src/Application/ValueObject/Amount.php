<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

use InvalidArgumentException;

/**
 * Amount.
 */
final class Amount extends IntegerValueObject
{
    public static function fromRoubles(float $value): self
    {
        $value = $value * 100;

        if (filter_var($value, FILTER_VALIDATE_INT) === false) {
            throw new InvalidArgumentException('Неверная сумма');
        }

        return new self((int)$value);
    }

    public function toRoubles(): float
    {
        return $this->value / 100;
    }
}
