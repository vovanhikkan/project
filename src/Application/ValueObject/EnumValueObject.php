<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

use Assert\Assertion;

/**
 * EnumValueObject.
 */
abstract class EnumValueObject extends IntegerValueObject
{
    private static array $values = [];

    public function __construct($value)
    {
        parent::__construct($value);
        Assertion::inArray($this->value, self::getValues(), 'Incorrect value ' . get_class($this) . ' ' . $this->value);
    }

    public static function __callStatic($name, $arguments)
    {
        $value = strtoupper($name);
        return new static(constant(static::class . '::' . $value));
    }

    public static function getNames(): array
    {
        if (!isset(self::$values[static::class])) {
            $constants = (new \ReflectionClass(static::class))->getConstants();
            self::$values[static::class] = array_combine($constants, array_keys($constants));
        }

        return self::$values[static::class];
    }

    public static function getValues(): array
    {
        return array_keys(self::getNames());
    }

    public function getName(): string
    {
        return static::getNames()[$this->getValue()];
    }
}
