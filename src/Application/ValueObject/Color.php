<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * Color.
 */
final class Color extends StringValueObject
{
}
