<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

use Assert\Assertion;

/**
 * Uuid.
 */
class Uuid
{
    protected string $value;

    public function __construct($value)
    {
        Assertion::uuid($value, 'Неверный UUID', $this->getPropertyPath());
        $this->value = (string)$value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

    public static function generate(): self
    {
        return new static(\Ramsey\Uuid\Uuid::uuid4()->toString());
    }

    public function getPropertyPath(): ?string
    {
        return null;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function isEqual(self $value): bool
    {
        return $this->value === $value->getValue();
    }
}
