<?php

declare(strict_types=1);

namespace App\Application\ValueObject;

/**
 * Barcode.
 */
final class Barcode extends StringValueObject
{
}
