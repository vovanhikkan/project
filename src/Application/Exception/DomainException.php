<?php

declare(strict_types=1);

namespace App\Application\Exception;

/**
 * DomainException.
 */
class DomainException extends \DomainException
{
}
