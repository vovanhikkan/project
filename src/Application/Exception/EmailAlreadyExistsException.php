<?php

declare(strict_types=1);

namespace App\Application\Exception;

use App\Application\ValueObject\Email;
use Throwable;

/**
 * EmailAlreadyExistsException.
 */
class EmailAlreadyExistsException extends DomainException
{
    public const CODE = 0;

    public function __construct(Email $email, Throwable $previous = null)
    {
        parent::__construct(
            sprintf('E-mail "%s" уже существует', $email),
            self::CODE,
            $previous
        );
    }
}
