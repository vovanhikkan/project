<?php

declare(strict_types=1);

namespace App\Application\Exception;

/**
 * Class InvalidRoleDeleteReviewException
 * @package App\Application\Exception
 */
class InvalidRoleDeleteReviewException extends InvalidArgumentException
{
}
