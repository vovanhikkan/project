<?php

declare(strict_types=1);

namespace App\Application\Exception;

use App\Application\ValueObject\Phone;
use Throwable;

/**
 * PhoneAlreadyExistsException.
 */
class PhoneAlreadyExistsException extends DomainException
{
    public const CODE = 0;

    public function __construct(Phone $phone, Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Телефон "%s" уже существует', $phone),
            self::CODE,
            $previous
        );
    }
}
