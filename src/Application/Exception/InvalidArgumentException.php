<?php

declare(strict_types=1);

namespace App\Application\Exception;

/**
 * InvalidArgumentException.
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}
