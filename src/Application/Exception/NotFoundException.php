<?php

declare(strict_types=1);

namespace App\Application\Exception;

/**
 * NotFoundException.
 */
class NotFoundException extends InvalidArgumentException
{
}
