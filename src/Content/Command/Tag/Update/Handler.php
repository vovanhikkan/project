<?php

declare(strict_types=1);

namespace App\Content\Command\Tag\Update;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\Tag;
use App\Content\Repository\TagRepository;
use App\Content\Service\Tag\Updater;
use App\Content\Model\Tag\Name;

class Handler
{
	private Updater $updater;
	private TagRepository $tagRepository;


	public function __construct(Updater $updater, TagRepository $tagRepository)
	{
		$this->tagRepository = $tagRepository;
		$this->updater = $updater;
	}


	public function handle(Command $command): Tag
	{
		$tag = $this->tagRepository->get(new Uuid($command->getId()));

		return $this->updater->update(
			$tag,
            new Name($command->getName())
		);
	}
}
