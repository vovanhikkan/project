<?php

declare(strict_types=1);

namespace App\Content\Command\Tag\Delete;

use App\Application\ValueObject\Uuid;
use App\Content\Service\Tag\Deleter;

class Handler
{
	private Deleter $deleter;


	public function __construct(Deleter $deleter)
	{
		$this->deleter = $deleter;
	}


	public function handle(Command $command): array
	{
		$this->deleter->delete(new Uuid($command->getId()));
		        return  ['message' => 'Tag удалена'];
	}
}
