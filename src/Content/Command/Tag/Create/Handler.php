<?php

declare(strict_types=1);

namespace App\Content\Command\Tag\Create;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\Tag;
use App\Content\Service\Tag\Creator;
use App\Content\Model\Tag\Name;

class Handler
{
	private Creator $creator;


	public function __construct(Creator $creator)
	{
		$this->creator = $creator;
	}


	public function handle(Command $command): Tag
	{
		return $this->creator->create(
            new Name($command->getName())
		);
	}
}
