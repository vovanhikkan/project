<?php

declare(strict_types=1);

namespace App\Content\Command\Category\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;

    /**
     * @Assert\NotBlank(message="Заполните название")
     */
    private array $name;

    public function __construct(string $id, array $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): array
    {
        return $this->name;
    }
}
