<?php

declare(strict_types=1);

namespace App\Content\Command\Category\Update;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Category\Category;
use App\Content\Model\Category\Name;
use App\Content\Service\Category\Updater;
use App\Content\Repository\CategoryRepository;

/**
 * Handler.
 */
class Handler
{
    private CategoryRepository $categoryRepository;
    private Updater $updater;

    public function __construct(CategoryRepository $categoryRepository, Updater $updater)
    {
        $this->categoryRepository = $categoryRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): Category
    {
        $category = $this->categoryRepository->get(new Uuid($command->getId()));

        $this->updater->update(
            $category,
            new Name($command->getName())
        );

        return $category;
    }
}
