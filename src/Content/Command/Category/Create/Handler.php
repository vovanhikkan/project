<?php

declare(strict_types=1);

namespace App\Content\Command\Category\Create;

use App\Content\Model\Category\Category;
use App\Content\Model\Category\Name;
use App\Content\Service\Category\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;

    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }

    public function handle(Command $command): Category
    {
        return $this->creator->create(
            new Name($command->getName())
        );
    }
}
