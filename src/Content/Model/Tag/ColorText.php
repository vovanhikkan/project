<?php

declare(strict_types=1);

namespace App\Content\Model\Tag;

use App\Application\ValueObject\StringValueObject;

/**
 * ColorText.
 */
final class ColorText extends StringValueObject
{
}
