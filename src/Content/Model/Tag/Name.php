<?php

declare(strict_types=1);

namespace App\Content\Model\Tag;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Name.
 */
final class Name extends ArrayValueObject
{
}
