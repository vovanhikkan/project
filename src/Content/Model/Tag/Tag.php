<?php

declare(strict_types=1);

namespace App\Content\Model\Tag;

use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;
use App\Application\ValueObject\Svg;

/**
 * Category.
 *
 * @ORM\Entity()
 * @ORM\Table(name="tag")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="content_tag_name")
     */
    private Name $name;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="icon", referencedColumnName="id")
     */
    private ?File $icon = null;

    /**
     * @ORM\Column(type="svg")
     */
    private ?Svg $iconSvg = null;

    /**
     * @ORM\Column(type="content_tag_color_text")
     */
    private ?ColorText $colorText = null;

    /**
     * @ORM\Column(type="content_tag_color_border")
     */
    private ?ColorBorder $colorBorder = null;

    /**
     * Tag constructor.
     * @param Uuid $id
     * @param Name $name
     * @param File|null $icon
     * @param Svg|null $iconSvg
     * @param ColorText|null $colorText
     * @param ColorBorder|null $colorBorder
     */
    public function __construct(
        Uuid $id,
        Name $name,
        ?File $icon,
        ?Svg $iconSvg,
        ?ColorText $colorText,
        ?ColorBorder $colorBorder
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->icon = $icon;
        $this->iconSvg = $iconSvg;
        $this->colorText = $colorText;
        $this->colorBorder = $colorBorder;
    }

    public function update(
        Name $name,
        ?File $icon = null,
        ?Svg $iconSvg = null,
        ?ColorText $colorText = null,
        ?ColorBorder $colorBorder = null
    ): void {
        $this->name = $name;
        $this->icon = $icon;
        $this->iconSvg = $iconSvg;
        $this->colorText = $colorText;
        $this->colorBorder = $colorBorder;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return File|null
     */
    public function getIcon(): ?File
    {
        return $this->icon;
    }

    /**
     * @param File|null $icon
     */
    public function setIcon(?File $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return Svg|null
     */
    public function getIconSvg(): ?Svg
    {
        return $this->iconSvg;
    }

    /**
     * @param Svg|null $iconSvg
     */
    public function setIconSvg(?Svg $iconSvg): void
    {
        $this->iconSvg = $iconSvg;
    }

    /**
     * @return ColorText|null
     */
    public function getColorText(): ?ColorText
    {
        return $this->colorText;
    }

    /**
     * @param ColorText|null $colorText
     */
    public function setColorText(?ColorText $colorText): void
    {
        $this->colorText = $colorText;
    }

    /**
     * @return ColorBorder|null
     */
    public function getColorBorder(): ?ColorBorder
    {
        return $this->colorBorder;
    }

    /**
     * @param ColorBorder|null $colorBorder
     */
    public function setColorBorder(?ColorBorder $colorBorder): void
    {
        $this->colorBorder = $colorBorder;
    }
}
