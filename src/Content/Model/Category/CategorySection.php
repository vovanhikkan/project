<?php

declare(strict_types=1);

namespace App\Content\Model\Category;

use App\Application\Model\SortableTrait;
use App\Application\ValueObject\Sort;
use App\Product\Model\Section\Section;
use Doctrine\ORM\Mapping as ORM;

/**
 * CategorySection.
 *
 * @ORM\Entity()
 * @ORM\Table(name="category_section")
 */
class CategorySection
{
    use SortableTrait;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="categorySections")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    private Category $category;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Product\Model\Section\Section")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id", nullable=false)
     */
    private Section $section;

    public function __construct(Category $category, Section $section)
    {
        $this->category = $category;
        $this->section = $section;
        $this->sort = new Sort(0);
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getSection(): Section
    {
        return $this->section;
    }
}
