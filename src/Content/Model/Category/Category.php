<?php

declare(strict_types=1);

namespace App\Content\Model\Category;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Color;
use App\Application\ValueObject\Svg;
use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category.
 *
 * @ORM\Entity()
 * @ORM\Table(name="category")
 */
class Category
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="content_category_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="color")
     */
    private ?Color $webGradientColor1 = null;

    /**
     * @ORM\Column(type="color")
     */
    private ?Color $webGradientColor2 = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="web_icon_id", referencedColumnName="id")
     */
    private ?File $webIcon = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="web_icon_svg", referencedColumnName="id")
     */
    private ?File $webIconSvg = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="mobile_icon_id", referencedColumnName="id")
     */
    private ?File $mobileIcon = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="mobile_background_id", referencedColumnName="id")
     */
    private ?File $mobileBackground = null;

    /**
     * @ORM\OneToMany(targetEntity="CategoryPlace", mappedBy="category", cascade={"all"})
     */
    private Collection $categoryPlaces;

    /**
     * @ORM\OneToMany(targetEntity="CategorySection", mappedBy="category", cascade={"all"})
     */
    private Collection $categorySections;

    public function __construct(
        Uuid $id,
        Name $name,
        bool $isActive,
        ?Color $webGradientColor1 = null,
        ?Color $webGradientColor2 = null,
        ?File $webIcon = null,
        ?File $webIconSvg = null,
        ?File $mobileIcon = null,
        ?File $mobileBackground = null,
        array $categoryPlaces = [],
        array $sections = []
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->webGradientColor1 = $webGradientColor1;
        $this->webGradientColor2 = $webGradientColor2;
        $this->webIcon = $webIcon;
        $this->webIconSvg = $webIconSvg;
        $this->mobileIcon = $mobileIcon;
        $this->mobileBackground = $mobileBackground;
        $this->categoryPlaces = new ArrayCollection($categoryPlaces);
        $this->categorySections = new ArrayCollection();
        foreach ($sections as $section) {
            $this->categorySections->add(new CategorySection($this, $section));
        }
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function update(
        Name $name,
        ?Color $webGradientColor1 = null,
        ?Color $webGradientColor2 = null,
        ?File $webIcon = null,
        ?File $webIconSvg = null,
        ?File $mobileIcon = null,
        ?File $mobileBackground = null,
        array $categoryPlaces = [],
        array $categorySections = []
    ): void {
        $this->name = $name;
        $this->webGradientColor1 = $webGradientColor1;
        $this->webGradientColor2 = $webGradientColor2;
        $this->webIcon = $webIcon;
        $this->webIconSvg = $webIconSvg;
        $this->mobileIcon = $mobileIcon;
        $this->mobileBackground = $mobileBackground;
        $this->categoryPlaces = new ArrayCollection($categoryPlaces);
        $this->categorySections = new ArrayCollection($categorySections);
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getWebGradientColor1(): ?Color
    {
        return $this->webGradientColor1;
    }

    public function getWebGradientColor2(): ?Color
    {
        return $this->webGradientColor2;
    }

    public function getWebIcon(): ?File
    {
        return $this->webIcon;
    }

    public function getWebIconSvg(): ?File
    {
        return $this->webIconSvg;
    }

    public function getMobileIcon(): ?File
    {
        return $this->mobileIcon;
    }

    public function getMobileBackground(): ?File
    {
        return $this->mobileBackground;
    }

    /**
     * @return CategoryPlace[]
     */
    public function getCategoryPlaces(): array
    {
        return $this->categoryPlaces->toArray();
    }

    /**
     * @return CategorySection[]
     */
    public function getCategorySections(): array
    {
        return $this->categorySections->toArray();
    }
}
