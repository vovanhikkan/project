<?php

declare(strict_types=1);

namespace App\Content\Model\Category;

use App\Application\Model\SortableTrait;
use App\Place\Model\Place\Place;
use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryPlace.
 *
 * @ORM\Entity()
 * @ORM\Table(name="category_place")
 */
class CategoryPlace
{
    use SortableTrait;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="categoryPlaces")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    private Category $category;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Place\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false)
     */
    private Place $place;

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getPlace(): Place
    {
        return $this->place;
    }
}
