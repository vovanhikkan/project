<?php

declare(strict_types=1);

namespace App\Content\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\Name;
use App\Content\Model\Tag\Tag;

/**
 * TagRepository.
 */
class TagRepository extends AbstractRepository
{
    public function add(Tag $category): void
    {
        $this->entityManager->persist($category);
    }

    /**
     * @return Tag[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('t');

        return $qb->getQuery()->getResult();
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    public function get(Uuid $id): Tag
    {
        /** @var Tag|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Tag with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getByNameOrNull(Name $name): ?Tag
    {
        /** @var Tag|null $model */
        $model = $this->entityRepository->findOneBy(['name' => $name->getValue()]);

        return $model;
    }

    protected function getModelClassName(): string
    {
        return Tag::class;
    }
}
