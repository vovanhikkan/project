<?php

declare(strict_types=1);

namespace App\Content\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Content\Model\Category\Category;

/**
 * CategoryRepository.
 */
class CategoryRepository extends AbstractRepository
{
    public function add(Category $category): void
    {
        $this->entityManager->persist($category);
    }

    /**
     * @return Category[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('c');
        $qb->orderBy('c.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Category
    {
        /** @var Category|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Category with id %s not found', (string)$id));
        }

        return $model;
    }

    protected function getModelClassName(): string
    {
        return Category::class;
    }
}
