<?php

declare(strict_types=1);

namespace App\Content\Service\Tag;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\ColorBorder;
use App\Content\Model\Tag\ColorText;
use App\Content\Model\Tag\Name;
use App\Data\Flusher;
use App\Content\Model\Tag\Tag;
use App\Content\Repository\TagRepository;
use App\Application\ValueObject\Svg;
use App\Storage\Model\File\File;

class Creator
{
	private Flusher $flusher;
	private TagRepository $tagRepository;


	public function __construct(TagRepository $tagRepository, Flusher $flusher)
	{
		$this->tagRepository = $tagRepository;
		$this->flusher = $flusher;
	}


	public function create(
        Name $name,
        ?File $icon = null,
        ?Svg $iconSvg = null,
        ?ColorText $colorText = null,
        ?ColorBorder $colorBorder = null
    ): Tag
	{
		$tag = new Tag(
            Uuid::generate(),
            $name,
            $icon,
            $iconSvg,
            $colorText,
            $colorBorder
		);

		$this->tagRepository->add($tag);
		$this->flusher->flush();

		return $tag;
	}
}
