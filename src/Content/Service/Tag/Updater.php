<?php

declare(strict_types=1);

namespace App\Content\Service\Tag;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\Name;
use App\Data\Flusher;
use App\Content\Model\Tag\Tag;
use App\Content\Repository\TagRepository;

class Updater
{
	private Flusher $flusher;
	private TagRepository $tagRepository;


	public function __construct(TagRepository $tagRepository, Flusher $flusher)
	{
		$this->tagRepository = $tagRepository;
		$this->flusher = $flusher;
	}


	public function update(Tag $tag, Name $name): Tag
	{
		$tag->update($name);
		$this->flusher->flush();
		return $tag;
	}
}
