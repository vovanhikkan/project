<?php

declare(strict_types=1);

namespace App\Content\Service\Tag;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Content\Repository\TagRepository;

class Deleter
{
	private Flusher $flusher;
	private TagRepository $tagRepository;


	public function __construct(TagRepository $tagRepository, Flusher $flusher)
	{
		$this->tagRepository = $tagRepository;
		$this->flusher = $flusher;
	}


	public function delete(Uuid $id): void
	{
		$this->tagRepository->delete($id);
		$this->flusher->flush();
	}
}
