<?php

declare(strict_types=1);

namespace App\Content\Service\Category;

use App\Content\Model\Category\Category;
use App\Content\Model\Category\Name;
use App\Data\Flusher;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function update(Category $category, Name $name): void
    {
        $category->update($name);
        $this->flusher->flush();
    }
}
