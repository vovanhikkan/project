<?php

declare(strict_types=1);

namespace App\Content\Service\Category;

use App\Application\ValueObject\Color;
use App\Application\ValueObject\Svg;
use App\Application\ValueObject\Uuid;
use App\Content\Model\Category\Category;
use App\Content\Model\Category\Name;
use App\Data\Flusher;
use App\Content\Repository\CategoryRepository;
use App\Storage\Model\File\File;

/**
 * Creator.
 */
class Creator
{
    private CategoryRepository $categoryRepository;
    private Flusher $flusher;

    public function __construct(CategoryRepository $categoryRepository, Flusher $flusher)
    {
        $this->categoryRepository = $categoryRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Name $name,
        ?Color $webGradientColor1 = null,
        ?Color $webGradientColor2 = null,
        ?File $webIcon = null,
        ?File $webIconSvg = null,
        ?File $mobileIcon = null,
        ?File $mobileBackground = null,
        array $categoryPlaces = [],
        array $categorySections = []
    ): Category
    {
        $category = new Category(
            Uuid::generate(),
            $name,
            true,
            $webGradientColor1,
            $webGradientColor2,
            $webIcon,
            $webIconSvg,
            $mobileIcon,
            $mobileBackground,
            $categoryPlaces,
            $categorySections
        );

        $this->categoryRepository->add($category);
        $this->flusher->flush();

        return $category;
    }
}
