<?php

declare(strict_types=1);

namespace App\Hotel\Service\TaxType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\TaxType\Name;
use App\Hotel\Model\TaxType\TaxType;
use App\Hotel\Model\TaxType\Value;
use App\Hotel\Repository\TaxTypeRepository;

class Creator
{
    private Flusher $flusher;
    private TaxTypeRepository $taxTypeRepository;


    public function __construct(TaxTypeRepository $taxTypeRepository, Flusher $flusher)
    {
        $this->taxTypeRepository = $taxTypeRepository;
        $this->flusher = $flusher;
    }


    public function create(Uuid $id, Name $name, Value $value): TaxType
    {
        $taxType = new TaxType(
            $id,
            $name,
            $value
        );

        $this->taxTypeRepository->add($taxType);
        $this->flusher->flush();

        return $taxType;
    }
}
