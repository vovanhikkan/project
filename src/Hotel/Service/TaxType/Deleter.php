<?php

declare(strict_types=1);

namespace App\Hotel\Service\TaxType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\TaxTypeRepository;

class Deleter
{
    private Flusher $flusher;
    private TaxTypeRepository $taxTypeRepository;


    public function __construct(TaxTypeRepository $taxTypeRepository, Flusher $flusher)
    {
        $this->taxTypeRepository = $taxTypeRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->taxTypeRepository->delete($id);
        $this->flusher->flush();
    }
}
