<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlan;

use App\Data\Flusher;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\RatePlan\Description;
use App\Hotel\Model\RatePlan\ExtraCondition;
use App\Hotel\Model\RatePlan\Name;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Model\TaxType\TaxType;
use App\Hotel\Repository\RatePlanRepository;
use DateTimeImmutable;

class Updater
{
    private Flusher $flusher;
    private RatePlanRepository $ratePlanRepository;


    public function __construct(RatePlanRepository $ratePlanRepository, Flusher $flusher)
    {
        $this->ratePlanRepository = $ratePlanRepository;
        $this->flusher = $flusher;
    }


    public function update(
        RatePlan $ratePlan,
        ?Hotel $hotel,
        ?Name $name,
        ?Description $description,
        ?RatePlanGroup $ratePlanGroup,
        ?FoodType $foodType,
        ?TaxType $taxType,
        ?bool $isActive,
        ?DateTimeImmutable $activeFrom,
        ?DateTimeImmutable $activeTo,
        ?DateTimeImmutable $shownFrom,
        ?DateTimeImmutable $shownTo,
        ?bool $isShownWithPackets,
        ?DateTimeImmutable $canBeCanceledDate,
        ?ExtraCondition $extraCondition
    ): RatePlan {
        
        $ratePlan->update(
            $hotel,
            $name,
            $description,
            $ratePlanGroup,
            $foodType,
            $taxType,
            $isActive,
            $activeFrom,
            $activeTo,
            $shownFrom,
            $shownTo,
            $isShownWithPackets,
            $canBeCanceledDate,
            $extraCondition
        );
        
        $this->flusher->flush();
        return $ratePlan;
    }
}
