<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlan;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Bundle\Repository\BundleRepository;
use App\Data\Flusher;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\RatePlan\Description;
use App\Hotel\Model\RatePlan\ExtraCondition;
use App\Hotel\Model\RatePlan\Name;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Model\TaxType\TaxType;
use App\Hotel\Repository\RatePlanRepository;
use DateTimeImmutable;

class Creator
{
    private Flusher $flusher;
    private RatePlanRepository $ratePlanRepository;
    private BundleRepository $bundleRepository;

    public function __construct(
        RatePlanRepository $ratePlanRepository,
        Flusher $flusher,
        BundleRepository $bundleRepository
    ) {
        $this->ratePlanRepository = $ratePlanRepository;
        $this->flusher = $flusher;
        $this->bundleRepository = $bundleRepository;
    }


    public function create(
        Uuid $id,
        Hotel $hotel,
        Name $name,
        Description $description,
        RatePlanGroup $ratePlanGroup,
        FoodType $foodType,
        TaxType $taxType,
        DateTimeImmutable $activeFrom,
        DateTimeImmutable $activeTo,
        DateTimeImmutable $shownFrom,
        DateTimeImmutable $shownTo,
        ?array $bundles,
        ?ExtraCondition $extraCondition,
        ?DateTimeImmutable $canBeCanceled
    ): RatePlan {

        $isActive = true;

        $ratePlan = new RatePlan(
            $id,
            $hotel,
            $name,
            $description,
            $ratePlanGroup,
            $foodType,
            $taxType,
            $isActive,
            $activeFrom,
            $activeTo,
            $shownFrom,
            $shownTo
        );

        $ratePlan->setIsShownWithPackets(true);

        if ($extraCondition) {
            $ratePlan->setExtraCondition($extraCondition);
        }

        if ($canBeCanceled) {
            $ratePlan->setCanBeCanceledDate($canBeCanceled);
        }

        if ($bundles && count($bundles)) {
            foreach ($bundles as $bundle) {
                /**
                 * @var Bundle $bundle
                 */
                $newBundle = $this->bundleRepository->getByNameOrNull($bundle->getName());

                if ($newBundle !== null) {
                    $ratePlan->addBundle($newBundle);
                    continue;
                }

                //создание нового bundle

                $ratePlan->addBundle($newBundle);
            }
        }
        

        $this->ratePlanRepository->add($ratePlan);
        $this->flusher->flush();

        return $ratePlan;
    }
}
