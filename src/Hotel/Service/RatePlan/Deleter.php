<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlan;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\RatePlanRepository;

class Deleter
{
    private Flusher $flusher;
    private RatePlanRepository $ratePlanRepository;


    public function __construct(RatePlanRepository $ratePlanRepository, Flusher $flusher)
    {
        $this->ratePlanRepository = $ratePlanRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->ratePlanRepository->delete($id);
        $this->flusher->flush();
    }
}
