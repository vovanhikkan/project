<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlanPrice;

use App\Application\ValueObject\Amount;
use App\Data\Flusher;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Model\Room\Room;
use App\Hotel\Repository\RatePlanPriceRepository;
use DateTimeImmutable;

class Updater
{
    private Flusher $flusher;
    private RatePlanPriceRepository $ratePlanPriceRepository;


    public function __construct(RatePlanPriceRepository $ratePlanPriceRepository, Flusher $flusher)
    {
        $this->ratePlanPriceRepository = $ratePlanPriceRepository;
        $this->flusher = $flusher;
    }


    public function update(
        RatePlanPrice $ratePlanPrice,
        ?RatePlan $ratePlan,
        ?Room $room,
        ?DateTimeImmutable $date,
        ?Amount $mainPlaceValue,
        ?Amount $value
    ): RatePlanPrice {
        
        $ratePlanPrice->update(
            $ratePlan,
            $room,
            $date,
            $mainPlaceValue,
            $value
        );
        
        $this->flusher->flush();
        return $ratePlanPrice;
    }
}
