<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlanPrice;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\RatePlanPriceRepository;

class Deleter
{
    private Flusher $flusher;
    private RatePlanPriceRepository $ratePlanPriceRepository;


    public function __construct(RatePlanPriceRepository $ratePlanPriceRepository, Flusher $flusher)
    {
        $this->ratePlanPriceRepository = $ratePlanPriceRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->ratePlanPriceRepository->delete($id);
        $this->flusher->flush();
    }
}
