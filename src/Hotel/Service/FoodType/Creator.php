<?php

declare(strict_types=1);

namespace App\Hotel\Service\FoodType;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\FoodType\Name;
use App\Hotel\Repository\FoodTypeRepository;

class Creator
{
    private Flusher $flusher;
    private FoodTypeRepository $foodTypeRepository;


    public function __construct(FoodTypeRepository $foodTypeRepository, Flusher $flusher)
    {
        $this->foodTypeRepository = $foodTypeRepository;
        $this->flusher = $flusher;
    }


    public function create(Uuid $id, Name $name): FoodType
    {
        $sort = $this->foodTypeRepository->getMaxSort();
        $foodType = new FoodType(
            $id,
            $name,
            true,
            new Sort($sort + 1)
        );

        $this->foodTypeRepository->add($foodType);
        $this->flusher->flush();

        return $foodType;
    }
}
