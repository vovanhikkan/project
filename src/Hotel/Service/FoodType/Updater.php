<?php

declare(strict_types=1);

namespace App\Hotel\Service\FoodType;

use App\Data\Flusher;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\FoodType\Name;
use App\Hotel\Repository\FoodTypeRepository;

class Updater
{
    private Flusher $flusher;
    private FoodTypeRepository $foodTypeRepository;


    public function __construct(FoodTypeRepository $foodTypeRepository, Flusher $flusher)
    {
        $this->foodTypeRepository = $foodTypeRepository;
        $this->flusher = $flusher;
    }


    public function update(FoodType $foodType, ?Name $name): FoodType
    {
        $foodType->update($name);
        $this->flusher->flush();
        return $foodType;
    }
}
