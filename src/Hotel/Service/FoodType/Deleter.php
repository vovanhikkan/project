<?php

declare(strict_types=1);

namespace App\Hotel\Service\FoodType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\FoodTypeRepository;

class Deleter
{
    private Flusher $flusher;
    private FoodTypeRepository $foodTypeRepository;


    public function __construct(FoodTypeRepository $foodTypeRepository, Flusher $flusher)
    {
        $this->foodTypeRepository = $foodTypeRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->foodTypeRepository->delete($id);
        $this->flusher->flush();
    }
}
