<?php

declare(strict_types=1);

namespace App\Hotel\Service\FacilityGroup;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\FacilityGroupRepository;

class Deleter
{
    private Flusher $flusher;
    private FacilityGroupRepository $facilityGroupRepository;


    public function __construct(
        FacilityGroupRepository $facilityGroupRepository,
        Flusher $flusher
    ) {
        $this->facilityGroupRepository = $facilityGroupRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->facilityGroupRepository->delete($id);
        $this->flusher->flush();
    }
}
