<?php

declare(strict_types=1);

namespace App\Hotel\Service\FacilityGroup;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\Hotel\Model\FacilityGroup\Name;
use App\Hotel\Repository\FacilityGroupRepository;
use App\Storage\Model\File\File;

class Updater
{
    private Flusher $flusher;
    private FacilityGroupRepository $facilityGroupRepository;


    public function __construct(
        FacilityGroupRepository $facilityGroupRepository,
        Flusher $flusher
    ) {
        $this->facilityGroupRepository = $facilityGroupRepository;
        $this->flusher = $flusher;
    }


    public function update(
        FacilityGroup $facilityGroup,
        ?Name $name,
        ?bool $isActive,
        ?bool $isShownHotel,
        ?bool $isShownRoom,
        ?File $mobIcon,
        ?File $webIcon
    ): FacilityGroup {
        $facilityGroup->update($name, $isActive, $isShownHotel, $isShownRoom, $mobIcon, $webIcon);
        $this->flusher->flush();
        return $facilityGroup;
    }
}
