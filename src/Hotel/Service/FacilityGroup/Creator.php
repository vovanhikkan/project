<?php

declare(strict_types=1);

namespace App\Hotel\Service\FacilityGroup;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\Hotel\Model\FacilityGroup\Name;
use App\Hotel\Repository\FacilityGroupRepository;
use App\Storage\Model\File\File;

class Creator
{
    private Flusher $flusher;
    private FacilityGroupRepository $facilityGroupRepository;


    public function __construct(FacilityGroupRepository $facilityGroupRepository, Flusher $flusher)
    {
        $this->facilityGroupRepository = $facilityGroupRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Name $name,
        bool $isActive,
        bool $isShownHotel,
        bool $isShownRoom,
        ?File $mobileIcon,
        ?File $webIcon
    ): FacilityGroup {
        $sort = $this->facilityGroupRepository->getMaxSort() + 1;
        $facilityGroup = new FacilityGroup(
            $id,
            $name,
            $isActive,
            $isShownHotel,
            $isShownRoom,
            new Sort($sort)
        );

        if ($mobileIcon) {
            $facilityGroup->setMobileIcon($mobileIcon);
        }

        if ($webIcon) {
            $facilityGroup->setWebIcon($webIcon);
        }

        $this->facilityGroupRepository->add($facilityGroup);
        $this->flusher->flush();

        return $facilityGroup;
    }
}
