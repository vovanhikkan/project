<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\RatePlan\RatePlan;
use DateTimeImmutable;

class Filter extends AbstractFilter
{

    public function do(
        array $serializedHotels,
        array $hotels,
        ?DateTimeImmutable $start,
        ?DateTimeImmutable $end,
        ?int $priceFrom,
        ?int $priceTo
    ): array {
        
        
        $this->checkPrice($priceFrom, $priceTo);
        
        if (!$this->checkDate($start, $end)) {
            return $serializedHotels;
        }
        
        $ready = $this->dateActivated ? $this->getReadyHotels($hotels) : [];

        $result = [];
        foreach ($serializedHotels as $key => $hotel) {
            if (!isset($ready[$hotel['id']]) && $this->dateActivated) {
                continue;
            }

            if ($this->dateActivated) {
                $rooms = [];
                foreach ($ready[$hotel['id']] as $roomId => $room) {
                    $tmp = [
                        'mainPlaceValue' => 0,
                        'value' => 0
                    ];
                    foreach ($room['prices'] as $date) {
                        $tmp['mainPlaceValue'] += $date['mainPlaceValue'];
                        $tmp['value'] += $date['value'];
                    }
                    $tmp['name'] = $room['name'];
                    $tmp['hasOtherRooms'] = count($ready[$hotel['id']]) > 1;

                    if ($this->priceActivated) {
                        if ($this->priceFrom !== null && $this->priceFrom > $tmp['mainPlaceValue']) {
                            continue;
                        }
                        if ($this->priceTo !== null && $this->priceTo < $tmp['mainPlaceValue']) {
                            continue;
                        }
                    }

                    $rooms[$roomId] = $tmp;
                }

                if (!count($rooms)) {
                    continue;
                }

                $min = array_shift($rooms);
                if (count($rooms) > 1) {
                    $min = $this->minPrice($rooms);
                }

                $hotel['rooms']['name'] = $min['name'];
                $hotel['rooms']['mainPlaceValue'] = $min['mainPlaceValue'];
                $hotel['rooms']['value'] = $min['value'];
                $hotel['rooms']['hasOtherRooms'] = $min['hasOtherRooms'];
            }
            $result[] = $hotel;
        }
        return $result;
    }


    public function doCount(
        array $hotels
    ): array {

        /** @var Hotel $hotel */
        $result = [];
        foreach ($hotels as $hotel) {
            $tmpTypeId = $hotel->getType()->getId()->getValue();
            if (!$result['hotelType'][$tmpTypeId]) {
                $result['hotelType'][$tmpTypeId]['count'] = 0;
                $result['hotelType'][$tmpTypeId]['name'] = $hotel->getType()->getName()->getValue();
            }
            $result['hotelType'][$tmpTypeId]['count'] += 1;

            /** @var Facility $facility */
            foreach ($hotel->getFacilities() as $facility) {
                if ($facility->getIsShownInSearch()) {
                    $tmpFacilityId = $facility->getId()->getValue();
                    if (!$result['facility'][$tmpFacilityId]) {
                        $result['facility'][$tmpFacilityId]['count'] = 0;
                        $result['facility'][$tmpFacilityId]['name'] = $facility->getName()->getValue();
                    }
                    $result['facility'][$tmpFacilityId]['count'] += 1;
                }
            }

            $rooms = $hotel->getRooms();
            foreach ($rooms as $room) {
                $tmpRatePlans = $this->getRatePlansForCount($room->getRatePlans());
                if (count($tmpRatePlans)) {
                    /** @var RatePlan $ratePlan */
                    foreach ($tmpRatePlans as $ratePlan) {
                        $tmpFoodTypeId = $ratePlan->getFoodType()->getId()->getValue();
                        if (!$result['foodType'][$tmpFoodTypeId]) {
                            $result['foodType'][$tmpFoodTypeId]['count'] = 0;
                            $result['foodType'][$tmpFoodTypeId]['name'] =
                                $ratePlan->getFoodType()->getName()->getValue();
                        }
                        $result['foodType'][$tmpFoodTypeId]['count'] += 1;
                    }
                }
            }
        }

        return $result;
    }


}
