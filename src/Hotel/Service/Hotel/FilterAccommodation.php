<?php


namespace App\Hotel\Service\Hotel;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Model\Room\Room;
use App\Hotel\Model\RoomQuota\RoomQuota;
use App\Hotel\Model\RoomQuotaValue\RoomQuotaValue;
use App\Order\Repository\OrderRepository;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use function Symfony\Component\Translation\t;

class FilterAccommodation
{

    private OrderRepository $orderRepository;
    private bool $dateActivated;
    private array $dates = [];
    private bool $canBeCanceled;
    private FoodType $foodType;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param array $serializedRooms
     * @param array $rooms
     * @param bool $canBeCanceled
     * @param FoodType $foodType
     * @param DateTimeImmutable|null $start
     * @param DateTimeImmutable|null $end
     * @return array
     */
    public function do(
        array $serializedRooms,
        array $rooms,
        bool $canBeCanceled,
        FoodType $foodType,
        ?DateTimeImmutable $start,
        ?DateTimeImmutable $end
    ): array {

        $this->dateActivated = $start !== null && $end !== null;

        $this->canBeCanceled = $canBeCanceled;
        $this->foodType = $foodType;

        if (!$this->dateActivated) {
            return $serializedRooms;
        }
        
        $period = new DatePeriod($start, new DateInterval('P1D'), $end->modify('+1 day'));

        $this->dates = [];
        foreach ($period as $date) {
            $this->dates[] = $date->format('Y-m-d');
        }

        $ready = $this->dateActivated ? $this->getReadyRoomsWithDates($rooms) : [];

        $result = [];

        if ($ready) {
            if ($this->dateActivated) {
                $rooms = [];
                foreach ($ready as $roomId => $room) {
//                    $tmp = [
//                        'mainPlaceValue' => 0,
//                        'value' => 0
//                    ];
//                    foreach ($room['prices'] as $date) {
//                        $tmp['mainPlaceValue'] += $date['mainPlaceValue'];
//                        $tmp['value'] += $date['value'];
//                    }

                    $tmp['id'] = $roomId;
                    $tmp['name'] = $room['name'];
                    $tmp['description'] = $room['description'];
                    $tmp['area'] = $room['area'];
                    $tmp['mainPlaceCount'] = $room['mainPlaceCount'];
                    $tmp['extraPlaceCount'] = $room['extraPlaceCount'];
                    $tmp['zeroPlaceCount'] = $room['zeroPlaceCount'];
                    $tmp['photos'] = $room['photos'];
                    $tmp['bedTypes'] = $room['bedTypes'];
                    $tmp['ratePlans'] = $room['ratePlans'];
                    $tmp['facilities'] = $room['facilities'];
                    $tmp['tags'] = $room['tags'];

                    $lastOrder = $this->orderRepository->findLastOrderByRoomId(new Uuid($roomId));
                    if ($lastOrder) {
                        $tmp['lastOrder'] = $lastOrder->getCreatedAt()->format(DateTimeImmutable::ATOM);
                    } else {
                        $tmp['lastOrder'] = null;
                    }

                    $rooms[] = $tmp;
                }
            }
            $result = $rooms;
        }
        
        return $result;
    }

    private function getReadyRoomsWithDates(array $rooms): array
    {
        /**
         * @var Room $room
         */
        $readyRooms = [];

        foreach ($rooms as $room) {
            $tmp = $this->getReadyRatePlans($room->getRatePlans());
            if (!$tmp['dates']) {
                continue;
            }
            
            if (count($tmp['dates']) == count($this->dates)) {
                $readyRooms[$room->getId()->getValue()] = [
                    'name' => $room->getName()->getValue(),
                    'description' => $room->getDescription()->getValue(),
                    'area' => $room->getArea()->getValue(),
                    'mainPlaceCount' => $room->getMainPlaceCount()->getValue(),
                    'extraPlaceCount' => $room->getExtraPlaceCount()->getValue(),
                    'zeroPlaceCount' => $room->getZeroPlaceCount()->getValue(),
                    'photos' => $room->getPhotos(),
                    'bedTypes' => $room->getHotel()->getBedTypes(),
                    'ratePlans' => $tmp['ratePlans'],
                    'facilities' => $this->getFacilities($room),
                    'prices' => $tmp['dates'],
                    'tags' => $room->getTags(),
                ];
            }
        }

        return $readyRooms;
    }


    private function getFacilities(Room $room) : array
    {
        $result = [];

        /** @var Facility $facility */
        foreach ($room->getFacilities() as $facility) {
            if ($facility->isShownInRoomList()) {
                $result[] = $facility;
            }
        }

        return $result;
    }


    private function getReadyRatePlans(array $ratePlans): array
    {
        /**
         * @var RatePlan $ratePlan
         */
        $tmp = [];
        $tmpRatePlans = [];
        foreach ($ratePlans as $ratePlan) {
            $validRatePlan = false;
            $validFoodType = false;
            $validCanBeCanceled = false;

            if ((!is_null($ratePlan->getCanBeCanceledDate()) && $this->canBeCanceled) ||
                (is_null($ratePlan->getCanBeCanceledDate()) && !$this->canBeCanceled)) {
                $validCanBeCanceled = true;
            }

            if ($ratePlan->getFoodType()->getId()->getValue() === $this->foodType->getId()->getValue()) {
                $validFoodType = true;
            }


            if ($validCanBeCanceled && $validFoodType) {
                $validRatePlan = true;
            }

            if ($validRatePlan) {
                foreach ($this->dates as $date) {
                    $dateVO = new DateTimeImmutable($date);

                    if ($ratePlan->getActiveFrom() <= $dateVO &&
                        $ratePlan->getActiveTo() >= $dateVO) {
                        $minPrice = $this->processPrices($ratePlan->getRatePlanPrices(), $dateVO);
                        $tmp['dates'][$date][] = $minPrice;
                        //$tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['datePrice'][$date] = $minPrice;
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['foodType']['name'] =
                            $ratePlan->getFoodType()->getName()->getValue()['ru'];
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['foodType']['id'] =
                            $ratePlan->getFoodType()->getId()->getValue();

                        $ratePlan->getCanBeCanceledDate() ?
                            $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['canBeCanceledDate'] =
                                $ratePlan->getCanBeCanceledDate()->format('Y-m-d') :
                            $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['canBeCanceledDate'] =
                                $ratePlan->getCanBeCanceledDate();

                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['name'] =
                            $ratePlan->getName()->getValue();
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['description'] =
                            $ratePlan->getDescription()->getValue();
                        $extraCondition = null;
                        if ($ratePlan->getExtraCondition()) {
                            $extraCondition = $ratePlan->getExtraCondition()->getValue();
                        }
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['extraCondition'] = $extraCondition;
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['prices']['mainPlaceValue']
                            += $minPrice['mainPlaceValue'];
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['prices']['value'] +=
                            $minPrice['value'];

                        $maxTotalQuotaValue = 0;
                        $maxBookedQuotaValue = 0;
                        /** @var RoomQuota $roomQuota */
                        foreach ($ratePlan->getRoomQuotas() as $roomQuota) {
                            if ($roomQuota->isActive()) {
                                $roomQuotaValues = $roomQuota->getQuotaValuesByDate($dateVO);
                                if (count($roomQuotaValues)) {

                                    /** @var RoomQuotaValue $roomQuotaValue */
                                    foreach ($roomQuotaValues as $roomQuotaValue) {
                                        if ($roomQuotaValue->getTotalQuantity()->getValue() > $maxTotalQuotaValue) {
                                            $maxTotalQuotaValue = $roomQuotaValue->getTotalQuantity()->getValue();
                                        }
                                        if (($roomQuotaValue->getHoldedQuantity()->getValue() +
                                                $roomQuotaValue->getOrderedQuantity()->getValue()) >
                                            $maxBookedQuotaValue) {
                                            $maxBookedQuotaValue = $roomQuotaValue->getHoldedQuantity()->getValue() +
                                                $roomQuotaValue->getOrderedQuantity()->getValue();
                                        }
                                    }
                                }
                            }
                        }
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['totalQuotaValues'][$date]
                            = $maxTotalQuotaValue;
                        $tmpRatePlans['ratePlans'][$ratePlan->getId()->getValue()]['bookedQuotaValues'][$date]
                            = $maxBookedQuotaValue;
                    }
                }
            }
        }

        if (!empty($tmpRatePlans)) {
            $tmpRatePlans = $this->minQuotaValue($tmpRatePlans);
        }

        $result = [];
        if (!empty($tmpRatePlans['ratePlans'])) {
            $ratePlansWithDates = [];

            if ($tmp['dates']) {
                foreach ($tmp['dates'] as $key => $date) {
                    if (count($date)) {
                        $ratePlansWithDates['dates'][$key] = $this->minPrice($date);
                    }
                }
            }

            $result = array_merge($ratePlansWithDates, $tmpRatePlans);
        }

        return $result;
    }

    private function processPrices(array $ratePlanPrices, DateTimeImmutable $date): array
    {
        /**
         * @var RatePlanPrice $ratePlanPrice
         */
        $readyPrices = [];
        foreach ($ratePlanPrices as $ratePlanPrice) {
            $interval = $date->diff($ratePlanPrice->getDate());
            if ($interval->days == 0) {
                if ($ratePlanPrice->getMainPlaceValue() != null && $ratePlanPrice->getValue() != null
                ) {
                    $readyPrices[] = [
                        "mainPlaceValue" => $ratePlanPrice->getMainPlaceValue()->getValue(),
                        "value" => $ratePlanPrice->getValue()->getValue()
                    ];
                }
            }
        }

        if (count($readyPrices)) {
            return $this->minPrice($readyPrices);
        }
        return [];
    }

    private function minQuotaValue(array $ratePlans) : array
    {
        if (is_null($ratePlans['ratePlans'])) {
            return [];
        }
        foreach ($ratePlans['ratePlans'] as $key => $ratePlan) {
            $minTotalRooms = min($ratePlan['totalQuotaValues']);
            foreach ($ratePlan['totalQuotaValues'] as $date => $value) {
                $roomsLeft[$date] = $value - $ratePlan['bookedQuotaValues'][$date];
            }
            $minLeftRooms = min($roomsLeft);

            if ($minTotalRooms <= 0 || $minLeftRooms <= 0) {
                unset($ratePlans['ratePlans'][$key]);
            } else {
                $ratePlans['ratePlans'][$key]['roomsTotal'] = $minTotalRooms;
                $ratePlans['ratePlans'][$key]['roomsLeft'] = $minLeftRooms;
            }
        }

        return $ratePlans;
    }

    private function minPrice(array $items): array
    {
        $min = 0;
        foreach ($items as $key => $price) {
            if ($min == 0) {
                $min = $price;
            }

            if (isset($min['mainPlaceValue']) && $min['mainPlaceValue'] > $price['mainPlaceValue']) {
                $min = $price;
            }
        }

        return $min;
    }
}
