<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Model\Room\Room;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;

abstract class AbstractFilter
{
    protected bool $dateActivated;
    protected array $dates = [];

    protected ?bool $priceActivated = false;
    protected ?int $priceFrom = null;
    protected ?int $priceTo = null;

    /**
     * @param int $priceFrom
     * @param int $priceTo
     */
    protected function checkPrice(?int $priceFrom, ?int $priceTo) : void
    {
        $this->priceActivated = $priceFrom !== null || $priceTo !== null;
        if ($this->priceActivated) {
            $this->priceFrom = $priceFrom;
            $this->priceTo = $priceTo;
        }
    }

    /**
     * @param DateTimeImmutable|null $start
     * @param DateTimeImmutable|null $end
     * @return bool
     */
    protected function checkDate(?DateTimeImmutable $start, ?DateTimeImmutable $end) :bool
    {
        $this->dateActivated = $start !== null && $end !== null;

        if ($this->dateActivated) {
            $period = new DatePeriod($start, new DateInterval('P1D'), $end->modify('+1 day'));

            foreach ($period as $date) {
                $this->dates[] = $date->format('Y-m-d');
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * @param array $hotels
     * @return array
     */
    protected function getReadyHotels(array $hotels): array
    {
        /**
         * @var Hotel $hotel
         */
        $ready = [];
        foreach ($hotels as $hotel) {
            $tmp = $this->getReadyRoomsWithDates($hotel->getRooms());
            if (count($tmp)) {
                $ready[$hotel->getId()->getValue()] = $tmp;
            }
        }

        return $ready;
    }

    /**
     * @param array $rooms
     * @return array
     */
    protected function getReadyRoomsWithDates(array $rooms): array
    {
        /**
         * @var Room $room
         */
        $readyRooms = [];

        foreach ($rooms as $room) {
            $tmp = $this->getReadyRatePlans($room->getRatePlans());
            if (count($tmp) == count($this->dates)) {
                $readyRooms[$room->getId()->getValue()] = [
                    'name' => $room->getName()->getValue(),
                    'prices' => $tmp
                ];
            }
        }

        return $readyRooms;
    }

    /**
     * @param array $ratePlans
     * @return array
     * @throws \Exception
     */
    protected function getReadyRatePlans(array $ratePlans): array
    {
        /**
         * @var RatePlan $ratePlan
         */
        $tmp = [];
        foreach ($ratePlans as $ratePlan) {
            foreach ($this->dates as $date) {
                $dateVO = new DateTimeImmutable($date);
                if ($ratePlan->getActiveFrom() <= $dateVO && $ratePlan->getActiveTo() >= $dateVO) {
                    $tmp[$date][] = $this->processPrices($ratePlan->getRatePlanPrices(), $dateVO);
                }
            }
        }

        $readyRatePlans = [];
        foreach ($tmp as $key => $date) {
            if (count($date)) {
                $readyRatePlans[$key] = $this->minPrice($date);
            }
        }

        return $readyRatePlans;
    }

    /**
     * @param array $ratePlans
     * @return array
     * @throws \Exception
     */
    protected function getRatePlansForCount(array $ratePlans): array
    {
        /**
         * @var RatePlan $ratePlan
         */
        $readyRatePlans = [];
        foreach ($ratePlans as $ratePlan) {
            foreach ($this->dates as $date) {
                $dateVO = new DateTimeImmutable($date);
                if ($ratePlan->getActiveFrom() <= $dateVO && $ratePlan->getActiveTo() >= $dateVO) {
                    $readyRatePlans[] = $ratePlan;
                }
            }
        }

        return $readyRatePlans;
    }

    /**
     * @param array $ratePlanPrices
     * @return array
     */
    protected function processPrices(array $ratePlanPrices, DateTimeImmutable $date): array
    {
        /**
         * @var RatePlanPrice $ratePlanPrice
         */
        $readyPrices = [];
        foreach ($ratePlanPrices as $ratePlanPrice) {
            $interval = $date->diff($ratePlanPrice->getDate());
            if ($interval->days == 0) {
                if ($ratePlanPrice->getMainPlaceValue() != null && $ratePlanPrice->getValue() != null
                ) {
                    $readyPrices[] = [
                        "mainPlaceValue" => $ratePlanPrice->getMainPlaceValue()->getValue(),
                        "value" => $ratePlanPrice->getValue()->getValue()
                    ];
                }
            }
        }

        if (count($readyPrices)) {
            return $this->minPrice($readyPrices);
        }
        return [];
    }

    /**
     * @param array $items
     * @return array|int
     */
    protected function minPrice(array $items): array
    {
        $min = 0;
        foreach ($items as $key => $price) {
            if ($min == 0) {
                $min = $price;
                $min['id'] = $key;
            }

            if (isset($min['mainPlaceValue']) && $min['mainPlaceValue'] > $price['mainPlaceValue']) {
                $min = $price;
                $min['id'] = $key;
            }
        }

        return $min;
    }
}
