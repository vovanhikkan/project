<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Model\Room\Room;
use App\Hotel\Model\RoomInterval\RoomInterval;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;

class PriceCalculator
{

    public function get(Hotel $hotel, DateTimeImmutable $start, DateTimeImmutable $end): ?array
    {
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

        $dates = [];
        foreach ($period as $date) {
            $dates[] = $date->format('Y-m-d');
        }

        $pricesOnDate = [];
        /**
         * @var Room $room
         * @var RoomInterval $interval
         * @var RatePlan $ratePlan
         * @var RatePlanPrice $ratePlanPrice
         */
        $roomSum = [];
        foreach ($hotel->getRooms() as $room) {
            if (count($interval = $room->getIntervalsByDates($dates))) {
                $interval = $interval[0];
                $ratePlans = $room->getRatePlansById(explode(', ', $interval->getRatePlan()));

                foreach ($ratePlans as $ratePlan) {
                    foreach ($dates as $date) {
                        if ($ratePlan->getActiveFrom() <= new DateTimeImmutable($date) &&
                            new DateTimeImmutable($date) <= $ratePlan->getActiveTo()) {
                            $prices = $ratePlan->getRatePlanPrices();
                            if (!count($prices)) {
                                break;
                            }
                            $pricesOnDate[$date] = $prices[0]->getValue()->getValue();
                            foreach ($prices as $ratePlanPrice) {
                                if ($ratePlanPrice->getValue() &&
                                    $ratePlanPrice->getValue()->getValue() < $pricesOnDate[$date]) {
                                    $pricesOnDate[$date] = $ratePlanPrice->getValue()->getValue();
                                }
                            }
                        }
                    }
                }
                $sum = 0;
                foreach ($pricesOnDate as $price) {
                    $sum += $price;
                }

                $roomSum[]  = [
                    'room' => [
                        'id' => $room->getId()->getValue(),
                        'name' => $room->getName()->getValue(),
                    ],
                    'minPrice' => $sum
                ];
            }
        }

        if (!count($roomSum)) {
            return null;
        }

        $min = $roomSum[0];
        foreach ($roomSum as $room) {
            if ($room['minPrice'] < $min['minPrice']) {
                $min = $room;
            }
        }

        return $min;
    }
}
