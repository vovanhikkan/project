<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use DateTimeImmutable;

class FilterBundle extends AbstractFilter
{

    public function do(
        array $serializedHotels,
        array $hotels,
        DateTimeImmutable $start,
        DateTimeImmutable $end
    ): array {

        if (!$this->checkDate($start, $end)) {
            return $serializedHotels;
        }

        $ready = $this->dateActivated ? $this->getReadyHotels($hotels) : [];

        $result = [];
        foreach ($serializedHotels as $key => $hotel) {
            if (!isset($ready[$hotel['id']]) && $this->dateActivated) {
                continue;
            }

            if ($this->dateActivated) {
                $rooms = [];
                foreach ($ready[$hotel['id']] as $roomId => $room) {
                    $tmp = [
                        'mainPlaceValue' => 0,
                        'value' => 0
                    ];
                    foreach ($room['prices'] as $date) {
                        $tmp['mainPlaceValue'] += $date['mainPlaceValue'];
                        $tmp['value'] += $date['value'];
                    }
                    $tmp['name'] = $room['name'];

                    $rooms[$roomId] = $tmp;
                }

                if (!count($rooms)) {
                    continue;
                }

                foreach ($rooms as $roomId => $room) {
                    $tmp['id'] = $roomId;
                    $tmp['name'] = $room['name'];
                    $tmp['value'] = $room['value'];
                    $tmp['mainPlaceValue'] = $room['mainPlaceValue'];
                    $hotel['rooms']['filtered'][] = $tmp;
                }
            }
            $result[] = $hotel;
        }
        return $result;
    }
}
