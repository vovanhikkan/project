<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\Tag;
use App\Content\Repository\TagRepository;
use App\Data\Flusher;
use App\Hotel\Dto\HotelTagDto;
use App\Hotel\Model\Brand\Brand;
use App\Hotel\Model\Hotel\Address;
use App\Hotel\Model\Hotel\CancelPrepayment;
use App\Hotel\Model\Hotel\CheckInTime;
use App\Hotel\Model\Hotel\CheckOutTime;
use App\Hotel\Model\Hotel\Description;
use App\Hotel\Model\Hotel\ExtraBeds;
use App\Hotel\Model\Hotel\ExtraInfo;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\Hotel\Lat;
use App\Hotel\Model\Hotel\Links;
use App\Hotel\Model\Hotel\Lon;
use App\Hotel\Model\Hotel\Name;
use App\Hotel\Model\Hotel\Payments;
use App\Hotel\Model\Hotel\Pets;
use App\Hotel\Model\Hotel\StarCount;
use App\Hotel\Model\HotelType\HotelType;
use App\Hotel\Repository\HotelRepository;
use App\Location\Model\Location\Location;
use App\Storage\Model\File\File;

class Creator
{
    private Flusher $flusher;
    private HotelRepository $hotelRepository;
    private TagRepository $tagRepository;


    public function __construct(HotelRepository $hotelRepository, Flusher $flusher, TagRepository $tagRepository)
    {
        $this->hotelRepository = $hotelRepository;
        $this->tagRepository = $tagRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Name $name,
        ?Description $description,
        StarCount $starCount,
        ?Location $location,
        Address $address,
        ?File $logo,
        ?File $logoSvg,
        ?Links $links,
        ?Lat $lat,
        ?Lon $lon,
        HotelType $hotelType,
        ?Brand $brand,
        ?array $tags,
        ?array $places,
        ?CheckInTime $checkInTime,
        ?CheckOutTime $checkOutTime,
        ?CancelPrepayment $cancelPrepayment,
        ?ExtraBeds $extraBeds,
        ?Pets $pets,
        ?ExtraInfo $extraInfo,
        ?Payments $payments,
        ?File $hotelPhoto
    ): Hotel {
        $hotel = new Hotel(
            $id,
            $name,
            $starCount,
            $address,
            $hotelType
        );

        if ($description) {
            $hotel->setDescription($description);
        }

        if ($location) {
            $hotel->setLocation($location);
        }

        if ($logo) {
            $hotel->setLogo($logo);
        }

        if ($logoSvg) {
            $hotel->setLogoSvg($logoSvg);
        }
        
        if ($links) {
            $hotel->setLinks($links);
        }

        if ($lat) {
            $hotel->setLat($lat);
        }

        if ($lon) {
            $hotel->setLon($lon);
        }

        if ($brand) {
            $hotel->setBrand($brand);
        }

        if ($checkInTime) {
            $hotel->setCheckInTime($checkInTime);
        }

        if ($checkOutTime) {
            $hotel->setCheckOutTime($checkOutTime);
        }

        if ($cancelPrepayment) {
            $hotel->setCancelPrepayment($cancelPrepayment);
        }

        if ($extraBeds) {
            $hotel->setExtraBeds($extraBeds);
        }

        if ($pets) {
            $hotel->setPets($pets);
        }

        if ($extraInfo) {
            $hotel->setExtraInfo($extraInfo);
        }

        if ($payments) {
            $hotel->setPayments($payments);
        }

        if ($tags && count($tags)) {
            foreach ($tags as $tagDto) {
                /**
                 * @var HotelTagDto $tagDto
                 */
                $tag = $this->tagRepository->getByNameOrNull($tagDto->getName());

                if ($tag !== null) {
                    $hotel->addTag($tag);
                    continue;
                }

                $tag = new Tag(Uuid::generate(), $tagDto->getName());
                $tag->setIcon($tagDto->getIcon());
                $tag->setIconSvg($tagDto->getIconSvg());
                $tag->setColorText($tagDto->getColorText());
                $tag->setColorBorder($tagDto->getColorBorder());

                $hotel->addTag($tag);
            }
        }

        if ($places && count($places)) {
            foreach ($places as $place) {
                $hotel->addPlace($place);
            }
        }

        if ($hotelPhoto) {
            $hotel->setHotelPhoto($hotelPhoto);
        }

        $this->hotelRepository->add($hotel);
        $this->flusher->flush();

        return $hotel;
    }
}
