<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use App\Hotel\Model\Hotel\Hotel;
use App\Review\Model\Review\Review;
use App\Review\Model\ReviewStar\ReviewStar;

class FilterReview
{
    public function do(
        array $hotels,
        ?array $reviewValues
    ): array {

        if (!$reviewValues) {
            return $hotels;
        }

        $result = [];
        /** @var Hotel $hotel */
        foreach ($hotels as $key => $hotel) {
            $reviews = $hotel->getReviews();
            $hotelReviewStars = [];
            /** @var Review $review */
            foreach ($reviews as $review) {
                $reviewStars = $review->getReviewStars();
                /** @var ReviewStar $reviewStar */
                foreach ($reviewStars as $reviewStar) {
                    $hotelReviewStars[$reviewStar->getReviewStarType()->getId()->getValue()][]
                        = $reviewStar->getValue()->getValue();
                }
            }

            //calc avg
            $hotelReviewStarsAvg = [];
            foreach ($hotelReviewStars as $reviewStarType => $reviewStarsValue) {
                $starsSum = 0;
                foreach ($reviewStarsValue as $star) {
                    $starsSum += $star;
                }
                $hotelReviewStarsAvg[$reviewStarType] = round($starsSum / count($reviewStarsValue), 1);
            }

            //compare stars
            $highRating = true;
            foreach ($reviewValues as $type => $value) {
                if ($hotelReviewStarsAvg[$type] < $value) {
                    $highRating = false;
                    break;
                }
            }

            if ($highRating) {
                $result[] = $hotel;
            }
        }
        return $result;
    }
}
