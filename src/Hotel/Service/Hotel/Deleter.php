<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\HotelRepository;

class Deleter
{
    private Flusher $flusher;
    private HotelRepository $hotelRepository;


    public function __construct(HotelRepository $hotelRepository, Flusher $flusher)
    {
        $this->hotelRepository = $hotelRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->hotelRepository->delete($id);
        $this->flusher->flush();
    }
}
