<?php

declare(strict_types=1);

namespace App\Hotel\Service\Hotel;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\Brand\Brand;
use App\Hotel\Model\Hotel\Address;
use App\Hotel\Model\Hotel\CancelPrepayment;
use App\Hotel\Model\Hotel\CheckInTime;
use App\Hotel\Model\Hotel\CheckOutTime;
use App\Hotel\Model\Hotel\Description;
use App\Hotel\Model\Hotel\ExtraBeds;
use App\Hotel\Model\Hotel\ExtraInfo;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\Hotel\Lat;
use App\Hotel\Model\Hotel\Links;
use App\Hotel\Model\Hotel\Lon;
use App\Hotel\Model\Hotel\Name;
use App\Hotel\Model\Hotel\Payments;
use App\Hotel\Model\Hotel\Pets;
use App\Hotel\Model\Hotel\Rating;
use App\Hotel\Model\Hotel\StarCount;
use App\Hotel\Model\HotelType\HotelType;
use App\Hotel\Repository\HotelRepository;
use App\Location\Model\Location\Location;
use App\Storage\Model\File\File;

class Updater
{
    private Flusher $flusher;
    private HotelRepository $hotelRepository;


    public function __construct(HotelRepository $hotelRepository, Flusher $flusher)
    {
        $this->hotelRepository = $hotelRepository;
        $this->flusher = $flusher;
    }


    public function update(
        Hotel $hotel,
        ?Name $name,
        ?Description $description,
        ?StarCount $starCount,
        ?Location $location,
        ?Address $address,
        ?File $logo,
        ?File $logoSvg,
        ?Links $links,
        ?Lat $lat,
        ?Lon $lon,
        ?HotelType $hotelType,
        ?Brand $brand,
        ?CheckInTime $checkInTime,
        ?CheckOutTime $checkOutTime,
        ?CancelPrepayment $cancelPrepayment,
        ?ExtraBeds $extraBeds,
        ?Pets $pets,
        ?ExtraInfo $extraInfo,
        ?Payments $payments,
        ?File $hotelPhoto
    ): Hotel {
        $hotel->update(
            $name,
            $description,
            $starCount,
            $location,
            $address,
            $logo,
            $logoSvg,
            $links,
            $lat,
            $lon,
            $brand,
            $hotelType,
            $checkInTime,
            $checkOutTime,
            $cancelPrepayment,
            $extraBeds,
            $pets,
            $extraInfo,
            $payments,
            $hotelPhoto
        );
        $this->flusher->flush();
        return $hotel;
    }

    /**
     * @param Hotel $hotel
     * @param Rating $rating
     * @return Hotel
     */
    public function updateRating(
        Hotel $hotel,
        Rating $rating
    ) {
        $hotel->setRating($rating);
        $this->flusher->flush();
        return $hotel;
    }
}
