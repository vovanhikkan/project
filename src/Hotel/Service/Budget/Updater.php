<?php

declare(strict_types=1);

namespace App\Hotel\Service\Budget;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Budget\Budget;
use App\Hotel\Model\Budget\Name;
use App\Hotel\Model\Budget\BudgetFrom;
use App\Hotel\Model\Budget\BudgetTo;
use App\Application\ValueObject\Sort;
use App\Hotel\Repository\BudgetRepository;
use App\Data\Flusher;

class Updater
{
    private Flusher $flusher;
    private BudgetRepository $budgetRepository;


    public function __construct(BudgetRepository $budgetRepository, Flusher $flusher)
    {
        $this->budgetRepository = $budgetRepository;
        $this->flusher = $flusher;
    }

    /**
     * @param Budget $budget
     * @param Name|null $name
     * @param BudgetFrom|null $budgetFrom
     * @param BudgetTo|null $budgetTo
     * @param Sort|null $sort
     * @return Budget
     */
    public function update(
        Budget $budget,
        ?Name $name,
        ?BudgetFrom $budgetFrom,
        ?BudgetTo $budgetTo,
        ?Sort $sort
    ): Budget {
        $budget->update($name, $budgetFrom, $budgetTo, $sort);
        $this->flusher->flush();
        return $budget;
    }
}
