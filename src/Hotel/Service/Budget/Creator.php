<?php

declare(strict_types=1);

namespace App\Hotel\Service\Budget;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Budget\Budget;
use App\Hotel\Model\Budget\BudgetFrom;
use App\Hotel\Model\Budget\BudgetTo;
use App\Hotel\Model\Budget\Name;
use App\Hotel\Repository\BudgetRepository;
use App\Data\Flusher;

class Creator
{
    private Flusher $flusher;
    private BudgetRepository $budgetRepository;


    public function __construct(BudgetRepository $budgetRepository, Flusher $flusher)
    {
        $this->budgetRepository = $budgetRepository;
        $this->flusher = $flusher;
    }


    public function create(Uuid $id, Name $name, BudgetFrom $budgetFrom, BudgetTo $budgetTo): Budget
    {
        $sort = $this->budgetRepository->getMaxSort() + 1;
        $budget = new Budget(
            $id,
            $name,
            $budgetFrom,
            $budgetTo,
            new Sort($sort),
            true
        );

        $this->budgetRepository->add($budget);
        $this->flusher->flush();

        return $budget;
    }
}
