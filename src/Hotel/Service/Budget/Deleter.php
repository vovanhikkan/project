<?php

declare(strict_types=1);

namespace App\Hotel\Service\Budget;

use App\Application\ValueObject\Uuid;
use App\Hotel\Repository\BudgetRepository;
use App\Data\Flusher;

class Deleter
{
    private Flusher $flusher;
    private BudgetRepository $budgetRepository;


    public function __construct(BudgetRepository $budgetRepository, Flusher $flusher)
    {
        $this->budgetRepository = $budgetRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->budgetRepository->delete($id);
        $this->flusher->flush();
    }
}
