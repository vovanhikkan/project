<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlanGroup;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\RatePlanGroup\Name;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Repository\RatePlanGroupRepository;

class Updater
{
    private Flusher $flusher;
    private RatePlanGroupRepository $ratePlanGroupRepository;


    public function __construct(RatePlanGroupRepository $ratePlanGroupRepository, Flusher $flusher)
    {
        $this->ratePlanGroupRepository = $ratePlanGroupRepository;
        $this->flusher = $flusher;
    }


    public function update(
        RatePlanGroup $ratePlanGroup,
        ?Name $name
    ): RatePlanGroup {
        
        $ratePlanGroup->update($name);
        
        $this->flusher->flush();
        return $ratePlanGroup;
    }
}
