<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlanGroup;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\RatePlanGroup\Name;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Repository\RatePlanGroupRepository;

class Creator
{
    private Flusher $flusher;
    private RatePlanGroupRepository $ratePlanGroupRepository;


    public function __construct(RatePlanGroupRepository $ratePlanGroupRepository, Flusher $flusher)
    {
        $this->ratePlanGroupRepository = $ratePlanGroupRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Name $name
    ): RatePlanGroup {
        $sort = $this->ratePlanGroupRepository->getMaxSort() + 1;

        $ratePlanGroup = new RatePlanGroup(
            $id,
            $name,
            new Sort($sort)
        );

        $this->ratePlanGroupRepository->add($ratePlanGroup);
        $this->flusher->flush();

        return $ratePlanGroup;
    }
}
