<?php

declare(strict_types=1);

namespace App\Hotel\Service\RatePlanGroup;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\RatePlanGroupRepository;

class Deleter
{
    private Flusher $flusher;
    private RatePlanGroupRepository $ratePlanGroupRepository;


    public function __construct(RatePlanGroupRepository $ratePlanGroupRepository, Flusher $flusher)
    {
        $this->ratePlanGroupRepository = $ratePlanGroupRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->ratePlanGroupRepository->delete($id);
        $this->flusher->flush();
    }
}
