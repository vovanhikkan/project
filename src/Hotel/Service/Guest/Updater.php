<?php

declare(strict_types=1);

namespace App\Hotel\Service\Guest;

use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Repository\GuestRepository;
use App\Data\Flusher;
use App\Hotel\Model\Guest\Age;

class Updater
{
    private Flusher $flusher;
    private GuestRepository $guestRepository;


    public function __construct(GuestRepository $guestRepository, Flusher $flusher)
    {
        $this->guestRepository = $guestRepository;
        $this->flusher = $flusher;
    }


    public function update(
        Guest $guest,
        ?FirstName $firstName,
        ?LastName $lastName,
        ?Age $age,
        ?bool $mainGuest
    ): Guest {
        $guest->update($firstName, $lastName, $age, $mainGuest);
        $this->flusher->flush();
        return $guest;
    }
}
