<?php

declare(strict_types=1);

namespace App\Hotel\Service\Guest;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\GuestRepository;

class Deleter
{
    private Flusher $flusher;
    private GuestRepository $guestRepository;


    public function __construct(GuestRepository $guestRepository, Flusher $flusher)
    {
        $this->guestRepository = $guestRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->guestRepository->delete($id);
        $this->flusher->flush();
    }
}
