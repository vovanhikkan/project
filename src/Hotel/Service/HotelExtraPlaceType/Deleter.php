<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelExtraPlaceType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\HotelExtraPlaceTypeRepository;

class Deleter
{
    private Flusher $flusher;
    private HotelExtraPlaceTypeRepository $hotelExtraPlaceRepository;


    public function __construct(HotelExtraPlaceTypeRepository $hotelExtraPlaceRepository, Flusher $flusher)
    {
        $this->hotelExtraPlaceRepository = $hotelExtraPlaceRepository;
        $this->flusher = $flusher;
    }

    public function delete(Uuid $id): void
    {
        $this->hotelExtraPlaceRepository->delete($id);
        $this->flusher->flush();
    }
}
