<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelExtraPlaceType;

use App\Data\Flusher;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\HotelExtraPlaceType\Name;
use App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType;
use App\Hotel\Repository\HotelExtraPlaceTypeRepository;

class Updater
{
    private Flusher $flusher;
    private HotelExtraPlaceTypeRepository $hotelExtraPlaceTypeRepository;


    public function __construct(HotelExtraPlaceTypeRepository $hotelExtraPlaceTypeRepository, Flusher $flusher)
    {
        $this->hotelExtraPlaceTypeRepository = $hotelExtraPlaceTypeRepository;
        $this->flusher = $flusher;
    }


    public function update(
        HotelExtraPlaceType $hotelExtraPlaceType,
        ?Name $name,
        ?Hotel $hotel
    ): HotelExtraPlaceType {
        $hotelExtraPlaceType->update(
            $name,
            $hotel
        );
        
        $this->flusher->flush();
        return $hotelExtraPlaceType;
    }
}
