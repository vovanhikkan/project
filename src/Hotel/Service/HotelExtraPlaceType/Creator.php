<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelExtraPlaceType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\HotelExtraPlaceType\Name;
use App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType;
use App\Hotel\Repository\HotelExtraPlaceTypeRepository;

class Creator
{
    private Flusher $flusher;
    private HotelExtraPlaceTypeRepository $hotelExtraPlaceRepository;
    
    public function __construct(
        HotelExtraPlaceTypeRepository $hotelExtraPlaceTypeRepository,
        Flusher $flusher
    ) {
        $this->hotelExtraPlaceRepository = $hotelExtraPlaceTypeRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Name $name,
        Hotel $hotel,
        ?array $hotelAgeRanges
    ): HotelExtraPlaceType {
        $hotelExtraPlaceType = new HotelExtraPlaceType(
            $id,
            $name,
            $hotel
        );

        if ($hotelAgeRanges && count($hotelAgeRanges)) {
            foreach ($hotelAgeRanges as $hotelAgeRange) {
                $hotelExtraPlaceType->addAgeRange($hotelAgeRange);
            }
        }

        $this->hotelExtraPlaceRepository->add($hotelExtraPlaceType);
        $this->flusher->flush();

        return $hotelExtraPlaceType;
    }
}
