<?php

declare(strict_types=1);

namespace App\Hotel\Service\Facility;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\FacilityRepository;

class Deleter
{
    private Flusher $flusher;
    private FacilityRepository $facilityRepository;


    public function __construct(FacilityRepository $facilityRepository, Flusher $flusher)
    {
        $this->facilityRepository = $facilityRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->facilityRepository->delete($id);
        $this->flusher->flush();
    }
}
