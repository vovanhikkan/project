<?php

declare(strict_types=1);

namespace App\Hotel\Service\Facility;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\Facility\Name;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\Hotel\Repository\FacilityRepository;
use App\Storage\Model\File\File;

class Creator
{
    private Flusher $flusher;
    private FacilityRepository $facilityRepository;


    public function __construct(FacilityRepository $facilityRepository, Flusher $flusher)
    {
        $this->facilityRepository = $facilityRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Name $name,
        FacilityGroup $facilityGroup,
        ?bool $isShownInRoomList,
        ?bool $isShownInSearch,
        ?File $mobileIcon,
        ?File $webIcon
    ): Facility {
        $sort = $this->facilityRepository->getMaxSort() + 1;
        $facility = new Facility(
            $id,
            $name,
            true,
            $facilityGroup,
            new Sort($sort),
            $isShownInRoomList !== null ? $isShownInRoomList : false
        );

        if ($mobileIcon) {
            $facility->setMobileIcon($mobileIcon);
        }

        if ($isShownInSearch !== null) {
            $facility->setIsShownInSearch($isShownInSearch);
        }

        if ($webIcon) {
            $facility->setWebIcon($webIcon);
        }

        $this->facilityRepository->add($facility);
        $this->flusher->flush();

        return $facility;
    }
}
