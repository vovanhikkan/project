<?php

declare(strict_types=1);

namespace App\Hotel\Service\Facility;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\Facility\Name;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\Hotel\Repository\FacilityRepository;
use App\Storage\Model\File\File;

class Updater
{
    private Flusher $flusher;
    private FacilityRepository $facilityRepository;


    public function __construct(FacilityRepository $facilityRepository, Flusher $flusher)
    {
        $this->facilityRepository = $facilityRepository;
        $this->flusher = $flusher;
    }


    public function update(
        Facility $facility,
        ?Name $name,
        ?FacilityGroup $group,
        ?bool $isShownInRoomList,
        ?bool $isShownInSearch,
        ?File $mobIcon,
        ?File $webIcon
    ): Facility {
        $facility->update($name, $group, $isShownInRoomList, $isShownInSearch, $mobIcon, $webIcon);
        $this->flusher->flush();
        return $facility;
    }
}
