<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelType;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelType\Name;
use App\Hotel\Model\HotelType\HotelType;
use App\Data\Flusher;
use App\Hotel\Repository\HotelTypeRepository;

class Creator
{
    private Flusher $flusher;
    private HotelTypeRepository $hotelTypeRepository;


    public function __construct(HotelTypeRepository $hotelTypeRepository, Flusher $flusher)
    {
        $this->hotelTypeRepository = $hotelTypeRepository;
        $this->flusher = $flusher;
    }


    public function create(Uuid $id, Name $name): HotelType
    {
        $sort = $this->hotelTypeRepository->getMaxSort() + 1;
        $isActive = true;
        $hotelType = new HotelType(
            $id,
            $name,
            $isActive,
            new Sort($sort)
        );

        $this->hotelTypeRepository->add($hotelType);
        $this->flusher->flush();

        return $hotelType;
    }
}
