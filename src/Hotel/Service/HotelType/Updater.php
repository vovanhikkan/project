<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelType;

use App\Application\ValueObject\Sort;
use App\Hotel\Model\HotelType\HotelType;
use App\Hotel\Model\HotelType\Name;
use App\Data\Flusher;
use App\Hotel\Repository\HotelTypeRepository;

class Updater
{
    private Flusher $flusher;
    private HotelTypeRepository $hotelTypeRepository;


    public function __construct(HotelTypeRepository $hotelTypeRepository, Flusher $flusher)
    {
        $this->hotelTypeRepository = $hotelTypeRepository;
        $this->flusher = $flusher;
    }

    /**
     * @param HotelType $hotelType
     * @param Name $name
     * @param bool $isActive
     * @param Sort $sort
     * @return HotelType
     */
    public function update(
        HotelType $hotelType,
        Name $name,
        bool $isActive,
        Sort $sort
    ): HotelType {
        $hotelType->update($name, $isActive, $sort);
        $this->flusher->flush();
        return $hotelType;
    }
}
