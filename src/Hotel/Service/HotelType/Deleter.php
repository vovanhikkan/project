<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\HotelTypeRepository;

class Deleter
{
    private Flusher $flusher;
    private HotelTypeRepository $hotelTypeRepository;


    public function __construct(HotelTypeRepository $hotelTypeRepository, Flusher $flusher)
    {
        $this->hotelTypeRepository = $hotelTypeRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->hotelTypeRepository->delete($id);
        $this->flusher->flush();
    }
}
