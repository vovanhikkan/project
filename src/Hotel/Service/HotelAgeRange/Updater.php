<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelAgeRange;

use App\Data\Flusher;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\HotelAgeRange\AgeFrom;
use App\Hotel\Model\HotelAgeRange\AgeTo;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\Hotel\Repository\HotelAgeRangeRepository;

class Updater
{
    private Flusher $flusher;
    private HotelAgeRangeRepository $hotelAgeRangeRepository;


    public function __construct(
        HotelAgeRangeRepository $hotelAgeRangeRepository,
        Flusher $flusher
    ) {
        $this->hotelAgeRangeRepository = $hotelAgeRangeRepository;
        $this->flusher = $flusher;
    }


    public function update(
        HotelAgeRange $hotelAgeRange,
        ?Hotel $hotel,
        ?AgeFrom $ageFrom,
        ?AgeTo $ageTo
    ): HotelAgeRange {

        $hotelAgeRange->update($hotel, $ageFrom, $ageTo);
        $this->flusher->flush();
        return $hotelAgeRange;
    }
}
