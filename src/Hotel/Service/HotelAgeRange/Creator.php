<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelAgeRange;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\HotelAgeRange\AgeFrom;
use App\Hotel\Model\HotelAgeRange\AgeTo;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\Hotel\Repository\HotelAgeRangeRepository;

class Creator
{
    private Flusher $flusher;
    private HotelAgeRangeRepository $hotelAgeRangeRepository;


    public function __construct(
        HotelAgeRangeRepository $hotelAgeRangeRepository,
        Flusher $flusher
    ) {
        $this->hotelAgeRangeRepository = $hotelAgeRangeRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Hotel $hotel,
        AgeFrom $ageFrom,
        AgeTo $ageTo
    ): HotelAgeRange {

        $hotelAgeRange = new HotelAgeRange(
            $id,
            $hotel,
            $ageFrom,
            $ageTo
        );

        $this->hotelAgeRangeRepository->add($hotelAgeRange);
        $this->flusher->flush();

        return $hotelAgeRange;
    }
}
