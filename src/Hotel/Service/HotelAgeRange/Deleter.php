<?php

declare(strict_types=1);

namespace App\Hotel\Service\HotelAgeRange;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Hotel\Repository\HotelAgeRangeRepository;

class Deleter
{
    private Flusher $flusher;
    private HotelAgeRangeRepository $hotelAgeRangeRepository;


    public function __construct(
        HotelAgeRangeRepository $hotelAgeRangeRepository,
        Flusher $flusher
    ) {
        $this->hotelAgeRangeRepository = $hotelAgeRangeRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->hotelAgeRangeRepository->delete($id);
        $this->flusher->flush();
    }
}
