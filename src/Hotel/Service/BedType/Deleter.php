<?php

declare(strict_types=1);

namespace App\Hotel\Service\BedType;

use App\Application\ValueObject\Uuid;
use App\Hotel\Repository\BedTypeRepository;
use App\Data\Flusher;

class Deleter
{
    private Flusher $flusher;
    private BedTypeRepository $bedTypeRepository;


    public function __construct(BedTypeRepository $bedTypeRepository, Flusher $flusher)
    {
        $this->bedTypeRepository = $bedTypeRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->bedTypeRepository->delete($id);
        $this->flusher->flush();
    }
}
