<?php

declare(strict_types=1);

namespace App\Hotel\Service\BedType;

use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\BedType\Name;
use App\Hotel\Repository\BedTypeRepository;
use App\Data\Flusher;
use App\Storage\Model\File\File;

class Updater
{
    private Flusher $flusher;
    private BedTypeRepository $bedTypeRepository;


    public function __construct(BedTypeRepository $bedTypeRepository, Flusher $flusher)
    {
        $this->bedTypeRepository = $bedTypeRepository;
        $this->flusher = $flusher;
    }

    public function update(
        BedType $bedType,
        ?Name $name,
        ?File $mobIcon,
        ?File $webIcon
    ): BedType {

        $bedType->update($name, $mobIcon, $webIcon);
        $this->flusher->flush();
        return $bedType;
    }
}
