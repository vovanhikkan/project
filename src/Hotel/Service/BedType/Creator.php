<?php

declare(strict_types=1);

namespace App\Hotel\Service\BedType;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\BedType\Name;
use App\Hotel\Repository\BedTypeRepository;
use App\Data\Flusher;
use App\Storage\Model\File\File;

class Creator
{
    private Flusher $flusher;
    private BedTypeRepository $bedTypeRepository;


    public function __construct(BedTypeRepository $bedTypeRepository, Flusher $flusher)
    {
        $this->bedTypeRepository = $bedTypeRepository;
        $this->flusher = $flusher;
    }


    public function create(
        Uuid $id,
        Name $name,
        ?File $mobileIcon,
        ?File $webIcon
    ): BedType {
        $bedType = new BedType(
            $id,
            $name
        );

        if ($mobileIcon) {
            $bedType->setMobileIcon($mobileIcon);
        }

        if ($webIcon) {
            $bedType->setWebIcon($webIcon);
        }

        $this->bedTypeRepository->add($bedType);
        $this->flusher->flush();

        return $bedType;
    }
}
