<?php

declare(strict_types=1);

namespace App\Hotel\Service\Brand;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Brand\Brand;
use App\Hotel\Model\Brand\Name;
use App\Hotel\Repository\BrandRepository;
use App\Data\Flusher;

class Creator
{
    private Flusher $flusher;
    private BrandRepository $brandRepository;


    public function __construct(BrandRepository $brandRepository, Flusher $flusher)
    {
        $this->brandRepository = $brandRepository;
        $this->flusher = $flusher;
    }


    public function create(Uuid $id, Name $name): Brand
    {
        $brand = new Brand(
            $id,
            $name,
            true
        );

        $this->brandRepository->add($brand);
        $this->flusher->flush();

        return $brand;
    }
}
