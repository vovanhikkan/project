<?php

declare(strict_types=1);

namespace App\Hotel\Service\Brand;

use App\Application\ValueObject\Uuid;
use App\Hotel\Repository\BrandRepository;
use App\Data\Flusher;

class Deleter
{
    private Flusher $flusher;
    private BrandRepository $brandRepository;


    public function __construct(BrandRepository $brandRepository, Flusher $flusher)
    {
        $this->brandRepository = $brandRepository;
        $this->flusher = $flusher;
    }


    public function delete(Uuid $id): void
    {
        $this->brandRepository->delete($id);
        $this->flusher->flush();
    }
}
