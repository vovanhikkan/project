<?php

declare(strict_types=1);

namespace App\Hotel\Service\Brand;

use App\Hotel\Model\Brand\Brand;
use App\Hotel\Model\Brand\Name;
use App\Hotel\Repository\BrandRepository;
use App\Data\Flusher;

class Updater
{
    private Flusher $flusher;
    private BrandRepository $brandRepository;


    public function __construct(BrandRepository $brandRepository, Flusher $flusher)
    {
        $this->brandRepository = $brandRepository;
        $this->flusher = $flusher;
    }


    public function update(Brand $brand, ?Name $name): Brand
    {
        $brand->update($name);
        $this->flusher->flush();
        return $brand;
    }
}
