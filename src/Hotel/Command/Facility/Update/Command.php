<?php

declare(strict_types=1);

namespace App\Hotel\Command\Facility\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?array $name = null;

    private ?string $facilityGroup = null;

    private ?bool $isShownInRoomList = null;

    private ?bool $isShownInSearch = null;

    private ?string $mobileIcon = null;

    private ?string $webIcon = null;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getFacilityGroup(): ?string
    {
        return $this->facilityGroup;
    }

    /**
     * @param string|null $facilityGroup
     */
    public function setFacilityGroup(?string $facilityGroup): void
    {
        $this->facilityGroup = $facilityGroup;
    }

    /**
     * @return bool|null
     */
    public function getIsShownInRoomList(): ?bool
    {
        return $this->isShownInRoomList;
    }

    /**
     * @param bool|null $isShownInRoomList
     */
    public function setIsShownInRoomList(?bool $isShownInRoomList): void
    {
        $this->isShownInRoomList = $isShownInRoomList;
    }

    /**
     * @return bool|null
     */
    public function getIsShownInSearch(): ?bool
    {
        return $this->isShownInSearch;
    }

    /**
     * @param bool|null $isShownInSearch
     */
    public function setIsShownInSearch(?bool $isShownInSearch): void
    {
        $this->isShownInSearch = $isShownInSearch;
    }

    /**
     * @return string|null
     */
    public function getMobileIcon(): ?string
    {
        return $this->mobileIcon;
    }

    /**
     * @param string|null $mobileIcon
     */
    public function setMobileIcon(?string $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @param string|null $webIcon
     */
    public function setWebIcon(?string $webIcon): void
    {
        $this->webIcon = $webIcon;
    }
}
