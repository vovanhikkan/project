<?php

declare(strict_types=1);

namespace App\Hotel\Command\Facility\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\Facility\Name;
use App\Hotel\Repository\FacilityGroupRepository;
use App\Hotel\Repository\FacilityRepository;
use App\Hotel\Service\Facility\Updater;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Updater $updater;
    private FacilityRepository $facilityRepository;
    private FacilityGroupRepository $facilityGroupRepository;
    private FileRepository $fileRepository;


    public function __construct(Updater $updater, FacilityRepository $facilityRepository, FacilityGroupRepository $facilityGroupRepository, FileRepository $fileRepository)
    {
        $this->facilityRepository = $facilityRepository;
        $this->facilityGroupRepository = $facilityGroupRepository;
        $this->fileRepository = $fileRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): Facility
    {
        $facility = $this->facilityRepository->get(new Uuid($command->getId()));
        if ($group = $command->getFacilityGroup()) {
            $group = $this->facilityGroupRepository->get(new Uuid($command->getFacilityGroup()));
        }
        $mobIcon = $this->fileRepository->getFileOrNull($command->getMobileIcon());
        $webIcon = $this->fileRepository->getFileOrNull($command->getWebIcon());

        return $this->updater->update(
            $facility,
            $command->getName() ? new Name($command->getName()) : null,
            $group,
            $command->getIsShownInRoomList() !== null ? $command->getIsShownInRoomList() : null,
            $command->getIsShownInSearch() !== null ? $command->getIsShownInSearch() : null,
            $mobIcon,
            $webIcon
        );
    }
}
