<?php

declare(strict_types=1);

namespace App\Hotel\Command\Facility\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private array $name;

    /** @Assert\NotBlank() */
    private string $facilityGroup;

    private ?bool $isShownInRoomList = null;

    private ?bool $isShownInSearch = null;

    private ?string $mobileIcon = null;

    private ?string $webIcon = null;

    /**
     * Command constructor.
     * @param array $name
     * @param string $facilityGroup
     */
    public function __construct(array $name, string $facilityGroup)
    {
        $this->name = $name;
        $this->facilityGroup = $facilityGroup;
    }

    /**
     * @return bool|null
     */
    public function getIsShownInRoomList(): ?bool
    {
        return $this->isShownInRoomList;
    }

    /**
     * @param bool|null $isShownInRoomList
     */
    public function setIsShownInRoomList(?bool $isShownInRoomList): void
    {
        $this->isShownInRoomList = $isShownInRoomList;
    }

    /**
     * @return bool|null
     */
    public function getIsShownInSearch(): ?bool
    {
        return $this->isShownInSearch;
    }

    /**
     * @param bool|null $isShownInSearch
     */
    public function setIsShownInSearch(?bool $isShownInSearch): void
    {
        $this->isShownInSearch = $isShownInSearch;
    }

    /**
     * @return string|null
     */
    public function getMobileIcon(): ?string
    {
        return $this->mobileIcon;
    }

    /**
     * @param string|null $mobileIcon
     */
    public function setMobileIcon(?string $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @param string|null $webIcon
     */
    public function setWebIcon(?string $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFacilityGroup(): string
    {
        return $this->facilityGroup;
    }
}
