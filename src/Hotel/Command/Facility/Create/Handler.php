<?php

declare(strict_types=1);

namespace App\Hotel\Command\Facility\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Facility\Facility;
use App\Hotel\Model\Facility\Name;
use App\Hotel\Repository\FacilityGroupRepository;
use App\Hotel\Service\Facility\Creator;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Creator $creator;
    private FacilityGroupRepository $facilityGroupRepository;
    private FileRepository $fileRepository;


    public function __construct(Creator $creator, FacilityGroupRepository $facilityGroupRepository, FileRepository $fileRepository)
    {
        $this->creator = $creator;
        $this->facilityGroupRepository = $facilityGroupRepository;
        $this->fileRepository = $fileRepository;
    }


    public function handle(Command $command): Facility
    {
        $group = $this->facilityGroupRepository->get(new Uuid($command->getFacilityGroup()));
        $mobIcon = $this->fileRepository->getFileOrNull($command->getMobileIcon());
        $webIcon = $this->fileRepository->getFileOrNull($command->getWebIcon());

        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
            $group,
            $command->getIsShownInRoomList(),
            $command->getIsShownInSearch(),
            $mobIcon,
            $webIcon
        );
    }
}
