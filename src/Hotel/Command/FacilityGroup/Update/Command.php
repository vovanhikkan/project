<?php

declare(strict_types=1);

namespace App\Hotel\Command\FacilityGroup\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?array $name = null;

    private ?bool $isActive = null;

    private ?bool $isShownHotel = null;

    private ?bool $isShownRoom = null;

    private ?string $mobileIcon = null;

    private ?string $webIcon = null;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool|null
     */
    public function getIsShownHotel(): ?bool
    {
        return $this->isShownHotel;
    }

    /**
     * @param bool|null $isShownHotel
     */
    public function setIsShownHotel(?bool $isShownHotel): void
    {
        $this->isShownHotel = $isShownHotel;
    }

    /**
     * @return bool|null
     */
    public function getIsShownRoom(): ?bool
    {
        return $this->isShownRoom;
    }

    /**
     * @param bool|null $isShownRoom
     */
    public function setIsShownRoom(?bool $isShownRoom): void
    {
        $this->isShownRoom = $isShownRoom;
    }

    /**
     * @return string|null
     */
    public function getMobileIcon(): ?string
    {
        return $this->mobileIcon;
    }

    /**
     * @param string|null $mobileIcon
     */
    public function setMobileIcon(?string $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @param string|null $webIcon
     */
    public function setWebIcon(?string $webIcon): void
    {
        $this->webIcon = $webIcon;
    }
}
