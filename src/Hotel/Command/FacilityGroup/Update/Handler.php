<?php

declare(strict_types=1);

namespace App\Hotel\Command\FacilityGroup\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\Hotel\Model\FacilityGroup\Name;
use App\Hotel\Repository\FacilityGroupRepository;
use App\Hotel\Service\FacilityGroup\Updater;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Updater $updater;
    private FacilityGroupRepository $facilityGroupRepository;
    private FileRepository $fileRepository;


    public function __construct(
        Updater $updater,
        FacilityGroupRepository $facilityGroupRepository,
        FileRepository $fileRepository
    ) {
        $this->facilityGroupRepository = $facilityGroupRepository;
        $this->fileRepository = $fileRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): FacilityGroup
    {
        $facilityGroup = $this->facilityGroupRepository->get(new Uuid($command->getId()));
        
        $mobIcon = $this->fileRepository->getFileOrNull($command->getMobileIcon());
        $webIcon = $this->fileRepository->getFileOrNull($command->getWebIcon());

        return $this->updater->update(
            $facilityGroup,
            $command->getName() ? new Name($command->getName()) : null,
            $command->getIsActive() !== null ? $command->getIsActive() : null,
            $command->getIsShownHotel() !== null ? $command->getIsShownHotel() : null,
            $command->getIsShownRoom() !== null ? $command->getIsShownRoom() : null,
            $mobIcon,
            $webIcon
        );
    }
}
