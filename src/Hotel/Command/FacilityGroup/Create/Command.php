<?php

declare(strict_types=1);

namespace App\Hotel\Command\FacilityGroup\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private array $name;

    private ?bool $isActive;

    private ?bool $isShownHotel;

    private ?bool $isShownRoom;

    private ?int $sort = null;

    private ?string $mobileIcon = null;

    private ?string $webIcon = null;

    /**
     * Command constructor.
     * @param array $name
     */
    public function __construct(
        array $name
    ) {
        $this->name = $name;
        $this->isActive = true;
        $this->isShownHotel = false;
        $this->isShownRoom = false;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getIsActive() : bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }


    /**
     * @return bool
     */
    public function getIsShownHotel() : bool
    {
        return $this->isShownHotel;
    }

    /**
     * @param bool|null $isShownHotel
     */
    public function setIsShownHotel(?bool $isShownHotel): void
    {
        $this->isShownHotel = $isShownHotel;
    }

    /**
     * @return bool
     */
    public function getIsShownRoom() : bool
    {
        return $this->isShownRoom;
    }

    /**
     * @param bool|null $isShownRoom
     */
    public function setIsShownRoom(?bool $isShownRoom): void
    {
        $this->isShownRoom = $isShownRoom;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @return string|null
     */
    public function getMobileIcon(): ?string
    {
        return $this->mobileIcon;
    }

    /**
     * @param string|null $mobileIcon
     */
    public function setMobileIcon(?string $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @param string|null $webIcon
     */
    public function setWebIcon(?string $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

}
