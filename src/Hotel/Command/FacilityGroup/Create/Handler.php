<?php

declare(strict_types=1);

namespace App\Hotel\Command\FacilityGroup\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\FacilityGroup\FacilityGroup;
use App\Hotel\Model\FacilityGroup\Name;
use App\Hotel\Service\FacilityGroup\Creator;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Creator $creator;
    private FileRepository $fileRepository;


    public function __construct(
        Creator $creator,
        FileRepository $fileRepository
    ) {
        $this->creator = $creator;
        $this->fileRepository = $fileRepository;
    }


    public function handle(Command $command): FacilityGroup
    {

        $mobIcon = $this->fileRepository->getFileOrNull($command->getMobileIcon());
        $webIcon = $this->fileRepository->getFileOrNull($command->getWebIcon());

        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
            $command->getIsActive(),
            $command->getIsShownHotel(),
            $command->getIsShownRoom(),
            $mobIcon,
            $webIcon
        );
    }
}
