<?php

declare(strict_types=1);

namespace App\Hotel\Command\FacilityGroup\Delete;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private string $id;


    public function __construct(string $id)
    {
        $this->id = $id;
    }


    public function getId(): string
    {
        return $this->id;
    }
}
