<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlanPrice\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private string $ratePlan;
    /** @Assert\NotBlank() */
    private string $room;
    /** @Assert\NotBlank() */
    private string $date;

    private ?int $mainPlaceValue = null;
    private ?int $value = null;

    
    public function __construct(
        string $ratePlan,
        string $room,
        string $date
    ) {
        $this->ratePlan = $ratePlan;
        $this->room = $room;
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getRatePlan(): string
    {
        return $this->ratePlan;
    }

    /**
     * @return string
     */
    public function getRoom(): string
    {
        return $this->room;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return int|null
     */
    public function getMainPlaceValue(): ?int
    {
        return $this->mainPlaceValue;
    }

    /**
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * @param int|null $mainPlaceValue
     */
    public function setMainPlaceValue(?int $mainPlaceValue): void
    {
        $this->mainPlaceValue = $mainPlaceValue;
    }

    /**
     * @param int|null $value
     */
    public function setValue(?int $value): void
    {
        $this->value = $value;
    }


}
