<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlanPrice\Create;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Repository\RatePlanRepository;
use App\Hotel\Repository\RoomRepository;
use App\Hotel\Service\RatePlanPrice\Creator;
use DateTimeImmutable;

class Handler
{
    private Creator $creator;
    private RatePlanRepository $ratePlanRepository;
    private RoomRepository $roomRepository;

    public function __construct(
        Creator $creator,
        RatePlanRepository $ratePlanRepository,
        RoomRepository $roomRepository
    ) {
        $this->creator = $creator;
        $this->ratePlanRepository = $ratePlanRepository;
        $this->roomRepository = $roomRepository;
    }


    public function handle(Command $command): RatePlanPrice
    {
        if ($ratePlan = $command->getRatePlan()) {
            $ratePlan = $this->ratePlanRepository->get(new Uuid($ratePlan));
        }

        if ($room = $command->getRoom()) {
            $room = $this->roomRepository->get(new Uuid($room));
        }

        return $this->creator->create(
            Uuid::generate(),
            $ratePlan,
            $room,
            new DateTimeImmutable($command->getDate()),
            $command->getMainPlaceValue() !== null ? new Amount($command->getMainPlaceValue()) : null,
            $command->getValue() !== null ? new Amount($command->getValue()) : null
        );
    }
}
