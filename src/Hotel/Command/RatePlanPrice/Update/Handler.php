<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlanPrice\Update;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Repository\RatePlanPriceRepository;
use App\Hotel\Repository\RatePlanRepository;
use App\Hotel\Repository\RoomRepository;
use App\Hotel\Service\RatePlanPrice\Updater;
use DateTimeImmutable;

class Handler
{
    private Updater $updater;
    private RatePlanPriceRepository $ratePlanPriceRepository;
    private RatePlanRepository $ratePlanRepository;
    private RoomRepository $roomRepository;

    public function __construct(
        Updater $updater,
        RatePlanPriceRepository $ratePlanPriceRepository,
        RatePlanRepository $ratePlanRepository,
        RoomRepository $roomRepository
    ) {
        $this->ratePlanPriceRepository = $ratePlanPriceRepository;
        $this->updater = $updater;
        $this->ratePlanRepository = $ratePlanRepository;
        $this->roomRepository = $roomRepository;
    }


    public function handle(Command $command): RatePlanPrice
    {
        $ratePlanPrice = $this->ratePlanPriceRepository->get(new Uuid($command->getId()));

        if ($ratePlan = $command->getRatePlan()) {
            $ratePlan = $this->ratePlanRepository->get(new Uuid($ratePlan));
        }

        if ($room = $command->getRoom()) {
            $room = $this->roomRepository->get(new Uuid($room));
        }

        return $this->updater->update(
            $ratePlanPrice,
            $ratePlan,
            $room,
            $command->getDate() ? new DateTimeImmutable($command->getDate()) : null,
            $command->getMainPlaceValue() ? new Amount($command->getMainPlaceValue()) : null,
            $command->getValue() ? new Amount($command->getValue()) : null,
        );
    }
}
