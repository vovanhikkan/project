<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlanPrice\Update;

use App\Application\ValueObject\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?string $ratePlan = null;
    private ?string $room = null;
    private ?string $date = null;

    private ?int $mainPlaceValue = null;
    private ?int $value = null;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getRatePlan(): ?string
    {
        return $this->ratePlan;
    }

    /**
     * @return string|null
     */
    public function getRoom(): ?string
    {
        return $this->room;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @return int|null
     */
    public function getMainPlaceValue(): ?int
    {
        return $this->mainPlaceValue;
    }

    /**
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * @param string|null $ratePlan
     */
    public function setRatePlan(?string $ratePlan): void
    {
        $this->ratePlan = $ratePlan;
    }

    /**
     * @param string|null $room
     */
    public function setRoom(?string $room): void
    {
        $this->room = $room;
    }

    /**
     * @param string|null $date
     */
    public function setDate(?string $date): void
    {
        $this->date = $date;
    }

    /**
     * @param int|null $mainPlaceValue
     */
    public function setMainPlaceValue(?int $mainPlaceValue): void
    {
        $this->mainPlaceValue = $mainPlaceValue;
    }

    /**
     * @param int|null $value
     */
    public function setValue(?int $value): void
    {
        $this->value = $value;
    }

}
