<?php

declare(strict_types=1);

namespace App\Hotel\Command\Budget\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $name;
    /**
     * @Assert\NotBlank()
     */
    private int $budgetFrom;
    /**
     * @Assert\NotBlank()
     */
    private int $budgetTo;


    /**
     * Command constructor.
     * @param array $name
     * @param int $budgetFrom
     * @param int $budgetTo
     * @param int $sort
     */
    public function __construct(array $name, int $budgetFrom, int $budgetTo)
    {
        $this->name = $name;
        $this->budgetFrom = $budgetFrom;
        $this->budgetTo = $budgetTo;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getBudgetFrom(): int
    {
        return $this->budgetFrom;
    }

    /**
     * @return int
     */
    public function getBudgetTo(): int
    {
        return $this->budgetTo;
    }
}
