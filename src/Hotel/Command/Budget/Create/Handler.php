<?php

declare(strict_types=1);

namespace App\Hotel\Command\Budget\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Budget\Budget;
use App\Hotel\Model\Budget\BudgetFrom;
use App\Hotel\Model\Budget\BudgetTo;
use App\Hotel\Model\Budget\Name;
use App\Hotel\Service\Budget\Creator;

class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): Budget
    {
        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
            new BudgetFrom($command->getBudgetFrom()),
            new BudgetTo($command->getBudgetTo())
        );
    }
}
