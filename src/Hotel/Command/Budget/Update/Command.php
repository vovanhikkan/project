<?php

declare(strict_types=1);

namespace App\Hotel\Command\Budget\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?array $name = null;
    private ?int $budgetFrom = null;
    private ?int $budgetTo = null;

    private ?int $sort = null;


    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getBudgetFrom(): ?int
    {
        return $this->budgetFrom;
    }

    /**
     * @return int|null
     */
    public function getBudgetTo(): ?int
    {
        return $this->budgetTo;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @param int|null $budgetFrom
     */
    public function setBudgetFrom(?int $budgetFrom): void
    {
        $this->budgetFrom = $budgetFrom;
    }

    /**
     * @param int|null $budgetTo
     */
    public function setBudgetTo(?int $budgetTo): void
    {
        $this->budgetTo = $budgetTo;
    }

    /**
     * @param int|null $sort
     */
    public function setSort(?int $sort): void
    {
        $this->sort = $sort;
    }

}
