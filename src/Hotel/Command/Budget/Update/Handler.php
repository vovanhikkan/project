<?php

declare(strict_types=1);

namespace App\Hotel\Command\Budget\Update;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Budget\Budget;
use App\Hotel\Model\Budget\BudgetFrom;
use App\Hotel\Model\Budget\BudgetTo;
use App\Hotel\Model\Budget\Name;
use App\Hotel\Repository\BudgetRepository;
use App\Hotel\Service\Budget\Updater;

class Handler
{
    private Updater $updater;
    private BudgetRepository $budgetRepository;


    public function __construct(Updater $updater, BudgetRepository $budgetRepository)
    {
        $this->budgetRepository = $budgetRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): Budget
    {
        $budget = $this->budgetRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $budget,
            $command->getName() ? new Name($command->getName()) : null,
            $command->getBudgetFrom() !== null ? new BudgetFrom($command->getBudgetFrom()) : null,
            $command->getBudgetTo() !== null ? new BudgetTo($command->getBudgetTo()) : null,
            $command->getSort() !== null ? new Sort($command->getSort()) : null
        );
    }
}
