<?php

declare(strict_types=1);

namespace App\Hotel\Command\FoodType\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $name;

    /**
     * Command constructor.
     * @param array $name
     */
    public function __construct(array $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }
}
