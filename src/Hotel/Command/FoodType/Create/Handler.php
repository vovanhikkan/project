<?php

declare(strict_types=1);

namespace App\Hotel\Command\FoodType\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\FoodType\Name;
use App\Hotel\Service\FoodType\Creator;

class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): FoodType
    {
        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName())
        );
    }
}
