<?php

declare(strict_types=1);

namespace App\Hotel\Command\FoodType\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\FoodType\Name;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Service\FoodType\Updater;

class Handler
{
    private Updater $updater;
    private FoodTypeRepository $foodTypeRepository;


    public function __construct(Updater $updater, FoodTypeRepository $foodTypeRepository)
    {
        $this->foodTypeRepository = $foodTypeRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): FoodType
    {
        $foodType = $this->foodTypeRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $foodType,
            $command->getName() ? new Name($command->getName()) : null
        );
    }
}
