<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelType\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelType\HotelType;
use App\Hotel\Model\HotelType\Name;
use App\Hotel\Service\HotelType\Creator;

class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): HotelType
    {
        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName())
        );
    }
}
