<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelType\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?array $name;
    
    private bool $isActive;

    /**
     * @Assert\NotBlank()
     */
    private int $sort;


    /**
     * Command constructor.
     * @param string $id
     * @param array|null $name
     * @param bool $isActive
     * @param int $sort
     */
    public function __construct(string $id, ?array $name, bool $isActive, int $sort)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getIsActive() : bool
    {
        return $this->isActive;
    }

    /**
     * @return int
     */
    public function getSort() : int
    {
        return $this->sort;
    }
}
