<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelType\Update;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelType\HotelType;
use App\Hotel\Model\HotelType\Name;
use App\Hotel\Repository\HotelTypeRepository;
use App\Hotel\Service\HotelType\Updater;

class Handler
{
    private Updater $updater;
    private HotelTypeRepository $hotelTypeRepository;


    public function __construct(Updater $updater, HotelTypeRepository $hotelTypeRepository)
    {
        $this->hotelTypeRepository = $hotelTypeRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): HotelType
    {
        $hotelType = $this->hotelTypeRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $hotelType,
            $command->getName() ? new Name($command->getName()) : null,
            $command->getIsActive(),
            new Sort($command->getSort())
        );
    }
}
