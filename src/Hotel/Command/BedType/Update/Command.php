<?php

declare(strict_types=1);

namespace App\Hotel\Command\BedType\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?array $name = null;

    private ?string $mobileIcon = null;

    private ?string $webIcon = null;

    /**
     * Command constructor.
     * @param string $id
     * @param array|null $name
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @param string|null $webIcon
     */
    public function setWebIcon(?string $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

    /**
     * @return string|null
     */
    public function getMobileIcon(): ?string
    {
        return $this->mobileIcon;
    }

    /**
     * @param string|null $mobileIcon
     */
    public function setMobileIcon(?string $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

}
