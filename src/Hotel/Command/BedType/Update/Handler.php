<?php

declare(strict_types=1);

namespace App\Hotel\Command\BedType\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\BedType\BedType;
use App\Hotel\Model\BedType\Name;
use App\Hotel\Repository\BedTypeRepository;
use App\Hotel\Service\BedType\Updater;
use App\Storage\Repository\FileRepository;

class Handler
{
    private Updater $updater;
    private BedTypeRepository $bedTypeRepository;
    private FileRepository $fileRepository;

    public function __construct(
        Updater $updater,
        BedTypeRepository $bedTypeRepository,
        FileRepository $fileRepository)
    {
        $this->bedTypeRepository = $bedTypeRepository;
        $this->updater = $updater;
        $this->fileRepository = $fileRepository;
    }


    public function handle(Command $command): BedType
    {
        $bedType = $this->bedTypeRepository->get(new Uuid($command->getId()));
        $mobIcon = $this->fileRepository->getFileOrNull($command->getMobileIcon());
        $webIcon = $this->fileRepository->getFileOrNull($command->getWebIcon());

        return $this->updater->update(
            $bedType,
            $command->getName() ? new Name($command->getName()) : null,
            $mobIcon,
            $webIcon
        );
    }
}
