<?php

declare(strict_types=1);

namespace App\Hotel\Command\BedType\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private array $name;

    private ?string $mobileIcon = null;

    private ?string $webIcon = null;

    /**
     * Command constructor.
     * @param array $name
     */
    public function __construct(array $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getMobileIcon(): ?string
    {
        return $this->mobileIcon;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @param string|null $mobileIcon
     */
    public function setMobileIcon(?string $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    /**
     * @param string|null $webIcon
     */
    public function setWebIcon(?string $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

}
