<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlan\Update;

use App\Application\ValueObject\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;
    private ?string $hotel = null;
    private ?array $name = null;
    private ?array $description = null;
    private ?string $ratePlanGroup = null;
    private ?string $foodType = null;
    private ?string $taxType = null;
    private ?bool $isActive = null;
    private ?string $activeFrom = null;
    private ?string $activeTo = null;
    private ?string $shownFrom = null;
    private ?string $shownTo = null;
    private ?string $canBeCanceledDate = null;

    private ?bool $isShownWithPackets = null;
    private ?array $extraCondition = null;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getHotel(): ?string
    {
        return $this->hotel;
    }

    /**
     * @param string|null $hotel
     */
    public function setHotel(?string $hotel): void
    {
        $this->hotel = $hotel;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @param array|null $description
     */
    public function setDescription(?array $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getRatePlanGroup(): ?string
    {
        return $this->ratePlanGroup;
    }

    /**
     * @param string|null $ratePlanGroup
     */
    public function setRatePlanGroup(?string $ratePlanGroup): void
    {
        $this->ratePlanGroup = $ratePlanGroup;
    }

    /**
     * @return string|null
     */
    public function getFoodType(): ?string
    {
        return $this->foodType;
    }

    /**
     * @param string|null $foodType
     */
    public function setFoodType(?string $foodType): void
    {
        $this->foodType = $foodType;
    }

    /**
     * @return string|null
     */
    public function getTaxType(): ?string
    {
        return $this->taxType;
    }

    /**
     * @param string|null $taxType
     */
    public function setTaxType(?string $taxType): void
    {
        $this->taxType = $taxType;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getCanBeCanceledDate(): ?string
    {
        return $this->canBeCanceledDate;
    }

    /**
     * @param string|null $canBeCanceledDate
     */
    public function setCanBeCanceledDate(?string $canBeCanceledDate): void
    {
        $this->canBeCanceledDate = $canBeCanceledDate;
    }

    /**
     * @return string|null
     */
    public function getActiveFrom(): ?string
    {
        return $this->activeFrom;
    }

    /**
     * @param string|null $activeFrom
     */
    public function setActiveFrom(?string $activeFrom): void
    {
        $this->activeFrom = $activeFrom;
    }

    /**
     * @return string|null
     */
    public function getActiveTo(): ?string
    {
        return $this->activeTo;
    }

    /**
     * @param string|null $activeTo
     */
    public function setActiveTo(?string $activeTo): void
    {
        $this->activeTo = $activeTo;
    }

    /**
     * @return string|null
     */
    public function getShownFrom(): ?string
    {
        return $this->shownFrom;
    }

    /**
     * @param string|null $shownFrom
     */
    public function setShownFrom(?string $shownFrom): void
    {
        $this->shownFrom = $shownFrom;
    }

    /**
     * @return string|null
     */
    public function getShownTo(): ?string
    {
        return $this->shownTo;
    }

    /**
     * @param string|null $shownTo
     */
    public function setShownTo(?string $shownTo): void
    {
        $this->shownTo = $shownTo;
    }

    /**
     * @return bool|null
     */
    public function getIsShownWithPackets(): ?bool
    {
        return $this->isShownWithPackets;
    }

    /**
     * @param bool|null $isShownWithPackets
     */
    public function setIsShownWithPackets(?bool $isShownWithPackets): void
    {
        $this->isShownWithPackets = $isShownWithPackets;
    }

    /**
     * @return array|null
     */
    public function getExtraCondition(): ?array
    {
        return $this->extraCondition;
    }

    /**
     * @param array|null $extraCondition
     */
    public function setExtraCondition(?array $extraCondition): void
    {
        $this->extraCondition = $extraCondition;
    }

}
