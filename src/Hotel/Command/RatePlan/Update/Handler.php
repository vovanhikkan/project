<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlan\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlan\Description;
use App\Hotel\Model\RatePlan\ExtraCondition;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Repository\RatePlanGroupRepository;
use App\Hotel\Repository\RatePlanRepository;
use App\Hotel\Repository\TaxTypeRepository;
use App\Hotel\Service\RatePlan\Updater;
use App\Hotel\Model\RatePlan\Name;
use DateTimeImmutable;

class Handler
{
    private Updater $updater;
    private RatePlanRepository $ratePlanRepository;
    private HotelRepository $hotelRepository;
    private RatePlanGroupRepository $ratePlanGroupRepository;
    private FoodTypeRepository $foodTypeRepository;
    private TaxTypeRepository $taxTypeRepository;


    public function __construct(
        Updater $updater,
        RatePlanRepository $ratePlanRepository,
        HotelRepository $hotelRepository,
        RatePlanGroupRepository $ratePlanGroupRepository,
        FoodTypeRepository $foodTypeRepository,
        TaxTypeRepository $taxTypeRepository
    ) {
        $this->ratePlanRepository = $ratePlanRepository;
        $this->updater = $updater;
        $this->hotelRepository = $hotelRepository;
        $this->ratePlanGroupRepository = $ratePlanGroupRepository;
        $this->foodTypeRepository = $foodTypeRepository;
        $this->taxTypeRepository = $taxTypeRepository;
    }


    public function handle(Command $command): RatePlan
    {
        $ratePlan = $this->ratePlanRepository->get(new Uuid($command->getId()));

        if ($hotel = $command->getHotel()) {
            $hotel = $this->hotelRepository->get(new Uuid($hotel));
        }

        if ($ratePlanGroup = $command->getRatePlanGroup()) {
            $ratePlanGroup = $this->ratePlanGroupRepository->get(new Uuid($ratePlanGroup));
        }

        if ($foodType = $command->getFoodType()) {
            $foodType = $this->foodTypeRepository->get(new Uuid($foodType));
        }

        if ($taxType = $command->getTaxType()) {
            $taxType = $this->taxTypeRepository->get(new Uuid($taxType));
        }
        
        return $this->updater->update(
            $ratePlan,
            $hotel,
            $command->getName() ? new Name($command->getName()) : null,
            $command->getDescription() ? new Description($command->getDescription()) : null,
            $ratePlanGroup,
            $foodType,
            $taxType,
            $command->getIsActive() !== null ? $command->getIsActive() : null,
            $command->getActiveFrom() ? new DateTimeImmutable($command->getActiveFrom()) : null,
            $command->getActiveTo() ? new DateTimeImmutable($command->getActiveTo()) : null,
            $command->getShownFrom() ? new DateTimeImmutable($command->getShownFrom()) : null,
            $command->getShownTo() ? new DateTimeImmutable($command->getShownTo()) : null,
            $command->getIsShownWithPackets() !== null ? $command->getIsShownWithPackets() : null,
            $command->getCanBeCanceledDate() !== null ? new DateTimeImmutable($command->getCanBeCanceledDate()) : null,
            $command->getExtraCondition() !== null ? new ExtraCondition($command->getExtraCondition()) : null,
        );
    }
}
