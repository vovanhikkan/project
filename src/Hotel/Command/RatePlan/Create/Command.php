<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlan\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private string $hotel;
    /** @Assert\NotBlank() */
    private array $name;
    /** @Assert\NotBlank() */
    private array $description;
    /** @Assert\NotBlank() */
    private string $ratePlanGroup;
    /** @Assert\NotBlank() */
    private string $foodType;
    /** @Assert\NotBlank() */
    private string $taxType;

    /** @Assert\NotBlank() */
    private string $activeFrom;
    /** @Assert\NotBlank() */
    private string $activeTo;
    /** @Assert\NotBlank() */
    private string $shownFrom;
    /** @Assert\NotBlank() */
    private string $shownTo;

    private ?bool $isActive = null;
    private ?bool $isShownWithPackets = null;
    private ?array $extraCondition = null;
    private ?string $canBeCanceledDate = null;

    
    public function __construct(
        string $hotel,
        array $name,
        array $description,
        string $ratePlanGroup,
        string $foodType,
        string $taxType,
        string $activeFrom,
        string $activeTo,
        string $shownFrom,
        string $shownTo
    ) {
        $this->hotel = $hotel;
        $this->name = $name;
        $this->description = $description;
        $this->ratePlanGroup = $ratePlanGroup;
        $this->foodType = $foodType;
        $this->taxType = $taxType;
        $this->activeFrom = $activeFrom;
        $this->activeTo = $activeTo;
        $this->shownFrom = $shownFrom;
        $this->shownTo = $shownTo;
    }

    /**
     * @return string
     */
    public function getHotel(): string
    {
        return $this->hotel;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getRatePlanGroup(): string
    {
        return $this->ratePlanGroup;
    }

    /**
     * @return string
     */
    public function getFoodType(): string
    {
        return $this->foodType;
    }

    /**
     * @return string
     */
    public function getTaxType(): string
    {
        return $this->taxType;
    }
    
    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @return bool|null
     */
    public function getIsShownWithPackets(): ?bool
    {
        return $this->isShownWithPackets;
    }

    /**
     * @return string
     */
    public function getActiveFrom(): string
    {
        return $this->activeFrom;
    }

    /**
     * @return string
     */
    public function getActiveTo(): string
    {
        return $this->activeTo;
    }

    /**
     * @return string
     */
    public function getShownFrom(): string
    {
        return $this->shownFrom;
    }

    /**
     * @return string
     */
    public function getShownTo(): string
    {
        return $this->shownTo;
    }

    /**
     * @param bool|null $isActive
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @param bool|null $isShownWithPackets
     */
    public function setIsShownWithPackets(?bool $isShownWithPackets): void
    {
        $this->isShownWithPackets = $isShownWithPackets;
    }

    /**
     * @return array|null
     */
    public function getExtraCondition(): ?array
    {
        return $this->extraCondition;
    }

    /**
     * @param array|null $extraCondition
     */
    public function setExtraCondition(?array $extraCondition): void
    {
        $this->extraCondition = $extraCondition;
    }

    /**
     * @return string|null
     */
    public function getCanBeCanceledDate(): ?string
    {
        return $this->canBeCanceledDate;
    }

    /**
     * @param string|null $canBeCanceledDate
     */
    public function setCanBeCanceledDate(?string $canBeCanceledDate): void
    {
        $this->canBeCanceledDate = $canBeCanceledDate;
    }
}
