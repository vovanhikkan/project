<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlan\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlan\Description;
use App\Hotel\Model\RatePlan\ExtraCondition;
use App\Hotel\Model\RatePlan\Name;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Repository\RatePlanGroupRepository;
use App\Hotel\Repository\TaxTypeRepository;
use App\Hotel\Service\RatePlan\Creator;
use DateTimeImmutable;

class Handler
{
    private Creator $creator;
    private HotelRepository $hotelRepository;
    private RatePlanGroupRepository $ratePlanGroupRepository;
    private FoodTypeRepository $foodTypeRepository;
    private TaxTypeRepository $taxTypeRepository;


    public function __construct(
        Creator $creator,
        HotelRepository $hotelRepository,
        RatePlanGroupRepository $ratePlanGroupRepository,
        FoodTypeRepository $foodTypeRepository,
        TaxTypeRepository $taxTypeRepository
    ) {
        $this->creator = $creator;
        $this->hotelRepository = $hotelRepository;
        $this->ratePlanGroupRepository = $ratePlanGroupRepository;
        $this->foodTypeRepository = $foodTypeRepository;
        $this->taxTypeRepository = $taxTypeRepository;
    }


    public function handle(Command $command): RatePlan
    {
        if ($hotel = $command->getHotel()) {
            $hotel = $this->hotelRepository->get(new Uuid($hotel));
        }

        if ($ratePlanGroup = $command->getRatePlanGroup()) {
            $ratePlanGroup = $this->ratePlanGroupRepository->get(new Uuid($ratePlanGroup));
        }

        if ($foodType = $command->getFoodType()) {
            $foodType = $this->foodTypeRepository->get(new Uuid($foodType));
        }

        if ($taxType = $command->getTaxType()) {
            $taxType = $this->taxTypeRepository->get(new Uuid($taxType));
        }

        $bundles = [];
        
        return $this->creator->create(
            Uuid::generate(),
            $hotel,
            new Name($command->getName()),
            new Description($command->getDescription()),
            $ratePlanGroup,
            $foodType,
            $taxType,
            new DateTimeImmutable($command->getActiveFrom()),
            new DateTimeImmutable($command->getActiveTo()),
            new DateTimeImmutable($command->getShownFrom()),
            new DateTimeImmutable($command->getShownTo()),
            $bundles !== null ? $bundles : null,
            $command->getExtraCondition() !== null ? new ExtraCondition($command->getExtraCondition()) : null,
            $command->getCanBeCanceledDate() !== null ? new DateTimeImmutable($command->getCanBeCanceledDate()) : null,
        );
    }
}
