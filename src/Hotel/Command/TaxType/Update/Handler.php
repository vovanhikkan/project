<?php

declare(strict_types=1);

namespace App\Hotel\Command\TaxType\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\TaxType\Name;
use App\Hotel\Model\TaxType\TaxType;
use App\Hotel\Model\TaxType\Value;
use App\Hotel\Repository\TaxTypeRepository;
use App\Hotel\Service\TaxType\Updater;

class Handler
{
    private Updater $updater;
    private TaxTypeRepository $taxTypeRepository;


    public function __construct(Updater $updater, TaxTypeRepository $taxTypeRepository)
    {
        $this->taxTypeRepository = $taxTypeRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): TaxType
    {
        $taxType = $this->taxTypeRepository->get(new Uuid($command->getId()));
        
        return $this->updater->update(
            $taxType,
            $command->getName() ? new Name($command->getName()) : null,
            $command->getValue() !== null ? new Value($command->getValue()) : null
        );
    }
}
