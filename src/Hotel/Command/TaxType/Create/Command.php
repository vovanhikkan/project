<?php

declare(strict_types=1);

namespace App\Hotel\Command\TaxType\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private array $name;
    /** @Assert\NotBlank() */
    private int $value;

    /**
     * Command constructor.
     * @param array $name
     * @param int $value
     */
    public function __construct(array $name, int $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
