<?php

declare(strict_types=1);

namespace App\Hotel\Command\TaxType\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\TaxType\Name;
use App\Hotel\Model\TaxType\TaxType;
use App\Hotel\Model\TaxType\Value;
use App\Hotel\Service\TaxType\Creator;

class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): TaxType
    {
        
        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
            new Value($command->getValue())
        );
    }
}
