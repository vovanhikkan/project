<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlanGroup\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Repository\RatePlanGroupRepository;
use App\Hotel\Service\RatePlanGroup\Updater;
use App\Hotel\Model\RatePlanGroup\Name;

class Handler
{
    private Updater $updater;
    private RatePlanGroupRepository $ratePlanGroupRepository;


    public function __construct(Updater $updater, RatePlanGroupRepository $ratePlanGroupRepository)
    {
        $this->ratePlanGroupRepository = $ratePlanGroupRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): RatePlanGroup
    {
        $ratePlanGroup = $this->ratePlanGroupRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $ratePlanGroup,
            $command->getName() ? new Name($command->getName()) : null,
        );
    }
}
