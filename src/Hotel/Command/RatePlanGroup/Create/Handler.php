<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlanGroup\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlanGroup\Name;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Service\RatePlanGroup\Creator;

class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): RatePlanGroup
    {
        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName())
        );
    }
}
