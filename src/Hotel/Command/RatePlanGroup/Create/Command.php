<?php

declare(strict_types=1);

namespace App\Hotel\Command\RatePlanGroup\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private array $name;
    
    private ?int $sort = null;
    
    public function __construct(
        array $name
    ) {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }
}
