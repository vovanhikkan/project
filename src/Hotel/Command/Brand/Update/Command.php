<?php

declare(strict_types=1);

namespace App\Hotel\Command\Brand\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private string $id;

    /**
     * @Assert\NotBlank()
     */
    private ?array $name;

    /**
     * Command constructor.
     * @param string $id
     * @param array|null $name
     */
    public function __construct(string $id, ?array $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }
}
