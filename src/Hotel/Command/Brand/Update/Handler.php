<?php

declare(strict_types=1);

namespace App\Hotel\Command\Brand\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Brand\Brand;
use App\Hotel\Model\Brand\Name;
use App\Hotel\Repository\BrandRepository;
use App\Hotel\Service\Brand\Updater;

class Handler
{
    private Updater $updater;
    private BrandRepository $brandRepository;


    public function __construct(Updater $updater, BrandRepository $brandRepository)
    {
        $this->brandRepository = $brandRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): Brand
    {
        $brand = $this->brandRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $brand,
            $command->getName() ? new Name($command->getName()) : null
        );
    }
}
