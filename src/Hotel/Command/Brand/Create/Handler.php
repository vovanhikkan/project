<?php

declare(strict_types=1);

namespace App\Hotel\Command\Brand\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Brand\Brand;
use App\Hotel\Model\Brand\Name;
use App\Hotel\Service\Brand\Creator;

class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): Brand
    {
        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
        );
    }
}
