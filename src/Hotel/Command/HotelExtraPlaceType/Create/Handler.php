<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelExtraPlaceType\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType;
use App\Hotel\Model\HotelExtraPlaceType\Name;
use App\Hotel\Repository\HotelAgeRangeRepository;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Service\HotelExtraPlaceType\Creator;

class Handler
{
    private Creator $creator;
    private HotelRepository $hotelRepository;
    private HotelAgeRangeRepository $hotelAgeRangeRepository;

    public function __construct(
        Creator $creator,
        HotelRepository $hotelRepository,
        HotelAgeRangeRepository $hotelAgeRangeRepository
    ) {
        $this->creator = $creator;
        $this->hotelRepository = $hotelRepository;
        $this->hotelAgeRangeRepository = $hotelAgeRangeRepository;
    }

    public function handle(Command $command): HotelExtraPlaceType
    {
        if ($hotel = $command->getHotel()) {
            $hotel = $this->hotelRepository->get(new Uuid($hotel));
        }

        $ageRanges = [];
        if ($ageRangeIds = $command->getAgeRanges()) {
            foreach ($ageRangeIds as $id) {
                $ageRanges[] = $this->hotelAgeRangeRepository->get(new Uuid($id));
            }
        }

        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
            $hotel,
            $ageRanges
        );
    }
}
