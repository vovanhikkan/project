<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelExtraPlaceType\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /** @Assert\NotBlank() */
    private array $name;
    private string $hotel;

    private ?array $ageRanges = null;


    /**
     * Command constructor.
     * @param array $name
     * @param string $hotel
     */
    public function __construct(array $name, string $hotel)
    {
        $this->name = $name;
        $this->hotel = $hotel;
    }

    /**
     * @return array|null
     */
    public function getAgeRanges(): ?array
    {
        return $this->ageRanges;
    }

    /**
     * @param array|null $ageRanges
     */
    public function setAgeRanges(?array $ageRanges): void
    {
        $this->ageRanges = $ageRanges;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }


    /**
     * @return string|null
     */
    public function getHotel(): ?string
    {
        return $this->hotel;
    }

}
