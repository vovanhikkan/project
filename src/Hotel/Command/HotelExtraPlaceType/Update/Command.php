<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelExtraPlaceType\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;
    private ?array $name = null;
    private ?string $hotel = null;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getHotel(): ?string
    {
        return $this->hotel;
    }

    /**
     * @param string|null $hotel
     */
    public function setHotel(?string $hotel): void
    {
        $this->hotel = $hotel;
    }
    
}
