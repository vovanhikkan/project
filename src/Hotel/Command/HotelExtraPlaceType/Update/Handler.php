<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelExtraPlaceType\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelExtraPlaceType\Name;
use App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType;
use App\Hotel\Repository\HotelExtraPlaceTypeRepository;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Service\HotelExtraPlaceType\Updater;

class Handler
{
    private Updater $updater;
    private HotelExtraPlaceTypeRepository $hotelExtraPlaceTypeRepository;
    private HotelRepository $hotelRepository;
    

    public function __construct(
        Updater $updater,
        HotelExtraPlaceTypeRepository $hotelExtraPlaceTypeRepository,
        HotelRepository $hotelRepository
    ) {
        $this->hotelExtraPlaceTypeRepository = $hotelExtraPlaceTypeRepository;
        $this->hotelRepository = $hotelRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): HotelExtraPlaceType
    {
        $hotelExtraPlaceType = $this->hotelExtraPlaceTypeRepository->get(new Uuid($command->getId()));

        if ($hotel = $command->getHotel()) {
            $hotel = $this->hotelRepository->get(new Uuid($hotel));
        }

        return $this->updater->update(
            $hotelExtraPlaceType,
            $command->getName() !== null ? new Name($command->getName()) : null,
            $hotel
        );
    }
}
