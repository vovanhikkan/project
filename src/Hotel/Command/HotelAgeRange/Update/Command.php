<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelAgeRange\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?string $hotel = null;

    private ?int $ageFrom = null;

    private ?int $ageTo = null;


    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getHotel(): ?string
    {
        return $this->hotel;
    }

    /**
     * @param string|null $hotel
     */
    public function setHotel(?string $hotel): void
    {
        $this->hotel = $hotel;
    }

    /**
     * @return int|null
     */
    public function getAgeFrom(): ?int
    {
        return $this->ageFrom;
    }

    /**
     * @param int|null $ageFrom
     */
    public function setAgeFrom(?int $ageFrom): void
    {
        $this->ageFrom = $ageFrom;
    }

    /**
     * @return int|null
     */
    public function getAgeTo(): ?int
    {
        return $this->ageTo;
    }

    /**
     * @param int|null $ageTo
     */
    public function setAgeTo(?int $ageTo): void
    {
        $this->ageTo = $ageTo;
    }
}
