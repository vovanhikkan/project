<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelAgeRange\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelAgeRange\AgeFrom;
use App\Hotel\Model\HotelAgeRange\AgeTo;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\Hotel\Repository\HotelAgeRangeRepository;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Service\HotelAgeRange\Updater;

class Handler
{
    private Updater $updater;
    private HotelAgeRangeRepository $hotelAgeRangeRepository;
    private HotelRepository $hotelRepository;


    public function __construct(
        Updater $updater,
        HotelAgeRangeRepository $hotelAgeRangeRepository,
        HotelRepository $hotelRepository
    ) {
        $this->hotelAgeRangeRepository = $hotelAgeRangeRepository;
        $this->hotelRepository = $hotelRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): HotelAgeRange
    {
        $hotelAgeRange = $this->hotelAgeRangeRepository->get(new Uuid($command->getId()));

        if ($hotel = $command->getHotel()) {
            $hotel = $this->hotelRepository->get(new Uuid($command->getHotel()));
        }

        return $this->updater->update(
            $hotelAgeRange,
            $hotel,
            $command->getAgeFrom() ? new AgeFrom($command->getAgeFrom()) : null,
            $command->getAgeTo() ? new AgeTo($command->getAgeTo()) : null
        );
    }
}
