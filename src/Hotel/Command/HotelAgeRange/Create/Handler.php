<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelAgeRange\Create;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use App\Hotel\Model\HotelAgeRange\AgeFrom;
use App\Hotel\Model\HotelAgeRange\AgeTo;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Service\HotelAgeRange\Creator;

class Handler
{
    private Creator $creator;
    private HotelRepository $hotelRepository;


    public function __construct(Creator $creator, HotelRepository $hotelRepository)
    {
        $this->creator = $creator;
        $this->hotelRepository = $hotelRepository;
    }


    public function handle(Command $command): HotelAgeRange
    {
        $hotel = $this->hotelRepository->get(new Uuid($command->getHotel()));

        return $this->creator->create(
            Uuid::generate(),
            $hotel,
            new AgeFrom($command->getAgeFrom()),
            new AgeTo($command->getAgeTo())
        );
    }
}
