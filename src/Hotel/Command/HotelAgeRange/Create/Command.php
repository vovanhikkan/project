<?php

declare(strict_types=1);

namespace App\Hotel\Command\HotelAgeRange\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{

    /** @Assert\NotBlank() */
    private string $hotel;
    /** @Assert\NotBlank() */
    private int $ageFrom;
    /** @Assert\NotBlank() */
    private int $ageTo;


    /**
     * Command constructor.
     * @param string $hotel
     * @param int $ageFrom
     * @param int $ageTo
     */
    public function __construct(
        string $hotel,
        int $ageFrom,
        int $ageTo
    ) {
        $this->hotel = $hotel;
        $this->ageFrom = $ageFrom;
        $this->ageTo = $ageTo;
    }

    /**
     * @return string
     */
    public function getHotel(): string
    {
        return $this->hotel;
    }

    /**
     * @return int
     */
    public function getAgeFrom(): int
    {
        return $this->ageFrom;
    }

    /**
     * @return int
     */
    public function getAgeTo(): int
    {
        return $this->ageTo;
    }
}
