<?php

declare(strict_types=1);

namespace App\Hotel\Command\Guest\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;

    private ?string $firstName = null;
    private ?string $lastName = null;
    private ?int $age = null;
    private ?bool $mainGuest = null;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int|null
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int|null $age
     */
    public function setAge(?int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return bool|null
     */
    public function getMainGuest(): ?bool
    {
        return $this->mainGuest;
    }

    /**
     * @param bool|null $mainGuest
     */
    public function setMainGuest(?bool $mainGuest): void
    {
        $this->mainGuest = $mainGuest;
    }
}
