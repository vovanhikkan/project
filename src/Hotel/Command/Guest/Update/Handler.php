<?php

declare(strict_types=1);

namespace App\Hotel\Command\Guest\Update;

use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Repository\GuestRepository;
use App\Hotel\Service\Guest\Updater;
use App\Hotel\Model\Guest\Age;

class Handler
{
    private Updater $updater;
    private GuestRepository $guestRepository;


    public function __construct(Updater $updater, GuestRepository $guestRepository)
    {
        $this->guestRepository = $guestRepository;
        $this->updater = $updater;
    }


    public function handle(Command $command): Guest
    {
        $guest = $this->guestRepository->get(new Uuid($command->getId()));

        return $this->updater->update(
            $guest,
            $command->getFirstName() !== null ? new FirstName($command->getFirstName()) : null,
            $command->getLastName() !== null ? new LastName($command->getLastName()) : null,
            $command->getAge() !== null ? new Age($command->getAge()) : null,
            $command->getMainGuest()
        );
    }
}
