<?php

declare(strict_types=1);

namespace App\Hotel\Command\Guest\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $firstName;
    /**
     * @Assert\NotBlank()
     */
    private string $lastName;

    /**
     * @Assert\NotBlank()
     */
    private int $age;

    private ?bool $mainGuest = null;

    /**
     * Command constructor.
     * @param string $firstName
     * @param string $lastName
     * @param int $age
     */
    public function __construct(
        string $firstName,
        string $lastName,
        int $age
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return bool|null
     */
    public function isMainGuest(): ?bool
    {
        return $this->mainGuest;
    }

    /**
     * @param bool|null $mainGuest
     */
    public function setMainGuest(?bool $mainGuest): void
    {
        $this->mainGuest = $mainGuest;
    }
}
