<?php

declare(strict_types=1);

namespace App\Hotel\Command\Guest\Create;

use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Guest\Age;
use App\Hotel\Model\Guest\Guest;
use App\Hotel\Service\Guest\Creator;


class Handler
{
    private Creator $creator;


    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }


    public function handle(Command $command): Guest
    {
        return $this->creator->create(
            Uuid::generate(),
            new FirstName($command->getFirstName()),
            new LastName($command->getLastName()),
            new Age($command->getAge()),
            $command->isMainGuest()
        );
    }
}
