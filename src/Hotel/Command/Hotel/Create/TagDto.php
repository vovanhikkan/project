<?php

declare(strict_types=1);

namespace App\Hotel\Command\Hotel\Create;

use App\Content\Model\Tag\ColorBorder;
use App\Content\Model\Tag\ColorText;
use Psr\Http\Message\UploadedFileInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PriceDto.
 */
class TagDto
{
    /**
     * @Assert\NotBlank(message="")
     */
    private array $name;

    /**
     * @Assert\NotBlank(message="")
     */
    private ?UploadedFileInterface $icon = null;

    /**
     * @Assert\NotBlank(message="")
     */
    private ?UploadedFileInterface $iconSvg = null;

    private ?string $colorText = null;

    private ?string $colorBorder = null;

    /**
     * TagDto constructor.
     * @param array $name
     */
    public function __construct(array $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @param UploadedFileInterface|null $icon
     */
    public function setIcon(?UploadedFileInterface $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @param UploadedFileInterface|null $iconSvg
     */
    public function setIconSvg(?UploadedFileInterface $iconSvg): void
    {
        $this->iconSvg = $iconSvg;
    }

    /**
     * @return UploadedFileInterface|null
     */
    public function getIcon(): ?UploadedFileInterface
    {
        return $this->icon;
    }

    /**
     * @return UploadedFileInterface|null
     */
    public function getIconSvg(): ?UploadedFileInterface
    {
        return $this->iconSvg;
    }

    /**
     * @return string|null
     */
    public function getColorText(): ?string
    {
        return $this->colorText;
    }

    /**
     * @param string|null $colorText
     */
    public function setColorText(?string $colorText): void
    {
        $this->colorText = $colorText;
    }

    /**
     * @return string|null
     */
    public function getColorBorder(): ?string
    {
        return $this->colorBorder;
    }

    /**
     * @param string|null $colorBorder
     */
    public function setColorBorder(?string $colorBorder): void
    {
        $this->colorBorder = $colorBorder;
    }
}
