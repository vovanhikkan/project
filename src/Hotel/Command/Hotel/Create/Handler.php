<?php

declare(strict_types=1);

namespace App\Hotel\Command\Hotel\Create;

use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\ColorBorder;
use App\Content\Model\Tag\ColorText;
use App\Hotel\Dto\HotelTagDto;
use App\Hotel\Model\Hotel\Address;
use App\Hotel\Model\Hotel\CancelPrepayment;
use App\Hotel\Model\Hotel\CheckInTime;
use App\Hotel\Model\Hotel\CheckOutTime;
use App\Hotel\Model\Hotel\Description;
use App\Hotel\Model\Hotel\ExtraBeds;
use App\Hotel\Model\Hotel\ExtraInfo;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\Hotel\Lat;
use App\Hotel\Model\Hotel\Links;
use App\Hotel\Model\Hotel\Lon;
use App\Hotel\Model\Hotel\Name;
use App\Hotel\Model\Hotel\Payments;
use App\Hotel\Model\Hotel\Pets;
use App\Hotel\Model\Hotel\StarCount;
use App\Hotel\Repository\BrandRepository;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Repository\HotelTypeRepository;
use App\Hotel\Service\Hotel\Creator;
use App\Location\Repository\LocationRepository;
use App\Place\Repository\PlaceRepository;
use App\Storage\Model\File\File;
use \App\Storage\Service\File\Creator as FileCreator;
use App\Storage\Repository\FileRepository;
use DateTimeImmutable;

class Handler
{
    private Creator $creator;
    private LocationRepository $locationRepository;
    private FileRepository $fileRepository;
    private FileCreator $fileCreator;
    private BrandRepository $brandRepository;
    private FoodTypeRepository $foodTypeRepository;
    private HotelTypeRepository $hotelTypeRepository;
    private PlaceRepository $placeRepository;

    public function __construct(
        Creator $creator,
        LocationRepository $locationRepository,
        FileRepository $fileRepository,
        BrandRepository $brandRepository,
        FoodTypeRepository $foodTypeRepository,
        HotelTypeRepository $hotelTypeRepository,
        FileCreator $fileCreator,
        PlaceRepository $placeRepository
    ) {
        $this->creator = $creator;
        $this->locationRepository = $locationRepository;
        $this->fileRepository = $fileRepository;
        $this->brandRepository = $brandRepository;
        $this->foodTypeRepository = $foodTypeRepository;
        $this->hotelTypeRepository = $hotelTypeRepository;
        $this->fileCreator = $fileCreator;
        $this->placeRepository = $placeRepository;
    }

    public function handle(Command $command): Hotel
    {
        if ($location = $command->getLocation()) {
            $location = $this->locationRepository->get(new Uuid($location));
        }

        if ($logo = $command->getLogo()) {
            $logo = $this->fileRepository->getFileOrNull($logo);
        }

        if ($logoSvg = $command->getLogoSvg()) {
            $logoSvg = $this->fileRepository->getFileOrNull($logoSvg);
        }

        if($hotelPhoto = $command->getHotelPhoto()) {
            $hotelPhoto = $this->fileRepository->getFileOrNull($hotelPhoto);
        }

        $type = $this->hotelTypeRepository->get(new Uuid($command->getType()));

        if ($brand = $command->getBrand()) {
            $brand = $this->brandRepository->get(new Uuid($brand));
        }

        $places = [];
        if ($placeIds = $command->getPlaces()) {
            foreach ($placeIds as $id) {
                $places[] = $this->placeRepository->get(new Uuid($id));
            }
        }

        $tagsDto = [];
        if ($tags = $command->getTags()) {
            foreach ($tags as $tag) {
                /**
                 * @var TagDto $tag
                 */
                $tagDto = new HotelTagDto(
                    new \App\Content\Model\Tag\Name($tag->getName()),
                );

                if ($icon = $tag->getIcon()) {
                    $tagDto->setIcon($this->fileCreator->upload(Uuid::generate(), $icon));
                }
                if ($iconSvg = $tag->getIconSvg()) {
                    $tagDto->setIconSvg($this->fileCreator->upload(Uuid::generate(), $tag->getIconSvg()));
                }
                if ($colorText = $tag->getColorText()) {
                    $tagDto->setColorText(new ColorText($colorText));
                }
                if ($colorBorder = $tag->getColorBorder()) {
                    $tagDto->setColorBorder(new ColorBorder($colorBorder));
                }

                $tagsDto[] = $tagDto;
            }
        }

        return $this->creator->create(
            Uuid::generate(),
            new Name($command->getName()),
            $command->getDescription() !== null ? new Description($command->getDescription()) : null,
            new StarCount($command->getStarCount()),
            $location,
            new Address($command->getAddress()),
            $logo,
            $logoSvg,
            $command->getLinks() !== null ? new Links($command->getLinks()) : null,
            $command->getLat() !== null ? new Lat($command->getLat()) : null,
            $command->getLon() !== null ? new Lon($command->getLon()) : null,
            $type,
            $brand,
            $tags !== null ? $tagsDto : null,
            $places,
            $command->getCheckInTime() !== null ? new CheckInTime($command->getCheckInTime()) : null,
            $command->getCheckOutTime() !== null ? new CheckOutTime($command->getCheckOutTime()) : null,
            $command->getCancelPrepayment() !== null ? new CancelPrepayment($command->getCancelPrepayment()) : null,
            $command->getExtraBeds() !== null ? new ExtraBeds($command->getExtraBeds()) : null,
            $command->getPets() !== null ? new Pets($command->getPets()) : null,
            $command->getExtraInfo() !== null ? new ExtraInfo($command->getExtraInfo()) : null,
            $command->getPayments() !== null ? new Payments($command->getPayments()) : null,
            $hotelPhoto
        );
    }
}
