<?php

declare(strict_types=1);

namespace App\Hotel\Command\Hotel\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     *
     * private string $id;
     */
    private string $id;
    private ?array $name = null;
    private ?array $description = null;
    private ?int $starCount = null;
    private ?string $location = null;
    private ?array $address = null;
    private ?string $logo = null;
    private ?string $logoSvg = null;
    private ?array $links = null;
    private ?float $lat = null;
    private ?float $lon = null;
    private ?string $type = null;
    private ?string $brand = null;
    private ?array $checkInTime = null;
    private ?array $checkOutTime = null;
    private ?array $cancelPrepayment = null;
    private ?array $extraBeds = null;
    private ?array $pets = null;
    private ?array $extraInfo = null;
    private ?array $payments = null;
    private ?string $hotelPhoto = null;
    private ?array $tags = null;

    /**
     * Command constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @param array|null $name
     */
    public function setName(?array $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @param array|null $description
     */
    public function setDescription(?array $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int|null
     */
    public function getStarCount(): ?int
    {
        return $this->starCount;
    }

    /**
     * @param int|null $starCount
     */
    public function setStarCount(?int $starCount): void
    {
        $this->starCount = $starCount;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     */
    public function setLocation(?string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?array
    {
        return $this->address;
    }

    /**
     * @param array|null $address
     */
    public function setAddress(?array $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        return $this->logo;
    }

    /**
     * @param string|null $logo
     */
    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @return string|null
     */
    public function getLogoSvg(): ?string
    {
        return $this->logoSvg;
    }

    /**
     * @param string|null $logoSvg
     */
    public function setLogoSvg(?string $logoSvg): void
    {
        $this->logoSvg = $logoSvg;
    }

    /**
     * @return array|null
     */
    public function getLinks(): ?array
    {
        return $this->links;
    }

    /**
     * @param array|null $links
     */
    public function setLinks(?array $links): void
    {
        $this->links = $links;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     */
    public function setLat(?float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float|null
     */
    public function getLon(): ?float
    {
        return $this->lon;
    }

    /**
     * @param float|null $lon
     */
    public function setLon(?float $lon): void
    {
        $this->lon = $lon;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * @param string|null $brand
     */
    public function setBrand(?string $brand): void
    {
        $this->brand = $brand;
    }
    
    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getCheckInTime(): ?array
    {
        return $this->checkInTime;
    }

    /**
     * @param array|null $checkInTime
     */
    public function setCheckInTime(?array $checkInTime): void
    {
        $this->checkInTime = $checkInTime;
    }

    /**
     * @return array|null
     */
    public function getCheckOutTime(): ?array
    {
        return $this->checkOutTime;
    }

    /**
     * @param array|null $checkOutTime
     */
    public function setCheckOutTime(?array $checkOutTime): void
    {
        $this->checkOutTime = $checkOutTime;
    }

    /**
     * @return array|null
     */
    public function getCancelPrepayment(): ?array
    {
        return $this->cancelPrepayment;
    }

    /**
     * @param array|null $cancelPrepayment
     */
    public function setCancelPrepayment(?array $cancelPrepayment): void
    {
        $this->cancelPrepayment = $cancelPrepayment;
    }

    /**
     * @return array|null
     */
    public function getExtraBeds(): ?array
    {
        return $this->extraBeds;
    }

    /**
     * @param array|null $extraBeds
     */
    public function setExtraBeds(?array $extraBeds): void
    {
        $this->extraBeds = $extraBeds;
    }

    /**
     * @return array|null
     */
    public function getPets(): ?array
    {
        return $this->pets;
    }

    /**
     * @param array|null $pets
     */
    public function setPets(?array $pets): void
    {
        $this->pets = $pets;
    }

    /**
     * @return array|null
     */
    public function getExtraInfo(): ?array
    {
        return $this->extraInfo;
    }

    /**
     * @param array|null $extraInfo
     */
    public function setExtraInfo(?array $extraInfo): void
    {
        $this->extraInfo = $extraInfo;
    }

    /**
     * @return array|null
     */
    public function getPayments(): ?array
    {
        return $this->payments;
    }

    /**
     * @param array|null $payments
     */
    public function setPayments(?array $payments): void
    {
        $this->payments = $payments;
    }

    /**
     * @return string|null
     */
    public function getHotelPhoto(): ?string
    {
        return $this->hotelPhoto;
    }

    /**
     * @param string|null $hotelPhoto
     */
    public function setHotelPhoto(?string $hotelPhoto): void
    {
        $this->hotelPhoto = $hotelPhoto;
    }

    /**
     * @return array|null
     */
    public function getTags(): ?array
    {
        return $this->tags;
    }

    /**
     * @param array|null $tags
     */
    public function setTags(?array $tags): void
    {
        $this->tags = $tags;
    }
}
