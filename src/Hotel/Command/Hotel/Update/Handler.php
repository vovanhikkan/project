<?php

declare(strict_types=1);

namespace App\Hotel\Command\Hotel\Update;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Hotel\Address;
use App\Hotel\Model\Hotel\CancelPrepayment;
use App\Hotel\Model\Hotel\CheckInTime;
use App\Hotel\Model\Hotel\CheckOutTime;
use App\Hotel\Model\Hotel\Description;
use App\Hotel\Model\Hotel\ExtraBeds;
use App\Hotel\Model\Hotel\ExtraInfo;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\Hotel\Lat;
use App\Hotel\Model\Hotel\Links;
use App\Hotel\Model\Hotel\Lon;
use App\Hotel\Model\Hotel\Name;
use App\Hotel\Model\Hotel\Payments;
use App\Hotel\Model\Hotel\Pets;
use App\Hotel\Model\Hotel\StarCount;
use App\Hotel\Repository\BrandRepository;
use App\Hotel\Repository\FoodTypeRepository;
use App\Hotel\Repository\HotelRepository;
use App\Hotel\Repository\HotelTypeRepository;
use App\Hotel\Service\Hotel\Updater;
use App\Location\Repository\LocationRepository;
use App\Storage\Repository\FileRepository;
use DateTimeImmutable;

class Handler
{
    private Updater $updater;
    private HotelRepository $hotelRepository;
    private LocationRepository $locationRepository;
    private FileRepository $fileRepository;
    private BrandRepository $brandRepository;
    private FoodTypeRepository $foodTypeRepository;
    private HotelTypeRepository $hotelTypeRepository;


    public function __construct(
        Updater $updater,
        HotelRepository $hotelRepository,
        LocationRepository $locationRepository,
        FileRepository $fileRepository,
        BrandRepository $brandRepository,
        FoodTypeRepository $foodTypeRepository,
        HotelTypeRepository $hotelTypeRepository
    ) {
        $this->hotelRepository = $hotelRepository;
        $this->updater = $updater;
        $this->locationRepository = $locationRepository;
        $this->fileRepository = $fileRepository;
        $this->brandRepository = $brandRepository;
        $this->foodTypeRepository = $foodTypeRepository;
        $this->hotelTypeRepository = $hotelTypeRepository;
    }


    public function handle(Command $command): Hotel
    {
        $hotel = $this->hotelRepository->get(new Uuid($command->getId()));

        if ($location = $command->getLocation()) {
            $location = $this->locationRepository->get(new Uuid($location));
        }

        if ($logo = $command->getLogo()) {
            $logo = $this->fileRepository->getFileOrNull($logo);
        }

        if ($logoSvg = $command->getLogoSvg()) {
            $logoSvg = $this->fileRepository->getFileOrNull($logoSvg);
        }

        if ($hotelPhoto = $command->getHotelPhoto()) {
            $hotelPhoto = $this->fileRepository->getFileOrNull($hotelPhoto);
        }

        if ($type = $command->getType()) {
            $type = $this->hotelTypeRepository->get(new Uuid($command->getType()));
        }

        if ($brand = $command->getBrand()) {
            $brand = $this->brandRepository->get(new Uuid($brand));
        }
        

        return $this->updater->update(
            $hotel,
            $command->getName() !== null ? new Name($command->getName()) : null,
            $command->getDescription() !== null ? new Description($command->getDescription()) : null,
            $command->getStarCount() !== null ? new StarCount($command->getStarCount()) : null,
            $location,
            $command->getAddress() !== null ? new Address($command->getAddress()) : null,
            $logo,
            $logoSvg,
            $command->getLinks() !== null ? new Links($command->getLinks()) : null,
            $command->getLat() !== null ? new Lat($command->getLat()) : null,
            $command->getLon() !== null ? new Lon($command->getLon()) : null,
            $type,
            $brand,
            $command->getCheckInTime() !== null ? new CheckInTime($command->getCheckInTime()) : null,
            $command->getCheckOutTime() !== null ? new CheckOutTime($command->getCheckOutTime()) : null,
            $command->getCancelPrepayment() !== null ? new CancelPrepayment($command->getCancelPrepayment()) : null,
            $command->getExtraBeds() !== null ? new ExtraBeds($command->getExtraBeds()) : null,
            $command->getPets() !== null ? new Pets($command->getPets()) : null,
            $command->getExtraInfo() !== null ? new ExtraInfo($command->getExtraInfo()) : null,
            $command->getPayments() !== null ? new Payments($command->getPayments()) : null,
            $hotelPhoto
        );
    }
}
