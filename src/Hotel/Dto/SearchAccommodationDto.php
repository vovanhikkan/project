<?php

declare(strict_types=1);

namespace App\Hotel\Dto;

use Symfony\Component\Validator\Constraints as Assert;


class SearchAccommodationDto
{
    private string $hotel;
    private ?array $foodTypes = null;
    private ?array $facilities = null;

    /**
     * @Assert\Date()
     */
    private ?string $dateFrom = null;

    /**
     * @Assert\Date()
     */
    private ?string $dateTo = null;
    private ?int $adultCount = null;
    private ?int $childCount = null;
    private ?array $childAges = null;

    private ?string $bundle = null;

    private ?bool $canBeCanceled = false;

    /**
     * SearchAccommodationDto constructor.
     * @param string $hotel
     */
    public function __construct(string $hotel)
    {
        $this->hotel = $hotel;
    }

    /**
     * @return array|null
     */
    public function getFoodTypes(): ?array
    {
        return $this->foodTypes;
    }

    /**
     * @param array|null $foodTypes
     */
    public function setFoodTypes(?array $foodTypes): void
    {
        $this->foodTypes = $foodTypes;
    }

    /**
     * @return array|null
     */
    public function getFacilities(): ?array
    {
        return $this->facilities;
    }

    /**
     * @param array|null $facilities
     */
    public function setFacilities(?array $facilities): void
    {
        $this->facilities = $facilities;
    }

    /**
     * @return string|null
     */
    public function getDateFrom(): ?string
    {
        return $this->dateFrom;
    }

    /**
     * @param string|null $dateFrom
     */
    public function setDateFrom(?string $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return string|null
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param string|null $dateTo
     */
    public function setDateTo(?string $dateTo): void
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return int|null
     */
    public function getAdultCount(): ?int
    {
        return $this->adultCount;
    }

    /**
     * @param int|null $adultCount
     */
    public function setAdultCount(?int $adultCount): void
    {
        $this->adultCount = $adultCount;
    }

    /**
     * @return int|null
     */
    public function getChildCount(): ?int
    {
        return $this->childCount;
    }

    /**
     * @param int|null $childCount
     */
    public function setChildCount(?int $childCount): void
    {
        $this->childCount = $childCount;
    }

    /**
     * @return array|null
     */
    public function getChildAges(): ?array
    {
        return $this->childAges;
    }

    /**
     * @param array|null $childAges
     */
    public function setChildAges(?array $childAges): void
    {
        $this->childAges = $childAges;
    }

    /**
     * @return string
     */
    public function getHotel(): string
    {
        return $this->hotel;
    }

    /**
     * @param string|null $bundle
     */
    public function setBundle(?string $bundle): void
    {
        $this->bundle = $bundle;
    }

    /**
     * @return string|null
     */
    public function getBundle(): ?string
    {
        return $this->bundle;
    }

    /**
     * @return bool|null
     */
    public function getCanBeCanceled(): ?bool
    {
        return $this->canBeCanceled;
    }

    /**
     * @param bool|null $canBeCanceled
     */
    public function setCanBeCanceled(?bool $canBeCanceled): void
    {
        $this->canBeCanceled = $canBeCanceled;
    }
}