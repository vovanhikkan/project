<?php

declare(strict_types=1);

namespace App\Hotel\Dto;

use App\Content\Model\Tag\ColorBorder;
use App\Content\Model\Tag\ColorText;
use App\Content\Model\Tag\Name;
use App\Storage\Model\File\File;

/**
 * HotelTagDto.
 */
class HotelTagDto
{
    private Name $name;
    private ?File $icon = null;
    private ?File $iconSvg = null;
    private ?ColorText $colorText = null;
    private ?ColorBorder $colorBorder = null;

    /**
     * HotelTagDto constructor.
     * @param Name $name
     */
    public function __construct(Name $name)
    {
        $this->name = $name;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return File|null
     */
    public function getIcon(): ?File
    {
        return $this->icon;
    }

    /**
     * @param File|null $icon
     */
    public function setIcon(?File $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return File|null
     */
    public function getIconSvg(): ?File
    {
        return $this->iconSvg;
    }

    /**
     * @param File|null $iconSvg
     */
    public function setIconSvg(?File $iconSvg): void
    {
        $this->iconSvg = $iconSvg;
    }

    /**
     * @return ColorText|null
     */
    public function getColorText(): ?ColorText
    {
        return $this->colorText;
    }

    /**
     * @param ColorText|null $colorText
     */
    public function setColorText(?ColorText $colorText): void
    {
        $this->colorText = $colorText;
    }

    /**
     * @return ColorBorder|null
     */
    public function getColorBorder(): ?ColorBorder
    {
        return $this->colorBorder;
    }

    /**
     * @param ColorBorder|null $colorBorder
     */
    public function setColorBorder(?ColorBorder $colorBorder): void
    {
        $this->colorBorder = $colorBorder;
    }
}
