<?php

declare(strict_types=1);

namespace App\Hotel\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class SearchDto
{
    private ?array $starCounts = null;
    private ?array $hotelTypes = null;
    private ?array $foodTypes = null;
    private ?array $brands = null;
    private ?array $facilities = null;
    private ?array $bundles = null;
    /**
     * @Assert\Date()
     */
    private ?string $dateFrom = null;

    /**
     * @Assert\Date()
     */
    private ?string $dateTo = null;
    private ?int $adultCount = null;
    private ?int $childCount = null;
    private ?array $childAges = null;
    private ?array $bedTypes = null;
    private ?int $priceFrom = null;
    private ?int $priceTo = null;

    /**
     * @return int|null
     */
    public function getPriceFrom(): ?int
    {
        return $this->priceFrom;
    }

    /**
     * @param int|null $priceFrom
     */
    public function setPriceFrom(?int $priceFrom): void
    {
        $this->priceFrom = $priceFrom;
    }

    /**
     * @return int|null
     */
    public function getPriceTo(): ?int
    {
        return $this->priceTo;
    }

    /**
     * @param int|null $priceTo
     */
    public function setPriceTo(?int $priceTo): void
    {
        $this->priceTo = $priceTo;
    }

    /**
     * @return array|null
     */
    public function getBedTypes(): ?array
    {
        return $this->bedTypes;
    }

    /**
     * @param array|null $bedTypes
     */
    public function setBedTypes(?array $bedTypes): void
    {
        $this->bedTypes = $bedTypes;
    }

    /**
     * @return array|null
     */
    public function getStarCounts(): ?array
    {
        return $this->starCounts;
    }

    /**
     * @param array|null $starCounts
     */
    public function setStarCounts(?array $starCounts): void
    {
        $this->starCounts = $starCounts;
    }

    /**
     * @return array|null
     */
    public function getHotelTypes(): ?array
    {
        return $this->hotelTypes;
    }

    /**
     * @param array|null $hotelTypes
     */
    public function setHotelTypes(?array $hotelTypes): void
    {
        $this->hotelTypes = $hotelTypes;
    }

    /**
     * @return array|null
     */
    public function getFoodTypes(): ?array
    {
        return $this->foodTypes;
    }

    /**
     * @param array|null $foodTypes
     */
    public function setFoodTypes(?array $foodTypes): void
    {
        $this->foodTypes = $foodTypes;
    }

    /**
     * @return array|null
     */
    public function getBrands(): ?array
    {
        return $this->brands;
    }

    /**
     * @param array|null $brands
     */
    public function setBrands(?array $brands): void
    {
        $this->brands = $brands;
    }

    /**
     * @return array|null
     */
    public function getFacilities(): ?array
    {
        return $this->facilities;
    }

    /**
     * @param array|null $facilities
     */
    public function setFacilities(?array $facilities): void
    {
        $this->facilities = $facilities;
    }

    /**
     * @return string|null
     */
    public function getDateFrom(): ?string
    {
        return $this->dateFrom;
    }

    /**
     * @param string|null $dateFrom
     */
    public function setDateFrom(?string $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return string|null
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param string|null $dateTo
     */
    public function setDateTo(?string $dateTo): void
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return int|null
     */
    public function getAdultCount(): ?int
    {
        return $this->adultCount;
    }

    /**
     * @param int|null $adultCount
     */
    public function setAdultCount(?int $adultCount): void
    {
        $this->adultCount = $adultCount;
    }

    /**
     * @return int|null
     */
    public function getChildCount(): ?int
    {
        return $this->childCount;
    }

    /**
     * @param int|null $childCount
     */
    public function setChildCount(?int $childCount): void
    {
        $this->childCount = $childCount;
    }

    /**
     * @return array|null
     */
    public function getChildAges(): ?array
    {
        return $this->childAges;
    }

    /**
     * @param array|null $childAges
     */
    public function setChildAges(?array $childAges): void
    {
        $this->childAges = $childAges;
    }

    /**
     * @return array|null
     */
    public function getBundles(): ?array
    {
        return $this->bundles;
    }

    /**
     * @param array|null $bundles
     */
    public function setBundles(?array $bundles): void
    {
        $this->bundles = $bundles;
    }
}
