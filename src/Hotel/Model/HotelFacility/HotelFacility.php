<?php

namespace App\Hotel\Model\HotelFacility;

use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\Facility\Facility;
use App\Application\ValueObject\Uuid;


/**
 * HotelFacility
 *
 * @ORM\Table(name="hotel_facility")
 * @ORM\Entity()
 */
class HotelFacility
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="uuid", nullable=false)
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private Hotel $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Facility\Facility")
     * @ORM\JoinColumn(name="facility_id", referencedColumnName="id")
     */
    private Facility $facility;

    /**
     * HotelFacility constructor.
     * @param Uuid $id
     * @param Hotel $hotel
     * @param Facility $facility
     */
    public function __construct(Uuid $id, Hotel $hotel, Facility $facility)
    {
        $this->id = $id;
        $this->hotel = $hotel;
        $this->facility = $facility;
    }

    /**
     * Get id.
     *
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get hotel.
     *
     * @return Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Get facility.
     *
     * @return Facility
     */
    public function getFacility()
    {
        return $this->facility;
    }
}
