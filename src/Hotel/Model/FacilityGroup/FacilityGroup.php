<?php

namespace App\Hotel\Model\FacilityGroup;

use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;

/**
 * Periods
 *
 * @ORM\Table(name="facility_group")
 * @ORM\Entity()
 */
class FacilityGroup
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_facility_group_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isShownHotel;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isShownRoom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="web_icon", referencedColumnName="id", nullable=true)
     */
    private ?File $webIcon = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="mobile_icon", referencedColumnName="id", nullable=true)
     */
    private ?File $mobileIcon = null;

    /**
     * FacilityGroup constructor.
     * @param Uuid $id
     * @param Name $name
     * @param bool $isActive
     * @param bool $isShownHotel
     * @param bool $isShownRoom
     * @param Sort $sort
     */
    public function __construct(
        Uuid $id,
        Name $name,
        bool $isActive,
        bool $isShownHotel,
        bool $isShownRoom,
        Sort $sort
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->isShownHotel = $isShownHotel;
        $this->isShownRoom = $isShownRoom;
        $this->sort = $sort;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function getIsShownHotel(): bool
    {
        return $this->isShownHotel;
    }

    /**
     * @return bool
     */
    public function getIsShownRoom(): bool
    {
        return $this->isShownRoom;
    }

    /**
     * @return Sort|null
     */
    public function getSort(): ?Sort
    {
        return $this->sort;
    }

    /**
     * @return File|null
     */
    public function getWebIcon(): ?File
    {
        return $this->webIcon;
    }

    /**
     * @return File|null
     */
    public function getMobileIcon(): ?File
    {
        return $this->mobileIcon;
    }

    /**
     * @param File|null $webIcon
     */
    public function setWebIcon(?File $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

    /**
     * @param File|null $mobileIcon
     */
    public function setMobileIcon(?File $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    public function update(
        ?Name $name,
        ?bool $isActive,
        ?bool $isShownHotel,
        ?bool $isShownRoom,
        ?File $mobIcon,
        ?File $webIcon
    ) {
        if ($name) {
            $this->name = $name;
        }

        if (!is_null($isActive)) {
            $this->isActive = $isActive;
        }

        if (!is_null($isShownHotel)) {
            $this->isShownHotel = $isShownHotel;
        }

        if (!is_null($isShownRoom)) {
            $this->isShownRoom = $isShownRoom;
        }

        if ($mobIcon) {
            $this->mobileIcon = $mobIcon;
        }

        if ($webIcon) {
            $this->webIcon = $webIcon;
        }
    }
}
