<?php

declare(strict_types=1);

namespace App\Hotel\Model\HotelAgeRange;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Hotel\Hotel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RoomMainPlaceVariant.
 * @ORM\Entity()
 * @ORM\Table(name="hotel_age_range")
 */
class HotelAgeRange
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_hotel_age_range_age_from")
     */
    private AgeFrom $ageFrom;

    /**
     * @ORM\Column(type="hotel_hotel_age_range_age_to")
     */
    private AgeTo $ageTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel", inversedBy="ageRange")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private Hotel $hotel;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType")
     */
    private ?Collection $extraPlaceTypes;

    /**
     * HotelAgeRange constructor.
     * @param Uuid $id
     * @param AgeFrom $ageFrom
     * @param AgeTo $ageTo
     * @param Hotel $hotel
     */
    public function __construct(Uuid $id, Hotel $hotel, AgeFrom $ageFrom, AgeTo $ageTo)
    {
        $this->id = $id;
        $this->hotel = $hotel;
        $this->ageFrom = $ageFrom;
        $this->ageTo = $ageTo;
        $this->extraPlaceTypes = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return AgeFrom
     */
    public function getAgeFrom(): AgeFrom
    {
        return $this->ageFrom;
    }

    /**
     * @return AgeTo
     */
    public function getAgeTo(): AgeTo
    {
        return $this->ageTo;
    }

    /**
     * @return array
     */
    public function getExtraPlaceTypes(): array
    {
        return $this->extraPlaceTypes->toArray();
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    public function update(
        ?Hotel $hotel,
        ?AgeFrom $ageFrom,
        ?AgeTo $ageTo
    ) {
        if ($hotel) {
            $this->hotel = $hotel;
        }

        if ($ageFrom) {
            $this->ageFrom = $ageFrom;
        }

        if ($ageTo) {
            $this->ageTo = $ageTo;
        }
    }
}


