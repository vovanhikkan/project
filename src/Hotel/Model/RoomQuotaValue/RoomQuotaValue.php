<?php

declare(strict_types=1);

namespace App\Hotel\Model\RoomQuotaValue;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RoomQuota\RoomQuota;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Budget.
 * @ORM\Entity()
 * @ORM\Table(name="room_quota_value")
 */
class RoomQuotaValue
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isStoppedStop;

    /**
     * @ORM\Column(type="amount")
     */
    private Amount $totalQuantity;

    /**
     * @ORM\Column(type="amount")
     */
    private Amount $holdedQuantity;

    /**
     * @ORM\Column(type="amount")
     */
    private Amount $orderedQuantity;

    /**
     * @ORM\Column(type="amount")
     */
    private Amount $canceledQuantity;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\RoomQuota\RoomQuota")
     * @ORM\JoinColumn(name="room_quota_id", referencedColumnName="id")
     */
    private RoomQuota $roomQuota;

    /**
     * Budget constructor.
     * @param Uuid $id
     * @param DateTimeImmutable $date
     */
    public function __construct(
        Uuid $id,
        DateTimeImmutable $date
    ) {
        $this->id = $id;
        $this->isStoppedStop = false;
        $this->totalQuantity = new Amount(0);
        $this->holdedQuantity = new Amount(0);
        $this->orderedQuantity = new Amount(0);
        $this->canceledQuantity = new Amount(0);
        $this->date = $date;
    }

    /**
     * @return Uuid
     */
    public function getId() : Uuid
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isStoppedStop(): bool
    {
        return $this->isStoppedStop;
    }

    /**
     * @return Amount
     */
    public function getTotalQuantity(): Amount
    {
        return $this->totalQuantity;
    }

    /**
     * @return Amount
     */
    public function getHoldedQuantity(): Amount
    {
        return $this->holdedQuantity;
    }

    /**
     * @return Amount
     */
    public function getOrderedQuantity(): Amount
    {
        return $this->orderedQuantity;
    }

    /**
     * @return Amount
     */
    public function getCanceledQuantity(): Amount
    {
        return $this->canceledQuantity;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return RoomQuota
     */
    public function getRoomQuota(): RoomQuota
    {
        return $this->roomQuota;
    }


    /**
     * @param bool|null $isStoppedStop
     * @param Amount|null $totalQuantity
     * @param Amount|null $holdedQuantity
     * @param Amount|null $orderedQuantity
     * @param Amount|null $canceledQuantity
     * @param DateTimeImmutable|null $date
     */
    public function update(
        ?bool $isStoppedStop,
        ?Amount $totalQuantity,
        ?Amount $holdedQuantity,
        ?Amount $orderedQuantity,
        ?Amount $canceledQuantity,
        ?DateTimeImmutable $date
    ) {
        if (!is_null($isStoppedStop)) {
            $this->isStoppedStop = $isStoppedStop;
        }

        if ($totalQuantity) {
            $this->totalQuantity = $totalQuantity;
        }

        if ($holdedQuantity) {
            $this->holdedQuantity = $holdedQuantity;
        }

        if ($orderedQuantity) {
            $this->orderedQuantity = $orderedQuantity;
        }

        if ($canceledQuantity) {
            $this->canceledQuantity = $canceledQuantity;
        }

        if ($date) {
            $this->date = $date;
        }
    }
}
