<?php


namespace App\Hotel\Model\Room;

use App\Application\ValueObject\IntegerValueObject;

/**
 * ZeroPlaceCount.
 */
final class ZeroPlaceCount extends IntegerValueObject
{
}