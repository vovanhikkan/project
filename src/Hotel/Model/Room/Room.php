<?php

declare(strict_types=1);

namespace App\Hotel\Model\Room;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\RoomInterval\RoomInterval;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\Hotel\Hotel;

/**
 * Hotel
 *
 * @ORM\Table(name="room")
 * @ORM\Entity()
 */
class Room
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id", nullable=true)
     */
    private Hotel $hotel;

    /**
     * @ORM\Column(type="hotel_room_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="hotel_room_description")
     */
    private Description $description;

    /**
     * @ORM\Column(type="hotel_room_area")
     */
    private Area $area;

    /**
     * @ORM\Column(type="hotel_room_main_place_count")
     */
    private MainPlaceCount $mainPlaceCount;

    /**
     * @ORM\Column(type="hotel_room_extra_place_count")
     */
    private ExtraPlaceCount $extraPlaceCount;

    /**
     * @ORM\Column(type="hotel_room_zero_place_count")
     */
    private ZeroPlaceCount $zeroPlaceCount;

    /**
     * @ORM\ManyToMany(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinTable(name="room_photo",
     *  joinColumns={@ORM\JoinColumn(name="room_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="photo_id", referencedColumnName="id")}
     * )
     */
    private Collection $photos;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\RoomVideo\RoomVideo", mappedBy="room")
     */
    private Collection $videos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\Facility\Facility")
     * @ORM\JoinTable(name="room_facility",
     *  joinColumns={@ORM\JoinColumn(name="room_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="facility_id", referencedColumnName="id")}
     * )
     */
    private Collection $facilities;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\RoomMainPlaceVariant\RoomMainPlaceVariant", mappedBy="room")
     */
    private Collection $roomMainPlaceVariant;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\RatePlan\RatePlan")
     * @ORM\JoinTable(name="rate_plan_room",
     *  joinColumns={@ORM\JoinColumn(name="room_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id")}
     * )
     */
    private Collection $ratePlans;


    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\RatePlan\RatePlan")
     * @ORM\JoinTable(name="rate_plan_room",
     *  joinColumns={@ORM\JoinColumn(name="room_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id")}
     * )
     */
    private Collection $bundles;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\RoomInterval\RoomInterval", mappedBy="room")
     */
    private Collection $intervals;

    /**
     * @ORM\ManyToMany(targetEntity="App\Content\Model\Tag\Tag", cascade={"persist"}), mappedBy="tags"
     * @ORM\JoinTable(
     *     name="room_tag",
     *     joinColumns={@ORM\JoinColumn(name="room_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")},
     * )
     */
    private Collection $tags;

    /**
     * Room constructor.
     * @param Uuid $id
     * @param Hotel $hotel
     * @param Name $name
     * @param Description $description
     * @param Area $area
     * @param MainPlaceCount $mainPlaceCount
     * @param ExtraPlaceCount $extraPlaceCount
     * @param ZeroPlaceCount $zeroPlaceCount
     */
    public function __construct(
        Uuid $id,
        Hotel $hotel,
        Name $name,
        Description $description,
        Area $area,
        MainPlaceCount $mainPlaceCount,
        ExtraPlaceCount $extraPlaceCount,
        ZeroPlaceCount $zeroPlaceCount
    ) {
        $this->id = $id;
        $this->hotel = $hotel;
        $this->name = $name;
        $this->description = $description;
        $this->area = $area;
        $this->mainPlaceCount = $mainPlaceCount;
        $this->extraPlaceCount = $extraPlaceCount;
        $this->zeroPlaceCount = $zeroPlaceCount;
        $this->photos     = new ArrayCollection();
        $this->videos     = new ArrayCollection();
        $this->facilities = new ArrayCollection();
        $this->ratePlans = new ArrayCollection();
        $this->intervals = new ArrayCollection();
        $this->roomMainPlaceVariant = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return Name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get description.
     *
     * @return Description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get area.
     *
     * @return Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Get mainPlaceCount.
     *
     * @return MainPlaceCount
     */
    public function getMainPlaceCount()
    {
        return $this->mainPlaceCount;
    }

    /**
     * Get extraPlaceCount.
     *
     * @return ExtraPlaceCount
     */
    public function getExtraPlaceCount()
    {
        return $this->extraPlaceCount;
    }

    /**
     * Get zeroPlaceCount.
     *
     * @return ZeroPlaceCount
     */
    public function getZeroPlaceCount()
    {
        return $this->zeroPlaceCount;
    }

    /**
     * Get hotel.
     *
     * @return Hotel|null
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Get photos.
     *
     * @return array
     */
    public function getPhotos()
    {
        return $this->photos->toArray();
    }

    /**
     * Get videos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * @return array
     */
    public function getFacilities() : array
    {
        return $this->facilities->toArray();
    }

    /**
     * @return array
     */
    public function getRoomMainPlaceVariant(): array
    {
        return $this->roomMainPlaceVariant->toArray();
    }

    /**
     * @return array
     */
    public function getIntervals(): array
    {
        return $this->intervals->toArray();
    }

    /**
     * @param array $dates
     * @return array
     */
    public function getIntervalsByDates(array $dates): array
    {
        return $this->intervals
            ->filter(function (RoomInterval $interval) use ($dates) {
                foreach ($dates as $date) {
                    if (!($interval->getStart() <= new DateTimeImmutable($date) &&
                        $interval->getEnd() >= new DateTimeImmutable($date))) {
                        return false;
                    }
                }
                return true;
            })
            ->toArray();
    }

    /**
     * @return array
     */
    public function getRatePlans(): array
    {
        return $this->ratePlans->toArray();
    }

    /**
     * @return array
     */
    public function getTags() : array
    {
        return $this->tags->toArray();
    }
}
