<?php


namespace App\Hotel\Model\Guest;

use App\Application\ValueObject\IntegerValueObject;

/**
 * Age.
 */
final class Age extends IntegerValueObject
{
}