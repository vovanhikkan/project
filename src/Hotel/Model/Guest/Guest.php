<?php

declare(strict_types=1);

namespace App\Hotel\Model\Guest;

use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\Uuid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Guest.
 * @ORM\Entity()
 * @ORM\Table(name="guest")
 */
class Guest
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="first_name")
     */
    private FirstName $firstName;

    /**
     * @ORM\Column(type="last_name")
     */
    private LastName $lastName;

    /**
     * @ORM\Column(type="hotel_guest_age")
     */
    private Age $age;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $mainGuest;

    /**
     * @ORM\ManyToMany(targetEntity="App\Auth\Model\User\User", inversedBy="guest")
     * @ORM\JoinTable(name="user_guest",
     *  joinColumns={@ORM\JoinColumn(name="guest_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private Collection $users;

    /**
     * @ORM\ManyToMany(targetEntity="App\Order\Model\Order\Order", inversedBy="guest")
     * @ORM\JoinTable(name="order_guest",
     *  joinColumns={@ORM\JoinColumn(name="guest_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")}
     * )
     */
    private Collection $orders;


    /**
     * Guest constructor.
     * @param Uuid $id
     * @param FirstName $firstName
     * @param LastName $lastName
     * @param Age $age
     * @param bool $mainGuest
     */
    public function __construct(
        Uuid $id,
        FirstName
        $firstName,
        LastName $lastName,
        Age $age,
        bool $mainGuest
    ) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->age = $age;
        $this->users = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->mainGuest = $mainGuest;
    }

    /**
     * @return Uuid
     */
    public function getId() : Uuid
    {
        return $this->id;
    }

    /**
     * @return FirstName
     */
    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    /**
     * @return LastName
     */
    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    /**
     * @return array
     */
    public function getUsers() : array
    {
        return $this->users->toArray();
    }

    /**
     * @return array
     */
    public function getOrders() : array
    {
        return $this->orders->toArray();
    }

    /**
     * @return Age
     */
    public function getAge(): Age
    {
        return $this->age;
    }

    /**
     * @return bool
     */
    public function isMainGuest(): bool
    {
        return $this->mainGuest;
    }

    /**
     * @param FirstName|null $firstName
     * @param LastName|null $lastName
     * @param Age|null $age
     * @param bool|null $mainGuest
     */
    public function update(
        ?FirstName $firstName,
        ?LastName $lastName,
        ?Age $age,
        ?bool $mainGuest
    ) {
        if (!is_null($firstName)) {
            $this->firstName = $firstName;
        }

        if (!is_null($lastName)) {
            $this->lastName = $lastName;
        }

        if (!is_null($age)) {
            $this->age = $age;
        }

        if (!is_null($mainGuest)) {
            $this->mainGuest = $mainGuest;
        }
    }
}
