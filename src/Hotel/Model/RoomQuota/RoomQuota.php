<?php

declare(strict_types=1);

namespace App\Hotel\Model\RoomQuota;

use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use App\Hotel\Model\RoomQuotaValue\RoomQuotaValue;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;

/**
 * Budget.
 * @ORM\Entity()
 * @ORM\Table(name="room_quota")
 */
class RoomQuota
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_room_quota_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\RoomQuotaValue\RoomQuotaValue", mappedBy="roomQuota")
     */
    private Collection $quotaValues;


    /**
     * Budget constructor.
     * @param Uuid $id
     * @param Name $name
     * @param bool $isActive
     * @param Sort $sort
     */
    public function __construct(Uuid $id, Name $name, bool $isActive, Sort $sort)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->sort = $sort;
    }

    /**
     * @return Uuid
     */
    public function getId() : Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName() : Name
    {
        return $this->name;
    }

    /**
     * @return Sort
     */
    public function getSort() : Sort
    {
        return $this->sort;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return array
     */
    public function getQuotaValues() : array
    {
        return $this->quotaValues->toArray();
    }

    /**
     * @param DateTimeImmutable $date
     * @return array
     */
    public function getQuotaValuesByDate(DateTimeImmutable $date): array
    {
        return $this->quotaValues->filter(function (RoomQuotaValue $roomQuotaValue) use ($date) {
            return $date->diff($roomQuotaValue->getDate())->days === 0;
        })->toArray();
    }

    /**
     * @param Name|null $name
     * @param bool|null $isActive
     * @param Sort|null $sort
     */
    public function update(
        ?Name $name,
        ?bool $isActive,
        ?Sort $sort
    ) {
        if (!is_null($name)) {
            $this->name = $name;
        }

        if (!is_null($isActive)) {
            $this->isActive = $isActive;
        }

        if (!is_null($sort)) {
            $this->sort = $sort;
        }
    }
}
