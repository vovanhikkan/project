<?php

namespace App\Hotel\Model\Facility;

use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\FacilityGroup\FacilityGroup;

/**
 * Facility
 *
 * @ORM\Table(name="facility")
 * @ORM\Entity()
 */
class Facility
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_facility_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\FacilityGroup\FacilityGroup")
     * @ORM\JoinColumn(name="facility_group_id", referencedColumnName="id")
     */
    private FacilityGroup $facilityGroup;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="web_icon", referencedColumnName="id")
     */
    private ?File $webIcon = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="mobile_icon", referencedColumnName="id", nullable=true)
     */
    private ?File $mobileIcon = null;

    /**
     * @ORM\Column(name="is_shown_in_room_list", type="boolean")
     */
    private bool $isShownInRoomList;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isShownInSearch = null;

    /**
     * Facility constructor.
     * @param Uuid $id
     * @param Name $name
     * @param bool $isActive
     * @param FacilityGroup $facilityGroup
     * @param Sort $sort
     * @param bool $isShownInRoomList
     */
    public function __construct(
        Uuid $id,
        Name $name,
        bool $isActive,
        FacilityGroup $facilityGroup,
        Sort $sort,
        bool $isShownInRoomList
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->facilityGroup = $facilityGroup;
        $this->sort = $sort;
        $this->isShownInRoomList = $isShownInRoomList;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return FacilityGroup
     */
    public function getFacilityGroup(): FacilityGroup
    {
        return $this->facilityGroup;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return File|null
     */
    public function getWebIcon(): ?File
    {
        return $this->webIcon;
    }

    /**
     * @return File|null
     */
    public function getMobileIcon(): ?File
    {
        return $this->mobileIcon;
    }

    /**
     * @return bool
     */
    public function isShownInRoomList(): bool
    {
        return $this->isShownInRoomList;
    }

    /**
     * @return bool|null
     */
    public function getIsShownInSearch(): ?bool
    {
        return $this->isShownInSearch;
    }

    /**
     * @param bool|null $isShownInSearch
     */
    public function setIsShownInSearch(?bool $isShownInSearch): void
    {
        $this->isShownInSearch = $isShownInSearch;
    }

    /**
     * @param File|null $webIcon
     */
    public function setWebIcon(?File $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

    /**
     * @param File|null $mobileIcon
     */
    public function setMobileIcon(?File $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    public function update(
        ?Name $name,
        ?FacilityGroup $group,
        ?bool $isShownInRoomList,
        ?bool $isShownInSearch,
        ?File $mobIcon,
        ?File $webIcon
    ) {
        if ($name) {
            $this->name = $name;
        }

        if ($group) {
            $this->facilityGroup = $group;
        }

        if ($isShownInRoomList) {
            $this->isShownInRoomList = $isShownInRoomList;
        }

        if ($isShownInSearch !== null) {
            $this->isShownInSearch = $isShownInSearch;
        }

        if ($mobIcon) {
            $this->mobileIcon = $mobIcon;
        }

        if ($webIcon) {
            $this->webIcon = $webIcon;
        }
    }
}
