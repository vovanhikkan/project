<?php

declare(strict_types=1);

namespace App\Hotel\Model\RatePlanPrice;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlan\RatePlan;
use App\Hotel\Model\Room\Room;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * RoomMainPlaceVariant.
 * @ORM\Entity()
 * @ORM\Table(name="rate_plan_price")
 */
class RatePlanPrice
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\RatePlan\RatePlan", inversedBy="ratePlan")
     * @ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id")
     */
    private RatePlan $ratePlan;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room", inversedBy="room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    private Room $room;

    /**
     * @ORM\Column(type="amount")
     */
    private ?Amount $mainPlaceValue = null;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $date;
    
    /**
     * @ORM\Column(type="amount")
     */
    private ?Amount $value = null;

    /**
     * RatePlanPrice constructor.
     * @param Uuid $id
     * @param RatePlan $ratePlan
     * @param Room $room
     * @param DateTimeImmutable $date
     */
    public function __construct(
        Uuid $id,
        RatePlan $ratePlan,
        Room $room,
        DateTimeImmutable $date
    ) {
        $this->id = $id;
        $this->ratePlan = $ratePlan;
        $this->room = $room;
        $this->date = $date;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return RatePlan
     */
    public function getRatePlan(): RatePlan
    {
        return $this->ratePlan;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return Amount|null
     */
    public function getMainPlaceValue(): ?Amount
    {
        return $this->mainPlaceValue;
    }

    /**
     * @return Amount|null
     */
    public function getValue(): ?Amount
    {
        return $this->value;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @param Amount|null $mainPlaceValue
     */
    public function setMainPlaceValue(?Amount $mainPlaceValue): void
    {
        $this->mainPlaceValue = $mainPlaceValue;
    }

    /**
     * @param Amount|null $value
     */
    public function setValue(?Amount $value): void
    {
        $this->value = $value;
    }

    public function update(
        ?RatePlan $ratePlan,
        ?Room $room,
        ?DateTimeImmutable $date,
        ?Amount $mainPlaceValue,
        ?Amount $value
    ) {
        if ($ratePlan) {
            $this->ratePlan = $ratePlan;
        }

        if ($room) {
            $this->room = $room;
        }

        if ($date) {
            $this->date = $date;
        }

        if ($mainPlaceValue) {
            $this->mainPlaceValue = $mainPlaceValue;
        }

        if ($value) {
            $this->value = $value;
        }
    }
}
