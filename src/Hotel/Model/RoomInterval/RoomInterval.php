<?php

declare(strict_types=1);

namespace App\Hotel\Model\RoomInterval;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Room\Room;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\Hotel\Hotel;

/**
 * RoomInterval
 *
 * @ORM\Table(name="room_interval")
 * @ORM\Entity()
 */
class RoomInterval
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="uniq_id", type="string")
     */
    private string $id;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $start;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $end;

    /**
     * @ORM\Column(type="string")
     */
    private string $ratePlan;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room", inversedBy="intervals")
     */
    private Room $room;

    /**
     * @return DateTimeImmutable
     */
    public function getStart(): DateTimeImmutable
    {
        return $this->start;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getEnd(): DateTimeImmutable
    {
        return $this->end;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return string
     */
    public function getRatePlan(): string
    {
        return $this->ratePlan;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
