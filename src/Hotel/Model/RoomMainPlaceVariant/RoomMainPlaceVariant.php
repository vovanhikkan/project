<?php

declare(strict_types=1);

namespace App\Hotel\Model\RoomMainPlaceVariant;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Room\Room;
use Doctrine\ORM\Mapping as ORM;

/**
 * RoomMainPlaceVariant.
 * @ORM\Entity()
 * @ORM\Table(name="room_main_place_variant")
 */
class RoomMainPlaceVariant
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_room_main_place_variant_value")
     */
    private Value $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room", inversedBy="roomMainPlaceVariant")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    private Room $room;

    /**
     * RoomMainPlaceVariant constructor.
     * @param Uuid $id
     * @param Value $value
     * @param Room $room
     */
    public function __construct(Uuid $id, Value $value, Room $room)
    {
        $this->id = $id;
        $this->value = $value;
        $this->room = $room;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Value
     */
    public function getValue(): Value
    {
        return $this->value;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }
}


