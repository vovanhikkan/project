<?php

declare(strict_types=1);

namespace App\Hotel\Model\RatePlan;

use App\Application\ValueObject\Uuid;
use App\Bundle\Model\Bundle\Bundle;
use App\Hotel\Model\FoodType\FoodType;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;
use App\Hotel\Model\RatePlanPrice\RatePlanPrice;
use App\Hotel\Model\TaxType\TaxType;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RoomMainPlaceVariant.
 * @ORM\Entity()
 * @ORM\Table(name="rate_plan")
 */
class RatePlan
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel", inversedBy="ratePlans")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private Hotel $hotel;

    /**
     * @ORM\Column(type="hotel_rate_plan_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="hotel_rate_plan_description")
     */
    private Description $description;

    /**
     * @ORM\Column(type="hotel_rate_plan_extra_condition")
     */
    private ?ExtraCondition $extraCondition = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\RatePlanGroup\RatePlanGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private RatePlanGroup $ratePlanGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\FoodType\FoodType")
     * @ORM\JoinColumn(name="food_type_id", referencedColumnName="id")
     */
    private FoodType $foodType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\TaxType\TaxType")
     * @ORM\JoinColumn(name="tax_type_id", referencedColumnName="id")
     */
    private TaxType $taxType;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $activeFrom;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $activeTo;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $shownFrom;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private DateTimeImmutable $shownTo;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isShownWithPackets;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\RatePlanPrice\RatePlanPrice", mappedBy="ratePlan")
     */
    private Collection $ratePlanPrices;

    /**
     * @ORM\ManyToMany(targetEntity="App\Bundle\Model\Bundle\Bundle")
     * @ORM\JoinTable(name="bundle_rate_plan",
     *  joinColumns={@ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="bundle_id", referencedColumnName="id")}
     * )
     */
    private Collection $bundles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\RoomQuota\RoomQuota"), mappedBy="retaPlans",
     * @ORM\JoinTable(
     *     name="room_quota_rate_plan",
     *     joinColumns={@ORM\JoinColumn(name="rate_plan_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="quota_id", referencedColumnName="id")}
     * )
     */
    private Collection $roomQuotas;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private ?DateTimeImmutable $canBeCanceledDate = null;


    /**
     * RatePlan constructor.
     * @param Uuid $id
     * @param Hotel $hotel
     * @param Name $name
     * @param Description $description
     * @param RatePlanGroup $ratePlanGroup
     * @param FoodType $foodType
     * @param TaxType $taxType
     * @param bool $isActive
     * @param DateTimeImmutable $activeFrom
     * @param DateTimeImmutable $activeTo
     * @param DateTimeImmutable $shownFrom
     * @param DateTimeImmutable $shownTo
     */
    public function __construct(
        Uuid $id,
        Hotel $hotel,
        Name $name,
        Description $description,
        RatePlanGroup $ratePlanGroup,
        FoodType $foodType,
        TaxType $taxType,
        bool $isActive,
        DateTimeImmutable $activeFrom,
        DateTimeImmutable $activeTo,
        DateTimeImmutable $shownFrom,
        DateTimeImmutable $shownTo
    ) {
        $this->id = $id;
        $this->hotel = $hotel;
        $this->name = $name;
        $this->description = $description;
        $this->ratePlanGroup = $ratePlanGroup;
        $this->foodType = $foodType;
        $this->taxType = $taxType;
        $this->isActive = $isActive;
        $this->activeFrom = $activeFrom;
        $this->activeTo = $activeTo;
        $this->shownFrom = $shownFrom;
        $this->shownTo = $shownTo;
        $this->ratePlanPrices = new ArrayCollection();
        $this->bundles = new ArrayCollection();
        $this->roomQuotas = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Description
     */
    public function getDescription(): Description
    {
        return $this->description;
    }

    /**
     * @return RatePlanGroup
     */
    public function getRatePlanGroup(): RatePlanGroup
    {
        return $this->ratePlanGroup;
    }

    /**
     * @return FoodType
     */
    public function getFoodType(): FoodType
    {
        return $this->foodType;
    }

    /**
     * @return TaxType
     */
    public function getTaxType(): TaxType
    {
        return $this->taxType;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function addRatePlanPrice(RatePlanPrice $ratePlanPrice): void
    {
        $this->ratePlanPrices->add($ratePlanPrice);
    }

    /**
     * @return DateTimeImmutable
     */
    public function getActiveFrom(): DateTimeImmutable
    {
        return $this->activeFrom;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getActiveTo(): DateTimeImmutable
    {
        return $this->activeTo;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getShownFrom(): DateTimeImmutable
    {
        return $this->shownFrom;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getShownTo(): DateTimeImmutable
    {
        return $this->shownTo;
    }

    /**
     * @return bool|null
     */
    public function getIsShownWithPackets(): ?bool
    {
        return $this->isShownWithPackets;
    }

    /**
     * @param bool|null $isShownWithPackets
     */
    public function setIsShownWithPackets(?bool $isShownWithPackets): void
    {
        $this->isShownWithPackets = $isShownWithPackets;
    }

    /**
     * @return array
     */
    public function getBundles(): array
    {
        return $this->bundles->toArray();
    }

    public function addBundle(Bundle $bundle): void
    {
        $this->bundles->add($bundle);
    }

    /**
     * @return array
     */
    public function getRatePlanPrices(): array
    {
        return $this->ratePlanPrices->toArray();
    }

    /**
     * @return array
     */
    public function getRoomQuotas() : array
    {
        return $this->roomQuotas->toArray();
    }

    /**
     * @return int|null
     */
    public function getMinPrice(): ?int
    {
        $ratePlans = $this->ratePlanPrices->toArray();
        if (!count($ratePlans)) {
            return null;
        }
        $min = $ratePlans[0]->getMainPlaceValue();
        foreach ($ratePlans as $ratePlanPrice) {
            if ($min < $ratePlanPrice->getMainPlaceValue()->getValue()) {
                $min = $ratePlanPrice->getMainPlaceValue()->getValue();
            }
        }

        return $min;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getCanBeCanceledDate(): ?DateTimeImmutable
    {
        return $this->canBeCanceledDate;
    }

    /**
     * @param DateTimeImmutable|null $canBeCanceledDate
     */
    public function setCanBeCanceledDate(?DateTimeImmutable $canBeCanceledDate): void
    {
        $this->canBeCanceledDate = $canBeCanceledDate;
    }

    /**
     * @return ExtraCondition|null
     */
    public function getExtraCondition(): ?ExtraCondition
    {
        return $this->extraCondition;
    }

    /**
     * @param ExtraCondition|null $extraCondition
     */
    public function setExtraCondition(?ExtraCondition $extraCondition): void
    {
        $this->extraCondition = $extraCondition;
    }


    public function update(
        ?Hotel $hotel,
        ?Name $name,
        ?Description $description,
        ?RatePlanGroup $ratePlanGroup,
        ?FoodType $foodType,
        ?TaxType $taxType,
        ?bool $isActive,
        ?DateTimeImmutable $activeFrom,
        ?DateTimeImmutable $activeTo,
        ?DateTimeImmutable $shownFrom,
        ?DateTimeImmutable $shownTo,
        ?bool $isShownWithPackets,
        ?DateTimeImmutable $canBeCanceledDate,
        ?ExtraCondition $extraCondition
    ) {
        if ($hotel) {
            $this->hotel = $hotel;
        }

        if ($name) {
            $this->name = $name;
        }

        if ($description) {
            $this->description = $description;
        }

        if ($extraCondition) {
            $this->extraCondition = $extraCondition;
        }

        if ($ratePlanGroup) {
            $this->ratePlanGroup = $ratePlanGroup;
        }

        if ($foodType) {
            $this->foodType = $foodType;
        }

        if ($taxType) {
            $this->taxType = $taxType;
        }

        if (!is_null($isActive)) {
            $this->isActive = $isActive;
        }

        if ($activeFrom) {
            $this->activeFrom = $activeFrom;
        }

        if ($activeTo) {
            $this->activeTo = $activeTo;
        }

        if ($shownFrom) {
            $this->shownFrom = $shownFrom;
        }

        if ($shownTo) {
            $this->shownTo = $shownTo;
        }

        if (!is_null($isShownWithPackets)) {
            $this->isShownWithPackets = $isShownWithPackets;
        }

        if ($canBeCanceledDate) {
            $this->canBeCanceledDate = $canBeCanceledDate;
        }
    }
}
