<?php


namespace App\Hotel\Model\RatePlan;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Class ExtraCondition
 * @package App\Hotel\Model\RatePlan
 */
final class ExtraCondition extends ArrayValueObject
{
}