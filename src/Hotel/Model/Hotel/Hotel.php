<?php

declare(strict_types=1);

namespace App\Hotel\Model\Hotel;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Content\Model\Tag\Tag;
use App\Hotel\Model\Brand\Brand;
use App\Hotel\Model\HotelType\HotelType;
use App\Location\Model\Location\Location;
use App\Place\Model\Place\Place;
use App\Storage\Model\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Hotel
 *
 * @ORM\Table(name="hotel")
 * @ORM\Entity()
 */
class Hotel
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_hotel_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="hotel_hotel_description")
     */
    private ?Description $description = null;

    /**
     * @ORM\Column(type="hotel_hotel_star_count")
     */
    private StarCount $starCount;

    /**
     * @ORM\Column(type="hotel_hotel_address")
     */
    private Address $address;

    /**
     * @ORM\Column(type="hotel_hotel_links")
     */
    private ?Links $links = null;

    /**
     * @ORM\Column(type="hotel_hotel_lat")
     */
    private ?Lat $lat = null;

    /**
     * @ORM\Column(type="hotel_hotel_lon")
     */
    private ?Lon $lon = null;

    /**
     * @ORM\Column(type="hotel_hotel_check_in_time")
     */
    private ?CheckInTime $checkInTime = null;

    /**
     * @ORM\Column(type="hotel_hotel_check_out_time")
     */
    private ?CheckOutTime $checkOutTime = null;

    /**
     * @ORM\Column(type="hotel_hotel_cancel_prepayment")
     */
    private ?CancelPrepayment $cancelPrepayment = null;

    /**
     * @ORM\Column(type="hotel_hotel_extra_beds")
     */
    private ?ExtraBeds $extraBeds = null;

    /**
     * @ORM\Column(type="hotel_hotel_pets")
     */
    private ?Pets $pets = null;

    /**
     * @ORM\Column(type="hotel_hotel_extra_info")
     */
    private ?ExtraInfo $extraInfo = null;

    /**
     * @ORM\Column(type="hotel_hotel_payments")
     */
    private ?Payments $payments = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Location\Model\Location\Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=true)
     */
    private ?Location $location = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Brand\Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hotel_brand_id", referencedColumnName="id", nullable=true)
     * })
     */
    private Brand $brand;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="logo_svg", referencedColumnName="id", nullable=true)
     */
    private ?File $logoSvg = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="logo", referencedColumnName="id", nullable=true)
     */
    private ?File $logo = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\HotelPhoto\HotelPhoto", mappedBy="hotel")
     */
    private ?Collection $photos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Storage\Model\Video\Video")
     * @ORM\JoinTable(
     *     name="hotel_video",
     *     joinColumns={@ORM\JoinColumn(name="hotel_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id")}
     * )
     */
    private Collection $videos;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\Room\Room", mappedBy="hotel")
     */
    private ?Collection $rooms;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\Facility\Facility")
     * @ORM\JoinTable(
     *     name="hotel_facility",
     *     joinColumns={@ORM\JoinColumn(name="hotel_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="facility_id", referencedColumnName="id")}
     * )
     */
    private Collection $facilities;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\HotelType\HotelType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=true)
     */
    private HotelType $type;
    

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType", mappedBy="hotel")
     */
    private Collection $hotelExtraPlaceType;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\HotelAgeRange\HotelAgeRange", mappedBy="hotel")
     */
    private Collection $ageRange;

    /**
     * @ORM\OneToMany(targetEntity="App\Hotel\Model\RatePlan\RatePlan", mappedBy="hotel")
     */
    private Collection $ratePlans;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\BedType\BedType"), mappedBy="bedTypes",
     * @ORM\JoinTable(
     *     name="hotel_bed_type",
     *     joinColumns={@ORM\JoinColumn(name="hotel_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="bed_type_id", referencedColumnName="id")}
     * )
     */
    private Collection $bedTypes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Content\Model\Tag\Tag", cascade={"persist"}), mappedBy="tags"
     * @ORM\JoinTable(
     *     name="hotel_tag",
     *     joinColumns={@ORM\JoinColumn(name="hotel_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")},
     * )
     */
    private Collection $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Review\Model\Review\Review", mappedBy="hotel")
     */
    private Collection $reviews;

    /**
     * @ORM\ManyToMany(targetEntity="App\Place\Model\Place\Place", cascade={"persist"})
     * @ORM\JoinTable(
     *     name="hotel_place",
     *     joinColumns={@ORM\JoinColumn(name="hotel_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="place_id", referencedColumnName="id")}
     * )
     */
    private Collection $places;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="hotel_photo", referencedColumnName="id")
     */
    private ?File $hotelPhoto = null;

    /**
     * @ORM\Column(type="sort")
     */
    private ?Sort $sort = null;


    /**
     * Hotel constructor.
     * @param Uuid $id
     * @param Name $name
     * @param StarCount $starCount
     * @param Address $address
     * @param HotelType $type
     */
    public function __construct(
        Uuid $id,
        Name $name,
        StarCount $starCount,
        Address $address,
        HotelType $type
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->starCount = $starCount;
        $this->address = $address;
        $this->photos = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->rooms = new ArrayCollection();
        $this->facilities = new ArrayCollection();
        $this->hotelExtraPlaceType = new ArrayCollection();
        $this->ageRange = new ArrayCollection();
        $this->ratePlans = new ArrayCollection();
        $this->bedTypes = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->places = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->type = $type;
        
        //$this->rating = new Rating(['1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0]);
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags->toArray();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Description|null
     */
    public function getDescription(): ?Description
    {
        return $this->description;
    }

    /**
     * @param Description|null $description
     */
    public function setDescription(?Description $description): void
    {
        $this->description = $description;
    }

    /**
     * @return StarCount
     */
    public function getStarCount(): StarCount
    {
        return $this->starCount;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return Links
     */
    public function getLinks(): ?Links
    {
        return $this->links;
    }

    /**
     * @return Lat|null
     */
    public function getLat(): ?Lat
    {
        return $this->lat;
    }

    /**
     * @return Lon|null
     */
    public function getLon(): ?Lon
    {
        return $this->lon;
    }

    /**
     * @return Location|null
     */
    public function getLocation(): ?Location
    {
        return $this->location;
    }

    /**
     * @return File|null
     */
    public function getHotelPhoto(): ?File
    {
        return $this->hotelPhoto;
    }

    /**
     * @return File|null
     */
    public function getLogoSvg(): ?File
    {
        return $this->logoSvg;
    }

    /**
     * @return File|null
     */
    public function getLogo(): ?File
    {
        return $this->logo;
    }

    /**
     * @return Collection
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    /**
     * @return array
     */
    public function getVideos(): array
    {
        return $this->videos->toArray();
    }

    /**
     * @return array
     */
    public function getRooms(): array
    {
        return $this->rooms->toArray();
    }

    /**
     * @return array
     */
    public function getBedTypes() : array
    {
        return $this->bedTypes->toArray();
    }

    /**
     * @return array
     */
    public function getFacilities(): array
    {
        return $this->facilities->toArray();
    }

    /**
     * @return HotelType
     */
    public function getType(): HotelType
    {
        return $this->type;
    }
    

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return CheckInTime|null
     */
    public function getCheckInTime(): ?CheckInTime
    {
        return $this->checkInTime;
    }

    /**
     * @return CheckOutTime|null
     */
    public function getCheckOutTime(): ?CheckOutTime
    {
        return $this->checkOutTime;
    }

    /**
     * @return CancelPrepayment
     */
    public function getCancelPrepayment() : ?CancelPrepayment
    {
        return $this->cancelPrepayment;
    }

    /**
     * @return ExtraBeds|null
     */
    public function getExtraBeds() : ?ExtraBeds
    {
        return $this->extraBeds;
    }

    /**
     * @return Pets|null
     */
    public function getPets() : ?Pets
    {
        return $this->pets;
    }

    /**
     * @return ExtraInfo|null
     */
    public function getExtraInfo() : ?ExtraInfo
    {
        return $this->extraInfo;
    }

    /**
     * @return Payments|null
     */
    public function getPayments() : ?Payments
    {
        return $this->payments;
    }

    /**
     * @return array
     */
    public function getReviews() : array
    {
        return $this->reviews->toArray();
    }

    /**
     * @param Lat|null $lat
     */
    public function setLat(?Lat $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @param Lon|null $lon
     */
    public function setLon(?Lon $lon): void
    {
        $this->lon = $lon;
    }

    /**
     * @param Location|null $location
     */
    public function setLocation(?Location $location): void
    {
        $this->location = $location;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand(Brand $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @param File|null $logoSvg
     */
    public function setLogoSvg(?File $logoSvg): void
    {
        $this->logoSvg = $logoSvg;
    }

    /**
     * @param File|null $logo
     */
    public function setLogo(?File $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @param Links|null $links
     */
    public function setLinks(?Links $links): void
    {
        $this->links = $links;
    }

    /**
     * @param HotelType $type
     */
    public function setType(HotelType $type): void
    {
        $this->type = $type;
    }

    /**
     * @param CheckInTime|null $checkInTime
     */
    public function setCheckInTime(?CheckInTime $checkInTime): void
    {
        $this->checkInTime = $checkInTime;
    }

    /**
     * @param CheckOutTime|null $checkOutTime
     */
    public function setCheckOutTime(?CheckOutTime $checkOutTime): void
    {
        $this->checkOutTime = $checkOutTime;
    }

    /**
     * @param CancelPrepayment|null $cancelPrepayment
     */
    public function setCancelPrepayment(?CancelPrepayment $cancelPrepayment) : void
    {
        $this->cancelPrepayment = $cancelPrepayment;
    }

    /**
     * @param ExtraBeds|null $extraBeds
     */
    public function setExtraBeds(?ExtraBeds $extraBeds) : void
    {
        $this->extraBeds = $extraBeds;
    }

    /**
     * @param Pets|null $pets
     */
    public function setPets(?Pets $pets) : void
    {
        $this->pets = $pets;
    }

    /**
     * @param ExtraInfo|null $extraInfo
     */
    public function setExtraInfo(?ExtraInfo $extraInfo) : void
    {
        $this->extraInfo = $extraInfo;
    }

    /**
     * @param Payments|null $payments
     */
    public function setPayments(?Payments $payments) : void
    {
        $this->payments = $payments;
    }

    /**
     * @param File|null $hotelPhoto
     */
    public function setHotelPhoto(?File $hotelPhoto): void
    {
        $this->hotelPhoto = $hotelPhoto;
    }


    public function update(
        ?Name $name,
        ?Description $description,
        ?StarCount $starCount,
        ?Location $location,
        ?Address $address,
        ?File $logo,
        ?File $logoSvg,
        ?Links $links,
        ?Lat $lat,
        ?Lon $lon,
        ?Brand $brand,
        ?HotelType $hotelType,
        ?CheckInTime $checkInTime,
        ?CheckOutTime $checkOutTime,
        ?CancelPrepayment $cancelPrepayment,
        ?ExtraBeds $extraBeds,
        ?Pets $pets,
        ?ExtraInfo $extraInfo,
        ?Payments $payments,
        ?File $hotelPhoto
    ) {
        if ($name) {
            $this->name = $name;
        }

        if ($description) {
            $this->description = $description;
        }

        if ($starCount) {
            $this->starCount = $starCount;
        }

        if ($location) {
            $this->location = $location;
        }

        if ($address) {
            $this->address = $address;
        }

        if ($logo) {
            $this->logo = $logo;
        }

        if ($logoSvg) {
            $this->logoSvg = $logoSvg;
        }

        if ($links) {
            $this->links = $links;
        }

        if ($lat) {
            $this->lat = $lat;
        }

        if ($lat) {
            $this->lat = $lat;
        }

        if ($lon) {
            $this->lon = $lon;
        }

        if ($brand) {
            $this->brand = $brand;
        }

        if ($hotelType) {
            $this->hotelType = $hotelType;
        }

        if ($checkInTime) {
            $this->checkInTime = $checkInTime;
        }

        if ($checkOutTime) {
            $this->checkOutTime = $checkOutTime;
        }

        if ($cancelPrepayment) {
            $this->cancelPrepayment = $cancelPrepayment;
        }

        if ($extraBeds) {
            $this->extraBeds = $extraBeds;
        }

        if ($pets) {
            $this->pets = $pets;
        }

        if ($extraInfo) {
            $this->extraInfo = $extraInfo;
        }

        if ($payments) {
            $this->payments = $payments;
        }

        if ($hotelPhoto) {
            $this->hotelPhoto = $hotelPhoto;
        }
    }
    
    

    public function addTag(Tag $tag)
    {
        $this->tags->add($tag);
    }

    /**
     * @return array
     */
    public function getPlaces(): array
    {
        return $this->places->toArray();
    }

    /**
     * @param Place $place
     */
    public function addPlace(Place $place): void
    {
        $this->places->add($place);
    }

    /**
     * @return array
     */
    public function getRatePlans(): array
    {
        return $this->ratePlans->toArray();
    }

    /**
     * @return Sort|null
     */
    public function getSort(): ?Sort
    {
        return $this->sort;
    }

    /**
     * @param Sort|null $sort
     */
    public function setSort(?Sort $sort): void
    {
        $this->sort = $sort;
    }
}
