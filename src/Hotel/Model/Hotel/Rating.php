<?php

declare(strict_types=1);

namespace App\Hotel\Model\Hotel;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Rating.
 */
final class Rating extends ArrayValueObject
{
}
