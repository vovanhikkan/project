<?php

declare(strict_types=1);

namespace App\Hotel\Model\RatePlanGroup;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * RatePlanGroup.
 * @ORM\Entity()
 * @ORM\Table(name="rate_plan_group")
 */
class RatePlanGroup
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_rate_plan_group_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * RatePlanGroup constructor.
     * @param Uuid $id
     * @param Name $name
     * @param Sort $sort
     */
    public function __construct(
        Uuid $id,
        Name $name,
        Sort $sort
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->sort = $sort;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @param Name|null $name
     */
    public function update(
        ?Name $name
    ) {
        if ($name) {
            $this->name = $name;
        }
    }
}
