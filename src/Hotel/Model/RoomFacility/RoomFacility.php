<?php

namespace App\Hotel\Model\RoomFacility;

use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\Room\Room;
use App\Hotel\Model\Facility\Facility;
use App\Application\ValueObject\Uuid;

// todo: refactor
use App\Entity\Storage as Storage;

/**
 * RoomFacility
 *
 * @ORM\Table(name="room_facility", indexes={@ORM\Index(name="facility_id", columns={"facility_id"}), @ORM\Index(name="room_id", columns={"room_id"})})
 * @ORM\Entity()
 */
class RoomFacility
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    private Room $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Facility\Facility")
     * @ORM\JoinColumn(name="facility_id", referencedColumnName="id")
     */
    private Facility $facility;

    /**
     * RoomFacility constructor.
     * @param Uuid $id
     * @param Room $room
     * @param Facility $facility
     */
    public function __construct(Uuid $id, Room $room, Facility $facility)
    {
        $this->id = $id;
        $this->room = $room;
        $this->facility = $facility;
    }


    /**
     * Get id.
     *
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get room.
     *
     * @return Room|null
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Get facility.
     *
     * @return Facility|null
     */
    public function getFacility()
    {
        return $this->facility;
    }

}
