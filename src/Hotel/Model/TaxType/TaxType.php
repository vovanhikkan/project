<?php

declare(strict_types=1);

namespace App\Hotel\Model\TaxType;

use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * TaxType.
 * @ORM\Entity()
 * @ORM\Table(name="tax_type")
 */
class TaxType
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_tax_type_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="hotel_tax_type_value")
     */
    private Value $value;

    /**
     * TaxType constructor.
     * @param Uuid $id
     * @param Name $name
     * @param Value $value
     */
    public function __construct(Uuid $id, Name $name, Value $value)
    {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Value
     */
    public function getValue(): Value
    {
        return $this->value;
    }

    /**
     * @param Name $name
     */
    public function setName(Name $name): void
    {
        $this->name = $name;
    }

    /**
     * @param Value $value
     */
    public function setValue(Value $value): void
    {
        $this->value = $value;
    }

    /**
     * @param Name|null $name
     * @param Value|null $value
     */
    public function update(
        ?Name $name,
        ?Value $value
    ) {
        if ($name) {
            $this->name = $name;
        }

        if ($value) {
            $this->value = $value;
        }
    }
}
