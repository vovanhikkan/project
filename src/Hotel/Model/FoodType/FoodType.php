<?php

namespace App\Hotel\Model\FoodType;

use Doctrine\ORM\Mapping as ORM;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;

/**
 * FoodType
 *
 * @ORM\Table(name="food_type")
 * @ORM\Entity()
 */
class FoodType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_food_type_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * FoodType constructor.
     * @param Uuid $id
     * @param Name $name
     * @param bool $isActive
     * @param Sort $sort
     */
    public function __construct(Uuid $id, Name $name, bool $isActive, Sort $sort)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->sort = $sort;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    public function update(?Name $name)
    {
        if ($name) {
            $this->name = $name;
        }
    }
}
