<?php

declare(strict_types=1);

namespace App\Hotel\Model\Budget;

use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;
use Doctrine\ORM\Mapping as ORM;

/**
 * Budget.
 * @ORM\Entity()
 * @ORM\Table(name="budget")
 */
class Budget
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_budget_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="hotel_budget_budget_from")
     */
    private BudgetFrom $budgetFrom;

    /**
     * @ORM\Column(type="hotel_budget_budget_to")
     */
    private BudgetTo $budgetTo;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * Budget constructor.
     * @param Uuid $id
     * @param Name $name
     * @param BudgetFrom $budgetFrom
     * @param BudgetTo $budgetTo
     * @param Sort $sort
     */
    public function __construct(Uuid $id, Name $name, BudgetFrom $budgetFrom, BudgetTo $budgetTo, Sort $sort, bool $isActive)
    {
        $this->id = $id;
        $this->name = $name;
        $this->budgetFrom = $budgetFrom;
        $this->budgetTo = $budgetTo;
        $this->sort = $sort;
        $this->isActive = $isActive;
    }

    /**
     * @return Uuid
     */
    public function getId() : Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName() : Name
    {
        return $this->name;
    }



    /**
     * @return BudgetFrom
     */
    public function getBudgetFrom() : BudgetFrom
    {
        return $this->budgetFrom;
    }

    /**
     * @return BudgetTo
     */
    public function getBudgetTo() : BudgetTo
    {
        return $this->budgetTo;
    }

    /**
     * @return Sort
     */
    public function getSort() : Sort
    {
        return $this->sort;
    }

    /**
     * @param Name|null $name
     * @param BudgetFrom|null $budgetFrom
     * @param BudgetTo|null $budgetTo
     * @param Sort|null $sort
     */
    public function update(
        ?Name $name,
        ?BudgetFrom $budgetFrom,
        ?BudgetTo $budgetTo,
        ?Sort $sort
    ) {
        if (!is_null($name)) {
            $this->name = $name;
        }

        if (!is_null($budgetFrom)) {
            $this->budgetFrom = $budgetFrom;
        }

        if (!is_null($budgetTo)) {
            $this->budgetTo = $budgetTo;
        }

        if (!is_null($sort)) {
            $this->sort = $sort;
        }
    }
}
