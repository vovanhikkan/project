<?php

declare(strict_types=1);

namespace App\Hotel\Model\HotelExtraPlaceType;

use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Hotel\Hotel;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * RoomMainPlaceVariant.
 * @ORM\Entity()
 * @ORM\Table(name="hotel_extra_place_type")
 */
class HotelExtraPlaceType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_hotel_extra_place_type_name")
     */
    private Name $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel", inversedBy="mainPlaceVariant")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private Hotel $hotel;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\HotelAgeRange\HotelAgeRange")
     * @ORM\JoinTable(
     *     name="hotel_extra_place_type_age_range",
     *     joinColumns={@ORM\JoinColumn(name="hotel_extra_place_type_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="hotel_age_range_id", referencedColumnName="id")}
     * )
     */
    private ?Collection $ageRanges;


    /**
     * HotelExtraPlaceType constructor.
     * @param Uuid $id
     * @param Name $name
     * @param Hotel $hotel
     */
    public function __construct(Uuid $id, Name $name, Hotel $hotel)
    {
        $this->id = $id;
        $this->name = $name;
        $this->hotel = $hotel;
        $this->ageRanges = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * @return array
     */
    public function getAgeRanges() : array
    {
        return $this->ageRanges->toArray();
    }

    /**
     * @param HotelAgeRange $hotelAgeRange
     */
    public function addAgeRange(HotelAgeRange $hotelAgeRange) : void
    {
        $this->ageRanges->add($hotelAgeRange);
    }

    /**
     * @param Name|null $name
     * @param Hotel|null $hotel
     */
    public function update(
        ?Name $name,
        ?Hotel $hotel
    ) {
        if ($name) {
            $this->name = $name;
        }

        if ($hotel) {
            $this->hotel = $hotel;
        }
    }
}
