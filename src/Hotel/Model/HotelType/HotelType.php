<?php

namespace App\Hotel\Model\HotelType;

use Doctrine\ORM\Mapping as ORM;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;

/**
 * HotelType
 *
 * @ORM\Table(name="hotel_type")
 * @ORM\Entity()
 */
class HotelType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_hotel_type_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * HotelType constructor.
     * @param Uuid $id
     * @param Name $name
     * @param bool $isActive
     * @param Sort $sort
     */
    public function __construct(Uuid $id, Name $name, bool $isActive, Sort $sort)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
        $this->sort = $sort;
    }

    /**
     * Get id.
     *
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return Name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Get sort.
     *
     * @return Sort|null
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param Name $name
     * @param bool $isActive
     * @param Sort $sort
     */
    public function update(Name $name, bool $isActive, Sort $sort)
    {
        if ($name) {
            $this->name = $name;
        }
        
        $this->isActive = $isActive;
        
        if ($sort) {
            $this->sort = $sort;
        }
    }
    
    
}
