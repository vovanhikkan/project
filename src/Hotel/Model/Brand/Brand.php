<?php

declare(strict_types=1);

namespace App\Hotel\Model\Brand;

use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Brand
 *
 * @ORM\Table(name="brand")
 * @ORM\Entity()
 */
class Brand
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_brand_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * Brand constructor.
     * @param Uuid $id
     * @param Name $name
     * @param bool $isActive
     */
    public function __construct(Uuid $id, Name $name, bool $isActive)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function update(?Name $name)
    {
        if ($name) {
            $this->name = $name;
        }
    }
}
