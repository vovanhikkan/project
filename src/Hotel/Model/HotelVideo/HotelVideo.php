<?php

namespace App\Hotel\Model\HotelVideo;

use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\Hotel\Hotel;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;

/**
 * RoomVideo
 *
 * @ORM\Table(name="hotels_videos", indexes={@ORM\Index(name="background", columns={"background"}), @ORM\Index(name="hotel_id", columns={"hotel_id"})})
 * @ORM\Entity()
 */
class HotelVideo
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_hotel_video_youtube_code")
     */
    private YoutubeCode $youtubeCode;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private Hotel $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="background", referencedColumnName="id")
     */
    private ?File $background = null;

    /**
     * HotelVideo constructor.
     * @param Uuid $id
     * @param YoutubeCode $youtubeCode
     * @param Sort $sort
     * @param Hotel $hotel
     */
    public function __construct(Uuid $id, YoutubeCode $youtubeCode, Sort $sort, Hotel $hotel)
    {
        $this->id = $id;
        $this->youtubeCode = $youtubeCode;
        $this->sort = $sort;
        $this->hotel = $hotel;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return YoutubeCode
     */
    public function getYoutubeCode(): YoutubeCode
    {
        return $this->youtubeCode;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * @return File|null
     */
    public function getBackground(): ?File
    {
        return $this->background;
    }
}
