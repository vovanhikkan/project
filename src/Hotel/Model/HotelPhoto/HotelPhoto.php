<?php

namespace App\Hotel\Model\HotelPhoto;

use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\Hotel\Hotel;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;

/**
 * HotelPhoto
 *
 * @ORM\Table(name="hotel_photo")
 * @ORM\Entity()
 */
class HotelPhoto
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Hotel\Hotel")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private Hotel $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     */
    private ?File $photo = null;

    /**
     * HotelPhoto constructor.
     * @param Uuid $id
     * @param Sort $sort
     * @param Hotel $hotel
     */
    public function __construct(Uuid $id, Sort $sort, Hotel $hotel)
    {
        $this->id = $id;
        $this->sort = $sort;
        $this->hotel = $hotel;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * @return File|null
     */
    public function getPhoto(): ?File
    {
        return $this->photo;
    }
}
