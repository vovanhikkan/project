<?php

declare(strict_types=1);

namespace App\Hotel\Model\BedType;

use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BedType.
 * @ORM\Entity()
 * @ORM\Table(name="bed_type")
 */
class BedType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_bed_type_name")
     */
    private Name $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\Hotel\Hotel")
     */
    private Collection $hotels;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="web_icon", referencedColumnName="id")
     */
    private ?File $webIcon = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="mobile_icon", referencedColumnName="id", nullable=true)
     */
    private ?File $mobileIcon = null;

    /**
     * BedType constructor.
     * @param Uuid $id
     * @param Name $name
     */
    public function __construct(Uuid $id, Name $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->hotels = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return File|null
     */
    public function getWebIcon(): ?File
    {
        return $this->webIcon;
    }

    /**
     * @return File|null
     */
    public function getMobileIcon(): ?File
    {
        return $this->mobileIcon;
    }

    /**
     * @param File|null $webIcon
     */
    public function setWebIcon(?File $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

    /**
     * @param File|null $mobileIcon
     */
    public function setMobileIcon(?File $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    public function update(
        ?Name $name,
        ?File $mobIcon,
        ?File $webIcon
    ) {
        if ($name) {
            $this->name = $name;
        }

        if ($mobIcon) {
            $this->mobileIcon = $mobIcon;
        }

        if ($webIcon) {
            $this->webIcon = $webIcon;
        }
    }
}
