<?php

namespace App\Hotel\Model\RoomVideo;

use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;
use App\Hotel\Model\Room\Room;
use App\Application\ValueObject\Uuid;
use App\Application\ValueObject\Sort;

/**
 * RoomVideo
 *
 * @ORM\Table(name="room_video")
 * @ORM\Entity()
 */
class RoomVideo
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="hotel_room_video_youtube_code")
     */
    private YoutubeCode $youtubeCode;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;

    /**
     * @ORM\ManyToOne(targetEntity="App\Hotel\Model\Room\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    private Room $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="background", referencedColumnName="id")
     */
    private ?File $background;

    /**
     * RoomVideo constructor.
     * @param Uuid $id
     * @param YoutubeCode $youtubeCode
     * @param Sort $sort
     * @param Room $room
     * @param File|null $background
     */
    public function __construct(Uuid $id, YoutubeCode $youtubeCode, Sort $sort, Room $room, ?File $background)
    {
        $this->id = $id;
        $this->youtubeCode = $youtubeCode;
        $this->sort = $sort;
        $this->room = $room;
        $this->background = $background;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return YoutubeCode
     */
    public function getYoutubeCode(): YoutubeCode
    {
        return $this->youtubeCode;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return File|null
     */
    public function getBackground(): ?File
    {
        return $this->background;
    }
}
