<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\RatePlanGroup\RatePlanGroup;

class RatePlanGroupRepository extends AbstractRepository
{
    public function add(RatePlanGroup $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('r');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): RatePlanGroup
    {
        /** @var RatePlanGroup|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('RatePlanGroup with id %s not found', (string)$id));
        }

        return $model;
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('a')
            ->select('MAX(a.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function getModelClassName(): string
    {
        return RatePlanGroup::class;
    }
}
