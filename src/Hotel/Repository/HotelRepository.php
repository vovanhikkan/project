<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Dto\SearchDto;
use App\Hotel\Model\Hotel\Hotel;

/**
 * HotelRepository.
 */
class HotelRepository extends AbstractRepository
{
    public function add(Hotel $hotel): void
    {
        $this->entityManager->persist($hotel);
    }

    /**
     * @return Hotel[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('h');
        $qb->orderBy('h.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Hotel
    {
        /** @var Hotel|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Hotel with id %s not found', (string)$id));
        }

        return $model;
    }

    public function searchHotelsById(array $ids)
    {
        $qb = $this->entityRepository->createQueryBuilder('h');
        if (count($ids)) {
            $qb->andWhere('h.id IN (:ids)')
                ->setParameter('ids', $ids);
        } else {
            return [];
        }

        return $qb->getQuery()->getResult();
    }

    public function searchHotels(SearchDto $searchDto)
    {
        $qb = $this->entityRepository->createQueryBuilder('h');
        $qb->orderBy('h.sort', 'ASC');
        if ($facilities = $searchDto->getFacilities()) {
            $qb->innerJoin('h.facilities', 'f')
                ->andWhere('f.id IN (:facilities)')
                ->setParameter('facilities', $facilities);
        }

        if ($bundles = $searchDto->getBundles()) {
            $qb->innerJoin('h.ratePlans', 'rp')
                ->innerJoin('rp.bundles', 'b')
                ->andWhere('b.id IN (:bundles)')
                ->setParameter('bundles', $bundles);
        }

        if ($starCounts = $searchDto->getStarCounts()) {
            $qb->andWhere('h.starCount IN (:starCounts)')
                ->setParameter('starCounts', $starCounts);
        }

        if ($hotelTypes = $searchDto->getHotelTypes()) {
            $qb->andWhere('h.type IN (:hotelTypes)')
                ->setParameter('hotelTypes', $hotelTypes);
        }

        if ($foodTypes = $searchDto->getFoodTypes()) {
            $qb->andWhere('rp.foodType IN (:foodTypes)')
                ->setParameter('foodTypes', $foodTypes);
        }

        if ($hotelBrands = $searchDto->getBrands()) {
            $qb->andWhere('h.brand IN (:hotelBrands)')
                ->setParameter('hotelBrands', $hotelBrands);
        }

        if ($adultCount = $searchDto->getAdultCount()) {
            if (!in_array('r', $qb->getAllAliases())) {
                $qb->innerJoin('h.rooms', 'r');
            }
            $qb->andWhere('r.mainPlaceCount >= :adultCount')
                ->setParameter('adultCount', $adultCount);
        }

        if ($childCount = $searchDto->getChildCount()) {
            if (!in_array('r', $qb->getAllAliases())) {
                $qb->innerJoin('h.rooms', 'r');
            }
            $qb->andWhere('(r.mainPlaceCount + r.extraPlaceCount + r.zeroPlaceCount) >= :childCount')
                ->setParameter('childCount', $childCount);
        }

        if ($bedTypes = $searchDto->getBedTypes()) {
            $qb->innerJoin('h.bedTypes', 'b')
                ->andWhere('b.id IN (:bedTypes)')
                ->setParameter('bedTypes', $bedTypes);
        }

        if ($childAges = $searchDto->getChildAges()) {
            $qb->innerJoin('h.hotelExtraPlaceType', 'hept')
                ->innerJoin('hept.ageRanges', 'ar');

            foreach ($childAges as $childAge) {
                $qb->andWhere('ar.ageFrom <= :childAge AND ar.ageTo >= :childAge')
                    ->setParameter('childAge', $childAge);
            }
        }

        return $qb->getQuery()->getResult();
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return Hotel::class;
    }
}
