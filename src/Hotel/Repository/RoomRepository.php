<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Dto\SearchAccommodationDto;
use App\Hotel\Model\Room\Room;
use DateTimeImmutable;

/**
 * RoomRepository.
 */
class RoomRepository extends AbstractRepository
{
    public function add(Room $room): void
    {
        $this->entityManager->persist($room);
    }

    /**
     * @return Room[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('r');
        $qb->orderBy('r.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Room
    {
        /** @var Room|null $model */
        $model = $this->entityRepository->findOneBy(['id' => (string)$id]);
        if ($model === null) {
            throw new NotFoundException(sprintf('Room with id %s not found', (string)$id));
        }

        return $model;
    }

    public function searchAccommodation(SearchAccommodationDto $searchDto)
    {
        $qb = $this->entityRepository->createQueryBuilder('r')
            ->where('r.hotel = :hotel')
            ->innerJoin('r.hotel', 'h')
            ->setParameter('hotel', $searchDto->getHotel());
 //           ->innerJoin('r.roomMainPlaceVariant', 'rpv');

        if ($facilities = $searchDto->getFacilities()) {
            $qb->innerJoin('r.facilities', 'f')
                ->andWhere('f.id IN (:facilities)')
                ->setParameter('facilities', $facilities);
        }

        if ($foodTypes = $searchDto->getFoodTypes()) {
            $qb->innerJoin('r.ratePlans', 'rp')
                ->andWhere('rp.foodType IN (:foodTypes)')
                ->setParameter('foodTypes', $foodTypes);
        }

        if ($adultCount = $searchDto->getAdultCount()) {
            $qb->andWhere('r.mainPlaceCount >= :adultCount')
                ->setParameter('adultCount', $adultCount);
        }

        if ($childCount = $searchDto->getChildCount()) {
            $qb->andWhere('(r.mainPlaceCount + r.extraPlaceCount + r.zeroPlaceCount) >= :childCount')
                ->setParameter('childCount', $childCount);
        }

        if ($adultCount && $childCount) {
            $countGuests = $adultCount + $childCount;
            $qb->andWhere('(r.mainPlaceCount + r.extraPlaceCount + r.zeroPlaceCount) >= :countGuests')
                ->setParameter('countGuests', $countGuests);
        }

        if ($childAges = $searchDto->getChildAges()) {
            $qb->innerJoin('h.hotelExtraPlaceType', 'hept')
                ->innerJoin('hept.ageRanges', 'ar');

            foreach ($childAges as $key => $childAge) {
                $condition = 'ar.ageFrom <= :child'.$key.' AND ar.ageTo >= :child'.$key;
                $qb->andWhere($condition)
                    ->setParameter('child'.$key, $childAge);
            }
        }

        return $qb->getQuery()->getResult();
    }

    protected function getModelClassName(): string
    {
        return Room::class;
    }
}
