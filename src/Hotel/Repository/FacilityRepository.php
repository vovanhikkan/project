<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Facility\Facility;

/**
 * FacilityRepository.
 */
class FacilityRepository extends AbstractRepository
{
    public function add(Facility $facility): void
    {
        $this->entityManager->persist($facility);
    }

    /**
     * @return Facility[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('r');
        $qb->orderBy('r.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Facility
    {
        /** @var Facility|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Faciility with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('f')
            ->select('MAX(f.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    public function fetchAllByShownSearch(?bool $shown)
    {
        $qb = $this->entityRepository->createQueryBuilder('r');
        if ($shown !== null) {
            $qb->andWhere('r.isShownInSearch = :shown')->setParameter('shown', $shown);
        }

        $qb->orderBy('r.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    protected function getModelClassName(): string
    {
        return Facility::class;
    }
}
