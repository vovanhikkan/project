<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Brand\Brand;

class BrandRepository extends AbstractRepository
{
    public function add(Brand $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): Brand
    {
        /** @var Brand|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Brand with id %s not found', (string)$id));
        }

        return $model;
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return Brand::class;
    }
}
