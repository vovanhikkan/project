<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Budget\Budget;

class BudgetRepository extends AbstractRepository
{
    public function add(Budget $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b');
        //$qb->orderBy('b.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function fetchAllByActive(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b')->where('b.isActive = 1');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): Budget
    {
        /** @var Budget|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Budget with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('a')
            ->select('MAX(a.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return Budget::class;
    }
}
