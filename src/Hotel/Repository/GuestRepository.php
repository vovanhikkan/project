<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\Guest\Guest;
use App\Order\Command\Cart\AddItemRoomGuest\CartItemRoomGuestDto;

class GuestRepository extends AbstractRepository
{
    public function add(Guest $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): Guest
    {
        /** @var Guest|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Guest with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getGuest($guestDto)
    {
        $qb = $this->entityRepository->createQueryBuilder('g')
            ->where('g.firstName = :firstName')
            ->andWhere('g.lastName = :lastName')
            ->andWhere('g.age = :age')
            ->setParameter('firstName', $guestDto->getFirstName())
            ->setParameter('lastName', $guestDto->getLastName())
            ->setParameter('age', $guestDto->getAge());

        return $qb->getQuery()->getResult();
    }



    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return Guest::class;
    }
}
