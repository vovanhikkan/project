<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelExtraPlaceType\HotelExtraPlaceType;

/**
 * Class HotelExtraPlaceTypeRepository
 * @package App\Hotel\Repository
 */
class HotelExtraPlaceTypeRepository extends AbstractRepository
{
    public function add(HotelExtraPlaceType $hotelExtraPlaceType): void
    {
        $this->entityManager->persist($hotelExtraPlaceType);
    }

    /**
     * @return HotelExtraPlaceType[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('h');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): HotelExtraPlaceType
    {
        /** @var HotelExtraPlaceType|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('HotelExtraPlaceType with id %s not found', (string)$id));
        }

        return $model;
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return HotelExtraPlaceType::class;
    }
}
