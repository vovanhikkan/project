<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelType\HotelType;

/**
 * TypeRepository.
 */
class HotelTypeRepository extends AbstractRepository
{
    public function add(HotelType $facility): void
    {
        $this->entityManager->persist($facility);
    }

    /**
     * @return HotelType[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('f');
        $qb->orderBy('f.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): HotelType
    {
        /** @var HotelType|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Hotel type with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('a')
            ->select('MAX(a.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return HotelType::class;
    }
}
