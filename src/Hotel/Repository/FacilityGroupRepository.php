<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\FacilityGroup\FacilityGroup;

/**
 * RoomRepository.
 */
class FacilityGroupRepository extends AbstractRepository
{
    public function add(FacilityGroup $facilityGroup): void
    {
        $this->entityManager->persist($facilityGroup);
    }

    /**
     * @return FacilityGroup[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('r');
        $qb->orderBy('r.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): FacilityGroup
    {
        /** @var FacilityGroup|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Faciility group with id %s not found', (string)$id));
        }

        return $model;
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('a')
            ->select('MAX(a.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return FacilityGroup::class;
    }
}
