<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\HotelAgeRange\HotelAgeRange;

/**
 * Class HotelAgeRangeRepository
 * @package App\Hotel\Repository
 */
class HotelAgeRangeRepository extends AbstractRepository
{
    public function add(HotelAgeRange $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('b');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): HotelAgeRange
    {
        /** @var HotelAgeRange|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('HotelAgeRange with id %s not found', (string)$id));
        }

        return $model;
    }



    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return HotelAgeRange::class;
    }
}
