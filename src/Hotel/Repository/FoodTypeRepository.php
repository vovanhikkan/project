<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\FoodType\FoodType;

/**
 * FoodTypeRepository.
 */
class FoodTypeRepository extends AbstractRepository
{
    public function add(FoodType $facility): void
    {
        $this->entityManager->persist($facility);
    }

    /**
     * @return FoodType[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('f');
        $qb->orderBy('f.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): FoodType
    {
        /** @var FoodType|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Food type with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort()
    {
        return (int)$this->entityRepository->createQueryBuilder('ft')
            ->select('MAX(ft.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return FoodType::class;
    }
}
