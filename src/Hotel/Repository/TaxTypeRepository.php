<?php

declare(strict_types=1);

namespace App\Hotel\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Hotel\Model\TaxType\TaxType;

class TaxTypeRepository extends AbstractRepository
{
    public function add(TaxType $model): void
    {
        $this->entityManager->persist($model);
    }


    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('t');

        return $qb->getQuery()->getResult();
    }


    public function get(Uuid $id): TaxType
    {
        /** @var TaxType|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('TaxType with id %s not found', (string)$id));
        }

        return $model;
    }


    public function delete(Uuid $id): void
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }


    public function getModelClassName(): string
    {
        return TaxType::class;
    }
}
