<?php

declare(strict_types=1);

namespace App\Place\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Place\Model\LabelType\LabelType;

class LabelTypeRepository extends AbstractRepository
{
	public function add(LabelType $model): void
	{
		$this->entityManager->persist($model);
	}


	public function fetchAll(): array
	{
		$qb = $this->entityRepository->createQueryBuilder('l');

		return $qb->getQuery()->getResult();
	}


	public function get(Uuid $id): LabelType
	{
		/** @var LabelType|null $model */
		$model = $this->entityRepository->find($id);
		if ($model === null) {
		throw new NotFoundException(sprintf('LabelType with id %s not found', (string)$id));
		}

		return $model;
	}


	public function delete(Uuid $id): void
	{
		$model = $this->get($id);
		$this->entityManager->remove($model);
	}


	public function getModelClassName(): string
	{
		return LabelType::class;
	}
}
