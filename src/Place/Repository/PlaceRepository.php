<?php

declare(strict_types=1);

namespace App\Place\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Place\Model\Place\Place;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;

/**
 * PlaceRepository.
 */
class PlaceRepository extends AbstractRepository
{
    public function add(Place $model): void
    {
        $this->entityManager->persist($model);
    }

    /**
     * @return Place[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('p');
        $qb->orderBy('p.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function fetchAllOpened() : array
    {
        $items = $this->fetchAll();
        $result = [];
        $now = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        foreach ($items as $place) {
            if ($this->isWorkingTime($now, $place)) {
                $result[] = $place;
            }
        }

        return $result;
    }

    /**
     * @return Place[]
     */
    public function fetchById(array $ids): array
    {
        $qb = $this->entityRepository->createQueryBuilder('p');
        $qb->where('p.id IN (:id)')->setParameter('id', $ids);

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Place
    {
        /** @var Place|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Place with id %s not found', (string)$id));
        }

        return $model;
    }

    /**
     * @param Uuid $id
     * @return Place
     * @throws \Exception
     */
    public function getOpened(Uuid $id): ?Place
    {
        /** @var Place|null $model */
        $model = $this->entityRepository->find($id);
        if ($model !== null) {
            $now = new DateTime();
            if ($this->isWorkingTime($now, $model)) {
                return $model;
            }
        }

        return null;
    }

    /**
     * @param DateTime $now
     * @param Place $model
     * @return bool
     * @throws \Exception
     */
    private function isWorkingTime(DateTime $now, Place $model) : bool
    {
        if (!$model->getWorksFrom() && !$model->getWorksTo()) {
            return false;
        }

        if ($model->isFullDay()) {
            return true;
        }

        $worksFrom = new DateTime($model->getWorksFrom()->format("H:i"), new DateTimeZone('Europe/Moscow'));
        $worksTo = new DateTime($model->getWorksTo()->format("H:i"), new DateTimeZone('Europe/Moscow'));
        if ($now >= $worksFrom && $now <= $worksTo) {
            return true;
        }

        return false;
    }

    public function getMaxSort(): int
    {
        return (int)$this->entityRepository->createQueryBuilder('p')
            ->select('MAX(p.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return Place::class;
    }
}
