<?php

declare(strict_types=1);

namespace App\Place\Model\Label;

use App\Application\ValueObject\Color;
use App\Application\ValueObject\Uuid;
use App\Place\Model\LabelType\LabelType;
use Doctrine\ORM\Mapping as ORM;

/**
 * Label.
 * @ORM\Entity()
 * @ORM\Table(name="label")
 */
class Label
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\LabelType\LabelType")
     * @ORM\JoinColumn(name="label_type_id", referencedColumnName="id")
     */
    private ?LabelType $labelType = null;

    /**
     * @ORM\Column(type="color")
     */
    private Color $color;

    /**
     * @ORM\Column(type="place_label_content")
     */
    private Content $content;

    public function __construct(
        Uuid $id,
        Color $color,
        Content $content
    ) {
        $this->id = $id;
        $this->color = $color;
        $this->content = $content;
    }

    public function update(
        ?LabelType $labelType,
        Color $color,
        Content $content
    ): void {
        $this->labelType = $labelType;
        $this->color = $color;
        $this->content = $content;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return LabelType|null
     */
    public function getLabelType(): ?LabelType
    {
        return $this->labelType;
    }

    /**
     * @param LabelType|null $labelType
     */
    public function setLabelType(?LabelType $labelType): void
    {
        $this->labelType = $labelType;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @param Color $color
     */
    public function setColor(Color $color): void
    {
        $this->color = $color;
    }

    /**
     * @return Content
     */
    public function getContent(): Content
    {
        return $this->content;
    }

    /**
     * @param Content $content
     */
    public function setContent(Content $content): void
    {
        $this->content = $content;
    }
}
