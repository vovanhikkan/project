<?php

declare(strict_types=1);

namespace App\Place\Model\Block;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Body.
 */
final class Body extends ArrayValueObject
{
}
