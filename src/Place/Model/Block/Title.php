<?php

declare(strict_types=1);

namespace App\Place\Model\Block;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Title.
 */
final class Title extends ArrayValueObject
{
}
