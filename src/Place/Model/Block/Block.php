<?php

declare(strict_types=1);

namespace App\Place\Model\Block;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Place\Model\Place\Place;
use App\Storage\Model\File\File;
use App\Storage\Model\Video\Video;
use Doctrine\ORM\Mapping as ORM;

/**
 * Block.
 *
 * @ORM\Entity()
 * @ORM\Table(name="place_block")
 */
class Block
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Place\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false)
     */
    private Place $place;

    /**
     * @ORM\Column(type="place_block_title")
     */
    private Title $title;

    /**
     * @ORM\Column(type="place_block_body")
     */
    private Body $body;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id", nullable=true)
     */
    private File $photo;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\Video\Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id", nullable=true)
     */
    private ?Video $video = null;

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Place
     */
    public function getPlace(): Place
    {
        return $this->place;
    }

    /**
     * @return Title
     */
    public function getTitle(): Title
    {
        return $this->title;
    }

    /**
     * @return Body
     */
    public function getBody(): Body
    {
        return $this->body;
    }

    /**
     * @return File
     */
    public function getPhoto(): File
    {
        return $this->photo;
    }

    /**
     * @return Video
     */
    public function getVideo(): ?Video
    {
        return $this->video;
    }

//    /**
//     * @var array|null
//     *
//     * @ORM\Column(name="params", type="json", nullable=true)
//     */
//    private $params;
}
