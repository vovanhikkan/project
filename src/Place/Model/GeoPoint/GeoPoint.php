<?php

declare(strict_types=1);

namespace App\Place\Model\GeoPoint;

use App\Application\Model\SortableTrait;
use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Latitude;
use App\Application\ValueObject\Longitude;
use App\Application\ValueObject\Uuid;
use App\Place\Model\Place\Place;
use Doctrine\ORM\Mapping as ORM;

/**
 * GeoPoint.
 *
 * @ORM\Entity()
 * @ORM\Table(name="place_geo_point")
 */
class GeoPoint
{
    use TimestampableTrait;
    use SortableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Place\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false)
     */
    private Place $place;

    /**
     * @ORM\Column(type="latitude", columnDefinition="DECIMAL(10, 8) UNSIGNED NOT NULL")
     */
    private Latitude $latitude;

    /**
     * @ORM\Column(type="longitude", columnDefinition="DECIMAL(11, 8) UNSIGNED NOT NULL")
     */
    private Longitude $longitude;

    /**
     * @ORM\Column(type="place_geo_point_description")
     */
    private Description $description;

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Place
     */
    public function getPlace(): Place
    {
        return $this->place;
    }

    /**
     * @return Latitude
     */
    public function getLatitude(): Latitude
    {
        return $this->latitude;
    }

    /**
     * @return Longitude
     */
    public function getLongitude(): Longitude
    {
        return $this->longitude;
    }

    /**
     * @return Description
     */
    public function getDescription(): Description
    {
        return $this->description;
    }
}
