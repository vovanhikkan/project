<?php

declare(strict_types=1);

namespace App\Place\Model\Place;

use App\Application\Model\SortableTrait;
use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Storage\Model\Video\Video;
use Doctrine\ORM\Mapping as ORM;

/**
 * PlaceVideo.
 *
 * @ORM\Entity()
 * @ORM\Table(name="place_video")
 */
class PlaceVideo
{
    use TimestampableTrait;
    use SortableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Place\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false)
     */
    private Place $place;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\Video\Video")
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id", nullable=false)
     */
    private Video $video;

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Video
     */
    public function getVideo(): Video
    {
        return $this->video;
    }
}
