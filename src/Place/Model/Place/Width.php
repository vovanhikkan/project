<?php

declare(strict_types=1);

namespace App\Place\Model\Place;

use App\Application\ValueObject\EnumValueObject;

/**
 * Width.
 */
final class Width extends EnumValueObject
{
    const SHORT = 100;
    const MEDIUM = 200;
    const LARGE = 300;
    const EXTRA_LARGE = 400;

    public function isShort(): bool
    {
        return $this->value === self::SHORT;
    }

    public function isMedium(): bool
    {
        return $this->value === self::MEDIUM;
    }

    public function isLarge(): bool
    {
        return $this->value === self::LARGE;
    }

    public function isExtraLarge(): bool
    {
        return $this->value === self::EXTRA_LARGE;
    }
}
