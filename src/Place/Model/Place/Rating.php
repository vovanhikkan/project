<?php

declare(strict_types=1);

namespace App\Place\Model\Place;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Rating.
 */
final class Rating extends ArrayValueObject
{
}
