<?php

declare(strict_types=1);

namespace App\Place\Model\Place;

use App\Application\Model\SortableTrait;
use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Location\Model\Location\Location;
use App\Place\Model\Block\Block;
use App\Place\Model\GeoPoint\GeoPoint;
use App\Product\Model\Section\Section;
use App\Storage\Model\File\File;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Place\Model\Label\Label;
use Doctrine\ORM\Mapping as ORM;

/**
 * Place.
 *
 * @ORM\Entity()
 * @ORM\Table(name="place")
 */
class Place
{
    use TimestampableTrait;
    use SortableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="place_place_title")
     */
    private Title $title;

    /**
     * @ORM\Column(type="place_place_description")
     */
    private Description $description;

    /**
     * @ORM\Column(type="place_place_sub_title", nullable=true)
     */
    private ?SubTitle $subTitle = null;

    /**
     * @ORM\Column(type="place_place_sub_description", nullable=true)
     */
    private ?SubDescription $subDescription = null;

    /**
     * @ORM\Column(type="place_place_working_hours_description")
     */
    private WorkingHoursDescription $workingHoursDescription;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL")
     */
    private bool $isFullDay;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private ?DateTimeImmutable $worksFrom;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private ?DateTimeImmutable $worksTo;

    /**
     * @ORM\Column(type="place_place_age_restriction", nullable=true)
     */
    private ?AgeRestriction $ageRestriction = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Label\Label")
     * @ORM\JoinColumn(name="label_id", referencedColumnName="id")
     */
    private ?Label $label = null;

    /**
     * @ORM\Column(type="place_place_address", nullable=true)
     */
    private ?Address $address = null;

    /**
     * @ORM\Column(type="phone", nullable=true)
     */
    private ?Phone $phone = null;

    /**
     * @ORM\Column(type="place_place_code", nullable=true)
     */
    private ?Code $code = null;

    /**
     * @ORM\Column(type="amount", nullable=true)
     */
    private ?Amount $minPrice = null;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL")
     */
    private bool $isFree;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL")
     */
    private bool $isCableWayRequired;

    /**
     * @ORM\Column(type="place_place_button_text")
     */
    private ButtonText $buttonText;

    /**
     * @ORM\Column(type="place_place_width")
     */
    private ?Width $width = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="background_id", referencedColumnName="id", nullable=true)
     */
    private ?File $background = null;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="place_place_rating", nullable=true)
     */
    private ?Rating $rating = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Location\Model\Location\Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=true)
     */
    private ?Location $location = null;

    /**
     * @ORM\Column(type="place_place_recommended_id")
     */
    private ?RecommendedId $recommendedId = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Product\Model\Section\Section")
     * @ORM\JoinTable(
     *     name="place_section",
     *     joinColumns={@ORM\JoinColumn(name="place_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="section_id", referencedColumnName="id")}
     * )
     */
    private Collection $sections;

    /**
     * @ORM\OneToMany(targetEntity="App\Place\Model\Block\Block", mappedBy="place")
     */
    private ?Collection $blocks = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Place\Model\GeoPoint\GeoPoint", mappedBy="place")
     */
    private ?Collection $geoPoints = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Place\Model\Place\PlacePhoto", mappedBy="place")
     */
    private ?Collection $placePhotos = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Place\Model\Place\PlaceVideo", mappedBy="place")
     */
    private ?Collection $placeVideos = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Content\Model\Category\Category")
     * @ORM\JoinTable(
     *     name="category_place",
     *     joinColumns={@ORM\JoinColumn(name="place_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private Collection $categories;

    // rating, locations, review, background

    public function __construct(
        Uuid $id,
        Title $title,
        Description $description,
        ?SubTitle $subTitle,
        ?SubDescription $subDescription,
        WorkingHoursDescription $workingHoursDescription,
        bool $isFullDay,
        ?DateTimeImmutable $worksFrom,
        ?DateTimeImmutable $worksTo,
        ?AgeRestriction $ageRestriction,
        ?Address $address,
        ?Phone $phone,
        ?Code $code,
        ?Amount $minPrice,
        bool $isFree,
        bool $isCableWayRequired,
        ButtonText $buttonText,
        ?Width $width,
        ?File $background,
        array $sections,
        Sort $sort
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->subTitle = $subTitle;
        $this->subDescription = $subDescription;
        $this->workingHoursDescription = $workingHoursDescription;
        $this->isFullDay = $isFullDay;
        $this->worksFrom = $worksFrom;
        $this->worksTo = $worksTo;
        $this->ageRestriction = $ageRestriction;
        $this->address = $address;
        $this->phone = $phone;
        $this->code = $code;
        $this->minPrice = $minPrice;
        $this->isFree = $isFree;
        $this->isCableWayRequired = $isCableWayRequired;
        $this->buttonText = $buttonText;
        $this->width = $width;
        $this->background = $background;
        $this->isActive = true;
        $this->sections = new ArrayCollection($sections);
        $this->sort = $sort;
        $this->createdAt = new DateTimeImmutable();
        $this->blocks = new ArrayCollection();
        $this->geoPoints = new ArrayCollection();
        $this->placePhotos = new ArrayCollection();
        $this->placeVideos = new ArrayCollection();
        $this->categories = new ArrayCollection();

        $this->rating = new Rating(['1'=>0, '2'=>0, '3'=>0, '4'=>0, '5'=>0]);
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getTitle(): Title
    {
        return $this->title;
    }

    public function getDescription(): Description
    {
        return $this->description;
    }

    public function getSubTitle(): ?SubTitle
    {
        return $this->subTitle;
    }

    public function getSubDescription(): ?SubDescription
    {
        return $this->subDescription;
    }

    public function getWorkingHoursDescription(): WorkingHoursDescription
    {
        return $this->workingHoursDescription;
    }

    /**
     * @return bool
     */
    public function isFullDay(): bool
    {
        return $this->isFullDay;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getWorksFrom(): ?DateTimeImmutable
    {
        return $this->worksFrom;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getWorksTo(): ?DateTimeImmutable
    {
        return $this->worksTo;
    }

    public function getAgeRestriction(): ?AgeRestriction
    {
        return $this->ageRestriction;
    }

    /**
     * @return Label|null
     */
    public function getLabel(): ?Label
    {
        return $this->label;
    }

    /**
     * @param Label|null $label
     */
    public function setLabel(?Label $label): void
    {
        $this->label = $label;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function getPhone(): ?Phone
    {
        return $this->phone;
    }

    public function getCode(): ?Code
    {
        return $this->code;
    }

    public function getMinPrice(): ?Amount
    {
        return $this->minPrice;
    }

    public function isFree(): bool
    {
        return $this->isFree;
    }

    public function isCableWayRequired(): bool
    {
        return $this->isCableWayRequired;
    }

    public function getButtonText(): ButtonText
    {
        return $this->buttonText;
    }

    public function getWidth(): ?Width
    {
        return $this->width;
    }

    public function getBackground(): ?File
    {
        return $this->background;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return Rating|null
     */
    public function getRating(): ?Rating
    {
        return $this->rating;
    }

    /**
     * @param Rating|null $rating
     */
    public function setRating(?Rating $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return Location|null
     */
    public function getLocation(): ?Location
    {
        return $this->location;
    }

    /**
     * @param Location|null $location
     */
    public function setLocation(?Location $location): void
    {
        $this->location = $location;
    }

    /**
     * @return RecommendedId|null
     */
    public function getRecommendedId(): ?RecommendedId
    {
        return $this->recommendedId;
    }

    /**
     * @param RecommendedId|null $recommendedId
     */
    public function setRecommendedId(?RecommendedId $recommendedId): void
    {
        $this->recommendedId = $recommendedId;
    }

    /**
     * @return Section[]
     */
    public function getSections(): array
    {
        return $this->sections->toArray();
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->categories->toArray();
    }

    /**
     * @return Block[]
     */
    public function getBlocks(): array
    {
        return $this->blocks->toArray();
    }

    /**
     * @return GeoPoint[]
     */
    public function getGeoPoints(): array
    {
        return $this->geoPoints->toArray();
    }

    /**
     * @return PlacePhoto[]
     */
    public function getPlacePhotos(): array
    {
        return $this->placePhotos->toArray();
    }

    /**
     * @return PlaceVideo[]
     */
    public function getPlaceVideos(): array
    {
        return $this->placeVideos->toArray();
    }

    /**
     * @param Rating $rating
     */
    public function update(Rating $rating): void
    {
        if ($rating) {
            $this->rating = $rating;
        }
    }
}
