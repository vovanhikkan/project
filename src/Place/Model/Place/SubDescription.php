<?php

declare(strict_types=1);

namespace App\Place\Model\Place;

use App\Application\ValueObject\ArrayValueObject;

/**
 * SubDescription.
 */
final class SubDescription extends ArrayValueObject
{
}
