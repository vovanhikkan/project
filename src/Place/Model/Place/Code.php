<?php

declare(strict_types=1);

namespace App\Place\Model\Place;

use App\Application\ValueObject\StringValueObject;
use Assert\Assertion;

/**
 * Code.
 */
final class Code extends StringValueObject
{
    public function __construct(string $value)
    {
        parent::__construct($value);
        Assertion::maxLength($value, 255, 'Слишком длинный код', $this->getPropertyPath());
    }
}
