<?php

declare(strict_types=1);

namespace App\Place\Model\Place;

use App\Application\Model\SortableTrait;
use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;

/**
 * PlacePhoto.
 *
 * @ORM\Entity()
 * @ORM\Table(name="place_photo")
 */
class PlacePhoto
{
    use TimestampableTrait;
    use SortableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Place\Model\Place\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false)
     */
    private Place $place;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", nullable=false)
     */
    private File $file;

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

}
