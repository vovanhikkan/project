<?php

declare(strict_types=1);

namespace App\Place\Model\LabelType;

use App\Application\ValueObject\StringValueObject;
use Assert\Assertion;

/**
 * DefaultContent.
 */
class DefaultContent extends StringValueObject
{
    public function __construct(string $value)
    {
        parent::__construct($value);
        Assertion::minLength(
            $this->value,
            2,
            sprintf('Минимальная длина поля "%s" 2 символа', $this->getPropertyLabel()),
            $this->getPropertyPath()
        );
    }
}
