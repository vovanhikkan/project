<?php

declare(strict_types=1);

namespace App\Place\Model\LabelType;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Color;
use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use Doctrine\ORM\Mapping as ORM;

/**
 * LabelType.
 * @ORM\Entity()
 * @ORM\Table(name="label_type")
 */
class LabelType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="icon", referencedColumnName="id")
     */
    private File $icon;

    /**
     * @ORM\ManyToOne(targetEntity="App\Storage\Model\File\File")
     * @ORM\JoinColumn(name="svg_icon", referencedColumnName="id")
     */
    private File $svgIcon;

    /**
     * @ORM\Column(type="color")
     */
    private Color $defaultColor;

    /**
     * @ORM\Column(type="place_label_type_default_content")
     */
    private DefaultContent $defaultContent;

    public function __construct(
        Uuid $id,
        File $icon,
        File $svgIcon,
        Color $defaultColor,
        DefaultContent $defaultContent
    ) {
        $this->id = $id;
        $this->icon = $icon;
        $this->svgIcon = $svgIcon;
        $this->defaultColor = $defaultColor;
        $this->defaultContent = $defaultContent;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return File
     */
    public function getIcon(): File
    {
        return $this->icon;
    }

    /**
     * @param File $icon
     */
    public function setIcon(File $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return File
     */
    public function getSvgIcon(): File
    {
        return $this->svgIcon;
    }

    /**
     * @param File $svgIcon
     */
    public function setSvgIcon(File $svgIcon): void
    {
        $this->svgIcon = $svgIcon;
    }

    /**
     * @return Color
     */
    public function getDefaultColor(): Color
    {
        return $this->defaultColor;
    }

    /**
     * @param Color $defaultColor
     */
    public function setDefaultColor(Color $defaultColor): void
    {
        $this->defaultColor = $defaultColor;
    }

    /**
     * @return DefaultContent
     */
    public function getDefaultContent(): DefaultContent
    {
        return $this->defaultContent;
    }

    /**
     * @param DefaultContent $defaultContent
     */
    public function setDefaultContent(DefaultContent $defaultContent): void
    {
        $this->defaultContent = $defaultContent;
    }
}
