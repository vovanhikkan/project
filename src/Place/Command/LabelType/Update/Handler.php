<?php

declare(strict_types=1);

namespace App\Place\Command\LabelType\Update;

use App\Application\ValueObject\Uuid;
use App\Place\Model\LabelType\LabelType;
use App\Place\Repository\LabelTypeRepository;
use App\Place\Service\LabelType\Updater;

class Handler
{
	private Updater $updater;
	private LabelTypeRepository $labelTypeRepository;


	public function __construct(Updater $updater, LabelTypeRepository $labelTypeRepository)
	{
		$this->labelTypeRepository = $labelTypeRepository;
		$this->updater = $updater;
	}


	public function handle(Command $command): LabelType
	{
		$labelType = $this->labelTypeRepository->get(new Uuid($command->getId()));

		return $this->updater->update(
			$labelType
		);
	}
}
