<?php

declare(strict_types=1);

namespace App\Place\Command\LabelType\Update;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
	/**
	 * @Assert\NotBlank()
	 *
	 * private string $id;
	 */
	private string $id;
}
