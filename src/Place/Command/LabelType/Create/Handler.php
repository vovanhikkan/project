<?php

declare(strict_types=1);

namespace App\Place\Command\LabelType\Create;

use App\Application\ValueObject\Uuid;
use App\Place\Model\LabelType\LabelType;
use App\Place\Service\LabelType\Creator;

class Handler
{
	private Creator $creator;


	public function __construct(Creator $creator)
	{
		$this->creator = $creator;
	}


	public function handle(Command $command): LabelType
	{
		return $this->creator->create(
		Uuid::generate()
		);
	}
}
