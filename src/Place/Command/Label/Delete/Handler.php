<?php

declare(strict_types=1);

namespace App\Place\Command\Label\Delete;

use App\Application\ValueObject\Uuid;
use App\Place\Service\Label\Deleter;

class Handler
{
	private Deleter $deleter;


	public function __construct(Deleter $deleter)
	{
		$this->deleter = $deleter;
	}


	public function handle(Command $command): array
	{
		$this->deleter->delete(new Uuid($command->getId()));
		        return  ['message' => 'Label удалена'];
	}
}
