<?php

declare(strict_types=1);

namespace App\Place\Command\Label\Create;

use App\Application\ValueObject\Color;
use App\Application\ValueObject\Uuid;
use App\Place\Model\Label\Content;
use App\Place\Model\Label\Label;
use App\Place\Service\Label\Creator;

class Handler
{
	private Creator $creator;


	public function __construct(Creator $creator)
	{
		$this->creator = $creator;
	}


	public function handle(Command $command): Label
	{
		return $this->creator->create(
            new Uuid($command->getLabelTypeId()),
            new Color($command->getColor()),
            new Content($command->getContent()),
		);
	}
}
