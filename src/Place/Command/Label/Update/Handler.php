<?php

declare(strict_types=1);

namespace App\Place\Command\Label\Update;

use App\Application\ValueObject\ArrayValueObject;
use App\Application\ValueObject\Color;
use App\Application\ValueObject\StringValueObject;
use App\Application\ValueObject\Uuid;
use App\Place\Model\Label\Content;
use App\Place\Model\Label\Label;
use App\Place\Repository\LabelRepository;
use App\Place\Service\Label\Updater;

class Handler
{
	private Updater $updater;
	private LabelRepository $labelRepository;


	public function __construct(Updater $updater, LabelRepository $labelRepository)
	{
		$this->labelRepository = $labelRepository;
		$this->updater = $updater;
	}


	public function handle(Command $command): Label
	{
		$label = $this->labelRepository->get(new Uuid($command->getId()));

		return $this->updater->update(
			$label,
            new Uuid($command->getLabelTypeId()),
            new Color($command->getColor()),
            new Content($command->getContent()),
		);
	}
}
