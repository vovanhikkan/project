<?php

declare(strict_types=1);

namespace App\Place\Command\Label\Update;

use App\Application\ValueObject\Color;
use App\Application\ValueObject\Uuid;
use App\Place\Model\Label\Content;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    private string $id;

    private string $labelTypeId;

    private string $color;

    private array $content;

    public function __construct(
        string $id,
        string $labelTypeId,
        string $color,
        array $content
    ) {
        $this->id = $id;
        $this->labelTypeId = $labelTypeId;
        $this->color = $color;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabelTypeId(): string
    {
        return $this->labelTypeId;
    }

    /**
     * @param string $labelTypeId
     */
    public function setLabelTypeId(string $labelTypeId): void
    {
        $this->labelTypeId = $labelTypeId;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param array $content
     */
    public function setContent(array $content): void
    {
        $this->content = $content;
    }
}
