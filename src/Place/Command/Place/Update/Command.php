<?php

declare(strict_types=1);

namespace App\Place\Command\Place\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;

    /**
     * @Assert\NotBlank()
     */
    private array $title;

    /**
     * @Assert\NotBlank()
     */
    private array $description;

    private ?array $subTitle = null;

    private ?array $subDescription = null;

    /**
     * @Assert\NotBlank()
     */
    private array $workingHoursDescription;
    /**
     * @Assert\NotBlank()
     */
    private string $worksFrom;
    /**
     * @Assert\NotBlank()
     */
    private string $worksTo;
    /**
     * @Assert\NotBlank()
     */

    private ?array $ageRestriction = null;

    private ?array $label = null;

    private ?array $address = null;

    private ?string $phone = null;

    private ?string $code = null;

    private ?int $minPrice = null;

    private bool $isFree;

    private bool $isCableWayRequired;

    /**
     * @Assert\NotBlank()
     */
    private array $buttonText;

    private ?int $width = null;

    private array $sections;
    private array $categories;

    public function __construct(
        string $id,
        array $title,
        array $description,
        array $workingHoursDescription,
        string $worksFrom,
        string $worksTo,
        bool $isFree,
        bool $isCableWayRequired,
        array $buttonText,
        array $sections,
        array $categories
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->workingHoursDescription = $workingHoursDescription;
        $this->worksFrom = $worksFrom;
        $this->worksTo = $worksTo;
        $this->isFree = $isFree;
        $this->isCableWayRequired = $isCableWayRequired;
        $this->buttonText = $buttonText;
        $this->sections = $sections;
        $this->categories = $categories;
    }

    public function setSubTitle(?array $subTitle): void
    {
        $this->subTitle = $subTitle;
    }

    public function setSubDescription(?array $subDescription): void
    {
        $this->subDescription = $subDescription;
    }

    public function setAgeRestriction(?array $ageRestriction): void
    {
        $this->ageRestriction = $ageRestriction;
    }

    public function setLabel(?array $label): void
    {
        $this->label = $label;
    }

    public function setAddress(?array $address): void
    {
        $this->address = $address;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function setMinPrice(?int $minPrice): void
    {
        $this->minPrice = $minPrice;
    }

    public function setWidth(?int $width): void
    {
        $this->width = $width;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): array
    {
        return $this->title;
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function getSubTitle(): ?array
    {
        return $this->subTitle;
    }

    public function getSubDescription(): ?array
    {
        return $this->subDescription;
    }

    public function getWorkingHoursDescription(): array
    {
        return $this->workingHoursDescription;
    }

    /**
     * @return string
     */
    public function getWorksFrom(): string
    {
        return $this->worksFrom;
    }

    /**
     * @return string
     */
    public function getWorksTo(): string
    {
        return $this->worksTo;
    }

    public function getAgeRestriction(): ?array
    {
        return $this->ageRestriction;
    }

    public function getLabel(): ?array
    {
        return $this->label;
    }

    public function getAddress(): ?array
    {
        return $this->address;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getMinPrice(): ?int
    {
        return $this->minPrice;
    }

    public function isFree(): bool
    {
        return $this->isFree;
    }

    public function isCableWayRequired(): bool
    {
        return $this->isCableWayRequired;
    }

    public function getButtonText(): array
    {
        return $this->buttonText;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getSections(): array
    {
        return $this->sections;
    }
}
