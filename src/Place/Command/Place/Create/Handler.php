<?php

declare(strict_types=1);

namespace App\Place\Command\Place\Create;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Uuid;
use App\Place\Model\Place\Address;
use App\Place\Model\Place\AgeRestriction;
use App\Place\Model\Place\ButtonText;
use App\Place\Model\Place\Code;
use App\Place\Model\Place\Description;
use App\Place\Model\Place\Label;
use App\Place\Model\Place\Place;
use App\Place\Model\Place\SubDescription;
use App\Place\Model\Place\SubTitle;
use App\Place\Model\Place\Title;
use App\Place\Model\Place\Width;
use App\Place\Model\Place\WorkingHoursDescription;
use App\Place\Service\Place\Creator;
use App\Product\Repository\SectionRepository;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private SectionRepository $sectionRepository;
    private Creator $creator;

    public function __construct(
        SectionRepository $sectionRepository,
        Creator $creator
    ) {
        $this->sectionRepository = $sectionRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): Place
    {
        $sections = array_map(
            fn (string $sectionId) => $this->sectionRepository->get(new Uuid($sectionId)),
            $command->getSections()
        );

        return $this->creator->create(
            Uuid::generate(),
            new Title($command->getTitle()),
            new Description($command->getDescription()),
            $command->getSubTitle() !== null ? new SubTitle($command->getSubTitle()) : null,
            $command->getSubDescription() !== null ? new SubDescription($command->getSubDescription()) : null,
            new WorkingHoursDescription($command->getWorkingHoursDescription()),
            new DateTimeImmutable($command->getWorksFrom()),
            new DateTimeImmutable($command->getWorksTo()),
            $command->getAgeRestriction() !== null ? new AgeRestriction($command->getAgeRestriction()) : null,
            $command->getLabel() !== null ? new Label($command->getLabel()) : null,
            $command->getAddress() !== null ? new Address($command->getAddress()) : null,
            $command->getPhone() !== null ? new Phone($command->getPhone()) : null,
            $command->getCode() !== null ? new Code($command->getCode()) : null,
            $command->getMinPrice() !== null ? new Amount($command->getMinPrice()) : null,
            $command->isFree(),
            $command->isCableWayRequired(),
            new ButtonText($command->getButtonText()),
            $command->getWidth() !== null ? new Width($command->getWidth()) : null,
            null,
            $sections
        );
    }
}
