<?php

declare(strict_types=1);

namespace App\Place\Service\Place;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Place\Model\Place\Address;
use App\Place\Model\Place\AgeRestriction;
use App\Place\Model\Place\ButtonText;
use App\Place\Model\Place\Code;
use App\Place\Model\Place\Description;
use App\Place\Model\Place\Label;
use App\Place\Model\Place\Place;
use App\Place\Model\Place\Rating;
use App\Place\Model\Place\SubDescription;
use App\Place\Model\Place\SubTitle;
use App\Place\Model\Place\Title;
use App\Place\Model\Place\Width;
use App\Place\Model\Place\WorkingHoursDescription;
use App\Place\Repository\PlaceRepository;
use App\Storage\Model\File\File;
use DateTimeImmutable;

/**
 * Creator.
 */
class Creator
{
    private PlaceRepository $placeRepository;
    private Flusher $flusher;

    public function __construct(
        PlaceRepository $placeRepository,
        Flusher $flusher
    ) {
        $this->placeRepository = $placeRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Uuid $id,
        Title $title,
        Description $description,
        ?SubTitle $subTitle,
        ?SubDescription $subDescription,
        WorkingHoursDescription $workingHoursDescription,
        bool $isFullDay,
        DateTimeImmutable $worksFrom,
        DateTimeImmutable $worksTo,
        ?AgeRestriction $ageRestriction,
        ?Label $label,
        ?Address $address,
        ?Phone $phone,
        ?Code $code,
        ?Amount $minPrice,
        bool $isFree,
        bool $isCableWayRequired,
        ButtonText $buttonText,
        ?Width $width,
        ?File $background,
        array $sections
    ): Place {
        $sort = $this->placeRepository->getMaxSort();

        $place = new Place(
            $id,
            $title,
            $description,
            $subTitle,
            $subDescription,
            $workingHoursDescription,
            $isFullDay,
            $worksFrom,
            $worksTo,
            $ageRestriction,
            $label,
            $address,
            $phone,
            $code,
            $minPrice,
            $isFree,
            $isCableWayRequired,
            $buttonText,
            $width,
            $background,
            $sections,
            new Sort($sort)
        );

        $this->placeRepository->add($place);
        $this->flusher->flush();

        return $place;
    }
}
