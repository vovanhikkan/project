<?php

declare(strict_types=1);

namespace App\Place\Service\Place;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Place\Model\Place\Address;
use App\Place\Model\Place\AgeRestriction;
use App\Place\Model\Place\ButtonText;
use App\Place\Model\Place\Code;
use App\Place\Model\Place\Description;
use App\Place\Model\Place\Label;
use App\Place\Model\Place\Place;
use App\Place\Model\Place\Rating;
use App\Place\Model\Place\SubDescription;
use App\Place\Model\Place\SubTitle;
use App\Place\Model\Place\Title;
use App\Place\Model\Place\Width;
use App\Place\Model\Place\WorkingHours;
use App\Place\Repository\PlaceRepository;
use App\Storage\Model\File\File;

class Updater
{
    private Flusher $flusher;
    private PlaceRepository $placeRepository;


    public function __construct(PlaceRepository $placeRepository, Flusher $flusher)
    {
        $this->placeRepository = $placeRepository;
        $this->flusher = $flusher;
    }


    public function update(Place $place, Rating $rating): Place
    {
        $place->update($rating);
       
        $this->flusher->flush();
        return $place;
    }
}
