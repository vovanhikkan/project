<?php

declare(strict_types=1);

namespace App\Place\Service\Label;

use App\Application\ValueObject\Color;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Place\Model\Label\Content;
use App\Place\Model\Label\Label;
use App\Place\Repository\LabelRepository;

class Creator
{
	private Flusher $flusher;
	private LabelRepository $labelRepository;


	public function __construct(LabelRepository $labelRepository, Flusher $flusher)
	{
		$this->labelRepository = $labelRepository;
		$this->flusher = $flusher;
	}


	public function create(
        Uuid $labelTypeId,
        Color $color,
        Content $content
    ): Label {
		$label = new Label(
            Uuid::generate(),
            $labelTypeId,
            $color,
            $content
		);

		$this->labelRepository->add($label);
		$this->flusher->flush();

		return $label;
	}
}
