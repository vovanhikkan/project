<?php

declare(strict_types=1);

namespace App\Place\Service\Label;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Place\Repository\LabelRepository;

class Deleter
{
	private Flusher $flusher;
	private LabelRepository $labelRepository;


	public function __construct(LabelRepository $labelRepository, Flusher $flusher)
	{
		$this->labelRepository = $labelRepository;
		$this->flusher = $flusher;
	}


	public function delete(Uuid $id): void
	{
		$this->labelRepository->delete($id);
		$this->flusher->flush();
	}
}
