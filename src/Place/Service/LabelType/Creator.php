<?php

declare(strict_types=1);

namespace App\Place\Service\LabelType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Place\Model\LabelType\LabelType;
use App\Place\Repository\LabelTypeRepository;

class Creator
{
	private Flusher $flusher;
	private LabelTypeRepository $labelTypeRepository;


	public function __construct(LabelTypeRepository $labelTypeRepository, Flusher $flusher)
	{
		$this->labelTypeRepository = $labelTypeRepository;
		$this->flusher = $flusher;
	}


	public function create(Uuid $id): LabelType
	{
		$labelType = new LabelType(
			$id,
		);

		$this->labelTypeRepository->add($labelType);
		$this->flusher->flush();

		return $labelType;
	}
}
