<?php

declare(strict_types=1);

namespace App\Place\Service\LabelType;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Place\Repository\LabelTypeRepository;

class Deleter
{
	private Flusher $flusher;
	private LabelTypeRepository $labelTypeRepository;


	public function __construct(LabelTypeRepository $labelTypeRepository, Flusher $flusher)
	{
		$this->labelTypeRepository = $labelTypeRepository;
		$this->flusher = $flusher;
	}


	public function delete(Uuid $id): void
	{
		$this->labelTypeRepository->delete($id);
		$this->flusher->flush();
	}
}
