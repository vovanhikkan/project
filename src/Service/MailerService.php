<?php

namespace App\Service;

use Swift_Mailer;
use Swift_Message;

class MailerService
{
    private Swift_Mailer $mailer;

    /**
     * @var string|array
     */
    private $addresses;

    /**
     * @var string|null
     */
    private ?string $name;

    /**
     * MailerService constructor.
     *
     * @param Swift_Mailer $mailer
     * @param string|array $addresses
     * @param string|null  $name
     */
    public function __construct(Swift_Mailer $mailer, $addresses, string $name = null)
    {
        $this->mailer    = $mailer;
        $this->addresses = $addresses;
        $this->name      = $name;
    }

    /**
     * @param string       $subject
     * @param string|array $addresses
     * @param string       $body
     * @param string       $contentType
     *
     * @return int Количество успешных получателей. Может быть 0, что указывает на сбой
     */
    public function send(string $subject, $addresses, string $body, $contentType = 'text/html'): int
    {
        $message = new Swift_Message($subject);

        if(!is_array($addresses)) {
            $addresses = [$addresses];
        }

        $message->setFrom($this->addresses)
            ->setTo($addresses)
            ->setBody($body, $contentType);

        //$message->getHeaders()->addTextHeader('From', "<{$this->addresses}>");

        return $this->mailer->send($message);
    }
}