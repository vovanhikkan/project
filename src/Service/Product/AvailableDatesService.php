<?php
declare(strict_types=1);

namespace App\Service\Product;

use App\Product\Model\Product\Product;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;

class AvailableDatesService
{
    public function getAvailableDates(Product $product): array
    {
        $result = [];

        $prices =  $product->getProductPrices();
        foreach ($prices as $price) {
            $result = array_merge($result, $this->datePeriodGrid($price->getDateStart(), $price->getDateEnd()));
        }

        if (!count($quotas = $product->getProductQuotas())) {
            return array_unique($result);
        }

        $dates = [];
        foreach ($quotas as $quota) {
            $quotaValues = $quota->getQuotaValues();

            if (!$quota->isDependsOnDate()) {
                $quotaValue = $quotaValues[0];

                if ($quotaValue->getTotalQuantity()->getValue() === 0) {
                    return [];
                }

                return array_unique($result);
            }

            foreach ($quotaValues as $quotaValue) {
                if ($quota->getHasValuesByDays() && $quotaValue->getTotalQuantity()->getValue() === 0) {
                    $dates[] = $quotaValue->getDateFrom()->format('Y-m-d');
                } elseif ($quotaValue->getTotalQuantity()->getValue() === 0) {
                    $dates = array_merge(
                        $dates,
                        $this->datePeriodGrid($quotaValue->getDateFrom(), $quotaValue->getDateTo()->modify('+1 day'))
                    );
                }
            }
        }

        return array_unique(array_diff($result, $dates));
    }

    private function datePeriodGrid(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

        $result = [];
        foreach ($period as $date) {
            $result[] = $date->format('Y-m-d');
        }

        return $result;
    }
}
