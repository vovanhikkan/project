<?php
declare(strict_types=1);

namespace App\Service\Product;

use App\Auth\Model\User\User;
use App\Loyalty\Model\Action\Action;
use App\Loyalty\Repository\ActionRepository;
use App\Product\Model\Product\Product;
use App\Product\Repository\ProductRepository;

class PriceCalculator
{
    private ProductRepository $productRepository;
    private ActionRepository $actionRepository;

    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     * @param ActionRepository $actionRepository
     */
    public function __construct(ProductRepository $productRepository, ActionRepository $actionRepository)
    {
        $this->productRepository = $productRepository;
        $this->actionRepository = $actionRepository;
    }

    public function getPrice(?User $user, Product $product, string $activationDate = null): array
    {
        if (!$activationDate) {
            $activationDate = date('Y-m-d');
        }

        $result = [
            'calculatedPriceValue' => 0,
            "calculatedCashbackPointValue" => 0,
            "discountAbsoluteValue" => 0,
            "discountPercentageValue" => 0,
            "cashbackPointAbsoluteValue" => 0,
            "cashbackPointPercentageValue" => 0,
            'priceValueWithoutDiscount' => 0,
            'productId' => $product->getId()->getValue()
        ];

        $price = $this->productRepository->getProductPrice($product->getId()->getValue(), $activationDate);
        if ($price) {
            $priceValue = $price->getPrice()->toRoubles();
            $calculatedPriceValue = $priceValue;

            $actionValues = $this->actionRepository->getActionsByActivationDateAndLevelAndProduct(
                $activationDate,
                $user ? $user->getLevel() : null,
                $product
            );

            $result['priceValueWithoutDiscount'] = $priceValue;

            foreach ($actionValues as $action) {
                /**
                 * @var Action $action
                 */
                $result['discountAbsoluteValue'] += $action->getDiscountAbsoluteValue()->getValue();
                $result['discountPercentageValue'] += $action->getDiscountPercentageValue()->getValue();
                $result['cashbackPointAbsoluteValue'] += $action->getCashbackPointAbsoluteValue()->getValue();
                $result['cashbackPointPercentageValue'] += $action->getCashbackPointPercentageValue()->getValue();
            }

            $result['calculatedPriceValue'] = ($calculatedPriceValue - $result['discountAbsoluteValue'])
                - (($result['discountPercentageValue'] * $calculatedPriceValue) / 100);
            $result['calculatedCashbackPointValue'] = $result['cashbackPointAbsoluteValue']
                + (($result['cashbackPointPercentageValue'] * $calculatedPriceValue) / 100);
        }
        return $result;
    }
}
