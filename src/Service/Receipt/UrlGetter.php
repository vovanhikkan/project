<?php
declare(strict_types=1);

namespace App\Service\Receipt;

use App\Order\Model\Receipt\Receipt;
use App\Order\Service\Receipt\ProviderFactory;
use App\Order\Service\Receipt\Updater;

class UrlGetter
{
    private ProviderFactory $providerFactory;
    private Updater $updater;

    /**
     * UrlGetter constructor.
     * @param ProviderFactory $providerFactory
     * @param Updater $updater
     */
    public function __construct(ProviderFactory $providerFactory, Updater $updater)
    {
        $this->providerFactory = $providerFactory;
        $this->updater = $updater;
    }

    public function getExternalUrl(Receipt $receipt): ?string
    {

        if (!$externalUrl = $receipt->getExternalUrl()) {
            $provider = $this->providerFactory->get($receipt->getProvider());
            $statusDto = $provider->getStatus($receipt);
            $externalUrl = $statusDto->getUrl();

            if ($externalUrl) {
                $this->updater->updateExternalUrl($receipt, $externalUrl);
                return $externalUrl;
            }
        }

        return $externalUrl;
    }
}
