<?php

declare(strict_types=1);

namespace App\Product\Dto;

use App\Application\ValueObject\Uuid;
use App\Product\Model\Section\ActiveDays;
use App\Product\Model\Section\Controls;

/**
 * SectionSettingsDto.
 */
class SectionSettingsDto
{
    private Uuid $id;
    private Controls $controls;
    private bool $isOnlineOnly;
    private bool $isActivationDateRequired;
    private ActiveDays $activeDays;
    private bool $isNotQuantitative;

    /**
     * SectionSettingsDto constructor.
     * @param Uuid $id
     * @param Controls $controls
     * @param bool $isOnlineOnly
     * @param bool $isActivationDateRequired
     * @param ActiveDays $activeDays
     * @param bool $isNotQuantitative
     */
    public function __construct(
        Uuid $id,
        Controls $controls,
        bool $isOnlineOnly,
        bool $isActivationDateRequired,
        ActiveDays $activeDays,
        bool $isNotQuantitative
    ) {
        $this->id = $id;
        $this->controls = $controls;
        $this->isOnlineOnly = $isOnlineOnly;
        $this->isActivationDateRequired = $isActivationDateRequired;
        $this->activeDays = $activeDays;
        $this->isNotQuantitative = $isNotQuantitative;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Controls
     */
    public function getControls(): Controls
    {
        return $this->controls;
    }

    /**
     * @return bool
     */
    public function isOnlineOnly(): bool
    {
        return $this->isOnlineOnly;
    }

    /**
     * @return bool
     */
    public function isActivationDateRequired(): bool
    {
        return $this->isActivationDateRequired;
    }

    /**
     * @return ActiveDays
     */
    public function getActiveDays(): ActiveDays
    {
        return $this->activeDays;
    }

    /**
     * @return bool
     */
    public function isNotQuantitative(): bool
    {
        return $this->isNotQuantitative;
    }
}

