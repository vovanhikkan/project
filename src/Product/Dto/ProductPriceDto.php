<?php

declare(strict_types=1);

namespace App\Product\Dto;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use DateTimeImmutable;

/**
 * ProductPriceDto.
 */
class ProductPriceDto
{
    private ?Uuid $id = null;
    private Amount $amount;
    private Id $ppsPeriod;
    private DateTimeImmutable $dateStart;
    private DateTimeImmutable $dateEnd;

    /**
     * ProductPriceDto constructor.
     * @param Amount $amount
     * @param Id $ppsPeriod
     * @param DateTimeImmutable $dateStart
     * @param DateTimeImmutable $dateEnd
     */
    public function __construct(Amount $amount, Id $ppsPeriod, DateTimeImmutable $dateStart, DateTimeImmutable $dateEnd)
    {
        $this->amount = $amount;
        $this->ppsPeriod = $ppsPeriod;
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
    }


    public function setId(Uuid $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getDateStart(): DateTimeImmutable
    {
        return $this->dateStart;
    }

    public function getDateEnd(): DateTimeImmutable
    {
        return $this->dateEnd;
    }

    /**
     * @return Id
     */
    public function getPpsPeriod(): Id
    {
        return $this->ppsPeriod;
    }
}
