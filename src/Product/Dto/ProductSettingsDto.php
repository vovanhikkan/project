<?php

declare(strict_types=1);

namespace App\Product\Dto;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\Age;
use App\Product\Model\Product\AmountOfDays;
use App\Product\Model\Product\CardType;
use App\Product\Model\Product\Rounds;
use DateTimeImmutable;

/**
 * ProductSettingsDto.
 */
class ProductSettingsDto
{
    private ?Uuid $id = null;
    private CardType $cardType;
    private Age $age;
    private AmountOfDays $amountOfDays;
    private Rounds $rounds;
    private Id $ppsProductId;
    private Id $newCardPpsProductId;
    private Id $oldCardPpsProductId;

    /**
     * ProductSettingsDto constructor.
     * @param Uuid|null $id
     * @param CardType $cardType
     * @param Age $age
     * @param AmountOfDays $amountOfDays
     * @param Rounds $rounds
     * @param Id $ppsProductId
     * @param Id $newCardPpsProductId
     * @param Id $oldCardPpsProductId
     */
    public function __construct(?Uuid $id, CardType $cardType, Age $age, AmountOfDays $amountOfDays, Rounds $rounds, Id $ppsProductId, Id $newCardPpsProductId, Id $oldCardPpsProductId)
    {
        $this->id = $id;
        $this->cardType = $cardType;
        $this->age = $age;
        $this->amountOfDays = $amountOfDays;
        $this->rounds = $rounds;
        $this->ppsProductId = $ppsProductId;
        $this->newCardPpsProductId = $newCardPpsProductId;
        $this->oldCardPpsProductId = $oldCardPpsProductId;
    }

    /**
     * @return Uuid|null
     */
    public function getId(): ?Uuid
    {
        return $this->id;
    }

    /**
     * @return CardType
     */
    public function getCardType(): CardType
    {
        return $this->cardType;
    }

    /**
     * @return Age
     */
    public function getAge(): Age
    {
        return $this->age;
    }

    /**
     * @return AmountOfDays
     */
    public function getAmountOfDays(): AmountOfDays
    {
        return $this->amountOfDays;
    }

    /**
     * @return Rounds
     */
    public function getRounds(): Rounds
    {
        return $this->rounds;
    }

    /**
     * @return Id
     */
    public function getPpsProductId(): Id
    {
        return $this->ppsProductId;
    }

    /**
     * @return Id
     */
    public function getNewCardPpsProductId(): Id
    {
        return $this->newCardPpsProductId;
    }

    /**
     * @return Id
     */
    public function getOldCardPpsProductId(): Id
    {
        return $this->oldCardPpsProductId;
    }

}
