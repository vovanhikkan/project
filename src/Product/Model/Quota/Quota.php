<?php

declare(strict_types=1);

namespace App\Product\Model\Quota;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\Product;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\Product\Model\QuotaValue\QuotaValue;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Quota.
 *
 * @ORM\Entity()
 * @ORM\Table(name="quota")
 */
class Quota
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;
    /**
     * @ORM\ManyToOne(targetEntity="App\Product\Model\QuotaGroup\QuotaGroup", inversedBy="quota_group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private QuotaGroup $group;
    /**
     * @ORM\Column(type="product_quota_name")
     */
    private Name $name;
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isDependsOnDate;
    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $hasValuesByDays = null;
    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Product\Model\QuotaValue\QuotaValue", mappedBy="quota")
     */
    private Collection $quotaValues;

    /**
     * @ORM\ManyToMany(targetEntity="App\Product\Model\Product\Product", inversedBy="quotas")
     */
    private Collection $products;

    /**
     * Quota constructor.
     * @param Uuid $id
     * @param QuotaGroup $group
     * @param Name $name
     * @param bool $isDependsOnDate
     * @param Sort $sort
     * @param bool $isActive
     */
    public function __construct(
        Uuid $id,
        QuotaGroup $group,
        Name $name,
        bool $isDependsOnDate,
        Sort $sort,
        bool $isActive
    ) {
        $this->id = $id;
        $this->group = $group;
        $this->name = $name;
        $this->isDependsOnDate = $isDependsOnDate;
        $this->sort = $sort;
        $this->isActive = $isActive;
        $this->products = new ArrayCollection();
    }

    /**
     * @param bool|null $hasValuesByDays
     */
    public function setHasValuesByDays(?bool $hasValuesByDays): void
    {
        $this->hasValuesByDays = $hasValuesByDays;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return QuotaGroup
     */
    public function getGroup(): QuotaGroup
    {
        return $this->group;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isDependsOnDate(): bool
    {
        return $this->isDependsOnDate;
    }

    /**
     * @return bool|null
     */
    public function getHasValuesByDays(): ?bool
    {
        return $this->hasValuesByDays;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return QuotaValue[]
     */
    public function getQuotaValues(): array
    {
        return $this->quotaValues->toArray();
    }

    /**
     * @param string $activationDate
     * @return QuotaValue[]
     */
    public function getQuotaValuesByActivationDate(string $activationDate): array
    {
        return $this->quotaValues->filter(function (QuotaValue $quotaValue) use ($activationDate) {
            return $quotaValue->getDateFrom()->getTimestamp() <= strtotime($activationDate) &&
                $quotaValue->getDateTo()->getTimestamp() >= strtotime($activationDate);
        })->toArray();
    }

    public function addProduct(Product $product)
    {
        $this->products->add($product);
    }

    public function getProducts(): array
    {
        return $this->products->toArray();
    }

    public function update(?QuotaGroup $group, ?Name $name, ?bool $isDependsOnDate, ?bool $hasValuesByDays, ?array $products)
    {
        $this->products = new ArrayCollection();
        if ($products) {
            foreach ($products as $product) {
                $this->products->add($product);
            }
        }
        if ($group) {
            $this->group = $group;
        }
        if ($name) {
            $this->name = $name;
        }
        if ($isDependsOnDate !== null) {
            $this->isDependsOnDate = $isDependsOnDate;
        }
        if ($hasValuesByDays !== null) {
            $this->hasValuesByDays = $hasValuesByDays;
        }
    }
}
