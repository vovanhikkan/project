<?php

declare(strict_types=1);

namespace App\Product\Model\QuotaGroup;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * QuotaGroup.
 *
 * @ORM\Entity()
 * @ORM\Table(name="quota_group")
 */
class QuotaGroup
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;
    /**
     * @ORM\Column(type="product_quota_group_name")
     */
    private Name $name;
    /**
     * @ORM\Column(type="sort")
     */
    private Sort $sort;
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * QuotaGroup constructor.
     * @param Uuid $id
     * @param Name $name
     * @param Sort $sort
     * @param bool $isActive
     */
    public function __construct(Uuid $id, Name $name, Sort $sort, bool $isActive)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sort = $sort;
        $this->isActive = $isActive;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return $this->sort;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function update(Name $name)
    {
      $this->name = $name;
    }
}
