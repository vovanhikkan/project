<?php

declare(strict_types=1);

namespace App\Product\Model\CardType;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CardType.
 *
 * @ORM\Entity()
 * @ORM\Table(name="card_type")
 */
class CardType
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="product_card_type_type", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Type $type;

    /**
     * @ORM\Column(type="amount", columnDefinition="BIGINT(20) UNSIGNED DEFAULT NULL")
     */
    private ?Amount $price = null;

    /**
     * @ORM\Column(type="id")
     */
    private Id $ppsProductId;

    public function __construct(Uuid $id, Type $type, Id $ppsProductId)
    {
        $this->id = $id;
        $this->type = $type;
        $this->ppsProductId = $ppsProductId;
        $this->createdAt = new DateTimeImmutable();
    }

    public function changePrice(Amount $price): void
    {
        $this->price = $price;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getPrice(): ?Amount
    {
        return $this->price;
    }

    public function getPpsProductId(): Id
    {
        return $this->ppsProductId;
    }
}
