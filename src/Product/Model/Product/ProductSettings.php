<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProductSettings.
 *
 * @ORM\Entity()
 * @ORM\Table(name="product_settings")
 */
class ProductSettings
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\OneToOne(targetEntity="Product", inversedBy="productSettings")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private Product $product;

    /**
     * @ORM\Column(type="product_product_settings_card_type")
     */
    private CardType $cardType;

    /**
     * @ORM\Column(type="product_product_settings_age")
     */
    private Age $age;

    /**
     * @ORM\Column(type="product_product_settings_amount_of_days")
     */
    private AmountOfDays $amountOfDays;

    /**
     * @ORM\Column(type="product_product_settings_rounds")
     */
    private Rounds $rounds;

    /**
     * @ORM\Column(type="id")
     */
    private Id $ppsProductId;

    /**
     * @ORM\Column(type="id")
     */
    private Id $newCardPpsProductId;

    /**
     * @ORM\Column(type="id")
     */
    private Id $oldCardPpsProductId;

    public function __construct(
        Uuid $id,
        Product $product,
        CardType $cardType,
        Age $age,
        AmountOfDays $amountOfDays,
        Rounds $rounds,
        Id $ppsProductId,
        Id $newCardPpsProductId,
        Id $oldCardPpsProductId
    ) {
        $this->id = $id;
        $this->product = $product;
        $this->cardType = $cardType;
        $this->age = $age;
        $this->amountOfDays = $amountOfDays;
        $this->rounds = $rounds;
        $this->ppsProductId = $ppsProductId;
        $this->newCardPpsProductId = $newCardPpsProductId;
        $this->oldCardPpsProductId = $oldCardPpsProductId;
        $this->createdAt = new DateTimeImmutable();
    }

    public function update(
        CardType $cardType,
        Age $age,
        AmountOfDays $amountOfDays,
        Rounds $rounds,
        Id $ppsProductId,
        Id $newCardPpsProductId,
        Id $oldCardPpsProductId
    ): void {
        $this->cardType = $cardType;
        $this->age = $age;
        $this->amountOfDays = $amountOfDays;
        $this->rounds = $rounds;
        $this->ppsProductId = $ppsProductId;
        $this->newCardPpsProductId = $newCardPpsProductId;
        $this->oldCardPpsProductId = $oldCardPpsProductId;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getCardType(): CardType
    {
        return $this->cardType;
    }

    public function getAge(): Age
    {
        return $this->age;
    }

    public function getAmountOfDays(): AmountOfDays
    {
        return $this->amountOfDays;
    }

    public function getRounds(): Rounds
    {
        return $this->rounds;
    }

    public function getPpsProductId(): Id
    {
        return $this->ppsProductId;
    }

    public function getNewCardPpsProductId(): Id
    {
        return $this->newCardPpsProductId;
    }

    public function getOldCardPpsProductId(): Id
    {
        return $this->oldCardPpsProductId;
    }
}
