<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Section\Section;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Product\Model\Quota\Quota;

/**
 * Product.
 *
 * @ORM\Entity()
 * @ORM\Table(name="product")
 */
class Product
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="product_product_type", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Type $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Product\Model\Section\Section")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id", nullable=false)
     */
    private Section $section;

    /**
     * @ORM\Column(type="product_product_name")
     */
    private Name $name;

//    /**
//     * @ORM\Column(type="amount")
//     */
//    private Amount $price;

    /**
     * @ORM\OneToOne(targetEntity="ProductSettings", mappedBy="product", cascade={"all"})
     */
    private ?ProductSettings $productSettings = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Product\Model\Quota\Quota", mappedBy="products")
     * @ORM\JoinTable(name="quota_product")
     */
    private Collection $quotas;

    /**
     * @ORM\OneToMany(targetEntity="ProductPrice", mappedBy="product", cascade={"all"})
     */
    private Collection $productPrices;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    public function __construct(Uuid $id, Section $section, Name $name, Amount $price, bool $isActive)
    {
        $this->id = $id;
        $this->section = $section;
        $this->name = $name;
//        $this->price = $price;
        $this->isActive = $isActive;
        $this->productPrices = new ArrayCollection();
        $this->quotas = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
    }

    public function addProductPrice(
        Uuid $id,
        Amount $amount,
        DateTimeImmutable $dateStart,
        DateTimeImmutable $dateEnd,
        bool $isActive,
        Id $ppsPeriodId
    ): ProductPrice {
        $productPrice = new ProductPrice($id, $this, $amount, $dateStart, $dateEnd, $isActive, $ppsPeriodId);
        $this->productPrices->add($productPrice);

        return $productPrice;
    }

    public function addProductSettings(
        Uuid $id,
        Product $product,
        CardType $cardType,
        Age $age,
        AmountOfDays $amountOfDays,
        Rounds $rounds,
        Id $ppsProductId,
        Id $newCardPpsProductId,
        Id $oldCardPpsProductId
    ): ProductSettings {
        $productSettings = new ProductSettings(
            $id,
            $product,
            $cardType,
            $age,
            $amountOfDays,
            $rounds,
            $ppsProductId,
            $newCardPpsProductId,
            $oldCardPpsProductId
        );
        $this->productSettings = $productSettings;

        return $productSettings;
    }

    public function removeProductPrice(ProductPrice $productPrice): void
    {
        $this->productPrices->removeElement($productPrice);
    }

    public function changeName(Name $name): void
    {
        $this->name = $name;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getSection(): Section
    {
        return $this->section;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getPrice(): Amount
    {
        return new Amount(0);
    }

    public function getProductSettings(): ?ProductSettings
    {
        return $this->productSettings;
    }

    /**
     * @return ProductPrice[]
     */
    public function getProductPrices(): array
    {
        return $this->productPrices->toArray();
    }

    /**
     * @return Quota[]
     */
    public function getProductQuotas(): array
    {
        return $this->quotas->toArray();
    }

    public function update(Section $section, Name $name, Amount $price)
    {
        $this->section = $section;
        $this->name = $name;
        $this->price = $price;
    }
    
    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }
}
