<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\ValueObject\StringValueObject;

/**
 * AmountOfDays.
 */
final class AmountOfDays extends StringValueObject
{
}
