<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\ValueObject\StringValueObject;

/**
 * Name.
 */
final class Name extends StringValueObject
{
}
