<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\ValueObject\StringValueObject;

/**
 * Age.
 */
final class Age extends StringValueObject
{
}
