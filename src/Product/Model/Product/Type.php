<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\ValueObject\EnumValueObject;

/**
 * Type.
 */
final class Type extends EnumValueObject
{
    public const NONE = 0;
    public const SKI_PASS = 100;
    public const YETI_PARK = 200;
    public const RODELBAN = 300;
    public const MUSEUM = 400;

    public function isSkiPass(): bool
    {
        return $this->value === self::SKI_PASS;
    }

    public function isYetiPark(): bool
    {
        return $this->value === self::YETI_PARK;
    }

    public function isRodelban(): bool
    {
        return $this->value === self::RODELBAN;
    }

    public function isMuseum(): bool
    {
        return $this->value === self::MUSEUM;
    }
}
