<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\ValueObject\IntegerValueObject;

/**
 * Rounds.
 */
final class Rounds extends IntegerValueObject
{
}
