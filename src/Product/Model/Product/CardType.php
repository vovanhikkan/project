<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\ValueObject\EnumValueObject;

/**
 * CardType.
 *
 * @deprecated
 */
class CardType extends EnumValueObject
{
    const NONE = 100;
    const PLASTIC = 200;
    const PLASTIC_FREE = 300;

    public function isNone(): bool
    {
        return $this->value === self::NONE;
    }

    public function isPlastic(): bool
    {
        return $this->value === self::PLASTIC;
    }

    public function isPlasticFree(): bool
    {
        return $this->value === self::PLASTIC_FREE;
    }
}
