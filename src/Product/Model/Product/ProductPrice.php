<?php

declare(strict_types=1);

namespace App\Product\Model\Product;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProductPrice.
 *
 * @ORM\Entity()
 * @ORM\Table(name="product_price")
 */
class ProductPrice
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="productPrices")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private Product $product;

    /**
     * @ORM\Column(type="amount")
     */
    private Amount $price;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $dateStart;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $dateEnd;

    /**
     * @ORM\Column(type="boolean", columnDefinition="TINYINT(1) UNSIGNED NOT NULL")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="id")
     */
    private Id $ppsPeriodId;

    public function __construct(
        Uuid $id,
        Product $product,
        Amount $price,
        DateTimeImmutable $dateStart,
        DateTimeImmutable $dateEnd,
        bool $isActive,
        Id $ppsPeriodId
    ) {
        $this->id = $id;
        $this->product = $product;
        $this->price = $price;
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
        $this->isActive = $isActive;
        $this->ppsPeriodId = $ppsPeriodId;
        $this->createdAt = new DateTimeImmutable();
    }

    public function update(
        Amount $price,
        DateTimeImmutable $dateStart,
        DateTimeImmutable $dateEnd,
        bool $isActive,
        Id $ppsPeriod
    ): void {
        $this->price = $price;
        $this->ppsPeriodId = $ppsPeriod;
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
        $this->isActive = $isActive;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getPrice(): Amount
    {
        return $this->price;
    }

    public function getDateStart(): DateTimeImmutable
    {
        return $this->dateStart;
    }

    public function getDateEnd(): DateTimeImmutable
    {
        return $this->dateEnd;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getPpsPeriodId(): Id
    {
        return $this->ppsPeriodId;
    }
}
