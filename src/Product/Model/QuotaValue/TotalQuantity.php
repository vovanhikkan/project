<?php

declare(strict_types=1);

namespace App\Product\Model\QuotaValue;

use App\Application\ValueObject\IntegerValueObject;

/**
 * TotalQuantity.
 */
final class TotalQuantity extends IntegerValueObject
{
}
