<?php

declare(strict_types=1);

namespace App\Product\Model\QuotaValue;

use App\Application\ValueObject\IntegerValueObject;

/**
 * HoldedQuantity.
 */
final class HoldedQuantity extends IntegerValueObject
{
}
