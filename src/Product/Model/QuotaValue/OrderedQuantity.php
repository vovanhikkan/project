<?php

declare(strict_types=1);

namespace App\Product\Model\QuotaValue;

use App\Application\ValueObject\IntegerValueObject;

/**
 * OrderedQuantity.
 */
final class OrderedQuantity extends IntegerValueObject
{
}
