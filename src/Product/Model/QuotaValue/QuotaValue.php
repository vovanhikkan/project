<?php

declare(strict_types=1);

namespace App\Product\Model\QuotaValue;

use App\Application\ValueObject\Quantity;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Quota\Quota;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * QuotaValue.
 *
 * @ORM\Entity()
 * @ORM\Table(name="quota_value")
 */
class QuotaValue
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;
    /**
     * @ORM\ManyToOne(targetEntity="App\Product\Model\Quota\Quota")
     * @ORM\JoinColumn(name="quota_id", referencedColumnName="id", nullable=false)
     */
    private Quota $quota;
    /**
     * @ORM\Column(type="product_quota_value_total_quantity")
     */
    private TotalQuantity $totalQuantity;
    /**
     * @ORM\Column(type="product_quota_value_holded_quantity")
     */
    private HoldedQuantity $holdedQuantity;
    /**
     * @ORM\Column(type="product_quota_value_ordered_quantity")
     */
    private OrderedQuantity $orderedQuantity;
    /**
     * @ORM\Column(type="date_immutable")
     */
    private ?DateTimeImmutable $dateFrom = null;
    /**
     * @ORM\Column(type="date_immutable")
     */
    private ?DateTimeImmutable $dateTo = null;

    /**
     * QuotaValue constructor.
     * @param Uuid $id
     * @param Quota $quota
     * @param TotalQuantity $totalQuantity
     * @param HoldedQuantity $holdedQuantity
     * @param OrderedQuantity $orderedQuantity
     */
    public function __construct(
        Uuid $id,
        Quota $quota,
        TotalQuantity $totalQuantity,
        HoldedQuantity $holdedQuantity,
        OrderedQuantity $orderedQuantity
    ) {
        $this->id = $id;
        $this->quota = $quota;
        $this->totalQuantity = $totalQuantity;
        $this->holdedQuantity = $holdedQuantity;
        $this->orderedQuantity = $orderedQuantity;
    }

    /**
     * @param DateTimeImmutable|null $dateFrom
     */
    public function setDateFrom(?DateTimeImmutable $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @param DateTimeImmutable|null $dateTo
     */
    public function setDateTo(?DateTimeImmutable $dateTo): void
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Quota
     */
    public function getQuota(): Quota
    {
        return $this->quota;
    }

    /**
     * @return TotalQuantity
     */
    public function getTotalQuantity(): TotalQuantity
    {
        return $this->totalQuantity;
    }

    /**
     * @return HoldedQuantity
     */
    public function getHoldedQuantity(): HoldedQuantity
    {
        return $this->holdedQuantity;
    }

    /**
     * @return OrderedQuantity
     */
    public function getOrderedQuantity(): OrderedQuantity
    {
        return $this->orderedQuantity;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateFrom(): ?DateTimeImmutable
    {
        return $this->dateFrom;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDateTo(): ?DateTimeImmutable
    {
        return $this->dateTo;
    }

    public function updateHoldedQuantity(Quantity $quantity): void
    {
        $this->holdedQuantity = new HoldedQuantity($this->holdedQuantity->getValue() + $quantity->getValue());
    }

    public function updateOrderedQuantity(Quantity $quantity): void
    {
        $this->updateHoldedQuantity(new Quantity(-1 * $quantity->getValue()));
        $this->orderedQuantity = new OrderedQuantity($this->orderedQuantity->getValue() + $quantity->getValue());
    }

    public function update(?Quota $quota, ?int $totalQuantity, ?int $holdedQuantity, ?int $orderedQuantity, ?DateTimeImmutable $dateFrom, ?DateTimeImmutable $dateTo)
    {
        if ($quota) {
            $this->quota = $quota;
        }
        if ($totalQuantity) {
            $this->totalQuantity = new TotalQuantity($totalQuantity);
        }
        if ($orderedQuantity) {
            $this->orderedQuantity = new OrderedQuantity($orderedQuantity);
        }
        if ($holdedQuantity) {
            $this->holdedQuantity = new HoldedQuantity($holdedQuantity);
        }

        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }
}
