<?php

declare(strict_types=1);

namespace App\Product\Model\Section;

use App\Application\ValueObject\ArrayValueObject;

/**
 * Name.
 */
final class Name extends ArrayValueObject
{
}
