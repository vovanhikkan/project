<?php

declare(strict_types=1);

namespace App\Product\Model\Section;

use App\Application\ValueObject\StringValueObject;

/**
 * Code.
 */
final class Code extends StringValueObject
{
}
