<?php

declare(strict_types=1);

namespace App\Product\Model\Section;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Storage\Model\File\File;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Section.
 *
 * @ORM\Entity()
 * @ORM\Table(name="section")
 */
class Section
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="product_section_code")
     */
    private Code $code;

    /**
     * @ORM\Column(type="product_section_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="product_section_description")
     */
    private Description $description;

    /**
     * @ORM\Column(type="product_section_tariff_description")
     */
    private TariffDescription $tariffDescription;

    /**
     * @ORM\Column(type="product_section_inner_title")
     */
    private InnerTitle $innerTitle;

    /**
     * @ORM\Column(type="product_section_inner_description")
     */
    private InnerDescription $innerDescription;

    /**
     * @ORM\OneToOne(targetEntity="SectionSettings", mappedBy="section", cascade={"all"})
     */
    private ?SectionSettings $sectionSettings = null;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Storage\Model\File\File", inversedBy="section", cascade={"all"})
     */
    private ?File $picture = null;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Storage\Model\File\File", inversedBy="section", cascade={"all"})
     */
    private ?File $webIcon = null;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Storage\Model\File\File", inversedBy="section", cascade={"all"})
     */
    private ?File $webBackground = null;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Storage\Model\File\File", inversedBy="section", cascade={"all"})
     */
    private ?File $mobileIcon = null;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Storage\Model\File\File", inversedBy="section", cascade={"all"})
     */
    private ?File $mobileBackground = null;

    /**
     * @ORM\Column(type="sort")
     */
    private Sort $recommendationSort;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="App\Content\Model\Category\Category")
     * @ORM\JoinTable(
     *     name="category_section",
     *     joinColumns={@ORM\JoinColumn(name="section_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private Collection $categories;

    public function __construct(
        Uuid $id,
        Code $code,
        Name $name,
        Description $description,
        TariffDescription $tariffDescription,
        InnerTitle $innerTitle,
        InnerDescription $innerDescription,
        Sort $recommendationSort,
        bool $isActive
    ) {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->description = $description;
        $this->tariffDescription = $tariffDescription;
        $this->innerTitle = $innerTitle;
        $this->innerDescription = $innerDescription;
        $this->createdAt = new DateTimeImmutable();
        $this->recommendationSort = $recommendationSort;
        $this->isActive = $isActive;
        $this->categories = new ArrayCollection();
    }

    public function update(
        Code $code,
        Name $name,
        Description $description,
        TariffDescription $tariffDescription,
        InnerTitle $innerTitle,
        InnerDescription $innerDescription,
        ?File $picture,
        ?File $webIcon,
        ?File $webBackground,
        ?File $mobIcon,
        ?File $mobBackground
    ) {
        $this->code = $code;
        $this->name = $name;
        $this->description = $description;
        $this->tariffDescription = $tariffDescription;
        $this->innerTitle = $innerTitle;
        $this->innerDescription = $innerDescription;
        $this->updatedAt = new DateTimeImmutable();
        if ($picture) {
            $this->picture = $picture;
        }
        if ($webIcon) {
            $this->webIcon = $webIcon;
        }
        if ($webBackground) {
            $this->webBackground = $webBackground;
        }
        if ($mobIcon) {
            $this->mobileIcon = $mobIcon;
        }
        if ($mobBackground) {
            $this->mobileBackground = $mobBackground;
        }
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getCode(): Code
    {
        return $this->code;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getDescription(): Description
    {
        return $this->description;
    }

    public function getTariffDescription(): TariffDescription
    {
        return $this->tariffDescription;
    }

    /**
     * @return File|null
     */
    public function getPicture(): ?File
    {
        return $this->picture;
    }

    /**
     * @return File|null
     */
    public function getWebIcon(): ?File
    {
        return $this->webIcon;
    }

    /**
     * @return File|null
     */
    public function getWebBackground(): ?File
    {
        return $this->webBackground;
    }

    /**
     * @return File|null
     */
    public function getMobileIcon(): ?File
    {
        return $this->mobileIcon;
    }

    /**
     * @return File|null
     */
    public function getMobileBackground(): ?File
    {
        return $this->mobileBackground;
    }

    /**
     * @return Sort
     */
    public function getRecommendationSort(): Sort
    {
        return $this->recommendationSort;
    }

    public function getInnerTitle(): InnerTitle
    {
        return $this->innerTitle;
    }

    public function getInnerDescription(): InnerDescription
    {
        return $this->innerDescription;
    }

    public function getSectionSettings(): ?SectionSettings
    {
        return $this->sectionSettings;
    }

    /**
     * @param SectionSettings|null $sectionSettings
     */
    public function setSectionSettings(?SectionSettings $sectionSettings): void
    {
        $this->sectionSettings = $sectionSettings;
    }

    /**
     * @param File|null $picture
     */
    public function setPicture(?File $picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @param File|null $webIcon
     */
    public function setWebIcon(?File $webIcon): void
    {
        $this->webIcon = $webIcon;
    }

    /**
     * @param File|null $webBackground
     */
    public function setWebBackground(?File $webBackground): void
    {
        $this->webBackground = $webBackground;
    }

    /**
     * @param File|null $mobileIcon
     */
    public function setMobileIcon(?File $mobileIcon): void
    {
        $this->mobileIcon = $mobileIcon;
    }

    /**
     * @param File|null $mobileBackground
     */
    public function setMobileBackground(?File $mobileBackground): void
    {
        $this->mobileBackground = $mobileBackground;
    }
    
    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return array
     */
    public function getCategories() : array
    {
        return $this->categories->toArray();
    }
}
