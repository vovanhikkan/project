<?php

declare(strict_types=1);

namespace App\Product\Model\Section;

use App\Application\ValueObject\IntegerValueObject;

/**
 * ActiveDays.
 */
final class ActiveDays extends IntegerValueObject
{
}
