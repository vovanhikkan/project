<?php

declare(strict_types=1);

namespace App\Product\Model\Section;

use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Uuid;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * SectionSettings.
 *
 * @ORM\Entity()
 * @ORM\Table(name="section_settings")
 */
class SectionSettings
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\OneToOne(targetEntity="Section", inversedBy="sectionSettings")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id", nullable=false)
     */
    private Section $section;

    /**
     * @ORM\Column(type="product_section_settings_controls")
     */
    private Controls $controls;

    /**
     * @ORM\Column(type="boolean", columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 0")
     */
    private bool $isOnlineOnly;

    /**
     * @ORM\Column(type="boolean", columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 0")
     */
    private bool $isActivationDateRequired;

    /**
     * @ORM\Column(type="product_section_settings_active_days")
     */
    private ActiveDays $activeDays;

    /**
     * @ORM\Column(type="boolean", columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 0")
     */
    private bool $isNotQuantitative;

    public function __construct(
        Uuid $id,
        Section $section,
        Controls $controls,
        bool $isOnlineOnly,
        bool $isActivationDateRequired,
        ActiveDays $activeDays,
        bool $isNotQuantitative
    ) {
        $this->id = $id;
        $this->section = $section;
        $this->controls = $controls;
        $this->isOnlineOnly = $isOnlineOnly;
        $this->isActivationDateRequired = $isActivationDateRequired;
        $this->activeDays = $activeDays;
        $this->isNotQuantitative = $isNotQuantitative;
        $this->createdAt = new DateTimeImmutable();
    }

    public function update(
        Controls $controls,
        bool $isOnlineOnly,
        bool $isActivationDateRequired,
        ActiveDays $activeDays,
        bool $isNotQuantitative
    ): void {
        $this->controls = $controls;
        $this->isOnlineOnly = $isOnlineOnly;
        $this->isActivationDateRequired = $isActivationDateRequired;
        $this->activeDays = $activeDays;
        $this->isNotQuantitative = $isNotQuantitative;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getSection(): Section
    {
        return $this->section;
    }

    public function getControls(): Controls
    {
        return $this->controls;
    }

    public function isOnlineOnly(): bool
    {
        return $this->isOnlineOnly;
    }

    public function isActivationDateRequired(): bool
    {
        return $this->isActivationDateRequired;
    }

    public function getActiveDays(): ActiveDays
    {
        return $this->activeDays;
    }

    public function isNotQuantitative(): bool
    {
        return $this->isNotQuantitative;
    }
}
