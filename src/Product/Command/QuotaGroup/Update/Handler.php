<?php

declare(strict_types=1);

namespace App\Product\Command\QuotaGroup\Update;

use App\Application\ValueObject\Uuid;
use App\Product\Model\QuotaGroup\Name;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\Product\Service\QuotaGroup\Updater;
use App\Product\Repository\QuotaGroupRepository;

/**
 * Handler.
 */
class Handler
{
    private QuotaGroupRepository $quotaGroupRepository;
    private Updater $updater;

    public function __construct(
        QuotaGroupRepository $quotaGroupRepository,
        Updater $updater
    ) {
        $this->quotaGroupRepository = $quotaGroupRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): QuotaGroup
    {
        $quotaGroup = $this->quotaGroupRepository->get(new Uuid($command->getId()));

        $this->updater->update(
            $quotaGroup,
            new Name($command->getName())
        );

        return $quotaGroup;
    }
}
