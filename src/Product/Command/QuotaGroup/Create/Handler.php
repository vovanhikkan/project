<?php

declare(strict_types=1);

namespace App\Product\Command\QuotaGroup\Create;

use App\Product\Model\QuotaGroup\Name;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\Product\Service\QuotaGroup\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;

    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }

    public function handle(Command $command): QuotaGroup
    {
        return $this->creator->create(
            new Name($command->getName())
        );
    }
}
