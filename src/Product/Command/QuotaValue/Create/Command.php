<?php

declare(strict_types=1);

namespace App\Product\Command\QuotaValue\Create;

use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $quota;

    /**
     * @Assert\NotBlank()
     */
    private int $totalQuantity;

    /**
     * @Assert\NotBlank()
     */
    private int $holdedQuantity;

    /**
     * @Assert\NotBlank()
     */
    private int $orderedQuantity;

    private ?string $dateFrom;
    private ?string $dateTo;

    /**
     * Command constructor.
     * @param string $quota
     * @param int $totalQuantity
     * @param int $holdedQuantity
     * @param int $orderedQuantity
     * @param string|null $dateFrom
     * @param string|null $dateTo
     */
    public function __construct(string $quota, int $totalQuantity, int $holdedQuantity, int $orderedQuantity, ?string $dateFrom, ?string $dateTo)
    {
        $this->quota = $quota;
        $this->totalQuantity = $totalQuantity;
        $this->holdedQuantity = $holdedQuantity;
        $this->orderedQuantity = $orderedQuantity;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    /**
     * @return string
     */
    public function getQuota(): string
    {
        return $this->quota;
    }

    /**
     * @return int
     */
    public function getTotalQuantity(): int
    {
        return $this->totalQuantity;
    }

    /**
     * @return int
     */
    public function getHoldedQuantity(): int
    {
        return $this->holdedQuantity;
    }

    /**
     * @return int
     */
    public function getOrderedQuantity(): int
    {
        return $this->orderedQuantity;
    }

    /**
     * @return string|null
     */
    public function getDateFrom(): ?string
    {
        return $this->dateFrom;
    }

    /**
     * @return string|null
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }
}
