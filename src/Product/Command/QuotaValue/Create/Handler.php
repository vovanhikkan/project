<?php

declare(strict_types=1);

namespace App\Product\Command\QuotaValue\Create;

use App\Application\ValueObject\Uuid;
use App\Product\Model\QuotaValue\QuotaValue;
use App\Product\Repository\QuotaRepository;
use App\Product\Service\QuotaValue\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;
    private QuotaRepository $quotaRepository;

    public function __construct(Creator $creator, QuotaRepository $quotaRepository)
    {
        $this->creator = $creator;
        $this->quotaRepository = $quotaRepository;
    }

    public function handle(Command $command): QuotaValue
    {
        $quota = $this->quotaRepository->get(new Uuid($command->getQuota()));

        return $this->creator->create(
            $quota,
            $command->getTotalQuantity(),
            $command->getHoldedQuantity(),
            $command->getOrderedQuantity(),
            $command->getDateFrom(),
            $command->getDateTo()
        );
    }
}
