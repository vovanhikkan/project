<?php

declare(strict_types=1);

namespace App\Product\Command\QuotaValue\Update;

use App\Application\ValueObject\Uuid;
use App\Product\Model\QuotaValue\QuotaValue;
use App\Product\Repository\QuotaRepository;
use App\Product\Repository\QuotaValueRepository;
use App\Product\Service\QuotaValue\Updater;

/**
 * Handler.
 */
class Handler
{
    private Updater $updater;
    private QuotaValueRepository $quotaValueRepository;
    private QuotaRepository $quotaRepository;

    public function __construct(Updater $updater, QuotaValueRepository $quotaValueRepository, QuotaRepository $quotaRepository)
    {
        $this->updater = $updater;
        $this->quotaValueRepository = $quotaValueRepository;
        $this->quotaRepository = $quotaRepository;
    }

    public function handle(Command $command): QuotaValue
    {
        $quotaValue = $this->quotaValueRepository->get(new Uuid($command->getId()));
        $quota = null;
        if ($command->getQuota()) {
            $quota = $this->quotaRepository->get(new Uuid($command->getQuota()));
        }


        $this->updater->update(
            $quotaValue,
            $quota,
            $command->getTotalQuantity(),
            $command->getHoldedQuantity(),
            $command->getOrderedQuantity(),
            $command->getDateFrom(),
            $command->getDateTo()
        );

        return $quotaValue;
    }
}
