<?php

declare(strict_types=1);

namespace App\Product\Command\QuotaValue\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;

    private ?string $quota;

    private ?int $totalQuantity;

    private ?int $holdedQuantity;

    private ?int $orderedQuantity;

    private ?string $dateFrom;
    private ?string $dateTo;

    /**
     * Command constructor.
     * @param string $id
     * @param string|null $quota
     * @param int|null $totalQuantity
     * @param int|null $holdedQuantity
     * @param int|null $orderedQuantity
     * @param string|null $dateFrom
     * @param string|null $dateTo
     */
    public function __construct(string $id, ?string $quota, ?int $totalQuantity, ?int $holdedQuantity, ?int $orderedQuantity, ?string $dateFrom, ?string $dateTo)
    {
        $this->id = $id;
        $this->quota = $quota;
        $this->totalQuantity = $totalQuantity;
        $this->holdedQuantity = $holdedQuantity;
        $this->orderedQuantity = $orderedQuantity;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getQuota(): ?string
    {
        return $this->quota;
    }

    /**
     * @return int|null
     */
    public function getTotalQuantity(): ?int
    {
        return $this->totalQuantity;
    }

    /**
     * @return int|null
     */
    public function getHoldedQuantity(): ?int
    {
        return $this->holdedQuantity;
    }

    /**
     * @return int|null
     */
    public function getOrderedQuantity(): ?int
    {
        return $this->orderedQuantity;
    }

    /**
     * @return string|null
     */
    public function getDateFrom(): ?string
    {
        return $this->dateFrom;
    }

    /**
     * @return string|null
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }
}
