<?php

declare(strict_types=1);

namespace App\Product\Command\Quota\Create;

use App\Application\ValueObject\Uuid;
use App\Product\Model\Quota\Name;
use App\Product\Model\Quota\Quota;
use App\Product\Repository\ProductRepository;
use App\Product\Repository\QuotaGroupRepository;
use App\Product\Service\Quota\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;
    private QuotaGroupRepository $quotaGroupRepository;
    private ProductRepository $productRepository;

    public function __construct(Creator $creator, QuotaGroupRepository $quotaGroupRepository, ProductRepository $productRepository)
    {
        $this->creator = $creator;
        $this->quotaGroupRepository = $quotaGroupRepository;
        $this->productRepository = $productRepository;
    }

    public function handle(Command $command): Quota
    {
        $group = $this->quotaGroupRepository->get(new Uuid($command->getGroupId()));
        $products = [];
        if ($productIds = $command->getProducts()) {
            $products = $this->productRepository->findByIds($productIds);
        }

        return $this->creator->create(
            new Name($command->getName()),
            $group,
            $command->isDependsOnDate(),
            $command->getHasValuesByDays(),
            $products
        );
    }
}
