<?php

declare(strict_types=1);

namespace App\Product\Command\Quota\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private array $name;

    /**
     * @Assert\NotBlank()
     */
    private string $group_id;

    /**
     * @Assert\NotBlank()
     */
    private bool $isDependsOnDate;

    private ?bool $hasValuesByDays;

    private ?array $products;

    /**
     * Command constructor.
     * @param array $name
     * @param string $group_id
     * @param bool $isDependsOnDate
     * @param bool|null $hasValuesByDays
     * @param array|null $products
     */
    public function __construct(array $name, string $group_id, bool $isDependsOnDate, ?bool $hasValuesByDays, ?array $products)
    {
        $this->name = $name;
        $this->group_id = $group_id;
        $this->isDependsOnDate = $isDependsOnDate;
        $this->hasValuesByDays = $hasValuesByDays;
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->group_id;
    }

    /**
     * @return bool
     */
    public function isDependsOnDate(): bool
    {
        return $this->isDependsOnDate;
    }

    /**
     * @return bool|null
     */
    public function getHasValuesByDays(): ?bool
    {
        return $this->hasValuesByDays;
    }

    /**
     * @return array|null
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }
}
