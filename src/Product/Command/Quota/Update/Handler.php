<?php

declare(strict_types=1);

namespace App\Product\Command\Quota\Update;

use App\Application\ValueObject\Uuid;
use App\Product\Model\Quota\Name;
use App\Product\Model\Quota\Quota;
use App\Product\Repository\ProductRepository;
use App\Product\Repository\QuotaRepository;
use App\Product\Service\Quota\Updater;
use App\Product\Repository\QuotaGroupRepository;

/**
 * Handler.
 */
class Handler
{
    private Updater $updater;
    private QuotaGroupRepository $quotaGroupRepository;
    private QuotaRepository $quotaRepository;
    private ProductRepository $productRepository;

    public function __construct(Updater $updater, QuotaRepository $quotaRepository, QuotaGroupRepository $quotaGroupRepository, ProductRepository $productRepository)
    {
        $this->updater = $updater;
        $this->quotaGroupRepository = $quotaGroupRepository;
        $this->productRepository = $productRepository;
        $this->quotaRepository = $quotaRepository;
    }

    public function handle(Command $command): Quota
    {
        $quota = $this->quotaRepository->get(new Uuid($command->getId()));
        $group = null;
        $products = null;
        if ($command->getGroupId()) {
            $group = $this->quotaGroupRepository->get(new Uuid($command->getGroupId()));
        }

        if ($productIds = $command->getProducts()) {
            $products = $this->productRepository->findByIds($productIds);
        }

        $this->updater->update(
            $quota,
            $command->getName() ? new Name($command->getName()) : null,
            $group,
            $command->getIsDependsOnDate(),
            $command->getHasValuesByDays(),
            $products
        );

        return $quota;
    }
}
