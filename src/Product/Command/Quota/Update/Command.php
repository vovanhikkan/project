<?php

declare(strict_types=1);

namespace App\Product\Command\Quota\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;

    private ?array $name;

    private ?string $group_id;

    private ?bool $isDependsOnDate;

    private ?bool $hasValuesByDays;

    private ?array $products;

    /**
     * Command constructor.
     * @param string $id
     * @param array|null $name
     * @param string|null $group_id
     * @param bool|null $isDependsOnDate
     * @param bool|null $hasValuesByDays
     * @param array|null $products
     */
    public function __construct(string $id, ?array $name, ?string $group_id, ?bool $isDependsOnDate, ?bool $hasValuesByDays, ?array $products)
    {
        $this->id = $id;
        $this->name = $name;
        $this->group_id = $group_id;
        $this->isDependsOnDate = $isDependsOnDate;
        $this->hasValuesByDays = $hasValuesByDays;
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getGroupId(): ?string
    {
        return $this->group_id;
    }

    /**
     * @return bool|null
     */
    public function getIsDependsOnDate(): ?bool
    {
        return $this->isDependsOnDate;
    }

    /**
     * @return bool|null
     */
    public function getHasValuesByDays(): ?bool
    {
        return $this->hasValuesByDays;
    }

    /**
     * @return array|null
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }
}
