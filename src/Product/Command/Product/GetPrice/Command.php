<?php

declare(strict_types=1);

namespace App\Product\Command\Product\GetPrice;

use App\Auth\Model\User\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;

    /**
     * @Assert\NotBlank(message="Введите дату")
     */
    private string $activationDate;

    private ?User $user;

    /**
     * Command constructor.
     * @param string $id
     * @param string $activationDate
     * @param User|null $user
     */
    public function __construct(string $id, string $activationDate, ?User $user)
    {
        $this->id = $id;
        $this->activationDate = $activationDate;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getActivationDate(): string
    {
        return $this->activationDate;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }
}
