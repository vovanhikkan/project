<?php

declare(strict_types=1);

namespace App\Product\Command\Product\GetPrice;

use App\Application\ValueObject\Uuid;
use App\Product\Repository\ProductRepository;
use App\Service\Product\PriceCalculator;

/**
 * Handler.
 */
class Handler
{
    private PriceCalculator $priceCalculator;
    private ProductRepository $productRepository;

    public function __construct(
        PriceCalculator $priceCalculator,
        ProductRepository $productRepository
    ) {
        $this->priceCalculator = $priceCalculator;
        $this->productRepository = $productRepository;
    }

    public function handle(Command $command): array
    {
        return $this->priceCalculator->getPrice(
            $command->getUser(),
            $this->productRepository->get(new Uuid($command->getId())),
            $command->getActivationDate()
        );
    }
}
