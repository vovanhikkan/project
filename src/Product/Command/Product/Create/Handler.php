<?php

declare(strict_types=1);

namespace App\Product\Command\Product\Create;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use App\Product\Dto\ProductPriceDto;
use App\Product\Dto\ProductSettingsDto;
use App\Product\Model\Product\Age;
use App\Product\Model\Product\AmountOfDays;
use App\Product\Model\Product\CardType;
use App\Product\Model\Product\Name;
use App\Product\Model\Product\Product;
use App\Product\Model\Product\Rounds;
use App\Product\Repository\SectionRepository;
use App\Product\Service\Product\Creator;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private SectionRepository $sectionRepository;
    private Creator $creator;

    public function __construct(SectionRepository $sectionRepository, Creator $creator)
    {
        $this->sectionRepository = $sectionRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): Product
    {
        $section = $this->sectionRepository->get(new Uuid($command->getSectionId()));
        $settings = $command->getSettings();

        $prices = [];
        foreach ($command->getPrices() as $priceDto) {
            $prices[] = new ProductPriceDto(
                Amount::fromRoubles($priceDto->getPrice()),
                new Id($priceDto->getPpsPeriod()),
                new DateTimeImmutable($priceDto->getDateStart()),
                new DateTimeImmutable($priceDto->getDateEnd())
            );
        }

        return $this->creator->create(
            $section,
            new Name($command->getName()),
            Amount::fromRoubles($command->getPrice()),
            $prices,
            new ProductSettingsDto(
                Uuid::generate(),
                new CardType($settings->getCardType()),
                new Age($settings->getAge()),
                new AmountOfDays($settings->getAmountOfDays()),
                new Rounds($settings->getRounds()),
                new Id($settings->getPpsProductId()),
                new Id($settings->getNewCardPpsProductId()),
                new Id($settings->getOldCardPpsProductId()),
            )
        );
    }
}
