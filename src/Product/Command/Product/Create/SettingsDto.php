<?php

declare(strict_types=1);

namespace App\Product\Command\Product\Create;

use Assert\Assertion;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class SettingsDto
{
    /**
     * @Assert\NotBlank()
     */
    private int $cardType;

    /**
     * @Assert\NotBlank()
     */
    private string $age;

    /**
     * @Assert\NotBlank()
     */
    private string $amountOfDays;

    /**
     * @Assert\NotBlank()
     */
    private int $rounds;

    /**
     * @Assert\NotBlank()
     */
    private int $ppsProductId;

    /**
     * @Assert\NotBlank()
     */
    private int $newCardPpsProductId;

    /**
     * @Assert\NotBlank()
     */
    private int $oldCardPpsProductId;

    /**
     * SettingsDto constructor.
     * @param int $cardType
     * @param string $age
     * @param string $amountOfDays
     * @param int $rounds
     * @param int $ppsProductId
     * @param int $newCardPpsProductId
     * @param int $oldCardPpsProductId
     */
    public function __construct(int $cardType, string $age, string $amountOfDays, int $rounds, int $ppsProductId, int $newCardPpsProductId, int $oldCardPpsProductId)
    {
        $this->cardType = $cardType;
        $this->age = $age;
        $this->amountOfDays = $amountOfDays;
        $this->rounds = $rounds;
        $this->ppsProductId = $ppsProductId;
        $this->newCardPpsProductId = $newCardPpsProductId;
        $this->oldCardPpsProductId = $oldCardPpsProductId;
    }

    /**
     * @return int
     */
    public function getCardType(): int
    {
        return $this->cardType;
    }

    /**
     * @return string
     */
    public function getAge(): string
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getAmountOfDays(): string
    {
        return $this->amountOfDays;
    }

    /**
     * @return int
     */
    public function getRounds(): int
    {
        return $this->rounds;
    }

    /**
     * @return int
     */
    public function getPpsProductId(): int
    {
        return $this->ppsProductId;
    }

    /**
     * @return int
     */
    public function getNewCardPpsProductId(): int
    {
        return $this->newCardPpsProductId;
    }

    /**
     * @return int
     */
    public function getOldCardPpsProductId(): int
    {
        return $this->oldCardPpsProductId;
    }
}
