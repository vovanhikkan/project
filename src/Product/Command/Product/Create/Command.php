<?php

declare(strict_types=1);

namespace App\Product\Command\Product\Create;

use Assert\Assertion;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните секцию")
     */
    private string $sectionId;

    /**
     * @Assert\NotBlank(message="Заполните название")
     */
    private string $name;

    /**
     * @Assert\NotBlank(message="Заполните стоимость")
     */
    private float $price;

    /**
     * @Assert\Valid()
     */
    private array $prices;

    /**
     * @Assert\Valid()
     */
    private SettingsDto $settings;

    /**
     * Command constructor.
     * @param string $sectionId
     * @param string $name
     * @param float $price
     * @param array $prices
     * @param SettingsDto $settings
     */
    public function __construct(string $sectionId, string $name, float $price, array $prices, SettingsDto $settings)
    {
        $this->sectionId = $sectionId;
        $this->name = $name;
        $this->price = $price;
        $this->prices = $prices;
        $this->settings = $settings;
    }


    public function getSectionId(): string
    {
        return $this->sectionId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return PriceDto[]
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

    /**
     * @return SettingsDto
     */
    public function getSettings(): SettingsDto
    {
        return $this->settings;
    }
}
