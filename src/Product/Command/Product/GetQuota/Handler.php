<?php

declare(strict_types=1);

namespace App\Product\Command\Product\GetQuota;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\Name;
use App\Product\Model\Quota\Quota;
use App\Product\Model\QuotaValue\QuotaValue;
use App\Product\Repository\ProductRepository;
use App\Product\Repository\QuotaRepository;
use App\Product\Repository\QuotaValueRepository;
use App\Product\Service\Product\Creator;

/**
 * Handler.
 */
class Handler
{
    private ProductRepository $productRepository;
    private QuotaValueRepository $quotaValueRepository;
    private QuotaRepository $quotaRepository;
    private Creator $creator;

    public function __construct(ProductRepository $productRepository, QuotaValueRepository $quotaValueRepository, QuotaRepository $quotaRepository, Creator $creator)
    {
        $this->productRepository = $productRepository;
        $this->quotaValueRepository = $quotaValueRepository;
        $this->quotaRepository = $quotaRepository;
        $this->creator = $creator;
    }

    public function handle(Command $command): array
    {
        $product = $this->productRepository->get(new Uuid($command->getProduct()));
        $quotas = $product->getProductQuotas();
        $quotaValues = [];
        if (count($quotas)) {
            foreach ($quotas as $quota) {
                /**
                 * @var Quota $quota
                 */
                if (!$quota->isDependsOnDate()) {
                    $quotaValues = array_merge($quotaValues, $quota->getQuotaValues());
                } else {
                    $quotaValues = array_merge(
                        $quotaValues,
                        $quota->getQuotaValuesByActivationDate($command->getActivationDate())
                    );
                }
            }
        }

        $result = [
            'availableQuantity' => 0
        ];

        foreach ($quotaValues as $quotaValue) {
            /**
             * @var QuotaValue $quotaValue
             */
            $result['availableQuantity'] += $quotaValue->getTotalQuantity()->getValue() - $quotaValue->getHoldedQuantity()->getValue() - $quotaValue->getOrderedQuantity()->getValue();
        }

        return $result;
    }
}
