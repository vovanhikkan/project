<?php

declare(strict_types=1);

namespace App\Product\Command\Product\GetQuota;

use App\Product\Model\Product\Product;
use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $product;
    private ?string $activationDate;

    /**
     * Command constructor.
     * @param int $product
     * @param string|null $activationDate
     */
    public function __construct(string $product, ?string $activationDate)
    {
        $this->product = $product;
        $this->activationDate = $activationDate;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @return string|null
     */
    public function getActivationDate(): ?string
    {
        return $this->activationDate;
    }
}
