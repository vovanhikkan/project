<?php

declare(strict_types=1);

namespace App\Product\Command\Product\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * PriceDto.
 */
class PriceDto
{
    private ?string $id = null;

    /**
     * @Assert\NotBlank(message="Заполните стоимость")
     */
    private float $price;

    /**
     * @Assert\NotBlank(message="Заполните id pps")
     */
    private int $ppsPeriod;

    /**
     * @Assert\NotBlank(message="Заполните дату начала периода")
     * @Assert\Date()
     */
    private string $dateStart;

    /**
     * @Assert\NotBlank(message="Заполните дату окончания периода")
     * @Assert\Date()
     */
    private string $dateEnd;

    /**
     * PriceDto constructor.
     * @param float $price
     * @param int $ppsPeriod
     * @param string $dateStart
     * @param string $dateEnd
     */
    public function __construct(float $price, int $ppsPeriod, string $dateStart, string $dateEnd)
    {
        $this->price = $price;
        $this->ppsPeriod = $ppsPeriod;
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
    }


    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getDateStart(): string
    {
        return $this->dateStart;
    }

    public function getDateEnd(): string
    {
        return $this->dateEnd;
    }

    /**
     * @return int
     */
    public function getPpsPeriod(): int
    {
        return $this->ppsPeriod;
    }
}
