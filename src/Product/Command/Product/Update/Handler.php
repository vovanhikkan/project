<?php

declare(strict_types=1);

namespace App\Product\Command\Product\Update;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use App\Product\Dto\ProductPriceDto;
use App\Product\Dto\ProductSettingsDto;
use App\Product\Model\Product\Age;
use App\Product\Model\Product\AmountOfDays;
use App\Product\Model\Product\CardType;
use App\Product\Model\Product\Name;
use App\Product\Model\Product\Product;
use App\Product\Model\Product\Rounds;
use App\Product\Repository\ProductRepository;
use App\Product\Repository\SectionRepository;
use App\Product\Service\Product\Updater;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private ProductRepository $productRepository;
    private SectionRepository $sectionRepository;
    private Updater $updater;

    public function __construct(
        ProductRepository $productRepository,
        SectionRepository $sectionRepository,
        Updater $updater
    ) {
        $this->productRepository = $productRepository;
        $this->sectionRepository = $sectionRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): Product
    {
        $product = $this->productRepository->get(new Uuid($command->getId()));
        $section = $this->sectionRepository->get(new Uuid($command->getSectionId()));
        $settings = $command->getSettings();
        $prices = [];
        foreach ($command->getPrices() as $priceDto) {
            $productPriceDto = new ProductPriceDto(
                Amount::fromRoubles($priceDto->getPrice()),
                new Id($priceDto->getPpsPeriod()),
                new DateTimeImmutable($priceDto->getDateStart()),
                new DateTimeImmutable($priceDto->getDateEnd())
            );
            if ($priceDto->getId() !== null) {
                $productPriceDto->setId(new Uuid($priceDto->getId()));
            }
            $prices[] = $productPriceDto;
        }

        $this->updater->update(
            $product,
            $section,
            new Name($command->getName()),
            Amount::fromRoubles($command->getPrice()),
            $prices,
            new ProductSettingsDto(
                Uuid::generate(),
                new CardType($settings->getCardType()),
                new Age($settings->getAge()),
                new AmountOfDays($settings->getAmountOfDays()),
                new Rounds($settings->getRounds()),
                new Id($settings->getPpsProductId()),
                new Id($settings->getNewCardPpsProductId()),
                new Id($settings->getOldCardPpsProductId()),
            )
        );

        return $product;
    }
}
