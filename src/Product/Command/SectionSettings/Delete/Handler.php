<?php

declare(strict_types=1);

namespace App\Product\Command\SectionSettings\Delete;

use App\Application\ValueObject\Uuid;
use App\Product\Service\SectionSettings\Deleter;

/**
 * Handler.
 */
class Handler
{
    private Deleter $deleter;

    public function __construct(Deleter $deleter)
    {
        $this->deleter = $deleter;
    }

    public function handle(Command $command): array
    {
        $this->deleter->delete(new Uuid($command->getId()));
        return  ['message' => 'Настройки секции удалены'];
    }
}
