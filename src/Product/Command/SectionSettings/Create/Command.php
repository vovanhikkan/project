<?php

declare(strict_types=1);

namespace App\Product\Command\SectionSettings\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $section;

    /**
     * @Assert\NotBlank()
     */
    private array $controls;

    /**
     * @Assert\Type("bool")
     */
    private bool $isOnlyOne;

    /**
     * @Assert\Type("bool")
     */
    private bool $isActivationDateRequired;

    /**
     * @Assert\NotBlank()
     */
    private int $activeDays;

    /**
     * @Assert\Type("bool")
     */
    private bool $isNotQuantitative;

    /**
     * Command constructor.
     * @param string $section
     * @param array $controls
     * @param bool $isOnlyOne
     * @param bool $isActivationDateRequired
     * @param int $activeDays
     * @param bool $isNotQuantitative
     */
    public function __construct(string $section, array $controls, bool $isOnlyOne, bool $isActivationDateRequired, int $activeDays, bool $isNotQuantitative)
    {
        $this->section = $section;
        $this->controls = $controls;
        $this->isOnlyOne = $isOnlyOne;
        $this->isActivationDateRequired = $isActivationDateRequired;
        $this->activeDays = $activeDays;
        $this->isNotQuantitative = $isNotQuantitative;
    }

    /**
     * @return string
     */
    public function getSection(): string
    {
        return $this->section;
    }

    /**
     * @return array
     */
    public function getControls(): array
    {
        return $this->controls;
    }

    /**
     * @return bool
     */
    public function isOnlyOne(): bool
    {
        return $this->isOnlyOne;
    }

    /**
     * @return bool
     */
    public function isActivationDateRequired(): bool
    {
        return $this->isActivationDateRequired;
    }

    /**
     * @return int
     */
    public function getActiveDays(): int
    {
        return $this->activeDays;
    }

    /**
     * @return bool
     */
    public function isNotQuantitative(): bool
    {
        return $this->isNotQuantitative;
    }
}
