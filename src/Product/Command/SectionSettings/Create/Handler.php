<?php

declare(strict_types=1);

namespace App\Product\Command\SectionSettings\Create;

use App\Application\ValueObject\Uuid;
use App\Product\Model\Section\ActiveDays;
use App\Product\Model\Section\Controls;
use App\Product\Model\Section\SectionSettings;
use App\Product\Repository\SectionRepository;
use App\Product\Service\SectionSettings\Creator;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;
    private SectionRepository $sectionRepository;

    public function __construct(Creator $creator, SectionRepository $sectionRepository)
    {
        $this->creator = $creator;
        $this->sectionRepository = $sectionRepository;
    }

    public function handle(Command $command): SectionSettings
    {
        $section = $this->sectionRepository->get(new Uuid($command->getSection()));

        return $this->creator->create(
            $section,
            new Controls($command->getControls()),
            $command->isOnlyOne(),
            $command->isActivationDateRequired(),
            new ActiveDays($command->getActiveDays()),
            $command->isNotQuantitative()
        );
    }
}
