<?php

declare(strict_types=1);

namespace App\Product\Command\SectionSettings\Update;

use App\Application\ValueObject\Uuid;
use App\Product\Model\Section\ActiveDays;
use App\Product\Model\Section\Controls;
use App\Product\Model\Section\SectionSettings;
use App\Product\Repository\SectionRepository;
use App\Product\Repository\SectionSettingsRepository;
use App\Product\Service\SectionSettings\Updater;

/**
 * Handler.
 */
class Handler
{
    private SectionRepository $sectionRepository;
    private SectionSettingsRepository $sectionSettingsRepository;
    private Updater $updater;

    public function __construct(SectionRepository $sectionRepository, Updater $updater, SectionSettingsRepository $sectionSettingsRepository)
    {
        $this->sectionRepository = $sectionRepository;
        $this->sectionSettingsRepository = $sectionSettingsRepository;
        $this->updater = $updater;
    }

    public function handle(Command $command): SectionSettings
    {
        $section = $this->sectionRepository->get(new Uuid($command->getSection()));
        $sectionSettings = $this->sectionSettingsRepository->get(new Uuid($command->getId()));

        $this->updater->update(
            $sectionSettings,
            $section,
            new Controls($command->getControls()),
            $command->getIsOnlyOne(),
            $command->getIsActivationDateRequired(),
            new ActiveDays($command->getActiveDays()),
            $command->getIsNotQuantitative()
        );

        return $sectionSettings;
    }
}
