<?php

declare(strict_types=1);

namespace App\Product\Command\SectionSettings\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;
    private ?string $section;
    private ?array $controls;
    private ?bool $isOnlyOne;
    private ?bool $isActivationDateRequired;
    private ?int $activeDays;
    private ?bool $isNotQuantitative;

    /**
     * Command constructor.
     * @param string $id
     * @param string|null $section
     * @param array|null $controls
     * @param bool|null $isOnlyOne
     * @param bool|null $isActivationDateRequired
     * @param int|null $activeDays
     * @param bool|null $isNotQuantitative
     */
    public function __construct(string $id, ?string $section, ?array $controls, ?bool $isOnlyOne, ?bool $isActivationDateRequired, ?int $activeDays, ?bool $isNotQuantitative)
    {
        $this->id = $id;
        $this->section = $section;
        $this->controls = $controls;
        $this->isOnlyOne = $isOnlyOne;
        $this->isActivationDateRequired = $isActivationDateRequired;
        $this->activeDays = $activeDays;
        $this->isNotQuantitative = $isNotQuantitative;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSection(): ?string
    {
        return $this->section;
    }

    /**
     * @return array|null
     */
    public function getControls(): ?array
    {
        return $this->controls;
    }

    /**
     * @return bool|null
     */
    public function getIsOnlyOne(): ?bool
    {
        return $this->isOnlyOne;
    }

    /**
     * @return bool|null
     */
    public function getIsActivationDateRequired(): ?bool
    {
        return $this->isActivationDateRequired;
    }

    /**
     * @return int|null
     */
    public function getActiveDays(): ?int
    {
        return $this->activeDays;
    }

    /**
     * @return bool|null
     */
    public function getIsNotQuantitative(): ?bool
    {
        return $this->isNotQuantitative;
    }

}
