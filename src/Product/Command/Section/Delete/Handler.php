<?php

declare(strict_types=1);

namespace App\Product\Command\Section\Delete;

use App\Application\ValueObject\Uuid;
use App\Product\Service\Section\Deleter;

/**
 * Handler.
 */
class Handler
{
    private Deleter $deleter;

    public function __construct(Deleter $deleter)
    {
        $this->deleter = $deleter;
    }

    public function handle(Command $command): array
    {
        $this->deleter->delete(new Uuid($command->getId()));
        return  ['message' => 'Секция удалена'];
    }
}
