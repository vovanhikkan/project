<?php

declare(strict_types=1);

namespace App\Product\Command\Section\Update;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $id;
    private ?string $code;
    private ?array $name;
    private ?array $description;
    private ?array $tariffDescription;
    private ?array $innerTitle;
    private ?array $innerDescription;
    /**
     * @Assert\NotBlank(message="Заполните inner description")
     */
    private ?SettingsDto $settings;
    private ?string $picture;
    private ?string $webIcon;
    private ?string $webBackground;
    private ?string $mobIcon;
    private ?string $mobileBackground;

    /**
     * Command constructor.
     * @param string $id
     * @param string|null $code
     * @param array|null $name
     * @param array|null $description
     * @param array|null $tariffDescription
     * @param array|null $innerTitle
     * @param array|null $innerDescription
     * @param SettingsDto|null $settings
     * @param string|null $picture
     * @param string|null $webIcon
     * @param string|null $webBackground
     * @param string|null $mobIcon
     * @param string|null $mobileBackground
     */
    public function __construct(
        string $id,
        ?string $code,
        ?array $name,
        ?array $description,
        ?array $tariffDescription,
        ?array $innerTitle,
        ?array $innerDescription,
        ?SettingsDto $settings,
        ?string $picture,
        ?string $webIcon,
        ?string $webBackground,
        ?string $mobIcon,
        ?string $mobileBackground
    ) {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
        $this->description = $description;
        $this->tariffDescription = $tariffDescription;
        $this->innerTitle = $innerTitle;
        $this->innerDescription = $innerDescription;
        $this->settings = $settings;
        $this->picture = $picture;
        $this->webIcon = $webIcon;
        $this->webBackground = $webBackground;
        $this->mobIcon = $mobIcon;
        $this->mobileBackground = $mobileBackground;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->name;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @return array|null
     */
    public function getTariffDescription(): ?array
    {
        return $this->tariffDescription;
    }

    /**
     * @return array|null
     */
    public function getInnerTitle(): ?array
    {
        return $this->innerTitle;
    }

    /**
     * @return array|null
     */
    public function getInnerDescription(): ?array
    {
        return $this->innerDescription;
    }

    /**
     * @return SettingsDto|null
     */
    public function getSettings(): ?SettingsDto
    {
        return $this->settings;
    }

    /**
     * @return string|null
     */
    public function getPicture(): ?string
    {
        return $this->picture;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @return string|null
     */
    public function getWebBackground(): ?string
    {
        return $this->webBackground;
    }

    /**
     * @return string|null
     */
    public function getMobIcon(): ?string
    {
        return $this->mobIcon;
    }

    /**
     * @return string|null
     */
    public function getMobileBackground(): ?string
    {
        return $this->mobileBackground;
    }
}
