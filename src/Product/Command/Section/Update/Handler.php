<?php

declare(strict_types=1);

namespace App\Product\Command\Section\Update;

use App\Application\ValueObject\Uuid;
use App\Product\Dto\SectionSettingsDto;
use App\Product\Model\Section\ActiveDays;
use App\Product\Model\Section\Code;
use App\Product\Model\Section\Controls;
use App\Product\Model\Section\Description;
use App\Product\Model\Section\InnerDescription;
use App\Product\Model\Section\InnerTitle;
use App\Product\Model\Section\Name;
use App\Product\Model\Section\Section;
use App\Product\Model\Section\TariffDescription;
use App\Product\Repository\SectionRepository;
use App\Product\Service\Section\Updater;
use App\Storage\Repository\FileRepository;

/**
 * Handler.
 */
class Handler
{
    private SectionRepository $sectionRepository;
    private Updater $updater;
    private FileRepository $fileRepository;

    public function __construct(SectionRepository $sectionRepository, Updater $updater, FileRepository $fileRepository)
    {
        $this->sectionRepository = $sectionRepository;
        $this->updater = $updater;
        $this->fileRepository = $fileRepository;
    }

    public function handle(Command $command): Section
    {
        $section = $this->sectionRepository->get(new Uuid($command->getId()));

        $settings = $command->getSettings();

        $sectionSettings = new SectionSettingsDto(
            Uuid::generate(),
            new Controls($settings->getControls()),
            $settings->isOnlyOne(),
            $settings->isActivationDateRequired(),
            new ActiveDays($settings->getActiveDays()),
            $settings->isNotQuantitave()
        );

        $this->updater->update(
            $section,
            new Code($command->getCode()),
            new Name($command->getName()),
            new Description($command->getDescription()),
            new TariffDescription($command->getTariffDescription()),
            new InnerTitle($command->getInnerTitle()),
            new InnerDescription($command->getTariffDescription()),
            $sectionSettings,
            $this->fileRepository->getFileOrNull($command->getPicture()),
            $this->fileRepository->getFileOrNull($command->getWebIcon()),
            $this->fileRepository->getFileOrNull($command->getWebBackground()),
            $this->fileRepository->getFileOrNull($command->getMobIcon()),
            $this->fileRepository->getFileOrNull($command->getMobileBackground()),
        );

        return $section;
    }
}
