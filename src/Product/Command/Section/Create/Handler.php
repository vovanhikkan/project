<?php

declare(strict_types=1);

namespace App\Product\Command\Section\Create;

use App\Application\ValueObject\Uuid;
use App\Product\Dto\SectionSettingsDto;
use App\Product\Model\Section\ActiveDays;
use App\Product\Model\Section\Code;
use App\Product\Model\Section\Controls;
use App\Product\Model\Section\Description;
use App\Product\Model\Section\InnerDescription;
use App\Product\Model\Section\InnerTitle;
use App\Product\Model\Section\Name;
use App\Product\Model\Section\Section;
use App\Product\Model\Section\SectionSettings;
use App\Product\Model\Section\TariffDescription;
use App\Product\Repository\SectionRepository;
use App\Product\Service\Section\Creator;
use App\Storage\Repository\FileRepository;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;
    private FileRepository $fileRepository;
    private SectionRepository $sectionRepository;

    public function __construct(Creator $creator, FileRepository $fileRepository, SectionRepository $sectionRepository)
    {
        $this->creator = $creator;
        $this->fileRepository = $fileRepository;
        $this->sectionRepository = $sectionRepository;
    }

    public function handle(Command $command): Section
    {
        $settings = $command->getSettings();

        $sectionSettings = new SectionSettingsDto(
            Uuid::generate(),
            new Controls($settings->getControls()),
            $settings->isOnlyOne(),
            $settings->isActivationDateRequired(),
            new ActiveDays($settings->getActiveDays()),
            $settings->isNotQuantitave()
        );

        return $this->creator->create(
            new Code($command->getCode()),
            new Name($command->getName()),
            new Description($command->getDescription()),
            new TariffDescription($command->getTariffDescription()),
            new InnerTitle($command->getInnerTitle()),
            new InnerDescription($command->getTariffDescription()),
            $sectionSettings,
            $this->fileRepository->getFileOrNull($command->getPicture()),
            $this->fileRepository->getFileOrNull($command->getWebIcon()),
            $this->fileRepository->getFileOrNull($command->getWebBackground()),
            $this->fileRepository->getFileOrNull($command->getMobIcon()),
            $this->fileRepository->getFileOrNull($command->getMobileBackground())
        );
    }
}
