<?php

declare(strict_types=1);

namespace App\Product\Command\Section\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * SettingsDto.
 */
class SettingsDto
{
    /**
     * @Assert\NotBlank()
     */
    private array $controls;

    /**
     * @Assert\NotBlank()
     */
    private bool $isOnlyOne;

    /**
     * @Assert\NotBlank()
     */
    private bool $isActivationDateRequired;

    /**
     * @Assert\NotBlank()
     */
    private int $activeDays;

    /**
     * @Assert\NotBlank()
     */
    private bool $isNotQuantitave;

    /**
     * SettingsDto constructor.
     * @param array $controls
     * @param bool $isOnlyOne
     * @param bool $isActivationDateRequired
     * @param int $activeDays
     * @param bool $isNotQuantitative
     */
    public function __construct(
        array $controls,
        bool $isOnlyOne,
        bool $isActivationDateRequired,
        int $activeDays,
        bool $isNotQuantitative
    ) {
        $this->controls = $controls;
        $this->isOnlyOne = $isOnlyOne;
        $this->isActivationDateRequired = $isActivationDateRequired;
        $this->activeDays = $activeDays;
        $this->isNotQuantitave = $isNotQuantitative;
    }

    /**
     * @return array
     */
    public function getControls(): array
    {
        return $this->controls;
    }

    /**
     * @return bool
     */
    public function isOnlyOne(): bool
    {
        return $this->isOnlyOne;
    }

    /**
     * @return bool
     */
    public function isActivationDateRequired(): bool
    {
        return $this->isActivationDateRequired;
    }

    /**
     * @return int
     */
    public function getActiveDays(): int
    {
        return $this->activeDays;
    }

    /**
     * @return bool
     */
    public function isNotQuantitave(): bool
    {
        return $this->isNotQuantitave;
    }
}

