<?php

declare(strict_types=1);

namespace App\Product\Command\Section\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните код")
     */
    private string $code;

    /**
     * @Assert\NotBlank(message="Заполните название")
     */
    private array $name;

    /**
     * @Assert\NotBlank(message="Заполните описание")
     */
    private array $description;

    /**
     * @Assert\NotBlank(message="Заполните описание тарифа")
     */
    private array $tariffDescription;

    /**
     * @Assert\NotBlank(message="Заполните inner title")
     */
    private array $innerTitle;

    /**
     * @Assert\NotBlank(message="Заполните inner description")
     */
    private array $innerDescription;

    /**
     * @Assert\NotBlank(message="Заполните inner description")
     */
    private SettingsDto $settings;

    private ?string $picture;
    private ?string $webIcon;
    private ?string $webBackground;
    private ?string $mobIcon;
    private ?string $mobileBackground;

    /**
     * Command constructor.
     * @param string $code
     * @param array $name
     * @param array $description
     * @param array $tariffDescription
     * @param array $innerTitle
     * @param array $innerDescription
     * @param SettingsDto $settings
     * @param string|null $picture
     * @param string|null $webIcon
     * @param string|null $webBackground
     * @param string|null $mobIcon
     * @param string|null $mobileBackground
     */
    public function __construct(
        string $code,
        array $name,
        array $description,
        array $tariffDescription,
        array $innerTitle,
        array $innerDescription,
        SettingsDto $settings,
        ?string $picture,
        ?string $webIcon,
        ?string $webBackground,
        ?string $mobIcon,
        ?string $mobileBackground
    ) {
        $this->code = $code;
        $this->name = $name;
        $this->description = $description;
        $this->tariffDescription = $tariffDescription;
        $this->innerTitle = $innerTitle;
        $this->innerDescription = $innerDescription;
        $this->settings = $settings;
        $this->picture = $picture;
        $this->webIcon = $webIcon;
        $this->webBackground = $webBackground;
        $this->mobIcon = $mobIcon;
        $this->mobileBackground = $mobileBackground;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getTariffDescription(): array
    {
        return $this->tariffDescription;
    }

    /**
     * @return array
     */
    public function getInnerTitle(): array
    {
        return $this->innerTitle;
    }

    /**
     * @return array
     */
    public function getInnerDescription(): array
    {
        return $this->innerDescription;
    }

    /**
     * @return SettingsDto
     */
    public function getSettings(): SettingsDto
    {
        return $this->settings;
    }

    /**
     * @return string|null
     */
    public function getPicture(): ?string
    {
        return $this->picture;
    }

    /**
     * @return string|null
     */
    public function getWebIcon(): ?string
    {
        return $this->webIcon;
    }

    /**
     * @return string|null
     */
    public function getWebBackground(): ?string
    {
        return $this->webBackground;
    }

    /**
     * @return string|null
     */
    public function getMobIcon(): ?string
    {
        return $this->mobIcon;
    }

    /**
     * @return string|null
     */
    public function getMobileBackground(): ?string
    {
        return $this->mobileBackground;
    }
}
