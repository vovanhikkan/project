<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Quota\Quota;
use App\Product\Model\QuotaValue\QuotaValue;

/**
 * QuotaRepository.
 */
class QuotaValueRepository extends AbstractRepository
{
    public function add(QuotaValue $product): void
    {
        $this->entityManager->persist($product);
    }

    /**
     * @return QuotaValue[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('qv');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): QuotaValue
    {
        /** @var QuotaValue|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Quota Value with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getQuotaValuesByActivationDateAndQuota(Quota $quota, ?string $activationDate)
    {
        $qb = $this->entityRepository->createQueryBuilder('qv')
            ->where('qv.quota = :quota')
            ->setParameter('quota', $quota->getId());

        if ($activationDate) {
            $qb->andWhere(':activationDate between qv.dateFrom AND qv.dateTo')
                ->setParameter('activationDate', $activationDate);
        }

        return $qb->getQuery()->getResult();
    }

    public function getQuotaValuesByQuota(Quota $quota)
    {
        $qb = $this->entityRepository->createQueryBuilder('qv')
            ->where('qv.quota = :quota')
            ->setParameter('quota', $quota->getId());

        return $qb->getQuery()->getResult();
    }

    protected function getModelClassName(): string
    {
        return QuotaValue::class;
    }
}
