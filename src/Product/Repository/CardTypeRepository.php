<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Id;
use App\Product\Model\CardType\CardType;
use App\Product\Model\CardType\Type;

/**
 * CardTypeRepository.
 */
class CardTypeRepository extends AbstractRepository
{
    public function fetchOneByPpsProductId(Id $ppsProductId): ?CardType
    {
        $qb = $this->entityRepository->createQueryBuilder('ct');
        $qb
            ->andWhere('ct.ppsProductId = :ppsProductId')
            ->setParameter('ppsProductId', $ppsProductId->getValue())
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function fetchOneByType(Type $type): ?CardType
    {
        $qb = $this->entityRepository->createQueryBuilder('ct');
        $qb
            ->andWhere('ct.type = :type')
            ->setParameter('type', $type->getValue())
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    protected function getModelClassName(): string
    {
        return CardType::class;
    }
}
