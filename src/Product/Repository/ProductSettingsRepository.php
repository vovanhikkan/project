<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\ProductSettings;

/**
 * ProductSettingsRepository.
 */
class ProductSettingsRepository extends AbstractRepository
{
    public function add(ProductSettings $product): void
    {
        $this->entityManager->persist($product);
    }

    /**
     * @return ProductSettings[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('p');
        $qb->orderBy('p.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): ProductSettings
    {
        /** @var ProductSettings|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Product Settings with id %s not found', (string)$id));
        }

        return $model;
    }

    public function delete(Uuid $id)
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return ProductSettings::class;
    }
}
