<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Product\Model\QuotaGroup\QuotaGroup;

/**
 * QuotaGroupRepository.
 */
class QuotaGroupRepository extends AbstractRepository
{
    public function add(QuotaGroup $product): void
    {
        $this->entityManager->persist($product);
    }

    /**
     * @return QuotaGroup[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('qg');
        $qb->orderBy('qg.sort', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): QuotaGroup
    {
        /** @var QuotaGroup|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Quota group with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort(): int
    {
        return (int)$this->entityRepository->createQueryBuilder('qg')
            ->select('MAX(qg.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return QuotaGroup::class;
    }
}
