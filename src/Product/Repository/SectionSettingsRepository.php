<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Section\SectionSettings;

/**
 * SectionSettingsRepository.
 */
class SectionSettingsRepository extends AbstractRepository
{
    public function add(SectionSettings $section): void
    {
        $this->entityManager->persist($section);
    }

    /**
     * @return SectionSettings[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('ss');
        $qb->orderBy('ss.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): SectionSettings
    {
        /** @var SectionSettings|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Section settings with id %s not found', (string)$id));
        }

        return $model;
    }

    public function delete(Uuid $id)
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return SectionSettings::class;
    }
}
