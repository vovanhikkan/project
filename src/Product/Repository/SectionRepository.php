<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Section\Section;

/**
 * SectionRepository.
 */
class SectionRepository extends AbstractRepository
{
    public function add(Section $section): void
    {
        $this->entityManager->persist($section);
    }

    /**
     * @return Section[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('s');
        $qb->orderBy('s.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Section
    {
        /** @var Section|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Section with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort(): int
    {
        return (int)$this->entityRepository->createQueryBuilder('p')
            ->select('MAX(p.recommendationSort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function delete(Uuid $id)
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return Section::class;
    }
}
