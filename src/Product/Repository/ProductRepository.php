<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Product\Product;
use App\Product\Model\Product\ProductPrice;
use Doctrine\ORM\Query\Expr\Join;

/**
 * ProductRepository.
 */
class ProductRepository extends AbstractRepository
{
    public function add(Product $product): void
    {
        $this->entityManager->persist($product);
    }

    /**
     * @return Product[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('p');
        $qb->orderBy('p.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function fetchOneByPpsProductId(Id $ppsProductId): ?Product
    {
        $qb = $this->entityRepository->createQueryBuilder('p');
        $qb
            ->innerJoin('p.productSettings', 'ps')
            ->andWhere('ps.ppsProductId = :ppsProductId')
            ->setParameter('ppsProductId', $ppsProductId->getValue())
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function fetchOneByNewCardPpsProductId(Id $newCardPpsProductId): ?Product
    {
        $qb = $this->entityRepository->createQueryBuilder('p');
        $qb
            ->innerJoin('p.productSettings', 'ps')
            ->andWhere('ps.newCardPpsProductId = :newCardPpsProductId')
            ->setParameter('newCardPpsProductId', $newCardPpsProductId->getValue())
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function get(Uuid $id): Product
    {
        /** @var Product|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Product with id %s not found', (string)$id));
        }

        return $model;
    }

    /**
     * @param string $id
     * @param string $activationDate
     *
     * @return ProductPrice|null
     */
    public function getProductPrice(string $id, string $activationDate): ?ProductPrice
    {
        $repository = $this->entityRepository;
        $query = $repository->createQueryBuilder('p')
            ->select('p2')
            ->leftJoin(ProductPrice::class, 'p2', Join::WITH, 'p.id = p2.product')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->andWhere(':activationDate between p2.dateStart AND p2.dateEnd')
            ->andWhere('p2.isActive = true')
            ->setParameter('activationDate', $activationDate);

        return count($query->getQuery()->getResult()) ? $query->getQuery()->getResult()[0] : null;
    }

    public function findByIds(array $productsIds)
    {
        $query = $this->entityRepository->createQueryBuilder('p')
            ->where('p.id IN (:ids)')
            ->setParameter('ids', $productsIds);

        return  $query->getQuery()->getResult();
    }

    public function delete(Uuid $id)
    {
        $model = $this->get($id);
        $this->entityManager->remove($model);
    }

    protected function getModelClassName(): string
    {
        return Product::class;
    }
}
