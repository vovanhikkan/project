<?php

declare(strict_types=1);

namespace App\Product\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Uuid;
use App\Product\Model\Quota\Quota;
use Doctrine\ORM\Query\Expr\Join;

/**
 * QuotaRepository.
 */
class QuotaRepository extends AbstractRepository
{
    public function add(Quota $product): void
    {
        $this->entityManager->persist($product);
    }

    /**
     * @return Quota[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('q');
        $qb->orderBy('q.sort', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function get(Uuid $id): Quota
    {
        /** @var Quota|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('Quota with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getMaxSort(): int
    {
        return (int)$this->entityRepository->createQueryBuilder('q')
            ->select('MAX(q.sort)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getModelClassName(): string
    {
        return Quota::class;
    }
}
