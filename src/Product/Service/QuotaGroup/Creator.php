<?php

declare(strict_types=1);

namespace App\Product\Service\QuotaGroup;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Product\Model\QuotaGroup\Name;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\Product\Repository\QuotaGroupRepository;

/**
 * Creator.
 */
class Creator
{
    private QuotaGroupRepository $quotaGroupRepository;
    private Flusher $flusher;

    public function __construct(QuotaGroupRepository $quotaGroupRepository, Flusher $flusher)
    {
        $this->quotaGroupRepository = $quotaGroupRepository;
        $this->flusher = $flusher;
    }

    public function create(Name $name): QuotaGroup
    {
        $sort = $this->quotaGroupRepository->getMaxSort() + 1;
        $quotaGroup = new QuotaGroup(Uuid::generate(), $name, new Sort($sort), true);

        $this->quotaGroupRepository->add($quotaGroup);
        $this->flusher->flush();

        return $quotaGroup;
    }
}
