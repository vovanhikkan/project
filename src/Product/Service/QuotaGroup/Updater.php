<?php

declare(strict_types=1);

namespace App\Product\Service\QuotaGroup;

use App\Data\Flusher;
use App\Product\Model\QuotaGroup\Name;
use App\Product\Model\QuotaGroup\QuotaGroup;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function update(
        QuotaGroup $quotaGroup,
        Name $name
    ): void {

        $quotaGroup->update(
            $name,
        );

        $this->flusher->flush();
    }
}
