<?php

declare(strict_types=1);

namespace App\Product\Service\Product;

use App\Application\ValueObject\Amount;
use App\Product\Model\Product\Product;
use DateTimeImmutable;

/**
 * PriceDetector.
 */
class PriceDetector
{
    public function detect(Product $product): Amount
    {
        $current = new DateTimeImmutable();
        foreach ($product->getProductPrices() as $productPrice) {
            if (
                $productPrice->getDateStart() <= $current &&
                $productPrice->getDateEnd() >= $current
            ) {
                return $productPrice->getPrice();
            }
        }

        return $product->getPrice();
    }
}
