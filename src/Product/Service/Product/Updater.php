<?php

declare(strict_types=1);

namespace App\Product\Service\Product;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Product\Dto\ProductPriceDto;
use App\Product\Dto\ProductSettingsDto;
use App\Product\Model\Product\Name;
use App\Product\Model\Product\Product;
use App\Product\Model\Section\Section;
use Assert\Assertion;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function update(
        Product $product,
        Section $section,
        Name $name,
        Amount $price,
        array $prices,
        ProductSettingsDto $settings
    ): void {
        Assertion::allIsInstanceOf($prices, ProductPriceDto::class);

        $product->update($section, $name, $price);

        /** @var string[] $actualProductPriceIds */
        $actualProductPriceIds = [];

        /** @var ProductPriceDto $productPriceDto */
        foreach ($prices as $productPriceDto) {
            if ($productPriceDto->getId() !== null) {
                $productPrice = null;
                foreach ($product->getProductPrices() as $item) {
                    if ($item->getId()->isEqual($productPriceDto->getId())) {
                        $productPrice = $item;
                        break;
                    }
                }
                if ($productPrice === null) {
                    throw new NotFoundException();
                }
                $productPrice->update(
                    $productPriceDto->getAmount(),
                    $productPriceDto->getDateStart(),
                    $productPriceDto->getDateEnd(),
                    true,
                    $productPriceDto->getPpsPeriod()
                );
                $actualProductPriceIds[] = (string)$productPrice->getId();
            } else {
                $productPrice = $product->addProductPrice(
                    Uuid::generate(),
                    $productPriceDto->getAmount(),
                    $productPriceDto->getDateStart(),
                    $productPriceDto->getDateEnd(),
                    true,
                    $productPriceDto->getPpsPeriod()
                );
                $actualProductPriceIds[] = (string)$productPrice->getId();
            }
        }

        foreach ($product->getProductPrices() as $productPrice) {
            if (!in_array((string)$productPrice->getId(), $actualProductPriceIds)) {
                $product->removeProductPrice($productPrice);
            }
        }

        $product->getProductSettings()->update(
            $settings->getCardType(),
            $settings->getAge(),
            $settings->getAmountOfDays(),
            $settings->getRounds(),
            $settings->getPpsProductId(),
            $settings->getNewCardPpsProductId(),
            $settings->getOldCardPpsProductId()
        );


        $this->flusher->flush();
    }
}
