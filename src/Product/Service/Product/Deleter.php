<?php

declare(strict_types=1);

namespace App\Product\Service\Product;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Language\Repository\LanguageRepository;
use App\Product\Repository\ProductRepository;

/**
 * Deleter.
 */
class Deleter
{
    private Flusher $flusher;
    private ProductRepository $productRepository;

    public function __construct(Flusher $flusher, ProductRepository $productRepository)
    {
        $this->flusher = $flusher;
        $this->productRepository = $productRepository;
    }

    public function delete(
        Uuid $id
    ): void {

        $this->productRepository->delete($id);

        $this->flusher->flush();
    }
}
