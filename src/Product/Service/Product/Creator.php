<?php

declare(strict_types=1);

namespace App\Product\Service\Product;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Product\Dto\ProductPriceDto;
use App\Product\Dto\ProductSettingsDto;
use App\Product\Model\Product\Name;
use App\Product\Model\Product\Product;
use App\Product\Model\Product\ProductSettings;
use App\Product\Model\Section\Section;
use App\Product\Repository\ProductRepository;
use Assert\Assertion;

/**
 * Creator.
 */
class Creator
{
    private ProductRepository $productRepository;
    private Flusher $flusher;

    public function __construct(ProductRepository $productRepository, Flusher $flusher)
    {
        $this->productRepository = $productRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Section $section,
        Name $name,
        Amount $price,
        array $prices,
        ProductSettingsDto $productSettings
    ): Product {
        Assertion::allIsInstanceOf($prices, ProductPriceDto::class);

        $product = new Product(Uuid::generate(), $section, $name, $price, true);

        $product->addProductSettings(
            Uuid::generate(),
            $product,
            $productSettings->getCardType(),
            $productSettings->getAge(),
            $productSettings->getAmountOfDays(),
            $productSettings->getRounds(),
            $productSettings->getPpsProductId(),
            $productSettings->getNewCardPpsProductId(),
            $productSettings->getOldCardPpsProductId()
        );

        /** @var ProductPriceDto $productPriceDto */
        foreach ($prices as $productPriceDto) {
            $product->addProductPrice(
                Uuid::generate(),
                $productPriceDto->getAmount(),
                $productPriceDto->getDateStart(),
                $productPriceDto->getDateEnd(),
                true,
                $productPriceDto->getPpsPeriod(),
            );
        }

        $this->productRepository->add($product);
        $this->flusher->flush();

        return $product;
    }
}
