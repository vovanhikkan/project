<?php

declare(strict_types=1);

namespace App\Product\Service\Quota;

use App\Data\Flusher;
use App\Product\Model\Quota\Quota;
use App\Product\Model\Quota\Name;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\Product\Repository\QuotaRepository;

/**
 * Updater.
 */
class Updater
{
    private QuotaRepository $quotaRepository;
    private Flusher $flusher;

    public function __construct(QuotaRepository $quotaRepository, Flusher $flusher)
    {
        $this->quotaRepository = $quotaRepository;
        $this->flusher = $flusher;
    }

    public function update(Quota $quota, ?Name $name, ?QuotaGroup $group, ?bool $isDependsOnDate, ?bool $hasValuesByDays, ?array $products): Quota
    {
        $quota->update($group, $name, $isDependsOnDate, $hasValuesByDays, $products);

        $this->quotaRepository->add($quota);
        $this->flusher->flush();

        return $quota;
    }
}