<?php

declare(strict_types=1);

namespace App\Product\Service\Quota;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Product\Model\Quota\Quota;
use App\Product\Model\Quota\Name;
use App\Product\Model\QuotaGroup\QuotaGroup;
use App\Product\Repository\QuotaRepository;

/**
 * Creator.
 */
class Creator
{
    private QuotaRepository $quotaRepository;
    private Flusher $flusher;

    public function __construct(QuotaRepository $quotaRepository, Flusher $flusher)
    {
        $this->quotaRepository = $quotaRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Name $name,
        QuotaGroup $group,
        bool $isDependsOnDate,
        ?bool $hasValuesByDays,
        ?array $products
    ): Quota {
        $sort = $this->quotaRepository->getMaxSort() + 1;
        $quota = new Quota(Uuid::generate(), $group, $name, $isDependsOnDate, new Sort($sort), true);

        $quota->setHasValuesByDays($hasValuesByDays);
        
        foreach ($products as $product) {
            $quota->addProduct($product);
        }

        $this->quotaRepository->add($quota);
        $this->flusher->flush();

        return $quota;
    }
}
