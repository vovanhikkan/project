<?php

declare(strict_types=1);

namespace App\Product\Service\Section;

use App\Data\Flusher;
use App\Product\Dto\SectionSettingsDto;
use App\Product\Model\Section\Code;
use App\Product\Model\Section\Description;
use App\Product\Model\Section\InnerDescription;
use App\Product\Model\Section\InnerTitle;
use App\Product\Model\Section\Name;
use App\Product\Model\Section\Section;
use App\Product\Model\Section\TariffDescription;
use App\Storage\Model\File\File;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function update(
        Section $section,
        Code $code,
        Name $name,
        Description $description,
        TariffDescription $tariffDescription,
        InnerTitle $innerTitle,
        InnerDescription $innerDescription,
        SectionSettingsDto $sectionSettingsDto,
        ?File $picture,
        ?File $webIcon,
        ?File $webBackground,
        ?File $mobIcon,
        ?File $mobBackground
    ): void {
        $section->update(
            $code,
            $name,
            $description,
            $tariffDescription,
            $innerTitle,
            $innerDescription,
            $picture,
            $webIcon,
            $webBackground,
            $mobIcon,
            $mobBackground
        );

        $section->getSectionSettings()->update(
            $sectionSettingsDto->getControls(),
            $sectionSettingsDto->isOnlineOnly(),
            $sectionSettingsDto->isActivationDateRequired(),
            $sectionSettingsDto->getActiveDays(),
            $sectionSettingsDto->isNotQuantitative()
        );

        $this->flusher->flush();
    }
}
