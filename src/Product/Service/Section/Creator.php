<?php

declare(strict_types=1);

namespace App\Product\Service\Section;

use App\Application\ValueObject\Sort;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Product\Dto\SectionSettingsDto;
use App\Product\Model\Section\Code;
use App\Product\Model\Section\Description;
use App\Product\Model\Section\InnerDescription;
use App\Product\Model\Section\InnerTitle;
use App\Product\Model\Section\Name;
use App\Product\Model\Section\Section;
use App\Product\Model\Section\SectionSettings;
use App\Product\Model\Section\TariffDescription;
use App\Product\Repository\SectionRepository;
use App\Storage\Model\File\File;

/**
 * Creator.
 */
class Creator
{
    private SectionRepository $sectionRepository;
    private Flusher $flusher;

    public function __construct(SectionRepository $sectionRepository, Flusher $flusher)
    {
        $this->sectionRepository = $sectionRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Code $code,
        Name $name,
        Description $description,
        TariffDescription $tariffDescription,
        InnerTitle $innerTitle,
        InnerDescription $innerDescription,
        SectionSettingsDto $sectionSettingsDto,
        ?File $picture,
        ?File $webIcon,
        ?File $webBackground,
        ?File $mobIcon,
        ?File $mobBackground
    ): Section {
        $recommendationSort = $this->sectionRepository->getMaxSort() + 1;

        $section = new Section(
            Uuid::generate(),
            $code,
            $name,
            $description,
            $tariffDescription,
            $innerTitle,
            $innerDescription,
            new Sort($recommendationSort),
            true
        );

        if ($picture) {
            $section->setPicture($picture);
        }
        if ($webIcon) {
            $section->setWebIcon($webIcon);
        }
        if ($webBackground) {
            $section->setWebBackground($webBackground);
        }
        if ($mobIcon) {
            $section->setMobileIcon($mobIcon);
        }
        if ($mobBackground) {
            $section->setMobileBackground($mobBackground);
        }

        $section->setSectionSettings(new SectionSettings(
            $sectionSettingsDto->getId(),
            $section,
            $sectionSettingsDto->getControls(),
            $sectionSettingsDto->isOnlineOnly(),
            $sectionSettingsDto->isActivationDateRequired(),
            $sectionSettingsDto->getActiveDays(),
            $sectionSettingsDto->isNotQuantitative()
        ));

        $this->sectionRepository->add($section);
        $this->flusher->flush();

        return $section;
    }
}
