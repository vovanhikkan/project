<?php

declare(strict_types=1);

namespace App\Product\Service\Section;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Product\Repository\SectionRepository;

/**
 * Deleter.
 */
class Deleter
{
    private Flusher $flusher;
    private SectionRepository $sectionRepository;

    public function __construct(Flusher $flusher, SectionRepository $sectionRepository)
    {
        $this->flusher = $flusher;
        $this->sectionRepository = $sectionRepository;
    }

    public function delete(
        Uuid $id
    ): void {

        $this->sectionRepository->delete($id);

        $this->flusher->flush();
    }
}
