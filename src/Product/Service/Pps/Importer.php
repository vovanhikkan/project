<?php

declare(strict_types=1);

namespace App\Product\Service\Pps;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Pps\Service\External\ServiceChecker;
use App\Pps\Service\External\ServiceFetcher;
use App\Product\Model\Product\Name;
use App\Product\Model\Product\Product;
use App\Product\Repository\ProductRepository;
use DateTimeImmutable;
use Psr\Log\LoggerInterface;

/**
 * Importer.
 */
class Importer
{
    private ServiceFetcher $serviceFetcher;
    private ServiceChecker $serviceChecker;
    private ProductRepository $productRepository;
    private Flusher $flusher;
    private LoggerInterface $logger;

    public function __construct(
        ServiceFetcher $serviceFetcher,
        ServiceChecker $serviceChecker,
        ProductRepository $productRepository,
        Flusher $flusher,
        LoggerInterface $logger
    ) {
        $this->serviceFetcher = $serviceFetcher;
        $this->serviceChecker = $serviceChecker;
        $this->productRepository = $productRepository;
        $this->flusher = $flusher;
        $this->logger = $logger;
    }

    public function import(): void
    {
        $services = $this->serviceFetcher->fetchAll();
        foreach ($services as $service) {
            $ppsProductId = new Id($service->ID);

            $product = $this->productRepository->fetchOneByPpsProductId($ppsProductId);
            if ($product === null) {
                $product = $this->productRepository->fetchOneByNewCardPpsProductId($ppsProductId);
                if ($product === null) {
                    continue;
                }
            }

            $product->changeName(new Name($service->Name));
            $this->processPeriods($product, $service);
        }

        $this->flusher->flush();
    }

    private function processPeriods(Product $product, object $service): void
    {
        if ($service->Periods === null || $service->Periods->ServicePeriod === null) {
            return;
        }
        $periods = $service->Periods->ServicePeriod;
        $periods = is_array($periods) ? $periods : [$periods];
        foreach ($periods as $period) {
            $ppsPeriodId = new Id((int)$period->ID);

            $productPrice = null;
            foreach ($product->getProductPrices() as $item) {
                if ($item->getPpsPeriodId()->isEqualTo($ppsPeriodId)) {
                    $productPrice = $item;
                    break;
                }
            }

            if ($period->Start) {
                $dateStart = new DateTimeImmutable($period->Start);
            } else {
                continue;
            }
            if ($period->End) {
                $dateEnd = new DateTimeImmutable($period->End);
            } else {
                continue;
            }


            foreach ($service->Parameters->ServiceParameter as $parameter) {
                if ($parameter->ID === 'TimeRelease') {
                    $parameter->Value = $dateEnd->format('d.m.Y');
                    break;
                }
            }

            $result = $this->serviceChecker->check($service);
            if ($result === null) {
                continue;
            }

            $amount = Amount::fromRoubles($result->Price);

            if ($productPrice === null) {
                $product->addProductPrice(
                    Uuid::generate(),
                    $amount,
                    $dateStart,
                    $dateEnd,
                    $period->IsActive,
                    $ppsPeriodId
                );
            } else {
                $productPrice->update(
                    $amount,
                    $dateStart,
                    $dateEnd,
                    $period->IsActive,
                    $ppsPeriodId
                );
            }

            $this->logger->info('pps product period imported success', [$ppsPeriodId->getValue()]);
        }
    }
}
