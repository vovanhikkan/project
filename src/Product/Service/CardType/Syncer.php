<?php

declare(strict_types=1);

namespace App\Product\Service\CardType;

use App\Application\ValueObject\Amount;
use App\Application\ValueObject\Id;
use App\Data\Flusher;
use App\Pps\Service\External\ServiceFetcher;
use App\Product\Repository\CardTypeRepository;
use DateTimeImmutable;
use Psr\Log\LoggerInterface;

/**
 * Syncer.
 */
class Syncer
{
    private ServiceFetcher $serviceFetcher;
    private CardTypeRepository $cardTypeRepository;
    private Flusher $flusher;
    private LoggerInterface $logger;

    public function __construct(
        ServiceFetcher $serviceFetcher,
        CardTypeRepository $cardTypeRepository,
        Flusher $flusher,
        LoggerInterface $logger
    ) {
        $this->serviceFetcher = $serviceFetcher;
        $this->cardTypeRepository = $cardTypeRepository;
        $this->flusher = $flusher;
        $this->logger = $logger;
    }

    public function sync(): void
    {
        $services = $this->serviceFetcher->fetchAll();
        foreach ($services as $service) {
            $ppsProductId = new Id($service->ID);

            $cardType = $this->cardTypeRepository->fetchOneByPpsProductId($ppsProductId);
            if ($cardType === null) {
                continue;
            }

            $period = $service->Periods->ServicePeriod;

            $dateStart = new DateTimeImmutable($period->Start);
            $dateEnd = new DateTimeImmutable($period->End);

            foreach ($service->Parameters->ServiceParameter as $parameter) {
                if ($parameter->ID === 'TimeRelease') {
                    $parameter->Value = $dateEnd->format('d.m.Y');
                    break;
                }
            }

            $result = $this->client->checkService($service);
            if ($result === null) {
                continue;
            }

            $cardType->changePrice(Amount::fromRoubles($result->Price));
        }

        $this->flusher->flush();
    }
}
