<?php

declare(strict_types=1);

namespace App\Product\Service\QuotaValue;

use App\Application\ValueObject\Uuid;
use App\Data\Flusher;
use App\Product\Model\Quota\Quota;
use App\Product\Model\QuotaValue\HoldedQuantity;
use App\Product\Model\QuotaValue\OrderedQuantity;
use App\Product\Model\QuotaValue\QuotaValue;
use App\Product\Model\QuotaValue\TotalQuantity;
use App\Product\Repository\QuotaValueRepository;
use DateTimeImmutable;

/**
 * Creator.
 */
class Creator
{
    private QuotaValueRepository $quotaValueRepository;
    private Flusher $flusher;

    public function __construct(QuotaValueRepository $quotaValueRepository, Flusher $flusher)
    {
        $this->quotaValueRepository = $quotaValueRepository;
        $this->flusher = $flusher;
    }

    public function create(
        Quota $quota,
        int $totalQuantity,
        int $holdedQuantity,
        int $orderedQuantity,
        ?string $dateFrom,
        ?string $dateTo
    ): QuotaValue {
        $quotaValue = new QuotaValue(
            Uuid::generate(),
            $quota,
            new TotalQuantity($totalQuantity),
            new HoldedQuantity($holdedQuantity),
            new OrderedQuantity($orderedQuantity)
        );

        $quotaValue->setDateFrom($dateFrom ? new DateTimeImmutable($dateFrom) : null);
        $quotaValue->setDateTo($dateTo ? new DateTimeImmutable($dateTo) : null);

        $this->quotaValueRepository->add($quotaValue);
        $this->flusher->flush();

        return $quotaValue;
    }
}
