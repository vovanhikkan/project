<?php

declare(strict_types=1);

namespace App\Product\Service\QuotaValue;

use App\Data\Flusher;
use App\Product\Model\Quota\Quota;
use App\Product\Model\QuotaValue\QuotaValue;

/**
 * Updater.
 */
class Updater
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function update(
        QuotaValue $quotaValue,
        ?Quota $quota,
        ?int $totalQuantity,
        ?int $holdedQuantity,
        ?int $orderedQuantity,
        ?string $dateFrom,
        ?string $dateTo
    ): void {

        $quotaValue->update(
            $quota,
            $totalQuantity,
            $holdedQuantity,
            $orderedQuantity,
            $dateFrom ? new \DateTimeImmutable($dateFrom) : null,
            $dateTo ? new \DateTimeImmutable($dateTo) : null,
        );

        $this->flusher->flush();
    }
}
