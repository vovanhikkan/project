<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

use App\Application\Exception\DomainException;
use App\Application\Exception\EmailAlreadyExistsException;
use App\Application\Exception\InvalidArgumentException;
use App\Application\Exception\InvalidStatusTransitionException;
use App\Application\Exception\PhoneAlreadyExistsException;
use App\Application\Model\TimestampableTrait;
use App\Application\ValueObject\Email;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Uuid;
use App\Auth\Service\User\PasswordManager;
use App\Auth\Specification\User\UniqueEmailSpecification;
use App\Auth\Specification\User\UniquePhoneSpecification;
use App\Hotel\Model\Guest\Guest;
use App\Loyalty\Model\Level\Level;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Throwable;

/**
 * User.
 *
 * @ORM\Entity()
 * @ORM\Table(name="user")
 */
class User
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="auth_user_role", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Role $role;

    /**
     * @ORM\Column(type="email")
     */
    private Email $email;

    /**
     * @ORM\Column(type="phone", nullable=true)
     */
    private ?Phone $phone = null;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private string $passwordHash;

    /**
     * @ORM\Column(type="auth_user_last_name")
     */
    private LastName $lastName;

    /**
     * @ORM\Column(type="auth_user_first_name")
     */
    private FirstName $firstName;

    /**
     * @ORM\Column(type="auth_user_middle_name", nullable=true)
     */
    private ?MiddleName $middleName = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $birthDate = null;

    /**
     * @ORM\Column(type="auth_user_sex", nullable=true, columnDefinition="SMALLINT(3) UNSIGNED DEFAULT NULL")
     */
    private ?Sex $sex = null;

    /**
     * @ORM\Column(type="auth_user_status", columnDefinition="SMALLINT(3) UNSIGNED NOT NULL")
     */
    private Status $status;

    /**
     * @ORM\Embedded(class="Token")
     */
    private ?Token $resetPasswordToken = null;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0")
     */
    private bool $isConfirmedServiceLetters;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0")
     */
    private bool $isConfirmedMailing;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0")
     */
    private bool $isEmailVerified;

    /**
     * @ORM\Embedded(class="Token")
     */
    private ?Token $verifyEmailToken = null;

    /**
     * @ORM\Column(type="boolean", columnDefinition="SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0")
     */
    private bool $isPhoneVerified;

    /**
     * @ORM\Embedded(class="Token")
     */
    private ?Token $verifyPhoneToken = null;

    /**
     * @ORM\OneToMany(targetEntity="UserRefreshToken", mappedBy="user", cascade="all")
     */
    private Collection $userRefreshTokens;

    /**
     * @ORM\ManyToOne(targetEntity="App\Loyalty\Model\Level\Level", inversedBy="user")
     */
    private ?Level $level = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Hotel\Model\Guest\Guest")
     * @ORM\JoinTable(name="user_guest",
     *  joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="guest_id", referencedColumnName="id")}
     * )
     */
    private Collection $guests;


    public function __construct(
        Uuid $id,
        Role $role,
        Email $email,
        ?Phone $phone,
        string $password,
        LastName $lastName,
        FirstName $firstName,
        ?MiddleName $middleName,
        ?DateTimeImmutable $birthDate,
        ?Sex $sex,
        bool $isConfirmedServiceLetters,
        bool $isConfirmedMailing,
        PasswordManager $passwordManager,
        UniqueEmailSpecification $uniqueEmailSpecification,
        UniquePhoneSpecification $uniquePhoneSpecification
    ) {
        $this->id = $id;
        $this->role = $role;
        $this->email = $email;
        $this->phone = $phone;
        $this->passwordHash = $passwordManager->hash($password);
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->birthDate = $birthDate;
        $this->sex = $sex;
        $this->status = Status::active();
        $this->isConfirmedServiceLetters = $isConfirmedServiceLetters;
        $this->isConfirmedMailing = $isConfirmedMailing;
        $this->isEmailVerified = false;
        $this->isPhoneVerified = false;
        $this->userRefreshTokens = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
        $this->guests = new ArrayCollection();


        if (!$uniqueEmailSpecification->isSatisfiedBy($this)) {
            throw new EmailAlreadyExistsException($this->email);
        }

        if (!$uniquePhoneSpecification->isSatisfiedBy($this)) {
            throw new PhoneAlreadyExistsException($this->phone);
        }
    }

    public function login(string $password, PasswordManager $passwordManager, Token $refreshToken)
    {
        if (!$this->status->isActive()) {
            throw new DomainException('Пользователь не активен');
        }

        if (!$passwordManager->validate($password, $this->passwordHash)) {
            throw new InvalidArgumentException('Неверный пароль');
        }

        $this->userRefreshTokens->add(
            new UserRefreshToken(
                Uuid::generate(),
                $this,
                $refreshToken
            )
        );

        $this->updatedAt = new DateTimeImmutable();
    }

    public function refresh(string $token, Token $refreshToken)
    {
        if (!$this->status->isActive()) {
            throw new DomainException('Пользователь не активен');
        }

        $valid = false;

        /** @var UserRefreshToken $userRefreshToken */
        foreach ($this->userRefreshTokens as $userRefreshToken) {
            try {
                $userRefreshToken->getToken()->validate($token);
                $valid = true;
                $userRefreshToken->refresh($refreshToken);
            } catch (Throwable $e) {
                continue;
            }
        }

        if (!$valid) {
            throw new InvalidArgumentException('Неверный токен');
        }

        $this->updatedAt = new DateTimeImmutable();
    }

    public function resetPasswordRequest(Token $token): void
    {
        if (!$this->status->isActive()) {
            throw new DomainException('Пользователь не активен');
        }

        $this->resetPasswordToken = $token;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function resetPasswordConfirm(string $token, string $password, PasswordManager $passwordManager): void
    {
        if (!$this->status->isActive()) {
            throw new DomainException('Пользователь не активен');
        }

        if ($this->resetPasswordToken === null) {
            throw new DomainException('Сброс пароля не запрашивался');
        }

        $this->resetPasswordToken->validate($token);
        $this->resetPasswordToken = null;
        $this->passwordHash = $passwordManager->hash($password);
        $this->updatedAt = new DateTimeImmutable();
    }

    public function changePassword(
        string $passwordCurrent,
        string $passwordNew,
        PasswordManager $passwordManager
    ): void {
        if (!$passwordManager->validate($passwordCurrent, $this->passwordHash)) {
            throw new InvalidArgumentException('Неверный пароль');
        }

        $this->passwordHash = $passwordManager->hash($passwordNew);
        $this->updatedAt = new DateTimeImmutable();
    }

    public function changeProfile(
        Email $email,
        ?Phone $phone,
        LastName $lastName,
        FirstName $firstName,
        ?MiddleName $middleName,
        ?DateTimeImmutable $birthDate,
        ?Sex $sex,
        UniqueEmailSpecification $uniqueEmailSpecification,
        UniquePhoneSpecification $uniquePhoneSpecification,
        bool $isConfirmedMailing,
        bool $isConfirmedServiceLetters
    ): void {
        if ($email->getValue() !== $this->email->getValue()) {
            $this->isEmailVerified = false;
        }
        $this->email = $email;
        if (($phone !== null ? $phone->getValue() : null)
            !== ($this->phone !== null ? $this->phone->getValue() : null)) {
            $this->isPhoneVerified = false;
        }
        $this->phone = $phone;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->birthDate = $birthDate;
        $this->sex = $sex;
        $this->isConfirmedMailing = $isConfirmedMailing;
        $this->isConfirmedServiceLetters = $isConfirmedServiceLetters;
        $this->updatedAt = new DateTimeImmutable();

        if (!$uniqueEmailSpecification->isSatisfiedBy($this)) {
            throw new EmailAlreadyExistsException($this->email);
        }

        if (!$uniquePhoneSpecification->isSatisfiedBy($this)) {
            throw new PhoneAlreadyExistsException($this->phone);
        }
    }

    public function block(): void
    {
        if ($this->status->isBlocked()) {
            throw new InvalidStatusTransitionException('Пользователь уже заблокирован');
        }

        $this->status = Status::blocked();
        $this->updatedAt = new DateTimeImmutable();
    }

    public function verifyEmailRequest(Token $token): void
    {
        if (!$this->status->isActive()) {
            throw new InvalidStatusTransitionException('Пользователь не активен');
        }

        $this->verifyEmailToken = $token;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function verifyEmailConfirm(string $token): void
    {
        if (!$this->status->isActive()) {
            throw new InvalidStatusTransitionException('Пользователь не активен');
        }

        if ($this->verifyEmailToken === null) {
            throw new DomainException('Подтверждение email не запрашивалось');
        }

        $this->verifyEmailToken->validate($token);
        $this->isEmailVerified = true;
        $this->verifyEmailToken = null;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function verifyPhoneRequest(Token $token): void
    {
        if (!$this->status->isActive()) {
            throw new InvalidStatusTransitionException('Пользователь не активен');
        }

        $this->verifyPhoneToken = $token;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function verifyPhoneConfirm(string $token): void
    {
        if (!$this->status->isActive()) {
            throw new InvalidStatusTransitionException('Пользователь не активен');
        }

        if ($this->verifyPhoneToken === null) {
            throw new DomainException('Подтверждение телефона не запрашивалось');
        }

        $this->verifyPhoneToken->validate($token);
        $this->isPhoneVerified = true;
        $this->verifyPhoneToken = null;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getPhone(): ?Phone
    {
        return $this->phone;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?MiddleName
    {
        return $this->middleName;
    }

    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    public function getSex(): ?Sex
    {
        return $this->sex;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getGuests() : array
    {
        return $this->guests->toArray();
    }

    public function isConfirmedServiceLetters(): bool
    {
        return $this->isConfirmedServiceLetters;
    }

    public function isConfirmedMailing(): bool
    {
        return $this->isConfirmedMailing;
    }

    public function isEmailVerified(): bool
    {
        return $this->isEmailVerified;
    }

    public function isPhoneVerified(): bool
    {
        return $this->isPhoneVerified;
    }

    /**
     * @return Level|null
     */
    public function getLevel(): ?Level
    {
        return $this->level;
    }
}
