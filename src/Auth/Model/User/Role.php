<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

use App\Application\ValueObject\EnumValueObject;

/**
 * Role.
 *
 * @method static customer()
 */
final class Role extends EnumValueObject
{
    const CUSTOMER = 100;
    const ADMIN = 200;

    public function isCustomer(): bool
    {
        return $this->value === self::CUSTOMER;
    }

    public function isAdmin(): bool
    {
        return $this->value === self::ADMIN;
    }
}
