<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

use App\Application\ValueObject\EnumValueObject;

/**
 * Sex.
 *
 * @method static male()
 * @method static female()
 */
class Sex extends EnumValueObject
{
    const MALE = 100;
    const FEMALE = 200;

    public function isMale(): bool
    {
        return $this->value === self::MALE;
    }

    public function isFemale(): bool
    {
        return $this->value === self::FEMALE;
    }
}
