<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use DomainException;
use InvalidArgumentException;

/**
 * Token.
 *
 * @ORM\Embeddable()
 */
class Token
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $value;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $expiredAt;

    public function __construct(string $value, DateTimeImmutable $expiredAt)
    {
        $this->value = $value;
        $this->expiredAt = $expiredAt;
    }

    public function validate(string $value): void
    {
        if ($this->value !== $value) {
            throw new InvalidArgumentException('Неверный токен');
        }
        if (!($this->expiredAt !== null && $this->expiredAt > (new DateTimeImmutable()))) {
            throw new DomainException('Токен истёк');
        }
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getExpiredAt(): ?DateTimeImmutable
    {
        return $this->expiredAt;
    }
}
