<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

/**
 * LastName.
 */
class LastName extends Name
{
    public function getPropertyPath(): ?string
    {
        return 'lastName';
    }

    public function getPropertyLabel(): ?string
    {
        return 'Фамилия';
    }
}
