<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

/**
 * MiddleName.
 */
class MiddleName extends Name
{
    public function getPropertyPath(): ?string
    {
        return 'middleName';
    }

    public function getPropertyLabel(): ?string
    {
        return 'Отчество';
    }
}
