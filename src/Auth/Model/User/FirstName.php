<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

/**
 * FirstName.
 */
class FirstName extends Name
{
    public function getPropertyPath(): ?string
    {
        return 'firstName';
    }

    public function getPropertyLabel(): ?string
    {
        return 'Имя';
    }
}
