<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

use App\Application\ValueObject\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserRefreshToken.
 *
 * @ORM\Entity()
 * @ORM\Table(name="user_refresh_token")
 */
class UserRefreshToken
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userRefreshTokens")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @ORM\Embedded(class="App\Auth\Model\User\Token")
     */
    private ?Token $token = null;

    public function __construct(Uuid $id, User $user, Token $token)
    {
        $this->id = $id;
        $this->user = $user;
        $this->token = $token;
    }

    public function refresh(Token $token): void
    {
        $this->token = $token;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getToken(): Token
    {
        return $this->token;
    }
}
