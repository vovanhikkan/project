<?php

declare(strict_types=1);

namespace App\Auth\Model\User;

use App\Application\ValueObject\EnumValueObject;

/**
 * Status.
 *
 * @method static active()
 * @method static blocked()
 */
final class Status extends EnumValueObject
{
    const ACTIVE = 100;
    const BLOCKED = 200;

    public function isActive(): bool
    {
        return $this->value === self::ACTIVE;
    }

    public function isBlocked(): bool
    {
        return $this->value === self::BLOCKED;
    }
}
