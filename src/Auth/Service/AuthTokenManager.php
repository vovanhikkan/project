<?php

declare(strict_types=1);

namespace App\Auth\Service;

use Firebase\JWT\JWT;

/**
 * AuthTokenManager.
 */
class AuthTokenManager
{
    public function encode(array $data): string
    {
        $key = getenv('SECRET_KEY');
        return JWT::encode($data, $key, 'HS256');
    }

    public function decode(string $token): array
    {
        $key = getenv('SECRET_KEY');
        return (array)JWT::decode($token, $key, ['HS256']);
    }
}
