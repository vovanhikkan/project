<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use Assert\Assertion;

/**
 * PasswordManager.
 */
class PasswordManager
{
    public function hash(string $password): string
    {
        Assertion::notEmpty($password);
        return password_hash($password, PASSWORD_ARGON2I, ['memory_cost' => PASSWORD_ARGON2_DEFAULT_MEMORY_COST]);
    }

    public function validate(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
