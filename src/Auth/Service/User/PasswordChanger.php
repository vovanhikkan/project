<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Auth\Model\User\User;
use App\Data\Flusher;

/**
 * PasswordChanger.
 */
class PasswordChanger
{
    private PasswordManager $passwordManager;
    private Flusher $flusher;

    public function __construct(
        PasswordManager $passwordManager,
        Flusher $flusher
    ) {
        $this->passwordManager = $passwordManager;
        $this->flusher = $flusher;
    }

    public function changePassword(User $user, string $passwordCurrent, string $passwordNew): void
    {
        $user->changePassword(
            $passwordCurrent,
            $passwordNew,
            $this->passwordManager
        );

        $this->flusher->flush();
    }
}
