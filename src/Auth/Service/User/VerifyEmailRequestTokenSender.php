<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Application\ValueObject\Email;
use App\Service\MailerService;
use Slim\Views\Twig;

/**
 * VerifyEmailRequestTokenSender.
 */
class VerifyEmailRequestTokenSender
{
    private Twig $twig;
    private MailerService $mailerService;

    public function __construct(
        Twig $twig,
        MailerService $mailerService
    ) {
        $this->twig = $twig;
        $this->mailerService = $mailerService;
    }

    public function send(Email $email, string $token): void
    {
        $body = $this->twig->fetch('emails/verify_email_request.twig', [
            'url' => getenv('FRONT_URL') . 'profile/settings/?token=' . $token
        ]);

        $this->mailerService->send(
            'Подтверждение почты',
            (string)$email,
            $body
        );
    }
}
