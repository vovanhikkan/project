<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

/**
 * PasswordGenerator.
 */
class PasswordGenerator
{
    private const CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    public function generate(int $length = 6): string
    {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $character = self::CHARSET[mt_rand(0, strlen(self::CHARSET) - 1)];
            $result .= $character;
        }

        return $result;
    }
}
