<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Application\ValueObject\Email;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\FirstName;
use App\Auth\Model\User\LastName;
use App\Auth\Model\User\MiddleName;
use App\Auth\Model\User\Role;
use App\Auth\Model\User\Sex;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use App\Auth\Specification\User\UniqueEmailSpecification;
use App\Auth\Specification\User\UniquePhoneSpecification;
use App\Data\Flusher;
use DateInterval;
use DateTimeImmutable;

/**
 * Creator.
 */
class Creator
{
    private UserRepository $userRepository;
    private PasswordManager $passwordManager;
    private UniqueEmailSpecification $uniqueEmailSpecification;
    private UniquePhoneSpecification $uniquePhoneSpecification;
    private TokenGenerator $tokenGenerator;
    private Flusher $flusher;
    private RegisterConfirmationSender $registerConfirmationSender;

    public function __construct(
        UserRepository $userRepository,
        PasswordManager $passwordManager,
        UniqueEmailSpecification $uniqueEmailSpecification,
        UniquePhoneSpecification $uniquePhoneSpecification,
        Flusher $flusher,
        TokenGenerator $tokenGenerator,
        RegisterConfirmationSender $registerConfirmationSender
    ) {
        $this->userRepository = $userRepository;
        $this->passwordManager = $passwordManager;
        $this->uniqueEmailSpecification = $uniqueEmailSpecification;
        $this->uniquePhoneSpecification = $uniquePhoneSpecification;
        $this->flusher = $flusher;
        $this->registerConfirmationSender = $registerConfirmationSender;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function create(
        Uuid $id,
        Role $role,
        Email $email,
        ?Phone $phone,
        string $password,
        LastName $lastName,
        FirstName $firstName,
        ?MiddleName $middleName,
        ?DateTimeImmutable $birthDate,
        ?Sex $sex,
        bool $isConfirmedServiceLetters,
        bool $isConfirmedMailing
    ): User {
        $user = new User(
            $id,
            $role,
            $email,
            $phone,
            $password,
            $lastName,
            $firstName,
            $middleName,
            $birthDate,
            $sex,
            $isConfirmedServiceLetters,
            $isConfirmedMailing,
            $this->passwordManager,
            $this->uniqueEmailSpecification,
            $this->uniquePhoneSpecification
        );

        $token = $this->tokenGenerator->generate(new DateTimeImmutable(), new DateInterval('PT24H'));
        $user->verifyEmailRequest($token);


        $this->userRepository->add($user);
        $this->flusher->flush();
        $this->registerConfirmationSender->send($user->getEmail(), $password, $firstName->getValue(), $token->getValue());

        return $user;
    }
}
