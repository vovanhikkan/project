<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Auth\Model\User\Token;
use DateInterval;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

/**
 * TokenGenerator.
 */
class TokenGenerator
{
    public function generate(DateTimeImmutable $date, DateInterval $interval, bool $simple = false): Token
    {
        if ($simple) {
            $value = (string)mt_rand(100000, 999999);
        } else {
            $value = Uuid::uuid4()->toString();
        }

        return new Token(
            $value,
            $date->add($interval)
        );
    }
}
