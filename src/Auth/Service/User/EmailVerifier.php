<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Application\Exception\InvalidArgumentException;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use App\Data\Flusher;
use DateInterval;
use DateTimeImmutable;

/**
 * EmailVerifier.
 */
class EmailVerifier
{
    private UserRepository $userRepository;
    private TokenGenerator $tokenGenerator;
    private Flusher $flusher;
    private VerifyEmailRequestTokenSender $verifyEmailRequestTokenSender;

    public function __construct(
        UserRepository $userRepository,
        TokenGenerator $tokenGenerator,
        Flusher $flusher,
        VerifyEmailRequestTokenSender $verifyEmailRequestTokenSender
    ) {
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
        $this->flusher = $flusher;
        $this->verifyEmailRequestTokenSender = $verifyEmailRequestTokenSender;
    }

    public function request(User $user): void
    {
        $token = $this->tokenGenerator->generate(new DateTimeImmutable(), new DateInterval('PT24H'));
        $user->verifyEmailRequest($token);
        $this->flusher->flush();

        $this->verifyEmailRequestTokenSender->send($user->getEmail(), $token->getValue());
    }

    public function confirm(string $token): void
    {
        $user = $this->userRepository->fetchOneByVerifyEmailToken($token);

        if ($user === null) {
            throw new InvalidArgumentException('Не верный код для подтверждения email');
        }

        $user->verifyEmailConfirm($token);
        $this->flusher->flush();
    }
}
