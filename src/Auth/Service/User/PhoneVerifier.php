<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Application\Exception\InvalidArgumentException;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use App\Data\Flusher;
use DateInterval;
use DateTimeImmutable;

/**
 * PhoneVerifier.
 */
class PhoneVerifier
{
    private UserRepository $userRepository;
    private TokenGenerator $tokenGenerator;
    private Flusher $flusher;
    private VerifyPhoneRequestTokenSender $verifyPhoneRequestTokenSender;

    public function __construct(
        UserRepository $userRepository,
        TokenGenerator $tokenGenerator,
        Flusher $flusher,
        VerifyPhoneRequestTokenSender $verifyPhoneRequestTokenSender
    ) {
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
        $this->flusher = $flusher;
        $this->verifyPhoneRequestTokenSender = $verifyPhoneRequestTokenSender;
    }

    public function request(User $user): void
    {
        $token = $this->tokenGenerator->generate(new DateTimeImmutable(), new DateInterval('PT24H'), true);
        $user->verifyPhoneRequest($token);
        $this->flusher->flush();

        $this->verifyPhoneRequestTokenSender->send($user->getPhone(), $token->getValue());
    }

    public function confirm(string $token): void
    {
        $user = $this->userRepository->fetchOneByVerifyPhoneToken($token);

        if ($user === null) {
            throw new InvalidArgumentException('Не верный код для подтверждения');
        }

        $user->verifyPhoneConfirm($token);
        $this->flusher->flush();
    }
}
