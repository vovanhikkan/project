<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Application\ValueObject\Email;
use App\Service\MailerService;
use Slim\Views\Twig;

/**
 * RegisterConfirmationSender.
 */
class RegisterConfirmationSender
{
    private Twig $twig;
    private MailerService $mailerService;

    public function __construct(
        Twig $twig,
        MailerService $mailerService
    ) {
        $this->twig = $twig;
        $this->mailerService = $mailerService;
    }

    public function send(Email $email, string $password, string $name, string $token): void
    {
        $body = $this->twig->fetch('emails/registration.twig', [
            'password' => $password,
            'name' => $name,
            'email' => $email,
            'url' => getenv('FRONT_URL') . 'profile/settings/?token=' . $token
        ]);

        $this->mailerService->send(
            'Роза Хутор: Регистрация на сайте',
            (string)$email,
            $body
        );
    }
}
