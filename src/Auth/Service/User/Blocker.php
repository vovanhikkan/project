<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Auth\Model\User\User;
use App\Data\Flusher;

/**
 * Blocker.
 */
class Blocker
{
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function block(User $user): void
    {
        $user->block();
        $this->flusher->flush();
    }
}
