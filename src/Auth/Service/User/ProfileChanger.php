<?php

declare(strict_types=1);

namespace App\Auth\Service\User;

use App\Application\ValueObject\Email;
use App\Application\ValueObject\Phone;
use App\Auth\Model\User\FirstName;
use App\Auth\Model\User\LastName;
use App\Auth\Model\User\MiddleName;
use App\Auth\Model\User\Sex;
use App\Auth\Model\User\User;
use App\Auth\Specification\User\UniqueEmailSpecification;
use App\Auth\Specification\User\UniquePhoneSpecification;
use App\Data\Flusher;
use DateTimeImmutable;

/**
 * ProfileChanger.
 */
class ProfileChanger
{
    private UniqueEmailSpecification $uniqueEmailSpecification;
    private UniquePhoneSpecification $uniquePhoneSpecification;
    private Flusher $flusher;

    public function __construct(
        UniqueEmailSpecification $uniqueEmailSpecification,
        UniquePhoneSpecification $uniquePhoneSpecification,
        Flusher $flusher
    ) {
        $this->uniqueEmailSpecification = $uniqueEmailSpecification;
        $this->uniquePhoneSpecification = $uniquePhoneSpecification;
        $this->flusher = $flusher;
    }

    public function changeProfile(
        User $user,
        Email $email,
        ?Phone $phone,
        LastName $lastName,
        FirstName $firstName,
        ?MiddleName $middleName,
        ?DateTimeImmutable $birthDate,
        ?Sex $sex,
        bool $isConfirmedMailing,
        bool $isConfirmedServiceLetters
    ): void {
        $user->changeProfile(
            $email,
            $phone,
            $lastName,
            $firstName,
            $middleName,
            $birthDate,
            $sex,
            $this->uniqueEmailSpecification,
            $this->uniquePhoneSpecification,
            $isConfirmedMailing,
            $isConfirmedServiceLetters
        );

        $this->flusher->flush();

        // @todo send confirmation for change email and phone
    }
}
