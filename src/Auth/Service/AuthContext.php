<?php

declare(strict_types=1);

namespace App\Auth\Service;

use App\Application\Exception\NotFoundException;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use Exception;
use Psr\Http\Message\ServerRequestInterface;

/**
 * AuthContext.
 */
class AuthContext
{
    private AuthTokenManager $authTokenManager;
    private UserRepository $userRepository;
    private bool $isAuthenticated = false;
    private ?User $currentUser = null;

    public function __construct(AuthTokenManager $authTokenManager, UserRepository $userRepository)
    {
        $this->authTokenManager = $authTokenManager;
        $this->userRepository = $userRepository;
    }

    public function handleRequest(ServerRequestInterface $request): void
    {
        $token = $request->getHeaderLine('Authorization');
        $token = str_replace(['Bearer', ' '],'', $token);

        if ($token) {
            $data = null;
            try {
                $data = $this->authTokenManager->decode($token);
            } catch (Exception $e) {
            }

            if (is_array($data)) {
                try {
                    $currentUser = $this->userRepository->get(new Uuid($data['id']));
                } catch (NotFoundException $e) {
                    return;
                }

                if (!$currentUser->getStatus()->isActive()) {
                    return;
                }

                $this->currentUser = $currentUser;
                $this->isAuthenticated = true;
            }
        }
    }

    public function isAuthenticated(): bool
    {
        return $this->isAuthenticated;
    }

    /**
     * @return User|null
     */
    public function getCurrentUser(): ?User
    {
        return $this->currentUser;
    }

    public function isConsole(): bool
    {
        return defined('APP_CONSOLE') && APP_CONSOLE === true;
    }
}
