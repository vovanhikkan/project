<?php

declare(strict_types=1);

namespace App\Auth\Specification\User;

use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;

/**
 * UniqueEmailSpecification.
 */
class UniqueEmailSpecification
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function isSatisfiedBy(User $user): bool
    {
        return !$this->userRepository->existByEmail($user->getEmail(), $user->getId());
    }
}
