<?php

declare(strict_types=1);

namespace App\Auth\Command\User\Create;

use App\Application\ValueObject\Email;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\FirstName;
use App\Auth\Model\User\LastName;
use App\Auth\Model\User\MiddleName;
use App\Auth\Model\User\Role;
use App\Auth\Model\User\Sex;
use App\Auth\Model\User\User;
use App\Auth\Service\User\Creator;
use App\Auth\Service\User\PasswordGenerator;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;
    private PasswordGenerator $passwordGenerator;

    public function __construct(
        Creator $creator,
        PasswordGenerator $passwordGenerator
    ) {
        $this->creator = $creator;
        $this->passwordGenerator = $passwordGenerator;
    }

    public function handle(Command $command): User
    {
        return $this->creator->create(
            Uuid::generate(),
            Role::customer(),
            new Email($command->getEmail()),
            $command->getPhone() !== null ? new Phone($command->getPhone()) : null,
            $this->passwordGenerator->generate(),
            new LastName($command->getLastName()),
            new FirstName($command->getFirstName()),
            $command->getMiddleName() !== null ? new MiddleName($command->getMiddleName()) : null,
            $command->getBirthDate() !== null ? new DateTimeImmutable($command->getBirthDate()) : null,
            $command->getSex() !== null ? new Sex($command->getSex()) : null,
            $command->isConfirmedServiceLetters(),
            $command->isConfirmedMailing()
        );
    }
}
