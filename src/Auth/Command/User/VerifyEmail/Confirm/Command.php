<?php

declare(strict_types=1);

namespace App\Auth\Command\User\VerifyEmail\Confirm;

/**
 * Command.
 */
class Command
{
    private string $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
