<?php

declare(strict_types=1);

namespace App\Auth\Command\User\VerifyEmail\Confirm;

use App\Auth\Service\User\EmailVerifier;

/**
 * Handler.
 */
class Handler
{
    private EmailVerifier $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    public function handle(Command $command): void
    {
        $this->emailVerifier->confirm($command->getToken());
    }
}
