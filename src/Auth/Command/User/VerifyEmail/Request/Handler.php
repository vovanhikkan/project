<?php

declare(strict_types=1);

namespace App\Auth\Command\User\VerifyEmail\Request;

use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\User\EmailVerifier;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private EmailVerifier $emailVerifier;

    public function __construct(
        UserRepository $userRepository,
        EmailVerifier $emailVerifier
    ) {
        $this->userRepository = $userRepository;
        $this->emailVerifier = $emailVerifier;
    }

    public function handle(Command $command): void
    {
        $user = $this->userRepository->get(new Uuid($command->getId()));
        $this->emailVerifier->request($user);
    }
}
