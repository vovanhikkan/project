<?php

declare(strict_types=1);

namespace App\Auth\Command\User\Register;

use App\Application\ValueObject\Email;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\FirstName;
use App\Auth\Model\User\LastName;
use App\Auth\Model\User\MiddleName;
use App\Auth\Model\User\Role;
use App\Auth\Model\User\Sex;
use App\Auth\Service\AuthTokenManager;
use App\Auth\Service\User\Creator;
use App\Auth\Service\User\PasswordGenerator;
use App\Auth\Service\User\PasswordManager;
use App\Auth\Service\User\TokenGenerator;
use DateInterval;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private Creator $creator;
    private PasswordGenerator $passwordGenerator;
    private TokenGenerator $tokenGenerator;
    private PasswordManager $passwordManager;
    private AuthTokenManager $authTokenManager;

    public function __construct(
        Creator $creator,
        PasswordGenerator $passwordGenerator,
        TokenGenerator $tokenGenerator,
        PasswordManager $passwordManager,
        AuthTokenManager $authTokenManager
    ) {
        $this->creator = $creator;
        $this->passwordGenerator = $passwordGenerator;
        $this->tokenGenerator = $tokenGenerator;
        $this->passwordManager = $passwordManager;
        $this->authTokenManager = $authTokenManager;
    }

    public function handle(Command $command): array
    {
        $pass = $this->passwordGenerator->generate();
        $user = $this->creator->create(
            Uuid::generate(),
            Role::customer(),
            new Email($command->getEmail()),
            $command->getPhone() !== null ? new Phone($command->getPhone()) : null,
            $pass,
            new LastName($command->getLastName()),
            new FirstName($command->getFirstName()),
            $command->getMiddleName() !== null ? new MiddleName($command->getMiddleName()) : null,
            $command->getBirthDate() !== null ? new DateTimeImmutable($command->getBirthDate()) : null,
            $command->getSex() !== null ? new Sex($command->getSex()) : null,
            $command->isConfirmedServiceLetters(),
            $command->isConfirmedMailing()
        );

        $refreshToken = $this->tokenGenerator->generate(
            new DateTimeImmutable(),
            new DateInterval('P1Y')
        );

        $user->login(
            $pass,
            $this->passwordManager,
            $refreshToken
        );

        $data = [
            'alg' => 'RS256',
            'typ' => 'JWT',
            'exp' => time() + 3600 * 24 * 30,
            'id' => (string)$user->getId()
        ];

        return [
            'accessToken' => $this->authTokenManager->encode($data),
            'refreshToken' => $refreshToken->getValue()
        ];
    }
}
