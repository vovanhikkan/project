<?php

declare(strict_types=1);

namespace App\Auth\Command\User\ChangeProfile;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните id")
     */
    private string $id;

    /**
     * @Assert\NotBlank(message="Заполните e-mail")
     */
    private string $email;

    private ?string $phone = null;

    /**
     * @Assert\NotBlank(message="Заполните фамилию")
     */
    private string $lastName;

    /**
     * @Assert\NotBlank(message="Заполните имя")
     */
    private string $firstName;

    private ?string $middleName = null;

    /**
     * @Assert\Date()
     */
    private ?string $birthDate = null;

    private ?int $sex = null;

    private bool $isConfirmedServiceLetters;

    private bool $isConfirmedMailing;

    public function __construct(
        string $id,
        string $email,
        string $lastName,
        string $firstName,
        bool $isConfirmedServiceLetters,
        bool $isConfirmedMailing
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->isConfirmedServiceLetters = $isConfirmedServiceLetters;
        $this->isConfirmedMailing = $isConfirmedMailing;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    public function setMiddleName(?string $middleName): void
    {
        $this->middleName = $middleName;
    }

    public function setBirthDate(?string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    public function setSex(?int $sex): void
    {
        $this->sex = $sex;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    public function getSex(): ?int
    {
        return $this->sex;
    }

    public function isConfirmedServiceLetters(): bool
    {
        return $this->isConfirmedServiceLetters;
    }

    public function isConfirmedMailing(): bool
    {
        return $this->isConfirmedMailing;
    }
}
