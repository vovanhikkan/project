<?php

declare(strict_types=1);

namespace App\Auth\Command\User\ChangeProfile;

use App\Application\ValueObject\Email;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\FirstName;
use App\Auth\Model\User\LastName;
use App\Auth\Model\User\MiddleName;
use App\Auth\Model\User\Sex;
use App\Auth\Model\User\User;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\User\ProfileChanger;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private ProfileChanger $profileChanger;

    public function __construct(
        UserRepository $userRepository,
        ProfileChanger $profileChanger
    ) {
        $this->userRepository = $userRepository;
        $this->profileChanger = $profileChanger;
    }

    public function handle(Command $command): User
    {
        $user = $this->userRepository->get(new Uuid($command->getId()));

        $this->profileChanger->changeProfile(
            $user,
            new Email($command->getEmail()),
            $command->getPhone() !== null ? new Phone($command->getPhone()) : null,
            new LastName($command->getLastName()),
            new FirstName($command->getFirstName()),
            $command->getMiddleName() !== null ? new MiddleName($command->getMiddleName()) : null,
            $command->getBirthDate() !== null ? new DateTimeImmutable($command->getBirthDate()) : null,
            $command->getSex() !== null ? new Sex($command->getSex()) : null,
            $command->isConfirmedMailing(),
            $command->isConfirmedServiceLetters()
        );

        return $user;
    }
}
