<?php

declare(strict_types=1);

namespace App\Auth\Command\User\Login;

use App\Application\ValueObject\Email;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\AuthTokenManager;
use App\Auth\Service\User\PasswordManager;
use App\Auth\Service\User\TokenGenerator;
use App\Data\Flusher;
use DateInterval;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private PasswordManager $passwordManager;
    private AuthTokenManager $authTokenManager;
    private TokenGenerator $tokenGenerator;
    private Flusher $flusher;

    public function __construct(
        UserRepository $userRepository,
        PasswordManager $passwordManager,
        AuthTokenManager $authTokenManager,
        TokenGenerator $tokenGenerator,
        Flusher $flusher
    ) {
        $this->userRepository = $userRepository;
        $this->passwordManager = $passwordManager;
        $this->authTokenManager = $authTokenManager;
        $this->tokenGenerator = $tokenGenerator;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): array
    {
        $user = $this->userRepository->getByEmail(new Email($command->getEmail()));

        $refreshToken = $this->tokenGenerator->generate(
            new DateTimeImmutable(),
            new DateInterval('P1Y')
        );

        $user->login(
            $command->getPassword(),
            $this->passwordManager,
            $refreshToken
        );

        $data = [
            'alg' => 'RS256',
            'typ' => 'JWT',
            'exp' => time() + 3600 * 24 * 30,
            'id' => (string)$user->getId()
        ];

        return [
            'accessToken' => $this->authTokenManager->encode($data),
            'refreshToken' => $refreshToken->getValue()
        ];
    }
}
