<?php

declare(strict_types=1);

namespace App\Auth\Command\User\Login;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните e-mail")
     */
    private string $email;

    /**
     * @Assert\NotBlank(message="Заполните пароль")
     */
    private string $password;

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
