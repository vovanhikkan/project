<?php

declare(strict_types=1);

namespace App\Auth\Command\User\Refresh;

use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\AuthTokenManager;
use App\Auth\Service\User\TokenGenerator;
use App\Data\Flusher;
use DateInterval;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private TokenGenerator $tokenGenerator;
    private Flusher $flusher;
    private AuthTokenManager $authTokenManager;

    public function __construct(
        UserRepository $userRepository,
        TokenGenerator $tokenGenerator,
        Flusher $flusher,
        AuthTokenManager $authTokenManager
    ) {
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
        $this->flusher = $flusher;
        $this->authTokenManager = $authTokenManager;
    }

    public function handle(Command $command)
    {
        $user = $this->userRepository->get(new Uuid($command->getId()));

        $refreshToken = $this->tokenGenerator->generate(
            new DateTimeImmutable(),
            new DateInterval('P1Y')
        );
        $user->refresh($command->getToken(), $refreshToken);
        $this->flusher->flush();

        $data = [
            'alg' => 'RS256',
            'typ' => 'JWT',
            'exp' => time() + 3600 * 24 * 30,
            'id' => (string)$user->getId()
        ];

        return [
            'accessToken' => $this->authTokenManager->encode($data),
            'refreshToken' => $refreshToken->getValue()
        ];
    }
}
