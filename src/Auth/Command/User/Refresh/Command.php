<?php

declare(strict_types=1);

namespace App\Auth\Command\User\Refresh;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните id")
     */
    private string $id;

    /**
     * @Assert\NotBlank(message="Заполните токен")
     */
    private string $token;

    public function __construct(string $id, string $token)
    {
        $this->id = $id;
        $this->token = $token;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
