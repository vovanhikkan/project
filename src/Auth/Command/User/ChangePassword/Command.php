<?php

declare(strict_types=1);

namespace App\Auth\Command\User\ChangePassword;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните id")
     */
    private string $id;

    /**
     * @Assert\NotBlank(message="Заполните текущий пароль")
     */
    private string $passwordCurrent;

    /**
     * @Assert\NotBlank(message="Заполните новый пароль")
     * @Assert\Length(min=6, minMessage="Новый пароль должен быть не короче 6 символов")
     */
    private string $passwordNew;

    public function __construct(
        string $id,
        string $passwordCurrent,
        string $passwordNew
    ) {
        $this->id = $id;
        $this->passwordCurrent = $passwordCurrent;
        $this->passwordNew = $passwordNew;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPasswordCurrent(): string
    {
        return $this->passwordCurrent;
    }

    public function getPasswordNew(): string
    {
        return $this->passwordNew;
    }
}
