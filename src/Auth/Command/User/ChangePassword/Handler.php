<?php

declare(strict_types=1);

namespace App\Auth\Command\User\ChangePassword;

use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\User\PasswordChanger;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private PasswordChanger $passwordChanger;

    public function __construct(
        UserRepository $userRepository,
        PasswordChanger $passwordChanger
    ) {
        $this->userRepository = $userRepository;
        $this->passwordChanger = $passwordChanger;
    }

    public function handle(Command $command): void
    {
        $user = $this->userRepository->get(new Uuid($command->getId()));

        $this->passwordChanger->changePassword(
            $user,
            $command->getPasswordCurrent(),
            $command->getPasswordNew()
        );
    }
}
