<?php

declare(strict_types=1);

namespace App\Auth\Command\User\Block;

use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\User\Blocker;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private Blocker $blocker;

    public function __construct(
        UserRepository $userRepository,
        Blocker $blocker
    ) {
        $this->userRepository = $userRepository;
        $this->blocker = $blocker;
    }

    public function handle(Command $command): void
    {
        $user = $this->userRepository->get(new Uuid($command->getId()));
        $this->blocker->block($user);
    }
}
