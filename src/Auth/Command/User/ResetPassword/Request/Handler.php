<?php

declare(strict_types=1);

namespace App\Auth\Command\User\ResetPassword\Request;

use App\Application\ValueObject\Email;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\User\ResetPasswordConfirmTokenSender;
use App\Auth\Service\User\TokenGenerator;
use App\Data\Flusher;
use DateInterval;
use DateTimeImmutable;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private TokenGenerator $tokenGenerator;
    private Flusher $flusher;
    private ResetPasswordConfirmTokenSender $resetPasswordConfirmTokenSender;

    public function __construct(
        UserRepository $userRepository,
        TokenGenerator $tokenGenerator,
        Flusher $flusher,
        ResetPasswordConfirmTokenSender $resetPasswordConfirmTokenSender
    ) {
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
        $this->flusher = $flusher;
        $this->resetPasswordConfirmTokenSender = $resetPasswordConfirmTokenSender;
    }

    public function handle(Command $command): void
    {
        $user = $this->userRepository->getByEmail(new Email($command->getEmail()));

        $token = $this->tokenGenerator->generate(new DateTimeImmutable(), new DateInterval('PT24H'));
        $user->resetPasswordRequest($token);

        $this->flusher->flush();

        $this->resetPasswordConfirmTokenSender->send($user->getEmail(), $token->getValue());
    }
}
