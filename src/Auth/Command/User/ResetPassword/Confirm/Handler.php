<?php

declare(strict_types=1);

namespace App\Auth\Command\User\ResetPassword\Confirm;

use App\Auth\Repository\UserRepository;
use App\Auth\Service\User\PasswordManager;
use App\Data\Flusher;
use InvalidArgumentException;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private PasswordManager $passwordManager;
    private Flusher $flusher;

    public function __construct(
        UserRepository $userRepository,
        PasswordManager $passwordManager,
        Flusher $flusher
    ) {
        $this->userRepository = $userRepository;
        $this->passwordManager = $passwordManager;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $user = $this->userRepository->fetchOneByResetPasswordToken($command->getToken());
        if ($user === null) {
            throw new InvalidArgumentException('Токен не найден');
        }

        $user->resetPasswordConfirm($command->getToken(), $command->getPassword(), $this->passwordManager);

        $this->flusher->flush();
    }
}
