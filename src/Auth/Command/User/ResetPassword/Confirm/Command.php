<?php

declare(strict_types=1);

namespace App\Auth\Command\User\ResetPassword\Confirm;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank(message="Заполните токен")
     */
    private string $token;

    /**
     * @Assert\NotBlank(message="Заполните пароль")
     * @Assert\Length(min=6, minMessage="Новый пароль должен быть не короче 6 символов")
     */
    private string $password;

    public function __construct(string $token, string $password)
    {
        $this->token = $token;
        $this->password = $password;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
