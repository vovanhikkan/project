<?php

declare(strict_types=1);

namespace App\Auth\Command\User\VerifyPhone\Confirm;

use App\Auth\Service\User\PhoneVerifier;

/**
 * Handler.
 */
class Handler
{
    private PhoneVerifier $phoneVerifier;

    public function __construct(PhoneVerifier $phoneVerifier)
    {
        $this->phoneVerifier = $phoneVerifier;
    }

    public function handle(Command $command): void
    {
        $this->phoneVerifier->confirm($command->getToken());
    }
}
