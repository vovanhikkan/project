<?php

declare(strict_types=1);

namespace App\Auth\Command\User\VerifyPhone\Request;

use App\Application\ValueObject\Uuid;
use App\Auth\Repository\UserRepository;
use App\Auth\Service\User\PhoneVerifier;

/**
 * Handler.
 */
class Handler
{
    private UserRepository $userRepository;
    private PhoneVerifier $phoneVerifier;

    public function __construct(
        UserRepository $userRepository,
        PhoneVerifier $phoneVerifier
    ) {
        $this->userRepository = $userRepository;
        $this->phoneVerifier = $phoneVerifier;
    }

    public function handle(Command $command): void
    {
        $user = $this->userRepository->get(new Uuid($command->getId()));
        $this->phoneVerifier->request($user);
    }
}
