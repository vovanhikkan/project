<?php

declare(strict_types=1);

namespace App\Auth\Repository;

use App\Application\Exception\NotFoundException;
use App\Application\Repository\AbstractRepository;
use App\Application\ValueObject\Email;
use App\Application\ValueObject\Phone;
use App\Application\ValueObject\Uuid;
use App\Auth\Model\User\User;

/**
 * UserRepository.
 */
class UserRepository extends AbstractRepository
{
    public function add(User $user): void
    {
        $this->entityManager->persist($user);
    }

    public function existByEmail(Email $email, ?Uuid $excludeId = null): bool
    {
        $qb = $this->entityRepository->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->where('LOWER(u.email) = :email')
            ->setParameter('email', mb_strtolower((string)$email))
        ;

        if ($excludeId !== null) {
            $qb->andWhere('u.id != :excludeId')->setParameter('excludeId', $excludeId);
        }

        return (int)$qb->getQuery()->getSingleScalarResult() > 0;
    }

    public function existByPhone(Phone $phone, ?Uuid $excludeId = null): bool
    {
        $qb = $this->entityRepository->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->where('u.phone = :phone')
            ->setParameter('phone', (string)$phone)
        ;

        if ($excludeId !== null) {
            $qb->andWhere('u.id != :excludeId')->setParameter('excludeId', $excludeId);
        }

        return (int)$qb->getQuery()->getSingleScalarResult() > 0;
    }

    /**
     * @return User[]
     */
    public function fetchAll(): array
    {
        $qb = $this->entityRepository->createQueryBuilder('u');
        $qb->orderBy('u.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function fetchOneByResetPasswordToken(string $token): ?User
    {
        return $this->entityRepository->findOneBy(['resetPasswordToken.value' => $token]);
    }

    public function fetchOneByVerifyEmailToken(string $token): ?User
    {
        return $this->entityRepository->findOneBy(['verifyEmailToken.value' => $token]);
    }

    public function fetchOneByVerifyPhoneToken(string $token): ?User
    {
        return $this->entityRepository->findOneBy(['verifyPhoneToken.value' => $token]);
    }

    public function get(Uuid $id): User
    {
        /** @var User|null $model */
        $model = $this->entityRepository->find($id);
        if ($model === null) {
            throw new NotFoundException(sprintf('User with id %s not found', (string)$id));
        }

        return $model;
    }

    public function getByEmail(Email $email): User
    {
        $qb = $this->entityRepository->createQueryBuilder('u')
            ->where('LOWER(u.email) = :email')
            ->setParameter('email', mb_strtolower((string)$email))
        ;

        $model = $qb->getQuery()->getOneOrNullResult();
        if ($model === null) {
            throw new NotFoundException(sprintf('User with e-mail %s not found', (string)$email));
        }

        return $model;
    }

    protected function getModelClassName(): string
    {
        return User::class;
    }
}
