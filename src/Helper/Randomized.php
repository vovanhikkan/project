<?php

declare(strict_types=1);

namespace App\Helper;

class Randomized
{
    private const CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    private const CHARSET_NUMBER = '0123456789';

private static string $string = '';

    /**
     * @param int $length
     *
     * @return string
     */
    public static function generateString(int $length): string
    {
        return self::generate(self::CHARSET, $length);
    }

    /**
     * @param int $length
     *
     * @return string
     */
    public static function generateNumber(int $length): string
    {
        return self::generate(self::CHARSET_NUMBER, $length);
    }

    /**
     * @param string $charset
     * @param        $length
     *
     * @return string
     */
    private static function generate(string $charset, $length): string
    {
        for ($i = 0; $i < $length; $i++) {
            $character    = $charset[mt_rand(0, strlen($charset) - 1)];
            self::$string .= $character;
        }

        return self::$string;
    }
}
