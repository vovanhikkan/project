<?php
namespace App\Helper;

use Carbon\Carbon;

/**
 * Класс-хелпер для работы с ППС
 * Class PPSHelper
 * @package App\Helpers
 */
class PPSHelper
{
    /**
     * Параметр "Дата выпуска"
     */
    const TIME_RELEASE_PARAMETER = 'TimeRelease';

    /**
     * Параметр "ID карты"
     */
    const CARD_ID_PARAMETER = 'ID_CARD';

    /**
     * Параметр "ID клиента"
     */
    const CLIENT_ID = 'CLI_TNODID';

    /**
     * Параметр "Цена на тариф"
     */
    const REWRITE_PRICE = 'REWRITE_PRICE';

    /**
     * Параметр "Скидка на тариф"
     */
    const REWRITE_DISCOUNT = 'REWRITE_DISCOUNT';

    /**
     * Получение параметра услуги ППС по его ID
     * @param mixed $service
     * @param string $paramId
     * @return mixed|null
     */
    public static function getServiceParameter($service, string $paramId)
    {
        foreach ($service->getParameters() as $parameter) {
            if ($parameter->getID() == $paramId) {
                return $parameter;
            }
        }
        return null;
    }

    /**
     * Устанавливает параметр услуги
     * @param mixed $service
     * @param string $paramId
     * @param string $value
     */
    public static function setServiceParameter($service, string $paramId, string $value): void
    {
        foreach ($service->getParameters() as $parameter) {
            if ($parameter->getID() == $paramId) {
                $parameter->setValue($value);
            }
        }
    }

    /**
     * Устанавливает дату выпуска для услуги
     * @param mixed $service
     * @param Carbon $date
     */
    public static function setTimeRelease($service, Carbon $date): void
    {
        self::setServiceParameter($service, self::TIME_RELEASE_PARAMETER, CarbonHelper::format($date));
    }

    /**
     * Устанавливает ID карты для услуги
     * @param mixed $service
     * @param string $cardId
     */
    public static function setCardId($service, string $cardId): void
    {
//        $id = self::correctCard($id);     //#49091 (RK-478) не отгружается с release-3.0.0
        self::setServiceParameter($service, self::CARD_ID_PARAMETER, $cardId);
    }

    /**
     * Устанавливает ID клиента для услуги
     * @param mixed $service
     * @param int $clientId
     */
    public static function setClientId($service, int $clientId): void
    {
        self::setServiceParameter($service, self::CLIENT_ID, $clientId);
    }

    /**
     * Устанавливает новую цену на услугу с учетом списания баллов
     * @param mixed $service
     * @param float $price
     */
    public static function setRewritePrice($service, float $price): void
    {
        self::setServiceParameter($service, self::REWRITE_PRICE, $price);
    }

    /**
     * Устанавливает скидку на услугу
     * @param mixed $service
     * @param float $discount
     */
    public static function setRewriteDiscount($service, float $discount): void
    {
        self::setServiceParameter($service, self::REWRITE_DISCOUNT, $discount);
    }

    /**
     * Корректирует номер карты единого скипасса для ППС
     * @param String $cardId
     * @return String
     */
    public static function correctCard(string $cardId): string
    {
        if (preg_match('/^[0-9]{20}$/i', $cardId)) {
            return strtoupper(gmp_strval($cardId, 16));
        }
        return $cardId;
    }
}
