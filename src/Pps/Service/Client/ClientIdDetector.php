<?php

declare(strict_types=1);

namespace App\Pps\Service\Client;

use App\Application\Exception\DomainException;
use App\Application\ValueObject\FirstName;
use App\Application\ValueObject\Id;
use App\Application\ValueObject\LastName;
use App\Application\ValueObject\MiddleName;
use App\Order\Model\Order\OrderItemProduct;
use DateTimeImmutable;

/**
 * ClientIdDetector.
 */
class ClientIdDetector
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function ensureOrderItemHasClientId(OrderItemProduct $orderItem): void
    {
        if ($orderItem->getClientId() !== null) {
            return;
        }

        if ($orderItem->getLastName() === null) {
            throw new DomainException('У клиента не задана фамилия');
        }

        if ($orderItem->getFirstName() === null) {
            throw new DomainException('У клиента не задано имя');
        }

        if ($orderItem->getBirthDate() === null) {
            throw new DomainException('У клиента не задана дата рождения');
        }

        $clientId = $this->searchClient(
            $orderItem->getLastName(),
            $orderItem->getFirstName(),
            $orderItem->getMiddleName(),
            $orderItem->getBirthDate()
        );

        if ($clientId === null) {
            $clientId = $this->addClient(
                $orderItem->getLastName(),
                $orderItem->getFirstName(),
                $orderItem->getMiddleName(),
                $orderItem->getBirthDate()
            );
        }

        $orderItem->changeClientId($clientId);
    }

    public function searchClient(
        LastName $lastName,
        FirstName $firstName,
        ?MiddleName $middleName,
        DateTimeImmutable $birthDate
    ): ?Id {
        $client = [
            'FirstName' => (string)$firstName,
            'LastName' => (string)$lastName,
            'BirthDate' => $birthDate->format('Y-m-d')
        ];

        $mask = [
            'FirstName' => true,
            'LastName' => true,
            'BirthDate' => true,
        ];

        if ($middleName !== null) {
            $client['SecondName'] = (string)$middleName;
            $mask['SecondName'] = true;
        }

        $data = [
            'SearchClient' => [
                'client' => $client,
                'mask' => $mask
            ]
        ];

        $result = $this->client->request('SearchClient', $data);

        if (isset($result->SearchClientResult->Client)) {
            if (is_array($result->SearchClientResult->Client)) {
                return new Id($result->SearchClientResult->Client[0]->tnodID);
            } else {
                return new Id($result->SearchClientResult->Client->tnodID);
            }
        } else {
            return null;
        }
    }

    public function addClient(
        LastName $lastName,
        FirstName $firstName,
        ?MiddleName $middleName,
        DateTimeImmutable $birthDate
    ): ?Id {
        $client = [
            'FirstName' => (string)$firstName,
            'LastName' => (string)$lastName,
            'BirthDate' => $birthDate->format('Y-m-d')
        ];

        if ($middleName !== null) {
            $client['SecondName'] = (string)$middleName;
        }

        $data = [
            'AddClient' => [
                'client' => $client,
            ]
        ];

        $result = $this->client->request('AddClient', $data);

        return isset($result->AddClientResult->tnodID)
            ? new Id($result->AddClientResult->tnodID)
            : null;
    }
}
