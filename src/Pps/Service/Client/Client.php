<?php

declare(strict_types=1);

namespace App\Pps\Service\Client;

use Psr\Log\LoggerInterface;
use RuntimeException;
use SoapClient;
use SoapHeader;
use Throwable;

/**
 * Client.
 */
class Client
{
    private string $url;
    private string $login;
    private string $password;
    private ?SoapClient $client = null;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->url = getenv('CLIENT_SERVICE_URL');
        $this->login = getenv('RH_EXTERNAL_SERVICE_LOGIN');
        $this->password = getenv('RH_EXTERNAL_SERVICE_PASSWORD');
        $this->logger = $logger;
    }

    public function request(string $method, array $data): object
    {
        $this->logger->info('client service request ', ['method' => $method, 'data' => $data]);

        try {
            $result = $this->getClient()->__soapCall($method, $data, null, $this->getHeaders($method));
            $this->logger->info(
                'client service success',
                [
                    'request' => $this->getClient()->__getLastRequest(),
                    'response' => $this->getClient()->__getLastResponse(),
                ]
            );

            return $result;
        } catch (Throwable $e) {
            $this->logger->info(
                'client service failed',
                [
                    'request' => $this->getClient()->__getLastRequest(),
                    'response' => $this->getClient()->__getLastResponse(),
                    'error' => $e->getMessage()
                ]
            );
            throw new RuntimeException(
                sprintf('Не удалось выполнить запрос: %s', $e->getMessage())
            );
        }
    }

    private function getHeaders(string $method): array
    {
        return [
            new SoapHeader('http://www.w3.org/2005/08/addressing', 'Action', 'http://tempuri.org/IISDClientWCFService/' . $method, true),
            new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', $this->url)
        ];
    }

    private function getClient(): SoapClient
    {
        if ($this->client === null) {
            $options = [
                'login' => $this->login,
                'password' => $this->password,
                'features' => true,
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_MEMORY,
                'compression' => (SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP),
                'stream_context' => stream_context_create([
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true,
                    ]
                ]),
                'trace' => true
            ];

            $this->client = new SoapClient(__DIR__ . '/ISDClient.wsdl', $options);
            $this->client->__setLocation($this->url);
        }

        return $this->client;
    }
}
