<?php

declare(strict_types=1);

namespace App\Pps\Service\External;

use App\Application\Exception\DomainException;
use App\Application\ValueObject\CardNum;
use App\Application\ValueObject\Id;
use App\Order\Model\Order\OrderItemProduct;

/**
 * CardChecker.
 */
class CardChecker
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function ensureOrderItemHasClientId(OrderItemProduct $orderItem): void
    {
        if ($orderItem->getClientId() !== null) {
            return;
        }

        if ($orderItem->getCardNum() === null) {
            throw new DomainException('У клиента не задан номер карты');
        }

        $clientId = $this->check($orderItem->getCardNum());

        if ($clientId === null) {
            throw new DomainException('Не найден clientId по номеру карты');
        }

        $orderItem->changeClientId($clientId);
    }

    public function check(CardNum $cardNum): ?Id
    {
        $result = $this->client->request(
            'GetClientIDByMedia',
            [
                'MediaType' => 11,
                'MediaNUM' => $cardNum->getValue(),
            ]
        );

        return isset($result->GetClientIDByMediaResult) && $result->GetClientIDByMediaResult > 0
            ? new Id($result->GetClientIDByMediaResult) : null;
    }
}
