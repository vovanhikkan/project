<?php

declare(strict_types=1);

namespace App\Pps\Service\External;

use App\Application\ValueObject\Id;

/**
 * ServiceFetcher.
 */
class ServiceFetcher
{
    private int $cashPointId;
    private Client $client;

    public function __construct(Client $client)
    {
        $this->cashPointId = (int)getenv('CASH_POINT_ID');
        $this->client = $client;
    }

    public function get(Id $id): object
    {
        $result = $this->client->request(
            'GetService',
            [
                'CashPointID' => $this->cashPointId,
                'ServiceID' => $id->getValue()
            ]
        );

        return $result->GetServiceResult;
    }

    public function fetchAll(): array
    {
        $result = $this->client->request(
            'GetServices',
            [
                'CashPointID' => $this->cashPointId,
            ]
        );

        return $result->GetServicesResult->Service ?? [];
    }
}
