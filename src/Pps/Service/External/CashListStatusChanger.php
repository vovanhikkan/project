<?php

declare(strict_types=1);

namespace App\Pps\Service\External;

use App\Application\ValueObject\Id;

/**
 * CashListStatusChanger.
 */
class CashListStatusChanger
{
    public const STATUS_NEW = 100;
    public const STATUS_PAYED = 101;

    private int $cashPointId;
    private Client $client;

    public function __construct(Client $client)
    {
        $this->cashPointId = (int)getenv('CASH_POINT_ID');
        $this->client = $client;
    }

    public function changeStatus(Id $cashListId, int $status): void
    {
        $this->client->request(
            'SetCashListStatus',
            [
                'CashPointID' => $this->cashPointId,
                'CashListID' => $cashListId->getValue(),
                'status' => $status
            ]
        );
    }
}
