<?php

declare(strict_types=1);

namespace App\Pps\Service\External;

/**
 * ServiceChecker.
 */
class ServiceChecker
{
    private int $cashPointId;
    private Client $client;

    public function __construct(Client $client)
    {
        $this->cashPointId = (int)getenv('CASH_POINT_ID');
        $this->client = $client;
    }

    public function check(object $service): ?object
    {
        $result = $this->client->request(
            'CheckService',
            [
                'CashPointID' => $this->cashPointId,
                'CashListID' => 0,
                'service' => $service
            ]
        );

        return $result->CheckServiceResult->CashItem ?? null;
    }
}
