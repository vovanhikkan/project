<?php

declare(strict_types=1);

namespace App\Pps\Service\External;

use App\Application\Exception\DomainException;
use App\Application\ValueObject\Barcode;
use App\Application\ValueObject\Id;
use App\Order\Model\Order\Order;
use App\Order\Model\Order\OrderItemProduct;
use App\Pps\Service\Client\ClientIdDetector;
use App\Product\Model\CardType\CardType;
use App\Product\Model\CardType\Type;
use App\Product\Repository\CardTypeRepository;
use RuntimeException;

/**
 * OrderCreator.
 */
class OrderCreator
{
    private int $cashPointId;
    private int $userId;
    private Client $client;
    private ClientIdDetector $clientIdDetector;
    private CardTypeRepository $cardTypeRepository;
    private CardChecker $cardChecker;
    private ServiceFetcher $serviceFetcher;
    private CashListStatusChanger $cashListStatusChanger;

    public function __construct(
        Client $client,
        ClientIdDetector $clientIdDetector,
        CardTypeRepository $cardTypeRepository,
        CardChecker $cardChecker,
        ServiceFetcher $serviceFetcher,
        CashListStatusChanger $cashListStatusChanger
    ) {
        $this->cashPointId = (int)getenv('CASH_POINT_ID');
        $this->userId = (int)getenv('RH_EXTERNAL_USER_ID');
        $this->client = $client;
        $this->clientIdDetector = $clientIdDetector;
        $this->cardTypeRepository = $cardTypeRepository;
        $this->cardChecker = $cardChecker;
        $this->serviceFetcher = $serviceFetcher;
        $this->cashListStatusChanger = $cashListStatusChanger;
    }

    public function create(Order $order): void
    {
        $isProductsInOrder = false;

        if (count($order->getOrderItemsProduct())) {
            $isProductsInOrder = true;
        }

        if ($isProductsInOrder) {
            $cashListId = $this->createCashList();
            $order->changeCashListId($cashListId);

            foreach ($order->getOrderItemsProduct() as $orderItem) {
                $this->processOrderItem($orderItem);
            }

            if ($isProductsInOrder) {
                $this->cashListStatusChanger->changeStatus($order->getCashListId(), CashListStatusChanger::STATUS_NEW);
            }

            $barcode = $this->getCashListBarcode($order->getCashListId());
            $order->changeBarcode($barcode);

        } else {
            $cashListId = new Id(0);
            $order->changeCashListId($cashListId);
        }
    }

    private function processOrderItem(OrderItemProduct $orderItem): void
    {
        $product = $orderItem->getProduct();

        if ($product->getProductSettings() === null) {
            throw new DomainException('У продукта не определён тип карты');
        }

        $cardType = $product->getProductSettings()->getCardType();
        $cardType = $this->cardTypeRepository->fetchOneByType(new Type($cardType->getValue()));

        $isNeedClientAssign = false;
        switch (true) {
            case $cardType->getType()->isNone():
            case $cardType->getType()->isPlastic():
                // Не требуется привязка к клиенту
                break;
            case $cardType->getType()->isPlasticFree():
                $isNeedClientAssign = true;
                if ($orderItem->getCardNum() !== null) {
                    $this->cardChecker->ensureOrderItemHasClientId($orderItem);
                } else {
                    $this->clientIdDetector->ensureOrderItemHasClientId($orderItem);
                }
                break;
            default:
                throw new DomainException('Неизвестный тип карты');
        }

        $cashItemId = $this->orderService($orderItem, $cardType, $isNeedClientAssign);
        $orderItem->changeCashItemId($cashItemId);

        if ($isNeedClientAssign) {
            $this->setCashItemClientId($orderItem);
        }
    }

    private function getCashListBarcode(Id $cashListId): ?Barcode
    {
        $result = $this->client->request(
            'GetCashListBarcode',
            [
                'CashListID' => $cashListId->getValue()
            ]
        );

        return isset($result->GetCashListBarcodeResult) ? new Barcode($result->GetCashListBarcodeResult) : null;
    }

    private function setCashItemClientId(OrderItemProduct $orderItem): void
    {
        if ($orderItem->getCashItemId() === null) {
            throw new DomainException('Не установлен cashItemId для OrderItemProduct');
        }

        if ($orderItem->getClientId() === null) {
            throw new DomainException('Не установлен clientId для OrderItemProduct');
        }

        $this->client->request(
            'SetCashItemClientID',
            [
                'CashItemID' => $orderItem->getCashItemId()->getValue(),
                'ClientID' => $orderItem->getClientId()->getValue(),
            ]
        );
    }

    private function orderService(OrderItemProduct $orderItem, CardType $cardType, bool $isNeedClientAssign): ?Id
    {
        $product = $orderItem->getProduct();
        $productSettings = $product->getProductSettings();

        $ppsProductId = null;
        switch (true) {
            case $cardType->getType()->isNone():
                $ppsProductId = $productSettings->getPpsProductId()->getValue() !== 0
                    ? $productSettings->getPpsProductId()
                    : null;
                break;
            case $cardType->getType()->isPlastic():
                if ($productSettings->getNewCardPpsProductId()->getValue() !== 0) {
                    $ppsProductId = $productSettings->getNewCardPpsProductId();
                } elseif ($productSettings->getPpsProductId()->getValue() !== 0) {
                    $ppsProductId = $productSettings->getPpsProductId();
                }
                break;
            case $cardType->getType()->isPlasticFree():
                if ($orderItem->getCardNum() !== null) {
                    $ppsProductId = $productSettings->getOldCardPpsProductId();
                } else {
                    $ppsProductId = $productSettings->getNewCardPpsProductId();
                }
                break;
            default:
                throw new DomainException('Неизвестный тип карты');
        }

        if (!($ppsProductId !== null && $ppsProductId->getValue() !== 0)) {
            throw new DomainException('Не удалось определить ID продукта в ППС');
        }

        $service = $this->serviceFetcher->get($ppsProductId);

        $parameters = [];

        if ($orderItem->getActivationDate() !== null) {
            $parameters[] = [
                'ID' => 'TimeRelease',
                'Value' => $orderItem->getActivationDate()->format('d.m.Y')
            ];
        }

        if ($orderItem->getCardNum() !== null) {
            $parameters[] = [
                'ID' => 'ID_CARD',
                'Value' => $orderItem->getCardNum()->getValue()
            ];
        }

        if ($isNeedClientAssign) {
            if ($orderItem->getClientId() === null) {
                throw new DomainException('Не указан client id у позиции заказа');
            }
            $parameters[] = [
                'ID' => 'CLI_TNODID',
                'Value' => $orderItem->getClientId()->getValue()
            ];
        }

        $result = $this->client->request(
            'OrderService',
            [
                'CashPointID' => $this->cashPointId,
                'CashListID' => $orderItem->getOrder()->getCashListId()->getValue(),
                'service' => [
                    'ID' => $service->ID,
                    'ModuleID' => $service->ModuleID,
                    'Parameters' => [
                        'ServiceParameter' => $parameters
                    ]
                ]
            ]
        );

        $cashItemId = null;
        if (is_array($result->OrderServiceResult->CashItem)) {
            foreach ($result->OrderServiceResult->CashItem as $cashItem) {
                if ($cashItem->ServiceID === $service->ID) {
                    $cashItemId = new Id($cashItem->ID);
                    break;
                }
            }
        } else {
            $cashItemId = new Id($result->OrderServiceResult->CashItem->ID);
        }

        if ($cashItemId === null) {
            throw new RuntimeException('Не удалось создать заказ в ППС');
        }

        if ($cardType->getType()->isPlastic() || $cardType->getType()->isPlasticFree()) {
            $service = $this->serviceFetcher->get($cardType->getPpsProductId());
            $this->client->request(
                'OrderService',
                [
                    'CashPointID' => $this->cashPointId,
                    'CashListID' => $orderItem->getOrder()->getCashListId()->getValue(),
                    'service' => [
                        'ID' => $service->ID,
                        'ModuleID' => $service->ModuleID,
                    ]
                ]
            );
        }

        return $cashItemId;
    }

    private function createCashList(): ?Id
    {
        $result = $this->client->request(
            'CreateCashList',
            [
                'CashPointID' => $this->cashPointId,
                'UserID' => $this->userId
            ]
        );

        return isset($result->CreateCashListResult) ? new Id($result->CreateCashListResult) : null;
    }
}
